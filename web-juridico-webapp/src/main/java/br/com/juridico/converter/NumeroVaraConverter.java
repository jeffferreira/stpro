package br.com.juridico.converter;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;

import org.apache.log4j.Logger;

import br.com.juridico.entidades.NumeroVara;
import br.com.juridico.impl.NumeroVaraSessionBean;

/**
 * 
 * @author Jefferson
 *
 */
@ManagedBean
public class NumeroVaraConverter implements Converter, Serializable {

	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = Logger.getLogger(NumeroVaraConverter.class);

	@Inject
	private NumeroVaraSessionBean numeroVaraSessionBean;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		try {
			if (value.isEmpty() || value.contains("^[a-Z]")) {
				return null;
			}
			return numeroVaraSessionBean.retrieve(Long.valueOf(value));
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			return null;
		}
	}

	@Override
	public String getAsString(FacesContext context, UIComponent componet, Object value) {
		if (value == null) {
			return null;
		}
		if (((NumeroVara) value).getId() == null) {
			return null;
		}
		return ((NumeroVara) value).getId().toString();
	}

}
