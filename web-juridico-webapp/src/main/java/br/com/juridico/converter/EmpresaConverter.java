package br.com.juridico.converter;

import br.com.juridico.entidades.Empresa;
import br.com.juridico.entidades.NumeroVara;
import br.com.juridico.impl.EmpresaSessionBean;
import com.google.common.base.Strings;

import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import java.io.Serializable;

/**
 * Created by jefferson.ferreira on 11/05/2017.
 */
@ManagedBean
public class EmpresaConverter implements Converter, Serializable {

    @Inject
    private EmpresaSessionBean empresaSessionBean;

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String value) {
        if(Strings.isNullOrEmpty(value)){
            return null;
        }
        return empresaSessionBean.retrieve(Long.parseLong(value));
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object value) {
        if (value == null) {
            return null;
        }
        if (((Empresa) value).getId() == null) {
            return null;
        }
        return ((Empresa) value).getId().toString();
    }
}
