package br.com.juridico.converter;


import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import br.com.juridico.util.ClassHelper;


/**
 * Classe para generalizacao de comportamento de {@link Converter} do JSF para objetos Enum<E>.
 * 
 * @author Jefferson Ferreira
 * 
 * @see AbstractEnumConverter
 * 
 */
public abstract class AbstractEnumConverter<E extends Enum<E>> implements Converter {
	
	private transient Logger logger = Logger.getLogger(AbstractEnumConverter.class);
	
	private static final Map<String, Method> valueOfMethodCache = Collections.synchronizedMap(new WeakHashMap<String,Method>());

	
	/**
	 * 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {

		E result = null;

		if(StringUtils.isEmpty(value) || null == value)
			return result;

		Method valueOf;
		String converterName = this.getClass().getSimpleName();

		if(null == (valueOf = valueOfMethodCache.get(converterName))) {

			Class<E> clazz = (Class<E>) ClassHelper.findGenericClass(this.getClass(), AbstractEnumConverter.class, 0);

			try {

				valueOf = clazz.getDeclaredMethod("valueOf", new Class<?> [] {String.class});
				
				valueOfMethodCache.put(converterName,valueOf);

			} catch (SecurityException e) {
				logger.error(e.getMessage(), e);
			} catch (NoSuchMethodException e) {
				logger.error(e.getMessage(), e);
			}

		}
		
		if(valueOf != null)
			try {
				result = (E) valueOf.invoke(null, value);
			} catch (IllegalArgumentException e) {
				logger.error(e.getMessage(), e);
			} catch (IllegalAccessException e) {
				logger.error(e.getMessage(), e);
			} catch (InvocationTargetException e) {
				logger.error(e.getMessage(), e);
			}
		
		return result;
	}

	/**
	 * 
	 */
	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {

		if(value == null)
			return "";

		return value.toString();

	}

}	
