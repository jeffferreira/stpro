package br.com.juridico.business;

import br.com.juridico.dto.ParteDto;
import br.com.juridico.entidades.PessoaFisica;
import br.com.juridico.entidades.Processo;
import br.com.juridico.exception.ValidationException;

import java.util.List;

/**
 * Created by jeffersonl2009_00 on 06/07/2016.
 */
public class ParteProcessoBusiness {

    private ParteProcessoBusiness(){}

    public static void adicionarParte(ParteDto parteDto, List<ParteDto> partes) {
        for (PessoaFisica pf : parteDto.getSelectedAdvogados()) {
            parteDto.addAdvogado(pf.getPessoa());
        }
        partes.add(parteDto);
    }
}
