package br.com.juridico.business;

/**
 * Created by jefferson.ferreira on 09/03/2017.
 */
public class OrganogramaBusiness {

    private OrganogramaBusiness(){}

    private static final String NO_UL_LI_PREFIX = "<ul><li>";
    private static final String NO_UL_LI_SUFIX = "</li></ul>";
    private static final String NO_VERTICAL_PREFIX = "<ul class=\"vertical\">";
    private static final String HREF_PREFIX = "<a href=\"#\">";
    private static final String HREF_SUFIX = "</a>";
    private static final String LI_PREFIX = "<li>";
    private static final String LI_SUFIX = "</li>";
    private static final String UL_PREFIX = "<ul>";
    private static final String UL_SUFIX = "</ul>";

    /**
     * <ul><li>
     *
     * @param code
     * @return
     */
    public static StringBuilder abrirTag(StringBuilder code){
        StringBuilder html = new StringBuilder(code);
        html.append(NO_UL_LI_PREFIX);
        return html;
    }

    /**
     * <ul><li><a href="#">item</a>
     *
     * @param code
     * @param item
     * @return
     */
    public static StringBuilder abrirTagItem(StringBuilder code, String item){
        StringBuilder html = new StringBuilder(code);
        html.append(NO_UL_LI_PREFIX + HREF_PREFIX + item + HREF_SUFIX);
        return html;
    }

    /**
     * </li></ul>
     *
     * @param code
     * @return
     */
    public static StringBuilder fecharTagItem(StringBuilder code){
        StringBuilder html = new StringBuilder(code);
        html.append(NO_UL_LI_SUFIX);
        return html;
    }

    /**
     * <li><a href="#">item
     *
     * @param code
     * @param child
     * @return
     */
    public static StringBuilder abrirTagFilho(StringBuilder code, String child){
        StringBuilder html = new StringBuilder(code);
        html.append(LI_PREFIX + HREF_PREFIX + child);
        return html;
    }

    /**
     * </a></li>
     *
     * @param code
     * @return
     */
    public static StringBuilder fecharTagFilho(StringBuilder code){
        StringBuilder html = new StringBuilder(code);
        html.append(HREF_SUFIX + LI_SUFIX);
        return html;
    }

    /**
     * <ul class="vertical">
     *
     * @param code
     * @return
     */
    public static StringBuilder abrirNoVertical(StringBuilder code){
        StringBuilder html = new StringBuilder(code);
        html.append(NO_VERTICAL_PREFIX);
        return html;
    }

    /**
     * </ul>
     *
     * @param code
     * @return
     */
    public static StringBuilder fecharNoVertical(StringBuilder code){
        StringBuilder html = new StringBuilder(code);
        html.append(UL_SUFIX);
        return html;
    }

    /**
     * <li>
     * <a href="#">Delivery</a>
     *
     * @param code
     * @return
     */
    public static StringBuilder abrirSomenteNoFilho(StringBuilder code, String child){
        StringBuilder html = new StringBuilder(code);
        html.append(LI_PREFIX + HREF_PREFIX + child + HREF_SUFIX);
        return html;

    }

    /**
     * </li>
     *
     * @param code
     * @return
     */
    public static StringBuilder fecharSomenteNoFilho(StringBuilder code){
        StringBuilder html = new StringBuilder(code);
        html.append(LI_SUFIX);
        return html;

    }

    /**
     * <ul>
     *
     * @param code
     * @return
     */
    public static StringBuilder abrirUl(StringBuilder code){
        StringBuilder html = new StringBuilder(code);
        html.append(UL_PREFIX);
        return html;

    }

    /**
     * </ul>
     *
     * @param code
     * @return
     */
    public static StringBuilder fecharUl(StringBuilder code){
        StringBuilder html = new StringBuilder(code);
        html.append(UL_SUFIX);
        return html;

    }

    /**
     * </a>
     *
     * @param code
     * @return
     */
    public static StringBuilder fecharHref(StringBuilder code){
        StringBuilder html = new StringBuilder(code);
        html.append(HREF_SUFIX);
        return html;

    }
}
