package br.com.juridico.dto;

import com.google.common.base.Strings;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;

/**
 * Created by jefferson.ferreira on 14/03/2017.
 */
public class ArvoreContrato implements Serializable, Comparable<ArvoreContrato> {

    private Long id;
    private String descricao;
    private String posicao;
    private boolean noRaiz;

    public ArvoreContrato(){super();}

    public ArvoreContrato(Long id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    public ArvoreContrato(Long id, String descricao, String posicao) {
        this.id = id;
        this.descricao = descricao;
        this.posicao = posicao;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getPosicao() {
        return posicao;
    }

    public void setPosicao(String posicao) {
        this.posicao = posicao;
    }

    public boolean isNoRaiz() {
        return noRaiz;
    }

    public void setNoRaiz(boolean noRaiz) {
        this.noRaiz = noRaiz;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(id).append(descricao).append(posicao).toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ArvoreContrato) {
            final ArvoreContrato other = (ArvoreContrato) obj;
            return new EqualsBuilder().append(id, other.id).append(descricao, other.descricao).append(posicao, other.posicao).isEquals();
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        if(Strings.isNullOrEmpty(this.posicao)) {
            return descricao;
        }
        String templateFormat = "%1$s. %2$s";
        return String.format(templateFormat, posicao, descricao);
    }

    public int compareTo(ArvoreContrato document) {
        return this.getDescricao().compareTo(document.getDescricao());
    }
}
