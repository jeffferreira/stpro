package br.com.juridico.dto;

import br.com.juridico.entidades.Imagem;

public class PessoaAgendaDto {

	private String nomeIniciais;

	private String nome;
	
	private Imagem imagem;
	

	public PessoaAgendaDto(String nome, Imagem imagem, String nomeIniciais){
		this.nome = nome;
		this.imagem = imagem;
		this.nomeIniciais = nomeIniciais;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Imagem getImagem() {
		return imagem;
	}

	public void setImagem(Imagem imagem) {
		this.imagem = imagem;
	}

	public String getNomeIniciais() {
		return nomeIniciais;
	}

	public void setNomeIniciais(String nomeIniciais) {
		this.nomeIniciais = nomeIniciais;
	}
}
