package br.com.juridico.filter;

import org.apache.log4j.Logger;

import javax.servlet.Filter;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * @author Jefferson
 *
 */
public class ManutencaoAuthenticationFilter implements Filter {

	private static final Logger LOGGER = Logger.getLogger(ManutencaoAuthenticationFilter.class);

	private static final Pattern FUNCIONALITY_OPERATION_REGEX = Pattern.compile("^.+/(.+)/(.+)\\.xhtml$");

	public void destroy() {
		// TODO Auto-generated method stub
	}

	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
	}

	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
						 FilterChain chain) throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) servletRequest;
		Boolean permiteAcesso = (Boolean) request.getSession().getAttribute("permiteAcessoManutencao");
		StringBuffer query = request.getRequestURL();
		Matcher matcher = FUNCIONALITY_OPERATION_REGEX.matcher(query);

		if (permiteAcesso == null || permiteAcesso == false) {
			LOGGER.debug("FAZER LOGIN");
			dispatcherToLogin(servletResponse, request);
		} else {
			goToPageRequest(servletResponse, chain, request);
		}
	}

	private void goToPageRequest(ServletResponse response, FilterChain filter, HttpServletRequest request)
			throws IOException, ServletException {
		LOGGER.debug("FOI AUTORIZADO ACESSAR A PAGINA SOLICITADA");
		filter.doFilter(request, response);
	}

	private void dispatcherToLogin(ServletResponse response, HttpServletRequest request)
			throws ServletException, IOException {
		request.getRequestDispatcher("/manutencao.xhtml?faces-redirect-true").forward(request, response);
	}

}
