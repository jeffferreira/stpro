package br.com.juridico.filter;

import br.com.juridico.controller.ApplicationController;
import br.com.juridico.entidades.User;
import br.com.juridico.entidades.UserPrimeiroAcesso;
import com.google.common.base.Strings;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Jefferson
 *
 */
public class AuthenticationFilter implements Filter {

	private static final Logger LOGGER = Logger.getLogger(AuthenticationFilter.class);
	private static final String USER_PRIMEIRO_ACESSO_LOGGED = "userPrimeiroAcessoLogged";
	private static final String USER_LOGGED = "userLogged";
	private static final Pattern FUNCIONALITY_OPERATION_REGEX = Pattern.compile("^.+/(.+)/(.+)\\.xhtml$");
	private static final String LOGIN = "login";
	private static final String ALTERA_SENHA = "altera_senha";
	private static final String RECUPERA_SENHA = "recupera_senha";

	@Inject
	private ApplicationController applicationController;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// init
	}

	@Override
	public void destroy() {
		// destroy
	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
			throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpServletResponse response = (HttpServletResponse) servletResponse;
		User user = (User) request.getSession().getAttribute(USER_LOGGED);

		if (user == null) {
			UserPrimeiroAcesso userPrimeiroAcesso = (UserPrimeiroAcesso) request.getSession().getAttribute(USER_PRIMEIRO_ACESSO_LOGGED);

			if (primeiroAcesso(request, response, userPrimeiroAcesso)) return;
			if (alterarSenha(filterChain, request, response)) return;
			if (recuperarSenha(filterChain, request, response)) return;
		}

		validaXhtml(response, filterChain, request, response, user);
	}

	private boolean primeiroAcesso(HttpServletRequest request, HttpServletResponse response, UserPrimeiroAcesso userPrimeiroAcesso) throws ServletException, IOException {
		if(userPrimeiroAcesso != null) {
			dispatcherToPrimeiroAcesso(request, response);
			return true;
		}
		return false;
	}

	private boolean alterarSenha(FilterChain filterChain, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		StringBuffer query = request.getRequestURL();
		Matcher matcher = FUNCIONALITY_OPERATION_REGEX.matcher(query);
		if (matcher.matches()) {
			String funcionality = matcher.group(1).replaceAll("-", "_");
			String operation = matcher.group(2).replaceAll("-", "_");

			if(LOGIN.equals(funcionality) && ALTERA_SENHA.equals(operation)){
				goToPageRequest(response, filterChain, request);
				return true;
			}
		}
		return false;
	}

	private boolean recuperarSenha(FilterChain filterChain, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		StringBuffer query = request.getRequestURL();
		Matcher matcher = FUNCIONALITY_OPERATION_REGEX.matcher(query);
		if (matcher.matches()) {
			String funcionality = matcher.group(1).replaceAll("-", "_");
			String operation = matcher.group(2).replaceAll("-", "_");

			if(LOGIN.equals(funcionality) && RECUPERA_SENHA.equals(operation)){
				goToPageRequest(response, filterChain, request);
				return true;
			}
		}
		return false;
	}

	private void validaXhtml(HttpServletResponse servletResponse, FilterChain filterChain, HttpServletRequest request, HttpServletResponse response, User user) throws IOException, ServletException {
		StringBuffer query = request.getRequestURL();
		Matcher matcher = FUNCIONALITY_OPERATION_REGEX.matcher(query);

		if (matcher.matches()) {
			String funcionality = matcher.group(1).replaceAll("-", "_");
			String operation = matcher.group(2).replaceAll("-", "_");

			if (user != null && user.getPerfil() != null && hasAccess(user, funcionality, operation)) {
				goToPageRequest(servletResponse, filterChain, request);
			} else if(user != null){
				dispacherToRestrictPage(request, response);
			} else {
				dispatcherToLogin(request, response);
			}
		} else {
			LOGGER.info("NAO FOI POSSIVEL FAZER MATCHER DA QUERY, OU PAGINA NAO CONTEM .xhtml");
		}
	}

	private boolean hasAccess(User user, String funcionality, String operation) {
		String key = funcionality.toUpperCase() + "_" + operation.toUpperCase();
		return !Strings.isNullOrEmpty(user.getAcessosMap().get(key));
	}

	private void goToPageRequest(HttpServletResponse response, FilterChain filter, HttpServletRequest request)
			throws IOException, ServletException {
		LOGGER.debug("FOI AUTORIZADO ACESSAR A PAGINA SOLICITADA");
		if (!response.isCommitted()){
			filter.doFilter(request, response);
		}
	}

	private void dispatcherToLogin(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		req.getRequestDispatcher("/login/login.xhtml?faces-redirect-true").forward(req, res);
	}

	private void dispatcherToPrimeiroAcesso(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		if (!res.isCommitted()) {
			req.getRequestDispatcher("/login/primeiro-acesso.xhtml?faces-redirect=true").forward(req, res);
		}
	}

	private void dispacherToRestrictPage(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		if (!res.isCommitted()) {
			req.getRequestDispatcher("/restrictAccess/restrictAccess.xhtml?faces-redirect=true").forward(req, res);
		}
	}

}
