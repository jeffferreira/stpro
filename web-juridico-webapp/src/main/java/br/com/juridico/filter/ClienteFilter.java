package br.com.juridico.filter;

import br.com.juridico.entidades.Posicao;

/**
 * 
 * @author Jefferson
 *
 */
public class ClienteFilter {

    private String tipoCliente;
    
    private Posicao posicaoSelecionado;

    public String getTipoCliente() {
        return tipoCliente;
    }

    public void setTipoCliente(String tipoCliente) {
        this.tipoCliente = tipoCliente;
    }

    public Posicao getPosicaoSelecionado() {
        return posicaoSelecionado;
    }

    public void setPosicaoSelecionado(Posicao posicaoSelecionado) {
        this.posicaoSelecionado = posicaoSelecionado;
    }
}
