package br.com.juridico.listener;

import br.com.juridico.controller.ApplicationController;
import br.com.juridico.entidades.User;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * Created by jeffersonl2009_00 on 07/11/2016.
 */
public class SessionListener implements HttpSessionListener {

    private static final Logger LOGGER = Logger.getLogger(SessionListener.class);
    private static final String USER_LOGGED = "userLogged";

    @Inject
    private ApplicationController applicationController;

    @Override
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
        LOGGER.info("Session Created: "+httpSessionEvent.getSession().getId());
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
        User user = (User) httpSessionEvent.getSession().getAttribute(USER_LOGGED);
        if(user != null) {
            applicationController.getSessionApplicationMap().remove(user.getLogin());
            LOGGER.info("Session Destroyed: "+httpSessionEvent.getSession().getId());
        }
    }
}
