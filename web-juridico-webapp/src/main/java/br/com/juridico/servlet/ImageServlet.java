package br.com.juridico.servlet;

import br.com.juridico.entidades.Imagem;
import br.com.juridico.impl.ImagemSessionBean;
import br.com.juridico.util.FileUtil;
import com.google.common.base.Strings;

import javax.faces.FactoryFinder;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.context.FacesContextFactory;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

/**
 * Created by jefferson on 23/10/2016.
 */
public class ImageServlet extends HttpServlet {

    private static final String PATH_AVATAR_PADRAO = "/resources/images/avatar/avatar_64.png";

    @Inject
    private ImagemSessionBean imagemSessionBean;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            byte[] imageBytes;
            String idImagem = req.getParameter("idImagem");
            req.getParameter("nome");

            if(!Strings.isNullOrEmpty(idImagem)) {
                imagemSessionBean.clearSession();
                Imagem imagem = imagemSessionBean.retrieve(Long.valueOf(idImagem));
                imageBytes = imagem.getLogo();
            } else {
                String absoluteDiskPath = getFacesContext(req, resp).getExternalContext().getRealPath(PATH_AVATAR_PADRAO);
                File file = new File(absoluteDiskPath);
                imageBytes = FileUtil.getContent(file);
            }

            resp.getOutputStream().write(imageBytes);
            resp.getOutputStream().close();

        } catch (Exception e) {
            resp.getWriter().write(e.getMessage());
            resp.getWriter().close();
        }
    }

    protected FacesContext getFacesContext(HttpServletRequest request, HttpServletResponse response) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        if (facesContext == null) {

            FacesContextFactory contextFactory  = (FacesContextFactory) FactoryFinder.getFactory(FactoryFinder.FACES_CONTEXT_FACTORY);
            LifecycleFactory lifecycleFactory = (LifecycleFactory) FactoryFinder.getFactory(FactoryFinder.LIFECYCLE_FACTORY);
            Lifecycle lifecycle = lifecycleFactory.getLifecycle(LifecycleFactory.DEFAULT_LIFECYCLE);

            facesContext = contextFactory.getFacesContext(request.getSession().getServletContext(), request, response, lifecycle);
            InnerFacesContext.setFacesContextAsCurrentInstance(facesContext);

            UIViewRoot view = facesContext.getApplication().getViewHandler().createView(facesContext, "");
            facesContext.setViewRoot(view);
        }
        return facesContext;
    }

    private abstract static class InnerFacesContext extends FacesContext {
        protected static void setFacesContextAsCurrentInstance(FacesContext facesContext) {
            FacesContext.setCurrentInstance(facesContext);
        }
    }
}
