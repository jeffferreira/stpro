package br.com.juridico.validator;

import br.com.juridico.controller.AbstractController;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.inject.Named;
import javax.servlet.http.Part;

/**
 * Created by Jefferson on 19/06/2016.
 */
@Named
public class ImportadorValidator extends AbstractController {

    private static final int MAX_SIZE = 2 * 1024 * 1024;

    /**
     *
     * @param context
     * @param component
     * @param value
     */
    public void valida(FacesContext context, UIComponent component, Object value) {
        Part arquivo = (Part) value;

        if (arquivo != null && arquivo.getSize() > MAX_SIZE) {
            String mensagem = "Arquivo muito grande. O arquivo deve ter o tamanho máximo de 2mb.";
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, mensagem, null));
        }

    }

    /**
     *
     * @param context
     * @param component
     * @param value
     */
    public void validaTypeText(FacesContext context, UIComponent component, Object value) {
        Part arquivo = (Part) value;

        if (arquivo != null && arquivo.getSize() > MAX_SIZE) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Arquivo muito grande", "O arquivo deve ter o tamanho máximo de 2mb.");
            throw new ValidatorException(msg);
        }

        if (!"text/plain".equals(arquivo.getContentType())) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Tipo de arquivo inválido", "O arquivo deve ser do tipo texto.");
            throw new ValidatorException(msg);
        }

    }
}
