package br.com.juridico.autocomplete;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import com.google.common.collect.Maps;

import br.com.juridico.entidades.Cidade;
import br.com.juridico.impl.CidadeSessionBean;

/**
 * 
 * @author Jefferson
 *
 */
@ManagedBean
@ViewScoped
public class CidadeAutoCompleteController implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Inject
    private CidadeSessionBean cidadeSessionBean;

    /**
     * 
     * @param query
     * @return
     */
    public List<Cidade> completeCidade(String query) {
        if (query == null || query.isEmpty()) {
            return cidadeSessionBean.retrieveAll();
        }
        Map<String, Object> parameters = Maps.newHashMap();
        parameters.put("descricao", query);

        return cidadeSessionBean.retrieveAll(parameters, false, null);
    }

}
