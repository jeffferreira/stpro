package br.com.juridico.autocomplete;

import br.com.juridico.controller.AbstractController;
import br.com.juridico.entidades.Processo;
import br.com.juridico.impl.ProcessoSessionBean;
import com.google.common.base.Strings;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.inject.Inject;
import java.util.List;

/**
 * Created by jeffersonl2009_00 on 17/11/2016.
 */
@ManagedBean
@RequestScoped
public class ProcessoAutoCompleteController extends AbstractController {

    @Inject
    private ProcessoSessionBean processoSessionBean;

    public List<Processo> completeProcesso(String query) {
        Long idProcesso = (Long) UIComponent.getCurrentComponent(getFacesContext()).getAttributes().get("idProcesso");

        if (!Strings.isNullOrEmpty(query)) {
            return processoSessionBean.findListByDescricaoProcesso(query, idProcesso, getStateManager().getEmpresaSelecionada().getId());
        }
        return processoSessionBean.retrieveAll(getStateManager().getEmpresaSelecionada().getId(), idProcesso);
    }
}
