package br.com.juridico.autocomplete;

import br.com.juridico.controller.AbstractController;
import br.com.juridico.entidades.Comarca;
import br.com.juridico.impl.ComarcaSessionBean;
import com.google.common.collect.Maps;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author Jefferson
 *
 */
@ManagedBean
@ViewScoped
public class ComarcaAutoCompleteController extends AbstractController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Inject
    private ComarcaSessionBean comarcaSessionBean;

    /**
     * 
     * @param query
     * @return
     */
    public List<Comarca> completeComarca(String query) {
        if (query == null || query.isEmpty()) {
            return comarcaSessionBean.retrieveAll(getStateManager().getEmpresaSelecionada().getId());
        }
        Map<String, Object> parameters = Maps.newHashMap();
        parameters.put("descricao", query);

        return comarcaSessionBean.retrieveAll(parameters, false, getStateManager().getEmpresaSelecionada().getId());
    }

}
