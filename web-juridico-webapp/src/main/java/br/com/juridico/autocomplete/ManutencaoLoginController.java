package br.com.juridico.autocomplete;

import br.com.juridico.controller.AbstractController;
import br.com.juridico.entidades.Manutencao;
import br.com.juridico.impl.ManutencaoLoginSessionBean;
import br.com.juridico.util.DecryptUtil;
import br.com.juridico.util.EncryptUtil;
import com.google.common.base.Strings;
import org.apache.log4j.Logger;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;

/**
 * Created by jeffersonl2009_00 on 26/10/2016.
 */
@RequestScoped
@ManagedBean
public class ManutencaoLoginController extends AbstractController {

    private static final Logger LOGGER = Logger.getLogger(ManutencaoLoginController.class);

    private static final String PAGINA_MANUTENCAO = "/manutencao/manutencao-template.xhtml?faces-redirect=true";

    @Inject
    private ManutencaoLoginSessionBean manutencaoLoginSessionBean;

    private String login;
    private String senha;

    public String goToManutencao(){
        try {
            if(Strings.isNullOrEmpty(login) || Strings.isNullOrEmpty(senha)) {
                exibirMensagem(FacesMessage.SEVERITY_ERROR, "Login e senha: campos de preenchimento obrigatório.");
                return "";
            }

            Manutencao manutencao = manutencaoLoginSessionBean.findByLogin(this.login);
            if(manutencao == null) {
                exibirMensagem(FacesMessage.SEVERITY_ERROR, "Login não encontrado.");
                return "";
            }

            String senha = DecryptUtil.execute(manutencao.getSenha());
            if(!senha.equals(this.senha)){
                exibirMensagem(FacesMessage.SEVERITY_ERROR, "Senha inválida.");
                return "";
            }

            boolean permiteAcesso = Boolean.TRUE;
            setSessionAttribute("permiteAcessoManutencao", permiteAcesso);

        } catch (Exception e) {
            LOGGER.error(e, e.getCause());
        }
        return PAGINA_MANUTENCAO;
    }

    private void criarNovaSenha() throws Exception {
        String primeiraSenha = "senha_aqui";
        byte[] bytes = EncryptUtil.execute(primeiraSenha);
        Manutencao m = new Manutencao("login_aqui", bytes);
        manutencaoLoginSessionBean.create(m);
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}
