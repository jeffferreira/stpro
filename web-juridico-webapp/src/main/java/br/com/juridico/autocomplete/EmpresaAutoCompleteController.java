package br.com.juridico.autocomplete;

import br.com.juridico.controller.AbstractController;
import br.com.juridico.entidades.Empresa;
import br.com.juridico.impl.EmpresaSessionBean;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import java.util.List;

/**
 * Created by jefferson.ferreira on 30/05/2017.
 */
@ManagedBean
@RequestScoped
public class EmpresaAutoCompleteController extends AbstractController {

    @Inject
    private EmpresaSessionBean empresaSessionBean;

    public List<Empresa> completeEmpresa(String query) {
        if(getUser() == null){
            return Lists.newArrayList();
        }
        empresaSessionBean.clearSession();
        if (Strings.isNullOrEmpty(query)) {
            return empresaSessionBean.findMatrizAndAllFiliais("", getUser().getCodEmpresa());
        }
        return empresaSessionBean.findMatrizAndAllFiliais(query, getUser().getCodEmpresa());
    }
}
