package br.com.juridico.autocomplete;

import br.com.juridico.controller.AbstractController;
import br.com.juridico.entidades.PessoaJuridica;
import br.com.juridico.impl.PessoaJuridicaSessionBean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import java.util.List;

@ManagedBean
@RequestScoped
public class PessoaJuridicaAutoCompleteController extends AbstractController {

	/**sim
	 * 
	 */
	private static final long serialVersionUID = -5852598592423732129L;
	
	@Inject
	private PessoaJuridicaSessionBean pessoaJuridicaSessionBean;


    public List<PessoaJuridica> completePessoaJuridica(String query) {
        pessoaJuridicaSessionBean.clearSession();
        if (query == null || query.isEmpty()) {
            return pessoaJuridicaSessionBean.findAllPessoasJuridicas(null, getUser().getCodEmpresa());
        }
        return pessoaJuridicaSessionBean.findAllPessoasJuridicas(query, getUser().getCodEmpresa());
    }

}
