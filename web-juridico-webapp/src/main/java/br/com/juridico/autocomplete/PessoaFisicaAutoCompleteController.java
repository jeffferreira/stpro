package br.com.juridico.autocomplete;

import br.com.juridico.controller.AbstractController;
import br.com.juridico.entidades.PessoaFisica;
import br.com.juridico.impl.PessoaFisicaSessionBean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import java.util.List;

@ManagedBean
@RequestScoped
public class PessoaFisicaAutoCompleteController extends AbstractController {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6850159018305116406L;

	@Inject
	private PessoaFisicaSessionBean pessoaFisicaSessionBean;

	public List<PessoaFisica> completePessoaFisica(String query) {
		pessoaFisicaSessionBean.clearSession();
		if (query == null || query.isEmpty()) {
			return pessoaFisicaSessionBean.findAllPessoasFisicasSemAdvogados(null, getUser().getCodEmpresa());
		}
		return pessoaFisicaSessionBean.findAllPessoasFisicasSemAdvogados(query, getUser().getCodEmpresa());
	}

	public List<PessoaFisica> completeAllPessoaFisicas(String query) {
		pessoaFisicaSessionBean.clearSession();
		if (query == null || query.isEmpty()) {
			return pessoaFisicaSessionBean.findAllPessoasFisicasComAdvogados(null, getUser().getCodEmpresa());
		}
		return pessoaFisicaSessionBean.findAllPessoasFisicasComAdvogados(query, getUser().getCodEmpresa());
	}

	public List<PessoaFisica> completePessoaInternas(String query) {
		pessoaFisicaSessionBean.clearSession();
		if (query == null || query.isEmpty()) {
			return pessoaFisicaSessionBean.findByAdvogadosAndColaboradores(null, getUser().getCodEmpresa());
		}
		return pessoaFisicaSessionBean.findByAdvogadosAndColaboradores(query, getUser().getCodEmpresa());
	}

	public List<PessoaFisica> completePessoaAdvogados(String query) {
		pessoaFisicaSessionBean.clearSession();
		if (query == null || query.isEmpty()) {
			return pessoaFisicaSessionBean.getAdvogados(true, getUser().getCodEmpresa());
		}
		return pessoaFisicaSessionBean.getAdvogados(query, true, getUser().getCodEmpresa());
	}

}
