package br.com.juridico.autocomplete;

import br.com.juridico.controller.AbstractController;
import br.com.juridico.entidades.Pessoa;
import br.com.juridico.entidades.PessoaFisica;
import br.com.juridico.entidades.SubAndamento;
import br.com.juridico.impl.PessoaFisicaSessionBean;
import br.com.juridico.impl.SubAndamentoSessionBean;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.Map;

@ManagedBean
@RequestScoped
public class SubAndamentoAutoCompleteController extends AbstractController {

	/**
	 *
	 */
	private static final long serialVersionUID = 6850159018305116406L;

	@Inject
	private SubAndamentoSessionBean subAndamentoSessionBean;

	public List<SubAndamento> completeSubAndamento(String query) {
		if (query == null || query.isEmpty()) {
			return subAndamentoSessionBean.retrieveAll(getStateManager().getEmpresaSelecionada().getId());
		}
		return subAndamentoSessionBean.findListByDescricao(query, getStateManager().getEmpresaSelecionada().getId());
	}

}
