package br.com.juridico.autocomplete;

import br.com.juridico.controller.AbstractController;
import br.com.juridico.entidades.RamoAtividade;
import br.com.juridico.impl.RamoAtividadeSessionBean;
import com.google.common.collect.Maps;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Jefferson
 *
 */
@ManagedBean
@ViewScoped
public class RamoAtividadeAutoCompleteController extends AbstractController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Inject
    private RamoAtividadeSessionBean ramoAtividadeSessionBean;

    /**
     *
     * @param query
     * @return
     */
    public List<RamoAtividade> completeRamoAtividade(String query) {
        if (query == null || query.isEmpty()) {
            return ramoAtividadeSessionBean.retrieveAll(getStateManager().getEmpresaSelecionada().getId());
        }
        Map<String, Object> parameters = Maps.newHashMap();
        parameters.put("descricao", query);

        return ramoAtividadeSessionBean.retrieveAll(parameters, false, getStateManager().getEmpresaSelecionada().getId());
    }

}
