package br.com.juridico.controller;

import br.com.juridico.entidades.EventoRejeito;
import br.com.juridico.entidades.Processo;
import br.com.juridico.impl.EventoRejeitoSessionBean;
import br.com.juridico.impl.ProcessoSessionBean;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.SessionScoped;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.util.List;

@Named
@SessionScoped
public class RejeitoControladoriaController extends AbstractController {

    @EJB
    private EventoRejeitoSessionBean eventoRejeitoSessionBean;
    @EJB
    private ProcessoSessionBean processoSessionBean;

    private List<EventoRejeito> rejeitos;

    @PostConstruct
    public void initialize(){
        this.eventoRejeitoSessionBean.clearSession();
        this.rejeitos = eventoRejeitoSessionBean.findRejeitosByUsuario(getUser().getPessoa());
    }

    public String editarRejeito(EventoRejeito rejeitoSelecionado){
        if(rejeitoSelecionado.getEvento() != null && rejeitoSelecionado.getEvento().getProcesso() != null) {
            DetalheProcessoController detalheProcessoController = getDetalheProcessoController();
            Processo processo = rejeitoSelecionado.getEvento().getProcesso();
            detalheProcessoController.setProcessoSelecionado(processoSessionBean.findByProcessoAndDataCadastro(processo.getDescricao(),processo.getDataCadastro()));
            return detalheProcessoController.goToDetalhes();
        }
        return "";
    }

    public DetalheProcessoController getDetalheProcessoController() {
        return getFacesContext().getApplication().evaluateExpressionGet(
                getFacesContext(), "#{detalheProcessoController}", DetalheProcessoController.class);
    }

    public List<EventoRejeito> getRejeitos() {
        return rejeitos;
    }
}