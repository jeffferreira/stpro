package br.com.juridico.controller;

import br.com.juridico.datamodel.LazyEntityDataModel;
import br.com.juridico.entidades.Direito;
import br.com.juridico.entidades.Posicao;
import br.com.juridico.exception.CrudException;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.impl.DireitoSessionBean;
import br.com.juridico.impl.PerfilAcessoSessionBean;
import br.com.juridico.impl.PosicaoSessionBean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Jefferson
 *
 */
@ManagedBean
@SessionScoped
public class PosicaoController extends CrudController<Posicao> {

	/**
	 *
	 */
	private static final long serialVersionUID = -5363621670335559115L;
	private static final String POSICAO_FORM = "posicaoForm";
	private static final String CHAVE_ACESSO = "POSICAO_POSICAO_TEMPLATE";
	private static final String PAGINA_FORMULARIO = "/pages/cadastros/posicao/posicao-template.xhtml?faces-redirect=true";

	@Inject
	private PosicaoSessionBean posicaoSessionBean;
	@Inject
	private PerfilAcessoSessionBean perfilAcessoSessionBean;
	@Inject
	private DireitoSessionBean direitoSessionBean;

	public void init(){
		carregaLazyDataModel();
	}

	@Override
	protected void validar() throws ValidationException {
		posicaoSessionBean.clearSession();
		boolean jaExistePosicao = posicaoSessionBean.isDescricaoJaCadastrada(getObjetoSelecionado(), getStateManager().getEmpresaSelecionada().getId());
		getObjetoSelecionado().validar(jaExistePosicao);
	}

	@Override
	protected void carregaLazyDataModel() {
		setLazyDataModel(new LazyEntityDataModel<Posicao>() {
			@Override
			protected List<Posicao> consultaFiltrada(ConsultaLazyFilter filtro) {
				filtro.setDescricao(getDescricaoFiltro());
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return posicaoSessionBean.filtrados(filtro);
			}

			@Override
			protected int countTotalRegistros(ConsultaLazyFilter filtro) {
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return posicaoSessionBean.quantidadeFiltrados(filtro);
			}
		});
	}

	@Override
	protected void create(Posicao posicao) throws ValidationException, CrudException {
		Posicao posicaoExclusao = getRegistroComExclusaoLogica();

		if (isEditavel() && posicaoExclusao != null) {
			posicaoExclusao.setDataExclusao(null);
			posicaoSessionBean.update(posicaoExclusao);
		} else if (posicao != null && posicao.getDescricao() != null && !posicao.getDescricao().isEmpty()) {
			posicaoSessionBean.create(posicao);
		} else {
			throw new CrudException();
		}

	}

	private Posicao getRegistroComExclusaoLogica() {
		return posicaoSessionBean.findByDescricao(getObjetoSelecionado().getDescricao(), getStateManager().getEmpresaSelecionada().getId());
	}

	@Override
	protected void antesDeExcluir() throws CrudException {
		getObjetoSelecionado().setDataExclusao(new Date());
	}

	@Override
	protected void aposExcluir() throws CrudException {
		super.buscarPorLazy();
	}

	@Override
	protected void aposCancelar() throws CrudException {
		getObjetoSelecionado().setDescricao(null);
		updateContext(POSICAO_FORM);
		super.aposCancelar();
	}

	/**
	 *
	 * @return
	 */
	public String goToFormulario() {
		return PAGINA_FORMULARIO;
	}

	@Override
	protected Posicao inicializaObjetosLazy(Posicao objeto) {
		posicaoSessionBean.clearSession();
		return posicaoSessionBean.retrieve(objeto.getId());
	}

	@Override
	protected Posicao instanciarObjetoNovo() {
		return new Posicao();
	}

	@Override
	protected void setEmpresaDeUsuarioLogado(Posicao objetoSelecionado) {
		objetoSelecionado.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
	}

	@Override
	protected void addChaveAcesso() {
		setChaveAcesso(CHAVE_ACESSO);
	}

	@Override
	protected boolean isPodeAlterar(){
		Direito direito = direitoSessionBean.retrieve(ALTERAR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

	@Override
	protected boolean isPodeExcluir(){
		Direito direito = direitoSessionBean.retrieve(EXCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

	@Override
	protected boolean isPodeIncluir(){
		Direito direito = direitoSessionBean.retrieve(INCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

}
