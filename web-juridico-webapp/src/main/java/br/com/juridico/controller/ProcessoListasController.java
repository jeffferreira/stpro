package br.com.juridico.controller;

import br.com.juridico.entidades.*;
import br.com.juridico.impl.*;

import javax.faces.bean.SessionScoped;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * Created by Jefferson on 02/07/2016.
 */
@Named
@SessionScoped
public class ProcessoListasController extends AbstractController {

    @Inject
    private PosicaoSessionBean posicaoSessionBean;
    @Inject
    private GrupoSessionBean grupoSessionBean;
    @Inject
    private MeioTramitacaoSessionBean meioTramitacaoSessionBean;
    @Inject
    private FaseSessionBean faseSessionBean;
    @Inject
    private ClasseProcessualSessionBean classeProcessualSessionBean;
    @Inject
    private NaturezaAcaoSessionBean naturezaAcaoSessionBean;
    @Inject
    private ForumSessionBean forumSessionBean;
    @Inject
    private VaraSessionBean varaSessionBean;
    @Inject
    private GrauRiscoSessionBean grauRiscoSessionBean;
    @Inject
    private CausaPedidoSessionBean causaPedidoSessionBean;
    @Inject
    private PedidoSessionBean pedidoSessionBean;
    @Inject
    private TipoAndamentoSessionBean tipoAndamentoSessionBean;

    public List<Posicao> getPosicoes() {
        return posicaoSessionBean.retrieveAll(getStateManager().getEmpresaSelecionada().getId());
    }

    public List<Grupo> getGrupos() {
        return grupoSessionBean.retrieveAll(getStateManager().getEmpresaSelecionada().getId());
    }

    public List<MeioTramitacao> getMeiosTramitacao() {
        return meioTramitacaoSessionBean.retrieveAll(getStateManager().getEmpresaSelecionada().getId());
    }

    public List<Fase> getFases() {
        return faseSessionBean.retrieveAll(getStateManager().getEmpresaSelecionada().getId());
    }

    public List<NaturezaAcao> getNaturezas() {
        return naturezaAcaoSessionBean.retrieveAll(getStateManager().getEmpresaSelecionada().getId());
    }

    public List<ClasseProcessual> getClassesProcessuais(){
        return classeProcessualSessionBean.retrieveAll(getStateManager().getEmpresaSelecionada().getId());
    }

    public List<Forum> getForuns(){
        return forumSessionBean.retrieveAll(getStateManager().getEmpresaSelecionada().getId());
    }

    public List<Vara> getVaras(){
        return varaSessionBean.retrieveAll(getStateManager().getEmpresaSelecionada().getId());
    }

    public List<GrauRisco> getGrausRisco(){
        return grauRiscoSessionBean.retrieveAll(getStateManager().getEmpresaSelecionada().getId());
    }

    public List<CausaPedido> getCausasPedido(){
        return causaPedidoSessionBean.retrieveAll(getStateManager().getEmpresaSelecionada().getId());
    }

    public List<Pedido> getPedidos(){
        return pedidoSessionBean.retrieveAll(getStateManager().getEmpresaSelecionada().getId());
    }

    public List<TipoAndamento> getTipoAndamentos() {
        this.tipoAndamentoSessionBean.clearSession();
        return tipoAndamentoSessionBean.retrieveAll(getStateManager().getEmpresaSelecionada().getId());
    }
}
