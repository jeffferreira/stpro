package br.com.juridico.controller;

import br.com.juridico.datamodel.LazyEntityDataModel;
import br.com.juridico.entidades.Direito;
import br.com.juridico.entidades.NaturezaAcao;
import br.com.juridico.exception.CrudException;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.impl.DireitoSessionBean;
import br.com.juridico.impl.NaturezaAcaoSessionBean;
import br.com.juridico.impl.PerfilAcessoSessionBean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Jefferson
 *
 */
@ManagedBean
@SessionScoped
public class NaturezaAcaoController extends CrudController<NaturezaAcao> {

	/**
	 *
	 */
	private static final long serialVersionUID = 6593636120714622459L;
	private static final String NATUREZA_ACAO_FORM = "naturezaAcaoForm";
	private static final String CHAVE_ACESSO = "NATUREZAACAO_NATUREZA_ACAO_TEMPLATE";
	private static final String PAGINA_FORMULARIO = "/pages/cadastros/naturezaAcao/natureza-acao-template.xhtml?faces-redirect=true";

	@Inject
	private NaturezaAcaoSessionBean naturezaAcaoSessionBean;
	@Inject
	private PerfilAcessoSessionBean perfilAcessoSessionBean;
	@Inject
	private DireitoSessionBean direitoSessionBean;

	public void init(){
		carregaLazyDataModel();
	}

	@Override
	protected void validar() throws ValidationException {
		naturezaAcaoSessionBean.clearSession();
		boolean jaExisteNaturezaAcao = naturezaAcaoSessionBean.isDescricaoJaCadastrada(getObjetoSelecionado(), getStateManager().getEmpresaSelecionada().getId());
		getObjetoSelecionado().validar(jaExisteNaturezaAcao);
	}

	@Override
	protected void create(NaturezaAcao naturezaAcao) throws ValidationException, CrudException {
		NaturezaAcao naturezaAcaoExclusao = getRegistroComExclusaoLogica();

		if (isEditavel() && naturezaAcaoExclusao != null) {
			naturezaAcaoExclusao.setDataExclusao(null);
			naturezaAcaoSessionBean.update(naturezaAcaoExclusao);
		} else if (naturezaAcao != null && naturezaAcao.getDescricao() != null && !naturezaAcao.getDescricao().isEmpty()) {
			naturezaAcaoSessionBean.create(naturezaAcao);
		} else {
			throw new CrudException();
		}

	}

	@Override
	protected void carregaLazyDataModel() {
		setLazyDataModel(new LazyEntityDataModel<NaturezaAcao>() {
			@Override
			protected List<NaturezaAcao> consultaFiltrada(ConsultaLazyFilter filtro) {
				filtro.setDescricao(getDescricaoFiltro());
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return naturezaAcaoSessionBean.filtrados(filtro);
			}

			@Override
			protected int countTotalRegistros(ConsultaLazyFilter filtro) {
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return naturezaAcaoSessionBean.quantidadeFiltrados(filtro);
			}
		});
	}

	@Override
	protected void antesDeExcluir() throws CrudException {
		getObjetoSelecionado().setDataExclusao(new Date());
	}

	@Override
	protected void aposExcluir() throws CrudException {
		super.buscarPorLazy();
	}

	@Override
	protected void aposCancelar() throws CrudException {
		getObjetoSelecionado().setDescricao(null);
		updateContext(NATUREZA_ACAO_FORM);
		super.aposCancelar();
	}

	@Override
	protected NaturezaAcao inicializaObjetosLazy(NaturezaAcao objeto) {
		naturezaAcaoSessionBean.clearSession();
		return naturezaAcaoSessionBean.retrieve(objeto.getId());
	}

	@Override
	protected NaturezaAcao instanciarObjetoNovo() {
		return new NaturezaAcao();
	}

	@Override
	protected void setEmpresaDeUsuarioLogado(NaturezaAcao objetoSelecionado) {
		objetoSelecionado.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
	}

	private NaturezaAcao getRegistroComExclusaoLogica() {
		return naturezaAcaoSessionBean.findByDescricao(getObjetoSelecionado().getDescricao(), getStateManager().getEmpresaSelecionada().getId());
	}

	/**
	 *
	 * @return
	 */
	public String goToFormulario() {
		return PAGINA_FORMULARIO;
	}

	@Override
	protected void addChaveAcesso() {
		setChaveAcesso(CHAVE_ACESSO);
	}

	@Override
	protected boolean isPodeAlterar(){
		Direito direito = direitoSessionBean.retrieve(ALTERAR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

	@Override
	protected boolean isPodeExcluir(){
		Direito direito = direitoSessionBean.retrieve(EXCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

	@Override
	protected boolean isPodeIncluir(){
		Direito direito = direitoSessionBean.retrieve(INCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}
}
