package br.com.juridico.controller;

import br.com.juridico.dao.IEntity;
import br.com.juridico.enums.CrudStatus;
import br.com.juridico.exception.CrudException;
import br.com.juridico.exception.ValidationException;
import org.apache.log4j.Logger;
import org.primefaces.model.LazyDataModel;

import javax.faces.application.FacesMessage;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jefferson Ferreira
 *
 * @param <T>
 */
public abstract class CrudController<T extends IEntity> extends AbstractController {

	/**
	 *
	 */
	private static final long serialVersionUID = -5595574461745839844L;

	private static final Logger LOGGER = Logger.getLogger(CrudController.class);

	protected static final String LIST_FLASH = "LIST_FLASH";

	private boolean editavel;
	protected CrudStatus crudStatus;
	private String descricaoFiltro;
	private String chaveAcesso;
	private T objetoSelecionado;
	private List<T> lista;
	private LazyDataModel<T> lazyDataModel;

	protected boolean permiteVisualizacaoEmail;
	protected boolean permiteInclusaoEmail;
	protected boolean permiteExclusaoEmail;
	protected boolean permiteVisualizacaoContato;
	protected boolean permiteInclusaoContato;
	protected boolean permiteExclusaoContato;
	protected boolean permiteVisualizacaoEndereco;
	protected boolean permiteInclusaoEndereco;
	protected boolean permiteExclusaoEndereco;
	protected boolean permiteVisualizacaoResponsavel;
	protected boolean permiteInclusaoResponsavel;
	protected boolean permiteExclusaoResponsavel;
	protected boolean permiteVisualizacaoCentroCusto;
	protected boolean permiteInclusaoCentroCusto;
	protected boolean permiteExclusaoCentroCusto;


	/**
	 *
	 */
	public CrudController() {
		try {
			selecionar(null);
		} catch (CrudException e) {
			LOGGER.error(e.getLocalizedMessage(), e);
		}
	}

	/**
	 *
	 * @param objeto
	 */
	public void editar(T objeto) {
		try {
			if (selecionar(objeto)) {
				editar();
			}
		} catch (CrudException e) {
			LOGGER.info(e.getCause(), e);
			exibirMensagem(e.getMensagemListDetalhe(), e.getMessage());
		}
	}

	/**
	 *
	 */
	public void limparObjetoSelecionado() {
		try {
			this.objetoSelecionado = instanciarObjetoNovo();
			aposLimparObjetoSelecionado();
		} catch (CrudException e) {
			LOGGER.error(e);
		}
	}

	/**
	 *
	 */
	public void editar() {
		try {
			validarEdicao();
			antesDeEditar(objetoSelecionado);
			crudStatus = CrudStatus.UPDATE;
			aposEditar();
		} catch (CrudException e) {
			LOGGER.error(e);
			exibirMensagem(e.getMensagemListDetalhe(), getMessage("msg_erro_editar"));
		}
	}

	private void validarEdicao() throws CrudException {
		if (objetoSelecionado == null) {
			throw new CrudException(getMessage("msg_selecione_registro_para_editar"));
		}
	}

	/**
	 *
	 */
	public void salvar() {
		try {
			antesDeSalvar();
			setEmpresaDeUsuarioLogado(objetoSelecionado);
			validar();
			salvarAposValidacao();
			exibirMensagem(FacesMessage.SEVERITY_INFO, getMessage("msg_sucesso_salvar"));
		} catch (CrudException e) {
			LOGGER.info(e.getCause(), e);
			exibirMensagem(e.getMensagemListDetalhe(), getMessage("msg_erro_salvar"));
		} catch (ValidationException e) {
			for (String message : e.getMessages()) {
				exibirMensagem(FacesMessage.SEVERITY_ERROR, message);
			}
		} finally {
			aposValidacao();
		}
	}

	/**
	 *
	 * @throws CrudException
	 * @throws ValidationException
	 */
	public void salvarAposValidacao() throws CrudException, ValidationException {
		create(objetoSelecionado);
		addicionarObjetoNaLista(objetoSelecionado);
		crudStatus = null;
		selecionarObjetoAposSalvar();
		aposSalvar();
	}

	/**
	 *
	 * @param objeto
	 */
	public void excluir(T objeto) {
		try {
			if (selecionar(objeto)) {
				excluir();
			}
		} catch (CrudException e) {
			LOGGER.info(e.getCause(), e);
			exibirMensagem(e.getMensagemListDetalhe(), e.getMessage());
		} catch (ValidationException e) {
			LOGGER.error(e.getLocalizedMessage(), e);
		}
	}

	/**
	 *
	 * @throws ValidationException
	 */
	public void excluir() throws ValidationException {
		try {
			antesDeExcluir();
			salvarAposValidacao();
			aposExcluir();
			exibirMensagem(FacesMessage.SEVERITY_INFO, getMessage("msg_sucesso_excluir"));
		} catch (CrudException e) {
			LOGGER.info(e.getCause(), e);
			exibirMensagem(e.getMensagemListDetalhe(), getMessage("msg_erro_excluir"));
		}
	}

	/**
	 *
	 */
	public void cancelar() {
		if (objetoSelecionado.getId() != null)
			objetoSelecionado = inicializaObjetosLazy(objetoSelecionado);

		limparObjetoSelecionado();
		crudStatus = null;
		try {
			selecionar(null);
			buscarPorLazy();
			aposCancelar();
		} catch (CrudException e) {
			LOGGER.info(e.getCause(), e);
			exibirMensagem(e.getMensagemListDetalhe(), getMessage("msg_erro_cancelar"));
		}
	}

	/**
	 *
	 * @param objeto
	 * @return
	 * @throws CrudException
	 */
	public boolean selecionar(T objeto) throws CrudException {
		if (CrudStatus.CREATE.equals(crudStatus) || validarEdicao(objeto)) {
			antesDeSelecionar(objeto);

			if (objeto == null) {
				objeto = instanciarObjetoNovo();
				if (objeto instanceof IEntity == false) {
					LOGGER.error(objeto.getClass() + " deve extender IEntity");
				}
			}

			if (objeto.getId() != null) {
				objeto = inicializaObjetosLazy(objeto);
			}
			objetoSelecionado = objeto;
			aposSelecionar();
			return true;
		}

		return false;
	}

	/**
	 *
	 * @param objeto
	 */
	public void visualizar(T objeto) {
		try {
			selecionar(objeto);
			crudStatus = CrudStatus.READ;
		} catch (CrudException e) {
			LOGGER.info(e.getCause(), e);
			exibirMensagem(e.getMensagemListDetalhe(), e.getMessage());
		}

	}

	/**
	 *
	 * @param novoRegistro
	 */
	public void novo(T novoRegistro) {
		insereNovoRegistro(novoRegistro);
	}

	/**
	 *
	 */
	public void novo() {
		T novoRegistro = instanciarObjetoNovo();
		insereNovoRegistro(novoRegistro);
	}

	/**
	 *
	 * @param novoRegistro
	 */
	public void insereNovoRegistro(T novoRegistro) {
		try {
			crudStatus = CrudStatus.CREATE;
			selecionar(novoRegistro);
			antesInserir();
			depoisInserir();
		} catch (CrudException e) {
			LOGGER.info(e.getCause(), e);
			exibirMensagem(e.getMensagemListDetalhe(), getMessage("msg_erro_inserir"));
		}
	}

	/**
	 *
	 * @return
	 */
	public boolean isEditavel() {
		this.editavel = Boolean.FALSE;

		if (CrudStatus.CREATE.equals(crudStatus)) {
			this.editavel = Boolean.TRUE;
		}

		if (CrudStatus.UPDATE.equals(crudStatus)) {
			this.editavel = Boolean.TRUE;
		}

		if (CrudStatus.DELETE.equals(crudStatus)) {
			this.editavel = Boolean.TRUE;
		}

		return this.editavel;
	}

	/**
	 *
	 * @param objeto
	 * @return
	 */
	public boolean validarEdicao(T objeto) {
		if (!permiteEditar(objeto)) {
			exibirMensagem(FacesMessage.SEVERITY_WARN, getMessage("msg_cancele_salve_registro_em_edicao"));
			return false;
		} else {
			return true;
		}
	}

	/**
	 *
	 * @param objeto
	 * @return
	 */
	public boolean permiteEditar(T objeto) {
		return CrudStatus.UPDATE.equals(crudStatus) || !CrudStatus.CREATE.equals(crudStatus);
	}

	/**
	 *
	 * @throws CrudException
	 */
	public void selecionarObjetoAposSalvar() throws CrudException {
		selecionar(null);
	}

	/**
	 *
	 * @param objeto
	 */
	public void addicionarObjetoNaLista(T objeto) {
		if (lista == null) {
			lista = new ArrayList<>();
		}

		if (!lista.contains(objeto)) {
			lista.add(objeto);
		}
	}

	/**
	 *
	 * @param novoObjeto
	 */
	public void clearObjetoSelecionado(T novoObjeto) {
		this.objetoSelecionado = novoObjeto;
	}

	public T getObjetoSelecionado() {
		return objetoSelecionado;
	}

	public void setObjetoSelecionado(T objetoSelecionado) {
		this.objetoSelecionado = objetoSelecionado;
	}

	/**
	 *
	 */
	public void buscarPorLazy() {
		carregaLazyDataModel();
	}

	public List<T> getLista() {
		if (lista == null) {
			return new ArrayList<>();
		}
		return lista;
	}

	public boolean isHasAccess(){
		addChaveAcesso();
		return possuiAcesso(this.chaveAcesso);
	}

	public void setLista(List<T> lista) {
		this.lista = lista;
	}

	public void setEditavel(boolean editavel) {
		this.editavel = editavel;
	}

	protected void antesDeSelecionar(T objeto) throws CrudException {
	}

	protected void aposSelecionar() throws CrudException {
	}

	protected void depoisInserir() throws CrudException {
	}

	protected void antesInserir() throws CrudException {
	}

	protected void antesDeSalvar() throws CrudException {
	}

	protected void aposSalvar() throws CrudException {
	}

	protected void aposExcluir() throws CrudException {
	}

	protected void antesDeExcluir() throws CrudException {
	}

	protected void antesDeBuscar() throws CrudException {
	}

	protected void aposBuscar() throws CrudException {
	}

	protected void antesDeEditar(T objeto) throws CrudException {
	}

	protected void aposLimparObjetoSelecionado() throws CrudException {
	}

	protected void validar() throws ValidationException {
	}

	protected void aposValidacao() {
	}

	protected void aposEditar() throws CrudException {
		buscarPorLazy();
	}

	protected void aposCancelar() throws CrudException {
	}

	protected abstract void create(T objetoSelecionado) throws ValidationException, CrudException;

	protected abstract void carregaLazyDataModel();

	protected abstract T inicializaObjetosLazy(T objetoSelecionado);

	protected abstract T instanciarObjetoNovo();

	protected abstract void setEmpresaDeUsuarioLogado(T objetoSelecionado);

	protected abstract void addChaveAcesso();

	protected abstract boolean isPodeAlterar();

	protected abstract boolean isPodeExcluir();

	protected abstract boolean isPodeIncluir();

	public String getDescricaoFiltro() {
		return descricaoFiltro;
	}

	public void setDescricaoFiltro(String descricaoFiltro) {
		this.descricaoFiltro = descricaoFiltro;
	}

	public LazyDataModel<T> getLazyDataModel() {
		return lazyDataModel;
	}

	public void setLazyDataModel(LazyDataModel<T> lazyDataModel) {
		this.lazyDataModel = lazyDataModel;
	}


	public void setChaveAcesso(String chaveAcesso) {
		this.chaveAcesso = chaveAcesso;
	}

	public boolean isPermiteAlteracao() {
		return isPodeAlterar();
	}


	public boolean isPermiteExclusao() {
		return isPodeExcluir();
	}

	public boolean isPermiteInclusao() {
		return isPodeIncluir();
	}


	public boolean isPermiteVisualizacaoEmail() {
		return permiteVisualizacaoEmail;
	}

	public void setPermiteVisualizacaoEmail(boolean permiteVisualizacaoEmail) {
		this.permiteVisualizacaoEmail = permiteVisualizacaoEmail;
	}

	public boolean isPermiteInclusaoEmail() {
		return permiteInclusaoEmail;
	}

	public void setPermiteInclusaoEmail(boolean permiteInclusaoEmail) {
		this.permiteInclusaoEmail = permiteInclusaoEmail;
	}

	public boolean isPermiteExclusaoEmail() {
		return permiteExclusaoEmail;
	}

	public void setPermiteExclusaoEmail(boolean permiteExclusaoEmail) {
		this.permiteExclusaoEmail = permiteExclusaoEmail;
	}

	public boolean isPermiteVisualizacaoContato() {
		return permiteVisualizacaoContato;
	}

	public void setPermiteVisualizacaoContato(boolean permiteVisualizacaoContato) {
		this.permiteVisualizacaoContato = permiteVisualizacaoContato;
	}

	public boolean isPermiteInclusaoContato() {
		return permiteInclusaoContato;
	}

	public void setPermiteInclusaoContato(boolean permiteInclusaoContato) {
		this.permiteInclusaoContato = permiteInclusaoContato;
	}

	public boolean isPermiteExclusaoContato() {
		return permiteExclusaoContato;
	}

	public void setPermiteExclusaoContato(boolean permiteExclusaoContato) {
		this.permiteExclusaoContato = permiteExclusaoContato;
	}

	public boolean isPermiteVisualizacaoEndereco() {
		return permiteVisualizacaoEndereco;
	}

	public void setPermiteVisualizacaoEndereco(boolean permiteVisualizacaoEndereco) {
		this.permiteVisualizacaoEndereco = permiteVisualizacaoEndereco;
	}

	public boolean isPermiteInclusaoEndereco() {
		return permiteInclusaoEndereco;
	}

	public void setPermiteInclusaoEndereco(boolean permiteInclusaoEndereco) {
		this.permiteInclusaoEndereco = permiteInclusaoEndereco;
	}

	public boolean isPermiteExclusaoEndereco() {
		return permiteExclusaoEndereco;
	}

	public void setPermiteExclusaoEndereco(boolean permiteExclusaoEndereco) {
		this.permiteExclusaoEndereco = permiteExclusaoEndereco;
	}

	public boolean isPermiteVisualizacaoResponsavel() {
		return permiteVisualizacaoResponsavel;
	}

	public void setPermiteVisualizacaoResponsavel(
			boolean permiteVisualizacaoResponsavel) {
		this.permiteVisualizacaoResponsavel = permiteVisualizacaoResponsavel;
	}

	public boolean isPermiteInclusaoResponsavel() {
		return permiteInclusaoResponsavel;
	}

	public void setPermiteInclusaoResponsavel(boolean permiteInclusaoResponsavel) {
		this.permiteInclusaoResponsavel = permiteInclusaoResponsavel;
	}

	public boolean isPermiteExclusaoResponsavel() {
		return permiteExclusaoResponsavel;
	}

	public void setPermiteExclusaoResponsavel(boolean permiteExclusaoResponsavel) {
		this.permiteExclusaoResponsavel = permiteExclusaoResponsavel;
	}

	public boolean isPermiteVisualizacaoCentroCusto() {
		return permiteVisualizacaoCentroCusto;
	}

	public void setPermiteVisualizacaoCentroCusto(
			boolean permiteVisualizacaoCentroCusto) {
		this.permiteVisualizacaoCentroCusto = permiteVisualizacaoCentroCusto;
	}

	public boolean isPermiteInclusaoCentroCusto() {
		return permiteInclusaoCentroCusto;
	}

	public void setPermiteInclusaoCentroCusto(boolean permiteInclusaoCentroCusto) {
		this.permiteInclusaoCentroCusto = permiteInclusaoCentroCusto;
	}

	public boolean isPermiteExclusaoCentroCusto() {
		return permiteExclusaoCentroCusto;
	}

	public void setPermiteExclusaoCentroCusto(boolean permiteExclusaoCentroCusto) {
		this.permiteExclusaoCentroCusto = permiteExclusaoCentroCusto;
	}
}
