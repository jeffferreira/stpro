package br.com.juridico.controller;

import br.com.juridico.business.EmailTemplateBusiness;
import br.com.juridico.business.ParametroEmailBusiness;
import br.com.juridico.business.SenhaEmailBusiness;
import br.com.juridico.cep.ws.BuscaCep;
import br.com.juridico.cep.ws.Webservicecep;
import br.com.juridico.dao.IEntity;
import br.com.juridico.dto.CepDto;
import br.com.juridico.entidades.*;
import br.com.juridico.enums.EmailIndicador;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.impl.*;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import org.apache.log4j.Logger;

import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.xml.bind.JAXBException;
import java.net.MalformedURLException;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Jefferson on 20/09/2016.
 */
public abstract class PessoasController<T extends IEntity> extends CrudController<T> {

    private static final Logger LOGGER = Logger.getLogger(PessoasController.class);
    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final Pattern pattern = Pattern.compile(EMAIL_PATTERN, Pattern.CASE_INSENSITIVE);
    private static final String REPETIDO = "REPETIDO";

    private PessoaEmail pessoaEmail;
    private PessoaContato pessoaContato;
    private PessoaEndereco pessoaEndereco;
    private PessoaJuridicaCentroCusto pessoaJuridicaCentroCusto;
    private PessoaJuridicaResponsavel pessoaJuridicaResponsavel;

    @Inject
    private UfSessionBean ufSessionBean;
    @Inject
    private CidadeSessionBean cidadeSessionBean;
    @Inject
    private EmailSessionBean emailSessionBean;
    @Inject
    private EmailCaixaSaidaSessionBean emailCaixaSaidaSessionBean;
    @Inject
    private PerfilAcessoSessionBean perfilAcessoSessionBean;
    @Inject
    private DireitoSessionBean direitoSessionBean;
    @Inject
    private CepSessioBean cepSessioBean;


    protected void adicionaHistoricoPessaFisicaCliente(PessoaFisica pessoaFisica, String justificativa) {
        HistoricoPessoaCliente historicoPessoaCliente = new HistoricoPessoaCliente(
                pessoaFisica.getPessoa().getCliente(),
                pessoaFisica.getPessoa(), new Date(),
                justificativa, getStateManager().getEmpresaSelecionada().getId());
        pessoaFisica.getPessoa().addHistorico(historicoPessoaCliente);
    }

    protected void adicionaHistoricoPessaJuridicaCliente(PessoaJuridica pessoaJuridica, String justificativa) {
        HistoricoPessoaCliente historicoPessoaCliente = new HistoricoPessoaCliente(
                pessoaJuridica.getPessoa().getCliente(),
                pessoaJuridica.getPessoa(), new Date(),
                justificativa, getStateManager().getEmpresaSelecionada().getId());
        pessoaJuridica.getPessoa().addHistorico(historicoPessoaCliente);
    }

    /**
     *
     * @return
     */
    protected String adicionarEmailPessoaFisica(PessoaFisica pessoaFisica) {
        if(this.pessoaEmail != null && !Strings.isNullOrEmpty(this.pessoaEmail.getEmail())) {
            return verificaSeEmailEstaValido(pessoaFisica.getPessoa());
        } else {
            showMessageInDialog(FacesMessage.SEVERITY_ERROR, "Email", getMessage("field.form.required.message"));
        }
        return null;
    }

    protected String adicionarEmailPessoaJuridica(PessoaJuridica pessoaJuridica) {
        if(this.pessoaEmail != null && !Strings.isNullOrEmpty(this.pessoaEmail.getEmail())) {
            return verificaSeEmailEstaValido(pessoaJuridica.getPessoa());
        } else {
            showMessageInDialog(FacesMessage.SEVERITY_ERROR, "Email", getMessage("field.form.required.message"));
        }
        return null;
    }

    protected void createUpdateLogin(PessoaFisica pessoaFisica, String usuario, byte[] senha){
        Pessoa pessoa = pessoaFisica.getPessoa();
        if(pessoa.getLogin() == null || pessoa.getLogin().getId() == null) {
            Login login = new Login(usuario, senha, getStateManager().getEmpresaSelecionada().getId());
            pessoaFisica.getPessoa().setLogin(login);
        } else {
            pessoaFisica.getPessoa().getLogin().setLogin(usuario);
            pessoaFisica.getPessoa().getLogin().setSenha(senha);
        }
    }

    /**
     *
     * @return
     */
    protected String adicionarContatoPessoaFisica(PessoaFisica pessoaFisica) {
        if (this.pessoaContato != null
                &&this.pessoaContato.getContato() != null
                && this.pessoaContato.getContato().getTipoContato() != null
                && this.pessoaContato.getContato().getDdd() != null &&
                !Strings.isNullOrEmpty(this.pessoaContato.getContato().getNumero())) {
            return verificaSeContatoEstaValido(pessoaFisica.getPessoa());
        } else {
            showMessageInDialog(FacesMessage.SEVERITY_ERROR, "Tipo do Contato, DDD e Número", getMessage("fields.form.required.message"));
        }
        return null;
    }

    /**
     *
     * @return
     */
    protected String adicionarContatoPessoaJuridica(PessoaJuridica pessoaJuridica) {
        if (this.pessoaContato.getContato() != null
                && this.pessoaContato.getContato().getTipoContato() != null
                && this.pessoaContato.getContato().getDdd() != null &&
                !Strings.isNullOrEmpty(this.pessoaContato.getContato().getNumero())) {
            return verificaSeContatoEstaValido(pessoaJuridica.getPessoa());
        } else {
            showMessageInDialog(FacesMessage.SEVERITY_ERROR, "Tipo do Contato, DDD e Número", getMessage("fields.form.required.message"));
        }
        return null;
    }

    /**
     *
     * @return
     */
    protected String adicionarEnderecoPessoaFisica(PessoaFisica pessoaFisica) {
        if (this.pessoaEndereco != null
                && this.pessoaEndereco.getEndereco() != null
                && enderecoCompleto(this.pessoaEndereco.getEndereco())) {
            return verificaSeEnderecoEstaValido(pessoaFisica.getPessoa());
        } else {
            showMessageInDialog(FacesMessage.SEVERITY_ERROR, "CEP, Rua, Cidade e Tipo do Endereço", getMessage("fields.form.required.message"));
        }
        return null;
    }

    /**
     *
     * @return
     */
    protected String adicionarEnderecoPessoaJuridica(PessoaJuridica pessoaJuridica) {
        if (this.pessoaEndereco.getEndereco() != null
                && enderecoCompleto(this.pessoaEndereco.getEndereco())) {
            return verificaSeEnderecoEstaValido(pessoaJuridica.getPessoa());
        } else {
            showMessageInDialog(FacesMessage.SEVERITY_ERROR, "CEP, Rua, Cidade e Tipo do Endereço", getMessage("fields.form.required.message"));
        }
        return null;
    }

    /**
     *
     * @return
     */
    protected String adicionarResponsavelPessoaJuridica(PessoaJuridica pessoaJuridica) {
        if (this.pessoaJuridicaResponsavel != null
                && this.pessoaJuridicaResponsavel.getPessoaFisica() != null) {
            return verificaSeResponsavelEstaValido(pessoaJuridica);
        } else {
            showMessageInDialog(FacesMessage.SEVERITY_ERROR, "Responsável", getMessage("field.form.required.message"));
        }
        return null;
    }

    /**
     *
     * @return
     */
    protected String adicionarCentroCustoPessoaJuridica(PessoaJuridica pessoaJuridica) {
        if (this.pessoaJuridicaCentroCusto != null
                && this.pessoaJuridicaCentroCusto.getCentroCusto() != null) {
            return verificaSeCentroCustoEstaValido(pessoaJuridica);
        } else {
            showMessageInDialog(FacesMessage.SEVERITY_ERROR, "Centro de Custo", getMessage("fields.form.required.message"));
        }
        return null;
    }

    private boolean enderecoCompleto(Endereco endereco) {
        return !Strings.isNullOrEmpty(endereco.getCep())
                && !Strings.isNullOrEmpty(endereco.getDescricao())
                && endereco.getCidade() != null && endereco.getTipoEndereco() != null;
    }

    /**
     *
     */
    protected void buscarCep(String cep) {
        setPessoaEndereco(new PessoaEndereco(getStateManager().getEmpresaSelecionada().getId()));
        if (cep == null || cep.isEmpty()) {
            return;
        }
        CepDto cepDto = cepSessioBean.buscaCep(cep);

        if(cepDto == null) {
            buscarCepWebService(cep);
            return;
        }

        if (!Strings.isNullOrEmpty(cepDto.getCidade())) {
            Cidade cidade = carregaCidade(cepDto.getUf(), cepDto.getCidade());
            this.pessoaEndereco.getEndereco().setCidade(cidade);
            Uf uf = ufSessionBean.findBySigla(cepDto.getUf());
            if (this.pessoaEndereco.getEndereco().getCidade() != null) {
                this.pessoaEndereco.getEndereco().getCidade().setUf(uf);
            }
        }

        if(!cepDto.isCepUnico()) {
            this.pessoaEndereco.getEndereco().setCep(cepDto.getCep());
            this.pessoaEndereco.getEndereco().setDescricao(cepDto.getTipoLogradouro().concat(" ").concat(cepDto.getLogradouro()));
            this.pessoaEndereco.getEndereco().setBairro(cepDto.getBairro());
        }
    }

    private void buscarCepWebService(String cep) {
        try {
            Webservicecep ws = BuscaCep.getEndereco(cep);
            this.pessoaEndereco.getEndereco().setCep(cep);
            if (ws != null) {
                this.pessoaEndereco.getEndereco()
                        .setDescricao(ws.getTipo_logradouro().concat(" ").concat(ws.getLogradouro()));
                this.pessoaEndereco.getEndereco().setBairro(ws.getBairro());

                if (!ws.getCidade().isEmpty()) {
                    Cidade cidade = carregaCidade(ws.getUf(), ws.getCidade());
                    this.pessoaEndereco.getEndereco().setCidade(cidade);
                    Uf uf = ufSessionBean.findBySigla(ws.getUf());
                    this.pessoaEndereco.getEndereco().getCidade().setUf(uf);
                }
            }
        } catch (JAXBException e) {
            LOGGER.error(e, e.getCause());
        } catch (MalformedURLException e) {
            LOGGER.error(e, e.getCause());
        }
    }

    protected void enviaEmailNovoUsuarioPessoa(Pessoa pessoa) throws ValidationException {
        Email email = emailSessionBean.findBySigla(EmailIndicador.SENHA_NOVO_USUARIO.getValue(),getStateManager().getEmpresaSelecionada().getId());

        if(email == null || (email != null && !email.getAtivo())) {
            exibirMensagem(FacesMessage.SEVERITY_INFO, "Email para Novo Usuário não cadastrado ou inativo...");
            return;
        }

        Map<String, String> parametersValues = Maps.newHashMap();
        for (EmailVinculoParametro parametro : email.getParametroEmails()) {
            String valor = SenhaEmailBusiness.execute(parametro.getParametroEmail(), pessoa, new Pessoa(), getUser());
            parametersValues.put(parametro.getParametroEmail().getSigla(), valor);
        }

        String textoFormatado = EmailTemplateBusiness.criarEmailTemplate(email.getAssunto(), email.getDescricao(), parametersValues);
        EmailCaixaSaida emailSaida = new EmailCaixaSaida(getUser().getName(), email.getAssunto(), textoFormatado, getStateManager().getEmpresaSelecionada().getId());

        for (PessoaEmail pe : pessoa.getPessoasEmails()) {
            EmailDestinatario destinatario = new EmailDestinatario(emailSaida, pe.getEmail());
            emailSaida.getEmails().add(destinatario);
        }

        if(!email.getNecessarioAprovacao()) {
            emailCaixaSaidaSessionBean.createSemAprovacao(emailSaida);
            exibirMensagem(FacesMessage.SEVERITY_INFO, "Email de novo usuário enviado...");
            return;
        }

        emailCaixaSaidaSessionBean.create(emailSaida);
        exibirMensagem(FacesMessage.SEVERITY_INFO, "Email de novo usuário aguardando aprovação do Administrador do sistema...");

    }

    protected void initObjetoPessoaCollectors(Pessoa pessoa) {
        this.pessoaEmail = new PessoaEmail(getStateManager().getEmpresaSelecionada().getId());
        this.pessoaEmail.setPessoa(pessoa);
        this.pessoaContato = new PessoaContato(getStateManager().getEmpresaSelecionada().getId());
        this.pessoaContato.setPessoa(pessoa);
        this.pessoaEndereco = new PessoaEndereco(getStateManager().getEmpresaSelecionada().getId());
        this.pessoaEndereco.setPessoa(pessoa);

    }

    protected void initObjetoPessoaJuridicaCollectors(PessoaJuridica pessoaJuridica){
        initObjetoPessoaCollectors(pessoaJuridica.getPessoa());
        this.pessoaJuridicaCentroCusto = new PessoaJuridicaCentroCusto(getStateManager().getEmpresaSelecionada().getId());
        this.pessoaJuridicaCentroCusto.setPessoaJuridica(pessoaJuridica);
        this.pessoaJuridicaResponsavel = new PessoaJuridicaResponsavel(getStateManager().getEmpresaSelecionada().getId());
        this.pessoaJuridicaResponsavel.setPessoaJuridica(pessoaJuridica);
    }

    private String verificaSeEmailEstaValido(Pessoa pessoa) {
        if(!validarEmail(this.pessoaEmail.getEmail())) {
            showMessageInDialog(FacesMessage.SEVERITY_ERROR, getMessage("commons.form.message"), getMessage("field.form.invalid.mail.message"));
            return null;
        } else if(!Strings.isNullOrEmpty(verificaSeEmailEstaRepetido(pessoa))){
            showMessageInDialog(FacesMessage.SEVERITY_ERROR, getMessage("commons.form.message"), getMessage("field.form.invalid.mail.message.unique"));
            return null;
        }
        pessoa.addPessoasEmails(new PessoaEmail(pessoa, this.pessoaEmail.getEmail(), getStateManager().getEmpresaSelecionada().getId()));
        setPessoaEmail(new PessoaEmail(getStateManager().getEmpresaSelecionada().getId()));
        return null;
    }

    private String verificaSeEmailEstaRepetido(Pessoa pessoa) {
        for (PessoaEmail email :pessoa.getPessoasEmails()) {
            if(email.getEmail().equals(this.pessoaEmail.getEmail())) {
                return REPETIDO;
            }
        }
        return null;
    }

    private String verificaSeContatoEstaValido(Pessoa pessoa) {
        pessoa.addPessoasContatos(new PessoaContato(this.pessoaContato.getContato(), pessoa, getStateManager().getEmpresaSelecionada().getId()));
        setPessoaContato(new PessoaContato(getStateManager().getEmpresaSelecionada().getId()));
        return null;
    }

    private String verificaSeEnderecoEstaValido(Pessoa pessoa) {
        pessoa.addPessoasEnderecos(new PessoaEndereco(this.pessoaEndereco.getEndereco(), pessoa, getStateManager().getEmpresaSelecionada().getId()));
        setPessoaEndereco(new PessoaEndereco(getStateManager().getEmpresaSelecionada().getId()));

        return null;
    }

    private String verificaSeResponsavelEstaValido(PessoaJuridica pessoaJuridica) {
        if(!Strings.isNullOrEmpty(verificaSeResponsavelEstaRepetido(pessoaJuridica))){
            showMessageInDialog(FacesMessage.SEVERITY_ERROR, getMessage("commons.form.message"), getMessage("field.form.invalid.responsavel.message.unique"));
            return null;
        }
        pessoaJuridica.getPessoasJuridicasResponsaveis().add(new PessoaJuridicaResponsavel(pessoaJuridica, this.pessoaJuridicaResponsavel.getPessoaFisica(), getStateManager().getEmpresaSelecionada().getId()));
        setPessoaJuridicaResponsavel(new PessoaJuridicaResponsavel(getStateManager().getEmpresaSelecionada().getId()));
        return null;
    }

    private String verificaSeCentroCustoEstaValido(PessoaJuridica pessoaJuridica) {
        if(!Strings.isNullOrEmpty(verificaSeCentroCustoEstaRepetido(pessoaJuridica))){
            showMessageInDialog(FacesMessage.SEVERITY_ERROR, getMessage("commons.form.message"), getMessage("field.form.invalid.centro.custo.message.unique"));
            return null;
        }
        pessoaJuridica.getPessoasJuridicasCentrosCustos().add(new PessoaJuridicaCentroCusto(pessoaJuridica, this.pessoaJuridicaCentroCusto.getCentroCusto(), this.pessoaJuridicaCentroCusto.getPessoaFisica(), getStateManager().getEmpresaSelecionada().getId()));
        setPessoaJuridicaCentroCusto(new PessoaJuridicaCentroCusto(getStateManager().getEmpresaSelecionada().getId()));
        return null;
    }

    private String verificaSeResponsavelEstaRepetido(PessoaJuridica pessoaJuridica) {
        for (PessoaJuridicaResponsavel pessoaJuridicaResponsavel :pessoaJuridica.getPessoasJuridicasResponsaveis()) {
            if(pessoaJuridicaResponsavel.getPessoaFisica().equals(this.pessoaJuridicaResponsavel.getPessoaFisica())) {
                return REPETIDO;
            }
        }
        return null;
    }

    private String verificaSeCentroCustoEstaRepetido(PessoaJuridica pessoaJuridica) {
        for (PessoaJuridicaCentroCusto pessoaJuridicaCentroCusto : pessoaJuridica.getPessoasJuridicasCentrosCustos()) {
            if(pessoaJuridicaCentroCusto.getPessoaFisica() != null
                    && pessoaJuridicaCentroCusto.getPessoaFisica().equals(this.pessoaJuridicaCentroCusto.getPessoaFisica())
                    && pessoaJuridicaCentroCusto.getCentroCusto().equals(this.pessoaJuridicaCentroCusto.getCentroCusto())) {
                return REPETIDO;
            }
        }
        return null;
    }

    private static boolean validarEmail(String email){
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    private Cidade carregaCidade(String siglaUf, String cidade) {
        Uf uf = ufSessionBean.findBySigla(siglaUf);
        return cidadeSessionBean.findByUfDescricao(uf, cidade);
    }

    protected boolean isPodeAlterar(String tela){
        Direito direito = direitoSessionBean.retrieve(ALTERAR);
        return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), tela, direito, getUser().getCodEmpresa());
    }

    protected boolean isPodeExcluir(String tela){
        Direito direito = direitoSessionBean.retrieve(EXCLUIR);
        return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), tela, direito, getUser().getCodEmpresa());
    }

    protected boolean isPodeIncluir(String tela){
        Direito direito = direitoSessionBean.retrieve(INCLUIR);
        return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), tela, direito, getUser().getCodEmpresa());
    }

    protected boolean isPodeConsultar(String tela){
        Direito direito = direitoSessionBean.retrieve(CONSULTAR);
        return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), tela, direito, getUser().getCodEmpresa());
    }

    protected boolean isPodeVisualizarEmails(String tela){
        Direito direito = direitoSessionBean.retrieve(CONSULTAR);
        return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), tela, direito, getUser().getCodEmpresa());
    }

    protected boolean isPodeIncluirEmails(String tela){
        Direito direito = direitoSessionBean.retrieve(INCLUIR);
        return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), tela, direito, getUser().getCodEmpresa());
    }

    protected boolean isPodeExcluirEmails(String tela){
        Direito direito = direitoSessionBean.retrieve(EXCLUIR);
        return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), tela, direito, getUser().getCodEmpresa());
    }

    protected boolean isPodeVisualizarContatos(String tela){
        Direito direito = direitoSessionBean.retrieve(CONSULTAR);
        return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), tela, direito, getUser().getCodEmpresa());
    }

    protected boolean isPodeIncluirContatos(String tela){
        Direito direito = direitoSessionBean.retrieve(INCLUIR);
        return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), tela, direito, getUser().getCodEmpresa());
    }

    protected boolean isPodeExcluirContatos(String tela){
        Direito direito = direitoSessionBean.retrieve(EXCLUIR);
        return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), tela, direito, getUser().getCodEmpresa());
    }

    protected boolean isPodeVisualizarEnderecos(String tela){
        Direito direito = direitoSessionBean.retrieve(CONSULTAR);
        return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), tela, direito, getUser().getCodEmpresa());
    }

    protected boolean isPodeIncluirEnderecos(String tela){
        Direito direito = direitoSessionBean.retrieve(INCLUIR);
        return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), tela, direito, getUser().getCodEmpresa());
    }

    protected boolean isPodeExcluirEnderecos(String tela){
        Direito direito = direitoSessionBean.retrieve(EXCLUIR);
        return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), tela, direito, getUser().getCodEmpresa());
    }

    protected boolean isPodeVisualizarResponsaveis(String tela){
        Direito direito = direitoSessionBean.retrieve(CONSULTAR);
        return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), tela, direito, getUser().getCodEmpresa());
    }

    protected boolean isPodeIncluirResponsaveis(String tela){
        Direito direito = direitoSessionBean.retrieve(INCLUIR);
        return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), tela, direito, getUser().getCodEmpresa());
    }

    protected boolean isPodeExcluirResponsaveis(String tela){
        Direito direito = direitoSessionBean.retrieve(EXCLUIR);
        return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), tela, direito, getUser().getCodEmpresa());
    }

    protected boolean isPodeVisualizarCentrosCusto(String tela){
        Direito direito = direitoSessionBean.retrieve(CONSULTAR);
        return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), tela, direito, getUser().getCodEmpresa());
    }

    protected boolean isPodeIncluirCentrosCusto(String tela){
        Direito direito = direitoSessionBean.retrieve(INCLUIR);
        return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), tela, direito, getUser().getCodEmpresa());
    }

    protected boolean isPodeExcluirCentrosCusto(String tela){
        Direito direito = direitoSessionBean.retrieve(EXCLUIR);
        return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), tela, direito, getUser().getCodEmpresa());
    }

    public PessoaEmail getPessoaEmail() {
        return pessoaEmail;
    }

    public void setPessoaEmail(PessoaEmail pessoaEmail) {
        this.pessoaEmail = pessoaEmail;
    }

    public PessoaContato getPessoaContato() {
        return pessoaContato;
    }

    public void setPessoaContato(PessoaContato pessoaContato) {
        this.pessoaContato = pessoaContato;
    }

    public PessoaEndereco getPessoaEndereco() {
        return pessoaEndereco;
    }

    public void setPessoaEndereco(PessoaEndereco pessoaEndereco) {
        this.pessoaEndereco = pessoaEndereco;
    }

    public PessoaJuridicaCentroCusto getPessoaJuridicaCentroCusto() {
        return pessoaJuridicaCentroCusto;
    }

    public void setPessoaJuridicaCentroCusto(PessoaJuridicaCentroCusto pessoaJuridicaCentroCusto) {
        this.pessoaJuridicaCentroCusto = pessoaJuridicaCentroCusto;
    }

    public PessoaJuridicaResponsavel getPessoaJuridicaResponsavel() {
        return pessoaJuridicaResponsavel;
    }

    public void setPessoaJuridicaResponsavel(PessoaJuridicaResponsavel pessoaJuridicaResponsavel) {
        this.pessoaJuridicaResponsavel = pessoaJuridicaResponsavel;
    }
}
