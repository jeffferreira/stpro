package br.com.juridico.controller;

import br.com.juridico.comparator.EventoComparator;
import br.com.juridico.entidades.ProjudiprProcessoControladoria;
import br.com.juridico.entidades.ProjudiprProcessoDocumento;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.filter.ControladoriaFilter;
import br.com.juridico.impl.ProjudiprControladoriaProcessoSessionBean;
import br.com.juridico.util.DownloadUtil;
import br.com.juridico.util.FileUtil;
import com.google.common.collect.Maps;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Jefferson
 *
 */
@ManagedBean
@ViewScoped
public class ControladoriaTramitacaoController extends AbstractController {

	private static final Logger LOGGER = Logger.getLogger(ControladoriaTramitacaoController.class);
	private static final String CHAVE_ACESSO = "TRAMITACAOPROCESSO_TRAMITACAO_PROCESSO_TEMPLATE";
	private static final String PAGE_FORM = "/pages/controladoria/tramitacaoprocesso/tramitacao-processo-template.xhtml?faces-redirect=true";

	@Inject
	private ProjudiprControladoriaProcessoSessionBean controladoriaProcessoSessionBean;

	private List<ProjudiprProcessoControladoria> projudiProcessos;
	private ProjudiprProcessoControladoria processoControladoriaSelecionado;

	private ControladoriaFilter filter;

	@PostConstruct
	public void initialize(){
		buscarAlteracoesProjudi();
	}

	private void buscarAlteracoesProjudi() {
		this.projudiProcessos = controladoriaProcessoSessionBean.findByProcessos(getStateManager().getEmpresaSelecionada().getId());
	}

	public void historicoListener(ProjudiprProcessoControladoria processoControladoria){
		this.processoControladoriaSelecionado = processoControladoria;
		RequestContext.getCurrentInstance().update("historicoDetail");
	}

	public void vistar(ProjudiprProcessoControladoria processoControladoria){
		try {
			processoControladoria.setVistoPelaControladoria(Boolean.TRUE);
			controladoriaProcessoSessionBean.update(processoControladoria);
			buscarAlteracoesProjudi();
		} catch (ValidationException e) {
			LOGGER.error(e, e.getCause());
		}
	}

	public String goToTramitacao(){
		return PAGE_FORM;
	}

	public boolean isHasAccess(){
		return possuiAcesso(CHAVE_ACESSO);
	}

	public List<ProjudiprProcessoControladoria> getProjudiProcessos() {
		for (ProjudiprProcessoControladoria p : projudiProcessos) {
			String descricao = p.getMovimentacao().getProcesso().getProcesso().replaceAll("Processo", "");
			p.getMovimentacao().getProcesso().setProcesso(descricao);
		}
		return projudiProcessos;
	}

	public void setProjudiProcessos(List<ProjudiprProcessoControladoria> projudiProcessos) {
		this.projudiProcessos = projudiProcessos;
	}

	public ProjudiprProcessoControladoria getProcessoControladoriaSelecionado() {
		return processoControladoriaSelecionado;
	}

	public void setProcessoControladoriaSelecionado(ProjudiprProcessoControladoria processoControladoriaSelecionado) {
		this.processoControladoriaSelecionado = processoControladoriaSelecionado;
	}

	public void dataInicioAlteracaoChanged(ValueChangeEvent event){
		this.filter.setDataInicioAlteracao((Date) event.getNewValue());
	}

	public void dataFimAlteracaoChanged(ValueChangeEvent event){
		this.filter.setDataFimAlteracao((Date) event.getNewValue());
	}

	public void downloadDocumento(ProjudiprProcessoControladoria processoControladoria) throws IOException {
		List<ProjudiprProcessoDocumento> documentos = processoControladoria.getMovimentacao().getDocumentoProcessos();

		if(documentos.size() == 1) {
			ProjudiprProcessoDocumento documento = documentos.get(0);
			File file = new File(documento.getNomeArquivo());

			if(file.exists()) {
				byte[] arquivo = FileUtil.getContent(file);
				String[] parts = formatarCaminhoArquivo(documento.getNomeArquivo());
				DownloadUtil.downloadFile(arquivo, parts[parts.length-1].trim());
			}
			exibirMensagem(FacesMessage.SEVERITY_ERROR, getMessage("projudipr_documento_nao_encontrado"));
			return;
		}
		ziparArquivos(documentos);

	}

	private void ziparArquivos(List<ProjudiprProcessoDocumento> documentos) throws IOException {
		Map<String, byte[]> arquivos = Maps.newHashMap();

		for (ProjudiprProcessoDocumento documento : documentos) {
			File file = new File(documento.getNomeArquivo());
			if(file.exists()) {
				String[] parts = formatarCaminhoArquivo(documento.getNomeArquivo());
				String nomeArquivo = parts[parts.length-1].trim();
				arquivos.put(nomeArquivo, FileUtil.getContent(file));
			}
		}

		if(!arquivos.isEmpty()){
			byte[] zip = FileUtil.createZip(arquivos);
			DownloadUtil.downloadFile(zip, "arquivos.zip");
			return;
		}
		exibirMensagem(FacesMessage.SEVERITY_ERROR, getMessage("projudipr_documentos_nao_encontrados"));
	}

	public boolean processoContemDocumento(ProjudiprProcessoControladoria projudiprProcessoControladoria) {
		return !projudiprProcessoControladoria.getMovimentacao().getDocumentoProcessos().isEmpty();
	}

	public int quantidadeDocumentos(ProjudiprProcessoControladoria projudiprProcessoControladoria) {
		List<ProjudiprProcessoDocumento> documentos = projudiprProcessoControladoria.getMovimentacao().getDocumentoProcessos();
		return documentos.isEmpty() ? 0 : documentos.size();
	}

	private static String[] formatarCaminhoArquivo(String linha) {
		String formatado = linha.replaceAll("/", "#");
		return formatado.replaceAll("##", "#").split("#");
	}

	public void pesquisar(){
		this.projudiProcessos = controladoriaProcessoSessionBean.findByProcessos(getStateManager().getEmpresaSelecionada().getId());
	}

	public void limpar(){
		this.filter = new ControladoriaFilter();
		RequestContext.getCurrentInstance().update("groupFilter");
	}

	public ControladoriaFilter getFilter() {
		return filter;
	}

	public void setFilter(ControladoriaFilter filter) {
		this.filter = filter;
	}
}
