package br.com.juridico.controller;

import br.com.juridico.entidades.*;
import br.com.juridico.impl.ProjudiprProcessoSessionBean;
import br.com.juridico.util.DownloadUtil;
import br.com.juridico.util.FileUtil;
import org.primefaces.context.RequestContext;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * Created by jeffersonl2009_00 on 25/07/2016.
 */
@ManagedBean
@ViewScoped
public class TramiteProcessoController extends AbstractController {

    private static final Long PROJUDI_PR = 1L;
    private static final String PROCESSO_FLASH = "PROCESSO_FLASH";
    private static final String PAGE_FORM = "/pages/tramite/consulta-tramite-template.xhtml?faces-redirect=true";

    @Inject
    private ProjudiprProcessoSessionBean projudiPrProcessoSessionBean;

    private Processo processoSelecionado;
    private MeioTramitacao meioTramitacaoSelecionado;
    private ProjudiprProcesso projudiPrSelecionado;

    /**
     * @PostConstruct
     */
    @PostConstruct
    public void initialize() {
        Processo processoFlash = (Processo) getFlashScope().get(PROCESSO_FLASH);

        if(processoFlash != null) {
            this.processoSelecionado = processoFlash;
            this.meioTramitacaoSelecionado = processoFlash.getMeioTramitacao();
            projudiPrProcessoSessionBean.clearSession();
            List<ProjudiprProcesso> processos = projudiPrProcessoSessionBean.findByProcessos(processoSelecionado);
            this.projudiPrSelecionado = processos.isEmpty() ? null : processos.get(0);
        }

    }

    public String goToConsultaTramiteProcesso(){
        if(isProjudipr()){
            getFlashScope().put(PROCESSO_FLASH, this.processoSelecionado);
            return PAGE_FORM;
        }
        return "";
    }

    public boolean isProjudipr(){
        if(processoSelecionado == null){
            return false;
        }
        meioTramitacaoSelecionado = processoSelecionado.getMeioTramitacao();
        return meioTramitacaoSelecionado.getRoboTramitacao() != null
                && PROJUDI_PR.equals(meioTramitacaoSelecionado.getRoboTramitacao().getId());
    }

    public boolean isPje(){
        return false;
    }

    public MeioTramitacao getMeioTramitacaoSelecionado() {
        return meioTramitacaoSelecionado;
    }

    public void setMeioTramitacaoSelecionado(MeioTramitacao meioTramitacaoSelecionado) {
        this.meioTramitacaoSelecionado = meioTramitacaoSelecionado;
    }

    public ProjudiprProcesso getProjudiPrSelecionado() {
        return projudiPrSelecionado;
    }

    public void setProjudiPrSelecionado(ProjudiprProcesso projudiPrSelecionado) {
        this.projudiPrSelecionado = projudiPrSelecionado;
    }

    public Processo getProcessoSelecionado() {
        return processoSelecionado;
    }

    public void setProcessoSelecionado(Processo processoSelecionado) {
        this.processoSelecionado = processoSelecionado;
    }

    public Date getDataUltimaAtualizacao(){
        if(projudiPrSelecionado == null || projudiPrSelecionado.getMovimentacoes().isEmpty()){
            return projudiPrSelecionado == null ? null : projudiPrSelecionado.getDataCadastro();
        }
        return projudiPrSelecionado.getMovimentacoes().get(0).getDataUltimaAtualizacao();
    }

    public void downloadDocumento(ProjudiprProcessoDocumento documento) throws IOException {
        File file = new File(documento.getNomeArquivo());

        if(file.exists()) {
            byte[] arquivo = FileUtil.getContent(file);
            DownloadUtil.downloadFile(arquivo, documento.getNome());
            return;
        }
        exibirMensagem(FacesMessage.SEVERITY_ERROR, getMessage("projudipr_documento_nao_encontrado"));
    }

    public boolean processoContemDocumento(ProjudiprMovimentacao movimentacao) {
        return !movimentacao.getDocumentoProcessos().isEmpty();
    }

    public int quantidadeDocumentos(ProjudiprMovimentacao movimentacao) {
        List<ProjudiprProcessoDocumento> documentos = movimentacao.getDocumentoProcessos();
        return documentos.isEmpty() ? 0 : documentos.size();
    }

}
