package br.com.juridico.controller.manutencao;

import br.com.juridico.controller.AbstractController;
import br.com.juridico.entidades.Configuracao;
import br.com.juridico.entidades.Empresa;
import br.com.juridico.entidades.EmpresaConfiguracao;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.impl.ConfiguracaoSessionBean;
import br.com.juridico.impl.EmpresaConfiguracaoSessionBean;
import br.com.juridico.impl.EmpresaSessionBean;
import br.com.juridico.impl.PessoaSessionBean;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Marcos Melo on 10/01/2017.
 */
@ManagedBean
@ViewScoped
public class ManutencaoConfiguracaoController extends AbstractController {

    private static final Logger LOGGER = Logger.getLogger(ManutencaoConfiguracaoController.class);

    @Inject
    private PessoaSessionBean pessoaSessionBean;
    @Inject
    private EmpresaConfiguracaoSessionBean empresaConfiguracaoSessionBean;
    @Inject
    private ConfiguracaoSessionBean configuracaoSessionBean;
    @Inject
    private EmpresaSessionBean empresaSessionBean;

    @PostConstruct
    public void initialize(){
        empresaSessionBean.clearSession();
    }

    public void atualizaModuloAtivo(Empresa empresa, String configuracao) {
        empresaSessionBean.clearSession();
        Configuracao conf = configuracaoSessionBean.findByDescricao(configuracao);
        EmpresaConfiguracao empresaConfiguracao = empresaConfiguracaoSessionBean.findByEmpresaAndConfiguracao(empresa.getId(), conf.getId());

        try {
            if (empresaConfiguracao != null) {
                for (Iterator<EmpresaConfiguracao> iterator = empresa.getConfiguracoes().iterator(); iterator.hasNext();) {
                    EmpresaConfiguracao empresaConf = iterator.next();
                    if (empresaConfiguracao.equals(empresaConf)) {
                        iterator.remove();
                    }
                }
                empresaSessionBean.update(empresa);
                return;
            }
            EmpresaConfiguracao novaConfiguracao = new EmpresaConfiguracao(empresa, conf);
            empresaConfiguracaoSessionBean.create(novaConfiguracao);
        } catch (ValidationException e) {
            LOGGER.error(e, e.getCause());
        }
    }

    public List<Empresa> getEmpresas(){
        return empresaSessionBean.findAllEmpresas();
    }
}
