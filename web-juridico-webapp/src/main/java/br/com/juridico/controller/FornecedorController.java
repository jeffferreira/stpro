package br.com.juridico.controller;

import br.com.juridico.datamodel.LazyEntityDataModel;
import br.com.juridico.entidades.Fornecedor;
import br.com.juridico.enums.TipoPessoaType;
import br.com.juridico.exception.CrudException;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.impl.FornecedorSessionBean;
import br.com.juridico.interfaces.Descriptable;
import com.google.common.base.Strings;

import javax.annotation.PostConstruct;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Jefferson
 *
 */
@Named
@ViewScoped
public class FornecedorController extends CrudController<Fornecedor> {

	/**
	 *
	 */
	private static final long serialVersionUID = -8741515223323095235L;

	@Inject
	private FornecedorSessionBean fornecedorSessionBean;

	private List<SelectItem> tiposPessoa;

	private boolean carregarFavorecido;

	@PostConstruct
	public void initialize(){
		carregaLazyDataModel();
		this.tiposPessoa = buildSelectItemList(TipoPessoaType.values());
	}

	@Override
	protected void validar() throws ValidationException {
		fornecedorSessionBean.clearSession();
		boolean jaExisteFornecedor = fornecedorSessionBean.isNomeJaCadastrada(getObjetoSelecionado(), getStateManager().getEmpresaSelecionada().getId());
		getObjetoSelecionado().validar(jaExisteFornecedor, carregarFavorecido);
	}

	@Override
	protected void create(Fornecedor objetoSelecionado) throws ValidationException, CrudException {
		if (isEditavel() ) {
			fornecedorSessionBean.update(objetoSelecionado);
		} else if (!Strings.isNullOrEmpty(objetoSelecionado.getNome())) {
			fornecedorSessionBean.create(objetoSelecionado);
		} else {
			throw new CrudException();
		}
	}

	@Override
	protected void antesDeSelecionar(Fornecedor objeto) throws CrudException {
		if(objeto == null){
			return;
		}
		if(!Strings.isNullOrEmpty(objeto.getNomeFavorecido())){
			this.carregarFavorecido = Boolean.TRUE;
		}
		super.antesDeSelecionar(objeto);
	}

	@Override
	protected void antesDeSalvar() throws CrudException {
		if(!carregarFavorecido){
			limparCamposFavorecido();
		}
		super.antesDeSalvar();
	}

	private void limparCamposFavorecido() {
		getObjetoSelecionado().setTipoFavorecido(null);
		getObjetoSelecionado().setNomeFavorecido(null);
		getObjetoSelecionado().setCpfCnpjFavorecido(null);
		getObjetoSelecionado().setBancoFavorecido(null);
		getObjetoSelecionado().setAgenciaFavorecido(null);
		getObjetoSelecionado().setContaFavorecido(null);
	}

	@Override
	protected Fornecedor inicializaObjetosLazy(Fornecedor objeto) {
		fornecedorSessionBean.clearSession();
		return fornecedorSessionBean.retrieve(objeto.getId());
	}

	@Override
	protected void carregaLazyDataModel() {
		setLazyDataModel(new LazyEntityDataModel<Fornecedor>() {
			@Override
			protected List<Fornecedor> consultaFiltrada(ConsultaLazyFilter filtro) {
				filtro.setDescricao(getDescricaoFiltro());
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return fornecedorSessionBean.filtrados(filtro);
			}

			@Override
			protected int countTotalRegistros(ConsultaLazyFilter filtro) {
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return fornecedorSessionBean.quantidadeFiltrados(filtro);
			}
		});
	}

	@Override
	protected Fornecedor instanciarObjetoNovo() {
		return new Fornecedor();
	}

	@Override
	protected void setEmpresaDeUsuarioLogado(Fornecedor objetoSelecionado) {
		objetoSelecionado.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
	}

	public void carregarFavorecidoChanged(ValueChangeEvent event){
		this.carregarFavorecido = (boolean) event.getNewValue();
		if(carregarFavorecido){
			getObjetoSelecionado().setTipoFavorecido(TipoPessoaType.F);
		}
	}

	@Override
	protected void antesDeExcluir() throws CrudException {
		getObjetoSelecionado().setDataExclusao(new Date());
	}

	@Override
	protected void aposExcluir() throws CrudException {
		super.buscarPorLazy();
	}

	@Override
	protected boolean isPodeAlterar() {
		return true;
	}

	@Override
	protected boolean isPodeExcluir() {
		return true;
	}

	@Override
	protected boolean isPodeIncluir() {
		return true;
	}

	@Override
	protected void addChaveAcesso() {
	}

	public List<SelectItem> getTiposPessoa() {
		return tiposPessoa;
	}

	public void setTiposPessoa(List<SelectItem> tiposPessoa) {
		this.tiposPessoa = tiposPessoa;
	}

	public boolean isCarregarFavorecido() {
		return carregarFavorecido;
	}

	public void setCarregarFavorecido(boolean carregarFavorecido) {
		this.carregarFavorecido = carregarFavorecido;
	}
}
