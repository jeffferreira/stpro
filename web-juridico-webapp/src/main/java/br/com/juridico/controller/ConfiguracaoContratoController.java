package br.com.juridico.controller;

import br.com.juridico.business.OrganogramaBusiness;
import br.com.juridico.dto.ArvoreContrato;
import br.com.juridico.entidades.Contrato;
import br.com.juridico.entidades.ContratoSubContrato;
import br.com.juridico.exception.CrudException;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.impl.ContratoSessionBean;
import br.com.juridico.impl.ContratoSubContratoSessionBean;
import br.com.juridico.util.PosicaoArvoreUtil;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.model.CheckboxTreeNode;
import org.primefaces.model.TreeNode;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 
 * @author Jefferson
 *
 */
@Named
@ViewScoped
public class ConfiguracaoContratoController extends CrudController<Contrato> {

	private static final Logger LOGGER = Logger.getLogger(ConfiguracaoContratoController.class);
	private static final String PAGINA_FORMULARIO = "/pages/contratos/configuracao/configuracao-contrato-template.xhtml?faces-redirect=true";

	@Inject
	private ContratoSessionBean contratoSessionBean;
	@Inject
	private ContratoSubContratoSessionBean contratoSubContratoSessionBean;

	private TreeNode arvore;
	private TreeNode[] selectedNodes;
	private String subContrato;
	private Contrato contratoSelecionado;
	private StringBuilder organograma;
	private String html;

	public String goToFormulario(){
		return PAGINA_FORMULARIO;
	}

	@PostConstruct
	public void initialize(){
		buscarContratos();
	}

	@Override
	protected void create(Contrato contrato) throws ValidationException, CrudException {
		Contrato contratoExclusao = getRegistroComExclusaoLogica();
		if (isEditavel() && contratoExclusao != null) {
			contratoExclusao.setDataExclusao(null);
			contratoSessionBean.update(contratoExclusao);
		} else if (contrato != null && !Strings.isNullOrEmpty(contrato.getDescricao())) {
			contratoSessionBean.create(contrato);
		} else {
			throw new CrudException();
		}
	}

	public void buscarContratos(){
		this.arvore = createArvoreContrato();
	}

	public void adicionarSubContrato(){
		try {
			if(contratoSelecionado == null) {
				exibirMensagem(FacesMessage.SEVERITY_ERROR, getMessage("msg_erro_salvar"));
				return;
			}
			updateSubContrato();
			exibirMensagem(FacesMessage.SEVERITY_INFO, getMessage("msg_sucesso_salvar"));
			RequestContext.getCurrentInstance().update("configuracaoContratoForm");
			buscarContratos();
			this.subContrato = "";
		} catch (ValidationException e) {
			LOGGER.error(e, e.getCause());
		}
	}

	@Override
	public void excluir() throws ValidationException {
		List<ContratoSubContrato> subContratos = Lists.newArrayList();
		List<Contrato> contratos = Lists.newArrayList();
		if(selectedNodes != null && selectedNodes.length > 0) {
			for (TreeNode node : selectedNodes) {
				ArvoreContrato arvoreContrato = (ArvoreContrato) node.getData();

				if(Strings.isNullOrEmpty(arvoreContrato.getPosicao())){
					Contrato contrato = contratoSessionBean.retrieve(arvoreContrato.getId());
					contratos.add(contrato);
				} else {
					ContratoSubContrato subContrato = contratoSubContratoSessionBean.retrieve(arvoreContrato.getId());
					subContratos.add(subContrato);
				}
			}
		}
		removerSubContratos(subContratos);
		removerContratos(contratos);
		buscarContratos();
	}

	private void removerSubContratos(List<ContratoSubContrato> subContratos) throws ValidationException {
		for (ContratoSubContrato subContrato : subContratos) {
			subContrato.setContratoSubContrato(null);
			contratoSubContratoSessionBean.update(subContrato);
			contratoSubContratoSessionBean.delete(subContrato.getId());
		}
	}

	private void removerContratos(List<Contrato> contratos) throws ValidationException {
		for (Contrato contrato : contratos) {
			contrato.setDataExclusao(new Date());
			contratoSessionBean.update(contrato);
		}
	}

	private void updateSubContrato() throws ValidationException {
		ContratoSubContrato subContrato = getContratoSubContrato(contratoSelecionado);
		contratoSubContratoSessionBean.create(subContrato);
		visualizarContrato(this.contratoSelecionado.getId());
	}

	public String ultimoNoAbaixoRaiz(Contrato contrato){
		List<PosicaoArvoreUtil> posicoes = Lists.newArrayList();
		List<ContratoSubContrato> subContratos = contratoSubContratoSessionBean.findByContrato(contrato.getId());
		posicoes.addAll(subContratos.stream().filter(subContrato -> subContrato.isNoAbaixoRaiz()).map(subContrato -> new PosicaoArvoreUtil(subContrato.getPosicao())).collect(Collectors.toList()));

		if(posicoes.isEmpty()) {
			return "0";
		}
		Collections.reverse(posicoes);
		return posicoes.get(0).getPosicao();
	}

	protected ContratoSubContrato getContratoSubContrato(Contrato contrato) {
		ContratoSubContrato subContrato = new ContratoSubContrato();
		Integer posicao = Integer.parseInt(ultimoNoAbaixoRaiz(contrato));
		posicao++;
		subContrato.setDescricao(this.subContrato);
		subContrato.setPosicao(posicao.toString());
		subContrato.setContrato(contrato);
		return subContrato;
	}

	public void visualizarContrato(Long idContrato){
		if(idContrato == null) {
			return;
		}
		contratoSessionBean.clearSession();
		Contrato contrato = contratoSessionBean.retrieve(idContrato);
		List<ContratoSubContrato> subContratos = contratoSubContratoSessionBean.findByContrato(idContrato);
		for (ContratoSubContrato contratoSubContrato : subContratos) {
			contratoSubContrato.setPossuiFilhos(!contratoSubContrato.getFilhos().isEmpty());
		}
		contrato.setSubContratos(subContratos);
		setObjetoSelecionado(contrato);
		montarOrganograma(contrato);
	}

	public void novoContratoListener(){
		getObjetoSelecionado().setDescricao("");
		RequestContext.getCurrentInstance().execute("openModal('#myModalNovoContrato')");
	}

	public void adicionarNovoSubContratoListener(Long idContrato){
		this.contratoSelecionado = contratoSessionBean.retrieve(idContrato);
		RequestContext.getCurrentInstance().execute("openModal('#myModalSubContrato')");
	}

	@Override
	protected void aposSalvar() throws CrudException {
		buscarContratos();
		RequestContext.getCurrentInstance().update("configuracaoContratoForm");
	}

	private Contrato getRegistroComExclusaoLogica() {
		return contratoSessionBean.findByDescricao(getObjetoSelecionado().getDescricao(), getUser().getCodEmpresa());
	}

	@Override
	protected void validar() throws ValidationException {
		boolean jaExisteContrato = contratoSessionBean.isDescricaoJaCadastrada(getObjetoSelecionado(), getUser().getCodEmpresa());
		getObjetoSelecionado().validar(jaExisteContrato);
	}

	@Override
	protected Contrato inicializaObjetosLazy(Contrato objetoSelecionado) {
		contratoSessionBean.clearSession();
		return contratoSessionBean.retrieve(objetoSelecionado.getId());
	}

	@Override
	protected Contrato instanciarObjetoNovo() {
		return new Contrato();
	}

	@Override
	protected void setEmpresaDeUsuarioLogado(Contrato objetoSelecionado) {
		objetoSelecionado.setCodigoEmpresa(getUser().getCodEmpresa());
	}

	protected void montarOrganograma(Contrato contrato) {
		this.organograma = new StringBuilder();
		this.organograma = OrganogramaBusiness.abrirTagItem(this.organograma, contrato.toString());
		contratoSubContratoSessionBean.clearSession();
		List<ContratoSubContrato> subContratos = contratoSubContratoSessionBean.findByContrato(contrato.getId());

		if(!subContratos.isEmpty()) {
			this.organograma = OrganogramaBusiness.abrirUl(this.organograma);
			subContratos.stream().filter(subContrato -> subContrato.isNoAbaixoRaiz() && subContrato.getContratoSubContrato() == null).forEach(subContrato -> {
				this.organograma = OrganogramaBusiness.abrirSomenteNoFilho(this.organograma, subContrato.toString());

				if (!subContrato.getFilhos().isEmpty()) {
					carregaFilhosOrganograma(subContrato.getFilhos());
				}
				this.organograma = OrganogramaBusiness.fecharSomenteNoFilho(this.organograma);
			});
			this.organograma = OrganogramaBusiness.fecharUl(this.organograma);
		}

		this.organograma = OrganogramaBusiness.fecharTagItem(this.organograma);
		this.html = this.organograma.toString();
	}

	private void carregaFilhosOrganograma(List<ContratoSubContrato> filhos){
		this.organograma = OrganogramaBusiness.abrirUl(this.organograma);
		for (ContratoSubContrato filho : filhos) {
			this.organograma = OrganogramaBusiness.abrirTagFilho(this.organograma, filho.toString());
			this.organograma = OrganogramaBusiness.fecharHref(this.organograma);

			if(!filho.getFilhos().isEmpty()) {
				carregaFilhosOrganograma(filho.getFilhos());
			}
			this.organograma = OrganogramaBusiness.fecharSomenteNoFilho(this.organograma);
		}
		this.organograma = OrganogramaBusiness.fecharUl(this.organograma);
	}

	public TreeNode createArvoreContrato() {
		TreeNode root = new CheckboxTreeNode(new ArvoreContrato(), null);
		contratoSessionBean.clearSession();
		List<Contrato> contratos = contratoSessionBean.findAll(getUser().getCodEmpresa());

		for (Contrato contrato : contratos) {
			ArvoreContrato arvoreContrato = new ArvoreContrato(contrato.getId(), contrato.getDescricao(), null);
			arvoreContrato.setNoRaiz(true);
			TreeNode noRaiz = new CheckboxTreeNode(arvoreContrato, root);
			contratoSubContratoSessionBean.clearSession();
			List<ContratoSubContrato> subContratos = contratoSubContratoSessionBean.findByContrato(contrato.getId());

			for (ContratoSubContrato subContrato : subContratos) {
				if (subContrato.isNoAbaixoRaiz() && subContrato.getContratoSubContrato() == null) {
					if(!subContrato.getFilhos().isEmpty()) {
						TreeNode noFilho = new CheckboxTreeNode("noFilho", new ArvoreContrato(subContrato.getId(), subContrato.getDescricao(), subContrato.getPosicao()), noRaiz);
						carregaFilhos(subContrato.getFilhos(), noFilho);
						continue;
					}
					new CheckboxTreeNode("noFilho", new ArvoreContrato(subContrato.getId(), subContrato.getDescricao(), subContrato.getPosicao()), noRaiz);
				}
			}
		}
		return root;
	}

	private void carregaFilhos(List<ContratoSubContrato> filhos, TreeNode noFilho){
		for (ContratoSubContrato filho : filhos) {
			if(filho.getFilhos().isEmpty()) {
				new CheckboxTreeNode("filhoDoFilho", new ArvoreContrato(filho.getId(), filho.getDescricao(), filho.getPosicao()), noFilho);
				continue;
			} else {
				TreeNode child = new CheckboxTreeNode("filhoDoFilho", new ArvoreContrato(filho.getId(), filho.getDescricao(), filho.getPosicao()), noFilho);
				carregaFilhos(filho.getFilhos(), child);
			}
		}
	}

	@Override
	protected void addChaveAcesso() {
	}

	@Override
	protected boolean isPodeAlterar() {
		return false;
	}

	@Override
	protected boolean isPodeExcluir() {
		return false;
	}

	@Override
	protected boolean isPodeIncluir() {
		return false;
	}

	@Override
	protected void carregaLazyDataModel() {
	}

	public String getSubContrato() {
		return subContrato;
	}

	public void setSubContrato(String subContrato) {
		this.subContrato = subContrato;
	}

	public TreeNode getArvore() {
		return arvore;
	}

	public void setArvore(TreeNode arvore) {
		this.arvore = arvore;
	}

	public String getHtml() {
		return html;
	}

	public void setHtml(String html) {
		this.html = html;
	}

	public TreeNode[] getSelectedNodes() {
		return selectedNodes;
	}

	public void setSelectedNodes(TreeNode[] selectedNodes) {
		this.selectedNodes = selectedNodes;
	}
}
