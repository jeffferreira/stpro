package br.com.juridico.controller;

import br.com.juridico.dto.ParteDto;
import br.com.juridico.entidades.*;
import br.com.juridico.enums.StatusProcessoIndicador;
import br.com.juridico.exception.CrudException;
import br.com.juridico.exception.StproException;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.impl.*;
import br.com.juridico.types.ProcessoStepsTypes;
import br.com.juridico.util.DateUtils;
import br.com.juridico.util.ExportUtil;
import br.com.juridico.util.Util;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.log4j.Logger;
import org.primefaces.PrimeFaces;
import org.primefaces.context.RequestContext;
import org.primefaces.event.RateEvent;
import org.primefaces.event.SelectEvent;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import javax.servlet.ServletContext;
import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static br.com.juridico.types.ProcessoStepsTypes.STEP_PARTES;

/**
 * Created by jeffersonl2009_00 on 06/07/2016.
 */
@ManagedBean
@SessionScoped
public class CadastroProcessoController extends AbstractProcessoController  {

    private static final Logger LOGGER = Logger.getLogger(CadastroProcessoController.class);

    private static final String PAGE_FORM = "/pages/processos/processo-form.xhtml?faces-redirect=true";
    private static final String FIM_ACAO = "Fim da ação";
    private static final String PROCESSOS_CADASTRO_HONORARIOS = "PROCESSOS_CADASTRO_HONORARIOS";
    private static final String[] STEP_TYPES = {"STEP_PARTES", "STEP_DADOS_PROCESSO", "STEP_RESUMO_ACAO", "STEP_VALORES", "STEP_HONORARIOS", "STEP_CONFIRMACAO", "STEP_EDICAO"};
    private static final String TIPO_ADVOGADO_INDIVIDUAL = "I";

    @Inject
    private ProcessoSessionBean processoSessionBean;
    @Inject
    private GrupoSessionBean grupoSessionBean;
    @Inject
    private PessoaFisicaSessionBean pessoaFisicaSessionBean;
    @Inject
    private PessoaJuridicaSessionBean pessoaJuridicaSessionBean;
    @Inject
    private NumeroVaraSessionBean numeroVaraSessionBean;
    @Inject
    private ContratoPessoaSessioBean contratoPessoaSessioBean;
    @Inject
    private MenuController menuController;
    @Inject
    private MeioTramitacaoSessionBean meioTramitacaoSessionBean;
    @Inject
    private FaseSessionBean faseSessionBean;
    @Inject
    private ClasseProcessualSessionBean classeProcessualSessionBean;
    @Inject
    private NaturezaAcaoSessionBean naturezaAcaoSessionBean;
    @Inject
    private ForumSessionBean forumSessionBean;
    @Inject
    private VaraSessionBean varaSessionBean;
    @Inject
    private ComarcaSessionBean comarcaSessionBean;
    @Inject
    private PedidoSessionBean pedidoSessionBean;
    @Inject
    private CausaPedidoSessionBean causaPedidoSessionBean;
    @Inject
    private GrauRiscoSessionBean grauRiscoSessionBean;

    private List<PessoaFisica> advogados = Lists.newArrayList();
    private List<Grupo> grupos = Lists.newArrayList();
    private List<ContratoPessoa> contratos = Lists.newArrayList();
    private List<StatusProcessoIndicador> statusProcessoItems;
    private List<NumeroVara> numeroList;
    private ProcessoStepsTypes processoStepSelecionado = STEP_PARTES;
    private String stepClick;

    private ParteDto novaParte;
    private ProcessoPedido pedidoSelecionado;
    private ProcessoPessoaJuridicaCentroCusto novoCentroCusto;
    private ParcelaHonorario parcelaHonorario;

    private Long processoEditId;

    private boolean checkDataFimAcaoPercentual;
    private boolean checkPagoPercentual;
    private boolean tooltipCliente;
    private int rowCountCentroCusto;

    private Map<PessoaFisica, Integer> advogadosRating = Maps.newHashMap();

    public void initialize() {
        this.stepClick = processoStepSelecionado.getValue();
        this.parcelaHonorario = new ParcelaHonorario(getStateManager().getEmpresaSelecionada().getId());
        getProcesso().setPrivado(Boolean.FALSE);
        carregarAbaPartes();
        carregaAbaDadosProcesso();
        carregaAbaValores();
        carregaAbaHonorarios();
        carregaStatusProcesso();
    }

    public void meioTramitacaoChanged(ValueChangeEvent event){
        getProcesso().setMeioTramitacao((MeioTramitacao) event.getNewValue());
        if(getProcesso().getMeioTramitacao() != null && getProcesso().getMeioTramitacao().getGeradorAutomatico()){
            processarGeradorNumeroSeNaturezaAcaoEstaVinculada();
            return;
        }
        PrimeFaces.current().ajax().update("processoDescricaoGroup");
    }

    public void naturezaAcaoChanged(ValueChangeEvent event){
        getProcesso().setNaturezaAcao((NaturezaAcao) event.getNewValue());
        if(getProcesso().getNaturezaAcao() != null
                && getProcesso().getMeioTramitacao() != null
                && getProcesso().getMeioTramitacao().getGeradorAutomatico()){
            processarGeradorNumeroSeNaturezaAcaoEstaVinculada();
        }
    }

    private void processarGeradorNumeroSeNaturezaAcaoEstaVinculada() {
        MeioTramitacao meioTramitacao = getProcesso().getMeioTramitacao();

        if(getProcesso().isPermiteCadastroComGeracaoAutomatica()){
            int tamanhoNumero = Strings.isNullOrEmpty(meioTramitacao.getMascara()) ? 14 : meioTramitacao.getMascara().replace("[-.;,:!?/]", "").length();
            getProcesso().setDescricao(Util.randomGenerateNumericValue(tamanhoNumero));
        } else {
            getProcesso().setDescricao(null);
        }
        PrimeFaces.current().ajax().update("processoDescricaoGroup");
    }

    private void carregaAbaHonorarios() {
        this.contratos = contratoPessoaSessioBean.findByContratosVigentes( getStateManager().getEmpresaSelecionada().getId());
    }

    private void initCentroCusto() {
        this.novoCentroCusto = new ProcessoPessoaJuridicaCentroCusto(getStateManager().getEmpresaSelecionada().getId());
    }

    public String cancelarProcesso(){
        getNovaParte().getSelectedAdvogados().clear();
        return menuController.goToProcessos();
    }

    /**
     *
     * @param step
     */
    public void stepSelect(String step){
        if(Strings.isNullOrEmpty(step)){
            return;
        }
        this.stepClick = step;
    }

    public void stepNext(final boolean possuiAcessoHonorarios) {
        switch (stepClick){
            case "STEP_PARTES" :
                this.processoStepSelecionado = ProcessoStepsTypes.fromStep(STEP_TYPES[1]);
                this.stepClick = STEP_TYPES[1];
                break;
            case "STEP_DADOS_PROCESSO" :
                this.processoStepSelecionado = ProcessoStepsTypes.fromStep(STEP_TYPES[2]);
                this.stepClick = STEP_TYPES[2];
                break;
            case "STEP_RESUMO_ACAO" :
                this.processoStepSelecionado = ProcessoStepsTypes.fromStep(STEP_TYPES[3]);
                this.stepClick = STEP_TYPES[3];
                break;
            case "STEP_VALORES" :
                if(possuiAcessoHonorarios) {
                    this.processoStepSelecionado = ProcessoStepsTypes.fromStep(STEP_TYPES[4]);
                    this.stepClick = STEP_TYPES[4];
                    break;
                }
                this.processoStepSelecionado = ProcessoStepsTypes.fromStep(STEP_TYPES[5]);
                this.stepClick = STEP_TYPES[5];
                break;
            case "STEP_HONORARIOS" :
                this.processoStepSelecionado = ProcessoStepsTypes.fromStep(STEP_TYPES[5]);
                this.stepClick = STEP_TYPES[5];
                break;
            default:
                this.processoStepSelecionado = ProcessoStepsTypes.fromStep(STEP_TYPES[0]);
                this.stepClick = STEP_TYPES[0];
        }

    }

    public String adicionarValorParcelaHonorario(){
        if (this.parcelaHonorario == null) {
            showMessageInDialog(FacesMessage.SEVERITY_ERROR, "Descrição, Valor e Data de Vencimento", getMessage("fields.form.required.message"));
            return null;
        }
        preencheDataOuDescricaoVencimento();

        if (this.parcelaHonorario != null
                && !Strings.isNullOrEmpty(this.parcelaHonorario.getDescricaoParcela())
                && this.parcelaHonorario.getValorParcela() != null
                && (this.parcelaHonorario.getDataVencimento() != null || !Strings.isNullOrEmpty(this.parcelaHonorario.getDescricaoVencimento()))) {
            this.parcelaHonorario.setProcessoHonorario(getProcesso().getProcessoHonorario());
            getProcesso().getProcessoHonorario().addParcelaValorHonorario(this.parcelaHonorario);
        } else {
            showMessageInDialog(FacesMessage.SEVERITY_ERROR, "Descrição, Valor e Data de Vencimento", getMessage("fields.form.required.message"));
        }
        this.parcelaHonorario = new ParcelaHonorario(getStateManager().getEmpresaSelecionada().getId());
        return null;
    }

    private void preencheDataOuDescricaoVencimento() {
        if(this.parcelaHonorario.getFlagDataFimAcao() != null && this.parcelaHonorario.getFlagDataFimAcao()) {
            this.parcelaHonorario.setDescricaoVencimento(FIM_ACAO);
        } else {
            this.parcelaHonorario.setDescricaoVencimento("");
        }
    }

    @Override
    protected Processo instanciarObjetoNovo() {
        return new Processo();
    }

    @Override
    protected void setEmpresaDeUsuarioLogado(Processo processo) {
        processo.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
    }

    @Override
    protected void antesDeSalvar() throws CrudException {
        adicionaDataUsuarioCadastro();
        getProcesso().setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
        getProcesso().setValorPedido(getProcesso().getTotalValorPedido());
        getProcesso().setValorProvavel(getProcesso().getTotalValorProvavel());
    }

    @Override
    protected void aposSalvar() throws CrudException {
        this.processoStepSelecionado = ProcessoStepsTypes.fromStep(STEP_TYPES[0]);
    }

    private void adicionaDataUsuarioCadastro() {
        if (getProcesso().getId() == null || getProcesso().getId() == 0) {
            getProcesso().setDataCadastro(new Date());
            getProcesso().setUsuarioCadastro(getUser().getLogin());
        }
    }

    /**
     * Utilizando em componente
     *
     * @return
     */
    public String adicionaCentrosCustos(){
        if (this.novoCentroCusto != null
                && this.novoCentroCusto.getCentroCusto() != null
                && this.novoCentroCusto.getPercentual() != null) {
            ProcessoPessoaJuridicaCentroCusto processoPessoaJuridicaCentroCusto =
                    new ProcessoPessoaJuridicaCentroCusto(novaParte.getParte(), getProcesso(), novoCentroCusto.getCentroCusto(), novoCentroCusto.getPercentual(), getStateManager().getEmpresaSelecionada().getId());
            novaParte.getParte().addCentroCustos(processoPessoaJuridicaCentroCusto);
            initCentroCusto();
        } else {
            showMessageInDialog(FacesMessage.SEVERITY_ERROR, "Centro de Custo e Percentual", getMessage("fields.form.required.message"));
        }
        return null;
    }

    @Override
    protected void validar() throws ValidationException {
        getProcesso().validarProcessoExistente(isExisteProcessoCadastrado());
        getProcesso().validarDuasPartes();
        getProcesso().validarPartesConcorrentes(getUser());
        getProcesso().validarDadosProcesso();
        getProcesso().validarResumoAcao();
        getProcesso().validarValoresProcesso();
        getProcesso().validarHonorarios();
    }

    private boolean isExisteProcessoCadastrado(){
        List<Processo> processos = processoSessionBean.findByDescricao(getProcesso().getId(), getProcesso().getDescricao(), getStateManager().getEmpresaSelecionada().getId());
        return !processos.isEmpty();
    }

    private void createParte(ParteDto parteDto) {
        parteDto.getParte().setProcesso(getProcesso());
        parteDto.getParte().setPessoa(parteDto.getPessoa());
        parteDto.getParte().setPosicao(parteDto.getPosicao());
        parteDto.getParte().setGrupo(parteDto.getGrupo());
        parteDto.getParte().setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
        parteDto.getParte().setClienteAtoProcesso(parteDto.getPessoa().isClienteEmpresa());
        Parte parte = parteDto.getParte();

        if(parte.getGrupo() != null) {
            for (GrupoAdvogado advogado : parte.getGrupo().getGruposAdvogados()) {
                ParteAdvogado parteAdvogado = new ParteAdvogado(advogado.getAdvogadoPessoa().getPessoa(), parte, getStateManager().getEmpresaSelecionada().getId());
                Integer rating = this.advogadosRating.get(advogado.getAdvogadoPessoa());
                parteAdvogado.setRatingPrincipal(rating);
                parte.addParteAdvogados(parteAdvogado);
            }
            getProcesso().addParte(parte);
            advogadosRating.clear();
            return;
        }

        for (PessoaFisica pessoaFisica : parteDto.getSelectedAdvogados()) {
            ParteAdvogado parteAdvogado = new ParteAdvogado(pessoaFisica.getPessoa(), parte, getStateManager().getEmpresaSelecionada().getId());
            Integer rating = advogadosRating.get(pessoaFisica);
            parteAdvogado.setRatingPrincipal(rating);
            parte.addParteAdvogados(parteAdvogado);
        }
        getProcesso().addParte(parte);
        advogadosRating.clear();
    }

    @Override
    protected void create(Processo objetoSelecionado) throws ValidationException, CrudException {
        processoSessionBean.create(objetoSelecionado);
    }

    @Override
    protected void navigationToDetalhes() {
        DetalheProcessoController detalheProcessoController = getFacesContext().getApplication().evaluateExpressionGet(getFacesContext(), "#{detalheProcessoController}", DetalheProcessoController.class);
        detalheProcessoController.setProcesso(getProcesso());
        detalheProcessoController.setProcessoSelecionado(processoSessionBean.findByProcessoAndDataCadastro(getProcesso().getDescricao(), getProcesso().getDataCadastro()));
        detalheProcessoController.carregaProcessoSelecionado();
        getFacesContext().getApplication().getNavigationHandler().handleNavigation(getFacesContext(), null, "/pages/processos/processo-detail.xhtml?faces-redirect=true");
    }

    public String adicionarPedido(){
        if (this.pedidoSelecionado != null
                && this.pedidoSelecionado.getPedido() != null
                && this.pedidoSelecionado.getCausaPedido() != null
                && this.pedidoSelecionado.getValorProvavel() != null
                && this.pedidoSelecionado.getValorPedido() != null) {

            this.pedidoSelecionado.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
            getProcesso().addPedido(this.pedidoSelecionado);
            initPedido();
        } else {
            showMessageInDialog(FacesMessage.SEVERITY_ERROR, "Pedido, Causa do Pedido, Valor Prov\u00e1vel e Valor do Pedido", getMessage("fields.form.required.message"));
        }
        return null;
    }

    private void initPedido() {
        this.pedidoSelecionado = new ProcessoPedido(getStateManager().getEmpresaSelecionada().getId());
    }

    private void initObjetoCollectors(Processo processo) {
        initPedido();
        this.pedidoSelecionado.setProcesso(processo);
        this.novoCentroCusto = new ProcessoPessoaJuridicaCentroCusto(getStateManager().getEmpresaSelecionada().getId());
    }

    public NumeroVara getVaraUnica() {
        return numeroVaraSessionBean.findByVaraUnica();
    }

    private void carregaAbaValores(){
        initPedido();
    }

    /**
     *
     * @return
     */
    public String goToCadastroProcessos() {
        novo();
        initialize();
        return PAGE_FORM;
    }

    public String goToEditar(){
        Processo processoSelecionado = processoSessionBean.findById(processoEditId, getStateManager().getEmpresaSelecionada().getId());
        setProcesso(processoSelecionado);
        initObjetoCollectors(getProcesso());
        initialize();
        return PAGE_FORM;
    }

    public void carregarCentroCustosListener(){
        PessoaJuridica pessoaJuridica = pessoaJuridicaSessionBean.retrieve(novaParte.getPessoaJuridica().getId());
        novaParte.setPessoaJuridica(pessoaJuridica);
        initCentroCusto();
    }

    private void carregarAbaPartes() {
        this.grupos = grupoSessionBean.retrieveAll(getStateManager().getEmpresaSelecionada().getId());
        initPartes();
    }

    private void carregaStatusProcesso() {
        this.statusProcessoItems = Lists.newArrayList();
        for (StatusProcessoIndicador status : StatusProcessoIndicador.values()) {
            this.statusProcessoItems.add(status);
        }
    }

    protected void initPartes() {
        this.novaParte = new ParteDto();
        advogados.clear();
        carregaListaAdvogados();
    }

    public List<String> carregaTooltip(Pessoa pessoa){
        if(pessoa == null || !pessoa.isClienteEmpresa()) {
            this.tooltipCliente = Boolean.FALSE;
            return null;
        }

        this.tooltipCliente = Boolean.TRUE;
        List<String> results = Lists.newArrayList();
        results.add(pessoa.getHistoricoClienteList().isEmpty() ? "" : DateUtils.formataDataBrasil(pessoa.getHistoricoClienteList().get(pessoa.getHistoricoClienteList().size()-1).getDataAlteracao()));
        results.add(pessoa.getPessoasContatos().isEmpty() ? "" : pessoa.getPessoasContatos().get(pessoa.getPessoasContatos().size()-1).getTelefones(pessoa));
        results.add(pessoa.getPessoasEmails().isEmpty() ? "" : pessoa.getPessoasEmails().get(pessoa.getPessoasEmails().size()-1).getEmails(pessoa));
        return results;
    }

    public String clienteTooltip(Pessoa pessoa){
        List<String> results = carregaTooltip(pessoa);
        if(results.isEmpty()) {
            return "";
        }
        return results.size() > 0 && Strings.isNullOrEmpty(results.get(0)) ? "" : results.get(0);
    }

    public String telefoneTooltip(Pessoa pessoa){
        List<String> results = carregaTooltip(pessoa);
        if(results.isEmpty()) {
            return "";
        }
        return results.size() > 1 && Strings.isNullOrEmpty(results.get(1)) ? "" : results.get(1);
    }

    public String emailTooltip(Pessoa pessoa){
        List<String> results = carregaTooltip(pessoa);
        if(results.isEmpty()) {
            return "";
        }
        return results.size() > 2 && Strings.isNullOrEmpty(results.get(2)) ? "" : results.get(2);
    }

    public void adicionarParte() {
        try {
            if (!camposObrigatoriosValidos()) {
                RequestContext.getCurrentInstance().update(":processoForm:messages pessoaGroup partes @parent");
                return;
            }

            if(novaParte.isTipoPessoaFisica()) {
                novaParte.getParte().getCentroCustos().clear();
            }
            getProcesso().validarParte(novaParte);
            createParte(novaParte);
            initPartes();
            verificaExistePartesConcorrentes();

        } catch (ValidationException e) {
            exibirMensagem(FacesMessage.SEVERITY_ERROR, "Favor preencher o(s) campo(s) obrigatório(s):");
            for (String message : e.getMessages()) {
                exibirMensagem(FacesMessage.SEVERITY_ERROR, message);
            }
        }
    }

    private void verificaExistePartesConcorrentes() {
        if (getProcesso().isExistePartesConcorrentes()) {
            showMessageInDialog(FacesMessage.SEVERITY_ERROR, "Aviso", "Existe mais de uma parte cliente informada.");
        }
    }

    public boolean camposObrigatoriosValidos() {
        if(novaParte.getPessoa() == null) {
            showMessageInDialog(FacesMessage.SEVERITY_ERROR, "Nome", getMessage("fields.form.required.message"));
            return false;
        }
        if (novaParte.isTipoAdvogadoIndividual() && !novaParte.getPessoa().isClienteEmpresa()){
            return true;
        }

        if (novaParte.isTipoAdvogadoIndividual() && camposAdvogadoIndividualIncompletos()) {
            showMessageInDialog(FacesMessage.SEVERITY_ERROR, "Posição e Advogados", getMessage("fields.form.required.message"));
            return false;
        } else if (!novaParte.isTipoAdvogadoIndividual() && camposGrupoAdvogadosIncompletos()) {
            showMessageInDialog(FacesMessage.SEVERITY_ERROR, "Posição e Grupo de Advogados", getMessage("fields.form.required.message"));
            return false;
        }
        return true;
    }

    private boolean camposGrupoAdvogadosIncompletos() {
        return novaParte.getPessoa() == null || novaParte.getPosicao() == null || novaParte.getGrupo() == null;
    }

    private boolean camposAdvogadoIndividualIncompletos() {
        return novaParte.getPessoa() == null || novaParte.getPosicao() == null || novaParte.getSelectedAdvogados().isEmpty();
    }

    public void onItemPfSelect(SelectEvent event) {
        PessoaFisica pessoaFisica = (PessoaFisica) event.getObject();
        this.novaParte.setPessoaFisica(pessoaFisica);
    }

    public void onItemPjSelect(SelectEvent event) {
        PessoaJuridica pessoaJuridica = (PessoaJuridica) event.getObject();
        this.novaParte.setPessoaJuridica(pessoaJuridica);
    }

    public void pfChanged(ValueChangeEvent event){
        novaParte.setPessoaFisica((PessoaFisica) event.getNewValue());
    }

    public void pjChanged(ValueChangeEvent event){
        PessoaJuridica pessoaJuridicaSelecionada = (PessoaJuridica) event.getNewValue();
        novaParte.setPessoaJuridica(pessoaJuridicaSelecionada);
    }

    public void carregaListaAdvogados(){
        this.novaParte.getSelectedAdvogados().clear();
        this.advogados = pessoaFisicaSessionBean.getAdvogados(this.novaParte.isAdvogadoInterno(), getStateManager().getEmpresaSelecionada().getId());
    }

    public void onrate(RateEvent rateEvent) {
        Map<String, String> params = getFacesContext().getExternalContext().getRequestParameterMap();
        Long idAdvogado = Long.parseLong(params.get("selectedAdvogado"));

        if(idAdvogado == null){
            return;
        }

        PessoaFisica pessoaFisica = pessoaFisicaSessionBean.retrieve(idAdvogado);
        this.advogadosRating.put(pessoaFisica, ((Integer) rateEvent.getRating()).intValue());
    }

    public void oncancel(PessoaFisica pessoaFisica) {
        this.advogadosRating.remove(pessoaFisica);
    }

    public boolean isPermiteInclusao() {
        return permiteInclusao;
    }

    private void carregaAbaDadosProcesso(){
        this.numeroList = numeroVaraSessionBean.findAll();
    }

    public ParteDto getNovaParte() {
        return novaParte;
    }

    public void setNovaParte(ParteDto novaParte) {
        this.novaParte = novaParte;
    }

    public List<PessoaFisica> getAdvogados() {
        return advogados;
    }

    public void setAdvogados(List<PessoaFisica> advogados) {
        this.advogados = advogados;
    }

    public List<Grupo> getGrupos() {
        return grupos;
    }

    public void setGrupos(List<Grupo> grupos) {
        this.grupos = grupos;
    }

    public int getRowCountCentroCusto() {
        return rowCountCentroCusto;
    }

    public void setRowCountCentroCusto(int rowCountCentroCusto) {
        this.rowCountCentroCusto = rowCountCentroCusto;
    }

    public List<NumeroVara> getNumeroList() {
        return numeroList;
    }

    public void setNumeroList(List<NumeroVara> numeroList) {
        this.numeroList = numeroList;
    }

    public ProcessoPedido getPedidoSelecionado() {
        return pedidoSelecionado;
    }

    public void setPedidoSelecionado(ProcessoPedido pedidoSelecionado) {
        this.pedidoSelecionado = pedidoSelecionado;
    }

    public List<StatusProcessoIndicador> getStatusProcessoItems() {
        return statusProcessoItems;
    }

    public void setStatusProcessoItems(List<StatusProcessoIndicador> statusProcessoItems) {
        this.statusProcessoItems = statusProcessoItems;
    }

    public ProcessoPessoaJuridicaCentroCusto getNovoCentroCusto() {
        return novoCentroCusto;
    }

    public void setNovoCentroCusto(ProcessoPessoaJuridicaCentroCusto novoCentroCusto) {
        this.novoCentroCusto = novoCentroCusto;
    }

    public boolean isCheckDataFimAcaoPercentual() {
        return checkDataFimAcaoPercentual;
    }

    public void setCheckDataFimAcaoPercentual(boolean checkDataFimAcaoPercentual) {
        this.checkDataFimAcaoPercentual = checkDataFimAcaoPercentual;
    }

    public boolean isHasAccessAbaHonorarios(){
        return possuiAcesso(PROCESSOS_CADASTRO_HONORARIOS);
    }

    public boolean isCheckPagoPercentual() {
        return checkPagoPercentual;
    }

    public void setCheckPagoPercentual(boolean checkPagoPercentual) {
        this.checkPagoPercentual = checkPagoPercentual;
    }

    public List<ContratoPessoa> getContratos() {
        return contratos;
    }

    public void setContratos(List<ContratoPessoa> contratos) {
        this.contratos = contratos;
    }

    public ParcelaHonorario getParcelaHonorario() {
        return parcelaHonorario;
    }

    public void setParcelaHonorario(ParcelaHonorario parcelaHonorario) {
        this.parcelaHonorario = parcelaHonorario;
    }

    public Long getProcessoEditId() {
        return processoEditId;
    }

    public void setProcessoEditId(Long processoEditId) {
        this.processoEditId = processoEditId;
    }

    public ProcessoStepsTypes getProcessoStepSelecionado() {
        return processoStepSelecionado;
    }

    public String getStepClick() {
        return stepClick;
    }

    public boolean isTooltipCliente() {
        return tooltipCliente;
    }

    public void salvarMeioTramitacao(){
        try {
            getMeioTramitacao().setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
            boolean jaCadastrada = meioTramitacaoSessionBean.isDescricaoJaCadastrada(getMeioTramitacao(), getStateManager().getEmpresaSelecionada().getId());
            getMeioTramitacao().validar(jaCadastrada);
            meioTramitacaoSessionBean.create(getMeioTramitacao());
            getProcesso().setMeioTramitacao(getMeioTramitacao());
            exibirMensagem(FacesMessage.SEVERITY_INFO, getMessage("msg_sucesso_salvar"));
        } catch (ValidationException e) {
            LOGGER.info(e.getCause(), e);
            for (String message : e.getMessages()) {
                exibirMensagem(FacesMessage.SEVERITY_ERROR, message);
            }
        }
    }

    public void salvarFase(){
        try {
            getFase().setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
            boolean jaCadastrada = faseSessionBean.isDescricaoJaCadastrada(getFase(), getStateManager().getEmpresaSelecionada().getId());
            getFase().validar(jaCadastrada);
            faseSessionBean.create(getFase());
            getProcesso().setFase(getFase());
            exibirMensagem(FacesMessage.SEVERITY_INFO, getMessage("msg_sucesso_salvar"));
        } catch (ValidationException e) {
            LOGGER.info(e.getCause(), e);
            for (String message : e.getMessages()) {
                exibirMensagem(FacesMessage.SEVERITY_ERROR, message);
            }
        }
    }

    public void salvarClasseProcessual(){
        try {
            getClasseProcessual().setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
            boolean jaCadastrada = classeProcessualSessionBean.isDescricaoJaCadastrada(getClasseProcessual(), getStateManager().getEmpresaSelecionada().getId());
            getClasseProcessual().validar(jaCadastrada);
            classeProcessualSessionBean.create(getClasseProcessual());
            getProcesso().setClasseProcessual(getClasseProcessual());
            exibirMensagem(FacesMessage.SEVERITY_INFO, getMessage("msg_sucesso_salvar"));
        } catch (ValidationException e) {
            LOGGER.info(e.getCause(), e);
            for (String message : e.getMessages()) {
                exibirMensagem(FacesMessage.SEVERITY_ERROR, message);
            }
        }
    }

    public void salvarNaturezaAcao(){
        try {
            getNaturezaAcao().setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
            boolean jaCadastrada = naturezaAcaoSessionBean.isDescricaoJaCadastrada(getNaturezaAcao(), getStateManager().getEmpresaSelecionada().getId());
            getNaturezaAcao().validar(jaCadastrada);
            naturezaAcaoSessionBean.create(getNaturezaAcao());
            getProcesso().setNaturezaAcao(getNaturezaAcao());
            exibirMensagem(FacesMessage.SEVERITY_INFO, getMessage("msg_sucesso_salvar"));
        } catch (ValidationException e) {
            LOGGER.info(e.getCause(), e);
            for (String message : e.getMessages()) {
                exibirMensagem(FacesMessage.SEVERITY_ERROR, message);
            }
        }
    }

    public void salvarForum(){
        try {
            getForum().setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
            boolean jaCadastrada = forumSessionBean.isDescricaoJaCadastrada(getForum(), getStateManager().getEmpresaSelecionada().getId());
            getForum().validar(jaCadastrada);
            forumSessionBean.create(getForum());
            getProcesso().setForum(getForum());
            exibirMensagem(FacesMessage.SEVERITY_INFO, getMessage("msg_sucesso_salvar"));
        } catch (ValidationException e) {
            LOGGER.info(e.getCause(), e);
            for (String message : e.getMessages()) {
                exibirMensagem(FacesMessage.SEVERITY_ERROR, message);
            }
        }
    }

    public void salvarVara(){
        try {
            getVara().setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
            boolean jaCadastrada = varaSessionBean.isDescricaoJaCadastrada(getVara(), getStateManager().getEmpresaSelecionada().getId());
            getVara().validar(jaCadastrada);
            varaSessionBean.create(getVara());
            getProcesso().setVara(getVara());
            exibirMensagem(FacesMessage.SEVERITY_INFO, getMessage("msg_sucesso_salvar"));
        } catch (ValidationException e) {
            LOGGER.info(e.getCause(), e);
            for (String message : e.getMessages()) {
                exibirMensagem(FacesMessage.SEVERITY_ERROR, message);
            }
        }
    }

    public void comarcaUfChanged(ValueChangeEvent event){
        getComarca().setUf((Uf) event.getNewValue());
    }

    public void salvarComarca() {
        try {
            getComarca().setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
            boolean jaCadastrada = comarcaSessionBean.isDescricaoJaCadastrada(getComarca(), getStateManager().getEmpresaSelecionada().getId());
            getComarca().validar(jaCadastrada);
            comarcaSessionBean.create(getComarca());
            getProcesso().setComarca(getComarca());
            exibirMensagem(FacesMessage.SEVERITY_INFO, getMessage("msg_sucesso_salvar"));
        } catch (ValidationException e) {
            LOGGER.info(e.getCause(), e);
            for (String message : e.getMessages()) {
                exibirMensagem(FacesMessage.SEVERITY_ERROR, message);
            }
        }
    }

    public void salvarPedido(){
        try {
            getPedido().setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
            boolean jaCadastrada = pedidoSessionBean.isDescricaoJaCadastrada(getPedido(), getStateManager().getEmpresaSelecionada().getId());
            getPedido().validar(jaCadastrada);
            pedidoSessionBean.create(getPedido());
            pedidoSelecionado.setPedido(getPedido());
            exibirMensagem(FacesMessage.SEVERITY_INFO, getMessage("msg_sucesso_salvar"));
        } catch (ValidationException e) {
            LOGGER.info(e.getCause(), e);
            for (String message : e.getMessages()) {
                exibirMensagem(FacesMessage.SEVERITY_ERROR, message);
            }
        }
    }

    public void salvarCausaPedido(){
        try {
            getCausaPedido().setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
            boolean jaCadastrada = causaPedidoSessionBean.isDescricaoJaCadastrada(getCausaPedido(), getStateManager().getEmpresaSelecionada().getId());
            getCausaPedido().validar(jaCadastrada);
            causaPedidoSessionBean.create(getCausaPedido());
            pedidoSelecionado.setCausaPedido(getCausaPedido());
            exibirMensagem(FacesMessage.SEVERITY_INFO, getMessage("msg_sucesso_salvar"));
        } catch (ValidationException e) {
            LOGGER.info(e.getCause(), e);
            for (String message : e.getMessages()) {
                exibirMensagem(FacesMessage.SEVERITY_ERROR, message);
            }
        }
    }

    public void salvarGrauRisco(){
        try {
            getGrauRisco().setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
            boolean jaCadastrada = grauRiscoSessionBean.isDescricaoJaCadastrada(getGrauRisco(), getStateManager().getEmpresaSelecionada().getId());
            getGrauRisco().validar(jaCadastrada);
            grauRiscoSessionBean.create(getGrauRisco());
            getProcesso().setGrauRisco(getGrauRisco());
            exibirMensagem(FacesMessage.SEVERITY_INFO, getMessage("msg_sucesso_salvar"));
        } catch (ValidationException e) {
            LOGGER.info(e.getCause(), e);
            for (String message : e.getMessages()) {
                exibirMensagem(FacesMessage.SEVERITY_ERROR, message);
            }
        }
    }

    public boolean isDesabilitaSelecaoAdvogados(){
        boolean processoFinalizado = getProcesso() != null && getProcesso().isProcessoFinalizado();
        return processoFinalizado
                || !TIPO_ADVOGADO_INDIVIDUAL.equals(getNovaParte().getTipoAdvogado())
                || getNovaParte().getGrupo() != null && !getNovaParte().isAdvogadoInterno();
    }

    public void grupoChanged(ValueChangeEvent event){
        Grupo grupo = (Grupo) event.getNewValue();
        getNovaParte().getSelectedAdvogados().clear();
        if(grupo == null) {
            return;
        }
        for (GrupoAdvogado grupoAdvogado : grupo.getGruposAdvogados()) {
            grupoAdvogado.getAdvogadoPessoa().setRatingPrincipal(null);
            getNovaParte().getSelectedAdvogados().add(grupoAdvogado.getAdvogadoPessoa());
        }
    }

    public void advogadoGrupoChanged(ValueChangeEvent event){
        String tipoAdvogado = (String) event.getNewValue();
        if(TIPO_ADVOGADO_INDIVIDUAL.equals(tipoAdvogado)){
            getNovaParte().getSelectedAdvogados().clear();
        }
    }

    public void imprimirProcesso() throws StproException {
        FacesContext context = FacesContext.getCurrentInstance();
        ServletContext servletContext = (ServletContext) context.getExternalContext().getContext();
        String path = servletContext.getRealPath("resources" + File.separator + "reports" + File.separator);
        String[] relatorios = new String[]{"processo-report"};
        Processo processoSelecionado = processoSessionBean.findById(processoEditId, getStateManager().getEmpresaSelecionada().getId());
        List<Processo> list = Lists.newArrayList(processoSelecionado);
        Map<String, Object> parameters = carregaReportParameters(processoSelecionado);

        ExportUtil.gerarPdfComSubreport(relatorios, list, path, "relatorio_processos", parameters);


    }

    private Map<String, Object> carregaReportParameters(Processo processoSelecionado) {
        Map<String, Object> parameters = Maps.newHashMap();
        parameters.put("PROCESSO", processoSelecionado.getDescricao());
        parameters.put("CLASSE_PROCESSUAL", processoSelecionado.getClasseProcessual().getDescricao());
        parameters.put("FORUM", processoSelecionado.getForum().getDescricao());
        parameters.put("VARA", processoSelecionado.getVara().getDescricao());
        parameters.put("DATA_ABERTURA", DateUtils.formataDataBrasil(processoSelecionado.getDataAbertura()));
        parameters.put("NATUREZA_ACAO", processoSelecionado.getNaturezaAcao().getDescricao());
        parameters.put("COMARCA", processoSelecionado.getComarca().getDescricao());
        parameters.put("NUMERACAO_SEC", processoSelecionado.getNumeracaoSecundaria());
        parameters.put("STATUS", processoSelecionado.getDescricaoStatus());
        parameters.put("MEIO_TRAMITACAO", processoSelecionado.getMeioTramitacao().getDescricao());
        parameters.put("FASE_PROCESSO", processoSelecionado.getFase().getDescricao());
        parameters.put("DATA_CITACAO", DateUtils.formataDataBrasil(processoSelecionado.getDataCitacao()));
        return parameters;
    }

}
