package br.com.juridico.controller;

import br.com.juridico.datamodel.LazyEntityDataModel;
import br.com.juridico.entidades.*;
import br.com.juridico.exception.CrudException;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.impl.*;
import br.com.juridico.util.DownloadUtil;
import br.com.juridico.util.FileUtil;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.primefaces.model.LazyDataModel;

import javax.annotation.PostConstruct;
import javax.faces.event.ValueChangeEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.Part;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Jefferson on 21/05/2016.
 */
@Named
@ViewScoped
public class ContratoController extends CrudController<ContratoPessoa> {

    /**
     *
     */
    private static final long serialVersionUID = 2344883510689740425L;

    private static final Logger LOGGER = Logger.getLogger(ContratoController.class);

    private static final String PESSOA_FISICA = "PF";
    private static final String PESSOA_JURIDICA = "PJ";
    private static final String ATOS_GROUP = "atosGroup";
    private static final String ACOMPANHAMENTO_MENSAL_GROUP = "acompMensalGroup";
    private static final String CONTRATO_FORM = "contratoForm";
    private static final String ANEXOS = "anexos";
    private static final String CHAVE_ACESSO = "CONTRATO_CONTRATO_TEMPLATE";
    private static final String PAGINA_FORMULARIO = "/pages/cadastros/contrato/contrato-template.xhtml?faces-redirect=true";

    @Inject
    private ContratoPessoaSessioBean contratoPessoaSessioBean;
    @Inject
    private TipoAndamentoSessionBean tipoAndamentoSessionBean;
    @Inject
    private ContratoDocumentoSessioBean contratoDocumentoSessioBean;
    @Inject
    private PerfilAcessoSessionBean perfilAcessoSessionBean;
    @Inject
    private DireitoSessionBean direitoSessionBean;
    @Inject
    private PessoaFisicaSessionBean pessoaFisicaSessionBean;
    @Inject
    private PessoaJuridicaSessionBean pessoaJuridicaSessionBean;

    private List<TipoAndamento> tipoAndamentos;

    private ContratoPessoaVigencia vigencia;
    private ContratoPessoaTipoAndamento contratoPessoaTipoAndamento;

    private Part arquivo;

    private String tipoPessoa;
    private boolean anexoSelected;
    private int rowCount;
    private int rowAnexoNumber;

    /**
     * @PostConstruct
     */
    @PostConstruct
    public void initialize() {
        this.contratoPessoaTipoAndamento = new ContratoPessoaTipoAndamento(getStateManager().getEmpresaSelecionada().getId());
        this.anexoSelected = false;
        LazyDataModel<ContratoPessoa> listFlash = (LazyDataModel<ContratoPessoa>) getFlashScope().get(LIST_FLASH);

        if(listFlash != null) {
            setLazyDataModel(listFlash);
            return;
        }
        super.buscarPorLazy();
    }

    @Override
    protected void antesInserir() throws CrudException {
        initVigencia();
        this.tipoPessoa = PESSOA_FISICA;
        getObjetoSelecionado().setAtos(false);
        getObjetoSelecionado().setAcompanhamentoMensal(false);
        this.tipoAndamentos = tipoAndamentoSessionBean.retrieveAll(getStateManager().getEmpresaSelecionada().getId());
        super.antesInserir();
    }

    @Override
    protected void aposCancelar() throws CrudException {
        getObjetoSelecionado().setDescricao(null);
        updateContext(CONTRATO_FORM);
        super.aposCancelar();
    }


    @Override
    protected void create(ContratoPessoa contratoPessoa) throws ValidationException, CrudException {
        ContratoPessoa contratoExclusao = getRegistroComExclusaoLogica();
        if (isEditavel() && contratoExclusao != null) {
            contratoExclusao.setDataExclusao(null);
            contratoPessoaSessioBean.update(contratoExclusao);
        } else if (contratoPessoa != null && contratoPessoa.getDescricao() != null
                && !contratoPessoa.getDescricao().isEmpty()) {
            contratoPessoaSessioBean.create(contratoPessoa);
        } else {
            throw new CrudException();
        }
    }

    @Override
    protected ContratoPessoa inicializaObjetosLazy(ContratoPessoa objetoSelecionado) {
        contratoPessoaSessioBean.clearSession();
        return contratoPessoaSessioBean.retrieve(objetoSelecionado.getId());
    }

    @Override
    protected void validar() throws ValidationException {
        if(this.vigencia == null || tipoPessoa == null) {
            throw new ValidationException("Vigencia ou Tipo de pessoa nao podem estar nulos.");
        }
        getObjetoSelecionado().validar(vigencia, tipoPessoa);
        vigencia.setContratoPessoa(getObjetoSelecionado());
        getObjetoSelecionado().addVigencia(vigencia);
    }

    @Override
    protected ContratoPessoa instanciarObjetoNovo() {
        return new ContratoPessoa();
    }

    @Override
    protected void setEmpresaDeUsuarioLogado(ContratoPessoa objetoSelecionado) {
        objetoSelecionado.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
    }

    @Override
    protected void antesDeExcluir() throws CrudException {
        getObjetoSelecionado().setDataExclusao(new Date());
    }

    @Override
    protected void aposExcluir() throws CrudException {
        super.buscarPorLazy();
    }

    @Override
    protected void carregaLazyDataModel() {
        setLazyDataModel(new LazyEntityDataModel<ContratoPessoa>() {
            @Override
            protected List<ContratoPessoa> consultaFiltrada(ConsultaLazyFilter filtro) {
                filtro.setDescricao(getDescricaoFiltro());
                filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
                return contratoPessoaSessioBean.filtrados(filtro);
            }

            @Override
            protected int countTotalRegistros(ConsultaLazyFilter filtro) {
                filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
                return contratoPessoaSessioBean.quantidadeFiltrados(filtro);
            }
        });
    }

    @Override
    protected void aposSalvar() throws CrudException {
        initVigencia();
        this.tipoPessoa = PESSOA_FISICA;
        super.aposSalvar();
    }

    @Override
    protected void antesDeSalvar() throws CrudException {
        if (!existeAtoPreenchido()) {
            this.vigencia.getAndamentoAtos().clear();
        }
    }

    @Override
    protected void antesDeEditar(ContratoPessoa contrato) throws CrudException {
        this.tipoAndamentos = tipoAndamentoSessionBean.retrieveAll(getStateManager().getEmpresaSelecionada().getId());
        this.tipoPessoa = contrato.getPessoa().isPessoaFisica() ? PESSOA_FISICA : PESSOA_JURIDICA;
        getObjetoSelecionado().setAtos(false);
        initVigencia();
        carregaPessoas(contrato);
    }

    private void carregaPessoas(ContratoPessoa contrato) {
        if(contrato.getPessoa().isPessoaFisica()) {
            PessoaFisica pessoaFisica = pessoaFisicaSessionBean.findByPessoa(contrato.getPessoa().getId(), getStateManager().getEmpresaSelecionada().getId());
            contrato.setPessoaFisica(pessoaFisica);
            return;
        }
        PessoaJuridica pessoaJuridica = pessoaJuridicaSessionBean.findByPessoa(contrato.getPessoa().getId(), getStateManager().getEmpresaSelecionada().getId());
        contrato.setPessoaJuridica(pessoaJuridica);
    }

    private void initVigencia() {
        this.vigencia = new ContratoPessoaVigencia();
    }

    public void anexar(){
        this.anexoSelected = true;
        updateContext(CONTRATO_FORM);
    }

    public void importar() {
        try {
            byte[] bytes = IOUtils.toByteArray(arquivo.getInputStream());
            String nomeArquivo = FileUtil.getFileName(arquivo);
            String extensao = nomeArquivo.substring(nomeArquivo.lastIndexOf("."), nomeArquivo.length());
            ContratoDocumento documento = new ContratoDocumento(getObjetoSelecionado(), nomeArquivo, extensao, bytes, getStateManager().getEmpresaSelecionada().getId());
            getObjetoSelecionado().addDocumentos(documento);
            updateContext(ANEXOS);
        } catch (IOException e) {
            LOGGER.error(e, e.getCause());
        }
    }

    public void concluirAnexos(){
        this.anexoSelected = false;
        updateContext(CONTRATO_FORM);
    }

    public void excluirAnexo() {
        getObjetoSelecionado().removeDocumentos(rowAnexoNumber);
        updateContext(ANEXOS);
    }

    /**
     *
     * @param contratoDocumento
     */
    public void downloadAnexo(ContratoDocumento contratoDocumento){
        DownloadUtil.downloadFile(contratoDocumento.getArquivo(), contratoDocumento.getDescricao());
    }

    private boolean existeAtoPreenchido() {
        boolean existeAto = Boolean.FALSE;

        for (Iterator<ContratoPessoaTipoAndamento> iterator = vigencia.getAndamentoAtos().iterator(); iterator.hasNext();) {
            ContratoPessoaTipoAndamento ato = iterator.next();
            if (ato.getTipoAndamento() == null || ato.getValor() == null || ato.getValor().equals(BigDecimal.ZERO)) {
                iterator.remove();
            } else {
                existeAto = Boolean.TRUE;
            }
        }
        return existeAto;
    }

    /**
     *
     * @param vigencia
     */
    public void editarVigencia(ContratoPessoaVigencia vigencia) {
        setVigencia(vigencia);
        vigencia.addRowNumberAndamentos();
        this.rowCount = vigencia.getAndamentoAtos().size();
        getObjetoSelecionado().setAtos(!vigencia.getAndamentoAtos().isEmpty() ? true : false);
        updateContext(ATOS_GROUP);
    }

    /**
     *
     * @param event
     */
    public void pfChanged(ValueChangeEvent event) {
        PessoaFisica pessoaFisica = (PessoaFisica) event.getNewValue();
        getObjetoSelecionado().setPessoa(pessoaFisica.getPessoa());
    }

    /**
     *
     * @param event
     */
    public void pjChanged(ValueChangeEvent event) {
        PessoaJuridica pessoaJuridica = (PessoaJuridica) event.getNewValue();
        getObjetoSelecionado().setPessoa(pessoaJuridica.getPessoa());
    }

    public String goToContratos() {
        getFlashScope().put(LIST_FLASH, getLazyDataModel());
        if(getObjetoSelecionado().getId() != null) {
            getFlashScope().put(CONTRATO_FORM, contratoPessoaSessioBean.retrieve(getObjetoSelecionado().getId()));
        }

        return PAGINA_FORMULARIO;
    }

    public boolean isSelectPessoaFisica() {
        return PESSOA_FISICA.equals(tipoPessoa);
    }

    private ContratoPessoa getRegistroComExclusaoLogica() {
        return contratoPessoaSessioBean.findByDescricao(getObjetoSelecionado().getDescricao(),
                getStateManager().getEmpresaSelecionada().getId());
    }

    public void atosRadioChanged(ValueChangeEvent event){
        Boolean atoSelected = (Boolean) event.getNewValue();
        getObjetoSelecionado().setAtos(atoSelected);
        if(atoSelected){
            adicionarTipoAndamento();
        }
        updateContext(ATOS_GROUP);
    }

    public void acompMensalRadioChanged(ValueChangeEvent event){
        Boolean selected = (Boolean) event.getNewValue();
        getObjetoSelecionado().setAcompanhamentoMensal(selected);
        updateContext(ACOMPANHAMENTO_MENSAL_GROUP);
    }

    public String adicionarTipoAndamento(){
        this.contratoPessoaTipoAndamento = new ContratoPessoaTipoAndamento(getStateManager().getEmpresaSelecionada().getId());
        return null;
    }

    /**
     *
     * @return
     */
    public String goToFormulario() {
        super.buscarPorLazy();
        getFlashScope().put(LIST_FLASH, getLazyDataModel());
        return "/pages/cadastros/contrato/contrato-template.xhtml?faces-redirect=true";
    }

    public String getTipoPessoa() {
        return tipoPessoa;
    }

    public void setTipoPessoa(String tipoPessoa) {
        this.tipoPessoa = tipoPessoa;
    }

    public ContratoPessoaVigencia getVigencia() {
        return vigencia;
    }

    public void setVigencia(ContratoPessoaVigencia vigencia) {
        this.vigencia = vigencia;
    }

    public int getRowCount() {
        return rowCount;
    }

    public void setRowCount(int rowCount) {
        this.rowCount = rowCount;
    }

    public List<TipoAndamento> getTipoAndamentos() {
        return tipoAndamentos;
    }

    public void setTipoAndamentos(List<TipoAndamento> tipoAndamentos) {
        this.tipoAndamentos = tipoAndamentos;
    }

    public boolean isAnexoSelected() {
        return anexoSelected;
    }

    public void setAnexoSelected(boolean anexoSelected) {
        this.anexoSelected = anexoSelected;
    }

    public Part getArquivo() {
        return arquivo;
    }

    public void setArquivo(Part arquivo) {
        this.arquivo = arquivo;
    }

    public int getRowAnexoNumber() {
        return rowAnexoNumber;
    }

    public void setRowAnexoNumber(int rowAnexoNumber) {
        this.rowAnexoNumber = rowAnexoNumber;
    }

    public ContratoPessoaTipoAndamento getContratoPessoaTipoAndamento() {
        return contratoPessoaTipoAndamento;
    }

    public void setContratoPessoaTipoAndamento(ContratoPessoaTipoAndamento contratoPessoaTipoAndamento) {
        this.contratoPessoaTipoAndamento = contratoPessoaTipoAndamento;
    }

    @Override
    protected void addChaveAcesso() {
        setChaveAcesso(CHAVE_ACESSO);
    }

    @Override
    protected boolean isPodeAlterar(){
        Direito direito = direitoSessionBean.retrieve(ALTERAR);
        return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
    }

    @Override
    protected boolean isPodeExcluir(){
        Direito direito = direitoSessionBean.retrieve(EXCLUIR);
        return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
    }

    @Override
    protected boolean isPodeIncluir(){
        Direito direito = direitoSessionBean.retrieve(INCLUIR);
        return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
    }
}
