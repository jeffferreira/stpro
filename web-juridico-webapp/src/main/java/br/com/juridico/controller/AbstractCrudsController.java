package br.com.juridico.controller;

import br.com.juridico.entidades.*;
import org.primefaces.context.RequestContext;

/**
 * Created by jefferson.ferreira on 15/05/2017.
 */
public class AbstractCrudsController extends AbstractController {

    public AbstractCrudsController(){
        this.meioTramitacao = new MeioTramitacao();
        this.fase = new Fase();
        this.classeProcessual = new ClasseProcessual();
        this.naturezaAcao = new NaturezaAcao();
        this.forum = new Forum();
        this.vara = new Vara();
        this.comarca = new Comarca();
        this.pedido = new Pedido();
        this.causaPedido = new CausaPedido();
        this.grauRisco = new GrauRisco();
    }

    private MeioTramitacao meioTramitacao;
    private Fase fase;
    private ClasseProcessual classeProcessual;
    private NaturezaAcao naturezaAcao;
    private Forum forum;
    private Comarca comarca;
    private Vara vara;
    private Pedido pedido;
    private CausaPedido causaPedido;
    private GrauRisco grauRisco;

    public void abrirMeioTramitacaoListener(){
        this.meioTramitacao = new MeioTramitacao();
        RequestContext.getCurrentInstance().execute("openModal('#meioTramitacaoModal')");

    }

    public void abrirFaseListener(){
        this.fase = new Fase();
        RequestContext.getCurrentInstance().execute("openModal('#faseModal')");
    }

    public void abrirClasseProcessualListener(){
        this.classeProcessual = new ClasseProcessual();
        RequestContext.getCurrentInstance().execute("openModal('#classeProcessualModal')");
    }

    public void abrirNaturezaAcaoListener(){
        this.naturezaAcao = new NaturezaAcao();
        RequestContext.getCurrentInstance().execute("openModal('#naturezaAcaoModal')");
    }

    public void abrirForumListener(){
        this.forum = new Forum();
        RequestContext.getCurrentInstance().execute("openModal('#forumModal')");
    }

    public void abrirComarcaListener(){
        this.comarca = new Comarca();
        RequestContext.getCurrentInstance().execute("openModal('#comarcaModal')");
    }

    public void abrirVaraListener(){
        this.vara = new Vara();
        RequestContext.getCurrentInstance().execute("openModal('#varaModal')");
    }

    public void abrirPedidoListener(){
        this.pedido = new Pedido();
        RequestContext.getCurrentInstance().execute("openModal('#pedidoModal')");
        RequestContext.getCurrentInstance().update("formularioPedido");
    }

    public void abrirCausaPedidoListener(){
        this.causaPedido = new CausaPedido();
        RequestContext.getCurrentInstance().execute("openModal('#causaPedidoModal')");
        RequestContext.getCurrentInstance().update("formularioCausaPedido");
    }

    public void abrirGrauRiscoListener(){
        this.grauRisco = new GrauRisco();
        RequestContext.getCurrentInstance().execute("openModal('#grauRiscoModal')");
        RequestContext.getCurrentInstance().update("formularioGrauRisco");
    }

    public MeioTramitacao getMeioTramitacao() {
        return meioTramitacao;
    }

    public void setMeioTramitacao(MeioTramitacao meioTramitacao) {
        this.meioTramitacao = meioTramitacao;
    }

    public Fase getFase() {
        return fase;
    }

    public void setFase(Fase fase) {
        this.fase = fase;
    }

    public ClasseProcessual getClasseProcessual() {
        return classeProcessual;
    }

    public void setClasseProcessual(ClasseProcessual classeProcessual) {
        this.classeProcessual = classeProcessual;
    }

    public NaturezaAcao getNaturezaAcao() {
        return naturezaAcao;
    }

    public void setNaturezaAcao(NaturezaAcao naturezaAcao) {
        this.naturezaAcao = naturezaAcao;
    }

    public Forum getForum() {
        return forum;
    }

    public void setForum(Forum forum) {
        this.forum = forum;
    }

    public Vara getVara() {
        return vara;
    }

    public void setVara(Vara vara) {
        this.vara = vara;
    }

    public Comarca getComarca() {
        return comarca;
    }

    public void setComarca(Comarca comarca) {
        this.comarca = comarca;
    }

    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }

    public CausaPedido getCausaPedido() {
        return causaPedido;
    }

    public void setCausaPedido(CausaPedido causaPedido) {
        this.causaPedido = causaPedido;
    }

    public GrauRisco getGrauRisco() {
        return grauRisco;
    }

    public void setGrauRisco(GrauRisco grauRisco) {
        this.grauRisco = grauRisco;
    }
}
