package br.com.juridico.controller;

import br.com.juridico.entidades.Processo;
import br.com.juridico.enums.CrudStatus;
import br.com.juridico.exception.CrudException;
import br.com.juridico.exception.ValidationException;
import org.apache.log4j.Logger;

import javax.faces.application.FacesMessage;

/**
 * Created by jeffersonl2009_00 on 06/07/2016.
 */
public abstract class AbstractProcessoController extends AbstractCrudsController {

    private static final Logger LOGGER = Logger.getLogger(AbstractProcessoController.class);

	protected boolean permiteAlteracaoAndamento;
	protected boolean permiteExclusaoAndamento;
	protected boolean permiteInclusaoAndamento;
	protected boolean permiteConsultaAndamento;
	protected boolean permiteInclusaoVinculado;
	protected boolean permiteAlteracaoVinculado;
	protected boolean permiteExclusaoVinculado;
	protected boolean permiteInclusaoProposta;
	protected boolean permiteAlteracaoProposta;
	protected boolean permiteExclusaoProposta;
	protected boolean permiteAlteracaoCusta;
	protected boolean permiteExclusaoCusta;
	protected boolean permiteInclusaoCusta;
	protected boolean permiteConsultaCusta;
    
    private Processo processo;

    /**
     *
     */
    public void novo() {
        processo = instanciarObjetoNovo();
    }

    public void salvar() {
        try {
            antesDeSalvar();
            setEmpresaDeUsuarioLogado(processo);
            createProcesso();
            exibirMensagem(FacesMessage.SEVERITY_INFO, getMessage("msg_sucesso_salvar"));
            navigationToDetalhes();
        } catch (CrudException e) {
            LOGGER.info(e.getCause(), e);
            exibirMensagem(e.getMensagemListDetalhe(), getMessage("msg_erro_salvar"));
        } catch (ValidationException e) {
            for (String message : e.getMessages()) {
                exibirMensagem(FacesMessage.SEVERITY_ERROR, message);
            }
        }
    }

    private void createProcesso() throws ValidationException, CrudException {
        validar();
        create(processo);
        aposSalvar();
    }

    public Processo getProcesso() {
        return processo;
    }

    public void setProcesso(Processo processo) {
        this.processo = processo;
    }

    protected abstract Processo instanciarObjetoNovo();
    protected void antesDeSalvar() throws CrudException {}
    protected void validar() throws ValidationException {}
    protected void aposSalvar() throws CrudException {}
    protected abstract void setEmpresaDeUsuarioLogado(Processo objetoSelecionado);
    protected abstract void create(Processo objetoSelecionado) throws ValidationException, CrudException;
    protected abstract void navigationToDetalhes();
    
	public boolean isPermiteAlteracaoAndamento() {
		return permiteAlteracaoAndamento;
	}

	public void setPermiteAlteracaoAndamento(boolean permiteAlteracaoAndamento) {
		this.permiteAlteracaoAndamento = permiteAlteracaoAndamento;
	}

	public boolean isPermiteExclusaoAndamento() {
		return permiteExclusaoAndamento;
	}

	public void setPermiteExclusaoAndamento(boolean permiteExclusaoAndamento) {
		this.permiteExclusaoAndamento = permiteExclusaoAndamento;
	}

	public boolean isPermiteInclusaoAndamento() {
		return permiteInclusaoAndamento;
	}

	public void setPermiteInclusaoAndamento(boolean permiteInclusaoAndamento) {
		this.permiteInclusaoAndamento = permiteInclusaoAndamento;
	}

	public boolean isPermiteConsultaAndamento() {
		return permiteConsultaAndamento;
	}

	public void setPermiteConsultaAndamento(boolean permiteConsultaAndamento) {
		this.permiteConsultaAndamento = permiteConsultaAndamento;
	}

	public boolean isPermiteInclusaoVinculado() {
		return permiteInclusaoVinculado;
	}

	public void setPermiteInclusaoVinculado(boolean permiteInclusaoVinculado) {
		this.permiteInclusaoVinculado = permiteInclusaoVinculado;
	}

	public boolean isPermiteAlteracaoVinculado() {
		return permiteAlteracaoVinculado;
	}

	public void setPermiteAlteracaoVinculado(boolean permiteAlteracaoVinculado) {
		this.permiteAlteracaoVinculado = permiteAlteracaoVinculado;
	}

	public boolean isPermiteExclusaoVinculado() {
		return permiteExclusaoVinculado;
	}

	public void setPermiteExclusaoVinculado(boolean permiteExclusaoVinculado) {
		this.permiteExclusaoVinculado = permiteExclusaoVinculado;
	}

	public boolean isPermiteAlteracaoCusta() {
		return permiteAlteracaoCusta;
	}

	public void setPermiteAlteracaoCusta(boolean permiteAlteracaoCusta) {
		this.permiteAlteracaoCusta = permiteAlteracaoCusta;
	}

	public boolean isPermiteExclusaoCusta() {
		return permiteExclusaoCusta;
	}

	public void setPermiteExclusaoCusta(boolean permiteExclusaoCusta) {
		this.permiteExclusaoCusta = permiteExclusaoCusta;
	}

	public boolean isPermiteInclusaoCusta() {
		return permiteInclusaoCusta;
	}

	public void setPermiteInclusaoCusta(boolean permiteInclusaoCusta) {
		this.permiteInclusaoCusta = permiteInclusaoCusta;
	}

	public boolean isPermiteConsultaCusta() {
		return permiteConsultaCusta;
	}

	public void setPermiteConsultaCusta(boolean permiteConsultaCusta) {
		this.permiteConsultaCusta = permiteConsultaCusta;
	}

	public boolean isPermiteInclusaoProposta() {
		return permiteInclusaoProposta;
	}

	public void setPermiteInclusaoProposta(boolean permiteInclusaoProposta) {
		this.permiteInclusaoProposta = permiteInclusaoProposta;
	}

	public boolean isPermiteAlteracaoProposta() {
		return permiteAlteracaoProposta;
	}

	public void setPermiteAlteracaoProposta(boolean permiteAlteracaoProposta) {
		this.permiteAlteracaoProposta = permiteAlteracaoProposta;
	}

	public boolean isPermiteExclusaoProposta() {
		return permiteExclusaoProposta;
	}

	public void setPermiteExclusaoProposta(boolean permiteExclusaoProposta) {
		this.permiteExclusaoProposta = permiteExclusaoProposta;
	}
}
