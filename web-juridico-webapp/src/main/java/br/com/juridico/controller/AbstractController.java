package br.com.juridico.controller;

import br.com.juridico.ejb.StateManager;
import br.com.juridico.entidades.*;
import br.com.juridico.interfaces.Descriptable;
import br.com.juridico.util.RowNumberUtil;
import br.com.juridico.util.Util;
import com.google.common.base.Strings;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URLConnection;
import java.util.*;

/**
 *
 * @author Jefferson
 *
 */
public abstract class AbstractController implements Serializable {

	private static final long serialVersionUID = 1L;

	protected static final Long ALTERAR = 1L;
	protected static final Long EXCLUIR = 4L;
	protected static final Long INCLUIR = 3L;
	protected static final Long CONSULTAR = 2L;

	private static final Logger LOGGER = Logger.getLogger(AbstractController.class);
	private static final String BUNDLE_NAME = "messages";
	protected static final String DETAIL_PROCESSO = "DETAIL_PROCESSO";
	protected static final String USER_LOGGED = "userLogged";

	private static final Map<Class<? extends Enum<?>>, Method> enumValuesMethodCache = Collections
			.synchronizedMap(new WeakHashMap<Class<? extends Enum<?>>, Method>());

	private List<SelectItem> rowsNumber;

	protected boolean permiteAlteracao;
	protected boolean permiteExclusao;
	protected boolean permiteInclusao;
	protected boolean permiteConsulta;

	/**
	 *
	 * @param <E>
	 * @param enumeration
	 * @return
	 */
	protected <E extends Enum<E>> List<SelectItem> transformToSelectItem(Class<E> enumeration) {

		List<SelectItem> result = new ArrayList<>();

		try {

			Method m;

			if(null == (m = enumValuesMethodCache.get(enumeration))) {
				m = enumeration.getMethod("values");

				enumValuesMethodCache.put(enumeration, m);
			}

			for(E e : ((E[]) m.invoke(null))) {

				SelectItem item = new SelectItem();

				item.setValue(e.name());
				item.setLabel(e.toString());

				result.add(item);
			}

		} catch (SecurityException e) {
			LOGGER.error(e.getMessage(),e);
		} catch (NoSuchMethodException e) {
			LOGGER.error(e.getMessage(),e);
		} catch (IllegalArgumentException e) {
			LOGGER.error(e.getMessage(),e);
		} catch (IllegalAccessException e) {
			LOGGER.error(e.getMessage(),e);
		} catch (InvocationTargetException e) {
			LOGGER.error(e.getMessage(),e);
		}

		return result;

	}

	protected List<SelectItem> buildSelectItemList(Descriptable[] items) {
		List<SelectItem> list = new ArrayList<>();

		for (Descriptable i : items) {
			list.add(new SelectItem(i, i.getDescription()));
		}
		return list;
	}


	protected void navigationToHome(){
		getFacesContext().getApplication().getNavigationHandler().handleNavigation(getFacesContext(), null, "/pages/index.xhtml?faces-redirect=true");
	}

	/**
	 *
	 * @param value
	 */
	protected void updateContext(String value) {
		RequestContext context = RequestContext.getCurrentInstance();
		context.update(value);
		context.execute("$(\".alert\").delay(5000).addClass(\"in\").fadeOut(800);");
	}

	/**
	 *
	 * @param severity
	 * @param titulo
	 * @param descricao
	 */
	protected void showMessageInDialog(FacesMessage.Severity severity, String titulo, String descricao){
		FacesMessage message = new FacesMessage(severity, titulo, descricao);
		RequestContext.getCurrentInstance().showMessageInDialog(message);
	}

	protected static FacesContext getFacesContext() {
		return FacesContext.getCurrentInstance();
	}

	protected static ExternalContext getExternalContext() {
		return getFacesContext().getExternalContext();
	}

	/**
	 *
	 * @return
	 */
	protected Flash getFlashScope() {
		return FacesContext.getCurrentInstance().getExternalContext().getFlash();
	}

	/**
	 *
	 * @return
	 */
	protected void removeFlashScope(String key) {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().remove(key);
	}

	protected Object getBeanInSession(String name) {
		LOGGER.debug("name: " + name);

		Object bean = getFacesContext().getExternalContext().getSessionMap().get(name);
		if (bean == null) {
			LOGGER.warn("Nao foi possivel encontrar o bean na Sessao com o nome: " + name);
		}
		return bean;
	}

	protected Object getParam(ActionEvent event, String id) {
		return event.getComponent().getAttributes().get(id);

	}

	protected void replaceBeanInSession(String entityName, Object bean) {
		getFacesContext().getExternalContext().getSessionMap().put(entityName, bean);
	}

	/**
	 *
	 * @param message
	 * @param severity
	 */
	protected void exibirMensagem(String message, FacesMessage.Severity severity) {
		exibirMensagem(severity, message);
	}

	/**
	 *
	 * @param severity
	 * @param message
	 */
	protected void exibirMensagem(FacesMessage.Severity severity, String message) {
		exibirMensagem(severity, message, null);
	}

	/**
	 * Add mensagem no Faces Context. o parametro "message" podera ser
	 * considerado tanto uma mensagem, como um codigo de Resource.messages.
	 *
	 * @param severity
	 * @param mensagem
	 * @param message
	 */
	protected void exibirMensagem(FacesMessage.Severity severity, String mensagem, String message) {
		String mensagemFinal = message;
		if (Util.nvl(message, " ").charAt(0) == ':') {
			mensagemFinal = getMessage(message.substring(1, message.length()));
		}
		getFacesContext().addMessage(null, new FacesMessage(severity, mensagem, mensagemFinal));
	}

	/**
	 *
	 * @param message
	 */
	protected void addMessage(String message) {
		addMessage(FacesMessage.SEVERITY_INFO, message);
	}

	/**
	 *
	 * @param severity
	 * @param message
	 */
	protected void addMessage(FacesMessage.Severity severity, String message) {
		addMessage(severity, message, null);
	}

	/**
	 * Add mensagem no Faces Context. o parametro "message" podera ser
	 * considerado tanto uma mensagem, como um codigo de Resource.messages.
	 *
	 * @param severity
	 * @param mensagem
	 * @param message
	 */
	protected void addMessage(FacesMessage.Severity severity, String mensagem, String message) {
		String mensagemFinal = message;
		if (Util.nvl(message, " ").charAt(0) == ':') {
			mensagemFinal = getMessage(message.substring(1, message.length()));
		}
		getFacesContext().addMessage(null, new FacesMessage(severity, mensagem, mensagemFinal));
	}

	/**
	 *
	 * @param errors
	 * @param operationMessage
	 */
	protected void exibirMensagem(List<String> errors, String operationMessage) {
		exibirMensagem(FacesMessage.SEVERITY_WARN, operationMessage);

		for (String error : errors) {
			exibirMensagem(FacesMessage.SEVERITY_WARN, error);
		}
	}

	/**
	 *
	 * @param key
	 * @return
	 */
	protected String getMessage(String key) {
		ResourceBundle bundle = ResourceBundle.getBundle(BUNDLE_NAME, getFacesContext().getViewRoot().getLocale());
		return bundle.getString(key);
	}

	protected StateManager getStateManager(){
		LoginController loginController = getFacesContext().getApplication().evaluateExpressionGet(getFacesContext(), "#{loginController}", LoginController.class);
		return loginController.getStateManager();
	}

	/**
	 *
	 * @return
	 */
	protected List<SelectItem> getRowsNumber() {
		if (rowsNumber == null) {
			rowsNumber = new ArrayList<>();
			rowsNumber.add(new SelectItem(RowNumberUtil.VALOR_UM));
			rowsNumber.add(new SelectItem(RowNumberUtil.VALOR_CINCO));
		}

		return rowsNumber;
	}

	protected static String getIpAddress() {
		return ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest())
				.getRemoteAddr();
	}

	/**
	 *
	 * @param id
	 * @return
	 */
	protected static Object findComponent(String id) {
		return FacesContext.getCurrentInstance().getViewRoot().findComponent(id);
	}

	/**
	 *
	 * @param key
	 * @param value
	 */
	protected static void setSessionAttribute(String key, Object value) {
		HttpSession httpSession = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		if(httpSession == null){
			return;
		}
		httpSession.setAttribute(key, value);
	}

	/**
	 *
	 * @param key
	 */
	protected static void removeSessionAttribute(String key) {
		((HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false)).removeAttribute(key);
	}

	/**
	 *
	 * @param key
	 * @return
	 */
	public static Object getSessionAttribute(String key) {
		HttpSession httpSession = (HttpSession) getExternalContext().getSession(false);
		if(httpSession == null) {
			return null;
		}
		return httpSession.getAttribute(key);
	}

	/**
	 *
	 * @param fileName
	 * @return
	 */
	protected static String getMimeTypeFrom(String fileName) {
		return URLConnection.getFileNameMap().getContentTypeFor(fileName);
	}

	/**
	 *
	 * @param key
	 * @return
	 */
	protected static Object getRequestAttribute(String key) {
		return FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(key);
	}

	/**
	 *
	 * @return
	 */
	protected static HttpServletResponse getResponse() {
		return (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
	}

	/**
	 *
	 * @param pessoa
	 * @return
	 */
	protected static User setUser(Pessoa pessoa, Empresa empresa) {
		if(pessoa == null  || pessoa.getLogin() == null) {
			return null;
		}

		User user = new User();
		user.setPessoa(pessoa);
		user.setId(pessoa.getId());
		user.setName(pessoa.getDescricao());
		user.setLogin(pessoa.getLogin().getLogin());
		user.setIp(getIpAddress());
		user.setDataCadastro(pessoa.getDataCadastro());
		user.setStatus(pessoa.getAtivo() ? "Ativo" : "Inativo");
		user.setCodEmpresa(pessoa.getCodigoEmpresa());
		user.setEmpresa(empresa);
		user.setSessionId(pessoa.getId().toString());
		user.setPerfil(pessoa.getPerfil());
		user.setImagem(pessoa.getImagem());
		carregaAcessosMap(pessoa.getPerfil(), user);
		return user;
	}

	protected static UserPrimeiroAcesso setUserPrimeiroAcesso(Pessoa pessoa) {
		UserPrimeiroAcesso userPrimeiroAcesso = new UserPrimeiroAcesso();
		userPrimeiroAcesso.setId(pessoa.getId());
		userPrimeiroAcesso.setName(pessoa.getDescricao());
		userPrimeiroAcesso.setLogin(pessoa.getLogin().getLogin());
		userPrimeiroAcesso.setIp(getIpAddress());
		userPrimeiroAcesso.setCodEmpresa(pessoa.getCodigoEmpresa());
		return userPrimeiroAcesso;
	}

	private static void carregaAcessosMap(Perfil perfil, User user){
		if(perfil != null) {
			for (PerfilAcesso pa : perfil.getAcessos()) {
				user.getAcessosMap().put(pa.getAcesso().getFuncionalidade(), pa.getAcesso().getDescricao());
			}
		}
	}

	public static boolean possuiAcesso(String key) {
		return !Strings.isNullOrEmpty(getUser().getAcessosMap().get(key));
	}

	public static User getUser() {
		return (User) getSessionAttribute("userLogged");
	}

	public static UserPrimeiroAcesso getUserPrimeiroAcesso() {
		return (UserPrimeiroAcesso) getSessionAttribute("userPrimeiroAcessoLogged");
	}

	public void setRowsNumber(List<SelectItem> rowsNumber) {
		this.rowsNumber = rowsNumber;
	}

	public boolean isPermiteAlteracao() {
		return permiteAlteracao;
	}

	public void setPermiteAlteracao(boolean permiteAlteracao) {
		this.permiteAlteracao = permiteAlteracao;
	}

	public boolean isPermiteExclusao() {
		return permiteExclusao;
	}

	public void setPermiteExclusao(boolean permiteExclusao) {
		this.permiteExclusao = permiteExclusao;
	}

	public boolean isPermiteInclusao() {
		return permiteInclusao;
	}

	public void setPermiteInclusao(boolean permiteInclusao) {
		this.permiteInclusao = permiteInclusao;
	}

	public boolean isPermiteConsulta() {
		return permiteConsulta;
	}

	public void setPermiteConsulta(boolean permiteConsulta) {
		this.permiteConsulta = permiteConsulta;
	}

}
