
package br.com.juridico.controller;

import br.com.juridico.business.MenuAcessoBusiness;
import br.com.juridico.entidades.Empresa;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author Jefferson
 *
 */
@Named
@ViewScoped
public class MenuController extends AbstractController {

	/**
	 *
	 */
	private static final long serialVersionUID = 6061186007636293687L;

	private static final String EMPRESA_FLASH = "EMPRESA_FLASH";
	private static final String PAGINA_HOME = "/pages/index.xhtml?faces-redirect=true";
	private static final String PAGINA_ATENDIMENTO = "/pages/atendimento/atendimento-template.xhtml?faces-redirect=true";
	private static final String PAGINA_CADASTROS = "/pages/cadastros/list-cadastros.xhtml?faces-redirect=true";
	private static final String PAGINA_CONTRATOS = "/pages/contratos/list-contratos.xhtml?faces-redirect=true";
	private static final String PAGINA_FINANCEIRO = "/pages/financeiro/list-financeiro.xhtml?faces-redirect=true";
	private static final String PAGINA_PROCESSOS = "/pages/processos/processo-list.xhtml?faces-redirect=true";
	private static final String PAGINA_RELATORIOS = "/pages/relatorios/filtros/report-filter-form.xhtml?faces-redirect=true";
	private static final String PAGINA_ACESSOS = "/pages/acessos/acessos-template.xhtml?faces-redirect=true";
	private static final String PAGINA_PERFIL = "/pages/profile.xhtml?faces-redirect=true";

	private Empresa empresa;

	@PostConstruct
	public void initialize() {
		Empresa empresaFlash = (Empresa) getFlashScope().get(EMPRESA_FLASH);
		if (empresaFlash == null) {
			buscarEmpresa();
			return;
		}
		this.empresa = empresaFlash;
	}

	private void buscarEmpresa() {
		if (getUser() != null) {
			this.empresa = getUser().getEmpresa();
			getFlashScope().put(EMPRESA_FLASH, this.empresa);
		}
	}

	/**
	 *
	 * @return
	 */
	public String goToHome() {
		return PAGINA_HOME;
	}

	/**
	 * MENU ADVOGADOS
	 *
	 * @return
	 */
	public boolean isRoleMenuAdvogados() {
		return MenuAcessoBusiness.isRoleMenuAdvogados(getUser());
	}

	/**
	 * MENU ATENDIMENTO
	 *
	 * @return
	 */
	public boolean isRoleMenuAtendimento() {
		return MenuAcessoBusiness.isRoleMenuAtendimento(getUser());
	}

	/**
	 *
	 * @return
	 */
	public String goToAtendimento() {
		return PAGINA_ATENDIMENTO;
	}

	/**
	 * MENU CADASTROS
	 *
	 * @return
	 */
	public boolean isRoleMenuCadastros() {
		return MenuAcessoBusiness.isRoleMenuCadastros(getUser());
	}

	/**
	 *
	 * @return
	 */
	public String goToCadastros() {
		return PAGINA_CADASTROS;
	}

	/**
	 * MENU CONTROLADORIA
	 * @return
	 */
	public boolean isRoleMenuControladoria() {
		return MenuAcessoBusiness.isRoleMenuControladoria(getUser()) && this.getEmpresa().isConfiguracaoControladoria();
	}

	/**
	 * MENU FINANCEIRO
	 * @return
	 */
	public boolean isRoleMenuFinanceiro() {
		return MenuAcessoBusiness.isRoleMenuFinanceiro(getUser()) && this.getEmpresa().isConfiguracaoFinanceiro();
	}

	/**
	 *
	 * @return
	 */
	public String goToFinanceiro() {
		return PAGINA_FINANCEIRO;
	}

	/**
	 *
	 * @return
	 */
	public String goToContratos() {
		return PAGINA_CONTRATOS;
	}

	/**
	 * MENU CONTRATOS
	 * @return
	 */
	public boolean isRoleMenuContratos() {
		return MenuAcessoBusiness.isRoleMenuContratos(getUser()) && getEmpresa().isConfiguracaoContratos();
	}

	/**
	 * MENU CLIENTES
	 *
	 * @return
	 */
	public boolean isRoleMenuPessoas() {
		return MenuAcessoBusiness.isRoleMenuPessoas(getUser());
	}

	/**
	 * MENU PROCESSOS
	 *
	 * @return
	 */
	public boolean isRoleMenuProcessos() {
		return MenuAcessoBusiness.isRoleMenuProcessos(getUser());
	}

	/**
	 *
	 * @return
	 */
	public String goToProcessos() {
		return PAGINA_PROCESSOS;
	}

	/**
	 * MENU RELATORIOS
	 *
	 * @return
	 */
	public boolean isRoleMenuRelatorios() {
		return MenuAcessoBusiness.isRoleMenuRelatorios(getUser());
	}

	/**
	 *
	 * @return
	 */
	public String goToRelatorios() {
		return PAGINA_RELATORIOS;
	}

	/**
	 * MENU ACESSOS
	 *
	 * @return
	 */
	public boolean isRoleMenuAcessos() {
		return MenuAcessoBusiness.isRoleMenuAcessos(getUser());
	}

	/**
	 *
	 * @return
	 */
	public String goToAcessos() {
		return PAGINA_ACESSOS;
	}

	/**
	 *
	 * @return
	 */
	public String goToMeuPerfil() {
		return PAGINA_PERFIL;
	}

	public boolean isRoleMenuAgenda() {
		return MenuAcessoBusiness.isRoleMenuAgenda(getUser());
	}

	public boolean isPerfilAdmin() {
		return getUser().getPerfil().isAdmin();
	}

	public Empresa getEmpresa() {
		return empresa;
	}
}
