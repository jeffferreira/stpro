package br.com.juridico.controller.manutencao;

import br.com.juridico.controller.CrudController;
import br.com.juridico.entidades.Acesso;
import br.com.juridico.entidades.Menu;
import br.com.juridico.exception.CrudException;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.impl.AcessoSessionBean;
import br.com.juridico.impl.MenuSessionBean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import java.util.List;

/**
 * Created by jefferson on 22/10/2016.
 */
@ManagedBean
@ViewScoped
public class ManutencaoAcessoController extends CrudController<Acesso> {

    @Inject
    private AcessoSessionBean acessoSessionBean;
    @Inject
    private MenuSessionBean menuSessionBean;


    public List<Acesso> getAcessos() {
        return acessoSessionBean.findAcessosDoSistema();
    }

    public List<Menu> getMenus(){
        return menuSessionBean.findAllMenus();
    }

    @Override
    protected void aposCancelar() throws CrudException {
        getObjetoSelecionado().setFuncionalidade(null);
        updateContext("form");
        super.aposCancelar();
    }

    @Override
    protected void create(Acesso acesso) throws ValidationException, CrudException {
        if (isEditavel()) {
            acessoSessionBean.update(acesso);
            return;
        }
        acessoSessionBean.create(acesso);
    }

    @Override
    protected void carregaLazyDataModel() {
        // carregaLazyDataModel
    }

    @Override
    protected Acesso inicializaObjetosLazy(Acesso objetoSelecionado) {
        acessoSessionBean.clearSession();
        return acessoSessionBean.retrieve(objetoSelecionado.getId());
    }

    @Override
    protected Acesso instanciarObjetoNovo() {
        return new Acesso();
    }

    @Override
    protected void setEmpresaDeUsuarioLogado(Acesso objetoSelecionado) {
        // setEmpresaDeUsuarioLogado
    }

    @Override
    protected void addChaveAcesso() {
        // addChaveAcesso
    }

    @Override
    protected boolean isPodeAlterar() {
        return false;
    }

    @Override
    protected boolean isPodeExcluir() {
        return false;
    }

    @Override
    protected boolean isPodeIncluir() {
        return false;
    }
}
