package br.com.juridico.controller;

import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Jefferson
 *
 */
@ManagedBean
@ViewScoped
public class ContaReceberController extends AbstractController {


	/**
	 *
	 */
	private static final long serialVersionUID = -5363621670335559115L;
	private static final Logger LOGGER = Logger.getLogger(ContaReceberController.class);

	private static final String CHAVE_ACESSO = "CONTASRECEBER_CONTAS_RECEBER_TEMPLATE";
	private static final String PAGINA_FORMULARIO_CONTA_RECEBER = "/pages/financeiro/contasreceber/contas-receber-template.xhtml?faces-redirect=true";



	@PostConstruct
	public void initialize(){
	}

	public String goToContaReceber(){
		return PAGINA_FORMULARIO_CONTA_RECEBER;
	}

	public boolean isHasAccess(){
		return possuiAcesso(CHAVE_ACESSO);
	}
}
