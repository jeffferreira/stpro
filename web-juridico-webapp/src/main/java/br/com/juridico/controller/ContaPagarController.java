package br.com.juridico.controller;

import br.com.juridico.datamodel.LazyEntityDataModel;
import br.com.juridico.entidades.*;
import br.com.juridico.enums.ContasPagarType;
import br.com.juridico.enums.StatusContasPagarType;
import br.com.juridico.exception.CrudException;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.filter.ContasPagarFilter;
import br.com.juridico.impl.ContaPagarSessionBean;
import br.com.juridico.impl.FornecedorSessionBean;
import br.com.juridico.util.DateUtils;
import com.google.common.base.Strings;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Jefferson
 *
 */
@ManagedBean
@ViewScoped
public class ContaPagarController extends CrudController<ContaPagar> {

	/**
	 *
	 */
	private static final long serialVersionUID = -5363621670335559115L;
	private static final Logger LOGGER = Logger.getLogger(ContaPagarController.class);

	private static final String CHAVE_ACESSO = "CONTASPAGAR_CONTAS_PAGAR_TEMPLATE";
	private static final String PAGINA_FORMULARIO_CONTAS_PAGAR = "/pages/financeiro/contaspagar/contas-pagar-template.xhtml?faces-redirect=true";
	private static final String FORM = "form";

	private List<SelectItem> tiposPagamentos;
	private ContasPagarFilter filtro = new ContasPagarFilter(StatusContasPagarType.TO);

	private boolean timeline;
	private boolean lancamento;
	private boolean aprovacao;
	private boolean pagamento;
	private boolean cadastroFornecedor;

	@Inject
	private ContaPagarSessionBean contaPagarSessionBean;
	@Inject
	private FornecedorSessionBean fornecedorSessionBean;

	@PostConstruct
	public void initialize(){
		carregaLazyDataModel();

		this.timeline = Boolean.TRUE;
		this.tiposPagamentos = transformToSelectItem(ContasPagarType.class);
	}

	@Override
	protected void validar() throws ValidationException {
		contaPagarSessionBean.clearSession();
		boolean jaExisteContaPagar = contaPagarSessionBean.isNfJaCadastrada(getObjetoSelecionado(), getStateManager().getEmpresaSelecionada().getId());
		getObjetoSelecionado().validar(jaExisteContaPagar);
	}

	@Override
	protected void antesDeSalvar() throws CrudException {
		getObjetoSelecionado().setDataCadastro(new Date());
		getObjetoSelecionado().setStatus(StatusContasPagarType.CA);
		getObjetoSelecionado().setUsuario(getUser().getLogin());
	}

	@Override
	protected void depoisInserir() throws CrudException {
		super.depoisInserir();
		ContaPagarEmpresaItem item = new ContaPagarEmpresaItem();
		item.setContaPagar(getObjetoSelecionado());
		item.setValor(new BigDecimal("0").setScale(2));
		Empresa e = new Empresa(2L);
		e.setDescricao("Copel");
		item.setEmpresa(e);

		getObjetoSelecionado().getItens().add(item);
	}

	public StatusContasPagarType[] getStatusConta() {
		return new StatusContasPagarType[]{StatusContasPagarType.TO, StatusContasPagarType.AP, StatusContasPagarType.PG, StatusContasPagarType.RJ, StatusContasPagarType.DE};
	}

	public void buscar(){

	}

	@Override
	protected void create(ContaPagar objetoSelecionado) throws ValidationException, CrudException {
		if (isEditavel() ) {
			contaPagarSessionBean.update(objetoSelecionado);
		} else if (!Strings.isNullOrEmpty(objetoSelecionado.getNotaFiscal())) {
			contaPagarSessionBean.create(objetoSelecionado);
		} else {
			throw new CrudException();
		}
	}

	@Override
	protected void carregaLazyDataModel() {
		setLazyDataModel(new LazyEntityDataModel<ContaPagar>() {
			@Override
			protected List<ContaPagar> consultaFiltrada(ConsultaLazyFilter filtro) {
				filtro.setDescricao(getDescricaoFiltro());
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return contaPagarSessionBean.filtrados(filtro);
			}

			@Override
			protected int countTotalRegistros(ConsultaLazyFilter filtro) {
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return contaPagarSessionBean.quantidadeFiltrados(filtro);
			}
		});
	}

	@Override
	protected ContaPagar inicializaObjetosLazy(ContaPagar objetoSelecionado) {
		contaPagarSessionBean.clearSession();
		return contaPagarSessionBean.retrieve(objetoSelecionado.getId());
	}

	@Override
	protected ContaPagar instanciarObjetoNovo() {
		return new ContaPagar();
	}

	@Override
	protected void setEmpresaDeUsuarioLogado(ContaPagar objetoSelecionado) {
		objetoSelecionado.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
	}

	@Override
	protected void addChaveAcesso() {
		setChaveAcesso(CHAVE_ACESSO);
	}

	@Override
	protected boolean isPodeAlterar() {
		return false;
	}

	@Override
	protected boolean isPodeExcluir() {
		return false;
	}

	@Override
	protected boolean isPodeIncluir() {
		return false;
	}

	public void goToHome(){
		this.timeline = Boolean.TRUE;
		this.lancamento = Boolean.FALSE;
		this.aprovacao = Boolean.FALSE;
		this.pagamento = Boolean.FALSE;
		this.cadastroFornecedor = Boolean.FALSE;
		RequestContext.getCurrentInstance().update(FORM);
	}

	public void goToCadastroFornecedor(){
		this.cadastroFornecedor = Boolean.TRUE;
		this.timeline = Boolean.FALSE;
		this.lancamento = Boolean.FALSE;
		this.aprovacao = Boolean.FALSE;
		this.pagamento = Boolean.FALSE;
		RequestContext.getCurrentInstance().update(FORM);
	}

	public void goToLancamento(){
		this.lancamento = Boolean.TRUE;
		this.aprovacao = Boolean.FALSE;
		this.pagamento = Boolean.FALSE;
		this.timeline = Boolean.FALSE;
		this.cadastroFornecedor = Boolean.FALSE;
		RequestContext.getCurrentInstance().update(FORM);
	}

	public void goToAprovacao(){
		this.lancamento = Boolean.FALSE;
		this.aprovacao = Boolean.TRUE;
		this.pagamento = Boolean.FALSE;
		this.timeline = Boolean.FALSE;
		this.cadastroFornecedor = Boolean.FALSE;
		RequestContext.getCurrentInstance().update(FORM);
	}

	public void goToPagamento(){
		this.lancamento = Boolean.FALSE;
		this.aprovacao = Boolean.FALSE;
		this.pagamento = Boolean.TRUE;
		this.timeline = Boolean.FALSE;
		this.cadastroFornecedor = Boolean.FALSE;
		RequestContext.getCurrentInstance().update(FORM);
	}

	public void inicilizaParcelas() {
		if (isEditavel()){
			List<ParcelaContaPagar> lists = new ArrayList<>();
			ParcelaContaPagar parcela;
			if (getObjetoSelecionado().getQtdeParcelas() != null) {
				for (int i = 0; i < getObjetoSelecionado().getQtdeParcelas(); i++) {
					parcela = new ParcelaContaPagar();
					parcela.setNumeroParcela(i + 1);
					parcela.setContaPagar(getObjetoSelecionado());
					lists.add(parcela);
				}
			}
			getObjetoSelecionado().setParcelas(lists);
		}
	}

	public Date getDataPagamentoDaParcela(ParcelaContaPagar parcela){
		if(parcela.getDataVencimento() == null){
			return null;
		}
		return parcela.getDataPagamento();
	}

	private Date getDataVencimentoUtil(ParcelaContaPagar parcela) {
		Date vencimentoDiaUtil = parcela.getDataVencimento();
		if(!DateUtils.isDiaUtil(parcela.getDataVencimento())){
			vencimentoDiaUtil = DateUtils.addUtilsDays(1, vencimentoDiaUtil);
		}
		return vencimentoDiaUtil;
	}

	public List<Fornecedor> completeFornecedores(String query){
		ConsultaLazyFilter consultaLazyFilter = new ConsultaLazyFilter();
		consultaLazyFilter.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());

		if (Strings.isNullOrEmpty(query)) {
			return fornecedorSessionBean.filtrados(consultaLazyFilter);
		}
		consultaLazyFilter.setDescricao(query);
		return fornecedorSessionBean.filtrados(consultaLazyFilter);
	}

	public String goToContasPagar(){
		return PAGINA_FORMULARIO_CONTAS_PAGAR;
	}

	public boolean isHasAccess(){
		return possuiAcesso(CHAVE_ACESSO);
	}

	public boolean isTimeline() {
		return timeline;
	}

	public void setTimeline(boolean timeline) {
		this.timeline = timeline;
	}

	public boolean isLancamento() {
		return lancamento;
	}

	public void setLancamento(boolean lancamento) {
		this.lancamento = lancamento;
	}

	public boolean isCadastroFornecedor() {
		return cadastroFornecedor;
	}

	public void setCadastroFornecedor(boolean cadastroFornecedor) {
		this.cadastroFornecedor = cadastroFornecedor;
	}

	public boolean isAprovacao() {
		return aprovacao;
	}

	public void setAprovacao(boolean aprovacao) {
		this.aprovacao = aprovacao;
	}

	public boolean isPagamento() {
		return pagamento;
	}

	public void setPagamento(boolean pagamento) {
		this.pagamento = pagamento;
	}

	public List<SelectItem> getTiposPagamentos() {
		return tiposPagamentos;
	}

	public void setTiposPagamentos(List<SelectItem> tiposPagamentos) {
		this.tiposPagamentos = tiposPagamentos;
	}

	public ContasPagarFilter getFiltro() {
		return filtro;
	}

	public void setFiltro(ContasPagarFilter filtro) {
		this.filtro = filtro;
	}
}
