package br.com.juridico.controller;

import br.com.juridico.datamodel.LazyEntityDataModel;
import br.com.juridico.entidades.Direito;
import br.com.juridico.entidades.StatusAgenda;
import br.com.juridico.enums.EventoColorTypes;
import br.com.juridico.exception.CrudException;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.impl.DireitoSessionBean;
import br.com.juridico.impl.PerfilAcessoSessionBean;
import br.com.juridico.impl.StatusAgendaSessionBean;
import com.google.common.base.Strings;
import org.apache.log4j.Logger;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Jefferson
 *
 */
@ManagedBean
@SessionScoped
public class StatusAgendaController extends CrudController<StatusAgenda> {

	/**
	 *
	 */
	private static final long serialVersionUID = -8741515223323095235L;
	private static final Logger LOGGER = Logger.getLogger(StatusAgendaController.class);

	private static final String FORM = "statusAgendaForm";
	private static final String CHAVE_ACESSO = "STATUSAGENDA_STATUS_AGENDA_TEMPLATE";
	private static final String PAGINA_FORMULARIO = "/pages/cadastros/statusAgenda/status-agenda-template.xhtml?faces-redirect=true";

	@Inject
	private StatusAgendaSessionBean statusAgendaSessionBean;
	@Inject
	private PerfilAcessoSessionBean perfilAcessoSessionBean;
	@Inject
	private DireitoSessionBean direitoSessionBean;

	private StatusAgenda statusAgendaCumprir;

	public void init(){
		carregaLazyDataModel();
	}

	@Override
	protected void validar() throws ValidationException {
		statusAgendaSessionBean.clearSession();
		boolean jaExiste = statusAgendaSessionBean.isDescricaoJaCadastrada(getObjetoSelecionado(), getStateManager().getEmpresaSelecionada().getId());
		getObjetoSelecionado().validar(jaExiste);
	}

	public void adicionaControladorioStatusAcumprirListener(){
		try {
			statusAgendaSessionBean.update(statusAgendaCumprir);
		} catch (ValidationException e) {
			LOGGER.error(e, e.getCause());
		}
	}

	@SuppressWarnings("serial")
	@Override
	protected void carregaLazyDataModel() {
		setLazyDataModel(new LazyEntityDataModel<StatusAgenda>() {
			@Override
			protected List<StatusAgenda> consultaFiltrada(ConsultaLazyFilter filtro) {
				filtro.setDescricao(getDescricaoFiltro());
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return statusAgendaSessionBean.filtrados(filtro);
			}

			@Override
			protected int countTotalRegistros(ConsultaLazyFilter filtro) {
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return statusAgendaSessionBean.quantidadeFiltrados(filtro);
			}
		});
	}

	@Override
	protected void aposCancelar() throws CrudException {
		getObjetoSelecionado().setDescricao(null);
		updateContext(FORM);
		super.aposCancelar();
	}

	@Override
	protected void create(StatusAgenda statusAgenda) throws ValidationException, CrudException {
		StatusAgenda statusAgendaExclusao = getRegistroComExclusaoLogica();
		String styleColor = EventoColorTypes.fromComponentId(getObjetoSelecionado().getStyleColor().replace("event-text-color-", "")).getTextColor();

		if (isEditavel() && statusAgendaExclusao != null) {
			statusAgendaExclusao.setDataExclusao(null);
			statusAgendaExclusao.setStyleColor(styleColor);
			statusAgendaSessionBean.update(statusAgendaExclusao);
		} else if (statusAgenda != null && !Strings.isNullOrEmpty(statusAgenda.getDescricao())) {
			getObjetoSelecionado().setStyleColor(styleColor);
			getObjetoSelecionado().setStatusAcumprirPassaControladoria(Boolean.FALSE);
			statusAgendaSessionBean.create(statusAgenda);
		} else {
			throw new CrudException();
		}

	}

	private StatusAgenda getRegistroComExclusaoLogica() {
		return statusAgendaSessionBean.findByDescricao(getObjetoSelecionado().getDescricao(), getStateManager().getEmpresaSelecionada().getId());
	}

	@Override
	protected void antesDeExcluir() throws CrudException {
		getObjetoSelecionado().setDataExclusao(new Date());
	}

	@Override
	protected void aposExcluir() throws CrudException {
		super.buscarPorLazy();
	}

	/**
	 *
	 * @return
	 */
	public String goToFormulario() {
		return PAGINA_FORMULARIO;
	}

	@Override
	protected StatusAgenda inicializaObjetosLazy(StatusAgenda objeto) {
		statusAgendaSessionBean.clearSession();
		return statusAgendaSessionBean.retrieve(objeto.getId());
	}

	@Override
	protected StatusAgenda instanciarObjetoNovo() {
		return new StatusAgenda();
	}

	@Override
	protected void setEmpresaDeUsuarioLogado(StatusAgenda objetoSelecionado) {
		objetoSelecionado.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());

	}

	public List<StatusAgenda> getStatusAgendaList(){
		return statusAgendaSessionBean.retrieveAll(getStateManager().getEmpresaSelecionada().getId());
	}

	@Override
	protected void addChaveAcesso() {
		setChaveAcesso(CHAVE_ACESSO);
	}

	@Override
	protected boolean isPodeAlterar(){
		Direito direito = direitoSessionBean.retrieve(ALTERAR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

	@Override
	protected boolean isPodeExcluir(){
		Direito direito = direitoSessionBean.retrieve(EXCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

	@Override
	protected boolean isPodeIncluir(){
		Direito direito = direitoSessionBean.retrieve(INCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

	public StatusAgenda getStatusAgendaCumprir() {
		return statusAgendaCumprir;
	}

	public void setStatusAgendaCumprir(StatusAgenda statusAgendaCumprir) {
		this.statusAgendaCumprir = statusAgendaCumprir;
	}

	public boolean isPossuiConfiguracaoControladoria(){
		return getStateManager().getEmpresaSelecionada().isConfiguracaoControladoria();
	}

}
