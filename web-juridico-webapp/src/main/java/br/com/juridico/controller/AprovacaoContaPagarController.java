package br.com.juridico.controller;

import br.com.juridico.datamodel.LazyEntityDataModel;
import br.com.juridico.entidades.*;
import br.com.juridico.enums.ContasPagarType;
import br.com.juridico.enums.StatusContasPagarType;
import br.com.juridico.enums.TipoDataContasPagarType;
import br.com.juridico.exception.CrudException;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.filter.ContasPagarFilter;
import br.com.juridico.impl.AprovacaoContaPagarSessionBean;
import br.com.juridico.impl.ContaPagarSessionBean;
import br.com.juridico.impl.FornecedorSessionBean;
import br.com.juridico.util.DateUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static br.com.juridico.enums.StatusContasPagarType.*;
import static br.com.juridico.enums.TipoDataContasPagarType.*;

/**
 *
 * @author Jefferson
 *
 */
@ManagedBean
@ViewScoped
public class AprovacaoContaPagarController extends AbstractController {

	@Inject
	private FornecedorSessionBean fornecedorSessionBean;
	@Inject
	private AprovacaoContaPagarSessionBean aprovacaoContaPagarSessionBean;

	private List<ContaPagar> lista = Lists.newArrayList();

	private ContasPagarFilter filtro = new ContasPagarFilter(CA);

	public StatusContasPagarType[] getStatusConta() {
		return new StatusContasPagarType[]{CA, AP};
	}

	public List<Fornecedor> completeFornecedores(String query){
		ConsultaLazyFilter consultaLazyFilter = new ConsultaLazyFilter();
		consultaLazyFilter.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());

		if (Strings.isNullOrEmpty(query)) {
			return fornecedorSessionBean.filtrados(consultaLazyFilter);
		}
		consultaLazyFilter.setDescricao(query);
		return fornecedorSessionBean.filtrados(consultaLazyFilter);
	}

	public void buscar(){
		this.lista = aprovacaoContaPagarSessionBean.findContasAprovacaoByFilter(filtro, getStateManager().getEmpresaSelecionada().getId());
	}

	public TipoDataContasPagarType[] getTipoDataContasPagar() {
		return new TipoDataContasPagarType[]{EMISSAO, LANCAMENTO, VENCIMENTO};
	}

	public ContasPagarFilter getFiltro() {
		return filtro;
	}

	public void setFiltro(ContasPagarFilter filtro) {
		this.filtro = filtro;
	}

	public List<ContaPagar> getLista() {
		return lista;
	}

	public void setLista(List<ContaPagar> lista) {
		this.lista = lista;
	}
}
