package br.com.juridico.controller;

import br.com.juridico.datamodel.LazyEntityDataModel;
import br.com.juridico.entidades.Direito;
import br.com.juridico.entidades.GrauRisco;
import br.com.juridico.exception.CrudException;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.impl.DireitoSessionBean;
import br.com.juridico.impl.GrauRiscoSessionBean;
import br.com.juridico.impl.PerfilAcessoSessionBean;
import org.primefaces.model.LazyDataModel;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Marcos
 *
 */
@ManagedBean
@SessionScoped
public class GrauRiscoController extends CrudController<GrauRisco> {

	/**
	 *
	 */
	private static final long serialVersionUID = 8258907913461635508L;
	private static final String GRAU_RISCO_FORM = "grauRiscoForm";
	private static final String CHAVE_ACESSO = "GRAURISCO_GRAU_RISCO_TEMPLATE";
	private static final String PAGINA_FORMULARIO = "/pages/cadastros/grauRisco/grau-risco-template.xhtml?faces-redirect=true";

	@Inject
	private GrauRiscoSessionBean grauRiscoSessionBean;
	@Inject
	private PerfilAcessoSessionBean perfilAcessoSessionBean;
	@Inject
	private DireitoSessionBean direitoSessionBean;

	public void init(){
		carregaLazyDataModel();
	}

	@Override
	protected void validar() throws ValidationException {
		grauRiscoSessionBean.clearSession();
		boolean jaExisteGrauRisco = grauRiscoSessionBean.isDescricaoJaCadastrada(getObjetoSelecionado(), getStateManager().getEmpresaSelecionada().getId());
		getObjetoSelecionado().validar(jaExisteGrauRisco);
	}

	@Override
	protected void create(GrauRisco grauRisco) throws ValidationException, CrudException {
		GrauRisco grauRiscoExclusao = getRegistroComExclusaoLogica();

		if (isEditavel() && grauRiscoExclusao != null) {
			grauRiscoExclusao.setDataExclusao(null);
			grauRiscoSessionBean.update(grauRiscoExclusao);
		} else if (grauRisco != null && grauRisco.getDescricao() != null && !grauRisco.getDescricao().isEmpty()) {
			grauRiscoSessionBean.create(grauRisco);
		} else {
			throw new CrudException();
		}

	}

	@Override
	protected void carregaLazyDataModel() {
		setLazyDataModel(new LazyEntityDataModel<GrauRisco>() {
			@Override
			protected List<GrauRisco> consultaFiltrada(ConsultaLazyFilter filtro) {
				filtro.setDescricao(getDescricaoFiltro());
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return grauRiscoSessionBean.filtrados(filtro);
			}

			@Override
			protected int countTotalRegistros(ConsultaLazyFilter filtro) {
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return grauRiscoSessionBean.quantidadeFiltrados(filtro);
			}
		});
	}

	@Override
	protected void antesDeExcluir() throws CrudException {
		getObjetoSelecionado().setDataExclusao(new Date());
	}

	@Override
	protected void aposExcluir() throws CrudException {
		super.buscarPorLazy();
	}

	@Override
	protected void aposCancelar() throws CrudException {
		getObjetoSelecionado().setDescricao(null);
		updateContext(GRAU_RISCO_FORM);
		super.aposCancelar();
	}

	@Override
	protected GrauRisco inicializaObjetosLazy(GrauRisco objeto) {
		grauRiscoSessionBean.clearSession();
		return grauRiscoSessionBean.retrieve(objeto.getId());
	}

	@Override
	protected GrauRisco instanciarObjetoNovo() {
		return new GrauRisco();
	}

	@Override
	protected void setEmpresaDeUsuarioLogado(GrauRisco objetoSelecionado) {
		objetoSelecionado.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());

	}

	private GrauRisco getRegistroComExclusaoLogica() {
		return grauRiscoSessionBean.findByDescricao(getObjetoSelecionado().getDescricao(), getStateManager().getEmpresaSelecionada().getId());
	}


	/**
	 *
	 * @return
	 */
	public String goToFormulario() {
		return PAGINA_FORMULARIO;
	}

	@Override
	protected void addChaveAcesso() {
		setChaveAcesso(CHAVE_ACESSO);
	}

	@Override
	protected boolean isPodeAlterar(){
		Direito direito = direitoSessionBean.retrieve(ALTERAR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

	@Override
	protected boolean isPodeExcluir(){
		Direito direito = direitoSessionBean.retrieve(EXCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

	@Override
	protected boolean isPodeIncluir(){
		Direito direito = direitoSessionBean.retrieve(INCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}
}
