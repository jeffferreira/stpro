package br.com.juridico.controller;

import br.com.juridico.business.AgendaEmailBusiness;
import br.com.juridico.business.EmailTemplateBusiness;
import br.com.juridico.business.TipoAndamentoBusiness;
import br.com.juridico.dto.PessoaAgendaDto;
import br.com.juridico.entidades.*;
import br.com.juridico.enums.ControladoriaIndicador;
import br.com.juridico.enums.TipoOperacaoIndicador;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.impl.*;
import br.com.juridico.state.ControladoriaState;
import br.com.juridico.state.controladoria.AprovaControladoria;
import br.com.juridico.state.controladoria.ControladoriaContext;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.LazyScheduleModel;
import org.primefaces.model.ScheduleEvent;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Created by Jefferson on 23/08/2016.
 */
@ManagedBean
@ViewScoped
public class AgendaGeralController extends AbstractAgendaController implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -7613942626769610539L;
	private static final Logger LOGGER = Logger.getLogger(AgendaGeralController.class);
	private static final String TODOS = "Todos";
	private static final String INDEFINIDO = "Indefinido";
	private static final String PESSOA_SELECT_FLASH = "PESSOA_SELECT_FLASH";
	private static final String CHAVE_ACESSO = "GERAL_AGENDA_GERAL_FORM";
	private static final String TRANSFERIR_AGENDA = "TRANSFERIR_AGENDA";
	private static final String CONSULTA_AGENDA_SEM_RESPONSAVEL = "CONSULTA_AGENDA_SEM_RESPONSAVEL";
	private static final String PAGINA_FORMULARIO = "/pages/agenda/geral/agenda-geral-form.xhtml?faces-redirect=true";
	private static final String CUMPRIDO = "Cumprido";

	@Inject
	private PessoaFisicaSessionBean pessoaFisicaSessionBean;
	@Inject
	private PessoaSessionBean pessoaSessionBean;
	@Inject
	private EventoSessionBean eventoSessionBean;
	@Inject
	private StatusAgendaSessionBean statusAgendaSessionBean;
	@Inject
	private EmailSessionBean emailSessionBean;
	@Inject
	private EmailCaixaSaidaSessionBean emailCaixaSaidaSessionBean;

	private Evento eventoSelecionado = new Evento();
	private ScheduleEvent event = new DefaultScheduleEvent();
	private Map<PessoaFisica, List<Evento>> pessoaEventosMap = Maps.newHashMap();
	private List<PessoaAgendaDto> menuPessoas = Lists.newArrayList();
	private String pessoaFisicaSelecionada;
	private Evento eventoAnterior;

	@PostConstruct
	public void initializeAgendas() {
		String flash = (String) getFlashScope().get(PESSOA_SELECT_FLASH);
		if (!Strings.isNullOrEmpty(flash)) {
			pessoaFisicaSelecionada = flash;
		} else {
			pessoaFisicaSelecionada = TODOS;
		}
		carregaPessoaEventosDaAgenda();
		carregaLazyEvent();
	}

	private void carregaPessoaEventosDaAgenda() {
		List<PessoaFisica> pessoas = getAdvogadosColaboradores();
		for (PessoaFisica pf : pessoas) {
			List<Evento> eventos = getEventosDaPessoa(pf);
			if (!eventos.isEmpty()) {
				pessoaEventosMap.put(pf, eventos);
			}
		}
		carregaMenuAgenda();
	}

	private void carregaLazyEvent() {
		if (!Strings.isNullOrEmpty(pessoaFisicaSelecionada)) {
			carregaLazyEventModel();
		} else {
			setLazyEventModel(new LazyScheduleModel());
		}
	}

	@Override
	protected List<Evento> listaDeEventos() {
		return carregaEventos(getDataInicioAgenda(), getDataFimAgenda());
	}


	private List<Evento> carregaEventos(Date start, Date end) {
		if (TODOS.equals(pessoaFisicaSelecionada)) {
			return getTodosEventosDaEmpresa(start, end);
		}
		if(INDEFINIDO.equals(pessoaFisicaSelecionada)){
			return getEventosPessoasIndefinidas(start, end);
		}

		Pessoa pessoa = pessoaSessionBean.findByNome(pessoaFisicaSelecionada, getStateManager().getEmpresaSelecionada().getId());
		eventoSessionBean.clearSession();
		return eventoSessionBean.findUserEvents(pessoa, start, end, getStateManager().getEmpresaSelecionada().getId());
	}

	public void onEventSelect(SelectEvent selectEvent) {
		event = (ScheduleEvent) selectEvent.getObject();
		setEventoSelecionado(eventoSessionBean.findByChaveId((String) event.getData()));
		RequestContext.getCurrentInstance().execute("PF('eventDialog').show();");
	}

	public void selectPessoa(String nomePessoaFisica) {
		pessoaFisicaSelecionada = nomePessoaFisica;
		getFlashScope().put(PESSOA_SELECT_FLASH, pessoaFisicaSelecionada);
	}

	public List<StatusAgenda> getStatus(){
		return statusAgendaSessionBean.findAllStatusAtivos(getStateManager().getEmpresaSelecionada().getId());
	}

	private void carregaMenuAgenda() {
		for (Entry<PessoaFisica, List<Evento>> entry : pessoaEventosMap.entrySet()) {
			Pessoa pessoa = entry.getKey().getPessoa();
			PessoaAgendaDto pessoaEstilo = new PessoaAgendaDto(pessoa.getDescricao(), pessoa.getImagem(), pessoa.getNomeIniciais());
			menuPessoas.add(pessoaEstilo);
		}

		if (!menuPessoas.isEmpty()) {
			Collections.sort(menuPessoas, (o1, o2) -> o1.toString().compareTo(o2.toString()));
			menuPessoas.add(0, new PessoaAgendaDto(TODOS, null, null));

			if (isHasAccessAgendaSemResponsavel()) {
				boolean existeEventoComResponsavelIndefinido = eventoSessionBean.isExisteEventoComPessoaIndefinida(getStateManager().getEmpresaSelecionada().getId());
				if (existeEventoComResponsavelIndefinido) {
					menuPessoas.add(1, new PessoaAgendaDto(INDEFINIDO, null, null));
				}
			}

		}
		setPessoaMenuList(menuPessoas);
	}

	public void addEvent(ActionEvent actionEvent) {
		try {
			atualizarEvento();
			RequestContext.getCurrentInstance().execute("PF('myschedule').update(); PF('eventDialog').hide();");
		} catch (ValidationException e) {
			LOGGER.error(e, e.getCause());
		}
	}

	public void transferenciaChanged(ValueChangeEvent event) {
		boolean transferir = (boolean) event.getNewValue();
		if (!transferir && eventoSelecionado!= null) {
			Pessoa pessoa = pessoaSessionBean.retrieve(getUser().getId());
			this.eventoSelecionado.setPessoa(pessoa);
		}
	}

	public void excluir() throws ValidationException {
		getLazyEventModel().updateEvent(event);
		if (getEventoSelecionado() != null) {
			eventoSessionBean.clearSession();
			eventoSessionBean.delete(getEventoSelecionado().getId());
		}
		enviarEmailRemocao(getEventoSelecionado());
	}

	public void statusChanged(ValueChangeEvent event){
		StatusAgenda statusAgenda = (StatusAgenda) event.getNewValue();
		this.eventoSelecionado.setStatusAgenda(statusAgenda);
	}

	private void enviarEmailRemocao(Evento evento){
		Email email = emailSessionBean.findBySigla(AGENDA_EXCLUIR_EVENTO, getStateManager().getEmpresaSelecionada().getId());
		Map<String, String> parametersValues = Maps.newHashMap();

		if (email == null) {
			exibirMensagem(FacesMessage.SEVERITY_INFO, "Email para Exclusão de evento não cadastrado...");
			return;
		}

		for (EmailVinculoParametro parametro : email.getParametroEmails()) {
			String valor = AgendaEmailBusiness.execute(parametro.getParametroEmail(), evento, new Evento(), getUser());
			parametersValues.put(parametro.getParametroEmail().getSigla(), valor);
		}

		String textoFormatado = EmailTemplateBusiness.criarEmailTemplate(email.getAssunto(), email.getDescricao(), parametersValues);
		enviarEmail(email, textoFormatado);
	}

	@Override
	protected void updateEvento() {
		try {
			Pessoa pessoa = this.eventoSelecionado.getPessoa();

			if(pessoa != null) {
				this.eventoSelecionado.setNomeIniciais(pessoa.getNomeIniciais());
			}
			if(this.eventoSelecionado.getAndamento() != null && this.eventoSelecionado.getPrazoAndamento() == null){
				this.eventoSelecionado.getAndamento().setStatusAgenda(this.eventoSelecionado.getStatusAgenda());
			} else if(this.eventoSelecionado.getPrazoAndamento() != null) {
				this.eventoSelecionado.getPrazoAndamento().setStatusAgenda(this.eventoSelecionado.getStatusAgenda());
			}

			adicionaControladoria(Boolean.TRUE);
			HistoricoEvento historicoEvento = new HistoricoEvento(this.eventoSelecionado, getUser().getName());
			historicoEvento.setTipoOperacao(TipoOperacaoIndicador.ALT);
			this.eventoSelecionado.addHistorico(historicoEvento);
			carregarEventoAnterior();
			eventoSessionBean.update(this.eventoSelecionado);
			enviarEmailAlteracao();
		} catch (ValidationException e) {
			LOGGER.error(e, e.getCause());
		}
	}

	private void adicionaControladoria(boolean edicao) {
		boolean possuiConfControladoria = getUser().getEmpresa() == null ? Boolean.FALSE : getUser().getEmpresa().isConfiguracaoControladoria();
		ControladoriaContext context = new ControladoriaContext();
		ControladoriaState aprovacaoState = new AprovaControladoria();
		context.setState(aprovacaoState);

		this.eventoSelecionado.setControladoriaStatus(context.controladoriaAction(
				edicao, this.eventoSelecionado.getStatusAgenda(),
				possuiConfControladoria,
				ControladoriaIndicador.fromValue(this.eventoSelecionado.getControladoriaStatus())).getStatus());
	}

	private void carregarEventoAnterior() throws ValidationException {
		Evento evento = new Evento();
		eventoSessionBean.clearSession();
		if (this.eventoSelecionado != null && this.eventoSelecionado.getId() != null) {
			evento = eventoSessionBean.retrieve(this.eventoSelecionado.getId());
		}
		this.eventoAnterior = evento;
		eventoSessionBean.clearSession();
	}

	private void atualizarEvento() throws ValidationException {
		super.atualizarEvento(getEventoSelecionado(), this.event);
		setEventoSelecionado(null);
	}

	private void addDestinatarios(EmailCaixaSaida caixaSaida) {
		List<Pessoa> administradores = pessoaSessionBean.findByAdministradores(getStateManager().getEmpresaSelecionada().getId());
		super.addDestinatarios(caixaSaida, administradores);
	}

	private void enviarEmailAlteracao() {
		Email email = emailSessionBean.findBySigla(AGENDA_ALTERACAO_AGENDA, getStateManager().getEmpresaSelecionada().getId());
		Map<String, String> parametersValues = Maps.newHashMap();

		if (email == null) {
			exibirMensagem(FacesMessage.SEVERITY_INFO, "Email para Alteração de agenda não cadastrado...");
			return;
		}

		for (EmailVinculoParametro parametro : email.getParametroEmails()) {
			String valor = AgendaEmailBusiness.execute(parametro.getParametroEmail(), this.eventoAnterior, getEventoSelecionado(), getUser());
			parametersValues.put(parametro.getParametroEmail().getSigla(), valor);
		}

		String textoFormatado = EmailTemplateBusiness.criarEmailTemplate(email.getAssunto(), email.getDescricao(), parametersValues);
		enviarEmail(email, textoFormatado);
	}

	@Override
	protected boolean enviarEmail(Email email, String textoFormatado) {
		try {
			EmailCaixaSaida emailCaixaSaida = new EmailCaixaSaida(getUser().getName(), email.getAssunto(), textoFormatado, getStateManager().getEmpresaSelecionada().getId());
			addDestinatarios(emailCaixaSaida);
			sendEmailCaixaSaida(email, emailCaixaSaida);
			return true;

		} catch (ValidationException e) {
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	private void sendEmailCaixaSaida(Email email, EmailCaixaSaida emailCaixaSaida) throws ValidationException {
		if(!email.getNecessarioAprovacao()){
			emailCaixaSaidaSessionBean.createSemAprovacao(emailCaixaSaida);
		} else {
			emailCaixaSaidaSessionBean.create(emailCaixaSaida);
		}
	}

	private StatusAgenda getStatusAgendaCumprido() {
		return statusAgendaSessionBean.findByDescricao(CUMPRIDO, getStateManager().getEmpresaSelecionada().getId());
	}

	public boolean isRenderizaHouveAcordoAndamento() {
		if(eventoSelecionado == null || eventoSelecionado.getAndamento() == null || eventoSelecionado.getPrazoAndamento() != null) {
			return false;
		}
		StatusAgenda prazoCumprido = getStatusAgendaCumprido();
		boolean prazoAtendido = eventoSelecionado.getAndamento().getStatusAgenda() != null
				&& prazoCumprido != null
				&& prazoCumprido.getId().equals(eventoSelecionado.getStatusAgenda().getId());

		return prazoAtendido && (TipoAndamentoBusiness.isAudienciaConciliacao(eventoSelecionado.getAndamento().getTipoAndamento())
				|| TipoAndamentoBusiness.isAudienciaInstrucao(eventoSelecionado.getAndamento().getTipoAndamento())
				|| TipoAndamentoBusiness.isAudienciaOitiva(eventoSelecionado.getAndamento().getTipoAndamento()));
	}

	public String goToAgendaGeral() {
		return PAGINA_FORMULARIO;
	}

	public List<PessoaFisica> getAdvogadosColaboradores() {
		return pessoaFisicaSessionBean.findByAdvogadosAndColaboradores(getStateManager().getEmpresaSelecionada().getId());
	}

	public List<PessoaFisica> getAdvogadosColaboradoresEscritorioList() {
		return pessoaFisicaSessionBean.findByAdvogadosAndColaboradores(getStateManager().getEmpresaSelecionada().getId());
	}

	public List<Evento> getEventosDaPessoa(PessoaFisica pessoaFisica) {
		eventoSessionBean.clearSession();
		return eventoSessionBean.findByPessoaFisica(pessoaFisica, getStateManager().getEmpresaSelecionada().getId());
	}

	private List<Evento> getTodosEventosDaEmpresa(Date start, Date end) {
		eventoSessionBean.clearSession();
		return eventoSessionBean.findByEventosDaEmpresa(start, end, getStateManager().getEmpresaSelecionada().getId(), isHasAccessAgendaSemResponsavel());
	}

	private List<Evento> getEventosPessoasIndefinidas(Date start, Date end){
		eventoSessionBean.clearSession();
		return eventoSessionBean.findByEventosDaEmpresaPessoaIndefinida(start, end, getStateManager().getEmpresaSelecionada().getId());
	}

	public Map<PessoaFisica, List<Evento>> getPessoaEventosMap() {
		return pessoaEventosMap;
	}

	public List<PessoaAgendaDto> getMenuPessoas() {
		return menuPessoas;
	}

	public String getPessoaFisicaSelecionada() {
		return pessoaFisicaSelecionada;
	}

	public void setPessoaFisicaSelecionada(String pessoaFisicaSelecionada) {
		this.pessoaFisicaSelecionada = pessoaFisicaSelecionada;
	}

	public String getTimeZone() {
		return TIME_ZONE;
	}

	public Evento getEventoSelecionado() {
		return eventoSelecionado;
	}

	public void setEventoSelecionado(Evento eventoSelecionado) {
		this.eventoSelecionado = eventoSelecionado;
	}

	public boolean isHasAccess(){
		return possuiAcesso(CHAVE_ACESSO);
	}

	public boolean isHasAccessTransferir(){
		return possuiAcesso(TRANSFERIR_AGENDA);
	}

	public boolean isHasAccessAgendaSemResponsavel() {
		return possuiAcesso(CONSULTA_AGENDA_SEM_RESPONSAVEL);
	}

}
