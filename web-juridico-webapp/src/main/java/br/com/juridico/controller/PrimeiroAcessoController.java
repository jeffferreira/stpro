package br.com.juridico.controller;

import br.com.juridico.ejb.StateManager;
import br.com.juridico.entidades.Empresa;
import br.com.juridico.entidades.Pessoa;
import br.com.juridico.entidades.User;
import br.com.juridico.entidades.UserPrimeiroAcesso;
import br.com.juridico.impl.EmpresaSessionBean;
import br.com.juridico.impl.PessoaSessionBean;
import br.com.juridico.util.DateUtils;
import br.com.juridico.util.EncryptUtil;
import org.apache.log4j.Logger;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Jefferson on 09/08/2016.
 */
@RequestScoped
@ManagedBean
public class PrimeiroAcessoController extends AbstractController {

    private static final Logger LOGGER = Logger.getLogger(PrimeiroAcessoController.class);
    private static final String USER_PRIMEIRO_ACESSO_LOGGED = "userPrimeiroAcessoLogged";

    private String novaSenha;
    private String confirmacaoSenha;

    @Inject
    private MenuController menuController;
    @Inject
    private PessoaSessionBean pessoaSessionBean;
    @Inject
    private EmpresaSessionBean empresaSessionBean;
    @Inject
    private StateManager stateManager;

    public String alterarSenha(){
        UserPrimeiroAcesso user = getUserPrimeiroAcesso();
        List<Pessoa> pessoas = pessoaSessionBean.findUserList(user.getLogin());

        if(pessoas.isEmpty()) {
            exibirMensagem(FacesMessage.SEVERITY_ERROR, "Login não encontrado.");
            return "";
        }
        if(!novaSenha.equals(confirmacaoSenha)) {
            exibirMensagem(FacesMessage.SEVERITY_ERROR, "Confirmação: Senhas não conferem.");
            return "";
        }
        pessoas.forEach(this::updateSenha);
        pessoaSessionBean.clearSession();
        Pessoa pessoa = pessoaSessionBean.retrieve(user.getId());
        carregaUsuarioNaSessao(pessoa);
        return menuController.goToHome();
    }

    public void updateSenha(Pessoa pessoa){
        try {
            pessoa.getLogin().setSenha(EncryptUtil.execute(this.novaSenha));
            pessoa.getLogin().setPrimeiroAcesso(false);
            pessoa.getLogin().setDataUltimoAcesso(Calendar.getInstance().getTime());
            pessoaSessionBean.update(pessoa);
            removeSessionAttribute(USER_PRIMEIRO_ACESSO_LOGGED);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    private void carregaUsuarioNaSessao(Pessoa pessoa) {
        Empresa empresa = empresaSessionBean.retrieve(pessoa.getCodigoEmpresa());
        stateManager.setEmpresaSelecionada(empresa);
        User userLogged = setUser(pessoa, empresa);
        setSessionAttribute(USER_LOGGED, userLogged);
        setSessionAttribute("perfil", pessoa.getPerfil().getDescricao());
        setSessionAttribute("dataCadastro", DateUtils.formataDataBrasil(pessoa.getDataCadastro()));
    }

    public String getNovaSenha() {
        return novaSenha;
    }

    public void setNovaSenha(String novaSenha) {
        this.novaSenha = novaSenha;
    }

    public String getConfirmacaoSenha() {
        return confirmacaoSenha;
    }

    public void setConfirmacaoSenha(String confirmacaoSenha) {
        this.confirmacaoSenha = confirmacaoSenha;
    }
}
