package br.com.juridico.controller;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.servlet.http.Part;

import com.google.common.collect.Lists;

import br.com.juridico.entidades.AndamentoAtendimento;
import br.com.juridico.entidades.Atendimento;
import br.com.juridico.entidades.Pessoa;
import br.com.juridico.entidades.TipoAtendimento;
import br.com.juridico.entidades.User;
import br.com.juridico.exception.CrudException;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.impl.AndamentoAtendimentoSessionBean;
import br.com.juridico.impl.AtendimentoSessionBean;
import br.com.juridico.impl.PessoaSessionBean;
import br.com.juridico.impl.TipoAtendimentoSessionBean;

/**
 * 
 * @author Jefferson
 *
 */
@ManagedBean
@ViewScoped
public class AtendimentoClienteController extends CrudController<Atendimento> {

	/**
     * 
     */
	private static final long serialVersionUID = 843505264485399749L;

	private static final String CHAVE_ACESSO = "";

	@Inject
	private TipoAtendimentoSessionBean tipoAtendimentoSessionBean;
	@Inject
	private AtendimentoSessionBean atendimentoSessionBean;
	@Inject
	private AndamentoAtendimentoSessionBean andamentoAtendimentoSessionBean;
	@Inject
	private PessoaSessionBean pessoaSessionBean;

	private List<TipoAtendimento> tipos;

	private Part arquivo;

	private Atendimento atendimentoSelecionado;
	private AndamentoAtendimento andamentoAtendimento;

	private Pessoa pessoaLogada;
	private String descricao;

	/**
	 * 
	 */
	@PostConstruct
	public void initialize() {
		this.tipos = tipoAtendimentoSessionBean.retrieveAll(getUser().getCodEmpresa());
		carregaAtendimento();
	}

	@Override
	protected void carregaLazyDataModel() {
		// LAZY
	}

	private void carregaAtendimento() {
		User usuarioLogado = getUser();
		pessoaLogada = pessoaSessionBean.retrieve(usuarioLogado.getId());
	}

	/**
	 * 
	 * @return
	 */
	public String goToClienteAtendimento() {
		return "/pages/atendimento/cliente/atendimento-cliente-template.xhtml";
	}

	protected Atendimento instanciarObjetoNovo() {
		return new Atendimento();
	}

	protected Atendimento inicializaObjetosLazy(Atendimento objeto) {
		atendimentoSessionBean.clearSession();
		return atendimentoSessionBean.retrieve(objeto.getId());
	}

	public List<TipoAtendimento> getTipos() {
		return tipos;
	}

	public void setTipos(List<TipoAtendimento> tipos) {
		this.tipos = tipos;
	}

	public Part getArquivo() {
		return arquivo;
	}

	public void setArquivo(Part arquivo) {
		this.arquivo = arquivo;
	}

	@Override
	protected void create(Atendimento objetoSelecionado) throws ValidationException, CrudException {
		if (getAtendimentoSelecionado() == null) {
			atendimentoSessionBean.create(objetoSelecionado);
			createAndamentoAtendimento();
		} else {
			createAndamentoAtendimento();
		}

	}

	private void createAndamentoAtendimento() throws ValidationException {
		andamentoAtendimento = new AndamentoAtendimento();
		andamentoAtendimento.setDescricao(descricao);
		andamentoAtendimento.setPessoa(pessoaLogada);
		andamentoAtendimento.setDataCadastro(new Date());

		if (getAtendimentoSelecionado() == null) {
			andamentoAtendimento.setAtendimento(getObjetoSelecionado());
		} else {
			andamentoAtendimento.setAtendimento(getAtendimentoSelecionado());
		}

		andamentoAtendimentoSessionBean.create(andamentoAtendimento);
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Atendimento getAtendimentoSelecionado() {
		return atendimentoSelecionado;
	}

	public void setAtendimentoSelecionado(Atendimento atendimentoSelecionado) {
		this.atendimentoSelecionado = atendimentoSelecionado;
	}

	@Override
	protected void setEmpresaDeUsuarioLogado(Atendimento objetoSelecionado) {
		objetoSelecionado.setCodigoEmpresa(getUser().getCodEmpresa());

	}

	@Override
	protected void addChaveAcesso() {
		setChaveAcesso(CHAVE_ACESSO);
	}

	@Override
	protected boolean isPodeAlterar() {
		return false;
	}

	@Override
	protected boolean isPodeExcluir() {
		return false;
	}

	@Override
	protected boolean isPodeIncluir() {
		return false;
	}

}
