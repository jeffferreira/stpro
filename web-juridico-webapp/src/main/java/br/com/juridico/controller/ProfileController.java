package br.com.juridico.controller;

import br.com.juridico.entidades.Imagem;
import br.com.juridico.entidades.Pessoa;
import br.com.juridico.entidades.User;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.impl.ImagemSessionBean;
import br.com.juridico.impl.PessoaSessionBean;
import br.com.juridico.util.FileUtil;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.primefaces.model.StreamedContent;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;

/**
 * Created by jeffersonl2009_00 on 21/10/2016.
 */
@ManagedBean
@RequestScoped
public class ProfileController extends AbstractController {

    private static final Logger LOGGER = Logger.getLogger(ProfileController.class);
    private static final String PATH_AVATAR_PADRAO = "/resources/images/avatar/avatar_64.png";
    private static final String PAGINA_PERFIL = "/pages/profile.xhtml?faces-redirect=true";

    @Inject
    private PessoaSessionBean pessoaSessionBean;
    @Inject
    private ImagemSessionBean imagemSessionBean;

    private StreamedContent fotoAtual;
    private Part fotoSelecionada;
    private Imagem imagem;

    @PostConstruct
    public void initialize(){
        carregaImagem();
    }

    private void carregaImagem() {
        if(getUser() == null) {
            return;
        }
        if(getUser().getImagem() != null) {
            this.imagem = getUser().getImagem();
        } else {
            String absoluteDiskPath = getExternalContext().getRealPath(PATH_AVATAR_PADRAO);
            File file = new File(absoluteDiskPath);
            try {
                this.imagem = new Imagem(FileUtil.getContent(file), "avatar_64", ".png");
            } catch (IOException e) {
                LOGGER.error(e, e.getCause());
            }
        }
    }

    public void importar() {
        if(fotoSelecionada == null) {
            return;
        }
        try {
            byte[] bytes = IOUtils.toByteArray(fotoSelecionada.getInputStream());
            String nomeArquivo = FileUtil.getFileName(fotoSelecionada);
            String extensao = nomeArquivo.substring(nomeArquivo.lastIndexOf("."), nomeArquivo.length());

            if(this.imagem.getId() == null) {
                this.imagem = new Imagem(bytes, nomeArquivo, extensao);
                imagemSessionBean.create(imagem);
            } else {
                this.imagem.setLogo(bytes);
                this.imagem.setNomeArquivo(nomeArquivo);
                this.imagem.setExtensaoArquivo(extensao);
                imagemSessionBean.update(imagem);
            }

            Pessoa pessoa = pessoaSessionBean.retrieve(getUser().getId());
            pessoa.setImagem(this.imagem);
            pessoaSessionBean.update(pessoa);
            carregaImagemUsuarioSessao();
            getFacesContext().getApplication().getNavigationHandler().handleNavigation(getFacesContext(), null, PAGINA_PERFIL);

        } catch (IOException e) {
            LOGGER.error(e, e.getCause());
        } catch (ValidationException e) {
            LOGGER.error(e, e.getCause());
        }
    }

    private void carregaImagemUsuarioSessao() {
        User userLogged = getUser();
        userLogged.setImagem(this.imagem);
        setSessionAttribute(USER_LOGGED, userLogged);
    }

    public Part getFotoSelecionada() {
        return fotoSelecionada;
    }

    public void setFotoSelecionada(Part fotoSelecionada) {
        this.fotoSelecionada = fotoSelecionada;
    }

    public StreamedContent getFotoAtual() {
        return fotoAtual;
    }

    public void setFotoAtual(StreamedContent fotoAtual) {
        this.fotoAtual = fotoAtual;
    }

    public Imagem getImagem() {
        return imagem;
    }

    public void setImagem(Imagem imagem) {
        this.imagem = imagem;
    }


}
