package br.com.juridico.controller;

import br.com.juridico.entidades.ProjudiprLogin;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.impl.ProjudiprLoginBean;
import br.com.juridico.util.DecryptUtil;
import br.com.juridico.util.EncryptUtil;
import org.apache.log4j.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

/**
 * Created by jefferson.ferreira on 31/03/2017.
 */
@ManagedBean
@ViewScoped
public class ProjudiprLoginController extends AbstractController {

    private static final Logger LOGGER = Logger.getLogger(ProjudiprLoginController.class);

    @Inject
    private ProjudiprLoginBean projudiprLoginBean;

    private ProjudiprLogin projudiprLogin;
    private String login;
    private String senha;

    private boolean editando;

    public String goToEditar(ProjudiprLogin projudiprLogin){
        try {
            this.editando = Boolean.TRUE;
            limparCampos(projudiprLogin);
        } catch (Exception e) {
            LOGGER.error(e, e.getCause());
        }
        return "";
    }

    private void limparCampos(ProjudiprLogin projudiprLogin) throws Exception {
        this.projudiprLogin = projudiprLogin;
        this.login = this.projudiprLogin.getLogin();
        this.senha = DecryptUtil.execute(this.projudiprLogin.getSenha());
    }

    public void salvarEditarLogin(){
        if (!editando){
            projudiprLogin = new ProjudiprLogin(getUser().getCodEmpresa());
            salvar();
            this.editando = Boolean.FALSE;
            return;
        }
        editar();
        this.editando = Boolean.FALSE;
    }

    public void cancelar(){
        this.login = "";
        this.senha = "";
        this.projudiprLogin = null;
        this.editando = false;
    }

    private void editar(){
        try {
            byte[] encrypted = EncryptUtil.execute(this.senha);
            this.projudiprLogin.setLogin(this.login);
            this.projudiprLogin.setSenha(encrypted);
            projudiprLoginBean.update(this.projudiprLogin);
            exibirMensagem(FacesMessage.SEVERITY_INFO, getMessage("msg_sucesso_salvar"));
        } catch (ValidationException e) {
            for (String message : e.getMessages()) {
                exibirMensagem(FacesMessage.SEVERITY_ERROR, message);
            }
        } catch (Exception e) {
            LOGGER.error(e, e.getCause());
        }
    }

    private void salvar(){
        boolean existeLoginCadastrado = projudiprLoginBean.findByLoginEmpresa(getUser().getCodEmpresa()) != null;
        try {
            byte[] encrypted = EncryptUtil.execute(this.senha);
            this.projudiprLogin.setLogin(this.login);
            this.projudiprLogin.setSenha(encrypted);
            this.projudiprLogin.validar(existeLoginCadastrado);
            projudiprLoginBean.create(this.projudiprLogin);
            exibirMensagem(FacesMessage.SEVERITY_INFO, getMessage("msg_sucesso_salvar"));
            limparCampos(this.projudiprLogin);

        } catch (ValidationException e) {
            for (String message : e.getMessages()) {
                exibirMensagem(FacesMessage.SEVERITY_ERROR, message);
            }
        } catch (Exception e) {
            LOGGER.error(e, e.getCause());
        }
    }

    public void excluir() {
        projudiprLoginBean.delete(this.projudiprLogin);
        exibirMensagem(FacesMessage.SEVERITY_INFO, getMessage("msg_sucesso_excluir"));
    }

    public ProjudiprLogin getProjudiprLoginEmpresa() {
        return projudiprLoginBean.findByLoginEmpresa(getUser().getCodEmpresa());
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public ProjudiprLogin getProjudiprLogin() {
        return projudiprLogin;
    }

    public void setProjudiprLogin(ProjudiprLogin projudiprLogin) {
        this.projudiprLogin = projudiprLogin;
    }

    public boolean isEditando() {
        return editando;
    }

    public void setEditando(boolean editando) {
        this.editando = editando;
    }
}
