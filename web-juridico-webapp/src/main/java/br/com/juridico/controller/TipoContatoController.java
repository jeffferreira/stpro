package br.com.juridico.controller;

import br.com.juridico.datamodel.LazyEntityDataModel;
import br.com.juridico.entidades.Direito;
import br.com.juridico.entidades.TipoContato;
import br.com.juridico.exception.CrudException;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.impl.DireitoSessionBean;
import br.com.juridico.impl.PerfilAcessoSessionBean;
import br.com.juridico.impl.TipoContatoSessionBean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Jefferson
 *
 */
@ManagedBean
@SessionScoped
public class TipoContatoController extends CrudController<TipoContato> {

	/**
	 *
	 */
	private static final long serialVersionUID = 3748906916756454729L;
	private static final String TIPO_CONTATO_FORM = "tipoContatoForm";
	private static final String CHAVE_ACESSO = "TIPOCONTATO_TIPO_CONTATO_TEMPLATE";
	private static final String PAGINA_FORMULARIO = "/pages/cadastros/tipoContato/tipo-contato-template.xhtml?faces-redirect=true";

	@Inject
	private TipoContatoSessionBean tipoContatoSessionBean;
	@Inject
	private PerfilAcessoSessionBean perfilAcessoSessionBean;
	@Inject
	private DireitoSessionBean direitoSessionBean;

	public void init(){
		carregaLazyDataModel();
	}

	@Override
	protected void validar() throws ValidationException {
		tipoContatoSessionBean.clearSession();
		boolean jaExisteTipoContato = tipoContatoSessionBean.isDescricaoJaCadastrada(getObjetoSelecionado(), getStateManager().getEmpresaSelecionada().getId());
		getObjetoSelecionado().validar(jaExisteTipoContato);
	}

	@Override
	protected void carregaLazyDataModel() {
		setLazyDataModel(new LazyEntityDataModel<TipoContato>() {
			@Override
			protected List<TipoContato> consultaFiltrada(ConsultaLazyFilter filtro) {
				filtro.setDescricao(getDescricaoFiltro());
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return tipoContatoSessionBean.filtrados(filtro);
			}

			@Override
			protected int countTotalRegistros(ConsultaLazyFilter filtro) {
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return tipoContatoSessionBean.quantidadeFiltrados(filtro);
			}
		});
	}

	@Override
	protected void create(TipoContato tipoContato) throws ValidationException, CrudException {
		TipoContato tipoContatoExclusao = getRegistroComExclusaoLogica();

		if (isEditavel() && tipoContatoExclusao != null) {
			tipoContatoExclusao.setDataExclusao(null);
			tipoContatoSessionBean.update(tipoContatoExclusao);
		} else if (tipoContato != null && tipoContato.getDescricao() != null && !tipoContato.getDescricao().isEmpty()) {
			tipoContatoSessionBean.create(tipoContato);
		} else {
			throw new CrudException();
		}

	}

	private TipoContato getRegistroComExclusaoLogica() {
		return tipoContatoSessionBean.findByDescricao(getObjetoSelecionado().getDescricao(), getStateManager().getEmpresaSelecionada().getId());
	}

	@Override
	protected void antesDeExcluir() throws CrudException {
		getObjetoSelecionado().setDataExclusao(new Date());
	}

	@Override
	protected void aposExcluir() throws CrudException {
		super.buscarPorLazy();
	}

	@Override
	protected void aposCancelar() throws CrudException {
		getObjetoSelecionado().setDescricao(null);
		updateContext(TIPO_CONTATO_FORM);
		super.aposCancelar();
	}

	/**
	 *
	 * @return
	 */
	public String goToFormulario() {
		return PAGINA_FORMULARIO;
	}

	@Override
	protected TipoContato inicializaObjetosLazy(TipoContato objeto) {
		tipoContatoSessionBean.clearSession();
		return tipoContatoSessionBean.retrieve(objeto.getId());
	}

	@Override
	protected TipoContato instanciarObjetoNovo() {
		return new TipoContato();
	}

	@Override
	protected void setEmpresaDeUsuarioLogado(TipoContato objetoSelecionado) {
		objetoSelecionado.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());

	}

	@Override
	protected void addChaveAcesso() {
		setChaveAcesso(CHAVE_ACESSO);
	}

	@Override
	protected boolean isPodeAlterar(){
		Direito direito = direitoSessionBean.retrieve(ALTERAR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

	@Override
	protected boolean isPodeExcluir(){
		Direito direito = direitoSessionBean.retrieve(EXCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

	@Override
	protected boolean isPodeIncluir(){
		Direito direito = direitoSessionBean.retrieve(INCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}
}
