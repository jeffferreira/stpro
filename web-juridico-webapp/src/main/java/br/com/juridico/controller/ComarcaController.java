package br.com.juridico.controller;

import br.com.juridico.datamodel.LazyEntityDataModel;
import br.com.juridico.entidades.Comarca;
import br.com.juridico.entidades.Direito;
import br.com.juridico.entidades.Uf;
import br.com.juridico.exception.CrudException;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.impl.ComarcaSessionBean;
import br.com.juridico.impl.DireitoSessionBean;
import br.com.juridico.impl.PerfilAcessoSessionBean;
import br.com.juridico.impl.UfSessionBean;
import org.primefaces.model.LazyDataModel;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ValueChangeEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Jefferson
 *
 */
@ManagedBean
@SessionScoped
public class ComarcaController extends CrudController<Comarca> {

	/**
	 *
	 */
	private static final long serialVersionUID = 7506255478099069711L;
	private static final String COMARCA_FORM = "comarcaForm";
	private static final String CHAVE_ACESSO = "COMARCA_COMARCA_TEMPLATE";
	private static final String PAGINA_FORMULARIO = "/pages/cadastros/comarca/comarca-template.xhtml?faces-redirect=true";

	@Inject
	private ComarcaSessionBean comarcaSessionBean;
	@Inject
	private PerfilAcessoSessionBean perfilAcessoSessionBean;
	@Inject
	private DireitoSessionBean direitoSessionBean;
	@Inject
	private UfSessionBean ufSessionBean;

	public void init(){
		carregaLazyDataModel();
	}

	@Override
	protected void validar() throws ValidationException {
		comarcaSessionBean.clearSession();
		boolean jaExisteComarca = comarcaSessionBean.isDescricaoJaCadastrada(getObjetoSelecionado(), getStateManager().getEmpresaSelecionada().getId());
		getObjetoSelecionado().validar(jaExisteComarca);
	}

	@Override
	protected void create(Comarca comarca) throws ValidationException, CrudException {
		Comarca comarcaExclusao = getRegistroComExclusaoLogica();

		if (isEditavel() && comarcaExclusao != null) {
			comarca.setDataExclusao(null);
			comarcaSessionBean.update(comarca);
		} else if (comarca != null && comarca.getDescricao() != null && !comarca.getDescricao().isEmpty()) {
			comarcaSessionBean.create(comarca);
		} else {
			throw new CrudException();
		}

	}

	@Override
	protected void aposCancelar() throws CrudException {
		getObjetoSelecionado().setDescricao(null);
		updateContext(COMARCA_FORM);
		super.aposCancelar();
	}

	@Override
	protected void antesDeExcluir() throws CrudException {
		getObjetoSelecionado().setDataExclusao(new Date());
	}

	@Override
	protected void aposExcluir() throws CrudException {
		super.buscarPorLazy();
	}

	@Override
	protected Comarca inicializaObjetosLazy(Comarca objeto) {
		comarcaSessionBean.clearSession();
		return comarcaSessionBean.retrieve(objeto.getId());
	}

	@Override
	protected Comarca instanciarObjetoNovo() {
		return new Comarca();
	}

	@Override
	protected void setEmpresaDeUsuarioLogado(Comarca objetoSelecionado) {
		objetoSelecionado.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
	}

	@Override
	protected void carregaLazyDataModel() {
		setLazyDataModel(new LazyEntityDataModel<Comarca>() {
			@Override
			protected List<Comarca> consultaFiltrada(ConsultaLazyFilter filtro) {
				filtro.setDescricao(getDescricaoFiltro());
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return comarcaSessionBean.filtrados(filtro);
			}

			@Override
			protected int countTotalRegistros(ConsultaLazyFilter filtro) {
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return comarcaSessionBean.quantidadeFiltrados(filtro);
			}
		});
	}

	private Comarca getRegistroComExclusaoLogica() {
		return comarcaSessionBean.findByDescricao(getObjetoSelecionado().getDescricao(), getStateManager().getEmpresaSelecionada().getId());
	}

	/**
	 *
	 * @return
	 */
	public String goToFormulario() {
		return PAGINA_FORMULARIO;
	}

	public void ufChanged(ValueChangeEvent event){
		getObjetoSelecionado().setUf((Uf) event.getNewValue());
	}

	@Override
	protected void addChaveAcesso() {
		setChaveAcesso(CHAVE_ACESSO);
	}

	@Override
	protected boolean isPodeAlterar(){
		Direito direito = direitoSessionBean.retrieve(ALTERAR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

	@Override
	protected boolean isPodeExcluir(){
		Direito direito = direitoSessionBean.retrieve(EXCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

	@Override
	protected boolean isPodeIncluir(){
		Direito direito = direitoSessionBean.retrieve(INCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

	public List<Uf> getUfs(){
		return ufSessionBean.findAll();
	}

}
