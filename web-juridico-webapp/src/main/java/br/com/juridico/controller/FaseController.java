package br.com.juridico.controller;

import br.com.juridico.datamodel.LazyEntityDataModel;
import br.com.juridico.entidades.Direito;
import br.com.juridico.entidades.Fase;
import br.com.juridico.exception.CrudException;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.impl.DireitoSessionBean;
import br.com.juridico.impl.FaseSessionBean;
import br.com.juridico.impl.PerfilAcessoSessionBean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Jefferson
 *
 */
@ManagedBean
@SessionScoped
public class FaseController extends CrudController<Fase> {

	/**
	 *
	 */
	private static final long serialVersionUID = -3576844242935007490L;
	private static final String FASE_FORM = "faseForm";
	private static final String CHAVE_ACESSO = "FASE_FASE_TEMPLATE";
	private static final String PAGINA_FORMULARIO = "/pages/cadastros/fase/fase-template.xhtml?faces-redirect=true";

	@Inject
	private FaseSessionBean faseSessionBean;
	@Inject
	private PerfilAcessoSessionBean perfilAcessoSessionBean;
	@Inject
	private DireitoSessionBean direitoSessionBean;

	public void init(){
		carregaLazyDataModel();
	}

	@Override
	protected void validar() throws ValidationException {
		faseSessionBean.clearSession();
		boolean jaExisteFase = faseSessionBean.isDescricaoJaCadastrada(getObjetoSelecionado(), getStateManager().getEmpresaSelecionada().getId());
		getObjetoSelecionado().validar(jaExisteFase);
	}

	@Override
	protected void aposCancelar() throws CrudException {
		getObjetoSelecionado().setDescricao(null);
		updateContext(FASE_FORM);
		super.aposCancelar();
	}

	@Override
	protected void create(Fase fase) throws ValidationException, CrudException {
		Fase faseExclusao = getRegistroComExclusaoLogica();

		if (isEditavel() && faseExclusao != null) {
			faseExclusao.setDataExclusao(null);
			faseSessionBean.update(faseExclusao);
		} else if (fase != null && fase.getDescricao() != null && !fase.getDescricao().isEmpty()) {
			faseSessionBean.create(fase);
		} else {
			throw new CrudException();
		}

	}

	@Override
	protected void carregaLazyDataModel() {
		setLazyDataModel(new LazyEntityDataModel<Fase>() {
			@Override
			protected List<Fase> consultaFiltrada(ConsultaLazyFilter filtro) {
				filtro.setDescricao(getDescricaoFiltro());
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return faseSessionBean.filtrados(filtro);
			}

			@Override
			protected int countTotalRegistros(ConsultaLazyFilter filtro) {
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return faseSessionBean.quantidadeFiltrados(filtro);
			}
		});
	}

	@Override
	protected void antesDeExcluir() throws CrudException {
		getObjetoSelecionado().setDataExclusao(new Date());
	}

	@Override
	protected void aposExcluir() throws CrudException {
		super.buscarPorLazy();
	}

	@Override
	protected Fase inicializaObjetosLazy(Fase objeto) {
		faseSessionBean.clearSession();
		return faseSessionBean.retrieve(objeto.getId());
	}

	@Override
	protected Fase instanciarObjetoNovo() {
		return new Fase();
	}

	@Override
	protected void setEmpresaDeUsuarioLogado(Fase objetoSelecionado) {
		objetoSelecionado.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
	}

	private Fase getRegistroComExclusaoLogica() {
		return faseSessionBean.findByDescricao(getObjetoSelecionado().getDescricao(), getStateManager().getEmpresaSelecionada().getId());
	}

	/**
	 *
	 * @return
	 */
	public String goToFormulario() {
		return PAGINA_FORMULARIO;
	}

	@Override
	protected void addChaveAcesso() {
		setChaveAcesso(CHAVE_ACESSO);
	}

	@Override
	protected boolean isPodeAlterar(){
		Direito direito = direitoSessionBean.retrieve(ALTERAR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

	@Override
	protected boolean isPodeExcluir(){
		Direito direito = direitoSessionBean.retrieve(EXCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

	@Override
	protected boolean isPodeIncluir(){
		Direito direito = direitoSessionBean.retrieve(INCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

}
