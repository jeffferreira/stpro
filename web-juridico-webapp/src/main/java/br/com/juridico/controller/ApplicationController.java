package br.com.juridico.controller;

import com.google.common.collect.Maps;
import org.apache.log4j.Logger;

import javax.ejb.Startup;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Destroyed;
import javax.enterprise.context.Initialized;
import javax.enterprise.event.Observes;
import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * Created by jeffersonl2009_00 on 11/11/2016.
 */
@Startup
@ApplicationScoped
public class ApplicationController {

    private static final Logger LOGGER = Logger.getLogger(ApplicationController.class);

    private Map<String, HttpSession> sessionApplicationMap;

    public void init( @Observes @Initialized( ApplicationScoped.class ) Object init ) {
        LOGGER.info("INIT APPLICATION SCOPED");
        this.sessionApplicationMap = Maps.newHashMap();
        LOGGER.info(sessionApplicationMap.size());
    }

    public void destroy( @Observes @Destroyed( ApplicationScoped.class ) Object init ) {
        LOGGER.info("DESTROY APPLICATION SCOPED");
    }

    public Map<String, HttpSession> getSessionApplicationMap() {
        return sessionApplicationMap;
    }
}
