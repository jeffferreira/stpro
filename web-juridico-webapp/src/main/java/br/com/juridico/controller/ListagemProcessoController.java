package br.com.juridico.controller;

import br.com.juridico.business.CalculaRegraTresBusiness;
import br.com.juridico.entidades.*;
import br.com.juridico.filter.ProcessoFilter;
import br.com.juridico.impl.*;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import java.util.Iterator;
import java.util.List;

/**
 * Created by jeffersonl2009_00 on 25/07/2016.
 */
@ManagedBean
@SessionScoped
public class ListagemProcessoController extends AbstractController{

    private static final String WIDTH = "width: ";
	private static final String PROCESSOS_PROCESSO_FORM = "PROCESSOS_PROCESSO_FORM";
	private static final String PROCESSOS_PROCESSO_LIST = "PROCESSOS_PROCESSO_LIST";
	private static final String PROCESSOS_PESQUISA_TODOS = "PROCESSOS_PESQUISA_TODOS";

    @Inject
    private ProcessoSessionBean processoSessionBean;
	@Inject
	private PerfilAcessoSessionBean perfilAcessoSessionBean;
	@Inject
	private DireitoSessionBean direitoSessionBean;
	@Inject
	private PessoaFisicaSessionBean pessoaFisicaSessionBean;
	@Inject
	private PessoaSessionBean pessoaSessionBean;

    private ProcessoFilter filter;

    private List<ViewProcesso> processos;

    private int totalAtivos;
    private int totalSuspensos;
    private int totalFinalizados;
    private int totalizador;
    private String percentAtivos;
    private String percentSuspensos;
    private String percentFinalizados;
    private Pessoa pessoaLogada;

    public void initialize() {
        if(filter == null || !filter.isPossuiFiltroCarregado()) {
            this.processos = Lists.newArrayList();
        }
        initFilter();
		setPermiteAlteracao(isPodeAlterar());
		setPermiteExclusao(isPodeExcluir());
		setPermiteInclusao(isPodeIncluir());
		setPermiteConsulta(isPodeConsultar());
    }

    public void listFromSimpleSearch(){
        if(!Strings.isNullOrEmpty(filter.getBuscaRapida())) {
            this.processos = processoSessionBean.findByConsultaAvancada(filter.getBuscaRapida(), getStateManager().getEmpresaSelecionada().getId());
        } else {
            pesquisarProcesso();
        }
        exibirMensagem();
    }

    private void exibirMensagem() {
        if(this.processos.isEmpty()) {
            exibirMensagem(FacesMessage.SEVERITY_WARN, "Nenhum processo encontrado.");
        } else {
            exibirMensagem(FacesMessage.SEVERITY_WARN, this.processos.size()+" processo(s) encontrado(s).");
        }
    }

    private void pesquisarProcesso() {
        processoSessionBean.clearSession();
        this.processos = processoSessionBean.findByFilters(filter, getStateManager().getEmpresaSelecionada().getId());
        this.filter.setBuscaRapida("Pesquisou com busca rapida vazia");
        filtrarPorPessoa();
    }

    private void filtrarPorPessoa() {
		if (this.filter.getPessoa() == null) {
			return;
		}
    	
		for (Iterator<ViewProcesso> iterator = processos.iterator(); iterator.hasNext();) {
			ViewProcesso viewProcesso = iterator.next();

			boolean existe = Boolean.FALSE;
			Processo processo = processoSessionBean.retrieve(viewProcesso.getId());
			for (Parte parte : processo.getPartes()) {
				if (parte.getPessoa().equals(this.filter.getPessoa())) {
					existe = Boolean.TRUE;
				}
                for (ParteAdvogado parteAdvogado : parte.getAdvogados()) {
                    if (parteAdvogado.getPessoa().equals(this.filter.getPessoa())) {
                        existe = Boolean.TRUE;
                    }
                }
            }
			if (!existe) {
				iterator.remove();
			}
		}
	}

	private void calcularPercentuais() {
        this.percentAtivos = WIDTH + CalculaRegraTresBusiness.calcula(totalAtivos, totalizador) + "%";
        this.percentSuspensos = WIDTH + CalculaRegraTresBusiness.calcula(totalSuspensos, totalizador) + "%";
        this.percentFinalizados = WIDTH + CalculaRegraTresBusiness.calcula(totalFinalizados, totalizador) + "%";
    }

    private void zerarTotalizadores() {
        totalizador = 0;
        totalAtivos = 0;
        totalSuspensos = 0;
        totalFinalizados = 0;
    }

    private void totalizaProcessos() {
        zerarTotalizadores();
        for (ViewProcesso processo : processos) {
            totalizador++;

            if ("ATIVO".equals(processo.getStatus())) {
                totalAtivos++;
            } else if ("SUSPENSO".equals(processo.getStatus())) {
                totalSuspensos++;
            } else {
                totalFinalizados++;
            }
        }
    }

    private void initFilter(){
        this.filter = new ProcessoFilter();
        
        if (!isHasAccessPesquisarTodos()) {
			User usuarioLogado = getUser();
			this.pessoaLogada = pessoaSessionBean.retrieve(usuarioLogado.getId());
			this.filter.setPessoa(pessoaLogada);
        }
    }

	public List<PessoaFisica> getAdvogadosColaboradoresEscritorioList() {
		return pessoaFisicaSessionBean.findByAdvogadosAndColaboradores(getStateManager().getEmpresaSelecionada().getId());
	}
    
    public ProcessoFilter getFilter() {
        return filter;
    }

    public void setFilter(ProcessoFilter filter) {
        this.filter = filter;
    }

	public boolean isHasAccessCadastro(){
		return possuiAcesso(PROCESSOS_PROCESSO_FORM);
	}
	
	public boolean isHasAccess(){
		return possuiAcesso(PROCESSOS_PROCESSO_LIST);
	}
    
    public int getTotalAtivos() {
        return totalAtivos;
    }

    public void setTotalAtivos(int totalAtivos) {
        this.totalAtivos = totalAtivos;
    }

    public int getTotalSuspensos() {
        return totalSuspensos;
    }

    public void setTotalSuspensos(int totalSuspensos) {
        this.totalSuspensos = totalSuspensos;
    }

    public int getTotalFinalizados() {
        return totalFinalizados;
    }

    public void setTotalFinalizados(int totalFinalizados) {
        this.totalFinalizados = totalFinalizados;
    }

    public int getTotalizador() {
        return totalizador;
    }

    public void setTotalizador(int totalizador) {
        this.totalizador = totalizador;
    }

    public String getPercentAtivos() {
        return percentAtivos;
    }

    public void setPercentAtivos(String percentAtivos) {
        this.percentAtivos = percentAtivos;
    }

    public String getPercentSuspensos() {
        return percentSuspensos;
    }

    public void setPercentSuspensos(String percentSuspensos) {
        this.percentSuspensos = percentSuspensos;
    }

    public String getPercentFinalizados() {
        return percentFinalizados;
    }

    public void setPercentFinalizados(String percentFinalizados) {
        this.percentFinalizados = percentFinalizados;
    }

    public List<ViewProcesso> getProcessos() {
        return processos;
    }

    public void setProcessos(List<ViewProcesso> processos) {
        this.processos = processos;
    }
    
	protected boolean isPodeAlterar(){
		Direito direito = direitoSessionBean.retrieve(ALTERAR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), PROCESSOS_PROCESSO_FORM, direito, getUser().getCodEmpresa());
	}

	protected boolean isPodeExcluir(){
		Direito direito = direitoSessionBean.retrieve(EXCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), PROCESSOS_PROCESSO_FORM, direito, getUser().getCodEmpresa());
	}

	protected boolean isPodeIncluir(){
		Direito direito = direitoSessionBean.retrieve(INCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), PROCESSOS_PROCESSO_FORM, direito, getUser().getCodEmpresa());
	}
	
	protected boolean isPodeConsultar(){
		Direito direito = direitoSessionBean.retrieve(CONSULTAR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), PROCESSOS_PROCESSO_FORM, direito, getUser().getCodEmpresa());
	}
	
	public boolean isPodeSomenteVisualizar() {
		return isHasAccess() && !isPodeAlterar() && !isPodeIncluir();
	}

	public Pessoa getPessoaLogada() {
		return pessoaLogada;
	}

	public void setPessoaLogada(Pessoa pessoaLogada) {
		this.pessoaLogada = pessoaLogada;
	}
	
	public boolean isHasAccessPesquisarTodos(){
		return possuiAcesso(PROCESSOS_PESQUISA_TODOS);
	}
	
	public boolean isHasAccessDetalhes(){
		return possuiAcesso("PROCESSOS_PROCESSO_DETAIL");
	}
}
