package br.com.juridico.controller.manutencao;

import br.com.juridico.controller.AbstractController;
import br.com.juridico.entidades.Empresa;
import br.com.juridico.entidades.Login;
import br.com.juridico.entidades.Pessoa;
import br.com.juridico.impl.EmpresaSessionBean;
import br.com.juridico.impl.PessoaSessionBean;
import br.com.juridico.util.DecryptUtil;
import com.google.common.collect.Lists;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;

/**
 * Created by jeffersonl2009_00 on 26/10/2016.
 */
@ManagedBean
@ViewScoped
public class ManutencaoSenhaUsuarioController extends AbstractController {

    private static final Logger LOGGER = Logger.getLogger(ManutencaoSenhaUsuarioController.class);

    @Inject
    private PessoaSessionBean pessoaSessionBean;
    @Inject
    private EmpresaSessionBean empresaSessionBean;

    private List<PessoaSenha> senhaPessoas = Lists.newArrayList();

    private Empresa empresaSelecionada;
    private String buscaRapida;

    @PostConstruct
    public void initialize(){
        carregaPessoas();
    }

    public void carregaPessoas() {
        pessoaSessionBean.clearSession();
        if (empresaSelecionada != null) {
            senhaPessoas = Lists.newArrayList();
            List<Pessoa> pessoas = pessoaSessionBean.retrieveAll(empresaSelecionada.getId());
            carregaPessoas(pessoas);
        }
    }

    public void listFromSimpleSearch(){
        pessoaSessionBean.clearSession();
        if (empresaSelecionada != null) {
            senhaPessoas = Lists.newArrayList();
            List<Pessoa> pessoas = pessoaSessionBean.findBySearchNome(buscaRapida, empresaSelecionada.getId());
            carregaPessoas(pessoas);
        }
    }

    private void carregaPessoas(List<Pessoa> pessoas) {
        try {
            for (Pessoa pessoa : pessoas) {
                if (pessoa.getLogin() != null) {
                    String senha = DecryptUtil.execute(pessoa.getLogin().getSenha());
                    senhaPessoas.add(new PessoaSenha(pessoa.getDescricao(), pessoa.getLogin().getLogin(), senha, pessoa.getLogin().getDataUltimoAcesso(), pessoa.getLogin().isPrimeiroAcesso()));
                }
            }
        } catch (Exception e) {
            LOGGER.error(e, e.getCause());
        }
    }

    public List<Empresa> getEmpresas(){
        return empresaSessionBean.findAllEmpresas();
    }

    public Empresa getEmpresaSelecionada() {
        return empresaSelecionada;
    }

    public void setEmpresaSelecionada(Empresa empresaSelecionada) {
        this.empresaSelecionada = empresaSelecionada;
    }

    public List<PessoaSenha> getSenhaPessoas() {
        return senhaPessoas;
    }

    public String getBuscaRapida() {
        return buscaRapida;
    }

    public void setBuscaRapida(String buscaRapida) {
        this.buscaRapida = buscaRapida;
    }

    public class PessoaSenha {
        private String nome;
        private String login;
        private String senha;
        private Date dataUltimoAcesso;
        private boolean primeiroAcesso;

        public PessoaSenha(String nome, String login, String senha, Date dataUltimoAcesso, boolean primeiroAcesso) {
            this.nome = nome;
            this.login = login;
            this.senha = senha;
            this.primeiroAcesso = primeiroAcesso;
            this.dataUltimoAcesso = dataUltimoAcesso;
        }

        public String getNome() {
            return nome;
        }

        public String getLogin() {
            return login;
        }

        public String getSenha() {
            return senha;
        }

        public Date getDataUltimoAcesso() {
            return dataUltimoAcesso;
        }

        public boolean isPrimeiroAcesso() {
            return primeiroAcesso;
        }
    }
}
