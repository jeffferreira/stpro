package br.com.juridico.controller;

import br.com.juridico.cep.ws.Webservicecep;
import br.com.juridico.datamodel.LazyEntityDataModel;
import br.com.juridico.entidades.*;
import br.com.juridico.enums.CrudStatus;
import br.com.juridico.enums.TipoEndereco;
import br.com.juridico.exception.CrudException;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.filter.ProcessoFilter;
import br.com.juridico.impl.*;
import br.com.juridico.util.EncryptUtil;
import br.com.juridico.util.Util;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import java.util.*;

/**
 *
 * @author Marcos
 *
 */
@ManagedBean
@SessionScoped
public class PessoaJuridicaController extends PessoasController<PessoaJuridica> {

	/**
	 *
	 */
	private static final long serialVersionUID = -8326579994907993316L;

	private static final Logger LOGGER = Logger.getLogger(PessoaJuridicaController.class);
	private static final String PAGES_PESSOAS_PESSOA_JURIDICA_FORM = "/pages/pessoas/pessoaJuridica/pessoa-juridica-form.xhtml";
	private static final String PAGES_PESSOAS_PESSOA_JURIDICA_LIST = "/pages/pessoas/pessoaJuridica/pessoa-juridica-list.xhtml";
	private static final Character PESSOA_JURIDICA = 'J';
	private static final String CHAVE_ACESSO = "PESSOAS_LIST_PESSOAS";
	private static final String PESSOAJURIDICA_PESSOA_JURIDICA_FORM = "PESSOAJURIDICA_PESSOA_JURIDICA_FORM";
	private static final String PESSOAJURIDICA_PESSOA_JURIDICA_LIST = "PESSOAJURIDICA_PESSOA_JURIDICA_LIST";
	private static final String PESSOA_JURIDICA_EMAILS_FORM = "PESSOA_JURIDICA_EMAILS_FORM";
	private static final String PESSOA_JURIDICA_CONTATOS_FORM = "PESSOA_JURIDICA_CONTATOS_FORM";
	private static final String PESSOA_JURIDICA_ENDERECOS_FORM = "PESSOA_JURIDICA_ENDERECOS_FORM";
	private static final String PESSOA_JURIDICA_RESPONSAVEIS_FORM = "PESSOA_JURIDICA_RESPONSAVEIS_FORM";
	private static final String PESSOA_JURIDICA_CENTROS_CUSTO_FORM = "PESSOA_JURIDICA_CENTROS_CUSTO_FORM";
	private static final String PESSOA_JURIDICA_FORM = "pessoaJuridicaForm";
	private static final String CLIENTE = "CLIENTE";

	@Inject
	private PessoaJuridicaSessionBean pessoaJuridicaSessionBean;
	@Inject
	private TipoContatoSessionBean tipoContatoSessionBean;
	@Inject
	private UfSessionBean ufSessionBean;
	@Inject
	private CidadeSessionBean cidadeSessionBean;
	@Inject
	private CentroCustoSessionBean centroCustoSessionBean;
	@Inject
	private PessoaSessionBean pessoaSessionBean;
	@Inject
	private PessoaFisicaSessionBean pessoaFisicaSessionBean;
	@Inject
	private PerfilSessionBean perfilSessionBean;
	@Inject
	private PessoaJuridicaCentroCustoSessionBean pessoaJuridicaCentroCustoSessionBean;
	@Inject
	private RamoAtividadeSessionBean ramoAtividadeSessionBean;
    @Inject
    private ProcessoSessionBean processoSessionBean;

	private PessoaJuridica pessoaJuridicaSelecionada;
	private PessoaFisica pessoaFisicaSelecionada;
	private CentroCusto centroCustoSelecionado;
	private String justificativaClienteNao;

	private List<TipoContato> tiposContatos;
	private List<TipoEndereco> tiposEnderecos;
	private List<CentroCusto> centrosCustosCrud;

	public List<ViewProcesso> processos;


	/**
	 * @PostConstruct
	 */
	@PostConstruct
	public void initialize() {
		verificaCliqueBreadCrumb();
		carregaPermissoes();
	}

	public void init(){
		// TODO Rever e retirar do postConstruct pois pode prejudicar performance
		this.tiposContatos = tipoContatoSessionBean.retrieveAll(getStateManager().getEmpresaSelecionada().getId());
		this.centrosCustosCrud = centroCustoSessionBean.retrieveAll(getStateManager().getEmpresaSelecionada().getId());
		carregaTiposEndereco();
	}

	public void goToDetalhes(){
		RequestContext.getCurrentInstance().execute("openModal('#pessoaJuridicaModal')");
        filtrarPorPessoa();
	}

    public void filtrarPorPessoa() {
        if (pessoaJuridicaSelecionada.getPessoa() == null) {
            return;
        }

        ProcessoFilter filter = getProcessoFilter();
        this.processos = processoSessionBean.findByFilters(filter, getUser().getCodEmpresa());
        atualizaListaSomentePessoaClienteAdverso();
    }

    public ProcessoFilter getProcessoFilter() {
        return new ProcessoFilter(
            pessoaJuridicaSelecionada.getNomePessoa(),
            pessoaJuridicaSelecionada.getNomePessoa(),
            pessoaJuridicaSelecionada.getPessoa());
    }

    public void atualizaListaSomentePessoaClienteAdverso() {
        for (Iterator<ViewProcesso> iterator = this.processos.iterator(); iterator.hasNext(); ) {
            ViewProcesso viewProcesso = iterator.next();

            boolean existe = Boolean.FALSE;
            Processo processo = processoSessionBean.retrieve(viewProcesso.getId());
            for (Parte parte : processo.getPartes()) {
                if (parte.getPessoa().equals(pessoaJuridicaSelecionada.getPessoa())) {
                    existe = Boolean.TRUE;
                }
                for (ParteAdvogado parteAdvogado : parte.getAdvogados()) {
                    if (parteAdvogado.getPessoa().equals(pessoaJuridicaSelecionada.getPessoa())) {
                        existe = Boolean.TRUE;
                    }
                }
            }
            if (!existe) {
                iterator.remove();
            }
        }
    }

	private void carregaPermissoes() {
		setPermiteAlteracao(isPodeAlterar());
		setPermiteExclusao(isPodeExcluir());
		setPermiteInclusao(isPodeIncluir());
		setPermiteVisualizacaoEmail(isPodeVisualizarEmails());
		setPermiteInclusaoEmail(isPodeIncluirEmails());
		setPermiteExclusaoEmail(isPodeExcluirEmails());
		setPermiteVisualizacaoContato(isPodeVisualizarContatos());
		setPermiteInclusaoContato(isPodeIncluirContatos());
		setPermiteExclusaoContato(isPodeExcluirContatos());
		setPermiteVisualizacaoEndereco(isPodeVisualizarEnderecos());
		setPermiteInclusaoEndereco(isPodeIncluirEnderecos());
		setPermiteExclusaoEndereco(isPodeExcluirEnderecos());
		setPermiteVisualizacaoResponsavel(isPodeVisualizarResponsaveis());
		setPermiteInclusaoResponsavel(isPodeIncluirResponsaveis());
		setPermiteExclusaoResponsavel(isPodeExcluirResponsaveis());
		setPermiteVisualizacaoCentroCusto(isPodeVisualizarCentrosCusto());
		setPermiteInclusaoCentroCusto(isPodeIncluirCentrosCusto());
		setPermiteExclusaoCentroCusto(isPodeExcluirCentrosCusto());
	}

	private void verificaCliqueBreadCrumb() {
		Map<String,String> params = getExternalContext().getRequestParameterMap();
		String pesquisaPorBreadCrumb = params.get("paramLink");
		if("true".equals(pesquisaPorBreadCrumb)) {
			super.buscarPorLazy();
			getFlashScope().put(LIST_FLASH, getLazyDataModel());
			carregaPermissoes();
		}
	}

	@Override
	protected void carregaLazyDataModel() {
		setLazyDataModel(new LazyEntityDataModel<PessoaJuridica>() {
			@Override
			protected List<PessoaJuridica> consultaFiltrada(ConsultaLazyFilter filtro) {
				filtro.setDescricao(getDescricaoFiltro());
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				filtro.setPropriedadeOrdenacao("pessoa.descricao");
				return pessoaJuridicaSessionBean.filtrados(filtro);
			}

			@Override
			protected int countTotalRegistros(ConsultaLazyFilter filtro) {
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return pessoaJuridicaSessionBean.quantidadeFiltrados(filtro);
			}
		});
	}

	@Override
	protected void aposSalvar() throws CrudException {
		super.aposSalvar();
		getObjetoSelecionado().setPessoa(new Pessoa());
	}

	private void carregaTiposEndereco() {
		this.setTiposEnderecos(Lists.newArrayList());
		for (TipoEndereco tipoEndereco : TipoEndereco.values()) {
			this.getTiposEnderecos().add(tipoEndereco);
		}
	}

	private void setCentroCustos() {
		List<PessoaJuridicaCentroCusto> centroCustos = pessoaJuridicaCentroCustoSessionBean.findByPessoaJuridica(getObjetoSelecionado().getId(), getStateManager().getEmpresaSelecionada().getId());
		Set<PessoaJuridicaCentroCusto> centroCustoSet = new HashSet<>(centroCustos);
		getObjetoSelecionado().setPessoasJuridicasCentrosCustos(centroCustoSet);
	}

	private void initObjetoCollectors() {
		super.initObjetoPessoaJuridicaCollectors(getObjetoSelecionado());
	}

	@Override
	protected void antesInserir() throws CrudException {
		getObjetoSelecionado().setPessoa(new Pessoa());
		initObjetoCollectors();
	}

	/**
	 *
	 * @return
	 */
	public String novaPessoaJuridica() {
		novo();
		return PAGES_PESSOAS_PESSOA_JURIDICA_FORM;
	}

	/**
	 *
	 * @param pessoaJuridica
	 * @return
	 */
	public String goToEditarPessoa(PessoaJuridica pessoaJuridica) {
		setObjetoSelecionado(pessoaJuridica);
		editar();
		return PAGES_PESSOAS_PESSOA_JURIDICA_FORM;
	}

	@Override
	protected void antesDeEditar(PessoaJuridica objeto) throws CrudException {
		initObjetoCollectors();
	}

	public boolean isHasAccessCadastro(){
		return possuiAcesso(PESSOAJURIDICA_PESSOA_JURIDICA_FORM);
	}

	public boolean isHasAccess(){
		return possuiAcesso(PESSOAJURIDICA_PESSOA_JURIDICA_LIST);
	}

	/**
	 *
	 * @return
	 */
	public String adicionarEndereco() {
		super.adicionarEnderecoPessoaJuridica(getObjetoSelecionado());
		return null;
	}

	/**
	 *
	 * @return
	 */
	public String adicionarEmail() {
		super.adicionarEmailPessoaJuridica(getObjetoSelecionado());
		return null;
	}

	/**
	 *
	 * @return
	 */
	public String adicionarContato() {
		super.adicionarContatoPessoaJuridica(getObjetoSelecionado());
		return null;
	}

	/**
	 *
	 * @return
	 */
	public String adicionarCentroCusto() {
		super.adicionarCentroCustoPessoaJuridica(getObjetoSelecionado());
		return null;
	}

	/**
	 *
	 * @return
	 */
	public String adicionarResponsavel() {
		super.adicionarResponsavelPessoaJuridica(getObjetoSelecionado());
		return null;
	}

	public void limpaNumeroEndereco() {
		if (getPessoaEndereco().getEndereco().getSemNumero()) {
			getPessoaEndereco().getEndereco().setNumero(null);
			return;
		}
		getPessoaEndereco().getEndereco().setSemNumero(false);
	}


	@Override
	protected void validar() throws ValidationException {
		pessoaJuridicaSessionBean.clearSession();
		getObjetoSelecionado().validar();
		getObjetoSelecionado().validarPessoaJaCadastrada(isCnpjJaExiste(), isNomeJaExiste());
	}

	private boolean isCnpjJaExiste(){
		String cnpj = getObjetoSelecionado().getCnpj() != null ? getObjetoSelecionado().getCnpj().replaceAll("[./-]", "") : "";
		List<PessoaJuridica> pessoas = !cnpj.isEmpty() ? pessoaJuridicaSessionBean.findByCnpj(getObjetoSelecionado().getId(), cnpj, getStateManager().getEmpresaSelecionada().getId()) : Lists.newArrayList();
		return !pessoas.isEmpty();
	}

	private boolean isNomeJaExiste(){
		List<PessoaJuridica> pessoas = pessoaJuridicaSessionBean.findByNome(getObjetoSelecionado().getId(), getObjetoSelecionado().getPessoa().getDescricao(), getStateManager().getEmpresaSelecionada().getId());
		return !pessoas.isEmpty();
	}

	/**
	 *
	 */
	public void buscarCep(ValueChangeEvent event) {
		String cep = (String) event.getNewValue();
		super.buscarCep(cep);
	}

	private Cidade carregaCidade(Webservicecep ws) {
		Uf uf = ufSessionBean.findBySigla(ws.getUf());
		return cidadeSessionBean.findByUfDescricao(uf, ws.getCidade());
	}

	@Override
	protected void antesDeSalvar() throws CrudException {
		try {
			adicionarCnpj();
			adicionarInscricaoEstadual();
			adicionarInscricaoMunicipal();
			getObjetoSelecionado().getPessoa().setTipo(PESSOA_JURIDICA);
			getObjetoSelecionado().getPessoa().setDataCadastro(new Date());
			getObjetoSelecionado().getPessoa().setAtivo(true);
			adicionaLoginSenha();

			if(!getObjetoSelecionado().getPessoa().getCliente()) {
				getObjetoSelecionado().getPessoa().setNomeIniciais(null);
			}

		} catch (Exception e) {
			LOGGER.error(e, e.getCause());
		}
	}

	private void adicionaLoginSenha() throws Exception{
		if(getObjetoSelecionado().getPessoa().getId() == null &&
				getObjetoSelecionado().getPessoa().isClienteEmpresa()) {
			String usuario = getObjetoSelecionado().getCnpj().replaceAll("[.-]", "");
			byte[] senha = EncryptUtil.execute(Util.randomGenerateValue(10));
			createUpdateLogin(usuario, senha);
			getObjetoSelecionado().getPessoa().setPerfil(perfilSessionBean.findBySigla(CLIENTE, getStateManager().getEmpresaSelecionada().getId()));
		}
	}

	private void createUpdateLogin(String usuario, byte[] senha){
		Pessoa pessoa = getObjetoSelecionado().getPessoa();
		if(pessoa.getLogin() == null || pessoa.getLogin().getId() == null) {
			Login login = new Login(usuario, senha, getStateManager().getEmpresaSelecionada().getId());
			getObjetoSelecionado().getPessoa().setLogin(login);
		} else {
			getObjetoSelecionado().getPessoa().getLogin().setLogin(usuario);
			getObjetoSelecionado().getPessoa().getLogin().setSenha(senha);
		}
	}

	@Override
	protected void antesDeExcluir() throws CrudException {
		getObjetoSelecionado().setDataExclusao(new Date());
		getObjetoSelecionado().getPessoa().setAtivo(false);
		getObjetoSelecionado().getPessoa().setDataExclusao(new Date());
	}

	@Override
	protected void aposExcluir() throws CrudException {
		super.buscarPorLazy();
	}

	@Override
	protected void aposCancelar() throws CrudException {
		updateContext(PESSOA_JURIDICA_FORM);
		super.aposCancelar();
		super.novo();
	}

	private void adicionarInscricaoMunicipal() {
		String inscricaoMunicipal = getObjetoSelecionado().getInscricaoMunicipal() != null
				? getObjetoSelecionado().getInscricaoMunicipal().replaceAll("[.-]", "") : "";
		getObjetoSelecionado().setInscricaoMunicipal(inscricaoMunicipal);
	}

	private void adicionarInscricaoEstadual() {
		String inscricaoEstadual = getObjetoSelecionado().getInscricaoEstadual() != null
				? getObjetoSelecionado().getInscricaoEstadual().replaceAll("[.-]", "") : "";
		getObjetoSelecionado().setInscricaoEstadual(inscricaoEstadual);
	}

	private void adicionarCnpj() {
		String cnpj = getObjetoSelecionado().getCnpj() != null
				? getObjetoSelecionado().getCnpj().replaceAll("[./-]", "") : "";
		getObjetoSelecionado().setCnpj(cnpj);
	}

	@Override
	protected void create(PessoaJuridica pessoaJuridica) throws ValidationException, CrudException {
		pessoaJuridica.getPessoa().setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
		adicionaHistoricoPessaJuridicaCliente(getObjetoSelecionado(), justificativaClienteNao);
		pessoaJuridicaSessionBean.create(pessoaJuridica);

		if(getObjetoSelecionado().getPessoa().getCliente() && CrudStatus.CREATE.equals(crudStatus) && pessoaJuridica.getPessoa().getPossuiEmail()) {
			enviaEmailNovoUsuarioPessoa(getObjetoSelecionado().getPessoa());
		}
	}

	public void verificaIsCliente() {
		if (!getObjetoSelecionado().getPessoa().getCliente()) {
			getObjetoSelecionado().getPessoa().setEnviaEmail(false);
		} else {
			getObjetoSelecionado().getPessoa().setEnviaEmail(true);
			setJustificativaClienteNao(null);
		}

		if (ultimoHistoricoEhCliente() && !getObjetoSelecionado().getPessoa().getCliente()) {
			RequestContext.getCurrentInstance().execute("PF('justificativaPanel').show('#{component.clientId}')");
			return;
		}
		this.justificativaClienteNao = "";
	}

	public void clienteChanged(ValueChangeEvent event){
		getObjetoSelecionado().getPessoa().setCliente((Boolean) event.getNewValue());
	}

	private boolean ultimoHistoricoEhCliente(){
		if (getObjetoSelecionado().getPessoa() != null && getObjetoSelecionado().getPessoa().getId() != null) {
			pessoaSessionBean.clearSession();
			Pessoa pessoa = pessoaSessionBean.retrieve(getObjetoSelecionado().getPessoa().getId());
			if(pessoa == null) {
				return false;
			}
			int posicaoList = pessoa.getHistoricoClienteList().size() - 1;
			return posicaoList >= 0 && pessoa.getHistoricoClienteList().get(posicaoList).getCliente();
		}
		return false;
	}

	/**
	 *
	 * @return
	 */
	public String goToList() {
		pessoaJuridicaSessionBean.clearSession();
		pessoaSessionBean.clearSession();
		super.buscarPorLazy();
		getFlashScope().put(LIST_FLASH, getLazyDataModel());
		return PAGES_PESSOAS_PESSOA_JURIDICA_LIST;
	}

	public List<RamoAtividade> getRamosAtividades() {
		return ramoAtividadeSessionBean.retrieveAll(getStateManager().getEmpresaSelecionada().getId());
	}

	@Override
	protected PessoaJuridica inicializaObjetosLazy(PessoaJuridica objeto) {
		pessoaJuridicaSessionBean.clearSession();
		return pessoaJuridicaSessionBean.retrieve(objeto.getId());
	}

	/**
	 * void
	 */
	public void listFromSimpleSearch() {
		pessoaJuridicaSessionBean.clearSession();
		super.buscarPorLazy();
	}

	@Override
	protected void setEmpresaDeUsuarioLogado(PessoaJuridica objetoSelecionado) {
		objetoSelecionado.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
	}

	public List<PessoaFisica> completeResponsaveis(String query){
		if (Strings.isNullOrEmpty(query)) {
			return pessoaFisicaSessionBean.findAllPessoasFisicasSemAdvogados(null, getStateManager().getEmpresaSelecionada().getId());
		}
		return pessoaFisicaSessionBean.findAllPessoasFisicasSemAdvogados(query, getStateManager().getEmpresaSelecionada().getId());
	}

	@Override
	protected PessoaJuridica instanciarObjetoNovo() {
		return new PessoaJuridica();
	}

	public List<TipoContato> getTiposContatos() {
		return tiposContatos;
	}

	public void setTiposContatos(List<TipoContato> tiposContatos) {
		this.tiposContatos = tiposContatos;
	}

	public List<CentroCusto> getCentrosCustosCrud() {
		return centrosCustosCrud;
	}

	public void setCentrosCustosCrud(List<CentroCusto> centrosCustosCrud) {
		this.centrosCustosCrud = centrosCustosCrud;
	}

	public PessoaFisica getPessoaFisicaSelecionada() {
		return pessoaFisicaSelecionada;
	}

	public void setPessoaFisicaSelecionada(PessoaFisica pessoaFisicaSelecionada) {
		this.pessoaFisicaSelecionada = pessoaFisicaSelecionada;
	}

	public CentroCusto getCentroCustoSelecionado() {
		return centroCustoSelecionado;
	}

	public void setCentroCustoSelecionado(CentroCusto centroCustoSelecionado) {
		this.centroCustoSelecionado = centroCustoSelecionado;
	}

	public List<TipoEndereco> getTiposEnderecos() {
		return tiposEnderecos;
	}

	public void setTiposEnderecos(List<TipoEndereco> tiposEnderecos) {
		this.tiposEnderecos = tiposEnderecos;
	}


	@Override
	protected void addChaveAcesso() {
		setChaveAcesso(CHAVE_ACESSO);
	}

	@Override
	protected boolean isPodeAlterar(){
		return super.isPodeAlterar(PESSOAJURIDICA_PESSOA_JURIDICA_FORM);
	}

	@Override
	protected boolean isPodeExcluir(){
		return super.isPodeExcluir(PESSOAJURIDICA_PESSOA_JURIDICA_FORM);
	}

	@Override
	protected boolean isPodeIncluir(){
		return super.isPodeIncluir(PESSOAJURIDICA_PESSOA_JURIDICA_FORM);
	}

	public boolean isPodeSomenteVisualizar() {
		return isHasAccess() && !isPodeAlterar() && !isPodeIncluir();
	}

	public boolean isPodeVisualizarEmails() {
		return super.isPodeVisualizarEmails(PESSOA_JURIDICA_EMAILS_FORM);
	}

	public boolean isPodeIncluirEmails() {
		return super.isPodeIncluirEmails(PESSOA_JURIDICA_EMAILS_FORM);
	}

	public boolean isPodeExcluirEmails() {
		return super.isPodeExcluirEmails(PESSOA_JURIDICA_EMAILS_FORM);
	}

	public boolean isPodeVisualizarContatos() {
		return super.isPodeVisualizarContatos(PESSOA_JURIDICA_CONTATOS_FORM);
	}

	public boolean isPodeIncluirContatos() {
		return super.isPodeIncluirContatos(PESSOA_JURIDICA_CONTATOS_FORM);
	}

	public boolean isPodeExcluirContatos() {
		return super.isPodeExcluirContatos(PESSOA_JURIDICA_CONTATOS_FORM);
	}

	public boolean isPodeVisualizarEnderecos() {
		return super.isPodeVisualizarEnderecos(PESSOA_JURIDICA_ENDERECOS_FORM);
	}

	public boolean isPodeIncluirEnderecos() {
		return super.isPodeIncluirEnderecos(PESSOA_JURIDICA_ENDERECOS_FORM);
	}

	public boolean isPodeExcluirEnderecos() {
		return super.isPodeExcluirEnderecos(PESSOA_JURIDICA_ENDERECOS_FORM);
	}

	public boolean isPodeVisualizarResponsaveis() {
		return super.isPodeVisualizarResponsaveis(PESSOA_JURIDICA_RESPONSAVEIS_FORM);
	}

	public boolean isPodeIncluirResponsaveis() {
		return super.isPodeIncluirResponsaveis(PESSOA_JURIDICA_RESPONSAVEIS_FORM);
	}

	public boolean isPodeExcluirResponsaveis() {
		return super.isPodeExcluirResponsaveis(PESSOA_JURIDICA_RESPONSAVEIS_FORM);
	}

	public boolean isPodeVisualizarCentrosCusto() {
		return super.isPodeVisualizarCentrosCusto(PESSOA_JURIDICA_CENTROS_CUSTO_FORM);
	}

	public boolean isPodeIncluirCentrosCusto() {
		return super.isPodeIncluirCentrosCusto(PESSOA_JURIDICA_CENTROS_CUSTO_FORM);
	}

	public boolean isPodeExcluirCentrosCusto() {
		return super.isPodeExcluirCentrosCusto(PESSOA_JURIDICA_CENTROS_CUSTO_FORM);
	}

	public String getJustificativaClienteNao() {
		return justificativaClienteNao;
	}

	public void setJustificativaClienteNao(String justificativaClienteNao) {
		this.justificativaClienteNao = justificativaClienteNao;
	}

	public PessoaJuridica getPessoaJuridicaSelecionada() {
		return pessoaJuridicaSelecionada;
	}

	public void setPessoaJuridicaSelecionada(PessoaJuridica pessoaJuridicaSelecionada) {
		this.pessoaJuridicaSelecionada = pessoaJuridicaSelecionada;
	}

    public List<ViewProcesso> getProcessos() {
        return processos;
    }

    public void setProcessos(List<ViewProcesso> processos) {
        this.processos = processos;
    }
}
