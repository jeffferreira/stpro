package br.com.juridico.controller;

import br.com.juridico.datamodel.LazyEntityDataModel;
import br.com.juridico.entidades.*;
import br.com.juridico.enums.CrudStatus;
import br.com.juridico.enums.TipoEndereco;
import br.com.juridico.exception.CrudException;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.filter.ProcessoFilter;
import br.com.juridico.impl.*;
import br.com.juridico.util.EncryptUtil;
import br.com.juridico.util.Util;
import com.google.common.collect.Lists;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author Jefferson
 */
@ManagedBean
@SessionScoped
public class PessoaFisicaController extends PessoasController<PessoaFisica> {

    /**
     *
     */
    private static final long serialVersionUID = -3108295287150832212L;

    private static final Logger LOGGER = Logger.getLogger(PessoaFisicaController.class);
    private static final String PAGES_PESSOAS_PESSOA_FISICA_LIST = "/pages/pessoas/pessoaFisica/pessoa-fisica-list.xhtml";
    private static final String PAGES_PESSOAS_PESSOA_FISICA_FORM = "/pages/pessoas/pessoaFisica/pessoa-fisica-form.xhtml";
    private static final Character PESSOA_FISICA = 'F';
    private static final String CHAVE_ACESSO = "PESSOAS_LIST_PESSOAS";
    private static final String PESSOAFISICA_PESSOA_FISICA_FORM = "PESSOAFISICA_PESSOA_FISICA_FORM";
    private static final String PESSOAFISICA_PESSOA_FISICA_LIST = "PESSOAFISICA_PESSOA_FISICA_LIST";
    private static final String PESSOA_FISICA_EMAILS_FORM = "PESSOA_FISICA_EMAILS_FORM";
    private static final String PESSOA_FISICA_CONTATOS_FORM = "PESSOA_FISICA_CONTATOS_FORM";
    private static final String PESSOA_FISICA_ENDERECOS_FORM = "PESSOA_FISICA_ENDERECOS_FORM";
    private static final String PESSOA_FISICA_FORM = "pessoaFisicaForm";
    private static final String CLIENTE = "CLIENTE";

    @Inject
    private PessoaFisicaSessionBean pessoaFisicaSessionBean;
    @Inject
    private EstadoCivilSessionBean estadoCivilSessionBean;
    @Inject
    private TipoContatoSessionBean tipoContatoSessionBean;
    @Inject
    private PessoaSessionBean pessoaSessionBean;
    @Inject
    private PerfilSessionBean perfilSessionBean;
    @Inject
    private ProcessoSessionBean processoSessionBean;

    private PessoaFisica pessoaFisicaSelecionada;

    private List<EstadoCivil> estadosCivis;
    private List<TipoContato> tiposContatos;
    private List<TipoEndereco> tiposEnderecos;

    public List<ViewProcesso> processos;

    private String validacaoEmail;
    private String justificativaClienteNao;

    @PostConstruct
    public void initialize(){
        verificaCliqueBreadCrumb();
        carregaPermissoes();
    }

    public void init() {
        this.estadosCivis = estadoCivilSessionBean.retrieveAll();
        this.tiposContatos = tipoContatoSessionBean.retrieveAll(getStateManager().getEmpresaSelecionada().getId());
        carregaTiposEndereco();
    }

    private void carregaPermissoes() {
        setPermiteAlteracao(isPodeAlterar());
        setPermiteExclusao(isPodeExcluir());
        setPermiteInclusao(isPodeIncluir());
        setPermiteVisualizacaoEmail(isPodeVisualizarEmails());
        setPermiteInclusaoEmail(isPodeIncluirEmails());
        setPermiteExclusaoEmail(isPodeExcluirEmails());
        setPermiteVisualizacaoContato(isPodeVisualizarContatos());
        setPermiteInclusaoContato(isPodeIncluirContatos());
        setPermiteExclusaoContato(isPodeExcluirContatos());
        setPermiteVisualizacaoEndereco(isPodeVisualizarEnderecos());
        setPermiteInclusaoEndereco(isPodeIncluirEnderecos());
        setPermiteExclusaoEndereco(isPodeExcluirEnderecos());
    }

    private void verificaCliqueBreadCrumb() {
        Map<String, String> params = getExternalContext().getRequestParameterMap();
        String pesquisaPorBreadCrumb = params.get("paramLink");
        if ("true".equals(pesquisaPorBreadCrumb)) {
            super.buscarPorLazy();
            getFlashScope().put(LIST_FLASH, getLazyDataModel());
            carregaPermissoes();
        }
    }

    public boolean isHasAccessCadastro() {
        return possuiAcesso(PESSOAFISICA_PESSOA_FISICA_FORM);
    }

    public boolean isHasAccess() {
        return possuiAcesso(PESSOAFISICA_PESSOA_FISICA_LIST);
    }

    private void initObjetoCollectors() {
        super.initObjetoPessoaCollectors(getObjetoSelecionado().getPessoa());
    }

    @Override
    protected void antesInserir() throws CrudException {
        initObjetoCollectors();
    }

    @Override
    protected void antesDeEditar(PessoaFisica objeto) throws CrudException {
        initObjetoCollectors();
    }

    private void carregaTiposEndereco() {
        this.tiposEnderecos = Lists.newArrayList();
        for (TipoEndereco tipoEndereco : TipoEndereco.values()) {
            this.tiposEnderecos.add(tipoEndereco);
        }
    }

    /**
     * @return
     */
    public String adicionarEmail() {
        super.adicionarEmailPessoaFisica(getObjetoSelecionado());
        return null;
    }

    /**
     * @return
     */
    public String adicionarEndereco() {
        super.adicionarEnderecoPessoaFisica(getObjetoSelecionado());
        return null;
    }

    /**
     * @return
     */
    public String adicionarContato() {
        super.adicionarContatoPessoaFisica(getObjetoSelecionado());
        return null;
    }

    public void limpaNumeroEndereco() {
        if (getPessoaEndereco().getEndereco().getSemNumero()) {
            getPessoaEndereco().getEndereco().setNumero(null);
            return;
        }
        getPessoaEndereco().getEndereco().setSemNumero(false);
    }

    /**
     *
     */
    public void buscarCep(ValueChangeEvent event) {
        String cep = (String) event.getNewValue();
        super.buscarCep(cep);
    }

    @Override
    protected void validar() throws ValidationException {
        getObjetoSelecionado().validar();
        getObjetoSelecionado().validarPessoaJaCadastrada(isCpfJaExiste(), isNomeJaExiste());

    }

    private boolean isCpfJaExiste() {
        String cpf = getObjetoSelecionado().getCpf() != null ? getObjetoSelecionado().getCpf().replaceAll("[.-]", "") : "";
        List<PessoaFisica> pessoas = !cpf.isEmpty() ? pessoaFisicaSessionBean.findByCpf(getObjetoSelecionado().getId(), cpf, getStateManager().getEmpresaSelecionada().getId()) : Lists.newArrayList();
        return !pessoas.isEmpty();
    }

    private boolean isNomeJaExiste() {
        return Boolean.FALSE;
    }

    @Override
    protected void antesDeSalvar() throws CrudException {
        try {
            adicionarCpf();
            adicionaRg();
            getObjetoSelecionado().setReceberPublicacoes(false);
            getObjetoSelecionado().setAdvogado(false);
            getObjetoSelecionado().getPessoa().setTipo(PESSOA_FISICA);
            getObjetoSelecionado().getPessoa().setDataCadastro(new Date());
            getObjetoSelecionado().getPessoa().setAtivo(true);
            adicionaLoginSenha();

            if (!getObjetoSelecionado().getPessoa().getCliente()) {
                getObjetoSelecionado().getPessoa().setNomeIniciais(null);
            }
            super.antesDeSalvar();
        } catch (Exception e) {
            LOGGER.error(e, e.getCause());
        }
    }

    private void adicionaLoginSenha() throws Exception {
        if (getObjetoSelecionado().getPessoa().getId() == null && getObjetoSelecionado().getPessoa().isClienteEmpresa()) {
            List<Pessoa> usuariosCadastrados = pessoaSessionBean.findUserList(getObjetoSelecionado().getCpf());
            String usuario = getObjetoSelecionado().getCpf().replaceAll("[.-]", "");

            if (usuariosCadastrados.isEmpty()) {
                byte[] senha = EncryptUtil.execute(Util.randomGenerateValue(10));
                createUpdateLogin(getObjetoSelecionado(), usuario, senha);
            } else {
                createUpdateLogin(getObjetoSelecionado(), usuario, usuariosCadastrados.get(0).getLogin().getSenha());
            }
            getObjetoSelecionado().getPessoa().setPerfil(perfilSessionBean.findBySigla(CLIENTE, getStateManager().getEmpresaSelecionada().getId()));
        }
    }

    public void goToDetalhes() {
        RequestContext.getCurrentInstance().execute("openModal('#pessoaFisicaModal')");
        filtrarPorPessoa();
    }

    public void filtrarPorPessoa() {
        if (pessoaFisicaSelecionada.getPessoa() == null) {
            return;
        }

        ProcessoFilter filter = getProcessoFilter();
        this.processos = processoSessionBean.findByFilters(filter, getUser().getCodEmpresa());
        atualizaListaSomentePessoaClienteAdverso();
    }

    public void atualizaListaSomentePessoaClienteAdverso() {
        for (Iterator<ViewProcesso> iterator = this.processos.iterator(); iterator.hasNext(); ) {
            ViewProcesso viewProcesso = iterator.next();

            boolean existe = Boolean.FALSE;
            Processo processo = processoSessionBean.retrieve(viewProcesso.getId());
            for (Parte parte : processo.getPartes()) {
                if (parte.getPessoa().equals(pessoaFisicaSelecionada.getPessoa())) {
                    existe = Boolean.TRUE;
                }
                for (ParteAdvogado parteAdvogado : parte.getAdvogados()) {
                    if (parteAdvogado.getPessoa().equals(pessoaFisicaSelecionada.getPessoa())) {
                        existe = Boolean.TRUE;
                    }
                }
            }
            if (!existe) {
                iterator.remove();
            }
        }
    }

    public ProcessoFilter getProcessoFilter() {
        return new ProcessoFilter(
                pessoaFisicaSelecionada.getNomePessoa(),
                pessoaFisicaSelecionada.getNomePessoa(),
                pessoaFisicaSelecionada.getPessoa());
    }

    @Override
    protected void antesDeExcluir() throws CrudException {
        getObjetoSelecionado().setDataExclusao(new Date());
        getObjetoSelecionado().getPessoa().setAtivo(false);
        getObjetoSelecionado().getPessoa().setDataExclusao(new Date());
    }

    @Override
    protected void aposExcluir() throws CrudException {
        super.buscarPorLazy();
    }

    @Override
    protected void aposCancelar() throws CrudException {
        updateContext(PESSOA_FISICA_FORM);
        super.aposCancelar();
        super.novo();
    }

    private void adicionaRg() {
        String rg = getObjetoSelecionado().getRg() != null ? getObjetoSelecionado().getRg().replaceAll("[.-]", "") : "";
        getObjetoSelecionado().setRg(rg);
    }

    private void adicionarCpf() {
        String cpf = getObjetoSelecionado().getCpf() != null ? getObjetoSelecionado().getCpf().replaceAll("[.-]", "") : "";
        getObjetoSelecionado().setCpf(cpf);
    }

    @Override
    protected void create(PessoaFisica pessoaFisica) throws ValidationException, CrudException {
        pessoaFisica.getPessoa().setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
        adicionaHistoricoPessaFisicaCliente(getObjetoSelecionado(), justificativaClienteNao);
        pessoaFisicaSessionBean.clearSession();
        pessoaFisicaSessionBean.create(pessoaFisica);

        if (pessoaFisica.getPessoa().getCliente() && CrudStatus.CREATE.equals(crudStatus) && pessoaFisica.getPessoa().getPossuiEmail()) {
            enviaEmailNovoUsuarioPessoa(getObjetoSelecionado().getPessoa());
        }
    }

    @Override
    protected void carregaLazyDataModel() {
        setLazyDataModel(new LazyEntityDataModel<PessoaFisica>() {
            @Override
            protected List<PessoaFisica> consultaFiltrada(ConsultaLazyFilter filtro) {
                filtro.setDescricao(getDescricaoFiltro());
                filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
                filtro.setPropriedadeOrdenacao("pessoa.descricao");
                return pessoaFisicaSessionBean.filtrados(filtro, false, false);
            }

            @Override
            protected int countTotalRegistros(ConsultaLazyFilter filtro) {
                filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
                return pessoaFisicaSessionBean.quantidadeFiltrados(filtro, false, false);
            }
        });
    }

    @Override
    protected void aposSalvar() throws CrudException {
        instanciarObjetoNovo();
        super.aposSalvar();
    }

    /**
     * @return
     */
    public String goToList() {
        pessoaFisicaSessionBean.clearSession();
        pessoaSessionBean.clearSession();
        super.buscarPorLazy();
        return PAGES_PESSOAS_PESSOA_FISICA_LIST;
    }

    /**
     * @return
     */
    public String novaPessoaFisica() {
        novo();
        return PAGES_PESSOAS_PESSOA_FISICA_FORM;
    }

    /**
     * @param pessoaFisica
     * @return
     */
    public String goToEditarPessoa(PessoaFisica pessoaFisica) {
        setObjetoSelecionado(pessoaFisica);
        editar();
        return PAGES_PESSOAS_PESSOA_FISICA_FORM;
    }

    /**
     * void
     */
    public void listFromSimpleSearch() {
        pessoaFisicaSessionBean.clearSession();
        super.buscarPorLazy();
    }

    @Override
    protected PessoaFisica inicializaObjetosLazy(PessoaFisica objeto) {
        pessoaFisicaSessionBean.clearSession();
        return pessoaFisicaSessionBean.retrieve(objeto.getId());
    }

    @Override
    protected void setEmpresaDeUsuarioLogado(PessoaFisica objetoSelecionado) {
        objetoSelecionado.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
        objetoSelecionado.getPessoa().setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
    }

    @Override
    protected void addChaveAcesso() {
        setChaveAcesso(CHAVE_ACESSO);
    }

    public void verificaIsCliente() {
        if (!getObjetoSelecionado().getPessoa().getCliente()) {
            getObjetoSelecionado().getPessoa().setEnviaEmail(false);
        } else {
            getObjetoSelecionado().getPessoa().setEnviaEmail(true);
            setJustificativaClienteNao(null);
        }

        if (ultimoHistoricoEhCliente() && !getObjetoSelecionado().getPessoa().getCliente()) {
            RequestContext.getCurrentInstance().execute("PF('justificativaPanel').show('#{component.clientId}')");
            return;
        }
        this.justificativaClienteNao = "";
    }

    public void clienteChanged(ValueChangeEvent event) {
        getObjetoSelecionado().getPessoa().setCliente((Boolean) event.getNewValue());
    }

    public boolean ultimoHistoricoEhCliente() {
        if (getObjetoSelecionado().getPessoa() != null && getObjetoSelecionado().getPessoa().getId() != null) {
            pessoaSessionBean.clearSession();
            Pessoa pessoa = pessoaSessionBean.retrieve(getObjetoSelecionado().getPessoa().getId());
            if (pessoa == null) {
                return false;
            }
            int posicaoList = pessoa.getHistoricoClienteList().size() - 1;
            return posicaoList >= 0 && pessoa.getHistoricoClienteList().get(posicaoList).getCliente();
        }
        return false;
    }

    public List<PessoaEmail> getEmails() {
        return getObjetoSelecionado().getPessoa().getPessoasEmails();
    }

    @Override
    protected PessoaFisica instanciarObjetoNovo() {
        return new PessoaFisica();
    }

    public List<EstadoCivil> getEstadosCivis() {
        return estadosCivis;
    }

    public void setEstadosCivis(List<EstadoCivil> estadosCivis) {
        this.estadosCivis = estadosCivis;
    }

    public List<TipoContato> getTiposContatos() {
        return tiposContatos;
    }

    public void setTiposContatos(List<TipoContato> tiposContatos) {
        this.tiposContatos = tiposContatos;
    }

    public List<TipoEndereco> getTiposEnderecos() {
        return tiposEnderecos;
    }

    public void setTiposEnderecos(List<TipoEndereco> tiposEnderecos) {
        this.tiposEnderecos = tiposEnderecos;
    }

    public String getValidacaoEmail() {
        return validacaoEmail;
    }

    public void setValidacaoEmail(String validacaoEmail) {
        this.validacaoEmail = validacaoEmail;
    }

    @Override
    protected boolean isPodeAlterar() {
        return super.isPodeAlterar(PESSOAFISICA_PESSOA_FISICA_FORM);
    }

    @Override
    protected boolean isPodeExcluir() {
        return super.isPodeExcluir(PESSOAFISICA_PESSOA_FISICA_FORM);
    }

    @Override
    protected boolean isPodeIncluir() {
        return super.isPodeIncluir(PESSOAFISICA_PESSOA_FISICA_FORM);
    }

    public boolean isPodeSomenteVisualizar() {
        return isHasAccess() && !isPodeAlterar() && !isPodeIncluir();
    }

    public boolean isPodeVisualizarEmails() {
        return super.isPodeVisualizarEmails(PESSOA_FISICA_EMAILS_FORM);
    }

    public boolean isPodeIncluirEmails() {
        return super.isPodeIncluirEmails(PESSOA_FISICA_EMAILS_FORM);
    }

    public boolean isPodeExcluirEmails() {
        return super.isPodeExcluirEmails(PESSOA_FISICA_EMAILS_FORM);
    }

    public boolean isPodeVisualizarContatos() {
        return super.isPodeVisualizarContatos(PESSOA_FISICA_CONTATOS_FORM);
    }

    public boolean isPodeIncluirContatos() {
        return super.isPodeIncluirContatos(PESSOA_FISICA_CONTATOS_FORM);
    }

    public boolean isPodeExcluirContatos() {
        return super.isPodeExcluirContatos(PESSOA_FISICA_CONTATOS_FORM);
    }

    public boolean isPodeVisualizarEnderecos() {
        return super.isPodeVisualizarEnderecos(PESSOA_FISICA_ENDERECOS_FORM);
    }

    public boolean isPodeIncluirEnderecos() {
        return super.isPodeIncluirEnderecos(PESSOA_FISICA_ENDERECOS_FORM);
    }

    public boolean isPodeExcluirEnderecos() {
        return super.isPodeExcluirEnderecos(PESSOA_FISICA_ENDERECOS_FORM);
    }

    public String getJustificativaClienteNao() {
        return justificativaClienteNao;
    }

    public PessoaFisica getPessoaFisicaSelecionada() {
        return pessoaFisicaSelecionada;
    }

    public void setPessoaFisicaSelecionada(PessoaFisica pessoaFisicaSelecionada) {
        this.pessoaFisicaSelecionada = pessoaFisicaSelecionada;
    }

    public void setJustificativaClienteNao(String justificativaClienteNao) {
        this.justificativaClienteNao = justificativaClienteNao;
    }

    public List<ViewProcesso> getProcessos() {
        return processos;
    }

    public void setProcessos(List<ViewProcesso> processos) {
        this.processos = processos;
    }
}
