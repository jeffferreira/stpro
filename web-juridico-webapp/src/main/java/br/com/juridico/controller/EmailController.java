package br.com.juridico.controller;

import br.com.juridico.business.ParametroEmailBusiness;
import br.com.juridico.datamodel.LazyEntityDataModel;
import br.com.juridico.entidades.Direito;
import br.com.juridico.entidades.Email;
import br.com.juridico.entidades.EmailVinculoParametro;
import br.com.juridico.entidades.ParametroEmail;
import br.com.juridico.exception.CrudException;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.impl.DireitoSessionBean;
import br.com.juridico.impl.EmailSessionBean;
import br.com.juridico.impl.ParametroEmailSessionBean;
import br.com.juridico.impl.PerfilAcessoSessionBean;
import br.com.juridico.types.EmailStepsTypes;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.primefaces.model.LazyDataModel;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * Created by jeffersonl2009_00 on 27/06/2016.
 */
@Named
@ViewScoped
public class EmailController extends CrudController<Email> {

    private static final String EMAIL_FORM = "emailForm";
    private static final String CHAVE_ACESSO = "EMAIL_EMAIL_TEMPLATE";
    private static final String PAGINA_FORMULARIO = "/pages/cadastros/email/email-template.xhtml?faces-redirect=true";
    private static final String AGENDA = "AGENDA";
    private static final String SENHA = "SENHA";
    private static final String PROCESSO = "PROC";

    private static final String[] STEP_TYPES = {"STEP_CONFIGURACAO", "STEP_EMAIL"};

    @Inject
    private EmailSessionBean emailSessionBean;
    @Inject
    private PerfilAcessoSessionBean perfilAcessoSessionBean;
    @Inject
    private DireitoSessionBean direitoSessionBean;
    @Inject
    private ParametroEmailSessionBean parametroEmailSessionBean;

    private  List<ParametroEmail> parametroEmailsPessoa = Lists.newArrayList();
    private  List<ParametroEmail> parametroEmailsProcesso = Lists.newArrayList();
    private  List<ParametroEmail> parametroEmailsAgenda = Lists.newArrayList();

    private String tipoEmailSelecionado;
    private String editorText;
    private EmailStepsTypes stepsTypesSelecionado;
    private String stepClick;

    @PostConstruct
    private void initialize(){
        this.initWizardConfiguracao();
        getObjetoSelecionado().setDescricao(null);
        LazyDataModel<Email> listFlash = (LazyDataModel<Email>) getFlashScope().get(LIST_FLASH);

        if(listFlash != null) {
            setLazyDataModel(listFlash);
            return;
        }
        super.buscarPorLazy();
    }

    protected void initWizardConfiguracao() {
        this.stepsTypesSelecionado = EmailStepsTypes.STEP_CONFIGURACAO;
        this.stepClick = stepsTypesSelecionado.getValue();
        this.getObjetoSelecionado().setDescricao(null);
    }

    @Override
    protected void aposCancelar() throws CrudException {
        getObjetoSelecionado().setDescricao(null);
        updateContext(EMAIL_FORM);
        super.aposCancelar();
    }

    @Override
    protected void carregaLazyDataModel() {
        setLazyDataModel(new LazyEntityDataModel<Email>() {
            @Override
            protected List<Email> consultaFiltrada(ConsultaLazyFilter filtro) {
                filtro.setDescricao(getDescricaoFiltro());
                filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
                filtro.setPossuiExclusaoLogica(false);
                return emailSessionBean.filtrados(filtro);
            }

            @Override
            protected int countTotalRegistros(ConsultaLazyFilter filtro) {
                filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
                filtro.setPossuiExclusaoLogica(false);
                return emailSessionBean.quantidadeFiltrados(filtro);
            }
        });
    }

    public void returnToConfiguracao() {
        initWizardConfiguracao();
        emailSessionBean.clearSession();
        updateContext(EMAIL_FORM);
    }

    public List<Email> getEmails() {
        return emailSessionBean.retrieveAll(getStateManager().getEmpresaSelecionada().getId());
    }

    @Override
    protected void create(Email email) throws ValidationException, CrudException {
        atualizarParametroNaTabelaVinculo(email);
        emailSessionBean.update(email);
    }

    private void atualizarParametroNaTabelaVinculo(Email email) {
        if(Strings.isNullOrEmpty(email.getDescricao())) {
            return;
        }

        getObjetoSelecionado().getParametroEmails().clear();
        String[] parametros = ParametroEmailBusiness.retornaParametrosDoTexto(email.getDescricao());
        if(parametros != null) {
            for (String parametro : parametros) {
                ParametroEmail parametroEmail = parametroEmailSessionBean.findBySigla(parametro);
                EmailVinculoParametro vinculoParametro = new EmailVinculoParametro(email, parametroEmail);
                getObjetoSelecionado().getParametroEmails().add(vinculoParametro);
            }
        }
    }

    @Override
    public void salvar() {
        super.salvar();
        returnToConfiguracao();
    }

    @Override
    protected void antesDeSalvar() throws CrudException {
        if(!Strings.isNullOrEmpty(editorText)) {
            getObjetoSelecionado().setDescricao(editorText);
        }
    }

    @Override
    protected Email inicializaObjetosLazy(Email objeto) {
        emailSessionBean.clearSession();
        return emailSessionBean.retrieve(objeto.getId());
    }

    @Override
    protected Email instanciarObjetoNovo() {
        return new Email();
    }

    @Override
    protected void setEmpresaDeUsuarioLogado(Email objetoSelecionado) {
        objetoSelecionado.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
    }

    private void initParametros() {
        this.parametroEmailsAgenda = parametroEmailSessionBean.findByGeralAndAgenda();
        this.parametroEmailsPessoa = parametroEmailSessionBean.findByGeralAndSenha();
        this.parametroEmailsProcesso = parametroEmailSessionBean.findByProcesso();
//        if(AGENDA.equals(tipoEmailSelecionado)) {
//            this.parametroEmailsAgenda = parametroEmailSessionBean.findByGeralAndAgenda();
//        } else if(SENHA.equals(tipoEmailSelecionado)) {
//            this.parametroEmailsPessoa = parametroEmailSessionBean.findByGeralAndSenha();
//        } else if(PROCESSO.equals(tipoEmailSelecionado)) {
//            this.parametroEmailsProcesso = parametroEmailSessionBean.findByProcesso();
//        }
    }

    public void stepNext() {
        switch (stepClick){
            case "STEP_CONFIGURACAO" :
                this.stepsTypesSelecionado = EmailStepsTypes.fromStep(STEP_TYPES[1]);
                this.stepClick = STEP_TYPES[1];
                break;
            default:
                this.stepsTypesSelecionado = EmailStepsTypes.fromStep(STEP_TYPES[0]);
                this.stepClick = STEP_TYPES[0];
        }
    }

    public void stepSelect(String step){
        if(Strings.isNullOrEmpty(step)){
            return;
        }
        this.stepClick = step;
    }

    /**
     *
     * @return
     */
    public String goToFormulario() {
        super.buscarPorLazy();
        getFlashScope().put(LIST_FLASH, getLazyDataModel());
        return PAGINA_FORMULARIO;
    }

    public void atualizaParametros(Email objeto) throws CrudException {
        if(objeto.getSigla().startsWith(AGENDA)) {
            tipoEmailSelecionado = AGENDA;
        } else if(objeto.getSigla().startsWith(SENHA)){
            tipoEmailSelecionado = SENHA;
        } else if(objeto.getSigla().startsWith(PROCESSO)){
            tipoEmailSelecionado = PROCESSO;
        }
        initParametros();
    }

    public List<ParametroEmail> getParametroEmailsPessoa() {
        return parametroEmailsPessoa;
    }

    public void setParametroEmailsPessoa(List<ParametroEmail> parametroEmailsPessoa) {
        this.parametroEmailsPessoa = parametroEmailsPessoa;
    }

    public List<ParametroEmail> getParametroEmailsProcesso() {
        return parametroEmailsProcesso;
    }

    public void setParametroEmailsProcesso(List<ParametroEmail> parametroEmailsProcesso) {
        this.parametroEmailsProcesso = parametroEmailsProcesso;
    }

    public List<ParametroEmail> getParametroEmailsAgenda() {
        return parametroEmailsAgenda;
    }

    public void setParametroEmailsAgenda(List<ParametroEmail> parametroEmailsAgenda) {
        this.parametroEmailsAgenda = parametroEmailsAgenda;
    }

    @Override
    protected void addChaveAcesso() {
        setChaveAcesso(CHAVE_ACESSO);
    }

    @Override
    protected boolean isPodeAlterar(){
        Direito direito = direitoSessionBean.retrieve(ALTERAR);
        return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
    }

    @Override
    protected boolean isPodeExcluir(){
        Direito direito = direitoSessionBean.retrieve(EXCLUIR);
        return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
    }

    @Override
    protected boolean isPodeIncluir(){
        Direito direito = direitoSessionBean.retrieve(INCLUIR);
        return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
    }

    public String getEditorText() {
        return editorText;
    }

    public void setEditorText(String editorText) {
        this.editorText = editorText;
    }

    public EmailStepsTypes getStepsTypesSelecionado() {
        return stepsTypesSelecionado;
    }

    public String getStepClick() {
        return stepClick;
    }

}
