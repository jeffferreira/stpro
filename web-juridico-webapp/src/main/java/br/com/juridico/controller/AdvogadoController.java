package br.com.juridico.controller;

import br.com.juridico.datamodel.LazyEntityDataModel;
import br.com.juridico.entidades.*;
import br.com.juridico.enums.CrudStatus;
import br.com.juridico.enums.TipoEndereco;
import br.com.juridico.exception.CrudException;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.impl.*;
import br.com.juridico.util.EncryptUtil;
import br.com.juridico.util.Util;
import com.google.common.collect.Lists;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Marcos Melo
 *
 */
@ManagedBean
@ViewScoped
public class AdvogadoController extends PessoasController<PessoaFisica> {

	/**
	 *
	 */
	private static final long serialVersionUID = -3108295287150832212L;

	private static final Logger LOGGER = Logger.getLogger(AdvogadoController.class);
	private static final Character PESSOA_FISICA = 'F';
	private static final String EDIT_FLASH = "EDIT_FLASH";
	private static final String IS_NOVO_FLASH = "IS_NOVO_FLASH";
	private static final String PAGES_ADVOGADOS_ADVOGADO_FORM = "/pages/advogados/advogado-form.xhtml";
	private static final String PAGES_ADVOGADOS_ADVOGADO_LIST = "/pages/advogados/advogado-list.xhtml";
	private static final String ADVOGADO_FORM = "advogadoForm";
	private static final String ADVOGADOS_ADVOGADO_FORM = "ADVOGADOS_ADVOGADO_FORM";
	private static final String ADVOGADOS_ADVOGADO_LIST = "ADVOGADOS_ADVOGADO_LIST";
	private static final String ADVOGADO_EMAILS_FORM = "ADVOGADO_EMAILS_FORM";
	private static final String ADVOGADO_CONTATOS_FORM = "ADVOGADO_CONTATOS_FORM";
	private static final String ADVOGADO_ENDERECOS_FORM = "ADVOGADO_ENDERECOS_FORM";
	private static final String CHAVE_ACESSO = "";
	private static final String ADMIN = "ADMIN";

	@Inject
	private PessoaFisicaSessionBean pessoaFisicaSessionBean;
	@Inject
	private AdvogadoSessionBean advogadoSessionBean;
	@Inject
	private EstadoCivilSessionBean estadoCivilSessionBean;
	@Inject
	private TipoContatoSessionBean tipoContatoSessionBean;
	@Inject
	private UfSessionBean ufSessionBean;
	@Inject
	private CidadeSessionBean cidadeSessionBean;
	@Inject
	private PerfilSessionBean perfilSessionBean;
	@Inject
	private PessoaSessionBean pessoaSessionBean;
	@Inject
	private EmailCaixaSaidaSessionBean emailCaixaSaidaSessionBean;
	@Inject
	private EmailSessionBean emailSessionBean;

	private List<EstadoCivil> estadosCivis;
	private List<TipoContato> tiposContatos;
	private List<TipoEndereco> tiposEnderecos;
	private List<Uf> ufs;

	private String filter;
	private PessoaFisica advogadoSelecionado;


	@PostConstruct
	public void initialize() {
		verificaCliqueBreadCrumb();
		LazyDataModel<PessoaFisica> listFlash = (LazyDataModel<PessoaFisica>) getFlashScope().get(LIST_FLASH);
		if(listFlash != null) {
			setLazyDataModel(listFlash);
			carregaPermissoes();
		}

		// TODO Rever e retirar do postConstruct pois pode prejudicar performance
		this.estadosCivis = estadoCivilSessionBean.retrieveAll();
		this.tiposContatos = tipoContatoSessionBean.retrieveAll(getStateManager().getEmpresaSelecionada().getId());

		this.setUfs(ufSessionBean.findAll());
		carregaTiposEndereco();
		carregaDadosFlash();
	}

	private void carregaPermissoes() {
		setPermiteAlteracao(isPodeAlterar());
		setPermiteExclusao(isPodeExcluir());
		setPermiteInclusao(isPodeIncluir());
		setPermiteVisualizacaoEmail(isPodeVisualizarEmails());
		setPermiteInclusaoEmail(isPodeIncluirEmails());
		setPermiteExclusaoEmail(isPodeExcluirEmails());
		setPermiteVisualizacaoContato(isPodeVisualizarContatos());
		setPermiteExclusaoContato(isPodeExcluirContatos());
		setPermiteInclusaoContato(isPodeIncluirContatos());
		setPermiteVisualizacaoEndereco(isPodeVisualizarEnderecos());
		setPermiteInclusaoEndereco(isPodeIncluirEnderecos());
		setPermiteExclusaoEndereco(isPodeExcluirEnderecos());
	}

	private void verificaCliqueBreadCrumb() {
		Map<String,String> params = getExternalContext().getRequestParameterMap();
		String pesquisaPorBreadCrumb = params.get("paramLink");
		if("true".equals(pesquisaPorBreadCrumb)) {
			super.buscarPorLazy();
			getFlashScope().put(LIST_FLASH, getLazyDataModel());
			carregaPermissoes();
		}
	}

	private void initObjetoCollectors() {
		super.initObjetoPessoaCollectors(getObjetoSelecionado().getPessoa());
	}

	/**
	 *
	 */
	public void buscarCep(ValueChangeEvent event) {
		String cep = (String) event.getNewValue();
		super.buscarCep(cep);
	}

	@Override
	protected void carregaLazyDataModel() {
		setLazyDataModel(new LazyEntityDataModel<PessoaFisica>() {
			@Override
			protected List<PessoaFisica> consultaFiltrada(ConsultaLazyFilter filtro) {
				filtro.setDescricao(getDescricaoFiltro());
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				filtro.setPropriedadeOrdenacao("pessoa.descricao");
				return pessoaFisicaSessionBean.filtrados(filtro, true, false);
			}

			@Override
			protected int countTotalRegistros(ConsultaLazyFilter filtro) {
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return pessoaFisicaSessionBean.quantidadeFiltrados(filtro, true, false);
			}
		});
	}

	private void carregaTiposEndereco() {
		this.tiposEnderecos = Lists.newArrayList();
		for (TipoEndereco tipoEndereco : TipoEndereco.values()) {
			this.tiposEnderecos.add(tipoEndereco);
		}
	}

	private void carregaDadosFlash() {
		PessoaFisica editFlash = (PessoaFisica) getFlashScope().get(EDIT_FLASH);
		Boolean novoRegistro = (Boolean) getFlashScope().get(IS_NOVO_FLASH);

		if (novoRegistro != null && novoRegistro) {
			novo();
			return;
		}

		if (editFlash != null) {
			setObjetoSelecionado(editFlash);
			editar(editFlash);
		}
	}

	public boolean isHasAccessCadastro(){
		return possuiAcesso(ADVOGADOS_ADVOGADO_FORM);
	}

	public boolean isHasAccess(){
		return possuiAcesso(ADVOGADOS_ADVOGADO_LIST);
	}

	@Override
	protected void antesInserir() throws CrudException {
		getObjetoSelecionado().setPessoa(new Pessoa());
		initObjetoCollectors();
	}

	@Override
	protected void aposCancelar() throws CrudException {
		updateContext(ADVOGADO_FORM);
		super.aposCancelar();
		super.novo();
	}

	public String novoAdvogado() {
		getFlashScope().put(IS_NOVO_FLASH, Boolean.TRUE);
		return PAGES_ADVOGADOS_ADVOGADO_FORM;
	}

	/**
	 *
	 * @return
	 */
	public String adicionarEmail() {
		super.adicionarEmailPessoaFisica(getObjetoSelecionado());
		setPessoaEmail(new PessoaEmail(getStateManager().getEmpresaSelecionada().getId()));
		return null;
	}


	@Override
	protected void antesDeEditar(PessoaFisica objeto) throws CrudException {
		initObjetoCollectors();
	}

	/**
	 *
	 * @return
	 */
	public String adicionarEndereco() {
		super.adicionarEnderecoPessoaFisica(getObjetoSelecionado());
		return null;
	}

	/**
	 *
	 * @return
	 */
	public String adicionarContato() {
		super.adicionarContatoPessoaFisica(getObjetoSelecionado());
		return null;
	}

	public void limpaNumeroEndereco() {
		if (getPessoaEndereco().getEndereco().getSemNumero()) {
			getPessoaEndereco().getEndereco().setNumero(null);
			return;
		}
		getPessoaEndereco().getEndereco().setSemNumero(false);
	}

	@Override
	protected void aposSalvar() throws CrudException {
		getObjetoSelecionado().setPessoa(new Pessoa());
		super.aposSalvar();
	}

	@Override
	protected void validar() throws ValidationException {
		PessoaFisica advogado = advogadoSessionBean.findNumeroOabExistente(getObjetoSelecionado());
		getObjetoSelecionado().validarAdvogado(advogado != null);
	}

	@Override
	protected void antesDeSalvar() throws CrudException {
		try {
			String cpf = getObjetoSelecionado().getCpf().replaceAll("[.-]", "");
			getObjetoSelecionado().setCpf(cpf);
			String rg = getObjetoSelecionado().getRg().replaceAll("[.-]", "");
			getObjetoSelecionado().setRg(rg);
			getObjetoSelecionado().setAdvogado(true);
			getObjetoSelecionado().getPessoa().setTipo(PESSOA_FISICA);
			getObjetoSelecionado().getPessoa().setCliente(false);
			getObjetoSelecionado().getPessoa().setAtivo(true);
			getObjetoSelecionado().getPessoa().setDataCadastro(new Date());
			adicionaLoginSenha();

			if(!getObjetoSelecionado().isAdvogadoInternoEscritorio()) {
				getObjetoSelecionado().getPessoa().setNomeIniciais(null);
			}

		} catch (Exception e) {
			LOGGER.error(e, e.getCause());
		}
	}

	private void adicionaLoginSenha() throws Exception {
		if(getObjetoSelecionado().getPessoa().getId() == null && getObjetoSelecionado().isAdvogadoInternoEscritorio()) {
			List<Pessoa> usuariosCadastrados = pessoaSessionBean.findUserList(getObjetoSelecionado().getCpf());
			String usuario = getObjetoSelecionado().getCpf().replaceAll("[.-]", "");

			if(usuariosCadastrados.isEmpty()) {
				byte[] senha = EncryptUtil.execute(Util.randomGenerateValue(10));
				createUpdateLogin(getObjetoSelecionado(), usuario, senha);
			} else {
				createUpdateLogin(getObjetoSelecionado(), usuario, usuariosCadastrados.get(0).getLogin().getSenha());
			}
		}
	}

	public void goToDetalhes(){
		RequestContext.getCurrentInstance().execute("openModal('#advogadoModal')");
	}

	@Override
	protected void antesDeExcluir() throws CrudException {
		getObjetoSelecionado().setDataExclusao(new Date());
		getObjetoSelecionado().getPessoa().setDataExclusao(new Date());
	}

	@Override
	protected void aposExcluir() throws CrudException {
		super.buscarPorLazy();
	}

	@Override
	protected void create(PessoaFisica pessoaFisica) throws ValidationException, CrudException {
		pessoaFisica.getPessoa().setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
		pessoaFisicaSessionBean.create(pessoaFisica);

		if(getObjetoSelecionado().isAdvogadoInternoEscritorio() && CrudStatus.CREATE.equals(crudStatus)) {
			enviaEmailNovoUsuarioPessoa(getObjetoSelecionado().getPessoa());
		}
	}

	public void verificaIsAdvogadoInterno() {
		if (!getObjetoSelecionado().isAdvogadoInternoEscritorio()) {
			getObjetoSelecionado().getPessoa().setEnviaEmail(false);
			getObjetoSelecionado().getPessoa().setLogin(null);
			getObjetoSelecionado().setAdvogadoTerceirizado(false);
		}
	}

	public void verificaIsAdvogadoTerceirizado() {
		if (getObjetoSelecionado().isAdvogadoInternoEscritorio() && getObjetoSelecionado().isAdvogadoTerceirizadoEscritorio()) {
			getObjetoSelecionado().getPessoa().setEnviaEmail(false);
			getObjetoSelecionado().getPessoa().setLogin(null);
		}
	}

	public String goToEditarPessoa(PessoaFisica pessoaFisica) {
		getFlashScope().put(EDIT_FLASH, pessoaFisica);
		return PAGES_ADVOGADOS_ADVOGADO_FORM;
	}

	public String goToList() {
		pessoaSessionBean.clearSession();
		pessoaFisicaSessionBean.clearSession();
		super.buscarPorLazy();
		getFlashScope().put(LIST_FLASH, getLazyDataModel());
		return PAGES_ADVOGADOS_ADVOGADO_LIST;
	}

	public void listFromSimpleSearch() {
		pessoaFisicaSessionBean.clearSession();
		super.buscarPorLazy();
	}

	@Override
	protected PessoaFisica inicializaObjetosLazy(PessoaFisica objeto) {
		pessoaFisicaSessionBean.clearSession();
		return pessoaFisicaSessionBean.retrieve(objeto.getId());
	}

	@Override
	protected void setEmpresaDeUsuarioLogado(PessoaFisica objetoSelecionado) {
		objetoSelecionado.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
	}

	@Override
	protected void addChaveAcesso() {
		setChaveAcesso(CHAVE_ACESSO);
	}

	@Override
	protected PessoaFisica instanciarObjetoNovo() {
		return new PessoaFisica();
	}

	public List<EstadoCivil> getEstadosCivis() {
		return estadosCivis;
	}

	public void setEstadosCivis(List<EstadoCivil> estadosCivis) {
		this.estadosCivis = estadosCivis;
	}

	public List<TipoContato> getTiposContatos() {
		return tiposContatos;
	}

	public void setTiposContatos(List<TipoContato> tiposContatos) {
		this.tiposContatos = tiposContatos;
	}

	public List<Uf> getUfs() {
		return ufs;
	}

	public void setUfs(List<Uf> ufs) {
		this.ufs = ufs;
	}

	public List<Perfil> getPerfis() {
		boolean admin = ADMIN.equals(getUser().getPerfil().getSigla());
		return perfilSessionBean.findAllByUsuario(getStateManager().getEmpresaSelecionada().getId(), admin);
	}

	public String getFilter() {
		return filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}

	public List<TipoEndereco> getTiposEnderecos() {
		return tiposEnderecos;
	}

	public void setTiposEnderecos(List<TipoEndereco> tiposEnderecos) {
		this.tiposEnderecos = tiposEnderecos;
	}

    public PessoaFisica getAdvogadoSelecionado() {
        return advogadoSelecionado;
    }

    public void setAdvogadoSelecionado(PessoaFisica advogadoSelecionado) {
        this.advogadoSelecionado = advogadoSelecionado;
    }

    @Override
    protected boolean isPodeAlterar(){
		return super.isPodeAlterar(ADVOGADOS_ADVOGADO_FORM);
	}

	@Override
	protected boolean isPodeExcluir(){
		return super.isPodeExcluir(ADVOGADOS_ADVOGADO_FORM);
	}

	@Override
	protected boolean isPodeIncluir(){
		return super.isPodeIncluir(ADVOGADOS_ADVOGADO_FORM);
	}

	public boolean isPodeSomenteVisualizar() {
		return isHasAccess() && !isPodeAlterar() && !isPodeIncluir();
	}

	public boolean isPodeVisualizarEmails() {
		return super.isPodeVisualizarEmails(ADVOGADO_EMAILS_FORM);
	}

	public boolean isPodeIncluirEmails() {
		return super.isPodeIncluirEmails(ADVOGADO_EMAILS_FORM);
	}

	public boolean isPodeExcluirEmails() {
		return super.isPodeExcluirEmails(ADVOGADO_EMAILS_FORM);
	}

	public boolean isPodeVisualizarContatos() {
		return super.isPodeVisualizarContatos(ADVOGADO_CONTATOS_FORM);
	}

	public boolean isPodeIncluirContatos() {
		return super.isPodeIncluirContatos(ADVOGADO_CONTATOS_FORM);
	}

	public boolean isPodeExcluirContatos() {
		return super.isPodeExcluirContatos(ADVOGADO_CONTATOS_FORM);
	}

	public boolean isPodeVisualizarEnderecos() {
		return super.isPodeVisualizarEnderecos(ADVOGADO_ENDERECOS_FORM);
	}

	public boolean isPodeIncluirEnderecos() {
		return super.isPodeIncluirEnderecos(ADVOGADO_ENDERECOS_FORM);
	}

	public boolean isPodeExcluirEnderecos() {
		return super.isPodeExcluirEnderecos(ADVOGADO_ENDERECOS_FORM);
	}

}
