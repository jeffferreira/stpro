package br.com.juridico.controller;

import br.com.juridico.datamodel.LazyEntityDataModel;
import br.com.juridico.entidades.EmailCaixaSaida;
import br.com.juridico.entidades.Parametro;
import br.com.juridico.enums.StatusEmailCaixaSaidaIndicador;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.impl.EmailCaixaSaidaSessionBean;
import br.com.juridico.impl.ParametroSessionBean;
import br.com.juridico.jms.GeradorDeMensagem;
import com.google.common.collect.Lists;
import org.apache.log4j.Logger;
import org.primefaces.model.LazyDataModel;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Jefferson on 06/08/2016.
 */
@ManagedBean
@ViewScoped
public class EmailCaixaSaidaController extends AbstractController {

    private static final Logger LOGGER = Logger.getLogger(EmailCaixaSaidaController.class);

    private static final String TEMPO_CONSULTA_EMAIL = "TEMPO_CONSULTA_EMAIL";
    private static final String MSG_SUCESSO_SALVAR = "msg_sucesso_salvar";
    private static final String ACTIVE = "active";

    @Inject
    private EmailCaixaSaidaSessionBean emailCaixaSaidaSessionBean;
    @Inject
    private ParametroSessionBean parametroSessionBean;
    @Inject
    private GeradorDeMensagem geradorDeMensagem;

    private EmailCaixaSaida emailCaixaSaidaSelecionado;

    private LazyDataModel<EmailCaixaSaida> emails;
    private List<EmailCaixaSaida> pendentes = Lists.newArrayList();
    private List<EmailCaixaSaida> selectedEmails = Lists.newArrayList();

    private String activePendentes;
    private String activeAguardandoEnvio;
    private String activeEnviados;
    private String activeLixeira;
    private boolean visualizaEmail;

    private int tempoPoll;
    private int countEmailsPendentes;
    private int countEmailsAguardandoEnvio;


    @PostConstruct
    public void initialize(){
        if(getUser() == null) {
            return;
        }
        carregaPendentes();
        if(tempoPoll == 0){
            parametroSessionBean.clearSession();
            Parametro parametro = parametroSessionBean.findBySigla(TEMPO_CONSULTA_EMAIL, getUser().getCodEmpresa());
            if(parametro == null) return;
            tempoPoll = Integer.parseInt(parametro.getDescricao());
            consultaEmailsPendentes();
            consultaEmailsAguardandoEnvio();
        }
    }

    public String updateCorpoEmail(){
        try {
            emailCaixaSaidaSessionBean.update(emailCaixaSaidaSelecionado);
        } catch (ValidationException e) {
            LOGGER.error(e, e.getCause());
        }
        visualizaEmail = Boolean.FALSE;
        return "";
    }

    private void consultaEmailsAguardandoEnvio() {
        if(getUser() == null) {
            return;
        }
        this.emailCaixaSaidaSessionBean.clearSession();
        this.countEmailsAguardandoEnvio = emailCaixaSaidaSessionBean.countEmails(
                StatusEmailCaixaSaidaIndicador.APROVADO.getValue(), false, getUser().getCodEmpresa());
    }

    public void consultaEmailsPendentes(){
        if(getUser() == null) {
            return;
        }
        this.emailCaixaSaidaSessionBean.clearSession();
        this.countEmailsPendentes = emailCaixaSaidaSessionBean.countEmails(
                StatusEmailCaixaSaidaIndicador.PENDENTE.getValue(), false, getUser().getCodEmpresa());
    }

    public void carregaPendentes(){
        if(getUser() == null){
            return;
        }
        activeMenu(0);
        boolean pesquisaProcessados = false;
        emailCaixaSaidaSessionBean.clearSession();
        this.pendentes = emailCaixaSaidaSessionBean.findEmailsPendentes(StatusEmailCaixaSaidaIndicador.PENDENTE, getUser().getCodEmpresa(), pesquisaProcessados);
    }

    public void carregaLazyAguardandoEnvio(){
        if(getUser() == null) {
            return;
        }
        activeMenu(1);
        setEmails(new LazyEntityDataModel<EmailCaixaSaida>() {
            boolean pesquisaProcessados = false;
            @Override
            protected List<EmailCaixaSaida> consultaFiltrada(ConsultaLazyFilter filtro) {
                filtro.setDescricao(StatusEmailCaixaSaidaIndicador.APROVADO.getValue());
                filtro.setCodigoEmpresa(getUser().getCodEmpresa());
                return emailCaixaSaidaSessionBean.filtrados(filtro, pesquisaProcessados);
            }

            @Override
            protected int countTotalRegistros(ConsultaLazyFilter filtro) {
                return emailCaixaSaidaSessionBean.countEmails(
                        StatusEmailCaixaSaidaIndicador.APROVADO.getValue(), pesquisaProcessados, getUser().getCodEmpresa());
            }
        });
    }

    public void carregaLazyEnviados(){
        if(getUser() == null) {
            return;
        }
        activeMenu(2);
        setEmails(new LazyEntityDataModel<EmailCaixaSaida>() {
            @Override
            protected List<EmailCaixaSaida> consultaFiltrada(ConsultaLazyFilter filtro) {
                filtro.setDescricao(StatusEmailCaixaSaidaIndicador.APROVADO.getValue());
                filtro.setCodigoEmpresa(getUser().getCodEmpresa());
                return emailCaixaSaidaSessionBean.filtrados(filtro, true);
            }

            @Override
            protected int countTotalRegistros(ConsultaLazyFilter filtro) {
                return emailCaixaSaidaSessionBean.countEmails(
                        StatusEmailCaixaSaidaIndicador.APROVADO.getValue(), true, getUser().getCodEmpresa());
            }
        });
    }

    public void carregaLazyRejeitados(){
        if(getUser() == null) {
            return;
        }
        activeMenu(3);
        setEmails(new LazyEntityDataModel<EmailCaixaSaida>() {
            boolean pesquisaProcessados = false;
            @Override
            protected List<EmailCaixaSaida> consultaFiltrada(ConsultaLazyFilter filtro) {
                filtro.setDescricao(StatusEmailCaixaSaidaIndicador.REJEITADO.getValue());
                filtro.setCodigoEmpresa(getUser().getCodEmpresa());
                return emailCaixaSaidaSessionBean.filtrados(filtro, pesquisaProcessados);
            }

            @Override
            protected int countTotalRegistros(ConsultaLazyFilter filtro) {
                return emailCaixaSaidaSessionBean.countEmails(
                        StatusEmailCaixaSaidaIndicador.REJEITADO.getValue(), pesquisaProcessados, getUser().getCodEmpresa());
            }
        });
    }

    public void aprovarEnvio(){
        if (!validaEmailsSelecionados()) return;
        atualizaStatus(StatusEmailCaixaSaidaIndicador.APROVADO.getCharValue());
        exibirMensagem(FacesMessage.SEVERITY_INFO, getMessage(MSG_SUCESSO_SALVAR));
        carregaPendentes();
        consultaEmailsPendentes();
        consultaEmailsAguardandoEnvio();
    }

    private void atualizaStatus(Character charValue) {
        for (EmailCaixaSaida selectedEmail : selectedEmails) {
            selectedEmail.setDataAlteracao(Calendar.getInstance().getTime());
            selectedEmail.setFlagStatus(charValue);
            try {
                emailCaixaSaidaSessionBean.update(selectedEmail);
                geradorDeMensagem.enviarMensagem(selectedEmail);
            } catch (ValidationException e) {
                for (String message : e.getMessages()) {
                    exibirMensagem(FacesMessage.SEVERITY_ERROR, message);
                }
            }
        }
    }

    public void enviarParaLixeira(){
        if (!validaEmailsSelecionados()) return;
        atualizaStatus(StatusEmailCaixaSaidaIndicador.REJEITADO.getCharValue());
        exibirMensagem(FacesMessage.SEVERITY_INFO, getMessage(MSG_SUCESSO_SALVAR));
        carregaPendentes();
        consultaEmailsPendentes();
        consultaEmailsAguardandoEnvio();
    }

    private boolean validaEmailsSelecionados() {
        if(selectedEmails.isEmpty()) {
            exibirMensagem(FacesMessage.SEVERITY_ERROR, "Por favor selecione um ou mais emails pendentes");
            return false;
        }
        return true;
    }

    public void atualizarCaixaSaida(){
        if(ACTIVE.equals(activePendentes)){
            carregaPendentes();
        } else if(ACTIVE.equals(activeAguardandoEnvio)) {
            carregaLazyAguardandoEnvio();
        } else if(ACTIVE.equals(activeEnviados)) {
            carregaLazyEnviados();
        } else {
            carregaLazyRejeitados();
        }
    }

    public String goToCaixaSaida(){
        return "/pages/email/list-email.xhtml?faces-redirect=true";
    }

    private void activeMenu(Integer posicao){
        switch (posicao){
            case 0:
                activePendentes = ACTIVE;
                activeAguardandoEnvio = "";
                activeEnviados = "";
                activeLixeira = "";
                break;
            case 1:
                activePendentes = "";
                activeAguardandoEnvio = ACTIVE;
                activeEnviados = "";
                activeLixeira = "";
                break;
            case 2:
                activePendentes = "";
                activeAguardandoEnvio = "";
                activeEnviados = ACTIVE;
                activeLixeira = "";
                break;
            case 3:
                activePendentes = "";
                activeAguardandoEnvio = "";
                activeEnviados = "";
                activeLixeira = ACTIVE;
                break;
        }
    }


    public int getCountEmailsPendentes() {
        return countEmailsPendentes;
    }

    public List<EmailCaixaSaida> getSelectedEmails() {
        return selectedEmails;
    }

    public void setSelectedEmails(List<EmailCaixaSaida> selectedEmails) {
        this.selectedEmails = selectedEmails;
    }

    public LazyDataModel<EmailCaixaSaida> getEmails() {
        return emails;
    }

    public void setEmails(LazyDataModel<EmailCaixaSaida> emails) {
        this.emails = emails;
    }

    public int getTempoPoll() {
        return tempoPoll;
    }

    public String getActivePendentes() {
        return activePendentes;
    }

    public String getActiveAguardandoEnvio() {
        return activeAguardandoEnvio;
    }

    public String getActiveEnviados() {
        return activeEnviados;
    }

    public String getActiveLixeira() {
        return activeLixeira;
    }

    public int getCountEmailsAguardandoEnvio() {
        return countEmailsAguardandoEnvio;
    }

    public List<EmailCaixaSaida> getPendentes() {
        return pendentes;
    }

    public void setPendentes(List<EmailCaixaSaida> pendentes) {
        this.pendentes = pendentes;
    }

    public boolean isVisualizaEmail() {
        return visualizaEmail;
    }

    public void setVisualizaEmail(boolean visualizaEmail) {
        this.visualizaEmail = visualizaEmail;
    }

    public EmailCaixaSaida getEmailCaixaSaidaSelecionado() {
        return emailCaixaSaidaSelecionado;
    }

    public void setEmailCaixaSaidaSelecionado(EmailCaixaSaida emailCaixaSaidaSelecionado) {
        this.emailCaixaSaidaSelecionado = emailCaixaSaidaSelecionado;
    }
}
