package br.com.juridico.controller;

import br.com.juridico.datamodel.LazyEntityDataModel;
import br.com.juridico.entidades.Direito;
import br.com.juridico.entidades.SubAndamento;
import br.com.juridico.exception.CrudException;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.impl.DireitoSessionBean;
import br.com.juridico.impl.PerfilAcessoSessionBean;
import br.com.juridico.impl.SubAndamentoSessionBean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Jefferson
 *
 */
@ManagedBean
@SessionScoped
public class SubAndamentoController extends CrudController<SubAndamento> {

	/**
	 *
	 */
	private static final long serialVersionUID = 2031590349467627766L;
	private static final String SUB_ANDAMENTO_FORM = "subAndamentoForm";
	private static final String CHAVE_ACESSO = "SUBANDAMENTO_SUB_ANDAMENTO_TEMPLATE";
	private static final String PAGINA_FORMULARIO = "/pages/cadastros/subandamento/sub-andamento-template.xhtml?faces-redirect=true";

	@Inject
	private SubAndamentoSessionBean subAndamentoSessionBean;
	@Inject
	private PerfilAcessoSessionBean perfilAcessoSessionBean;
	@Inject
	private DireitoSessionBean direitoSessionBean;

	public void init(){
		carregaLazyDataModel();
	}

	public void criarEventoChanged(ValueChangeEvent event){
		getObjetoSelecionado().setCriaEventoFlag((Boolean) event.getNewValue());
	}

	@Override
	protected void validar() throws ValidationException {
		subAndamentoSessionBean.clearSession();
		boolean jaExisteSubAndamento = subAndamentoSessionBean.isDescricaoJaCadastrada(getObjetoSelecionado(), getStateManager().getEmpresaSelecionada().getId());
		getObjetoSelecionado().validar(jaExisteSubAndamento);
	}

	@Override
	protected void create(SubAndamento subAndamento) throws ValidationException, CrudException {
		SubAndamento subAndamentoExclusao = getRegistroComExclusaoLogica();

		if (isEditavel() && subAndamentoExclusao != null) {
			subAndamentoExclusao.setDataExclusao(null);
			subAndamentoExclusao.setCriaEventoFlag(subAndamento.getCriaEventoFlag());
			subAndamentoExclusao.setDiasPrazoSeguranca(subAndamento.getDiasPrazoSeguranca());
			subAndamentoExclusao.setFlagPrazoPosteriorAndamento(subAndamento.getFlagPrazoPosteriorAndamento());
			subAndamentoSessionBean.update(subAndamentoExclusao);
		} else if (subAndamento != null && subAndamento.getDescricao() != null && !subAndamento.getDescricao().isEmpty()) {
			subAndamentoSessionBean.create(subAndamento);
		} else {
			throw new CrudException();
		}

	}

	@Override
	protected void carregaLazyDataModel() {
		setLazyDataModel(new LazyEntityDataModel<SubAndamento>() {
			@Override
			protected List<SubAndamento> consultaFiltrada(ConsultaLazyFilter filtro) {
				filtro.setDescricao(getDescricaoFiltro());
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return subAndamentoSessionBean.filtrados(filtro);
			}

			@Override
			protected int countTotalRegistros(ConsultaLazyFilter filtro) {
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return subAndamentoSessionBean.quantidadeFiltrados(filtro);
			}
		});
	}


	private SubAndamento getRegistroComExclusaoLogica() {
		return subAndamentoSessionBean.findByDescricao(getObjetoSelecionado().getDescricao(), getStateManager().getEmpresaSelecionada().getId());
	}

	@Override
	protected void antesDeExcluir() throws CrudException {
		getObjetoSelecionado().setDataExclusao(new Date());
	}

	@Override
	protected void aposExcluir() throws CrudException {
		super.buscarPorLazy();
	}

	@Override
	protected void aposCancelar() throws CrudException {
		getObjetoSelecionado().setDescricao(null);
		updateContext(SUB_ANDAMENTO_FORM);
		super.aposCancelar();
	}

	/**
	 *
	 * @return
	 */
	public String goToFormulario() {
		return PAGINA_FORMULARIO;
	}

	@Override
	protected SubAndamento inicializaObjetosLazy(SubAndamento objeto) {
		subAndamentoSessionBean.clearSession();
		return subAndamentoSessionBean.retrieve(objeto.getId());
	}

	@Override
	protected SubAndamento instanciarObjetoNovo() {
		return new SubAndamento();
	}

	@Override
	protected void setEmpresaDeUsuarioLogado(SubAndamento objetoSelecionado) {
		objetoSelecionado.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());

	}

	@Override
	protected void addChaveAcesso() {
		setChaveAcesso(CHAVE_ACESSO);
	}

	@Override
	protected boolean isPodeAlterar(){
		Direito direito = direitoSessionBean.retrieve(ALTERAR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

	@Override
	protected boolean isPodeExcluir(){
		Direito direito = direitoSessionBean.retrieve(EXCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

	@Override
	protected boolean isPodeIncluir(){
		Direito direito = direitoSessionBean.retrieve(INCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}
}
