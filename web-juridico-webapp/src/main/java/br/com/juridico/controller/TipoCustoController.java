package br.com.juridico.controller;

import br.com.juridico.datamodel.LazyEntityDataModel;
import br.com.juridico.entidades.Direito;
import br.com.juridico.entidades.TipoCusto;
import br.com.juridico.exception.CrudException;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.impl.DireitoSessionBean;
import br.com.juridico.impl.PerfilAcessoSessionBean;
import br.com.juridico.impl.TipoCustoSessionBean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;

/**
 * 
 * @author Marcos
 *
 */
@ManagedBean
@SessionScoped
public class TipoCustoController extends CrudController<TipoCusto> {

	/**
     * 
     */
	private static final long serialVersionUID = 6933129545478297709L;
	private static final String TIPO_CUSTO_FORM = "tipoCustoForm";
	private static final String CHAVE_ACESSO = "TIPOCUSTO_TIPO_CUSTO_TEMPLATE";
	private static final String PAGINA_FORMULARIO = "/pages/cadastros/tipoCusto/tipo-custo-template.xhtml?faces-redirect=true";

	@Inject
	private TipoCustoSessionBean tipoCustoSessionBean;
	@Inject
	private PerfilAcessoSessionBean perfilAcessoSessionBean;
	@Inject
	private DireitoSessionBean direitoSessionBean;

	public void init(){
		carregaLazyDataModel();
	}
	
	@Override
	protected void validar() throws ValidationException {
		tipoCustoSessionBean.clearSession();
		boolean jaExisteTipoCusto = tipoCustoSessionBean.isDescricaoJaCadastrada(getObjetoSelecionado(), getStateManager().getEmpresaSelecionada().getId());
		getObjetoSelecionado().validar(jaExisteTipoCusto);
	}

	@Override
	protected void carregaLazyDataModel() {
		setLazyDataModel(new LazyEntityDataModel<TipoCusto>() {
			@Override
			protected List<TipoCusto> consultaFiltrada(ConsultaLazyFilter filtro) {
				filtro.setDescricao(getDescricaoFiltro());
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return tipoCustoSessionBean.filtrados(filtro);
			}

			@Override
			protected int countTotalRegistros(ConsultaLazyFilter filtro) {
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return tipoCustoSessionBean.quantidadeFiltrados(filtro);
			}
		});
	}

	@Override
	protected void create(TipoCusto tipoCusto) throws ValidationException, CrudException {
		TipoCusto tipoCustoExclusao = getRegistroComExclusaoLogica();

		if (isEditavel() && tipoCustoExclusao != null) {
			tipoCustoExclusao.setDataExclusao(null);
			tipoCustoSessionBean.update(tipoCustoExclusao);
		} else if (tipoCusto != null && tipoCusto.getDescricao() != null && !tipoCusto.getDescricao().isEmpty()) {
			tipoCustoSessionBean.create(tipoCusto);
		} else {
			throw new CrudException();
		}

	}

	private TipoCusto getRegistroComExclusaoLogica() {
		return tipoCustoSessionBean.findByDescricao(getObjetoSelecionado().getDescricao(), getStateManager().getEmpresaSelecionada().getId());
	}

	@Override
	protected void antesDeExcluir() throws CrudException {
		getObjetoSelecionado().setDataExclusao(new Date());
	}

	@Override
	protected void aposExcluir() throws CrudException {
		super.buscarPorLazy();
	}
	
    @Override
    protected void aposCancelar() throws CrudException {
        getObjetoSelecionado().setDescricao(null);
        updateContext(TIPO_CUSTO_FORM);
        super.aposCancelar();
    }

	/**
	 * 
	 * @return
	 */
	public String goToFormulario() {
		return PAGINA_FORMULARIO;
	}

	@Override
	protected TipoCusto inicializaObjetosLazy(TipoCusto objeto) {
		tipoCustoSessionBean.clearSession();
		return tipoCustoSessionBean.retrieve(objeto.getId());
	}

	@Override
	protected TipoCusto instanciarObjetoNovo() {
		return new TipoCusto();
	}

	@Override
	protected void setEmpresaDeUsuarioLogado(TipoCusto objetoSelecionado) {
		objetoSelecionado.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());

	}

	@Override
	protected void addChaveAcesso() {
		setChaveAcesso(CHAVE_ACESSO);
	}

	@Override
	protected boolean isPodeAlterar(){
		Direito direito = direitoSessionBean.retrieve(ALTERAR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

	@Override
	protected boolean isPodeExcluir(){
		Direito direito = direitoSessionBean.retrieve(EXCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

	@Override
	protected boolean isPodeIncluir(){
		Direito direito = direitoSessionBean.retrieve(INCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

}
