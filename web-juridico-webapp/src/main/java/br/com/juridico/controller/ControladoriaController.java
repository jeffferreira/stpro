package br.com.juridico.controller;

import br.com.juridico.business.AgendaEmailBusiness;
import br.com.juridico.business.EmailTemplateBusiness;
import br.com.juridico.entidades.*;
import br.com.juridico.enums.ControladoriaIndicador;
import br.com.juridico.enums.EventoColorTypes;
import br.com.juridico.enums.TipoOperacaoIndicador;
import br.com.juridico.enums.TipoPessoaControladoriaType;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.filter.ControladoriaFilter;
import br.com.juridico.impl.*;
import br.com.juridico.state.ControladoriaState;
import br.com.juridico.state.controladoria.AprovaControladoria;
import br.com.juridico.state.controladoria.ControladoriaContext;
import br.com.juridico.state.controladoria.RejeitaControladoria;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.log4j.Logger;
import org.primefaces.PrimeFaces;
import org.primefaces.context.RequestContext;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 *
 * @author Jefferson
 *
 */
@ManagedBean
@SessionScoped
public class ControladoriaController extends AbstractController {


	/**
	 *
	 */
	private static final long serialVersionUID = -5363621670335559115L;
	private static final Logger LOGGER = Logger.getLogger(ControladoriaController.class);

	private static final String CHAVE_ACESSO = "PRAZOPROCESSO_PRAZO_PROCESSO_TEMPLATE";
	private static final String PAGINA_FORMULARIO_PRAZO_PROCESSO = "/pages/controladoria/prazoprocesso/prazo-processo-template.xhtml?faces-redirect=true";
	private static final String PROCESSOS_PROCESSO_DETAIL = "PROCESSOS_PROCESSO_DETAIL";
	private static final String ANDAMENTO_NOVO = "ANDAMENTO_NOVO";
	private static final String ANDAMENTO_ALTERACAO = "ANDAMENTO_ALTERACAO";
	private static final String ANDAMENTO_REJEITADO_CONTROLADORIA = "ANDAMENTO_REJEITADO_CONTROLADORIA";
	private static final String REJEITADO = "Rejeitado";

	@Inject
	private EventoSessionBean eventoSessionBean;
	@Inject
	private EmailSessionBean emailSessionBean;
	@Inject
	private PessoaSessionBean pessoaSessionBean;
	@Inject
	private PessoaFisicaSessionBean pessoaFisicaSessionBean;
	@Inject
	private EmailCaixaSaidaSessionBean emailCaixaSaidaSessionBean;
	@Inject
	private StatusAgendaSessionBean statusAgendaSessionBean;
	@Inject
	private ParteSessionBean parteSessionBean;

	private Evento eventoSelecionado;
	private List<Evento> eventosSelecionados;
	private ControladoriaFilter filter;

	private List<Evento> eventos = Lists.newArrayList();
	private List<SelectItem> tipoPessoaControladoria;
	private String motivoRejeito;

	@PostConstruct
	public void initialize(){
		this.filter = new ControladoriaFilter();
		this.tipoPessoaControladoria = buildSelectItemList(TipoPessoaControladoriaType.values());
		init();
	}

	public void init(){
		pesquisar();
	}

	public String goToPrazosProcessos(){
		return PAGINA_FORMULARIO_PRAZO_PROCESSO;
	}

	public void historicoListener(Evento evento){
		this.eventoSelecionado = evento;
		this.eventoSelecionado.getRejeitos();
		RequestContext.getCurrentInstance().update("historicoDetail");
	}

	/**
	 *
	 * @param event
	 */
	public void pfChanged(ValueChangeEvent event) {
		PessoaFisica pessoaFisica = (PessoaFisica) event.getNewValue();
		this.filter.setPessoaFisica(pessoaFisica);
	}

	/**
	 *
	 * @param event
	 */
	public void pfAdvogadoChanged(ValueChangeEvent event) {
		PessoaFisica pessoaFisica = (PessoaFisica) event.getNewValue();
		this.filter.setAdvogadoColaborador(pessoaFisica);
	}

	public void statusChanged(ValueChangeEvent event) {
		this.filter.setStatus((String) event.getNewValue());
		if(REJEITADO.equals(this.filter.getStatus())){
			this.filter.setControladoriaStatus(null);
		}
	}

	/**
	 *
	 * @param event
	 */
	public void pjChanged(ValueChangeEvent event) {
		PessoaJuridica pessoaJuridica = (PessoaJuridica) event.getNewValue();
		this.filter.setPessoaJuridica(pessoaJuridica);
	}

	public void pesquisar(){
		eventoSessionBean.clearSession();
		this.eventos = eventoSessionBean.findByEventosControladoriaFilter(this.filter, getStateManager().getEmpresaSelecionada().getId());
		carregaTiposOperacaoHistorico();
		limpar();
	}

	public List<Parte> getPartes(Evento evento){
		return parteSessionBean.findByProcesso(evento.getProcesso().getId(), getUser().getCodEmpresa());
	}

	public void limpar(){
		this.filter = new ControladoriaFilter();
		RequestContext.getCurrentInstance().update("groupFilter");
	}

	public void justificarRejeito(Evento evento){
		this.eventoSelecionado = evento;
		this.motivoRejeito = "";
		PrimeFaces.current().executeScript("PF('rejeitoDialog').show();");

	}

	public void carregaTiposOperacaoHistorico(){
		this.eventos.stream().forEach(e -> {
			Optional ultimoHistorico = e.getHistoricoEventos().stream().findFirst();
			HistoricoEvento historicoEvento = ultimoHistorico.isPresent() ? (HistoricoEvento) ultimoHistorico.get() : null;
			if(historicoEvento != null) {
				e.setUltimoTipoOperacao(historicoEvento.getTipoOperacao().getDescricao());
			} else {
				e.setUltimoTipoOperacao("");
			}
		});
	}

	public List<PessoaFisica> getAdvogadosAndColaboradores(){
		pessoaFisicaSessionBean.clearSession();
		return pessoaFisicaSessionBean.findByAdvogadosAndColaboradores(null, getUser().getCodEmpresa());
	}

	public void rejeitarListener(){
		if(Strings.isNullOrEmpty(this.motivoRejeito)){
			showMessageInDialog(FacesMessage.SEVERITY_ERROR, "Motivo", getMessage("fields.form.required.message"));
			return;
		}
		PessoaFisica pessoaRejeito = getPessoaFisicaRejeito();

		if(pessoaRejeito == null){
			showMessageInDialog(FacesMessage.SEVERITY_ERROR, "Mensagem", "N\u00e3o existe nenhum respons\u00e1vel para altera\u00e7\u00e3o.");
			return;
		}

		try {
			adicionaControladoria(new RejeitaControladoria());
			HistoricoEvento historicoEvento = new HistoricoEvento(this.eventoSelecionado, getUser().getName());
			historicoEvento.setQuemAlterou(getUser().getName());
			historicoEvento.setTipoOperacao(TipoOperacaoIndicador.RJT);
			this.eventoSelecionado.addHistorico(historicoEvento);
			this.eventoSelecionado.setStatusAgenda(statusAgendaSessionBean.findByDescricao(REJEITADO, getUser().getCodEmpresa()));
			this.eventoSelecionado.setStyle(EventoColorTypes.fromTextColor(eventoSelecionado.getStatusAgenda().getStyleColor()).getValue());
			this.eventoSelecionado.getRejeitos().add(new EventoRejeito(this.eventoSelecionado, getUser().getPessoa().getDescricao(), pessoaRejeito.getCpf(), this.motivoRejeito, getUser().getCodEmpresa()));
			enviarEmailEvento(this.eventoSelecionado);
			eventoSessionBean.create(this.eventoSelecionado);
			pesquisar();
			this.motivoRejeito = "";
		} catch (ValidationException e) {
			LOGGER.error(e, e.getCause());
		}
	}

	public PessoaFisica getPessoaFisicaRejeito() {
		if(this.eventoSelecionado != null){
			Pessoa pessoa = pessoaSessionBean.findByNome(this.eventoSelecionado.getHistoricoEventos().get(0).getQuemAlterou(), getUser().getCodEmpresa());
			return pessoaFisicaSessionBean.findByPessoa(pessoa.getId(), getUser().getCodEmpresa());
		}
		return null;
	}

	public void aprovacaoListener(Evento evento){
		this.eventoSelecionado = evento;
		try {
			adicionaControladoria(new AprovaControladoria());
			HistoricoEvento historicoEvento = new HistoricoEvento(this.eventoSelecionado, getUser().getName());
			historicoEvento.setQuemAprovou(getUser().getName());
			historicoEvento.setTipoOperacao(TipoOperacaoIndicador.APV);
			this.eventoSelecionado.addHistorico(historicoEvento);
			this.eventoSelecionado.setStyle(EventoColorTypes.fromTextColor(eventoSelecionado.getStatusAgenda().getStyleColor()).getValue());
			enviarEmailEvento(eventoSelecionado);
			eventoSessionBean.update(this.eventoSelecionado);
			pesquisar();
		} catch (ValidationException e) {
			LOGGER.error(e, e.getCause());
		}
	}

	public void aprovacaoListenerSelecionados(){
		if(this.eventosSelecionados.isEmpty()){
			showMessageInDialog(FacesMessage.SEVERITY_ERROR, "Mensagem", "N\u00e3o existe nenhum evento selecionado.");
			return;
		}
		for (Evento evento : this.eventosSelecionados) {
			if("P".equals(evento.getControladoriaStatus())) {
				this.aprovacaoListener(evento);
			}
		}
	}

	private void enviarEmailEvento(Evento eventoSelecionado) throws ValidationException {
		if (eventoSelecionado.getProcesso() != null && !eventoSelecionado.getHistoricoEventos().isEmpty()) {
			TipoOperacaoIndicador tipoOperacaoIndicadorEvento = eventoSelecionado.getHistoricoEventos().stream().findFirst().get().getTipoOperacao();

			if (tipoOperacaoIndicadorEvento == TipoOperacaoIndicador.INC || tipoOperacaoIndicadorEvento == TipoOperacaoIndicador.APV) {
				enviarEmail(ANDAMENTO_NOVO, eventoSelecionado);
			} else if (tipoOperacaoIndicadorEvento == TipoOperacaoIndicador.ALT) {
				enviarEmail(ANDAMENTO_ALTERACAO, eventoSelecionado);
			} else if(tipoOperacaoIndicadorEvento == TipoOperacaoIndicador.RJT){
				enviarEmail(ANDAMENTO_REJEITADO_CONTROLADORIA, eventoSelecionado);
			}
		}
	}

	private void enviarEmail(String tipoOperacaoIndicador, Evento evento) throws ValidationException {
		Email email = emailSessionBean.findBySigla(tipoOperacaoIndicador, getStateManager().getEmpresaSelecionada().getId());
		if(email == null || !email.getAtivo()) {
			return;
		}
		Map<String, String> parametersValues = Maps.newHashMap();
		for (EmailVinculoParametro parametro : email.getParametroEmails()) {
			String valor = AgendaEmailBusiness.execute(parametro.getParametroEmail(), evento, new Evento(), getUser());
			parametersValues.put(parametro.getParametroEmail().getSigla(), valor);
		}

		String textoFormatado = EmailTemplateBusiness.criarEmailTemplate(email.getAssunto(), email.getDescricao(), parametersValues);
		EmailCaixaSaida emailCaixaSaida = new EmailCaixaSaida(getUser().getName(), email.getAssunto(), textoFormatado, getStateManager().getEmpresaSelecionada().getId());
		Pessoa responsavel = evento.getPessoa() != null ? pessoaSessionBean.retrieve(evento.getPessoa().getId()) : null;

		addDestinatarios(emailCaixaSaida, responsavel);
		sendEmailCaixaSaida(email, emailCaixaSaida);
	}

	private void addDestinatarios(EmailCaixaSaida caixaSaida, Pessoa responsavel) {
		List<Pessoa> administradores = pessoaSessionBean.findByAdministradores(getStateManager().getEmpresaSelecionada().getId());

		for (Pessoa p : administradores) {
			PessoaEmail pessoaEmail = p.getPessoasEmails().isEmpty() ? null : p.getPessoasEmails().get(0);
			if (pessoaEmail != null) {
				EmailDestinatario destinatario = new EmailDestinatario(caixaSaida, pessoaEmail.getEmail());
				caixaSaida.addEmails(destinatario);
			}
		}

		if(responsavel != null) {
			caixaSaida.addEmails(new EmailDestinatario(caixaSaida, responsavel.getPessoasEmails().get(0).getEmail()));
		}
	}

	private void sendEmailCaixaSaida(Email email, EmailCaixaSaida emailCaixaSaida) throws ValidationException {
		if(!email.getNecessarioAprovacao()){
			emailCaixaSaidaSessionBean.createSemAprovacao(emailCaixaSaida);
		} else {
			emailCaixaSaidaSessionBean.create(emailCaixaSaida);
		}
	}

	private void adicionaControladoria(ControladoriaState controladoriaState) {
		ControladoriaContext context = new ControladoriaContext();
		context.setState(controladoriaState);
		this.eventoSelecionado.setControladoriaStatus(context.controladoriaAction(
				false, this.eventoSelecionado.getStatusAgenda(),
				getUser().getEmpresa().isConfiguracaoControladoria(),
				ControladoriaIndicador.fromValue(this.eventoSelecionado.getControladoriaStatus())).getStatus());
	}

	public List<SelectItem> getTipoPessoaControladoria() {
		return tipoPessoaControladoria;
	}

	public void setTipoPessoaControladoria(List<SelectItem> tipoPessoaControladoria) {
		this.tipoPessoaControladoria = tipoPessoaControladoria;
	}

	public void dataInicioAlteracaoChanged(ValueChangeEvent event){
		this.filter.setDataInicioAlteracao((Date) event.getNewValue());
	}

	public void dataFimAlteracaoChanged(ValueChangeEvent event){
		this.filter.setDataFimAlteracao((Date) event.getNewValue());
	}

	public void dataInicioPrazoChanged(ValueChangeEvent event){
		this.filter.setDataInicioPrazo((Date) event.getNewValue());
	}

	public void dataFimPrazoChanged(ValueChangeEvent event){
		this.filter.setDataFimPrazo((Date) event.getNewValue());
	}

	public boolean isHasAccessDetalhes(){
		return possuiAcesso(PROCESSOS_PROCESSO_DETAIL);
	}

	public boolean isHasAccess(){
		return possuiAcesso(CHAVE_ACESSO);
	}

	public List<Evento> getEventos() {
		return eventos;
	}

	public Evento getEventoSelecionado() {
		return eventoSelecionado;
	}

	public void setEventoSelecionado(Evento eventoSelecionado) {
		this.eventoSelecionado = eventoSelecionado;
	}

	public List<Evento> getEventosSelecionados() {
		return eventosSelecionados;
	}

	public void setEventosSelecionados(List<Evento> eventosSelecionados) {
		this.eventosSelecionados = eventosSelecionados;
	}

	public ControladoriaFilter getFilter() {
		return filter;
	}

	public void setFilter(ControladoriaFilter filter) {
		this.filter = filter;
	}

	public String getMotivoRejeito() {
		return motivoRejeito;
	}

	public void setMotivoRejeito(String motivoRejeito) {
		this.motivoRejeito = motivoRejeito;
	}

}
