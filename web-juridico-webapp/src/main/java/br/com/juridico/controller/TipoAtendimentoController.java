package br.com.juridico.controller;

import br.com.juridico.datamodel.LazyEntityDataModel;
import br.com.juridico.entidades.Direito;
import br.com.juridico.entidades.TipoAtendimento;
import br.com.juridico.exception.CrudException;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.impl.DireitoSessionBean;
import br.com.juridico.impl.PerfilAcessoSessionBean;
import br.com.juridico.impl.TipoAtendimentoSessionBean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;

/**
 * 
 * @author Jefferson
 *
 */
@ManagedBean
@SessionScoped
public class TipoAtendimentoController extends CrudController<TipoAtendimento> {

	private static final String TIPO_ATENDIMENTO_FORM = "tipoAtendimentoForm";
	private static final String CHAVE_ACESSO = "TIPOATENDIMENTO_TIPO_ATENDIMENTO_TEMPLATE";
	private static final String PAGINA_FORMULARIO = "/pages/cadastros/tipoAtendimento/tipo-atendimento-template.xhtml?faces-redirect=true";

	@Inject
	private TipoAtendimentoSessionBean tipoAtendimentoSessionBean;
	@Inject
	private PerfilAcessoSessionBean perfilAcessoSessionBean;
	@Inject
	private DireitoSessionBean direitoSessionBean;

	public void init(){
		carregaLazyDataModel();
	}
	
	@Override
	protected void validar() throws ValidationException {
		tipoAtendimentoSessionBean.clearSession();
		boolean jaExisteTipoAtendimento = tipoAtendimentoSessionBean.isDescricaoJaCadastrada(getObjetoSelecionado(), getStateManager().getEmpresaSelecionada().getId());
		getObjetoSelecionado().validar(jaExisteTipoAtendimento);
	}

	@Override
	protected void carregaLazyDataModel() {
		setLazyDataModel(new LazyEntityDataModel<TipoAtendimento>() {
			@Override
			protected List<TipoAtendimento> consultaFiltrada(ConsultaLazyFilter filtro) {
				filtro.setDescricao(getDescricaoFiltro());
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return tipoAtendimentoSessionBean.filtrados(filtro);
			}

			@Override
			protected int countTotalRegistros(ConsultaLazyFilter filtro) {
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return tipoAtendimentoSessionBean.quantidadeFiltrados(filtro);
			}
		});
	}

	@Override
	protected void create(TipoAtendimento tipoAtendimento) throws ValidationException, CrudException {
		TipoAtendimento tipoAtendimentoExclusao = getRegistroComExclusaoLogica();

		if (isEditavel() && tipoAtendimentoExclusao != null) {
			tipoAtendimentoExclusao.setDataExclusao(null);
			tipoAtendimentoSessionBean.update(tipoAtendimentoExclusao);
		} else if (tipoAtendimento != null && tipoAtendimento.getDescricao() != null
				&& !tipoAtendimento.getDescricao().isEmpty()) {
			tipoAtendimentoSessionBean.create(tipoAtendimento);
		} else {
			throw new CrudException();
		}

	}

	private TipoAtendimento getRegistroComExclusaoLogica() {
		return tipoAtendimentoSessionBean.findByDescricao(getObjetoSelecionado().getDescricao(),
				getStateManager().getEmpresaSelecionada().getId());
	}

	@Override
	protected void antesDeExcluir() throws CrudException {
		getObjetoSelecionado().setDataExclusao(new Date());
	}

	@Override
	protected void aposExcluir() throws CrudException {
		super.buscarPorLazy();
	}
	
    @Override
    protected void aposCancelar() throws CrudException {
        getObjetoSelecionado().setDescricao(null);
        updateContext(TIPO_ATENDIMENTO_FORM);
        super.aposCancelar();
    }

	/**
	 * 
	 * @return
	 */
	public String goToFormulario() {
		return PAGINA_FORMULARIO;
	}

	@Override
	protected TipoAtendimento inicializaObjetosLazy(TipoAtendimento objeto) {
		tipoAtendimentoSessionBean.clearSession();
		return tipoAtendimentoSessionBean.retrieve(objeto.getId());
	}

	@Override
	protected TipoAtendimento instanciarObjetoNovo() {
		return new TipoAtendimento();
	}

	@Override
	protected void setEmpresaDeUsuarioLogado(TipoAtendimento objetoSelecionado) {
		objetoSelecionado.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());

	}

	@Override
	protected void addChaveAcesso() {
		setChaveAcesso(CHAVE_ACESSO);
	}

	@Override
	protected boolean isPodeAlterar(){
		Direito direito = direitoSessionBean.retrieve(ALTERAR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

	@Override
	protected boolean isPodeExcluir(){
		Direito direito = direitoSessionBean.retrieve(EXCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

	@Override
	protected boolean isPodeIncluir(){
		Direito direito = direitoSessionBean.retrieve(INCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}
}
