package br.com.juridico.controller;

import br.com.juridico.datamodel.LazyEntityDataModel;
import br.com.juridico.entidades.Direito;
import br.com.juridico.entidades.FormaPagamento;
import br.com.juridico.exception.CrudException;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.impl.DireitoSessionBean;
import br.com.juridico.impl.FormaPagamentoSessionBean;
import br.com.juridico.impl.PerfilAcessoSessionBean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Jefferson
 *
 */
@ManagedBean
@SessionScoped
public class FormaPagamentoController extends CrudController<FormaPagamento> {

    private static final String FORMA_PAGAMENTO_FORM = "formaPagamentoForm";
    private static final String CHAVE_ACESSO = "FORMAPAGAMENTO_FORMA_PAGAMENTO_TEMPLATE";
    private static final String PAGINA_FORMULARIO = "/pages/cadastros/formapagamento/forma-pagamento-template.xhtml?faces-redirect=true";

    @Inject
    private FormaPagamentoSessionBean formaPagamentoSessionBean;
    @Inject
    private PerfilAcessoSessionBean perfilAcessoSessionBean;
    @Inject
    private DireitoSessionBean direitoSessionBean;

    public void init(){
        carregaLazyDataModel();
    }

    @Override
    protected void validar() throws ValidationException {
        formaPagamentoSessionBean.clearSession();
        boolean jaExisteFormaPagamento = formaPagamentoSessionBean.isDescricaoJaCadastrada(getObjetoSelecionado(), getStateManager().getEmpresaSelecionada().getId());
        getObjetoSelecionado().validar(jaExisteFormaPagamento);
    }

    @Override
    protected void aposCancelar() throws CrudException {
        getObjetoSelecionado().setDescricao(null);
        updateContext(FORMA_PAGAMENTO_FORM);
        super.aposCancelar();
    }

    @Override
    protected void create(FormaPagamento formaPagamento) throws ValidationException, CrudException {
        FormaPagamento formaPagamentoExclusao = getRegistroComExclusaoLogica();

        if (isEditavel() && formaPagamentoExclusao != null) {
            formaPagamentoExclusao.setDataExclusao(null);
            formaPagamentoSessionBean.update(formaPagamentoExclusao);
        } else if (formaPagamento != null && formaPagamento.getDescricao() != null && !formaPagamento.getDescricao().isEmpty()) {
            formaPagamentoSessionBean.create(formaPagamento);
        } else {
            throw new CrudException();
        }
    }

    @Override
    protected void antesDeExcluir() throws CrudException {
        getObjetoSelecionado().setDataExclusao(new Date());
    }

    @Override
    protected void aposExcluir() throws CrudException {
        super.buscarPorLazy();
    }

    @Override
    protected void carregaLazyDataModel() {
        setLazyDataModel(new LazyEntityDataModel<FormaPagamento>() {
            @Override
            protected List<FormaPagamento> consultaFiltrada(ConsultaLazyFilter filtro) {
                filtro.setDescricao(getDescricaoFiltro());
                filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
                return formaPagamentoSessionBean.filtrados(filtro);
            }

            @Override
            protected int countTotalRegistros(ConsultaLazyFilter filtro) {
                filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
                return formaPagamentoSessionBean.quantidadeFiltrados(filtro);
            }
        });
    }


    @Override
    protected FormaPagamento inicializaObjetosLazy(FormaPagamento objeto) {
        formaPagamentoSessionBean.clearSession();
        return formaPagamentoSessionBean.retrieve(objeto.getId());
    }

    @Override
    protected FormaPagamento instanciarObjetoNovo() {
        return new FormaPagamento();
    }

    @Override
    protected void setEmpresaDeUsuarioLogado(FormaPagamento objetoSelecionado) {
        objetoSelecionado.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());

    }

    private FormaPagamento getRegistroComExclusaoLogica() {
        return formaPagamentoSessionBean.findByDescricao(getObjetoSelecionado().getDescricao(), getStateManager().getEmpresaSelecionada().getId());
    }

    /**
     *
     * @return
     */
    public String goToFormulario() {
        return PAGINA_FORMULARIO;
    }

    @Override
    protected void addChaveAcesso() {
        setChaveAcesso(CHAVE_ACESSO);
    }

    @Override
    protected boolean isPodeAlterar(){
        Direito direito = direitoSessionBean.retrieve(ALTERAR);
        return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
    }

    @Override
    protected boolean isPodeExcluir(){
        Direito direito = direitoSessionBean.retrieve(EXCLUIR);
        return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
    }

    @Override
    protected boolean isPodeIncluir(){
        Direito direito = direitoSessionBean.retrieve(INCLUIR);
        return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
    }
}
