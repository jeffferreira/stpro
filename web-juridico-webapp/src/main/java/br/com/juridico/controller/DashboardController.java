package br.com.juridico.controller;

import br.com.juridico.dto.DashboardDto;
import br.com.juridico.entidades.*;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.impl.*;
import com.google.common.collect.Lists;
import org.apache.log4j.Logger;
import org.primefaces.model.chart.PieChartModel;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import java.io.IOException;
import java.util.List;

@ManagedBean
@SessionScoped
public class DashboardController extends AbstractController {

	private static final Logger LOGGER = Logger.getLogger(DashboardController.class);
	private static final Integer PROCESSOS_RECENTES_MAX = Integer.valueOf(10);
	private static final String DASHBOARD_PROCESSOS_ADICIONADOS_RECENTEMENTE = "DASHBOARD_PROCESSOS_ADICIONADOS_RECENTEMENTE";
	private static final String DASHBOARD_MENU_DESCRIPTION = "Dashboards";

	/**
	 *
	 */
	private static final long serialVersionUID = -1867569420706404150L;

	@Inject
	private ProcessoSessionBean processoSessionBean;
	@Inject
	private AcessoSessionBean acessoSessionBean;
	@Inject
	private PerfilAcessoSessionBean perfilAcessoSessionBean;
	@Inject
	private DireitoSessionBean direitoSessionBean;
	@Inject
	private DireitoPerfilSessionBean direitoPerfilSessionBean;
	@Inject
	private MenuSessionBean menuSessionBean;
	@Inject
	private PessoaConfiguracaoSessionBean pessoaConfiguracaoSessionBean;
	@Inject
	private StatusAgendaSessionBean statusAgendaSessionBean;
	@Inject
	private EventoSessionBean eventoSessionBean;
	@Inject
	private EventoRejeitoSessionBean eventoRejeitoSessionBean;


	private PieChartModel pieAgendaModel;

	private List<DashboardDto> dashboardList = Lists.newArrayList();
	private List<PessoaConfiguracao> configuracoes = Lists.newArrayList();

	/**
	 * @PostConstruct
	 */
	@PostConstruct
	public void initialize() {
		createPieAgendaModel();
		setPermiteConsulta(isPodeConsultar());
		this.pessoaConfiguracaoSessionBean.clearSession();
		this.configuracoes = pessoaConfiguracaoSessionBean.findByPessoa(getUser().getId());
		carregarDashboardDoUsuarioPerfil();

	}

	private void carregarDashboardDoUsuarioPerfil() {
		Menu menu = menuSessionBean.findByDescricao(DASHBOARD_MENU_DESCRIPTION);
		getUser().getPerfil().getAcessos().stream().filter(perfilAcesso -> perfilAcesso.getAcesso().getMenu().getId().equals(menu.getId())).forEach(perfilAcesso -> {
			DashboardDto dashboardDto = new DashboardDto();
			for (PessoaConfiguracao config : this.configuracoes) {
				if(config.getDireitoPerfil().getPerfilAcesso().getId().equals(perfilAcesso.getId())){
					dashboardDto.setChecked(true);
					break;
				}
			}

			dashboardDto.setFuncionalidade(perfilAcesso.getAcesso().getFuncionalidade());
			dashboardDto.setTitulo(perfilAcesso.getAcesso().getDescricao());
			dashboardDto.setSubTitulo(perfilAcesso.getAcesso().getSubTitulo());
			dashboardList.add(dashboardDto);
		});
	}

	public Integer getEventosRejeitados(){
		List<EventoRejeito> rejeitos = eventoRejeitoSessionBean.findRejeitosByUsuario(getUser().getPessoa());
		return rejeitos.size();
	}

	public List<ViewProcesso> getProcessosRecentes(){
		processoSessionBean.clearSession();
		return processoSessionBean.findByTopValue(PROCESSOS_RECENTES_MAX, getStateManager().getEmpresaSelecionada().getId());
	}

	protected boolean isPodeConsultar(){
		Direito direito = direitoSessionBean.retrieve(CONSULTAR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), DASHBOARD_PROCESSOS_ADICIONADOS_RECENTEMENTE, direito, getUser().getCodEmpresa());
	}

	public void atualizaConfiguracao(DashboardDto configuracao) throws IOException {
		if(configuracao.isChecked()) {
			createConfiguracao(configuracao);
			navigationToHome();
			return;
		}
		deleteConfiguracao(configuracao);
		navigationToHome();
	}

	private void deleteConfiguracao(DashboardDto configuracao) {
		pessoaConfiguracaoSessionBean.clearSession();
		List<PessoaConfiguracao> configuracoes = pessoaConfiguracaoSessionBean.findByPessoa(getUser().getId());
		for (PessoaConfiguracao config : configuracoes) {
			Acesso acesso = config.getDireitoPerfil().getPerfilAcesso().getAcesso();
			if(acesso.getFuncionalidade().equals(configuracao.getFuncionalidade())){
				pessoaConfiguracaoSessionBean.delete(config);
			}
		}
	}

	private void createConfiguracao(DashboardDto configuracao) {
		try {
			Acesso acesso = acessoSessionBean.findByFuncionalidade(configuracao.getFuncionalidade());
			PerfilAcesso perfilAcesso = perfilAcessoSessionBean.findByPerfilAndAcesso(getUser().getPerfil().getId(), acesso.getId(), getUser().getCodEmpresa());
			List<DireitoPerfil> direitos = direitoPerfilSessionBean.findByPerfilAcesso(perfilAcesso.getId(),getUser().getCodEmpresa());
			if(direitos.isEmpty()){
				return;
			}
			PessoaConfiguracao pessoaConfiguracao = new PessoaConfiguracao(getUser().getPessoa(), direitos.stream().findFirst().get(), configuracao.getTitulo(), getUser().getCodEmpresa());
			pessoaConfiguracaoSessionBean.create(pessoaConfiguracao);

		} catch (ValidationException e) {
			LOGGER.error(e, e.getCause());
		}
	}

	public boolean isConfigurouDashboard(String funcionalidade) {
		Acesso acesso = acessoSessionBean.findByFuncionalidade(funcionalidade);
		perfilAcessoSessionBean.clearSession();
		PerfilAcesso perfilAcesso = perfilAcessoSessionBean.findByPerfilAndAcesso(getUser().getPerfil().getId(), acesso.getId(), getUser().getCodEmpresa());

		if(perfilAcesso != null) {
			pessoaConfiguracaoSessionBean.clearSession();
			this.configuracoes = pessoaConfiguracaoSessionBean.findByPessoa(getUser().getId());

			for (DireitoPerfil direitoPerfil : perfilAcesso.getDireitoPerfis()) {
				for (PessoaConfiguracao config : configuracoes) {
					if (config.getDireitoPerfil().getId().equals(direitoPerfil.getId())) {
						return Boolean.TRUE;
					}
				}
			}
		}
		return Boolean.FALSE;
	}

	public boolean isUsuarioPossuiAlgumEventoCadastrado(){
		return getUser().getPessoa() != null && eventoSessionBean.findCountByUser(getUser().getPessoa(), getUser().getCodEmpresa()) > 0;

	}

	private void createPieAgendaModel() {
		pieAgendaModel = new PieChartModel();
		List<StatusAgenda> todosStatus = statusAgendaSessionBean.findAllStatusAtivos(getUser().getCodEmpresa());
		todosStatus.stream().forEach(s -> pieAgendaModel.set(s.getDescricao(), carregarQtdeTotalAgendaPorTipo(s)));
		pieAgendaModel.setLegendPosition("w");
	}

	private Integer carregarQtdeTotalAgendaPorTipo(StatusAgenda statusAgenda) {

		return eventoSessionBean.findCountByUserStatus(statusAgenda, getUser().getPessoa(), getUser().getCodEmpresa());
	}

	public List<DashboardDto> getDashboardList() {
		return dashboardList;
	}

	public PieChartModel getPieAgendaModel() {
		return pieAgendaModel;
	}

	public void setPieAgendaModel(PieChartModel pieAgendaModel) {
		this.pieAgendaModel = pieAgendaModel;
	}
}
