package br.com.juridico.controller.relatorios;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import br.com.juridico.entidades.StatusAgenda;
import br.com.juridico.impl.StatusAgendaSessionBean;
import br.com.juridico.util.DateUtils;
import org.apache.log4j.Logger;
import org.primefaces.event.SelectEvent;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import br.com.juridico.entidades.Evento;
import br.com.juridico.entidades.PessoaFisica;
import br.com.juridico.filter.RelatorioAgendaGeralFilter;
import br.com.juridico.impl.EventoSessionBean;
import br.com.juridico.impl.PessoaFisicaSessionBean;
import br.com.juridico.util.ExportUtil;

@ManagedBean
@ViewScoped
public class RelatorioAgendaGeralController extends AbstractRelatorioController<Evento> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8904567556191395015L;
	private static final Logger LOGGER = Logger.getLogger(RelatorioAgendaGeralController.class);
	
	private static final String COLABORADORES = "COLABORADORES";
	private static final String TODOS = "TODOS";
	private static final String JRXML = "agenda-geral-report";
	private static final String NOME_RELATORIO = "Relatório Agenda";
	private static final String CHAVE_ACESSO = "FILTROS_AGENDA_GERAL_FILTER_FORM";

	@Inject
	private EventoSessionBean eventoSessionBean;
	@Inject
	private PessoaFisicaSessionBean pessoaFisicaSessionBean;
	@Inject
	private StatusAgendaSessionBean statusAgendaSessionBean;

	private RelatorioAgendaGeralFilter filter;
	private List<PessoaFisica> pessoas = Lists.newArrayList();

	/**
	 * @PostConstruct
	 */
	@PostConstruct
	public void initialize() {
		this.filter = new RelatorioAgendaGeralFilter();
		this.pessoas = pessoaFisicaSessionBean.findByAdvogadosAndColaboradores(getStateManager().getEmpresaSelecionada().getId());
	}

	@Override
	public String nomeRelatorioJrxml() {
		return JRXML;
	}

	@Override
	public String nomeRelatorio() {
		return NOME_RELATORIO;
	}

	@Override
	public void addParameters() {
		Map<String, Object> parameters = Maps.newHashMap();

		try {
			String realPath = getFacesContext().getExternalContext()
					.getRealPath(File.separator + "resources" + File.separator + "logo4.png");
			parameters.put("PERIODO_FILTRO", filter.getDescricaoPeriodo());
			parameters.put("LOGO", ExportUtil.getLogo(new File(realPath)));

		} catch (IOException e) {
			LOGGER.error(e, e.getCause());
		}

		setParameters(parameters);
	}

	@Override
	public List<Evento> pesquisar() {
		this.filter.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
		DateUtils.somaDias(this.filter.getDataFinal(), 1);
		return eventoSessionBean.findReportByPeriodoAndPessoa(filter);
	}

	@Override
	public boolean validouFiltros() {
		return filter.getDataInicial() != null && filter.getDataFinal() != null;
	}

	@Override
	protected void addChaveAcesso() {
		setChaveAcesso(CHAVE_ACESSO);
	}

	@Override
	public void gerarRelatorioPdf() {
		super.gerarRelatorioPdf();
	}

	public void opcoesChanged() {
		if (TODOS.equals(this.filter.getOpcaoPessoa())) {
			this.pessoas = pessoaFisicaSessionBean.findByAdvogadosAndColaboradores(getStateManager().getEmpresaSelecionada().getId());
		} else if (COLABORADORES.equals(this.filter.getOpcaoPessoa())) {
			this.pessoas = pessoaFisicaSessionBean.getColaboradores(getStateManager().getEmpresaSelecionada().getId());
		} else {
			this.pessoas = pessoaFisicaSessionBean.getAdvogados(true, getStateManager().getEmpresaSelecionada().getId());
		}

	}

	public List<StatusAgenda> getStatus(){
		return statusAgendaSessionBean.findAllStatusAtivos(getStateManager().getEmpresaSelecionada().getId());
	}

	public void dataInicioChanged(SelectEvent event) {
		filter.setDataInicial((Date) event.getObject());
	}

	public RelatorioAgendaGeralFilter getFilter() {
		return filter;
	}

	public void setFilter(RelatorioAgendaGeralFilter filter) {
		this.filter = filter;
	}

	public List<PessoaFisica> getPessoas() {
		return pessoas;
	}

	@Override
	public boolean getPermiteVisualizar() {
		return super.isPodeVisualizar(CHAVE_ACESSO);
	}
	
	public boolean isHasAccess(){
		return possuiAcesso(CHAVE_ACESSO);
	}


}
