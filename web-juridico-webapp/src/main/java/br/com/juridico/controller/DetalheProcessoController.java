package br.com.juridico.controller;

import br.com.juridico.business.EmailTemplateBusiness;
import br.com.juridico.business.ProcessoEmailBusiness;
import br.com.juridico.business.TipoAndamentoBusiness;
import br.com.juridico.entidades.*;
import br.com.juridico.enums.ControladoriaIndicador;
import br.com.juridico.enums.EmailIndicador;
import br.com.juridico.enums.EventoColorTypes;
import br.com.juridico.enums.TipoOperacaoIndicador;
import br.com.juridico.exception.CrudException;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.impl.*;
import br.com.juridico.state.ControladoriaState;
import br.com.juridico.state.controladoria.AprovaControladoria;
import br.com.juridico.state.controladoria.ControladoriaContext;
import br.com.juridico.types.DetalheProcessoStepsTypes;
import br.com.juridico.util.DateUtils;
import br.com.juridico.util.FileUtil;
import br.com.juridico.util.Util;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.primefaces.event.SelectEvent;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import javax.servlet.http.Part;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Jefferson on 24/07/2016.
 */
@ManagedBean
@SessionScoped
public class DetalheProcessoController extends AbstractProcessoController {

	/**
	 *
	 */
	private static final long serialVersionUID = 1798118538154537761L;
	private static final Logger LOGGER = Logger.getLogger(DetalheProcessoController.class);
	private static final String[] STEP_TYPES = {"STEP_ANDAMENTO", "STEP_PRAZOS"};

	private static final String PAGES_PROCESSOS_PROCESSO_DETAIL = "/pages/processos/processo-detail.xhtml?faces-redirect=true";
	private static final String PROCESSO_DETALHE_FORM = "processoDetalheForm";
	private static final String MSG_SUCESSO_SALVAR = "msg_sucesso_salvar";
	private static final String MSG_SUCESSO_EXCLUIR = "msg_sucesso_excluir";
	private static final String PROCESSOS_VISUALIZACAO_PARTES = "PROCESSOS_VISUALIZACAO_PARTES";
	private static final String PROCESSOS_VISUALIZACAO_PRAZO_FATAL = "PROCESSOS_VISUALIZACAO_PRAZO_FATAL";
	private static final String PROCESSOS_VISUALIZACAO_DADOS_PROCESSO = "PROCESSOS_VISUALIZACAO_DADOS_PROCESSO";
	private static final String PROCESSOS_VISUALIZACAO_RESUMO = "PROCESSOS_VISUALIZACAO_RESUMO";
	private static final String PROCESSOS_VISUALIZACAO_VALORES = "PROCESSOS_VISUALIZACAO_VALORES";
	private static final String PROCESSOS_CADASTRO_HONORARIOS = "PROCESSOS_CADASTRO_HONORARIOS";
	private static final String PROCESSOS_ANDAMENTOS = "PROCESSOS_ANDAMENTOS";
	private static final String PROCESSOS_VINCULADOS = "PROCESSOS_VINCULADOS";
	private static final String PROCESSOS_PROPOSTAS = "PROCESSOS_PROPOSTAS";
	private static final String PROCESSOS_CUSTAS = "PROCESSOS_CUSTAS";
	private static final String CUMPRIDO = "Cumprido";
	private static final String A_CUMPRIR = "À cumprir";
	private static final String NAO_HA_CADASTRO = "Não há cadastro";
	public static final String PROPRIO_JUIZO = "P";

	@Inject
	private ProcessoSessionBean processoSessionBean;
	@Inject
	private AndamentoSessionBean andamentoSessionBean;
	@Inject
	private TipoAndamentoSessionBean tipoAndamentoSessionBean;
	@Inject
	private TipoCustoSessionBean tipoCustoSessionBean;
	@Inject
	private FormaPagamentoSessionBean formaPagamentoSessionBean;
	@Inject
	private PessoaFisicaSessionBean pessoaFisicaSessionBean;
	@Inject
	private CustaSessionBean custaSessionBean;
	@Inject
	private EmailCaixaSaidaSessionBean emailCaixaSaidaSessionBean;
	@Inject
	private EmailSessionBean emailSessionBean;
	@Inject
	private EventoSessionBean eventoSessionBean;
	@Inject
	private DireitoSessionBean direitoSessionBean;
	@Inject
	private PerfilAcessoSessionBean perfilAcessoSessionBean;
	@Inject
	private ProcessoVinculoSessionBean processoVinculoSessionBean;
	@Inject
	private StatusAgendaSessionBean statusAgendaSessionBean;
	@Inject
	private PessoaSessionBean pessoaSessionBean;
	@Inject
	private EmpresaSessionBean empresaSessionBean;
	@Inject
	private HistoricoPropostaSessionBean historicoPropostaSessionBean;
	@Inject
	private ParteSessionBean parteSessionBean;

	private List<TipoAndamento> tiposAndamentos;
	private List<AndamentoAdvogado> andamentoAdvogados = Lists.newArrayList();
	private List<PessoaFisica> selectedAdvogadosResumoAndamento = Lists.newArrayList();
	private List<PessoaFisica> advogadosResumoAndamento = Lists.newArrayList();
	private List<ProcessoVinculo> processoVinculos = Lists.newArrayList();
	private List<PrazoAndamento> prazosSelecionados = Lists.newArrayList();

	private boolean andamentoSelected;
	private boolean vinculadosSelected;
	private boolean custasSelected;
	private boolean propostasSelected;
	private boolean inicializouContexto;
	private int rowAnexoCustaNumber;

	private Empresa empresa;
	private TipoOperacaoIndicador tipoOperacao;
	private Part arquivoCustas;
	private ViewProcesso processoSelecionado;
	private Andamento novoAndamento;
	private Custa novaCusta;
	private ProcessoVinculo novoVinculo;
	private Processo processoVinculoSelecionado;
	private DetalheProcessoStepsTypes stepsTypesSelecionado;
	private HistoricoProposta novaProposta;
	private String stepClick;

	public void initialize() {
		if(!inicializouContexto){
			initWizardAndamento();
			this.empresa = empresaSessionBean.retrieve(getStateManager().getEmpresaSelecionada().getId());
			carregaProcessoSelecionado();
			inicializouContexto = Boolean.TRUE;
		}
	}

	protected void initWizardAndamento() {
		this.stepsTypesSelecionado = DetalheProcessoStepsTypes.STEP_ANDAMENTO;
		this.stepClick = stepsTypesSelecionado.getValue();
	}

	public void carregaProcessoSelecionado(){
		if(this.andamentoSelected) {
			return;
		}

		setPermiteAlteracaoAndamento(isPodeAlterarAndamento());
		setPermiteExclusaoAndamento(isPodeExcluirAndamento());
		setPermiteInclusaoAndamento(isPodeIncluirAndamento());
		setPermiteInclusaoVinculado(isPodeIncluirVinculado());
		setPermiteAlteracaoVinculado(isPodeAlterarVinculado());
		setPermiteExclusaoVinculado(isPodeExcluirVinculado());
		setPermiteAlteracaoCusta(isPodeAlterarCusta());
		setPermiteExclusaoCusta(isPodeExcluirCusta());
		setPermiteInclusaoCusta(isPodeIncluirCusta());
		setPermiteAlteracaoProposta(isPodeAlterarProposta());
		setPermiteExclusaoProposta(isPodeExcluirProposta());
		setPermiteInclusaoProposta(isPodeIncluirProposta());
		carregaProcessoRelacionados();
	}

	private void carregaProcessoRelacionados() {
		if(getProcesso() != null) {
			this.processoVinculos = processoVinculoSessionBean.findListByProcesso(getProcesso().getId(), getStateManager().getEmpresaSelecionada().getId());
			for (ProcessoVinculo processoVinculo : this.processoVinculos) {
				processoVinculo.setProcesso(getProcesso().getId());
			}
		}
	}

	public void removerSubAndamento(){

	}

	@Override
	protected Processo instanciarObjetoNovo() {
		return new Processo();
	}

	@Override
	protected void setEmpresaDeUsuarioLogado(Processo objetoSelecionado) {
		objetoSelecionado.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
	}

	@Override
	protected void create(Processo objetoSelecionado) throws ValidationException, CrudException {
		create(objetoSelecionado);
	}

	@Override
	protected void navigationToDetalhes() {
		getFacesContext().getApplication().getNavigationHandler().handleNavigation(getFacesContext(), null, "/pages/processos/processo-detail.xhtml?faces-redirect=true");
	}

	public void excluirAndamento() throws ValidationException {
		if(novoAndamento.isPossuiEventoPendente()) {
			showMessageInDialog(FacesMessage.SEVERITY_ERROR, getMessage("msg_erro_excluir"), getMessage("fields.form.controladoria.message"));
			return;
		}
		Date dataExclusao = Calendar.getInstance().getTime();
		novoAndamento.setDataExclusao(dataExclusao);
		novoAndamento.getPrazos().stream().forEach(p -> p.setDataExclusao(dataExclusao));
		novoAndamento.getEventos().stream().forEach(e -> e.setDataExclusao(dataExclusao));
		novoAndamento.getAndamentoAdvogados().stream().forEach(a -> a.setDataExclusao(dataExclusao));

		andamentoSessionBean.update(novoAndamento);
		exibirMensagem(FacesMessage.SEVERITY_INFO, getMessage(MSG_SUCESSO_EXCLUIR));
		processoSessionBean.clearSession();
		setProcesso(processoSessionBean.findById(getProcesso().getId(), getProcesso().getCodigoEmpresa()));
	}

	public void excluirCusta() {
		getProcesso().getCustas().remove(novaCusta);
		updateProcesso(MSG_SUCESSO_EXCLUIR);
	}

	public void excluirProposta() {
		getProcesso().getPropostas().remove(novaProposta);
		updateProcesso(MSG_SUCESSO_EXCLUIR);
	}

	public void atualizarPagamentoHonorario(int posicao){
		processoSessionBean.clearSession();
		setProcesso(processoSessionBean.retrieve(getProcesso().getId()));
		ParcelaHonorario parcelaHonorario = getProcesso().getProcessoHonorario().getParcelaHonorarios().get(posicao);

		if(parcelaHonorario.getPago() != null && parcelaHonorario.getPago()) {
			getProcesso().getProcessoHonorario().getParcelaHonorarios().get(posicao).setPago(false);
		} else if(parcelaHonorario.getPago() != null && !parcelaHonorario.getPago()) {
			getProcesso().getProcessoHonorario().getParcelaHonorarios().get(posicao).setPago(true);
		} else {
			getProcesso().getProcessoHonorario().getParcelaHonorarios().get(posicao).setPago(false);
		}

		try {
			processoSessionBean.update(getProcesso());
			updateContext(PROCESSO_DETALHE_FORM);
		} catch (ValidationException e) {
			LOGGER.error(e,e.getCause());
		}
	}

	public void excluirVinculado() {
		processoVinculoSessionBean.delete(novoVinculo.getId());
		updateProcesso(MSG_SUCESSO_EXCLUIR);
	}

	public void excluirCustaDocumento(){
		int posicaoLista = 0;
		for (Iterator<CustaDocumento> iterator = novaCusta.getCustaDocumentos().iterator(); iterator.hasNext();) {
			iterator.next();
			if(rowAnexoCustaNumber == posicaoLista){
				iterator.remove();
			}
			posicaoLista++;
		}
	}

	private void updateProcesso(String mensagemSucesso) {
		try {
			processoSessionBean.update(getProcesso());
			processoSessionBean.clearSession();
			setProcesso(processoSessionBean.retrieve(getProcesso().getId()));
			exibirMensagem(FacesMessage.SEVERITY_INFO, getMessage(mensagemSucesso));
		} catch (ValidationException e) {
			for (String message : e.getMessages()) {
				exibirMensagem(FacesMessage.SEVERITY_ERROR, message);
			}
		}
	}

	public void importar() {
		try {
			byte[] bytes = IOUtils.toByteArray(arquivoCustas.getInputStream());
			String fileName = FileUtil.getFileName(arquivoCustas);
			String extensao = fileName.substring(fileName.lastIndexOf("."), fileName.length());
			CustaDocumento documento = new CustaDocumento(novaCusta, fileName, extensao, bytes,
					getStateManager().getEmpresaSelecionada().getId());
			getNovaCusta().addCustaDocumento(documento);
			updateContext(PROCESSO_DETALHE_FORM);
		} catch (IOException e) {
			LOGGER.error(e, e.getCause());
		}
	}

	private void antesDeSalvarAndamentoProcesso() throws CrudException, ValidationException {
		if (!novoAndamento.getDefinePrazoFinal()) {
			limpaDadosDoPrazo();
		}

		if(novoAndamento.getHorarioIndefinido() != null && !novoAndamento.getHorarioIndefinido()) {
			Calendar fim = Calendar.getInstance();
			fim.setTime(novoAndamento.getDataFinalPrazoFatal());
			fim.set(Calendar.HOUR_OF_DAY, getHora(novoAndamento.getHoraFim(), 21));
			fim.set(Calendar.MINUTE, getMinuto(novoAndamento.getHoraFim()));
			novoAndamento.setDataFinalPrazoFatal(fim.getTime());
		}
		novoAndamento.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
		novoAndamento.setProcesso(getProcesso());
		setDataPrazoFinal();
		setAndamentoNoPrazo();
		enviaAgendaParaResponsaveis();
	}

	private void setDataPrazoFinal() {
		if (novoAndamento.getDataFinalPrazo() != null) {
			novoAndamento.setDataFinalPrazoFatal(novoAndamento.getDataFinalPrazo());
			novoAndamento.setDataFinalPrazo(DateUtils.diaMenosUmValor(novoAndamento.getDataFinalPrazoFatal(), novoAndamento.getTipoAndamento().getDiasPrazoSeguranca().intValue()));
		}
	}

	private void setAndamentoNoPrazo() {
		for (PrazoAndamento prazoAndamento : novoAndamento.getPrazos()) {
			prazoAndamento.setAndamento(novoAndamento);
			if (prazoAndamento.getDataInicioPrazo() != null && prazoAndamento.getDataFinalPrazo() != null) {
				prazoAndamento.setDataInicioPrazoFatal(prazoAndamento.getDataInicioPrazo());
				prazoAndamento.setDataFinalPrazoFatal(prazoAndamento.getDataFinalPrazo());

				prazoAndamento.setDataInicioPrazo(DateUtils.diaMenosUmValor(prazoAndamento.getDataInicioPrazo(), prazoAndamento.getSubAndamento().getDiasPrazoSeguranca().intValue()));
				prazoAndamento.setDataFinalPrazo(DateUtils.diaMenosUmValor(prazoAndamento.getDataFinalPrazo(), prazoAndamento.getSubAndamento().getDiasPrazoSeguranca().intValue()));
			}
		}
	}

	private void enviaAgendaParaResponsaveis() throws ValidationException {
		novoAndamento.getPrazos().stream().filter(prazo -> prazo.getId() == null && prazo.getPessoaFisica() != null).forEach(prazo -> {
			Evento evento = new Evento(generateChaveEventoId(), prazo.getSubAndamento().getDescricao(),
					novoAndamento.getStatusAgenda(), prazo.getDataInicioPrazo(), prazo.getDataFinalPrazo(),
					EventoColorTypes.YELLOW.getValue(), prazo.getPessoaFisica().getPessoa(), this.empresa);
			evento.setHorarioIndefinido(prazo.getHorarioIndefinido());
			evento.setAndamento(novoAndamento);
			novoAndamento.getEventos().add(evento);
		});
	}

	private String generateChaveEventoId() {
		StringBuilder sb = new StringBuilder();
		sb.append(Util.randomGenerateValue(8)).append("-").append(Util.randomGenerateValue(4)).append("-")
				.append(Util.randomGenerateValue(4)).append("-").append(Util.randomGenerateValue(4)).append("-")
				.append(Util.randomGenerateValue(12));
		return sb.toString();
	}

	public void stepNext() {
		switch (stepClick){
			case "STEP_ANDAMENTO" :
				this.stepsTypesSelecionado = DetalheProcessoStepsTypes.fromStep(STEP_TYPES[1]);
				this.stepClick = STEP_TYPES[1];
				break;
			default:
				this.stepsTypesSelecionado = DetalheProcessoStepsTypes.fromStep(STEP_TYPES[0]);
				this.stepClick = STEP_TYPES[0];
		}
		carregaAdvogadoPrincipal();

	}

	public boolean isRenderizaHouveAcordoAndamento() {
		if(novoAndamento == null) {
			return false;
		}
		StatusAgenda prazoCumprido = getStatusAgendaCumprido();
		boolean prazoAtendido = novoAndamento.getStatusAgenda() != null
				&& prazoCumprido != null
				&& prazoCumprido.getId().equals(novoAndamento.getStatusAgenda().getId());

		return prazoAtendido && (TipoAndamentoBusiness.isAudienciaConciliacao(novoAndamento.getTipoAndamento())
				|| TipoAndamentoBusiness.isAudienciaInstrucao(novoAndamento.getTipoAndamento())
				|| TipoAndamentoBusiness.isAudienciaOitiva(novoAndamento.getTipoAndamento()));
	}

	private StatusAgenda getStatusAgendaCumprido() {
		return statusAgendaSessionBean.findByDescricao(CUMPRIDO, getStateManager().getEmpresaSelecionada().getId());
	}

	private void limpaDadosDoPrazo() {
		novoAndamento.setDataFinalPrazo(null);
		novoAndamento.setDataFinalPrazoFatal(null);
		novoAndamento.setHorarioIndefinido(true);
		novoAndamento.setHoraInicio("");
		novoAndamento.setHoraFim("");
	}

	public void editarAndamento(Andamento andamentoSelecionado) {
		this.novoAndamento = andamentoSelecionado;
		this.selectedAdvogadosResumoAndamento = Lists.newArrayList();

		for (AndamentoAdvogado andamentoAdvogado : novoAndamento.getAndamentoAdvogados()) {
			novoAndamento.setEnviaResumoAdvogado(Boolean.TRUE);
			selectedAdvogadosResumoAndamento.add(andamentoAdvogado.getAdvogadoPessoa());
		}
		goToAndamento();
	}

	public void definePrazoChanged(ValueChangeEvent event) {
		boolean checkDefinePrazo = (Boolean) event.getNewValue();
		novoAndamento.setDefinePrazoFinal(checkDefinePrazo);
		novoAndamento.setHoraInicio("");
		novoAndamento.setHoraFim("");
	}

	public void localizacaoChanged(ValueChangeEvent event){
		novoAndamento.setOpcaoLocal(event.getNewValue().toString());
		if(PROPRIO_JUIZO.equals(novoAndamento.getOpcaoLocal())){
			String templateFormat = "%1$s | %2$s";
			novoAndamento.setLocalizacao(String.format(templateFormat, getProcesso().getVara().getDescricao(), getProcesso().getComarca().toString()));
			return;
		}
		novoAndamento.setLocalizacao("");
	}

	public void editarCusta() {
		goToCusta();
	}

	public void editarProposta() {
		goToPropostas();
	}

	private void goToCusta() {
		custaSessionBean.clearSession();
		this.andamentoSelected = false;
		this.vinculadosSelected = false;
		this.propostasSelected = false;
		this.custasSelected = true;
		updateContext(PROCESSO_DETALHE_FORM);
	}

	private void goToPropostas(){
		this.andamentoSelected = false;
		this.vinculadosSelected = false;
		this.propostasSelected = true;
		this.custasSelected = false;
		updateContext(PROCESSO_DETALHE_FORM);
	}

	private void antesDeSalvarCustaProcesso() throws CrudException {
		novaCusta.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
		novaCusta.setProcesso(getProcesso());
	}

	private void goToVinculado() {
		this.andamentoSelected = false;
		this.vinculadosSelected = true;
		this.custasSelected = false;
		this.propostasSelected = false;
		updateContext(PROCESSO_DETALHE_FORM);
	}

	public void salvarAndamentoProcesso() {
		try {
			adicionarAdvogadosSelecionados();
			novoAndamento.validarAndamento();
			novoAndamento.validarPrazos();
			antesDeSalvarAndamentoProcesso();
			setEmpresaDeUsuarioLogado(getProcesso());

			if (novoAndamento.getId() == null) {
				this.tipoOperacao = TipoOperacaoIndicador.INC;
				enviarEventoCliente();
				enviarEventoResponsavelAndamento();
				enviarEventoResponsavelSubAndamento();
				criarAndamento();
			} else {
				this.tipoOperacao = TipoOperacaoIndicador.ALT;
				atualizarAndamento();
			}

			enviarEmailResumoAndamento();
			enviarEmailAdvogados();
			exibirMensagem(FacesMessage.SEVERITY_INFO, getMessage(MSG_SUCESSO_SALVAR));
			inicializouContexto = Boolean.FALSE;
		} catch (CrudException e) {
			LOGGER.error(e);
		} catch (ValidationException e) {
			for (String message : e.getMessages()) {
				exibirMensagem(FacesMessage.SEVERITY_ERROR, message);
			}
			return;
		}
		returnToDetalhes();
	}

	private void adicionarAdvogadosSelecionados() {
		setAdvogadosAndamento();
		novoAndamento.setAndamentoAdvogados(getAndamentoAdvogados());
	}

	private void adicionaControladoria(boolean edicao, Evento evento) {
		ControladoriaContext context = new ControladoriaContext();
		ControladoriaState aprovacaoState = new AprovaControladoria();
		context.setState(aprovacaoState);
		evento.setControladoriaStatus(context.controladoriaAction(
				edicao, evento.getStatusAgenda(),
				getUser().getEmpresa().isConfiguracaoControladoria(),
				ControladoriaIndicador.fromValue(evento.getControladoriaStatus())).getStatus());
	}

	private void criarAndamento() throws ValidationException {
		novoAndamento.removePrazosNovosExcluidosOuAdicionaAndamento();
		getProcesso().addAndamento(novoAndamento);
		processoSessionBean.update(getProcesso());

		int lastIndex = getProcesso().getAndamentos().size() - 1;

		if(lastIndex >= 0)
			novoAndamento = getProcesso().getAndamentos().get(lastIndex);
	}

	private void atualizarAndamento() throws ValidationException {
		enviarEventoCliente();
		enviarEventoResponsavelAndamento();
		enviarEventoResponsavelSubAndamento();
		if(!novoAndamento.getEventos().isEmpty()) {
			atualizaStatusEstiloAgenda();
		}
		andamentoSessionBean.clearSession();
		andamentoSessionBean.update(novoAndamento);
	}

	private void atualizaStatusEstiloAgenda() {
		for (Evento evento : novoAndamento.getEventos()) {
			if(evento.getPrazoAndamento() == null) {
				StatusAgenda statusAgenda = novoAndamento.getStatusAgenda();
				EventoColorTypes colorType = EventoColorTypes.fromTextColor(statusAgenda.getStyleColor());
				String styleClassSelect = colorType == null ? null : colorType.getValue();
				evento.setStatusAgenda(statusAgenda);
				evento.setStyle(styleClassSelect);
			} else {
				StatusAgenda statusAgenda = evento.getPrazoAndamento().getStatusAgenda();
				EventoColorTypes colorType = EventoColorTypes.fromTextColor(statusAgenda.getStyleColor());
				String styleClassSelect = colorType == null ? null : colorType.getValue();
				evento.setStatusAgenda(statusAgenda);
				evento.setStyle(styleClassSelect);
			}
		}
	}

	private void enviarEventoResponsavelSubAndamento() {
		for (PrazoAndamento sub : novoAndamento.getPrazos()) {

			Map<Evento, Evento> eventoMap = eventoSessionBean.findByPrazoAndamento(sub.getId(), getStateManager().getEmpresaSelecionada().getId());
			if(eventoMap.isEmpty()) {
				PessoaFisica pessoaFisica = sub.getPessoaFisica();
				Calendar inicio = getHoraInicioSubAndamento(sub);
				Calendar fim = getHoraFimSubAndamento(sub);
				StatusAgenda statusAgenda = sub.getStatusAgenda();
				EventoColorTypes colorType = EventoColorTypes.fromTextColor(statusAgenda.getStyleColor());
				String styleClassSelect = colorType == null ? null : colorType.getValue();
				Pessoa pessoa = pessoaFisica == null ? null : pessoaFisica.getPessoa();
				Evento evento = new Evento(generateChaveEventoId(), sub.getSubAndamento().getDescricao(), statusAgenda, inicio.getTime(), fim.getTime(), styleClassSelect, pessoa, this.empresa);
				evento.setNomeIniciais(pessoa == null ? null : pessoa.getNomeIniciais());
				evento.setObservacao(sub.getObservacao());
				evento.setAndamento(novoAndamento);
				evento.setProcesso(novoAndamento.getProcesso());
				evento.setHorarioIndefinido(sub.getHorarioIndefinido());
				evento.setPrazoAndamento(sub);
				HistoricoEvento historico = new HistoricoEvento(evento, getUser().getName());
				historico.setTipoOperacao(this.tipoOperacao);
				evento.addHistorico(historico);
				this.novoAndamento.getEventos().add(evento);
				continue;
			}
			Calendar inicio = getHoraInicioSubAndamento(sub);
			Calendar fim = getHoraFimSubAndamento(sub);
			setEventoDaListaSubAndamento(sub, inicio, fim);
		}
	}

	private Calendar getHoraFimSubAndamento(PrazoAndamento sub) {
		Calendar fim = Calendar.getInstance();
		fim.setTime(getDia(sub.getDataFinalPrazo(), novoAndamento.getDataFinalPrazo()));
		fim.set(Calendar.HOUR_OF_DAY, getHora(sub.getHoraFim(), 21));
		fim.set(Calendar.MINUTE, getMinuto(sub.getHoraFim()));
		return fim;
	}

	private Calendar getHoraInicioSubAndamento(PrazoAndamento sub) {
		Calendar inicio = Calendar.getInstance();
		inicio.setTime(getDia(sub.getDataInicioPrazo(), novoAndamento.getDataFinalPrazo()));
		inicio.set(Calendar.HOUR_OF_DAY, getHora(sub.getHoraInicio(), 6));
		inicio.set(Calendar.MINUTE, getMinuto(sub.getHoraInicio()));
		return inicio;
	}

	private void enviarEventoResponsavelAndamento() {
		try {
			if (!novoAndamento.getDefinePrazoFinal()) {
				return;
			}
			eventoSessionBean.clearSession();
			Map<Evento, Evento> eventoMap = eventoSessionBean.findByAndamento(novoAndamento.getId(), getStateManager().getEmpresaSelecionada().getId());

			if(eventoMap.isEmpty()) {
				PessoaFisica pessoaFisica = novoAndamento.getPessoaFisica();
				Evento evento = getEvento(pessoaFisica == null ? null : pessoaFisica.getPessoa());
				if(evento != null) {
					criarEvento(evento);
				}
				return;
			}
			Calendar inicio = Calendar.getInstance();
			inicio.setTime(novoAndamento.getDataFinalPrazo());
			inicio.set(Calendar.HOUR_OF_DAY, getHora(novoAndamento.getHoraInicio(), 6));
			inicio.set(Calendar.MINUTE, getMinuto(novoAndamento.getHoraInicio()));

			Calendar fim = Calendar.getInstance();
			fim.setTime(novoAndamento.getDataFinalPrazo());
			fim.set(Calendar.HOUR_OF_DAY, getHora(novoAndamento.getHoraFim(), 21));
			fim.set(Calendar.MINUTE, getMinuto(novoAndamento.getHoraFim()));
			setEventoDaLista(inicio, fim);

		} catch (ValidationException e) {
			for (String message : e.getMessages()) {
				exibirMensagem(FacesMessage.SEVERITY_ERROR, message);
			}
		}
	}

	private void setEventoDaLista(Calendar inicio, Calendar fim) {
		if(novoAndamento.getPessoaFisica() == null) {
			return;
		}
		for (Evento evento : novoAndamento.getEventos()) {
			if(evento.getPrazoAndamento() == null && evento.getAndamento().getId().equals(novoAndamento.getId())){
				boolean edicao = novoAndamento.getId() != null;
				evento.setPessoa(novoAndamento.getPessoaFisica().getPessoa());
				evento.setNomeIniciais(novoAndamento.getPessoaFisica().getPessoa().getNomeIniciais());
				evento.setStatusAgenda(novoAndamento.getStatusAgenda());
				evento.setObservacao(novoAndamento.getComplemento());
				evento.setDataInicio(inicio.getTime());
				evento.setDataFim(fim.getTime());
				evento.setHorarioIndefinido(novoAndamento.getHorarioIndefinido());
				evento.setDescricao(novoAndamento.getTipoAndamento().getDescricao());
				evento.setLocalizacao(novoAndamento.getLocalizacao());
				adicionaControladoria(edicao, evento);
				HistoricoEvento historico = new HistoricoEvento(evento, getUser().getName());
				historico.setTipoOperacao(this.tipoOperacao);
				evento.addHistorico(historico);
			}
		}
	}

	private void setEventoDaListaSubAndamento(PrazoAndamento prazoAndamento, Calendar inicio, Calendar fim) {
		if(prazoAndamento.getPessoaFisica() == null){
			return;
		}
		for (Evento evento : novoAndamento.getEventos()) {
			if(evento.getPrazoAndamento() != null && evento.getPrazoAndamento().getId().equals(prazoAndamento.getId())){
				boolean edicao = evento.getPrazoAndamento().getId() != null;
				evento.setPessoa(prazoAndamento.getPessoaFisica() != null ? prazoAndamento.getPessoaFisica().getPessoa() : null);
				evento.setNomeIniciais(prazoAndamento.getPessoaFisica() != null ? prazoAndamento.getPessoaFisica().getPessoa().getNomeIniciais() : null);
				evento.setStatusAgenda(prazoAndamento.getStatusAgenda());
				evento.setObservacao(prazoAndamento.getObservacao());
				evento.setDataInicio(inicio.getTime());
				evento.setDataFim(fim.getTime());
				evento.setHorarioIndefinido(prazoAndamento.getHorarioIndefinido());
				evento.setDescricao(prazoAndamento.getSubAndamento().getDescricao());
				adicionaControladoria(edicao, evento);
				HistoricoEvento historico = new HistoricoEvento(evento, getUser().getName());
				historico.setTipoOperacao(this.tipoOperacao);
				evento.addHistorico(historico);
			}
		}
	}

	/**
	 * Envia evento para cliente caso tipo do andamento possuir flag = true
	 */
	public void enviarEventoCliente() throws ValidationException {
		Pessoa cliente = novoAndamento.getProcesso().getClienteProcesso();

		if(cliente == null) return;
		if(novoAndamento.getProcesso().getPrivado()) return;

		if(novoAndamento.getTipoAndamento().getCriaEventoFlag()) {
			// enviar evento para agenda do cliente
		}
	}

	private void criarEvento(Evento evento) throws ValidationException {
		this.novoAndamento.getEventos().clear();
		novoAndamento.getEventos().add(evento);
	}

	private Evento getEvento(Pessoa pessoa) {
		if(!novoAndamento.getDefinePrazoFinal()) {
			return null;
		}
		Calendar inicio = Calendar.getInstance();
		inicio.setTime(novoAndamento.getDataFinalPrazo());
		inicio.set(Calendar.HOUR_OF_DAY, getHora(novoAndamento.getHoraInicio(), 6));
		inicio.set(Calendar.MINUTE, getMinuto(novoAndamento.getHoraInicio()));

		Calendar fim = Calendar.getInstance();
		fim.setTime(novoAndamento.getDataFinalPrazo());
		fim.set(Calendar.HOUR_OF_DAY, getHora(novoAndamento.getHoraFim(), 21));
		fim.set(Calendar.MINUTE, getMinuto(novoAndamento.getHoraFim()));

		StatusAgenda statusAgenda = novoAndamento.getStatusAgenda();
		EventoColorTypes colorType = EventoColorTypes.fromTextColor(statusAgenda.getStyleColor());
		String styleClassSelect = colorType == null ? null : colorType.getValue();

		Evento evento = new Evento(generateChaveEventoId(), novoAndamento.getTipoAndamento().getDescricao(), statusAgenda, inicio.getTime(), fim.getTime(), styleClassSelect, pessoa, this.empresa);
		evento.setNomeIniciais(pessoa == null ? null : pessoa.getNomeIniciais());
		evento.setObservacao(novoAndamento.getInformacaoCliente());
		evento.setAndamento(novoAndamento);
		evento.setProcesso(novoAndamento.getProcesso());
		evento.setHorarioIndefinido(novoAndamento.getHorarioIndefinido());
		HistoricoEvento historico = new HistoricoEvento(evento, getUser().getName());
		historico.setTipoOperacao(this.tipoOperacao);
		evento.addHistorico(historico);
		evento.setLocalizacao(novoAndamento.getLocalizacao());
		return evento;
	}

	private Date getDia(Date dataSub, Date dataFinalAndamento) {
		return dataSub != null ? dataSub : dataFinalAndamento;
	}

	private Integer getHora(String valorHora, int hora) {
		return !Strings.isNullOrEmpty(valorHora) ? Integer.parseInt(valorHora.substring(0, 2)) : hora;
	}

	private Integer getMinuto(String valorHora) {
		return !Strings.isNullOrEmpty(valorHora) ? Integer.parseInt(valorHora.substring(3, 5)) : 0;
	}

	private void enviarEmailAdvogados() throws ValidationException {
		if (novoAndamento.getEnviaResumoAdvogados()) {
			enviaEmailParaTodosAdvogados();
		} else if (novoAndamento.getEnviaResumoAdvogado()) {
			// enviar para selecionados
		}
	}

	private void enviarEmailResumoAndamento() throws ValidationException {
		if (novoAndamento.getEnviaResumoCliente() && !getProcesso().getPrivado()) {
			enviaEmailParaCliente();
		}
	}

	private void enviaEmailParaTodosAdvogados() {

	}

	private void enviaEmailParaCliente() throws ValidationException {
		Pessoa pessoa = pessoaSessionBean.findUser(getProcesso().getClienteProcesso().getLogin().getLogin(), getUser().getCodEmpresa());

		if (pessoa != null) {
			Email email = emailSessionBean.findBySigla(EmailIndicador.PROCESSO_RESUMO_PROCESSO.getValue(),
					getStateManager().getEmpresaSelecionada().getId());

			if(email == null || (email != null && !email.getAtivo())) {
				exibirMensagem(FacesMessage.SEVERITY_INFO, "Email para Resumo do Processo não cadastrado ou inativo...");
				return;
			}

			Map<String, String> parametersValues = Maps.newHashMap();
			for (EmailVinculoParametro parametro : email.getParametroEmails()) {
				String valor = ProcessoEmailBusiness.execute(parametro.getParametroEmail(), getProcesso(), getUser());
				parametersValues.put(parametro.getParametroEmail().getSigla(), valor);
			}

			String textoFormatado = EmailTemplateBusiness.criarEmailTemplate(email.getAssunto(), email.getDescricao(), parametersValues);
			EmailCaixaSaida emailSaida = new EmailCaixaSaida(getUser().getName(), email.getAssunto(), textoFormatado, getStateManager().getEmpresaSelecionada().getId());

			for (PessoaEmail pessoaEmail : pessoa.getPessoasEmails()) {
				emailSaida.getEmails().add(new EmailDestinatario(emailSaida, pessoaEmail.getEmail()));
			}

			if (!email.getNecessarioAprovacao()) {
				emailCaixaSaidaSessionBean.createSemAprovacao(emailSaida);
				return;
			}
			emailCaixaSaidaSessionBean.create(emailSaida);
		}
	}

	private void setAdvogadosAndamento() {
		List<PessoaFisica> advogadosDoAndamento = Lists.newArrayList();
		getAdvogadosDoAndamento(advogadosDoAndamento);

		selectedAdvogadosResumoAndamento.stream().filter(pessoaFisica -> !advogadosDoAndamento.contains(pessoaFisica))
				.forEach(pessoaFisica -> {
					AndamentoAdvogado andamentoAdvogado = new AndamentoAdvogado();
					andamentoAdvogado.setAndamento(novoAndamento);
					andamentoAdvogado.setAdvogadoPessoa(pessoaFisica);
					andamentoAdvogado.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
					getAndamentoAdvogados().add(andamentoAdvogado);
				});
	}

	public void adicionarCusta() {
		this.novaCusta = new Custa();
		goToCusta();
	}

	public void adicionarAndamento() {
        this.novoAndamento = new Andamento();
		this.novoAndamento.setDefinePrazoFinal(Boolean.TRUE);
		goToAndamento();
	}

	private StatusAgenda getStatuACumprir() {
		return statusAgendaSessionBean.findByDescricao(A_CUMPRIR, getStateManager().getEmpresaSelecionada().getId());
	}

	public void removeAndamento(Integer index) {
		int i = 0;
		Iterator<PrazoAndamento> it = this.novoAndamento.getPrazos().iterator();
		while (it.hasNext()) {
			PrazoAndamento prazoAndamento = it.next();
			if (i == index) {
				prazoAndamento.setDataExclusao(Calendar.getInstance().getTime());
				break;
			}
			i++;
		}
	}

	private void goToAndamento() {
		this.andamentoSelected = true;
		this.vinculadosSelected = false;
		this.custasSelected = false;
		this.propostasSelected = false;
		updateContext(PROCESSO_DETALHE_FORM);
	}

	private void getAdvogadosDoAndamento(List<PessoaFisica> advogadosDoAndamento) {
		advogadosDoAndamento.addAll(novoAndamento.getAndamentoAdvogados().stream()
				.map(AndamentoAdvogado::getAdvogadoPessoa).collect(Collectors.toList()));
	}

	public void vincularProcesso() {
		this.novoVinculo = new ProcessoVinculo();
		goToVinculado();
	}

	public void adicionarProposta(){
		this.novaProposta = new HistoricoProposta(this.getProcesso(), getStateManager().getEmpresaSelecionada().getId());
		goToPropostas();
	}

	public void salvarProcessosRelacionados(){
		try {
			ProcessoVinculo processoVinculo = new ProcessoVinculo(this.getProcesso(), this.processoVinculoSelecionado, getStateManager().getEmpresaSelecionada().getId());
			processoVinculo.validar();
			processoVinculoSessionBean.create(processoVinculo);
			returnToDetalhes();
			exibirMensagem(FacesMessage.SEVERITY_INFO, getMessage(MSG_SUCESSO_SALVAR));
			inicializouContexto = Boolean.FALSE;
		} catch (ValidationException e) {
			for (String message : e.getMessages()) {
				exibirMensagem(FacesMessage.SEVERITY_ERROR, message);
			}
		}
	}

	public void salvarProposta(){
		try {
			this.novaProposta.validar();

			if (novaProposta.getId() == null) {
				this.getProcesso().addProposta(this.novaProposta);
				processoSessionBean.clearSession();
				processoSessionBean.update(getProcesso());
			} else {
				historicoPropostaSessionBean.update(novaProposta);
			}
			returnToDetalhes();
			exibirMensagem(FacesMessage.SEVERITY_INFO, getMessage(MSG_SUCESSO_SALVAR));
			inicializouContexto = Boolean.FALSE;
		} catch (ValidationException e) {
			for (String message : e.getMessages()) {
				exibirMensagem(FacesMessage.SEVERITY_ERROR, message);
			}
		}
	}

	public void returnToDetalhes() {
		this.andamentoSelected = false;
		this.vinculadosSelected = false;
		this.custasSelected = false;
		this.propostasSelected = false;
		this.novoAndamento = null;
		initWizardAndamento();
		updateContext(PROCESSO_DETALHE_FORM);
	}

	public void dateCustasChanged(SelectEvent event) {
		novaCusta.setDataPagamento((Date) event.getObject());
	}

	public BigDecimal getValorTotalCustas() {
		BigDecimal total = BigDecimal.ZERO;
		if (getProcesso() != null) {
			for (Custa custa : getProcesso().getCustas()) {
				total = total.add(custa.getValorCusta());
			}
		}
		return total;
	}

	public void salvarCustaProcesso() {
		try {
			novaCusta.validar();
			antesDeSalvarCustaProcesso();
			setEmpresaDeUsuarioLogado(getProcesso());

			if (novaCusta.getId() == null) {
				getProcesso().getCustas().add(novaCusta);
				processoSessionBean.clearSession();
				processoSessionBean.update(getProcesso());
			} else {
				processoSessionBean.clearSession();
				custaSessionBean.update(novaCusta);
			}
			returnToDetalhes();
			exibirMensagem(FacesMessage.SEVERITY_INFO, getMessage(MSG_SUCESSO_SALVAR));
			inicializouContexto = Boolean.FALSE;
		} catch (CrudException e) {
			LOGGER.error(e);
		} catch (ValidationException e) {
			for (String message : e.getMessages()) {
				exibirMensagem(FacesMessage.SEVERITY_ERROR, message);
			}
		}
	}

	public String goToDetalhes() {
		if(processoSelecionado != null) {
			setProcesso(processoSessionBean.retrieve(processoSelecionado.getId()));
			returnToDetalhes();
		}
		return PAGES_PROCESSOS_PROCESSO_DETAIL;
	}

	public String goToDetalhesRelacionado(Processo processoSelected) {
		setProcesso(processoSelected);
		return PAGES_PROCESSOS_PROCESSO_DETAIL;
	}

	public void horaInicioChanged(ValueChangeEvent event) throws ParseException {
		String hora = (String) event.getNewValue();
		novoAndamento.setHoraInicio(hora);
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		if (!Strings.isNullOrEmpty(hora)) {
			Date dataFimComUmaHoraMais = DateUtils.somaHoras(sdf.parse(hora), 1);
			novoAndamento.setHoraFim(DateUtils.formataHoraBrasil(dataFimComUmaHoraMais));
			updateContext(PROCESSO_DETALHE_FORM);
		}
	}

	public void semHorarioChanged(ValueChangeEvent event) {
		boolean checked = (boolean) event.getNewValue();
		if (checked) {
			this.novoAndamento.setHoraInicio("");
			this.novoAndamento.setHoraFim("");
		}
	}

	public void semHorarioSubAndamentoChanged(ValueChangeEvent event) {
		boolean checked = (boolean) event.getNewValue();
		if (checked) {
			Map<String, String> params = getExternalContext().getRequestParameterMap();
			String indexSelected = params.get("indexSelected");
			this.novoAndamento.getPrazos().get(Integer.parseInt(indexSelected)).setHoraInicio("");
			this.novoAndamento.getPrazos().get(Integer.parseInt(indexSelected)).setHoraFim("");
		}
	}

	public void horaInicioSubAndamentoChanged(ValueChangeEvent event) throws ParseException {
		String hora = (String) event.getNewValue();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		if (!Strings.isNullOrEmpty(hora)) {
			Date dataFimComUmaHoraMais = DateUtils.somaHoras(sdf.parse(hora), 1);
			Map<String, String> params = getExternalContext().getRequestParameterMap();
			String indexSelected = params.get("indexSelected");
			this.novoAndamento.getPrazos().get(Integer.parseInt(indexSelected))
					.setHoraFim(DateUtils.formataHoraBrasil(dataFimComUmaHoraMais));
		}
	}

	public void dataInicioSubAndamentoChanged(ValueChangeEvent event) throws ParseException {
		Map<String, String> params = getExternalContext().getRequestParameterMap();
		String indexSelected = params.get("indexSelected");

		Date dataInicial = (Date) event.getNewValue();
		if (dataInicial != null) {
			this.novoAndamento.getPrazos().get(Integer.parseInt(indexSelected)).setDataFinalPrazo(dataInicial);
			return;
		}
		this.novoAndamento.getPrazos().get(Integer.parseInt(indexSelected)).setDataFinalPrazo(null);
	}

	public void dataFinalPrazoSelect(SelectEvent event) {
		this.novoAndamento.setDataFinalPrazoFatal((Date) event.getObject());
	}

	public void dataFinalPrazoChanged(AjaxBehaviorEvent event) {
		this.novoAndamento.setDataFinalPrazoFatal(novoAndamento.getDataFinalPrazo());
	}

	public void tipoAndamentoChanged(ValueChangeEvent event) {
		TipoAndamento tipoAndamentoSelecionado = ((TipoAndamento) event.getNewValue());
		if(tipoAndamentoSelecionado != null) {
			TipoAndamento tipoAndamento = tipoAndamentoSessionBean.retrieve(tipoAndamentoSelecionado.getId());
			this.novoAndamento.limparSubAndamentos();

			if (!tipoAndamento.getSubAndamentos().isEmpty()) {
				for (TipoAndamentoSubAndamento sub : tipoAndamento.getSubAndamentos()) {
					this.novoAndamento.addPrazo(new PrazoAndamento(sub.getSubAndamento()));
				}
			}
		}
	}

	public void stepSelect(String step){
		if(Strings.isNullOrEmpty(step)){
			return;
		}
		this.stepClick = step;
	}

	private void carregaAdvogadoPrincipal() {
		Set<PessoaFisica> principais = responsaveisPrincipais();
		if(!principais.isEmpty() && this.novoAndamento.getPessoaFisica() == null){
			this.novoAndamento.setPessoaFisica(principais.iterator().next());
		}
	}

	public void adicionarSubAndamento() {
		if(novoAndamento.getStatusAgenda() != null && novoAndamento.getStatusAgenda().getId().equals(getStatusAgendaCumprido().getId())) {
			return;
		}
		PrazoAndamento prazoAndamento = new PrazoAndamento();
		prazoAndamento.setAndamento(novoAndamento);
		prazoAndamento.setStatusAgenda(getStatuACumprir());
		this.novoAndamento.addPrazo(prazoAndamento);
	}

	public void removerListenerSelecionados(){
		if(this.prazosSelecionados.isEmpty()){
			showMessageInDialog(FacesMessage.SEVERITY_ERROR, "Mensagem", "N\u00e3o existe nenhum sub-andamento selecionado.");
			return;
		}

        for (PrazoAndamento prazosSelecionado : this.prazosSelecionados) {
           novoAndamento.removeSubAndamentoPorDescricao(prazosSelecionado.getSubAndamento().getDescricao());
        }
        this.prazosSelecionados.clear();
	}

	public List<PessoaFisica> getPessoasFisicas() {
		return pessoaFisicaSessionBean.retrieveAll(getStateManager().getEmpresaSelecionada().getId());
	}

	public Andamento getNovoAndamento() {
		return novoAndamento;
	}

	public void setNovoAndamento(Andamento novoAndamento) {
		this.novoAndamento = novoAndamento;
	}

	public List<TipoAndamento> getTiposAndamentos() {
		return tiposAndamentos;
	}

	public List<PessoaFisica> getSelectedAdvogadosResumoAndamento() {
		return selectedAdvogadosResumoAndamento;
	}

	public void setSelectedAdvogadosResumoAndamento(List<PessoaFisica> selectedAdvogadosResumoAndamento) {
		this.selectedAdvogadosResumoAndamento = selectedAdvogadosResumoAndamento;
	}

	public List<PessoaFisica> getAdvogadosResumoAndamento() {
		return advogadosResumoAndamento;
	}

	public void setAdvogadosResumoAndamento(List<PessoaFisica> advogadosResumoAndamento) {
		this.advogadosResumoAndamento = advogadosResumoAndamento;
	}

	public List<AndamentoAdvogado> getAndamentoAdvogados() {
		return andamentoAdvogados;
	}

	public ViewProcesso getProcessoSelecionado() {
		return processoSelecionado;
	}

	public void setProcessoSelecionado(ViewProcesso processoSelecionado) {
		this.processoSelecionado = processoSelecionado;
	}

	public Custa getNovaCusta() {
		return novaCusta;
	}

	public void setNovaCusta(Custa novaCusta) {
		this.novaCusta = novaCusta;
	}

	public List<TipoCusto> getTiposCustos() {
		return tipoCustoSessionBean.retrieveAll(getStateManager().getEmpresaSelecionada().getId());
	}

	public List<FormaPagamento> getFormasPagamentos() {
		return formaPagamentoSessionBean.retrieveAll(getStateManager().getEmpresaSelecionada().getId());
	}

	public boolean isAndamentoSelected() {
		return andamentoSelected;
	}

	public void setAndamentoSelected(boolean andamentoSelected) {
		this.andamentoSelected = andamentoSelected;
	}

	public boolean isVinculadosSelected() {
		return vinculadosSelected;
	}

	public void setVinculadosSelected(boolean vinculadosSelected) {
		this.vinculadosSelected = vinculadosSelected;
	}

	public boolean isCustasSelected() {
		return custasSelected;
	}

	public void setCustasSelected(boolean custasSelected) {
		this.custasSelected = custasSelected;
	}

	public boolean isPropostasSelected() {
		return propostasSelected;
	}

	public void setPropostasSelected(boolean propostasSelected) {
		this.propostasSelected = propostasSelected;
	}

	public Part getArquivoCustas() {
		return arquivoCustas;
	}

	public void setArquivoCustas(Part arquivoCustas) {
		this.arquivoCustas = arquivoCustas;
	}

	public List<PessoaFisica> getResponsaveis() {
		return pessoaFisicaSessionBean.findByAdvogadosAndColaboradores(getStateManager().getEmpresaSelecionada().getId());
	}

	public List<PessoaFisica> completeResponsavel(String query){
		List<PessoaFisica> results = Lists.newArrayList();
		Set<PessoaFisica> responsaveisPrincipais = responsaveisPrincipais();
		List<PessoaFisica> responsaveis = pessoaFisicaSessionBean.findByAdvogadosAndColaboradores(getStateManager().getEmpresaSelecionada().getId());

		for (PessoaFisica principal : responsaveisPrincipais) {
			for (PessoaFisica responsavel : responsaveis) {
				if(responsavel.getId().equals(principal.getId())){
					responsavel.setRatingPrincipal(principal.getRatingPrincipal());
				}
			}
		}

		if(Strings.isNullOrEmpty(query)){
			return responsaveis;
		}

		for (PessoaFisica responsavel : responsaveis) {
			if(responsavel.getPessoa().getDescricao().toLowerCase().startsWith(query)){
				results.add(responsavel);
			}
		}
		return results;

	}

	private Set<PessoaFisica> responsaveisPrincipais(){
		Set<PessoaFisica> principais = Sets.newHashSet();
		for (Parte parte : getProcesso().getPartes()) {
			Parte parteProcesso = parteSessionBean.retrieve(parte.getId());
			adicionaAdvogadosPrincipais(principais, parteProcesso);
		}
		return principais;
	}

	private void adicionaAdvogadosPrincipais(Set<PessoaFisica> principais, Parte parte) {
		for (ParteAdvogado parteAdvogado : parte.getAdvogados()) {
			if(parteAdvogado.getRatingPrincipal() != null) {
				PessoaFisica pessoaFisica = pessoaFisicaSessionBean.findByPessoa(parteAdvogado.getPessoa().getId(), getStateManager().getEmpresaSelecionada().getId());
				pessoaFisica.setRatingPrincipal(parteAdvogado.getRatingPrincipal());
				principais.add(pessoaFisica);
			}
		}
	}

	public int getRowAnexoCustaNumber() {
		return rowAnexoCustaNumber;
	}

	public void setRowAnexoCustaNumber(int rowAnexoCustaNumber) {
		this.rowAnexoCustaNumber = rowAnexoCustaNumber;
	}


	public boolean isHasAccessAbaPartes(){
		return possuiAcesso(PROCESSOS_VISUALIZACAO_PARTES);
	}

	public boolean isHasAccessAbaDadosProcesso(){
		return possuiAcesso(PROCESSOS_VISUALIZACAO_DADOS_PROCESSO);
	}

	public boolean isHasAccessAbaResumo(){
		return possuiAcesso(PROCESSOS_VISUALIZACAO_RESUMO);
	}

	public boolean isHasAccessAbaValores(){
		return possuiAcesso(PROCESSOS_VISUALIZACAO_VALORES);
	}

	public boolean isHasAccessAbaHonorarios(){
		return possuiAcesso(PROCESSOS_CADASTRO_HONORARIOS);
	}

	public boolean isHasAccessAndamentos(){
		return possuiAcesso(PROCESSOS_ANDAMENTOS);
	}

	public boolean isHasAccessVinculados(){
		return possuiAcesso(PROCESSOS_VINCULADOS);
	}

	public boolean isHasAccessPropostas(){
		return possuiAcesso(PROCESSOS_PROPOSTAS);
	}

	public boolean isHasAccessCustas(){
		return possuiAcesso(PROCESSOS_CUSTAS);
	}

	public boolean isHasViewPrazoFatal(){
		return possuiAcesso(PROCESSOS_VISUALIZACAO_PRAZO_FATAL);
	}

	protected boolean isPodeAlterarAndamento(){
		Direito direito = direitoSessionBean.retrieve(ALTERAR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), PROCESSOS_ANDAMENTOS, direito, getUser().getCodEmpresa());
	}

	protected boolean isPodeExcluirAndamento(){
		Direito direito = direitoSessionBean.retrieve(EXCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), PROCESSOS_ANDAMENTOS, direito, getUser().getCodEmpresa());
	}

	protected boolean isPodeIncluirAndamento(){
		Direito direito = direitoSessionBean.retrieve(INCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), PROCESSOS_ANDAMENTOS, direito, getUser().getCodEmpresa());
	}

	protected boolean isPodeIncluirVinculado(){
		Direito direito = direitoSessionBean.retrieve(INCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), PROCESSOS_VINCULADOS, direito, getUser().getCodEmpresa());
	}

	protected boolean isPodeAlterarVinculado(){
		Direito direito = direitoSessionBean.retrieve(ALTERAR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), PROCESSOS_VINCULADOS, direito, getUser().getCodEmpresa());
	}

	protected boolean isPodeExcluirVinculado(){
		Direito direito = direitoSessionBean.retrieve(EXCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), PROCESSOS_VINCULADOS, direito, getUser().getCodEmpresa());
	}

	protected boolean isPodeAlterarCusta(){
		Direito direito = direitoSessionBean.retrieve(ALTERAR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), PROCESSOS_CUSTAS, direito, getUser().getCodEmpresa());
	}

	protected boolean isPodeExcluirCusta(){
		Direito direito = direitoSessionBean.retrieve(EXCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), PROCESSOS_CUSTAS, direito, getUser().getCodEmpresa());
	}

	protected boolean isPodeIncluirCusta(){
		Direito direito = direitoSessionBean.retrieve(INCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), PROCESSOS_CUSTAS, direito, getUser().getCodEmpresa());
	}

	protected boolean isPodeAlterarProposta(){
		Direito direito = direitoSessionBean.retrieve(ALTERAR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), PROCESSOS_PROPOSTAS, direito, getUser().getCodEmpresa());
	}

	protected boolean isPodeExcluirProposta(){
		Direito direito = direitoSessionBean.retrieve(EXCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), PROCESSOS_PROPOSTAS, direito, getUser().getCodEmpresa());
	}

	protected boolean isPodeIncluirProposta(){
		Direito direito = direitoSessionBean.retrieve(INCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), PROCESSOS_PROPOSTAS, direito, getUser().getCodEmpresa());
	}

	public ProcessoVinculo getNovoVinculo() {
		return novoVinculo;
	}

	public void setNovoVinculo(ProcessoVinculo novoVinculo) {
		this.novoVinculo = novoVinculo;
	}

	public Processo getProcessoVinculoSelecionado() {
		return processoVinculoSelecionado;
	}

	public void setProcessoVinculoSelecionado(Processo processoVinculoSelecionado) {
		this.processoVinculoSelecionado = processoVinculoSelecionado;
	}

	public List<PrazoAndamento> getPrazosSelecionados() {
		return prazosSelecionados;
	}

	public void setPrazosSelecionados(List<PrazoAndamento> prazosSelecionados) {
		this.prazosSelecionados = prazosSelecionados;
	}

	public String getClienteDesde(Pessoa pessoa){
		if(pessoa != null){
			for (HistoricoPessoaCliente historico : pessoa.getHistoricoClienteList()) {
				if(historico.getCliente()){
					return DateUtils.formataDataBrasil(historico.getDataAlteracao());
				}
			}
		}
		return NAO_HA_CADASTRO;
	}

	public String getTelefoneCliente(Pessoa pessoa){
		for (HistoricoPessoaCliente historico : pessoa.getHistoricoClienteList()) {
			if(historico.getCliente()){
				return historico.getTelefones(pessoa);
			}
		}
		return NAO_HA_CADASTRO;
	}

	public String getEmailCliente(Pessoa pessoa){
		for (HistoricoPessoaCliente historico : pessoa.getHistoricoClienteList()) {
			if(historico.getCliente()){
				return historico.getEmails(pessoa);
			}
		}
		return NAO_HA_CADASTRO;
	}

	public List<ProcessoVinculo> getProcessoVinculos() {
		return processoVinculos;
	}

	public DetalheProcessoStepsTypes getStepsTypesSelecionado() {
		return stepsTypesSelecionado;
	}

	public String getStepClick() {
		return stepClick;
	}

	public HistoricoProposta getNovaProposta() {
		return novaProposta;
	}

	public void setNovaProposta(HistoricoProposta novaProposta) {
		this.novaProposta = novaProposta;
	}

	public String getCadastradoPor(){
		List<Pessoa> pessoas = pessoaSessionBean.findByCpf(getProcesso().getUsuarioCadastro());
		if(pessoas.isEmpty()){
			return "";
		}
		Optional<Pessoa> pessoa = pessoas.stream().findFirst();
		String dataHora = DateUtils.formataDataHoraBrasil(getProcesso().getDataCadastro());
		return pessoa.get().getDescricao().concat(" - ").concat(dataHora);
	}

}
