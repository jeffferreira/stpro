package br.com.juridico.controller;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.primefaces.component.tabview.TabView;
import org.primefaces.event.TabChangeEvent;

import br.com.juridico.dto.PerfilAcessoDto;
import br.com.juridico.dto.PerfilSubAcessoDto;
import br.com.juridico.entidades.Acesso;
import br.com.juridico.entidades.Direito;
import br.com.juridico.entidades.DireitoPerfil;
import br.com.juridico.entidades.Perfil;
import br.com.juridico.entidades.PerfilAcesso;
import br.com.juridico.entidades.PessoaConfiguracao;
import br.com.juridico.enums.DireitosIndicador;
import br.com.juridico.enums.MenuIndicador;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.impl.AcessoSessionBean;
import br.com.juridico.impl.DireitoPerfilSessionBean;
import br.com.juridico.impl.DireitoSessionBean;
import br.com.juridico.impl.PerfilAcessoSessionBean;
import br.com.juridico.impl.PerfilSessionBean;
import br.com.juridico.impl.PessoaConfiguracaoSessionBean;

import com.google.common.collect.Lists;

/**
 *
 * @author Jefferson
 *
 */
@ManagedBean
@ViewScoped
public class AcessoController extends AbstractController {

	private static final Logger LOGGER = Logger.getLogger(AcessoController.class);

	private static final String CHAVE_ACESSO = "ACESSOS_ACESSOS_TEMPLATE";

	@Inject
	private PerfilSessionBean perfilSessionBean;
	@Inject
	private PerfilAcessoSessionBean perfilAcessoSessionBean;
	@Inject
	private AcessoSessionBean acessoSessionBean;
	@Inject
	private DireitoPerfilSessionBean direitoPerfilSessionBean;
	@Inject
	private DireitoSessionBean direitoSessionBean;
	@Inject
	private PessoaConfiguracaoSessionBean pessoaConfiguracaoSessionBean;

	private PerfilAcesso perfilAcesso;
	private Perfil perfilSelecionado;

	private List<PerfilAcessoDto> perfilCadastroAcessos;
	private List<PerfilAcessoDto> perfilPessoasAcessos;
	private List<PerfilAcessoDto> perfilAdvogadosAcessos;
	private List<PerfilAcessoDto> perfilProcessosAcessos;
	private List<PerfilAcessoDto> perfilContratosAcessos;
	private List<PerfilAcessoDto> perfilControladoriaAcessos;
	private List<PerfilAcessoDto> perfilDashboardsAcessos;
	private List<PerfilAcessoDto> perfilAgendasAcessos;
	private List<PerfilAcessoDto> perfilAcessosAcessos;
	private List<PerfilAcessoDto> perfilRelatoriosAcessos;
	private List<PerfilAcessoDto> perfilHomeAcessos;
	private List<PerfilAcessoDto> perfilFinanceiroAcessos;

	private int activeIndex;

	@PostConstruct
	public void postConstruct(){
		initialize();
	}

	public void initialize(){
		perfilSelecionado = getPerfis().get(0);
		carregaListasAcessos();
	}

	private void carregaListaCadastroAcessos(){
		this.perfilCadastroAcessos = Lists.newArrayList();
		this.acessoSessionBean.clearSession();
		List<Acesso> acessos = acessoSessionBean.findByMenu(MenuIndicador.CADASTROS.getValue());
		this.perfilAcesso = new PerfilAcesso();
		this.perfilAcesso.setPerfil(perfilSelecionado);

		for (Acesso acesso : acessos) {
			PerfilAcesso pa = getPerfilAcesso(acesso);
			PerfilAcessoDto dto = new PerfilAcessoDto(acesso, pa);
			this.perfilCadastroAcessos.add(dto);
		}
	}

    private void carregaListaContratosAcessos(){
        this.perfilContratosAcessos = Lists.newArrayList();
        this.acessoSessionBean.clearSession();
        List<Acesso> acessos = acessoSessionBean.findByMenu(MenuIndicador.CONTRATOS.getValue());
        this.perfilAcesso = new PerfilAcesso();
        this.perfilAcesso.setPerfil(perfilSelecionado);
        for (Acesso acesso : acessos) {
            PerfilAcesso pa = getPerfilAcesso(acesso);
            PerfilAcessoDto dto = new PerfilAcessoDto(acesso, pa);
            this.perfilContratosAcessos.add(dto);
        }
    }

	private void carregaListaControladoriaAcessos(){
		this.perfilControladoriaAcessos = Lists.newArrayList();
		this.acessoSessionBean.clearSession();
		List<Acesso> acessos = acessoSessionBean.findByMenu(MenuIndicador.CONTROLADORIA.getValue());
		this.perfilAcesso = new PerfilAcesso();
		this.perfilAcesso.setPerfil(perfilSelecionado);
		for (Acesso acesso : acessos) {
			PerfilAcesso pa = getPerfilAcesso(acesso);
			PerfilAcessoDto dto = new PerfilAcessoDto(acesso, pa);
			this.perfilControladoriaAcessos.add(dto);
		}
	}

	private void carregaListaFinanceiroAcessos(){
		this.perfilFinanceiroAcessos = Lists.newArrayList();
		this.acessoSessionBean.clearSession();
		List<Acesso> acessos = acessoSessionBean.findByMenu(MenuIndicador.FINANCEIRO.getValue());
		this.perfilAcesso = new PerfilAcesso();
		this.perfilAcesso.setPerfil(perfilSelecionado);
		for (Acesso acesso : acessos) {
			PerfilAcesso pa = getPerfilAcesso(acesso);
			PerfilAcessoDto dto = new PerfilAcessoDto(acesso, pa);
			this.perfilFinanceiroAcessos.add(dto);
		}
	}

	private void carregaListaPessoasAcessos() {
		this.perfilPessoasAcessos = Lists.newArrayList();
		this.acessoSessionBean.clearSession();
		List<Acesso> acessos = acessoSessionBean.findByMenu(MenuIndicador.PESSOAS.getValue());
		this.perfilAcesso = new PerfilAcesso();
		this.perfilAcesso.setPerfil(perfilSelecionado);

		for (Acesso acesso : acessos) {
			PerfilAcesso pa = getPerfilAcesso(acesso);
			PerfilAcessoDto dto = new PerfilAcessoDto(acesso, pa);
			carregaAcessosFilhos(acesso, dto);
			this.perfilPessoasAcessos.add(dto);
		}
	}

	private void carregaListaAdvogadosAcessos(){
		this.perfilAdvogadosAcessos = Lists.newArrayList();
		this.acessoSessionBean.clearSession();
		List<Acesso> acessos = acessoSessionBean.findByMenu(MenuIndicador.ADVOGADOS.getValue());
		this.perfilAcesso = new PerfilAcesso();
		this.perfilAcesso.setPerfil(perfilSelecionado);
		for (Acesso acesso : acessos) {
			PerfilAcesso pa = getPerfilAcesso(acesso);
			PerfilAcessoDto dto = new PerfilAcessoDto(acesso, pa);
			carregaAcessosFilhos(acesso, dto);
			this.perfilAdvogadosAcessos.add(dto);
		}
	}

	private void carregaListaProcessosAcessos(){
		this.perfilProcessosAcessos = Lists.newArrayList();
		this.acessoSessionBean.clearSession();
		List<Acesso> acessos = acessoSessionBean.findByMenu(MenuIndicador.PROCESSOS.getValue());
		this.perfilAcesso = new PerfilAcesso();
		this.perfilAcesso.setPerfil(perfilSelecionado);
		for (Acesso acesso : acessos) {
			PerfilAcesso pa = getPerfilAcesso(acesso);
			PerfilAcessoDto dto = new PerfilAcessoDto(acesso, pa);
			this.perfilProcessosAcessos.add(dto);
		}
	}

	private void carregaListaDashboardsAcessos(){
		this.perfilDashboardsAcessos = Lists.newArrayList();
		this.acessoSessionBean.clearSession();
		List<Acesso> acessos = acessoSessionBean.findByMenu(MenuIndicador.DASHBOARDS.getValue());
		this.perfilAcesso = new PerfilAcesso();
		this.perfilAcesso.setPerfil(perfilSelecionado);
		for (Acesso acesso : acessos) {
			PerfilAcesso pa = getPerfilAcesso(acesso);
			PerfilAcessoDto dto = new PerfilAcessoDto(acesso, pa);
			this.perfilDashboardsAcessos.add(dto);
		}
	}

	private void carregaListaAgendasAcessos(){
		this.perfilAgendasAcessos = Lists.newArrayList();
		this.acessoSessionBean.clearSession();
		List<Acesso> acessos = acessoSessionBean.findByMenu(MenuIndicador.AGENDA.getValue());
		this.perfilAcesso = new PerfilAcesso();
		this.perfilAcesso.setPerfil(perfilSelecionado);
		for (Acesso acesso : acessos) {
			PerfilAcesso pa = getPerfilAcesso(acesso);
			PerfilAcessoDto dto = new PerfilAcessoDto(acesso, pa);
			this.perfilAgendasAcessos.add(dto);
		}
	}

	private void carregaListaAcessosAcessos(){
		this.perfilAcessosAcessos = Lists.newArrayList();
		this.acessoSessionBean.clearSession();
		List<Acesso> acessos = acessoSessionBean.findByMenu(MenuIndicador.ACESSOS.getValue());
		this.perfilAcesso = new PerfilAcesso();
		this.perfilAcesso.setPerfil(perfilSelecionado);
		for (Acesso acesso : acessos) {
			PerfilAcesso pa = getPerfilAcesso(acesso);
			PerfilAcessoDto dto = new PerfilAcessoDto(acesso, pa);
			this.perfilAcessosAcessos.add(dto);
		}
	}	
	
	private void carregaListaRelatoriosAcessos(){
		this.perfilRelatoriosAcessos = Lists.newArrayList();
		this.acessoSessionBean.clearSession();
		List<Acesso> acessos = acessoSessionBean.findByMenu(MenuIndicador.RELATORIOS.getValue());
		this.perfilAcesso = new PerfilAcesso();
		this.perfilAcesso.setPerfil(perfilSelecionado);
		for (Acesso acesso : acessos) {
			PerfilAcesso pa = getPerfilAcesso(acesso);
			PerfilAcessoDto dto = new PerfilAcessoDto(acesso, pa);
			this.perfilRelatoriosAcessos.add(dto);
		}
	}

	private void carregaListaHomeAcessos(){
		this.perfilHomeAcessos = Lists.newArrayList();
		this.acessoSessionBean.clearSession();
		List<Acesso> acessos = acessoSessionBean.findByMenu(MenuIndicador.HOME.getValue());
		this.perfilAcesso = new PerfilAcesso();
		this.perfilAcesso.setPerfil(perfilSelecionado);
		for (Acesso acesso : acessos) {
			PerfilAcesso pa = getPerfilAcesso(acesso);
			PerfilAcessoDto dto = new PerfilAcessoDto(acesso, pa);
			this.perfilHomeAcessos.add(dto);
		}
	}
	
	private void carregaAcessosFilhos(Acesso acesso, PerfilAcessoDto dto) {
		List<Acesso> acessosFilhos = acessoSessionBean.findByAcessoFilho(acesso.getId());

		for (Acesso acessoFilho : acessosFilhos) {
			PerfilAcesso perfilFilho = getPerfilAcesso(acessoFilho);

			if(perfilFilho == null) {
				perfilFilho = getNovoPerfilAcesso(acessoFilho, perfilFilho);
			}
			direitoPerfilSessionBean.clearSession();
			List<DireitoPerfil> direitoPerfis = direitoPerfilSessionBean.findByPerfilAcesso(perfilFilho.getId(), getStateManager().getEmpresaSelecionada().getId());
			dto.addAcessosFilhos(perfilFilho, direitoPerfis);

		}
	}

	private PerfilAcesso getNovoPerfilAcesso(Acesso acessoFilho, PerfilAcesso perfilFilho) {
		try {
            perfilFilho = createPerfilAcesso(acessoFilho);
        } catch (ValidationException e) {
            LOGGER.error(e, e.getCause());
        }
		return perfilFilho;
	}

	public void perfilChanged(ValueChangeEvent event){
		perfilSelecionado = (Perfil) event.getNewValue();
		carregaListasAcessos();
	}

	private void carregaListasAcessos() {
		carregaListaCadastroAcessos();
		carregaListaContratosAcessos();
		carregaListaControladoriaAcessos();
		carregaListaFinanceiroAcessos();
		carregaListaPessoasAcessos();
		carregaListaAdvogadosAcessos();
		carregaListaProcessosAcessos();
		carregaListaDashboardsAcessos();
		carregaListaAgendasAcessos();
		carregaListaAcessosAcessos();
		carregaListaRelatoriosAcessos();
		carregaListaHomeAcessos();		
	}

	public void adicionarTodos(String menu){
		MenuIndicador indicador = MenuIndicador.findByDescricao(menu);
		if(indicador != null) {
			switch (indicador) {
				case CADASTROS:
					carregaListaCadastroAcessos();
					alterarTodosAcessos(Boolean.TRUE, perfilCadastroAcessos);
					carregaListaCadastroAcessos();
					break;
				case PESSOAS:
					carregaListaPessoasAcessos();
					alterarTodosAcessos(Boolean.TRUE, perfilPessoasAcessos);
					carregaListaPessoasAcessos();
					break;
				case ADVOGADOS:
					carregaListaAdvogadosAcessos();
					alterarTodosAcessos(Boolean.TRUE, perfilAdvogadosAcessos);
					carregaListaAdvogadosAcessos();
					break;
				case PROCESSOS:
					carregaListaProcessosAcessos();
					alterarTodosAcessos(Boolean.TRUE, perfilProcessosAcessos);
					carregaListaProcessosAcessos();
					break;
                case CONTRATOS:
                    carregaListaContratosAcessos();
                    alterarTodosAcessos(Boolean.TRUE, perfilContratosAcessos);
                    carregaListaContratosAcessos();
                    break;
                case CONTROLADORIA:
					carregaListaControladoriaAcessos();
					alterarTodosAcessos(Boolean.TRUE, perfilControladoriaAcessos);
					carregaListaControladoriaAcessos();
					break;
				case FINANCEIRO:
					carregaListaFinanceiroAcessos();
					alterarTodosAcessos(Boolean.TRUE, perfilFinanceiroAcessos);
					carregaListaFinanceiroAcessos();
					break;
				case ACESSOS:
					carregaListaAcessosAcessos();
					alterarTodosAcessos(Boolean.TRUE, perfilAcessosAcessos);
					carregaListaAcessosAcessos();
					break;
				case DASHBOARDS:
					carregaListaDashboardsAcessos();
					alterarTodosAcessos(Boolean.TRUE, perfilDashboardsAcessos);
					carregaListaDashboardsAcessos();
					break;
				case AGENDA:
					carregaListaAgendasAcessos();
					alterarTodosAcessos(Boolean.TRUE, perfilAgendasAcessos);
					carregaListaAgendasAcessos();
					break;
				case RELATORIOS:
					carregaListaRelatoriosAcessos();
					alterarTodosAcessos(Boolean.TRUE, perfilRelatoriosAcessos);
					carregaListaRelatoriosAcessos();
					break;
				case HOME:
					carregaListaHomeAcessos();
					alterarTodosAcessos(Boolean.TRUE, perfilHomeAcessos);
					carregaListaHomeAcessos();
					break;
			}
		}
	}

	public void removerTodos(String menu){
		MenuIndicador indicador = MenuIndicador.findByDescricao(menu);
		if(indicador != null) {
			switch (indicador) {
				case CADASTROS:
					carregaListaCadastroAcessos();
					alterarTodosAcessos(Boolean.FALSE, perfilCadastroAcessos);
					carregaListaCadastroAcessos();
					break;
				case PESSOAS:
					carregaListaPessoasAcessos();
					alterarTodosAcessos(Boolean.FALSE, perfilPessoasAcessos);
					carregaListaPessoasAcessos();
					break;
				case ADVOGADOS:
					carregaListaAdvogadosAcessos();
					alterarTodosAcessos(Boolean.FALSE, perfilAdvogadosAcessos);
					carregaListaAdvogadosAcessos();
					break;
				case PROCESSOS:
					carregaListaProcessosAcessos();
					alterarTodosAcessos(Boolean.FALSE, perfilProcessosAcessos);
					carregaListaProcessosAcessos();
					break;
                case CONTRATOS:
                    carregaListaContratosAcessos();
                    alterarTodosAcessos(Boolean.FALSE, perfilContratosAcessos);
                    carregaListaContratosAcessos();
                    break;
				case CONTROLADORIA:
					carregaListaControladoriaAcessos();
					alterarTodosAcessos(Boolean.FALSE, perfilControladoriaAcessos);
					carregaListaControladoriaAcessos();
					break;
				case FINANCEIRO:
					carregaListaFinanceiroAcessos();
					alterarTodosAcessos(Boolean.FALSE, perfilFinanceiroAcessos);
					carregaListaFinanceiroAcessos();
					break;
				case ACESSOS:
					carregaListaAcessosAcessos();
					alterarTodosAcessos(Boolean.FALSE, perfilAcessosAcessos);
					carregaListaAcessosAcessos();
					break;
				case DASHBOARDS:
					carregaListaDashboardsAcessos();
					alterarTodosAcessos(Boolean.FALSE, perfilDashboardsAcessos);
					carregaListaDashboardsAcessos();
					break;
				case AGENDA:
					carregaListaAgendasAcessos();
					alterarTodosAcessos(Boolean.FALSE, perfilAgendasAcessos);
					carregaListaAgendasAcessos();
					break;
				case RELATORIOS:
					carregaListaRelatoriosAcessos();
					alterarTodosAcessos(Boolean.FALSE, perfilRelatoriosAcessos);
					carregaListaRelatoriosAcessos();
					break;
				case HOME:
					carregaListaHomeAcessos();
					alterarTodosAcessos(Boolean.FALSE, perfilHomeAcessos);
					carregaListaHomeAcessos();
					break;
			}
		}
	}
	
	private void alterarTodosAcessos(boolean permitirAcesso, List<PerfilAcessoDto> perfilAcessosDtos) {
		try {
			for (PerfilAcessoDto perfilAcessoDto : perfilAcessosDtos) {
				List<DireitoPerfil> direitos = consultaDireitos(perfilAcessoDto);

				if (permitirAcesso) {
					removeDireitos(direitos);
					perfilAcessoSessionBean.delete(perfilAcessoDto.getPerfilAcesso());

					PerfilAcesso novoPerfilAcesso = new PerfilAcesso(perfilSelecionado, perfilAcessoDto.getAcesso(), getStateManager().getEmpresaSelecionada().getId());
					perfilAcessoSessionBean.create(novoPerfilAcesso);
					perfilAcessoDto.setPerfilAcesso(novoPerfilAcesso);
					adicionaTodosDireitos(novoPerfilAcesso);
				} else {
					removeDireitos(direitos);
					perfilAcessoSessionBean.delete(perfilAcessoDto.getPerfilAcesso());
				}
			}

		} catch (ValidationException e){
			LOGGER.error(e,e.getCause());
		}
	}
	

	private List<DireitoPerfil> consultaDireitos(PerfilAcessoDto perfilCadastroAcesso) throws ValidationException {
		List<DireitoPerfil> direitos;

		if(perfilCadastroAcesso.getPerfilAcesso() == null) {
            PerfilAcesso perfilAcesso = new PerfilAcesso(perfilSelecionado, perfilCadastroAcesso.getAcesso(), getStateManager().getEmpresaSelecionada().getId());
            perfilAcessoSessionBean.create(perfilAcesso);
            perfilCadastroAcesso.setPerfilAcesso(perfilAcesso);
            direitos = direitoPerfilSessionBean.findByPerfilAcesso(perfilAcesso.getId(), getStateManager().getEmpresaSelecionada().getId());
        } else {
            direitos = direitoPerfilSessionBean.findByPerfilAcesso(perfilCadastroAcesso.getPerfilAcesso().getId(), getStateManager().getEmpresaSelecionada().getId());
        }
		return direitos;
	}

	private void adicionaTodosDireitos(PerfilAcesso perfilAcesso) {
		List<Direito> direitos = direitoSessionBean.findAll();
		try {
			for (Direito direito : direitos) {
				direitoPerfilSessionBean.create(new DireitoPerfil(direito, perfilAcesso, getStateManager().getEmpresaSelecionada().getId()));
			}
		} catch (ValidationException e) {
			e.printStackTrace();
		}
	}

	public void atualizaOperacaoExcluir(Acesso acesso, Integer posicaoLista){
		PerfilAcesso pa = getPerfilAcesso(acesso);
		DireitoPerfil dp = getDireitoPerfil(pa, EXCLUIR);
		salvarDiretoPerfil(pa, dp, EXCLUIR, posicaoLista);
	}

	public void atualizaOperacaoConsultar(Acesso acesso, Integer posicaoLista){
		PerfilAcesso pa = getPerfilAcesso(acesso);
		DireitoPerfil dp = getDireitoPerfil(pa, CONSULTAR);
		salvarDiretoPerfil(pa, dp, CONSULTAR, posicaoLista);
	}

	public void atualizaOperacaoInserir(Acesso acesso, Integer posicaoLista){
		PerfilAcesso pa = getPerfilAcesso(acesso);
		DireitoPerfil dp = getDireitoPerfil(pa, INCLUIR);
		salvarDiretoPerfil(pa, dp, INCLUIR, posicaoLista);
	}

	public void atualizaOperacaoEditar(Acesso acesso, Integer posicaoLista){
		PerfilAcesso pa = getPerfilAcesso(acesso);
		DireitoPerfil dp = getDireitoPerfil(pa, ALTERAR);
		salvarDiretoPerfil(pa, dp, ALTERAR, posicaoLista);
	}

	public void atualizaOperacaoSubAcessos(PerfilAcessoDto perfilAcessoDto, String operacao, String funcionalidade) {
		boolean consultar = Boolean.FALSE;
		boolean excluir = Boolean.FALSE;
		boolean incluir = Boolean.FALSE;
		DireitosIndicador direitoSelecionado = DireitosIndicador.fromTextSigla(operacao);

		switch (direitoSelecionado){
			case CON:
				consultar = Boolean.TRUE;
				break;
			case EXC:
				excluir = Boolean.TRUE;
				break;
			case INC:
				incluir = Boolean.TRUE;
				break;
		}

		PerfilSubAcessoDto subAcesso = perfilAcessoDto.getSubAcesso(funcionalidade);
		Acesso acesso = acessoSessionBean.findByFuncionalidade(subAcesso.getSubAcesso());
		PerfilAcesso perfilAcesso = perfilAcessoSessionBean.findByPerfilAndAcesso(this.perfilAcesso.getPerfil().getId(), acesso.getId(), getStateManager().getEmpresaSelecionada().getId());

		subAcesso.setExclui(excluir);
		createSubAcessos(direitoSelecionado, perfilAcesso, EXCLUIR);
		subAcesso.setVisualiza(consultar);
		createSubAcessos(direitoSelecionado, perfilAcesso, CONSULTAR);
		subAcesso.setInsere(incluir);
		createSubAcessos(direitoSelecionado, perfilAcesso,INCLUIR);
	}

	private void createSubAcessos(DireitosIndicador direitoSelecionado, PerfilAcesso perfilAcesso, Long operacao) {
		try {
			List<DireitoPerfil> direitos = this.direitoPerfilSessionBean.findByPerfilAcesso(perfilAcesso.getId(), getStateManager().getEmpresaSelecionada().getId());
			if(direitos.isEmpty()) {
				if(direitoSelecionado.getCodigo().equals(operacao)) {
					createDireitoPerfil(perfilAcesso, operacao);
				}
				return;
			}

			for (DireitoPerfil d : direitos) {
				if (d.containsPerfilDireito(direitoSelecionado.getSigla(), perfilAcesso.getId(), operacao)) {
					direitoPerfilSessionBean.deleteFromId(d.getId());
					return;
				}
			}

			if(direitoSelecionado.getCodigo().equals(operacao)) {
				createDireitoPerfil(perfilAcesso, operacao);
			}

		} catch (ValidationException e) {
			LOGGER.error(e.getMessage(), e);
		}
	}

	private void createDireitoPerfil(PerfilAcesso perfilAcesso, Long operacao) throws ValidationException {
		Direito direito = direitoSessionBean.retrieve(operacao);
		DireitoPerfil direitoPerfil = new DireitoPerfil(direito, perfilAcesso, getStateManager().getEmpresaSelecionada().getId());
		this.direitoPerfilSessionBean.create(direitoPerfil);
	}


	public void atualizaAcessoMenu(Acesso acesso, Integer posicaoLista){
		PerfilAcesso pa = getPerfilAcesso(acesso);

		try {
			if(pa == null) {
				PerfilAcesso perfilAcesso = new PerfilAcesso(this.perfilAcesso.getPerfil(), acesso, getStateManager().getEmpresaSelecionada().getId());
				this.perfilAcessoSessionBean.create(perfilAcesso);
				adicionaPerfilNaLista(posicaoLista);
			} else {
				removeDireitos(pa.getDireitoPerfis());
				List<DireitoPerfil> direitos = direitoPerfilSessionBean.findByPerfilAcesso(pa.getId(), getStateManager().getEmpresaSelecionada().getId());
				removePerfilDaLista(direitos, posicaoLista);
				this.perfilAcessoSessionBean.delete(pa);
			}
		} catch (ValidationException e) {
			for (String message : e.getMessages()) {
				exibirMensagem(FacesMessage.SEVERITY_ERROR, message);
			}
		}
		carregaListasAcessos();
	}

	private void adicionaPerfilNaLista(Integer posicaoLista) {
		switch (this.activeIndex){
			case 0:
				this.perfilHomeAcessos.get(posicaoLista).setPerfilAcesso(perfilAcesso);
				break;		
			case 1:
				this.perfilAgendasAcessos.get(posicaoLista).setPerfilAcesso(perfilAcesso);
				break;
			case 2:
				this.perfilAdvogadosAcessos.get(posicaoLista).setPerfilAcesso(perfilAcesso);
				break;
			case 3:
				this.perfilCadastroAcessos.get(posicaoLista).setPerfilAcesso(perfilAcesso);
				break;
            case 4:
                this.perfilContratosAcessos.get(posicaoLista).setPerfilAcesso(perfilAcesso);
                break;
			case 5:
				this.perfilControladoriaAcessos.get(posicaoLista).setPerfilAcesso(perfilAcesso);
				break;
			case 6:
				this.perfilFinanceiroAcessos.get(posicaoLista).setPerfilAcesso(perfilAcesso);
				break;
			case 7:
				this.perfilPessoasAcessos.get(posicaoLista).setPerfilAcesso(perfilAcesso);
				break;
			case 8:
				this.perfilProcessosAcessos.get(posicaoLista).setPerfilAcesso(perfilAcesso);
				break;
			case 9:
				this.perfilRelatoriosAcessos.get(posicaoLista).setPerfilAcesso(perfilAcesso);
				break;
			case 10:
				this.perfilAcessosAcessos.get(posicaoLista).setPerfilAcesso(perfilAcesso);
				break;
			case 11:
				this.perfilDashboardsAcessos.get(posicaoLista).setPerfilAcesso(perfilAcesso);
				break;
		}
	}

	private void removePerfilDaLista(List<DireitoPerfil> direitos, Integer posicaoLista){
		switch (this.activeIndex){
			case 0:
				this.perfilHomeAcessos.get(posicaoLista).getPerfilAcesso().setDireitoPerfis(direitos);
				this.perfilHomeAcessos.get(posicaoLista).setPerfilAcesso(null);
				break;
			case 1:
				this.perfilAgendasAcessos.get(posicaoLista).getPerfilAcesso().setDireitoPerfis(direitos);
				this.perfilAgendasAcessos.get(posicaoLista).setPerfilAcesso(null);
				break;
			case 2:
				this.perfilAdvogadosAcessos.get(posicaoLista).getPerfilAcesso().setDireitoPerfis(direitos);
				this.perfilAdvogadosAcessos.get(posicaoLista).setPerfilAcesso(null);
				break;
			case 3:
				this.perfilCadastroAcessos.get(posicaoLista).getPerfilAcesso().setDireitoPerfis(direitos);
				this.perfilCadastroAcessos.get(posicaoLista).setPerfilAcesso(null);
				break;
            case 4:
                this.perfilContratosAcessos.get(posicaoLista).getPerfilAcesso().setDireitoPerfis(direitos);
                this.perfilContratosAcessos.get(posicaoLista).setPerfilAcesso(null);
                break;
			case 5:
				this.perfilControladoriaAcessos.get(posicaoLista).getPerfilAcesso().setDireitoPerfis(direitos);
				this.perfilControladoriaAcessos.get(posicaoLista).setPerfilAcesso(null);
				break;
			case 6:
				this.perfilFinanceiroAcessos.get(posicaoLista).getPerfilAcesso().setDireitoPerfis(direitos);
				this.perfilFinanceiroAcessos.get(posicaoLista).setPerfilAcesso(null);
				break;
			case 7:
				this.perfilPessoasAcessos.get(posicaoLista).getPerfilAcesso().setDireitoPerfis(direitos);
				this.perfilPessoasAcessos.get(posicaoLista).setPerfilAcesso(null);
				break;
			case 8:
				this.perfilProcessosAcessos.get(posicaoLista).getPerfilAcesso().setDireitoPerfis(direitos);
				this.perfilProcessosAcessos.get(posicaoLista).setPerfilAcesso(null);
				break;
			case 9:
				this.perfilRelatoriosAcessos.get(posicaoLista).getPerfilAcesso().setDireitoPerfis(direitos);
				this.perfilRelatoriosAcessos.get(posicaoLista).setPerfilAcesso(null);
				break;
			case 10:
				this.perfilAcessosAcessos.get(posicaoLista).getPerfilAcesso().setDireitoPerfis(direitos);
				this.perfilAcessosAcessos.get(posicaoLista).setPerfilAcesso(null);
				break;
			case 11:
				this.perfilDashboardsAcessos.get(posicaoLista).getPerfilAcesso().setDireitoPerfis(direitos);
				this.perfilDashboardsAcessos.get(posicaoLista).setPerfilAcesso(null);
				break;
		}
	}

	public void onTabChange(TabChangeEvent event) {
		TabView tabView = (TabView) event.getComponent();
		activeIndex = tabView.getChildren().indexOf(event.getTab());
	}

	private void removeDireitos(final List<DireitoPerfil> direitos){
		for (DireitoPerfil direito : direitos) {
			removePessoaConfiguracao(direito);
			this.direitoPerfilSessionBean.delete(direito);
		}
	}

	private void removePessoaConfiguracao(DireitoPerfil direito) {
		List<PessoaConfiguracao> pessoaConfiguracoes = pessoaConfiguracaoSessionBean.findByDireitoPerfil(direito.getId(), getStateManager().getEmpresaSelecionada().getId());
		for (PessoaConfiguracao pessoaConfiguracao : pessoaConfiguracoes) {
			this.pessoaConfiguracaoSessionBean.delete(pessoaConfiguracao);
		}
	}

	private void salvarDiretoPerfil(PerfilAcesso pa, DireitoPerfil dp, Long operacao, Integer posicaoLista) {
		try {
			if (dp == null) {
				createDireitoPerfil(pa, operacao);
			} else {
				this.direitoPerfilSessionBean.delete(dp);
			}

			salvaPerfilAcesso(pa, posicaoLista);

		} catch (ValidationException e) {
			for (String message : e.getMessages()) {
				exibirMensagem(FacesMessage.SEVERITY_ERROR, message);
			}
		}
	}

	private void salvaPerfilAcesso(PerfilAcesso pa, Integer posicaoLista) {
		List<DireitoPerfil> direitos = direitoPerfilSessionBean.findByPerfilAcesso(pa.getId(), getStateManager().getEmpresaSelecionada().getId());

		switch (this.activeIndex){
			case 0:
				this.perfilHomeAcessos.get(posicaoLista).getPerfilAcesso().setDireitoPerfis(direitos);
				break;		
			case 1:
				this.perfilAgendasAcessos.get(posicaoLista).getPerfilAcesso().setDireitoPerfis(direitos);
				break;
			case 2:
				this.perfilAdvogadosAcessos.get(posicaoLista).getPerfilAcesso().setDireitoPerfis(direitos);
				break;
			case 3:
				this.perfilCadastroAcessos.get(posicaoLista).getPerfilAcesso().setDireitoPerfis(direitos);
				break;
            case 4:
                this.perfilContratosAcessos.get(posicaoLista).getPerfilAcesso().setDireitoPerfis(direitos);
                break;
			case 5:
				this.perfilControladoriaAcessos.get(posicaoLista).getPerfilAcesso().setDireitoPerfis(direitos);
				break;
			case 6:
				this.perfilFinanceiroAcessos.get(posicaoLista).getPerfilAcesso().setDireitoPerfis(direitos);
				break;
			case 7:
				this.perfilPessoasAcessos.get(posicaoLista).getPerfilAcesso().setDireitoPerfis(direitos);
				break;
			case 8:
				this.perfilProcessosAcessos.get(posicaoLista).getPerfilAcesso().setDireitoPerfis(direitos);
				break;
			case 9:
				this.perfilRelatoriosAcessos.get(posicaoLista).getPerfilAcesso().setDireitoPerfis(direitos);
				break;
			case 10:
				this.perfilAcessosAcessos.get(posicaoLista).getPerfilAcesso().setDireitoPerfis(direitos);
				break;
			case 11:
				this.perfilDashboardsAcessos.get(posicaoLista).getPerfilAcesso().setDireitoPerfis(direitos);
				break;
		}
	}

	private DireitoPerfil getDireitoPerfil(PerfilAcesso pa, Long direito) {
		direitoPerfilSessionBean.clearSession();
		return direitoPerfilSessionBean.findByPerfilAndDireito(pa.getId(), direito, getStateManager().getEmpresaSelecionada().getId());
	}

	private PerfilAcesso getPerfilAcesso(Acesso acesso) {
		perfilAcessoSessionBean.clearSession();
		return perfilAcessoSessionBean.findByPerfilAndAcesso(
				this.perfilAcesso.getPerfil().getId(), acesso.getId(), getStateManager().getEmpresaSelecionada().getId());
	}

	private PerfilAcesso createPerfilAcesso(Acesso acesso) throws ValidationException {
		PerfilAcesso perfilAcesso = new PerfilAcesso(perfilSelecionado, acesso, getStateManager().getEmpresaSelecionada().getId());
		perfilAcessoSessionBean.create(perfilAcesso);
		return perfilAcesso;
	}

    public boolean isConfiguracaoContratos() {
        return getUser().getEmpresa().isConfiguracaoContratos();
    }

    public boolean isConfiguracaoControladoria() {
		return getUser().getEmpresa().isConfiguracaoControladoria();
	}

	public boolean isConfiguracaoFinanceiro() {
		return getUser().getEmpresa().isConfiguracaoFinanceiro();
	}

	public boolean isConfiguracaoRobo() {
		return getUser().getEmpresa().isConfiguracaoRobot();
	}

	/**
	 *
	 * @return
	 */
	public String goToFormulario() {
		return "/pages/acessos/acessos-template.xhtml";
	}

	public List<Perfil> getPerfis() {
		return perfilSessionBean.retrieveAll(getStateManager().getEmpresaSelecionada().getId());
	}

	public PerfilAcesso getPerfilAcesso() {
		return perfilAcesso;
	}

	public void setPerfilAcesso(PerfilAcesso perfilAcesso) {
		this.perfilAcesso = perfilAcesso;
	}

	public List<PerfilAcessoDto> getPerfilCadastroAcessos() {
		return perfilCadastroAcessos;
	}

	public List<PerfilAcessoDto> getPerfilPessoasAcessos() {
		return perfilPessoasAcessos;
	}

	public List<PerfilAcessoDto> getPerfilAdvogadosAcessos() {
		return perfilAdvogadosAcessos;
	}

	public void setPerfilAdvogadosAcessos(List<PerfilAcessoDto> perfilAdvogadosAcessos) {
		this.perfilAdvogadosAcessos = perfilAdvogadosAcessos;
	}

	public List<PerfilAcessoDto> getPerfilProcessosAcessos() {
		return perfilProcessosAcessos;
	}

	public void setPerfilProcessosAcessos(List<PerfilAcessoDto> perfilProcessosAcessos) {
		this.perfilProcessosAcessos = perfilProcessosAcessos;
	}

	public List<PerfilAcessoDto> getPerfilDashboardsAcessos() {
		return perfilDashboardsAcessos;
	}

	public void setPerfilDashboardsAcessos(
			List<PerfilAcessoDto> perfilDashboardsAcessos) {
		this.perfilDashboardsAcessos = perfilDashboardsAcessos;
	}

	public int getActiveIndex() {
		return activeIndex;
	}

	public void setActiveIndex(int activeIndex) {
		this.activeIndex = activeIndex;
	}

	public List<PerfilAcessoDto> getPerfilRelatoriosAcessos() {
		return perfilRelatoriosAcessos;
	}

	public void setPerfilRelatoriosAcessos(List<PerfilAcessoDto> perfilRelatoriosAcessos) {
		this.perfilRelatoriosAcessos = perfilRelatoriosAcessos;
	}

	public List<PerfilAcessoDto> getPerfilAgendasAcessos() {
		return perfilAgendasAcessos;
	}

	public void setPerfilAgendasAcessos(List<PerfilAcessoDto> perfilAgendasAcessos) {
		this.perfilAgendasAcessos = perfilAgendasAcessos;
	}

	public List<PerfilAcessoDto> getPerfilAcessosAcessos() {
		return perfilAcessosAcessos;
	}

	public void setPerfilAcessosAcessos(List<PerfilAcessoDto> perfilAcessosAcessos) {
		this.perfilAcessosAcessos = perfilAcessosAcessos;
	}

	public List<PerfilAcessoDto> getPerfilHomeAcessos() {
		return perfilHomeAcessos;
	}

	public void setPerfilHomeAcessos(List<PerfilAcessoDto> perfilHomeAcessos) {
		this.perfilHomeAcessos = perfilHomeAcessos;
	}

    public List<PerfilAcessoDto> getPerfilContratosAcessos() {
        return perfilContratosAcessos;
    }

    public void setPerfilContratosAcessos(List<PerfilAcessoDto> perfilContratosAcessos) {
        this.perfilContratosAcessos = perfilContratosAcessos;
    }

	public List<PerfilAcessoDto> getPerfilControladoriaAcessos() {
		return perfilControladoriaAcessos;
	}

	public List<PerfilAcessoDto> getPerfilFinanceiroAcessos() {
		return perfilFinanceiroAcessos;
	}

	public void setPerfilFinanceiroAcessos(List<PerfilAcessoDto> perfilFinanceiroAcessos) {
		this.perfilFinanceiroAcessos = perfilFinanceiroAcessos;
	}

	public void setPerfilControladoriaAcessos(
			List<PerfilAcessoDto> perfilControladoriaAcessos) {
		this.perfilControladoriaAcessos = perfilControladoriaAcessos;
	}
}
