package br.com.juridico.controller;

import br.com.juridico.datamodel.LazyEntityDataModel;
import br.com.juridico.entidades.ClasseProcessual;
import br.com.juridico.entidades.Direito;
import br.com.juridico.exception.CrudException;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.impl.ClasseProcessualSessionBean;
import br.com.juridico.impl.DireitoSessionBean;
import br.com.juridico.impl.PerfilAcessoSessionBean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Jefferson
 *
 */
@ManagedBean
@SessionScoped
public class ClasseProcessualController extends CrudController<ClasseProcessual> {

	/**
	 *
	 */
	private static final long serialVersionUID = -1215733267347987900L;
	private static final String CLASSE_PROCESSUAL_FORM = "classeProcessualForm";
	private static final String CHAVE_ACESSO = "CLASSEPROCESSUAL_CLASSE_PROCESSUAL_TEMPLATE";
	private static final String PAGINA_FORMULARIO = "/pages/cadastros/classeProcessual/classe-processual-template.xhtml?faces-redirect=true";

	@Inject
	private ClasseProcessualSessionBean classeProcessualSessionBean;
	@Inject
	private PerfilAcessoSessionBean perfilAcessoSessionBean;
	@Inject
	private DireitoSessionBean direitoSessionBean;

	public void init(){
		carregaLazyDataModel();
	}

	@Override
	protected void validar() throws ValidationException {
		classeProcessualSessionBean.clearSession();
		boolean jaExisteClasseProcessual = classeProcessualSessionBean.isDescricaoJaCadastrada(getObjetoSelecionado(), getStateManager().getEmpresaSelecionada().getId());
		getObjetoSelecionado().validar(jaExisteClasseProcessual);
	}

	@Override
	protected void aposCancelar() throws CrudException {
		getObjetoSelecionado().setDescricao(null);
		updateContext(CLASSE_PROCESSUAL_FORM);
		super.aposCancelar();
	}

	@Override
	protected void create(ClasseProcessual classeProcessual) throws ValidationException, CrudException {
		ClasseProcessual classeExclusao = getRegistroComExclusaoLogica();

		if (isEditavel() && classeExclusao != null) {
			classeExclusao.setDataExclusao(null);
			classeProcessualSessionBean.update(classeExclusao);
		} else if (classeProcessual != null && classeProcessual.getDescricao() != null && !classeProcessual.getDescricao().isEmpty()) {
			classeProcessualSessionBean.create(classeProcessual);
		} else {
			throw new CrudException();
		}

	}

	private ClasseProcessual getRegistroComExclusaoLogica() {
		return classeProcessualSessionBean.findByDescricao(getObjetoSelecionado().getDescricao(), getStateManager().getEmpresaSelecionada().getId());
	}

	@Override
	protected void antesDeExcluir() throws CrudException {
		getObjetoSelecionado().setDataExclusao(new Date());
	}

	@Override
	protected void aposExcluir() throws CrudException {
		super.buscarPorLazy();
	}

	@SuppressWarnings("serial")
	@Override
	protected void carregaLazyDataModel() {
		setLazyDataModel(new LazyEntityDataModel<ClasseProcessual>() {
			@Override
			protected List<ClasseProcessual> consultaFiltrada(ConsultaLazyFilter filtro) {
				filtro.setDescricao(getDescricaoFiltro());
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return classeProcessualSessionBean.filtrados(filtro);
			}

			@Override
			protected int countTotalRegistros(ConsultaLazyFilter filtro) {
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return classeProcessualSessionBean.quantidadeFiltrados(filtro);
			}
		});
	}

	/**
	 *
	 * @return
	 */
	public String goToFormClasseProcessual() {
		return PAGINA_FORMULARIO;
	}


	@Override
	protected ClasseProcessual inicializaObjetosLazy(ClasseProcessual objeto) {
		classeProcessualSessionBean.clearSession();
		return classeProcessualSessionBean.retrieve(objeto.getId());
	}

	@Override
	protected ClasseProcessual instanciarObjetoNovo() {
		return new ClasseProcessual();
	}

	@Override
	protected void setEmpresaDeUsuarioLogado(ClasseProcessual objetoSelecionado) {
		objetoSelecionado.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
	}

	@Override
	protected void addChaveAcesso() {
		setChaveAcesso(CHAVE_ACESSO);
	}

	@Override
	protected boolean isPodeAlterar(){
		Direito direito = direitoSessionBean.retrieve(ALTERAR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

	@Override
	protected boolean isPodeExcluir(){
		Direito direito = direitoSessionBean.retrieve(EXCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

	@Override
	protected boolean isPodeIncluir(){
		Direito direito = direitoSessionBean.retrieve(INCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}
}
