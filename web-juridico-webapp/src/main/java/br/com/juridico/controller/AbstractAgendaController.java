package br.com.juridico.controller;

import br.com.juridico.dto.PessoaAgendaDto;
import br.com.juridico.entidades.*;
import br.com.juridico.enums.ControladoriaIndicador;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.enums.EventoColorTypes;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.LazyScheduleModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by jeffersonl2009_00 on 06/07/2016.
 */
public abstract class AbstractAgendaController extends AbstractController {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7153741089763299149L;

	protected static final String TIME_ZONE = "GMT-3";
	protected static final String MONTH_TYPE = "month";
	protected static final String AGENDA_DAY_TYPE = "agendaDay";
	protected static final String EVENTO_SELECIONADO_SESSION = "EVENTO_SELECIONADO_SESSION";
	protected static final String AGENDA_ALTERACAO_AGENDA = "AGENDA_ALTERACAO_AGENDA";
	protected static final String AGENDA_ADICIONAR_EVENTO = "AGENDA_ADICIONAR_EVENTO";
	protected static final String AGENDA_EXCLUIR_EVENTO = "AGENDA_EXCLUIR_EVENTO";

	private List<PessoaAgendaDto> pessoaMenuList = Lists.newArrayList();

	private String styleClassSelect;
	private ScheduleModel lazyEventModel;
	private Date dataInicioAgenda;
	private Date dataFimAgenda;
	protected boolean criarAgenda;


	@SuppressWarnings("serial")
	protected void carregaLazyEventModel() {
		lazyEventModel = new LazyScheduleModel() {
			@Override
			public void loadEvents(Date start, Date end) {
				clear();
				setDataInicioAgenda(start);
				setDataFimAgenda(end);
				for (Evento evento : listaDeEventos()) {
					String flagIniciais = Strings.isNullOrEmpty(evento.getNomeIniciais()) ? "" : evento.getNomeIniciais().concat(" - ");
					if(ControladoriaIndicador.PENDENTE.getStatus().equals(evento.getControladoriaStatus())) {
						evento.setStyle(EventoColorTypes.BLACK.getValue());
					}

					DefaultScheduleEvent scheduleEvent = new DefaultScheduleEvent(flagIniciais + evento.getDescricao(),
							evento.getDataInicio(), evento.getDataFim(), evento.getStyle());
					scheduleEvent.setData(evento.getChaveId());
					scheduleEvent.setAllDay(evento.getHorarioIndefinido() != null && evento.getHorarioIndefinido());
					scheduleEvent.setDescription(evento.getObservacao());
					addEvent(scheduleEvent);
				}
			}
		};
	}

	protected void verificaUsuarioLogadoEhAdmin(List<Pessoa> administradores) {
		for (Iterator<Pessoa> iterator = administradores.iterator(); iterator.hasNext();) {
			Pessoa pessoa = iterator.next();
			if (getUser().getLogin().equals(pessoa)) {
				iterator.remove();
			}
		}
	}

	protected void addDestinatarios(EmailCaixaSaida caixaSaida, List<Pessoa> administradores) {
		String emailUsuarioLogado = getUser().getEmail();
		if (!Strings.isNullOrEmpty(emailUsuarioLogado))
			caixaSaida.addEmails(new EmailDestinatario(caixaSaida, emailUsuarioLogado));

		verificaUsuarioLogadoEhAdmin(administradores);
		for (Pessoa pessoa : administradores) {
			PessoaEmail pessoaEmail = pessoa.getPessoasEmails().isEmpty() ? null : pessoa.getPessoasEmails().get(0);
			if (pessoaEmail != null) {
				EmailDestinatario destinatario = new EmailDestinatario(caixaSaida, pessoaEmail.getEmail());
				caixaSaida.addEmails(destinatario);
			}

		}
	}

	protected void atualizarEvento(Evento eventoSelecionado, ScheduleEvent event) throws ValidationException {
		getLazyEventModel().updateEvent(event);
		if (eventoSelecionado != null) {
			StatusAgenda statusAgenda = eventoSelecionado.getStatusAgenda();
			if (statusAgenda != null) {
				eventoSelecionado.setStyle(EventoColorTypes.fromTextColor(statusAgenda.getStyleColor()).getValue());
			}
			updateEvento();
		}
	}

	protected Integer getHora(String valorHora) {
		return !Strings.isNullOrEmpty(valorHora) ? Integer.parseInt(valorHora.substring(0, 2)) : 0;
	}

	protected Integer getMinuto(String valorHora) {
		return !Strings.isNullOrEmpty(valorHora) ? Integer.parseInt(valorHora.substring(3, 5)) : 0;
	}

	protected abstract List<Evento> listaDeEventos();


	public String getStyleClassSelect() {
		return styleClassSelect;
	}

	public void setStyleClassSelect(String styleClassSelect) {
		this.styleClassSelect = styleClassSelect;
	}

	public ScheduleModel getLazyEventModel() {
		return lazyEventModel;
	}

	public void setLazyEventModel(ScheduleModel lazyEventModel) {
		this.lazyEventModel = lazyEventModel;
	}

	public Date getDataInicioAgenda() {
		return dataInicioAgenda;
	}

	public void setDataInicioAgenda(Date dataInicioAgenda) {
		this.dataInicioAgenda = dataInicioAgenda;
	}

	public Date getDataFimAgenda() {
		return dataFimAgenda;
	}

	public void setDataFimAgenda(Date dataFimAgenda) {
		this.dataFimAgenda = dataFimAgenda;
	}

	public List<PessoaAgendaDto> getPessoaMenuList() {
		return pessoaMenuList;
	}

	public void setPessoaMenuList(List<PessoaAgendaDto> pessoaMenuList) {
		this.pessoaMenuList = pessoaMenuList;
	}

	/**
	 *
	 */
	protected abstract void updateEvento();

	/**
	 *
	 * @param email
	 * @param textoFormatado
     * @return
     */
	protected abstract boolean enviarEmail(Email email, String textoFormatado);

}
