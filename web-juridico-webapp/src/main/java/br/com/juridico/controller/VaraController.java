package br.com.juridico.controller;

import br.com.juridico.datamodel.LazyEntityDataModel;
import br.com.juridico.entidades.Direito;
import br.com.juridico.entidades.Vara;
import br.com.juridico.exception.CrudException;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.impl.DireitoSessionBean;
import br.com.juridico.impl.PerfilAcessoSessionBean;
import br.com.juridico.impl.VaraSessionBean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Jefferson
 *
 */
@ManagedBean
@SessionScoped
public class VaraController extends CrudController<Vara> {

	/**
	 *
	 */
	private static final long serialVersionUID = -8741515223323095235L;
	private static final String VARA_FORM = "varaForm";
	private static final String CHAVE_ACESSO = "VARA_VARA_TEMPLATE";
	private static final String PAGINA_FORMULARIO = "/pages/cadastros/vara/vara-template.xhtml?faces-redirect=true";

	@Inject
	private VaraSessionBean varaSessionBean;
	@Inject
	private PerfilAcessoSessionBean perfilAcessoSessionBean;
	@Inject
	private DireitoSessionBean direitoSessionBean;

	public void init(){
		carregaLazyDataModel();
	}

	@Override
	protected void validar() throws ValidationException {
		varaSessionBean.clearSession();
		boolean jaExisteVara = varaSessionBean.isDescricaoJaCadastrada(getObjetoSelecionado(), getStateManager().getEmpresaSelecionada().getId());
		getObjetoSelecionado().validar(jaExisteVara);
	}

	@Override
	protected void carregaLazyDataModel() {
		setLazyDataModel(new LazyEntityDataModel<Vara>() {
			@Override
			protected List<Vara> consultaFiltrada(ConsultaLazyFilter filtro) {
				filtro.setDescricao(getDescricaoFiltro());
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return varaSessionBean.filtrados(filtro);
			}

			@Override
			protected int countTotalRegistros(ConsultaLazyFilter filtro) {
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return varaSessionBean.quantidadeFiltrados(filtro);
			}
		});
	}



	@Override
	protected void aposCancelar() throws CrudException {
		getObjetoSelecionado().setDescricao(null);
		updateContext(VARA_FORM);
		super.aposCancelar();
	}

	@Override
	protected void create(Vara vara) throws ValidationException, CrudException {
		Vara varaExclusao = getRegistroComExclusaoLogica();

		if (isEditavel() && varaExclusao != null) {
			varaExclusao.setDataExclusao(null);
			varaSessionBean.update(varaExclusao);
		} else if (vara != null && vara.getDescricao() != null && !vara.getDescricao().isEmpty()) {
			varaSessionBean.create(vara);
		} else {
			throw new CrudException();
		}

	}

	private Vara getRegistroComExclusaoLogica() {
		return varaSessionBean.findByDescricao(getObjetoSelecionado().getDescricao(), getStateManager().getEmpresaSelecionada().getId());
	}

	@Override
	protected void antesDeExcluir() throws CrudException {
		getObjetoSelecionado().setDataExclusao(new Date());
	}

	@Override
	protected void aposExcluir() throws CrudException {
		super.buscarPorLazy();
	}

	/**
	 *
	 * @return
	 */
	public String goToFormulario() {
		return PAGINA_FORMULARIO;
	}

	@Override
	protected Vara inicializaObjetosLazy(Vara objeto) {
		varaSessionBean.clearSession();
		return varaSessionBean.retrieve(objeto.getId());
	}

	@Override
	protected Vara instanciarObjetoNovo() {
		return new Vara();
	}

	@Override
	protected void setEmpresaDeUsuarioLogado(Vara objetoSelecionado) {
		objetoSelecionado.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());

	}

	@Override
	protected void addChaveAcesso() {
		setChaveAcesso(CHAVE_ACESSO);
	}

	@Override
	protected boolean isPodeAlterar(){
		Direito direito = direitoSessionBean.retrieve(ALTERAR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

	@Override
	protected boolean isPodeExcluir(){
		Direito direito = direitoSessionBean.retrieve(EXCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

	@Override
	protected boolean isPodeIncluir(){
		Direito direito = direitoSessionBean.retrieve(INCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

}
