package br.com.juridico.controller;

import br.com.juridico.datamodel.LazyEntityDataModel;
import br.com.juridico.entidades.Direito;
import br.com.juridico.entidades.Forum;
import br.com.juridico.exception.CrudException;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.impl.DireitoSessionBean;
import br.com.juridico.impl.ForumSessionBean;
import br.com.juridico.impl.PerfilAcessoSessionBean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Jefferson
 *
 */
@ManagedBean
@SessionScoped
public class ForumController extends CrudController<Forum> {

	/**
	 *
	 */
	private static final long serialVersionUID = -1647697717463857500L;
	private static final String FORUM_FORM = "forumForm";
	private static final String CHAVE_ACESSO = "FORUM_FORUM_TEMPLATE";
	private static final String PAGINA_FORMULARIO = "/pages/cadastros/forum/forum-template.xhtml?faces-redirect=true";

	@Inject
	private ForumSessionBean forumSessionBean;
	@Inject
	private PerfilAcessoSessionBean perfilAcessoSessionBean;
	@Inject
	private DireitoSessionBean direitoSessionBean;

	public void init(){
		carregaLazyDataModel();
	}

	@Override
	protected void validar() throws ValidationException {
		forumSessionBean.clearSession();
		boolean jaExisteForum = forumSessionBean.isDescricaoJaCadastrada(getObjetoSelecionado(), getStateManager().getEmpresaSelecionada().getId());
		getObjetoSelecionado().validar(jaExisteForum);
	}

	@Override
	protected void aposCancelar() throws CrudException {
		getObjetoSelecionado().setDescricao(null);
		updateContext(FORUM_FORM);
		super.aposCancelar();
	}

	@Override
	protected void create(Forum forum) throws ValidationException, CrudException {
		Forum forumExclusao = getRegistroComExclusaoLogica();

		if (isEditavel() && forumExclusao != null) {
			forumExclusao.setDataExclusao(null);
			forumSessionBean.update(forumExclusao);
		} else if (forum != null && forum.getDescricao() != null && !forum.getDescricao().isEmpty()) {
			forumSessionBean.create(forum);
		} else {
			throw new CrudException();
		}

	}

	@Override
	protected void carregaLazyDataModel() {
		setLazyDataModel(new LazyEntityDataModel<Forum>() {
			@Override
			protected List<Forum> consultaFiltrada(ConsultaLazyFilter filtro) {
				filtro.setDescricao(getDescricaoFiltro());
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return forumSessionBean.filtrados(filtro);
			}

			@Override
			protected int countTotalRegistros(ConsultaLazyFilter filtro) {
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return forumSessionBean.quantidadeFiltrados(filtro);
			}
		});
	}

	@Override
	protected void antesDeExcluir() throws CrudException {
		getObjetoSelecionado().setDataExclusao(new Date());
	}

	@Override
	protected void aposExcluir() throws CrudException {
		super.buscarPorLazy();
	}

	@Override
	protected Forum inicializaObjetosLazy(Forum objeto) {
		forumSessionBean.clearSession();
		return forumSessionBean.retrieve(objeto.getId());
	}

	@Override
	protected Forum instanciarObjetoNovo() {
		return new Forum();
	}

	@Override
	protected void setEmpresaDeUsuarioLogado(Forum objetoSelecionado) {
		objetoSelecionado.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());

	}

	private Forum getRegistroComExclusaoLogica() {
		return forumSessionBean.findByDescricao(getObjetoSelecionado().getDescricao(), getStateManager().getEmpresaSelecionada().getId());
	}

	/**
	 *
	 * @return
	 */
	public String goToFormulario() {
		return PAGINA_FORMULARIO;
	}

	@Override
	protected void addChaveAcesso() {
		setChaveAcesso(CHAVE_ACESSO);
	}

	@Override
	protected boolean isPodeAlterar(){
		Direito direito = direitoSessionBean.retrieve(ALTERAR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

	@Override
	protected boolean isPodeExcluir(){
		Direito direito = direitoSessionBean.retrieve(EXCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

	@Override
	protected boolean isPodeIncluir(){
		Direito direito = direitoSessionBean.retrieve(INCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

}
