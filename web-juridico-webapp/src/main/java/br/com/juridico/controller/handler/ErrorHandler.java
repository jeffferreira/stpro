package br.com.juridico.controller.handler;

/**
 * Created by jeffersonl2009_00 on 11/11/2016.
 */

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@SessionScoped
public class ErrorHandler {

    public String getStatusCode(){
        return String.valueOf((FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get("javax.servlet.error.status_code")));
    }

    public String getMessage(){
        String val = (String)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("javax.servlet.error.message");
        return val;
    }

    public String getExceptionType(){
        Object val = FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get("javax.servlet.error.exception_type");
        return val == null ? "" : val.toString();
    }

    public String getException(){
        Object val = FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get("javax.servlet.error.exception");
        return val == null ? "" : val.toString();
    }

    public String getRequestURI(){
        return (String)FacesContext.getCurrentInstance().getExternalContext().
                getRequestMap().get("javax.servlet.error.request_uri");
    }

    public String getServletName(){
        return (String)FacesContext.getCurrentInstance().getExternalContext().
                getRequestMap().get("javax.servlet.error.servlet_name");
    }
}
