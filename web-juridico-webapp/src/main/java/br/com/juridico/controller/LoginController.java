package br.com.juridico.controller;

import br.com.juridico.business.EmailTemplateBusiness;
import br.com.juridico.business.ParametroEmailBusiness;
import br.com.juridico.business.SenhaEmailBusiness;
import br.com.juridico.dto.LoginEmailDto;
import br.com.juridico.ejb.StateManager;
import br.com.juridico.entidades.*;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.impl.EmailCaixaSaidaSessionBean;
import br.com.juridico.impl.EmailSessionBean;
import br.com.juridico.impl.EmpresaSessionBean;
import br.com.juridico.impl.PessoaSessionBean;
import br.com.juridico.proxy.ProxyLogin;
import br.com.juridico.util.DateUtils;
import br.com.juridico.util.DecryptUtil;
import br.com.juridico.util.EncryptUtil;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.*;
import java.util.stream.Collectors;

/**
 *
 * @author Jefferson
 *
 */

@ManagedBean(name="loginController")
@SessionScoped
public class LoginController extends AbstractController {

    /**
     *
     */
    private static final long serialVersionUID = -5428132449515505134L;

    private static final Logger LOGGER = Logger.getLogger(LoginController.class);

    private static final String SESSAO_ID = "SESSAO_ID";
    private static final String PRIMEIRO_ACESSO = "Primeiro Acesso";
    private static final String SENHA_ALTERACAO_SENHA = "SENHA_ALTERACAO_SENHA";
    private static final String SENHA_RECUPERACAO_SENHA = "SENHA_RECUPERACAO_SENHA";
    private static final String USER_PRIMEIRO_ACESSO_LOGGED = "userPrimeiroAcessoLogged";
    private static final String PAGINA_LOGIN = "/login/login.xhtml?faces-redirect=true";
    private static final String PAGES_LOGIN_RECUPERA_SENHA = "/login/recupera-senha.xhtml";
    private static final String PAGES_LOGIN_ALTERA_SENHA = "/login/altera-senha.xhtml";
    private static final String PERMITE_TROCAR_FILIAL = "PERMITE_TROCAR_FILIAL";

    protected Pessoa pessoa;
    private String login;
    private String cpf;
    private String senha;
    private String email;
    private String senhaAtual;
    private String novaSenha;
    private String confirmacaoSenha;
    private Long empresaSelecionada;
    private Empresa empresa;
    private Perfil perfilSelecionado;

    private boolean pessoaCadastradaEmMaisDeUmaEmpresa;
    private boolean pessoaCadastradaEmMaisDeUmPerfil;

    private List<Empresa> empresas = Lists.newArrayList();
    private List<Perfil> perfis = Lists.newArrayList();
    private List<Pessoa> pessoas = Lists.newArrayList();
    private Set<Long> empresasSet = Sets.newHashSet();

    @Inject
    private ApplicationController applicationController;
    @Inject
    private PessoaSessionBean pessoaSessionBean;
    @Inject
    private EmailSessionBean emailSessionBean;
    @Inject
    private EmpresaSessionBean empresaSessionBean;
    @Inject
    private EmailCaixaSaidaSessionBean emailCaixaSaidaSessionBean;
    @Inject
    private StateManager stateManager;

    public void exception() throws Exception {
        throw new Exception("Excecao teste");
    }

    /**
     *
     * @return
     */
    public String acessarSistema() {
        if(pessoaCadastradaEmMaisDeUmaEmpresa && !pessoaCadastradaEmMaisDeUmPerfil) {
            atualizaEmpresa();
            this.pessoa = this.pessoas.get(0);
        }
        ProxyLogin proxy = new ProxyLogin(pessoa, this.senha);
        String redirect = proxy.executeLogin();
        addMensagensProxy(proxy);
        return redirect;
    }

    public void loginChanged(ValueChangeEvent event){
        this.login = (String) event.getNewValue();
        this.pessoaSessionBean.clearSession();
        this.pessoas = pessoaSessionBean.findUserList(login);
        carregaPessoa(this.pessoas);
        verificaPessoaCadastradaEmMaisDeUmaEmpresa(pessoas);
        if(!pessoaCadastradaEmMaisDeUmaEmpresa){
            verificaPessaoCadastradaEmMaisDeUmPerfil(pessoas);
        }
    }

    public void atualizaEmpresa(){
        this.empresa = empresaSessionBean.retrieve(empresaSelecionada);
        for (Iterator<Pessoa> it = pessoas.iterator(); it.hasNext();) {
            Pessoa pessoa = it.next();
            if(!pessoa.getCodigoEmpresa().equals(this.empresa.getId())){
                it.remove();
            }
        }
        setPessoaCadastradaEmMaisDeUmPerfil(pessoas.size() > 1);
    }

    public void atualizaPerfil(){
        if(this.perfilSelecionado == null) {
            return;
        }
        for (Pessoa p : this.pessoas) {
            if(p.getPerfil().getId().equals(this.perfilSelecionado.getId())){
                this.pessoa = p;
                return;
            }
        }
    }

    public void verificaPessoaCadastradaEmMaisDeUmaEmpresa(List<Pessoa> pessoas){
        Set<Long> empresas = new HashSet<>();
        pessoas.stream().forEach(p -> empresas.add(p.getCodigoEmpresa()));
        this.pessoaCadastradaEmMaisDeUmaEmpresa = empresas.size() > 1;
    }

    public void verificaPessaoCadastradaEmMaisDeUmPerfil(List<Pessoa> pessoas) {
        Set<Long> perfis = new HashSet<>();
        this.perfis.clear();
        for (Pessoa p : pessoas) {
            if(perfis.add(p.getPerfil().getId())){
                this.perfis.add(p.getPerfil());
            }
        }
        this.pessoaCadastradaEmMaisDeUmPerfil = perfis.size() > 1;
    }

    protected void carregaPessoa(List<Pessoa> pessoas) {
        if(pessoas.size() == 1){
            this.pessoa = pessoas.get(0);
            this.empresaSelecionada = empresaSessionBean.retrieve(pessoa.getCodigoEmpresa()).getId();
        } else if(pessoas.size() > 1) {
            empresasSet.addAll(pessoas.stream().map(Pessoa::getCodigoEmpresa).collect(Collectors.toList()));
            empresas.addAll(empresasSet.stream().map(idEmpresa -> empresaSessionBean.retrieve(idEmpresa)).collect(Collectors.toList()));
        }
    }

    public void recuperarSenha() {
        if(!Strings.isNullOrEmpty(email) && !Strings.isNullOrEmpty(this.cpf)) {
            List<Pessoa> pessoas = pessoaSessionBean.findByCpf(this.cpf);
            if(pessoas.isEmpty()) {
                exibirMensagem(FacesMessage.SEVERITY_ERROR, "O CPF não está cadastrado no sistema, favor entrar em contato com administrador.");
                return;
            }
            LoginEmailDto loginEmailDto = pessoaSessionBean.findByEmailAtivo(this.email, this.cpf, pessoas.get(0).getCodigoEmpresa());

            if(loginEmailDto == null) {
                exibirMensagem(FacesMessage.SEVERITY_ERROR, "O email não está cadastrado no sistema, favor entrar em contato com administrador.");
            } else {
                Pessoa p = pessoaSessionBean.findUser(loginEmailDto.getLogin(), pessoas.get(0).getCodigoEmpresa());
                enviaEmailRecuperacaoSenha(p);
                limparCamposRecuperacaoSenha();
            }
        }
    }

    public boolean isMatriz(){
        empresaSessionBean.clearSession();
        Empresa empresa = empresaSessionBean.retrieve(getUser().getCodEmpresa());

        return empresa != null
                && !empresa.getFiliais().isEmpty()
                && empresa.getMatriz() == null;
    }

    private void limparCamposRecuperacaoSenha() {
        this.cpf = "";
        this.email = "";
    }

    public void alterarSenha(){
        try {
            List<Pessoa> pessoas = pessoaSessionBean.findUserList(login);

            if(pessoas.isEmpty()) {
                exibirMensagem(FacesMessage.SEVERITY_ERROR, "Login não encontrado.");
                return;
            }

            String decrypted = DecryptUtil.execute(pessoas.get(0).getLogin().getSenha());

            if(!decrypted.equals(senhaAtual)) {
                exibirMensagem(FacesMessage.SEVERITY_ERROR, "Senha atual inválida.");
                return;
            }

            if(!novaSenha.equals(confirmacaoSenha)) {
                exibirMensagem(FacesMessage.SEVERITY_ERROR, "Confirmação: Senhas não conferem.");
                return;
            }

            pessoas.stream().forEach(p -> updateSenha(p));

        } catch (Exception e) {
            LOGGER.error("Alter password error: ", e);
        }
    }

    /**
     *
     * @return
     */
    public String logout() {
        try {
            getExternalContext().invalidateSession();
            return PAGINA_LOGIN;
        } catch (Exception e) {
            LOGGER.error(e, e.getCause());
            getFacesContext().addMessage(null, new FacesMessage("Logout failed."));
            return "";
        }
    }

    public void updateSenha(Pessoa pessoa){
        try {
            pessoa.getLogin().setSenha(EncryptUtil.execute(this.novaSenha));
            pessoaSessionBean.update(pessoa);
            exibirMensagem(FacesMessage.SEVERITY_INFO, "Senha alterada com sucesso.");
            enviaEmailAlteracaoSenha(pessoa);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

    }

    public void atualizarEstadoPaginaListener(SelectEvent event){
        getStateManager().setEmpresaSelecionada((Empresa) event.getObject());
        RequestContext.getCurrentInstance().execute("window.location.reload()");
    }

    private void addMensagensProxy(ProxyLogin proxy) {
        if(Strings.isNullOrEmpty(proxy.getMessage())){
            carregaUsuarioNaSessao();
            this.pessoa.getLogin().setDataUltimoAcesso(Calendar.getInstance().getTime());
            updatePessoa();
            return;
        } else if(PRIMEIRO_ACESSO.equals(proxy.getMessage())) {
            carregaUsuarioPrimeiroAcessoNaSessao();
            return;
        }

        this.senha = proxy.isLimpaSenha() ? "" : this.senha;
        this.login = proxy.isLimpaLogin() ? "" : this.login;
        super.addMessage(FacesMessage.SEVERITY_ERROR, proxy.getMessage());

    }

    private void updatePessoa(){
        try {
            pessoaSessionBean.update(this.pessoa);
        } catch (ValidationException e) {
            LOGGER.error(e, e.getCause());
        }
    }

    private void carregaUsuarioNaSessao() {
        HttpServletRequest request = (HttpServletRequest) getExternalContext().getRequest();
        empresaSessionBean.clearSession();
        Empresa empresa = empresaSessionBean.retrieve(this.pessoa.getCodigoEmpresa());
        stateManager.setEmpresaSelecionada(empresa);
        User userLogged = setUser(this.pessoa, empresa);
        setSessionAttribute(USER_LOGGED, userLogged);
        setSessionAttribute(SESSAO_ID, request.getSession().getId());
        if(this.pessoa.getPerfil() != null) {
            setSessionAttribute("perfil", this.pessoa.getPerfil().getDescricao());
            setSessionAttribute("dataCadastro", DateUtils.formataDataBrasil(this.pessoa.getDataCadastro()));
        }
        if(applicationController.getSessionApplicationMap().get(userLogged.getLogin()) == null) {
            applicationController.getSessionApplicationMap().put(this.pessoa.getLogin().getLogin(), request.getSession());
        }
    }

    private void carregaUsuarioPrimeiroAcessoNaSessao() {
        UserPrimeiroAcesso userPrimeiroAcesso = setUserPrimeiroAcesso(this.pessoa);
        setSessionAttribute(USER_PRIMEIRO_ACESSO_LOGGED, userPrimeiroAcesso);
    }

    private void enviaEmailRecuperacaoSenha(Pessoa pessoa) {
        try {
            Email email = emailSessionBean.findBySigla(SENHA_RECUPERACAO_SENHA, pessoa.getCodigoEmpresa());

            if(email == null || (email != null && !email.getAtivo())) {
                exibirMensagem(FacesMessage.SEVERITY_INFO, "Email para recuperação de senha não cadastrado ou inativo...");
                return;
            }

            Map<String, String> parametersValues = Maps.newHashMap();
            for (EmailVinculoParametro parametro : email.getParametroEmails()) {
                String valor = SenhaEmailBusiness.execute(parametro.getParametroEmail(), pessoa, pessoa, getUser());
                parametersValues.put(parametro.getParametroEmail().getSigla(), valor);
            }

            String textoFormatado = EmailTemplateBusiness.criarEmailTemplate(email.getAssunto(), email.getDescricao(), parametersValues);
            EmailCaixaSaida emailCaixaSaida = new EmailCaixaSaida(getEmail(), email.getAssunto(), textoFormatado, pessoa.getCodigoEmpresa());
            addDestinatarios(emailCaixaSaida, pessoa);

            if(!email.getNecessarioAprovacao()) {
                emailCaixaSaidaSessionBean.createSemAprovacao(emailCaixaSaida);
                exibirMensagem(FacesMessage.SEVERITY_INFO, "Email com recuperação da senha enviado...");
                return;
            }

            emailCaixaSaidaSessionBean.create(emailCaixaSaida);
            exibirMensagem(FacesMessage.SEVERITY_INFO, "Email de recuperação da senha aguardando aprovação do Administrador do sistema...");
        } catch (Exception e) {
            LOGGER.error(e.getCause(), e);
        }
    }


    private void enviaEmailAlteracaoSenha(Pessoa pessoa) {
        String enderecoEmail = !pessoa.getPessoasEmails().isEmpty() ? pessoa.getPessoasEmails().get(0).getEmail() : "";
        if(Strings.isNullOrEmpty(enderecoEmail)) {
            return;
        }

        try {
            Email email = emailSessionBean.findBySigla(SENHA_ALTERACAO_SENHA, pessoa.getCodigoEmpresa());

            if(email == null || (email != null && !email.getAtivo())) {
                exibirMensagem(FacesMessage.SEVERITY_INFO, "Email para alteração de senha não cadastrado ou inativo...");
                return;
            }

            Map<String, String> parametersValues = Maps.newHashMap();
            for (EmailVinculoParametro parametro : email.getParametroEmails()) {
                String valor = SenhaEmailBusiness.execute(parametro.getParametroEmail(), pessoa, pessoa, getUser());
                parametersValues.put(parametro.getParametroEmail().getSigla(), valor);
            }

            String textoFormatado = EmailTemplateBusiness.criarEmailTemplate(email.getAssunto(), email.getDescricao(), parametersValues);
            EmailCaixaSaida emailCaixaSaida = new EmailCaixaSaida(enderecoEmail, email.getAssunto(), textoFormatado, pessoa.getCodigoEmpresa());
            addDestinatarios(emailCaixaSaida, pessoa);

            if(!email.getNecessarioAprovacao()) {
                emailCaixaSaidaSessionBean.createSemAprovacao(emailCaixaSaida);
                exibirMensagem(FacesMessage.SEVERITY_INFO, "Email com recuperação da senha enviado...");
                return;
            }

            emailCaixaSaidaSessionBean.create(emailCaixaSaida);
            exibirMensagem(FacesMessage.SEVERITY_INFO, "Email de recuperação da senha aguardando aprovação do Administrador do sistema...");

        } catch (Exception e) {
            LOGGER.error(e.getCause(), e);
        }
    }

    private void addDestinatarios(EmailCaixaSaida caixaSaida, Pessoa pessoa) {
        for (PessoaEmail pessoaEmail : pessoa.getPessoasEmails()) {
            EmailDestinatario destinatario = new EmailDestinatario(caixaSaida, pessoaEmail.getEmail());
            caixaSaida.addEmails(destinatario);
        }
    }

    public String goToHome(){
        return "/pages/index.xhtml?faces-redirect=true";
    }

    public String goToRecuperarSenha(){
        return PAGES_LOGIN_RECUPERA_SENHA;
    }

    public String goToAlterarSenha(){
        return PAGES_LOGIN_ALTERA_SENHA;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenhaAtual() {
        return senhaAtual;
    }

    public void setSenhaAtual(String senhaAtual) {
        this.senhaAtual = senhaAtual;
    }

    public String getNovaSenha() {
        return novaSenha;
    }

    public void setNovaSenha(String novaSenha) {
        this.novaSenha = novaSenha;
    }

    public String getConfirmacaoSenha() {
        return confirmacaoSenha;
    }

    public void setConfirmacaoSenha(String confirmacaoSenha) {
        this.confirmacaoSenha = confirmacaoSenha;
    }

    public Long getEmpresaSelecionada() {
        return empresaSelecionada;
    }

    public void setEmpresaSelecionada(Long empresaSelecionada) {
        this.empresaSelecionada = empresaSelecionada;
    }

    public List<Empresa> getEmpresas() {
        return empresas;
    }

    public void setEmpresas(List<Empresa> empresas) {
        this.empresas = empresas;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public boolean isPessoaCadastradaEmMaisDeUmaEmpresa() {
        return pessoaCadastradaEmMaisDeUmaEmpresa;
    }

    public void setPessoaCadastradaEmMaisDeUmaEmpresa(boolean pessoaCadastradaEmMaisDeUmaEmpresa) {
        this.pessoaCadastradaEmMaisDeUmaEmpresa = pessoaCadastradaEmMaisDeUmaEmpresa;
    }

    public boolean isPessoaCadastradaEmMaisDeUmPerfil() {
        return pessoaCadastradaEmMaisDeUmPerfil;
    }

    public void setPessoaCadastradaEmMaisDeUmPerfil(boolean pessoaCadastradaEmMaisDeUmPerfil) {
        this.pessoaCadastradaEmMaisDeUmPerfil = pessoaCadastradaEmMaisDeUmPerfil;
    }

    public List<Perfil> getPerfis() {
        return perfis;
    }

    public Perfil getPerfilSelecionado() {
        return perfilSelecionado;
    }

    public void setPerfilSelecionado(Perfil perfilSelecionado) {
        this.perfilSelecionado = perfilSelecionado;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public StateManager getStateManager() {
        return stateManager;
    }

    public boolean isPermiteTrocarFilial() {
        return possuiAcesso(PERMITE_TROCAR_FILIAL);
    }
}
