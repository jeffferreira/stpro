package br.com.juridico.controller;

import br.com.juridico.datamodel.LazyEntityDataModel;
import br.com.juridico.entidades.Direito;
import br.com.juridico.entidades.MeioTramitacao;
import br.com.juridico.entidades.MeioTramitacaoNaturezaAcao;
import br.com.juridico.entidades.NaturezaAcao;
import br.com.juridico.exception.CrudException;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.impl.DireitoSessionBean;
import br.com.juridico.impl.MeioTramitacaoSessionBean;
import br.com.juridico.impl.NaturezaAcaoSessionBean;
import br.com.juridico.impl.PerfilAcessoSessionBean;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.primefaces.model.DualListModel;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author Jefferson
 *
 */
@ManagedBean
@SessionScoped
public class MeioTramitacaoController extends CrudController<MeioTramitacao> {

	/**
	 *
	 */
	private static final long serialVersionUID = 6933129545478297709L;
	private static final String MEIO_TRAMITACAO_FORM = "meioTramitacaoForm";
	private static final String CHAVE_ACESSO = "MEIOTRAMITACAO_MEIO_TRAMITACAO_TEMPLATE";
	private static final String PAGINA_FORMULARIO = "/pages/cadastros/meioTramitacao/meio-tramitacao-template.xhtml?faces-redirect=true";

	@Inject
	private MeioTramitacaoSessionBean meioTramitacaoSessionBean;
	@Inject
	private PerfilAcessoSessionBean perfilAcessoSessionBean;
	@Inject
	private DireitoSessionBean direitoSessionBean;
	@Inject
	private NaturezaAcaoSessionBean naturezaAcaoSessionBean;

	private DualListModel<NaturezaAcao> naturezas;
	private List<NaturezaAcao> paramsSource;
	private List<NaturezaAcao> paramsTarget;

	public void init(){
		carregaLazyDataModel();
	}

	private void initNaturezas() {
		this.paramsSource = naturezaAcaoSessionBean.retrieveAll(getStateManager().getEmpresaSelecionada().getId());
		this.paramsTarget = Lists.newArrayList();
		naturezas = new DualListModel<>(paramsSource, paramsTarget);
	}

	@Override
	protected void validar() throws ValidationException {
		meioTramitacaoSessionBean.clearSession();
		boolean jaExisteMeioTramitacao = meioTramitacaoSessionBean.isDescricaoJaCadastrada(getObjetoSelecionado(), getStateManager().getEmpresaSelecionada().getId());
		getObjetoSelecionado().validar(jaExisteMeioTramitacao);
	}

	@Override
	protected void antesDeSalvar() throws CrudException {
		naturezas.getTarget().stream().forEach(n -> adicionarNatureza(n));
		removerNaoAdicionados();
	}

	private void removerNaoAdicionados() {
		for(Iterator<MeioTramitacaoNaturezaAcao> i = getObjetoSelecionado().getMeioTramitacaoNaturezas().iterator(); i.hasNext();) {
			MeioTramitacaoNaturezaAcao item = i.next();
			if(naturezas.getTarget().stream().map(NaturezaAcao::getDescricao).noneMatch(n -> n.equals(item.getNaturezaAcao().getDescricao()))){
				i.remove();
			}
		}
	}

	private void adicionarNatureza(NaturezaAcao n) {
		if(isPermiteAdicionar(n)) {
			getObjetoSelecionado().addNaturezas(new MeioTramitacaoNaturezaAcao(getObjetoSelecionado(), n, getStateManager().getEmpresaSelecionada().getId()));
		}
	}

	private boolean isPermiteAdicionar(NaturezaAcao naturezaAcao) {
		return getObjetoSelecionado().getMeioTramitacaoNaturezas().stream()
				.map(MeioTramitacaoNaturezaAcao::getNaturezaAcao)
				.noneMatch(n -> n.equals(naturezaAcao));
	}

	@Override
	protected void antesDeEditar(MeioTramitacao objeto) throws CrudException {
		initNaturezas();
		this.paramsTarget.addAll(objeto.getMeioTramitacaoNaturezas().stream().map(MeioTramitacaoNaturezaAcao::getNaturezaAcao).collect(Collectors.toList()));
		for (MeioTramitacaoNaturezaAcao item : objeto.getMeioTramitacaoNaturezas()) {
			this.paramsSource.remove(item.getNaturezaAcao());
		}
		super.antesDeEditar(objeto);
	}

	@Override
	protected void create(MeioTramitacao meioTramitacao) throws ValidationException, CrudException {
		MeioTramitacao meioTramitacaoExisteNoBd = getRegistroComExclusaoLogica();

		if (isEditavel() && meioTramitacaoExisteNoBd != null) {
			getObjetoSelecionado().setDataExclusao(null);
			meioTramitacaoSessionBean.update(getObjetoSelecionado());
		} else if (meioTramitacao != null && !Strings.isNullOrEmpty(meioTramitacao.getDescricao())) {
			meioTramitacaoSessionBean.create(meioTramitacao);
		} else {
			throw new CrudException();
		}
	}

	@Override
	protected void carregaLazyDataModel() {
		setLazyDataModel(new LazyEntityDataModel<MeioTramitacao>() {
			@Override
			protected List<MeioTramitacao> consultaFiltrada(ConsultaLazyFilter filtro) {
				filtro.setDescricao(getDescricaoFiltro());
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return meioTramitacaoSessionBean.filtrados(filtro);
			}

			@Override
			protected int countTotalRegistros(ConsultaLazyFilter filtro) {
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return meioTramitacaoSessionBean.quantidadeFiltrados(filtro);
			}
		});
	}

	@Override
	protected void antesDeExcluir() throws CrudException {
		getObjetoSelecionado().setDataExclusao(new Date());
	}

	@Override
	protected void aposExcluir() throws CrudException {
		super.buscarPorLazy();
	}

	@Override
	protected void aposCancelar() throws CrudException {
		getObjetoSelecionado().setDescricao(null);
		updateContext(MEIO_TRAMITACAO_FORM);
		super.aposCancelar();
	}

	@Override
	protected MeioTramitacao inicializaObjetosLazy(MeioTramitacao objeto) {
		meioTramitacaoSessionBean.clearSession();
		return meioTramitacaoSessionBean.retrieve(objeto.getId());
	}

	@Override
	protected MeioTramitacao instanciarObjetoNovo() {
		return new MeioTramitacao();
	}

	@Override
	protected void setEmpresaDeUsuarioLogado(MeioTramitacao objetoSelecionado) {
		objetoSelecionado.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
	}

	public void geradorAutomaticoChanged(ValueChangeEvent event){
		getObjetoSelecionado().setGeradorAutomatico((Boolean) event.getNewValue());
		initNaturezas();
	}

	/**
	 *
	 * @return
	 */
	public String goToFormulario() {
		return PAGINA_FORMULARIO;
	}

	private MeioTramitacao getRegistroComExclusaoLogica() {
		return meioTramitacaoSessionBean.findByDescricao(getObjetoSelecionado().getDescricao(), getStateManager().getEmpresaSelecionada().getId());
	}

	@Override
	protected void addChaveAcesso() {
		setChaveAcesso(CHAVE_ACESSO);
	}

	@Override
	protected boolean isPodeAlterar(){
		Direito direito = direitoSessionBean.retrieve(ALTERAR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

	@Override
	protected boolean isPodeExcluir(){
		Direito direito = direitoSessionBean.retrieve(EXCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

	@Override
	protected boolean isPodeIncluir(){
		Direito direito = direitoSessionBean.retrieve(INCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

	public DualListModel<NaturezaAcao> getNaturezas() {
		return naturezas;
	}

	public void setNaturezas(DualListModel<NaturezaAcao> naturezas) {
		this.naturezas = naturezas;
	}
}
