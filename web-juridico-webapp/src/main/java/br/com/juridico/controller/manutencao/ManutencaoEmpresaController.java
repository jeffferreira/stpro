package br.com.juridico.controller.manutencao;

import br.com.juridico.controller.CrudController;
import br.com.juridico.dto.PerfilAcessoDto;
import br.com.juridico.entidades.*;
import br.com.juridico.enums.EmailIndicador;
import br.com.juridico.enums.MenuIndicador;
import br.com.juridico.exception.CrudException;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.impl.*;
import br.com.juridico.mail.MailBusiness;
import br.com.juridico.util.DecryptUtil;
import br.com.juridico.util.EncryptUtil;
import br.com.juridico.util.FileUtil;
import br.com.juridico.util.Util;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.servlet.http.Part;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * Created by jefferson on 22/10/2016.
 */
@ManagedBean
@ViewScoped
public class ManutencaoEmpresaController extends CrudController<Empresa> {

    private static final Logger LOGGER = Logger.getLogger(ManutencaoEmpresaController.class);
    private static final String BLOCK = "BLOCK";
    public static final String EMAIL_DESTINATARIO = "EMAIL_DESTINATARIO";
    public static final String TEMPO_CONSULTA_EMAIL = "TEMPO_CONSULTA_EMAIL";
    public static final Character PESSOA_FISICA = 'F';
    public static final String ADMIN = "ADMIN";
    public static final String CLIENTE = "CLIENTE";


    @Inject
    private EmpresaSessionBean empresaSessionBean;
    @Inject
    private ImagemSessionBean imagemSessionBean;
    @Inject
    private PerfilSessionBean perfilSessionBean;
    @Inject
    private ParametroSessionBean parametroSessionBean;
    @Inject
    private PosicaoSessionBean posicaoSessionBean;
    @Inject
    private UfSessionBean ufSessionBean;
    @Inject
    private PessoaFisicaSessionBean pessoaFisicaSessionBean;
    @Inject
    private AcessoSessionBean acessoSessionBean;
    @Inject
    private PerfilAcessoSessionBean perfilAcessoSessionBean;
    @Inject
    private DireitoPerfilSessionBean direitoPerfilSessionBean;
    @Inject
    private PessoaConfiguracaoSessionBean pessoaConfiguracaoSessionBean;
    @Inject
    private DireitoSessionBean direitoSessionBean;
    @Inject
    private StatusAgendaSessionBean statusAgendaSessionBean;
    @Inject
    private TipoAndamentoSessionBean tipoAndamentoSessionBean;
    @Inject
    private EmailSessionBean emailSessionBean;


    private Imagem imagem;
    private Part arquivo;
    private String liberacao;
    private boolean novaEmpresaSelected;
    private PessoaFisica pessoaFisica;
    private List<Uf> ufs;
    private String confEmail;
    private boolean empresaMatriz = Boolean.TRUE;

    @PostConstruct
    public void initialize(){
        initImagem();
        this.setUfs(ufSessionBean.findAll());
        this.pessoaFisica = new PessoaFisica();
        this.pessoaFisica.setPessoa(new Pessoa());
    }

    private void initImagem() {
        this.imagem = new Imagem();
    }

    public List<Empresa> getEmpresas(){
        return empresaSessionBean.findAllEmpresas();
    }

    @Override
    protected void aposCancelar() throws CrudException {
        getObjetoSelecionado().setDescricao(null);
        initImagem();
        updateContext("form");
        super.aposCancelar();
    }

    @Override
    protected void antesDeSalvar() throws CrudException {
        try {
            getObjetoSelecionado().setDataCadastro(new Date());
            if(!Strings.isNullOrEmpty(liberacao)){
                getObjetoSelecionado().setLiberacao(EncryptUtil.execute(liberacao));
            }
        } catch (Exception e) {
            LOGGER.error(e, e.getCause());
        }
    }

    @Override
    protected void create(Empresa empresa) throws ValidationException, CrudException {
        if (isEditavel()) {
            empresaSessionBean.update(empresa);
            return;
        }
        empresaSessionBean.create(empresa);
    }

    @Override
    protected void carregaLazyDataModel() {
        // carregaLazyDataModel
    }

    public void buscaImagemEmpresa(){
        if(getUser() == null){
            return;
        }
        Empresa empresa = empresaSessionBean.retrieve(getUser().getCodEmpresa());
        this.imagem = empresa.getImagem();
    }

    @Override
    protected Empresa inicializaObjetosLazy(Empresa objetoSelecionado) {
        empresaSessionBean.clearSession();
        return empresaSessionBean.retrieve(objetoSelecionado.getId());
    }

    @Override
    protected Empresa instanciarObjetoNovo() {
        return new Empresa();
    }



    @Override
    protected void antesDeEditar(Empresa objeto) throws CrudException {
        if(objeto.getImagem() != null) {
            this.imagem = objeto.getImagem();
        }
        try {
            setLiberacao(DecryptUtil.execute(getObjetoSelecionado().getLiberacao()));
        } catch (Exception e) {
            LOGGER.error(e, e.getCause());
        }
    }

    public void importar() {
        try {
            byte[] bytes = IOUtils.toByteArray(arquivo.getInputStream());
            String nomeArquivo = FileUtil.getFileName(arquivo);
            String extensao = nomeArquivo.substring(nomeArquivo.lastIndexOf("."), nomeArquivo.length());

            if(this.imagem.getId() == null) {
                this.imagem = new Imagem(bytes, nomeArquivo, extensao);
                imagemSessionBean.create(imagem);
                getObjetoSelecionado().setImagem(this.imagem);
            } else {
                this.imagem.setLogo(bytes);
                this.imagem.setNomeArquivo(nomeArquivo);
                this.imagem.setExtensaoArquivo(extensao);
                imagemSessionBean.update(imagem);
                getObjetoSelecionado().setImagem(this.imagem);

            }
        } catch (IOException e) {
            LOGGER.error(e, e.getCause());
        } catch (ValidationException e) {
            LOGGER.error(e, e.getCause());
        }
    }

    @Override
    protected void validar() throws ValidationException {

    }

    public String salvarNovoEmpresa(){
        try {
            antesDeSalvar();
            getObjetoSelecionado().validar();
            empresaSessionBean.create(getObjetoSelecionado());
            salvarStatusAgenda();
            salvarTipoAndamento();
            salvarPerfis();
            salvarParametros();
            salvarPosicao();
            salvarEmails();
            salvarPessoa();
            adicionaLoginSenha();
            pessoaFisicaSessionBean.update(this.pessoaFisica);
            adicionarAcessos();
            salvarConfiguracaoEmail();
            enviaEmailNovoUsuarioPessoa();
            getEmpresas();
            exibirMensagem(FacesMessage.SEVERITY_INFO, getMessage("msg_sucesso_salvar"));
            return goToEmpresas();

        } catch (ValidationException e) {
            for (String message : e.getMessages()) {
                exibirMensagem(FacesMessage.SEVERITY_ERROR, message);
            }
            return "";
        } catch (CrudException e) {
            LOGGER.error(e, e.getCause());
            return "";
        } catch (Exception e) {
            LOGGER.error(e, e.getCause());
            return "";
        }
    }

    private void salvarConfiguracaoEmail() throws ValidationException {
        ConfiguracaoEmail configuracaoEmail = new ConfiguracaoEmail(
                "mail.stpro.com.br",
                587,
                "naoresponda@stpro.com.br",
                "advogados2016",
                "SMTP",
                this.getConfEmail(),
                getObjetoSelecionado().getId());

        getObjetoSelecionado().setConfiguracaoEmail(configuracaoEmail);
    }

    private void salvarStatusAgenda() throws ValidationException {
        statusAgendaSessionBean.create(new StatusAgenda("Cumprido", "event-text-color-lightSlateGray", Boolean.FALSE, getObjetoSelecionado().getId()));
        statusAgendaSessionBean.create(new StatusAgenda("À cumprir", "event-text-color-green", Boolean.FALSE, getObjetoSelecionado().getId()));
        statusAgendaSessionBean.create(new StatusAgenda("Cancelado", "event-text-color-red", Boolean.FALSE, getObjetoSelecionado().getId()));
        statusAgendaSessionBean.create(new StatusAgenda("Rejeitado", "event-text-color-orange", Boolean.FALSE, getObjetoSelecionado().getId()));
    }

    private void salvarTipoAndamento() throws ValidationException {
        TipoAndamento tipoAudienciaConciliacao = new TipoAndamento("Audiência de Conciliação", Boolean.TRUE, 0L, getObjetoSelecionado().getId());
        TipoAndamento tipoAudienciaOitiva = new TipoAndamento("Audiência Oitiva", Boolean.TRUE, 0L, getObjetoSelecionado().getId());
        TipoAndamento tipoAudienciaInstrucao = new TipoAndamento("Audiência de Instrução", Boolean.TRUE, 0L, getObjetoSelecionado().getId());

        tipoAndamentoSessionBean.create(tipoAudienciaConciliacao);
        tipoAndamentoSessionBean.create(tipoAudienciaOitiva);
        tipoAndamentoSessionBean.create(tipoAudienciaInstrucao);
    }

    private void salvarEmails() throws ValidationException {
        emailSessionBean.create(new Email("SENHA_RECUPERACAO_SENHA", "Recuperar senha", ".", getObjetoSelecionado().getId(), Boolean.TRUE, Boolean.TRUE));
        emailSessionBean.create(new Email("SENHA_ALTERACAO_SENHA", "Alteração de senha", ".", getObjetoSelecionado().getId(), Boolean.TRUE, Boolean.TRUE));
        emailSessionBean.create(new Email("SENHA_NOVO_USUARIO", "Novo Usuário", ".", getObjetoSelecionado().getId(), Boolean.TRUE, Boolean.TRUE));
        emailSessionBean.create(new Email("AGENDA_ALTERACAO_AGENDA", "Alteração na agenda", ".", getObjetoSelecionado().getId(), Boolean.TRUE, Boolean.TRUE));
        emailSessionBean.create(new Email("AGENDA_ADICIONAR_EVENTO", "Alteração de status", ".", getObjetoSelecionado().getId(), Boolean.TRUE, Boolean.TRUE));
        emailSessionBean.create(new Email("PROCESSO_PRAZO_FINAL", "Prazo final do processo", ".", getObjetoSelecionado().getId(), Boolean.TRUE, Boolean.TRUE));
        emailSessionBean.create(new Email("PROCESSO_RESUMO_PROCESSO", "Resumo do processo", ".", getObjetoSelecionado().getId(), Boolean.TRUE, Boolean.TRUE));
        emailSessionBean.create(new Email("AGENDA_EXCLUIR_EVENTO", "Exclusão de evento", ".", getObjetoSelecionado().getId(), Boolean.TRUE, Boolean.TRUE));
        emailSessionBean.create(new Email("ANDAMENTO_NOVO", "Novo andamento", ".", getObjetoSelecionado().getId(), Boolean.TRUE, Boolean.TRUE));
        emailSessionBean.create(new Email("ANDAMENTO_ALTERACAO", "Andamento alterado", ".", getObjetoSelecionado().getId(), Boolean.TRUE, Boolean.TRUE));
        emailSessionBean.create(new Email("PRAZO_ANDAMENTO_VENCIDO", "Prazo do andamento vencido", ".", getObjetoSelecionado().getId(), Boolean.TRUE, Boolean.TRUE));
        emailSessionBean.create(new Email("ANDAMENTO_REJEITADO_CONTROLADORIA", "Rejeitado pela Controladoria", ".", getObjetoSelecionado().getId(), Boolean.TRUE, Boolean.TRUE));
    }

    private void salvarPosicao() throws ValidationException {
        posicaoSessionBean.create(new Posicao("Autor", getObjetoSelecionado().getId()));
        posicaoSessionBean.create(new Posicao("Réu", getObjetoSelecionado().getId()));
        posicaoSessionBean.create(new Posicao("Terceiro", getObjetoSelecionado().getId()));
    }

    private void salvarParametros() throws ValidationException {
        parametroSessionBean.create(new Parametro(EMAIL_DESTINATARIO, "pessoa.nome", getObjetoSelecionado().getId()));
        parametroSessionBean.create(new Parametro(TEMPO_CONSULTA_EMAIL, "120", getObjetoSelecionado().getId()));
    }

    private void salvarPerfis() throws ValidationException {
        Perfil perfilAdmin = new Perfil(ADMIN, "Administrador do Sistema", getObjetoSelecionado().getId(), Boolean.TRUE);
        Perfil perfilCliente = new Perfil(CLIENTE, "Cliente", getObjetoSelecionado().getId(), Boolean.FALSE);
        perfilSessionBean.create(perfilAdmin);
        perfilSessionBean.create(perfilCliente);
    }

    private void salvarPessoa() throws ValidationException {
        String cpf = pessoaFisica.getCpf().replaceAll("[.-]", "");
        pessoaFisica.setCodigoEmpresa(getObjetoSelecionado().getId());
        pessoaFisica.setCpf(cpf);
        pessoaFisica.setAdvogado(Boolean.TRUE);
        pessoaFisica.setAdvogadoInterno(Boolean.TRUE);
        pessoaFisica.getPessoa().setTipo(PESSOA_FISICA);
        pessoaFisica.getPessoa().setCliente(Boolean.FALSE);
        pessoaFisica.getPessoa().setAtivo(Boolean.TRUE);
        pessoaFisica.getPessoa().setDataCadastro(new Date());
        pessoaFisica.getPessoa().setCodigoEmpresa(getObjetoSelecionado().getId());
        pessoaFisica.getPessoa().addPessoasEmails(new PessoaEmail(pessoaFisica.getPessoa(), getConfEmail(), getObjetoSelecionado().getId()));

        salvarPerfilAdmin();

        pessoaFisicaSessionBean.create(pessoaFisica);
    }

    private void salvarPerfilAdmin() {
        Perfil perfilAdmin = getNovoPerfilAdmin();
        pessoaFisica.getPessoa().setPerfil(perfilAdmin);
    }

    private Perfil getNovoPerfilAdmin() {
        return perfilSessionBean.findBySigla(ADMIN, getObjetoSelecionado().getId());
    }

    private void adicionaLoginSenha() throws Exception {
        String usuario = pessoaFisica.getCpf().replaceAll("[.-]", "");
        byte[] senha = EncryptUtil.execute(Util.randomGenerateValue(10));
        createUpdateLogin(pessoaFisica, usuario, senha);
    }

    private void createUpdateLogin(PessoaFisica pessoaFisica, String usuario, byte[] senha){
        Pessoa pessoa = pessoaFisica.getPessoa();
        if(pessoa.getLogin() == null || pessoa.getLogin().getId() == null) {
            Login login = new Login(usuario, senha, getObjetoSelecionado().getId());
            this.pessoaFisica.getPessoa().setLogin(login);
        } else {
            pessoaFisica.getPessoa().getLogin().setLogin(usuario);
            pessoaFisica.getPessoa().getLogin().setSenha(senha);
        }
    }

    private void adicionarAcessos(){
        MenuIndicador[] menuIndicadores = MenuIndicador.values();
        for (MenuIndicador menuIndicador : menuIndicadores) {
            List<PerfilAcessoDto> perfilAcessoDtos = carregaListaAcessos(menuIndicador);
            alterarTodosAcessos(Boolean.TRUE, perfilAcessoDtos);
        }
    }

    private List<PerfilAcessoDto> carregaListaAcessos(MenuIndicador menuIndicador){
        List<PerfilAcessoDto> perfilAcessos = Lists.newArrayList();
        acessoSessionBean.clearSession();
        List<Acesso> acessos = acessoSessionBean.findByMenu(menuIndicador.getValue());

        PerfilAcesso perfilAcesso = new PerfilAcesso();

        Perfil perfilAdmin = getNovoPerfilAdmin();
        perfilAcesso.setPerfil(perfilAdmin);

        for (Acesso acesso : acessos) {
            PerfilAcesso pa = getPerfilAcesso(perfilAdmin, acesso);
            PerfilAcessoDto dto = new PerfilAcessoDto(acesso, pa);
            perfilAcessos.add(dto);
        }

        return perfilAcessos;
    }

    private PerfilAcesso getPerfilAcesso(Perfil perfilAdmin, Acesso acesso) {
        perfilAcessoSessionBean.clearSession();
        return perfilAcessoSessionBean.findByPerfilAndAcesso(perfilAdmin.getId(), acesso.getId(), getObjetoSelecionado().getId());
    }

    private void alterarTodosAcessos(boolean permitirAcesso, List<PerfilAcessoDto> perfilAcessosDtos) {
        try {
            for (PerfilAcessoDto perfilAcessoDto : perfilAcessosDtos) {
                List<DireitoPerfil> direitos = consultaDireitos(perfilAcessoDto);

                if (permitirAcesso) {
                    removeDireitos(direitos);
                    perfilAcessoSessionBean.delete(perfilAcessoDto.getPerfilAcesso());

                    PerfilAcesso novoPerfilAcesso = new PerfilAcesso(getNovoPerfilAdmin(), perfilAcessoDto.getAcesso(), getObjetoSelecionado().getId());
                    perfilAcessoSessionBean.create(novoPerfilAcesso);
                    perfilAcessoDto.setPerfilAcesso(novoPerfilAcesso);
                    adicionaTodosDireitos(novoPerfilAcesso);
                } else {
                    removeDireitos(direitos);
                    perfilAcessoSessionBean.delete(perfilAcessoDto.getPerfilAcesso());
                }
            }

        } catch (ValidationException e){
            LOGGER.error(e,e.getCause());
        }
    }

    private void removeDireitos(final List<DireitoPerfil> direitos){
        for (DireitoPerfil direito : direitos) {
            removePessoaConfiguracao(direito);
            this.direitoPerfilSessionBean.delete(direito);
        }
    }

    private void removePessoaConfiguracao(DireitoPerfil direito) {
        List<PessoaConfiguracao> pessoaConfiguracoes = pessoaConfiguracaoSessionBean.findByDireitoPerfil(direito.getId(), getObjetoSelecionado().getId());
        for (PessoaConfiguracao pessoaConfiguracao : pessoaConfiguracoes) {
            this.pessoaConfiguracaoSessionBean.delete(pessoaConfiguracao);
        }
    }

    private void adicionaTodosDireitos(PerfilAcesso perfilAcesso) {
        List<Direito> direitos = direitoSessionBean.findAll();
        try {
            for (Direito direito : direitos) {
                direitoPerfilSessionBean.create(new DireitoPerfil(direito, perfilAcesso, getObjetoSelecionado().getId()));
            }
        } catch (ValidationException e) {
            LOGGER.error(e,e.getCause());
        }
    }

    private List<DireitoPerfil> consultaDireitos(PerfilAcessoDto perfilCadastroAcesso) throws ValidationException {
        List<DireitoPerfil> direitos;

        if(perfilCadastroAcesso.getPerfilAcesso() == null) {
            PerfilAcesso perfilAcesso = new PerfilAcesso(getNovoPerfilAdmin(), perfilCadastroAcesso.getAcesso(), getObjetoSelecionado().getId());
            perfilAcessoSessionBean.create(perfilAcesso);
            perfilCadastroAcesso.setPerfilAcesso(perfilAcesso);
            direitos = direitoPerfilSessionBean.findByPerfilAcesso(perfilAcesso.getId(), getObjetoSelecionado().getId());
        } else {
            direitos = direitoPerfilSessionBean.findByPerfilAcesso(perfilCadastroAcesso.getPerfilAcesso().getId(), getObjetoSelecionado().getId());
        }
        return direitos;
    }

    private void enviaEmailNovoUsuarioPessoa() throws ValidationException {
        Email email = emailSessionBean.findBySigla(EmailIndicador.SENHA_NOVO_USUARIO.getValue(), getObjetoSelecionado().getId());
        ConfiguracaoEmail configuracaoEmail = getObjetoSelecionado().getConfiguracaoEmail();

        if(email == null || (email != null && !email.getAtivo())) {
            exibirMensagem(FacesMessage.SEVERITY_INFO, "Email para Novo Usuário não cadastrado ou inativo...");
            return;
        }

        String textoFormatado = criaDescricaoEmailNovoUsuario();


        try {
            MailBusiness.enviarEmail(configuracaoEmail, "Novo Usuário Empresa", textoFormatado, "suporte@stpro.com.br");
        } catch (MessagingException e) {
            LOGGER.error(e,e.getCause());
        }
    }

    private String criaDescricaoEmailNovoUsuario() {
        String senha = null;
        try {
            senha = DecryptUtil.execute(this.pessoaFisica.getPessoa().getLogin().getSenha());
        } catch (Exception e) {
            LOGGER.error(e, e.getCause());
        }

        String nomePessoa = this.pessoaFisica.getNomePessoa();
        String cpf = this.pessoaFisica.getCpf();
        String nomeEmpresa = getObjetoSelecionado().getRazaoSocial();
        String texto = "Nova Empresa e Usuário cadastrado: \n\n"
                .concat("Empresa: ").concat(nomeEmpresa).concat("\n")
                .concat("Pessoa: ").concat(nomePessoa).concat("\n")
                .concat("Login: ").concat(cpf).concat("\n")
                .concat("Senha: ").concat(senha);
        return texto;
    }

    public String goToCadastroNovaEmpresa(){
        this.novaEmpresaSelected = Boolean.TRUE;
        return "";
    }

    public String goToEmpresas(){
        this.novaEmpresaSelected = Boolean.FALSE;
        return "";
    }

    @Override
    protected void aposSalvar() throws CrudException {
        initImagem();
    }

    @Override
    protected void setEmpresaDeUsuarioLogado(Empresa objetoSelecionado) {
        // setEmpresaDeUsuarioLogado
    }

    @Override
    protected void addChaveAcesso() {
        // addChaveAcesso
    }

    @Override
    protected boolean isPodeAlterar() {
        return false;
    }

    @Override
    protected boolean isPodeExcluir() {
        return false;
    }

    @Override
    protected boolean isPodeIncluir() {
        return false;
    }

    public void empresaMatrizChanged(ValueChangeEvent event){
        setEmpresaMatriz((Boolean) event.getNewValue());
    }

    public Imagem getImagem() {
        return imagem;
    }

    public void setImagem(Imagem imagem) {
        this.imagem = imagem;
    }

    public Part getArquivo() {
        return arquivo;
    }

    public void setArquivo(Part arquivo) {
        this.arquivo = arquivo;
    }

    public String getLiberacao() {
        return liberacao;
    }

    public void setLiberacao(String liberacao) {
        this.liberacao = liberacao;
    }

    public boolean isNovaEmpresaSelected() {
        return novaEmpresaSelected;
    }

    public void setNovaEmpresaSelected(boolean novaEmpresaSelected) {
        this.novaEmpresaSelected = novaEmpresaSelected;
    }

    public PessoaFisica getPessoaFisica() {
        return pessoaFisica;
    }

    public void setPessoaFisica(PessoaFisica pessoaFisica) {
        this.pessoaFisica = pessoaFisica;
    }

    public List<Uf> getUfs() {
        return ufs;
    }

    public void setUfs(List<Uf> ufs) {
        this.ufs = ufs;
    }

    public boolean isAtivo(){
        String chave = null;
        if(getObjetoSelecionado().getLiberacao() == null) {
            return Boolean.TRUE;
        } else {
            try {
                chave = DecryptUtil.execute(getObjetoSelecionado().getLiberacao());
            } catch (Exception e) {
                LOGGER.error(e, e.getCause());
            }
        }
        return !BLOCK.equals(chave);
    }

    public String getConfEmail() {
        return confEmail;
    }

    public void setConfEmail(String confEmail) {
        this.confEmail = confEmail;
    }

    public boolean isEmpresaMatriz() {
        return empresaMatriz;
    }

    public void setEmpresaMatriz(boolean empresaMatriz) {
        this.empresaMatriz = empresaMatriz;
    }
}
