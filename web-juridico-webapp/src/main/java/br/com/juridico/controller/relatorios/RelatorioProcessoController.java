package br.com.juridico.controller.relatorios;

import br.com.juridico.builder.RelatorioFiltroBuilder;
import br.com.juridico.controller.AbstractController;
import br.com.juridico.entidades.RelatorioCriterioProcesso;
import br.com.juridico.entidades.RelatorioFiltroProcesso;
import br.com.juridico.entidades.ViewProcesso;
import br.com.juridico.enums.RelatorioCriterioType;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.impl.RelatorioCriterioProcessoSessionBean;
import br.com.juridico.impl.RelatorioProcessoFiltroSessionBean;
import br.com.juridico.report.ProcessoPdfFileReport;
import br.com.juridico.util.DownloadUtil;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

@ManagedBean
@ViewScoped
public class RelatorioProcessoController extends AbstractController implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -8904567556191395015L;

	private static final Logger LOGGER = Logger.getLogger(RelatorioProcessoController.class);
	private static final String CHAVE_ACESSO = "FILTROS_REPORT_FILTER_FORM";
	private static final String TEXT = "text";
	private static final String PERIODO_DATA = "periodoData";
	private static final String PERIODO_VALOR = "periodoValor";

	@Inject
	private RelatorioProcessoFiltroSessionBean relatorioProcessoFiltroSessionBean;
	@Inject
	private RelatorioCriterioProcessoSessionBean relatorioCriterioProcessoSessionBean;

	private RelatorioFiltroProcesso relatorioSelecionado;
	private RelatorioFiltroProcesso relatorioSelecionadoNoCombo;
	private RelatorioFiltroBuilder filtroBuilder;

	private List<RelatorioFiltroProcesso> relatorios = Lists.newArrayList();
	private List<RelatorioCriterioProcesso> criteriosAtivos = Lists.newArrayList();
	private boolean landscape;

	/**
	 * @PostConstruct
	 */
	@PostConstruct
	public void initialize() {
		this.filtroBuilder = new RelatorioFiltroBuilder(getUser().getCodEmpresa());
		this.relatorioSelecionado = filtroBuilder.build();
		this.relatorios = relatorioProcessoFiltroSessionBean.retrieveAll(getUser().getCodEmpresa());
	}

	public void salvarRelatorioFiltro(){
		try {
			validar();
			relatorioProcessoFiltroSessionBean.create(relatorioSelecionado);
			this.relatorioSelecionado = filtroBuilder.build();
			this.relatorioSelecionadoNoCombo = this.relatorioSelecionado;
			this.relatorios = relatorioProcessoFiltroSessionBean.retrieveAll(getUser().getCodEmpresa());
			cancelar();
			exibirMensagem(FacesMessage.SEVERITY_INFO, getMessage("msg_sucesso_salvar"));
		} catch (ValidationException e) {
			for (String message : e.getMessages()) {
				exibirMensagem(FacesMessage.SEVERITY_ERROR, message);
			}
		}
	}

	private void validar() throws ValidationException {
		this.relatorioSelecionado.validar();
	}

	public List<ViewProcesso> pesquisar() {
		return relatorioProcessoFiltroSessionBean.findByCriterios(getStateManager().getEmpresaSelecionada().getId(), this.criteriosAtivos);
	}

	public void gerarRelatorioPdf() throws Exception {
		List<String> mensagens = validarCriteriosAtivos();
		if (!mensagens.isEmpty()) {
			mensagens.stream().forEach(m -> exibirMensagem(FacesMessage.SEVERITY_ERROR, m));
			return;
		}
		List<ViewProcesso> lista = pesquisar();
		Map<String, String> parameters = Maps.newHashMap();
		parameters.put("USUARIO", getUser().getName());

		ProcessoPdfFileReport report = new ProcessoPdfFileReport(relatorioSelecionadoNoCombo.getCriterios());
		byte[] relatorio = report.generateFile(lista, relatorioSelecionadoNoCombo.getDescricao(), this.landscape, parameters);

		DownloadUtil.downloadFile(relatorio, "Relatorio de processo.pdf");
	}

	public List<String> validarCriteriosAtivos() {
		List<String> mensagens = Lists.newArrayList();
		criteriosAtivos.stream().forEach(c -> {
			if(c.getFiltroObrigatorio()){
				validaCampoFiltro(c, mensagens);
			}
		});
		return mensagens;
	}

	public void validaCampoFiltro(RelatorioCriterioProcesso filtro, List<String> mensagens) {
		if(TEXT.equals(filtro.getTipoCampo()) && Strings.isNullOrEmpty(filtro.getValorString())) {
			adicionaMensagemValicao(filtro, mensagens);
		} else if(PERIODO_DATA.equals(filtro.getTipoCampo()) && (filtro.getValorPeriodoUm() == null && filtro.getValorPeriodoDois() == null)) {
			adicionaMensagemValicao(filtro, mensagens);
		} else if(PERIODO_VALOR.equals(filtro.getTipoCampo()) && (filtro.getValorPeriodoNumberUm() == null && filtro.getValorPeriodoNumberDois() == null)) {
			adicionaMensagemValicao(filtro, mensagens);
		}
	}

	private void adicionaMensagemValicao(RelatorioCriterioProcesso filtro, List<String> mensagens) {
		mensagens.add("Campo "+filtro.getDescricao()+" \u00e9 de preenchimento obrigat\u00f3rio.");
	}

	public void selecionaRelatorio(){
		if(this.relatorioSelecionadoNoCombo == null) {
			cancelar();
			return;
		}
		carregarCriteriosAtivos();
		this.relatorioSelecionado = this.relatorioSelecionadoNoCombo;
	}

	private void carregarCriteriosAtivos() {
		if (relatorioSelecionado == null) {
			return;
		}
		relatorioCriterioProcessoSessionBean.clearSession();
		setCriteriosAtivos(relatorioCriterioProcessoSessionBean.findCriteriosAtivosByIdRelatorio(relatorioSelecionadoNoCombo.getId()));
	}

	public boolean isHasAccess(){
		return possuiAcesso(CHAVE_ACESSO);
	}

	public List<SelectItem> getCriterioTypes(){
		return transformToSelectItem(RelatorioCriterioType.class);
	}

	public RelatorioFiltroProcesso getRelatorioSelecionado() {
		return relatorioSelecionado;
	}

	public void setRelatorioSelecionado(RelatorioFiltroProcesso relatorioSelecionado) {
		this.relatorioSelecionado = relatorioSelecionado;
	}

	public List<RelatorioFiltroProcesso> getRelatorios() {
		return relatorios;
	}

	public void setRelatorios(List<RelatorioFiltroProcesso> relatorios) {
		this.relatorios = relatorios;
	}

	public RelatorioFiltroProcesso getRelatorioSelecionadoNoCombo() {
		return relatorioSelecionadoNoCombo;
	}

	public void setRelatorioSelecionadoNoCombo(RelatorioFiltroProcesso relatorioSelecionadoNoCombo) {
		this.relatorioSelecionadoNoCombo = relatorioSelecionadoNoCombo;
	}

	public void cancelar() {
		this.filtroBuilder = new RelatorioFiltroBuilder(getUser().getCodEmpresa());
		this.relatorioSelecionado = filtroBuilder.build();
		this.relatorioSelecionadoNoCombo = this.relatorioSelecionado;
		this.relatorios = relatorioProcessoFiltroSessionBean.retrieveAll(getUser().getCodEmpresa());
		RequestContext.getCurrentInstance().execute("PF('accordion').unselect(0)");

	}

	public boolean isDesabilitaBotaoRelatorio(){
		return relatorioSelecionadoNoCombo == null || relatorioSelecionadoNoCombo.getId() == null;
	}

	public List<RelatorioCriterioProcesso> getCriteriosAtivos() {
		return criteriosAtivos;
	}

	public void setCriteriosAtivos(List<RelatorioCriterioProcesso> criteriosAtivos) {
		this.criteriosAtivos = criteriosAtivos;
	}

	public void atualizaRowCriterio(RelatorioCriterioProcesso criterio) {
		if (!criterio.getFiltroAtivo()) {
			criterio.setFiltroObrigatorio(Boolean.FALSE);
			criterio.setCriterioType(RelatorioCriterioType.E);
		}
	}

	public boolean isHabilitaExclusao(){
		return relatorioSelecionadoNoCombo != null && relatorioSelecionadoNoCombo.getId() != null;
	}

	public void excluir(){
		this.relatorioSelecionadoNoCombo.setDataExclusao(new Date());
		try {
			relatorioProcessoFiltroSessionBean.update(this.relatorioSelecionadoNoCombo);
			cancelar();
			exibirMensagem(FacesMessage.SEVERITY_INFO, getMessage("msg_sucesso_salvar"));
			RequestContext.getCurrentInstance().update("@form");
		} catch (ValidationException e) {
			for (String message : e.getMessages()) {
				exibirMensagem(FacesMessage.SEVERITY_ERROR, message);
			}
		}
	}

	public boolean isLandscape() {
		return landscape;
	}

	public void setLandscape(boolean landscape) {
		this.landscape = landscape;
	}
}
