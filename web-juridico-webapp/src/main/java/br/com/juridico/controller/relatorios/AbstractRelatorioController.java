package br.com.juridico.controller.relatorios;

import br.com.juridico.controller.AbstractController;
import br.com.juridico.dao.IEntity;
import br.com.juridico.entidades.Direito;
import br.com.juridico.exception.StproException;
import br.com.juridico.impl.DireitoSessionBean;
import br.com.juridico.impl.PerfilAcessoSessionBean;
import br.com.juridico.util.ExportUtil;
import org.apache.log4j.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.ServletContext;
import java.io.File;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

public abstract class AbstractRelatorioController<T extends IEntity> extends AbstractController implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -8564347918825570635L;
	private static final Logger LOGGER = Logger.getLogger(AbstractRelatorioController.class);

	private FacesContext context = FacesContext.getCurrentInstance();
	private ServletContext servletContext = (ServletContext) context.getExternalContext().getContext();
	private String chaveAcesso;
	private Map<String, Object> parameters;

	@Inject
	private PerfilAcessoSessionBean perfilAcessoSessionBean;
	@Inject
	private DireitoSessionBean direitoSessionBean;

	protected void gerarRelatorioPdfComSubReports(String[] relatorio){
		addParameters();
		String path = servletContext.getRealPath("resources" + File.separator + "reports" + File.separator);

		if(!validouFiltros()){
			exibirMensagem(FacesMessage.SEVERITY_ERROR, getMessage("msg_required_filter"));
			return;
		}

		List<T> lista = pesquisar();

		if (lista.isEmpty()) {
			exibirMensagem(FacesMessage.SEVERITY_WARN, getMessage("msg_nenhum_registro"));
			return;
		}

		try {
			ExportUtil.gerarPdfComSubreport(relatorio, lista, path, nomeRelatorio(), parameters);
		} catch (StproException e) {
			LOGGER.error(e, e.getCause());
		}
	}

	/**
	 *
	 */
	protected void gerarRelatorioPdf() {
		addParameters();
		String path = servletContext.getRealPath("resources" + File.separator + "reports" + File.separator);

		if(!validouFiltros()){
			exibirMensagem(FacesMessage.SEVERITY_ERROR, getMessage("msg_required_filter"));
			return;
		}

		List<T> lista = pesquisar();

		if (lista.isEmpty()) {
			exibirMensagem(FacesMessage.SEVERITY_WARN, getMessage("msg_nenhum_registro"));
			return;
		}

		try {
			ExportUtil.exportarRelatorioPdf(nomeRelatorioJrxml(), nomeRelatorio(), lista, path, parameters);
		} catch (StproException e) {
			LOGGER.error(e, e.getCause());
		}
	}

	public boolean isHasAccess(){
		addChaveAcesso();
		return possuiAcesso(this.chaveAcesso);
	}

	public abstract String nomeRelatorioJrxml();
	public abstract String nomeRelatorio();
	public abstract void addParameters();
	protected abstract void addChaveAcesso();
	public abstract List<T> pesquisar();
	public abstract boolean validouFiltros();
	public abstract boolean getPermiteVisualizar();


	public void setChaveAcesso(String chaveAcesso) {
		this.chaveAcesso = chaveAcesso;
	}

	public void setParameters(Map<String, Object> parameters) {
		this.parameters = parameters;
	}

	protected boolean isPodeVisualizar(String tela){
		Direito direito = direitoSessionBean.retrieve(CONSULTAR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), tela, direito, getUser().getCodEmpresa());
	}
}
