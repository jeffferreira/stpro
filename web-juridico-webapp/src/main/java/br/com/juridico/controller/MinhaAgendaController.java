package br.com.juridico.controller;

import br.com.juridico.business.AgendaEmailBusiness;
import br.com.juridico.business.EmailTemplateBusiness;
import br.com.juridico.business.TipoAndamentoBusiness;
import br.com.juridico.entidades.*;
import br.com.juridico.enums.ControladoriaIndicador;
import br.com.juridico.enums.EventoColorTypes;
import br.com.juridico.enums.TipoOperacaoIndicador;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.impl.*;
import br.com.juridico.state.ControladoriaState;
import br.com.juridico.state.controladoria.AprovaControladoria;
import br.com.juridico.state.controladoria.ControladoriaContext;
import br.com.juridico.util.DateUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.log4j.Logger;
import org.primefaces.PrimeFaces;
import org.primefaces.context.RequestContext;
import org.primefaces.event.ScheduleEntryMoveEvent;
import org.primefaces.event.ScheduleEntryResizeEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.map.GeocodeEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.map.*;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import java.io.Serializable;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Jefferson
 *
 */
@ManagedBean
@SessionScoped
public class MinhaAgendaController extends AbstractAgendaController implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -5363621670335559115L;

	private static final Logger LOGGER = Logger.getLogger(MinhaAgendaController.class);


	private static final String STATUS_SELECT_FLASH = "STATUS_SELECT_FLASH";
	private static final String TOGGLE_SELECT_FLASH = "TOGGLE_SELECT_FLASH";
	private static final String A_CUMPRIR = "\u00c0 cumprir";
	private static final String CUMPRIDO = "Cumprido";
	private static final String CHAVE_ACESSO = "UNICA_MINHA_AGENDA_FORM";
	private static final String TRANSFERIR_AGENDA = "TRANSFERIR_AGENDA";
	private static final String PROCESSOS_PESQUISA_TODOS = "PROCESSOS_PESQUISA_TODOS";
	private static final String CONTROLADORIA_LIST_CONTROLADORIA = "CONTROLADORIA_LIST_CONTROLADORIA";
	private static final String PAGINA_FORMULARIO = "/pages/agenda/unica/minha-agenda-form.xhtml?faces-redirect=true";


	@Inject
	private EventoSessionBean eventoSessionBean;
	@Inject
	private PessoaSessionBean pessoaSessionBean;
	@Inject
	private PessoaFisicaSessionBean pessoaFisicaSessionBean;
	@Inject
	private EmailCaixaSaidaSessionBean emailCaixaSaidaSessionBean;
	@Inject
	private EmailSessionBean emailSessionBean;
	@Inject
	private StatusAgendaSessionBean statusAgendaSessionBean;
	@Inject
	private EmpresaSessionBean empresaSessionBean;
	@Inject
	private ProcessoSessionBean processoSessionBean;
	@Inject
	private ParteSessionBean parteSessionBean;
	@Inject
	private AndamentoSessionBean andamentoSessionBean;

	private ScheduleEvent event = new DefaultScheduleEvent();
	private Evento eventoSelecionado = new Evento();
	private Evento eventoAnterior;
	private PessoaFisica pessoaFisicaSelecionada;
	private Pessoa pessoaSelecionada;
	private StatusAgenda statusAgendaSelecionado;
	private Processo processoSelecionado;
	private Pessoa advogadoSelecionado;
	private Empresa empresa;

	private MapModel geoModel;
	private String centerGeoMap;

	private Date dataSelect;
	private Date initialDate;
	private String viewType;
	private String horaInicio;
	private String horaFim;
	private String localizacao;
	private String eventoColorSelecionado;

	@PostConstruct
	public void init() {
		setInitialDate(new Date());
		setStyleClassSelect(EventoColorTypes.BLUE.getValue());
		setViewType(MONTH_TYPE);
		carregaAgenda();
		initCenterGeoMap();
		setResponsavel();
		this.empresa = empresaSessionBean.retrieve(getStateManager().getEmpresaSelecionada().getId());
		toggleLeftMenu();
	}

	public void toggleLeftMenu() {
		Boolean toggleSelectFlash = (Boolean) getFlashScope().get(TOGGLE_SELECT_FLASH);
		if(toggleSelectFlash != null && toggleSelectFlash){
			PrimeFaces.current().executeScript("document.getElementById('btnToogleMenu').click()");
		}
	}

	private void setResponsavel() {
		Pessoa pessoaLogada = pessoaSessionBean.retrieve(getUser().getId());
		this.pessoaSelecionada = pessoaLogada;
	}

	private void initCenterGeoMap() {
		this.geoModel = new DefaultMapModel();
		Empresa empresa = empresaSessionBean.retrieve(getStateManager().getEmpresaSelecionada().getId());
		this.centerGeoMap = empresa.getLatitudeLongitudeCentro();
	}

	public List<Processo> completeProcesso(String query) {
		List<BigInteger> processoIds = Lists.newArrayList();
		if(advogadoSelecionado != null) {
			processoIds = parteSessionBean.findProcessoIdByPessoa(advogadoSelecionado.getId(), getStateManager().getEmpresaSelecionada().getId());
			if(processoIds.isEmpty()) {
				return Lists.newArrayList();
			}
		}
		return processoSessionBean.findAutoCompleteProcesso(processoIds, query, getStateManager().getEmpresaSelecionada().getId());
	}

	public void advogadoChanged(ValueChangeEvent event){
		this.advogadoSelecionado = (Pessoa) event.getNewValue();
		updateContext("processoAutoCompleteId");
	}

	public String criarNovaAgenda() {
		criarAgenda = Boolean.TRUE;
		event = new DefaultScheduleEvent("", new Date(), new Date());
		if(!isHasAccessPesquisarTodos()){
			Pessoa pessoa = pessoaSessionBean.retrieve(getUser().getId());
			this.advogadoSelecionado = pessoa;
		}
		return "";
	}

	private void carregaAgenda() {
		carregaLazyEventModel();
	}

	public void selectAgenda(StatusAgenda statusAgenda) {
		statusAgendaSelecionado = statusAgenda;
		getFlashScope().put(STATUS_SELECT_FLASH, statusAgendaSelecionado);
	}

	@Override
	protected List<Evento> listaDeEventos() {
		return carregaEventos(getDataInicioAgenda(), getDataFimAgenda());
	}

	private List<Evento> carregaEventos(Date start, Date end) {
		Pessoa pessoa = pessoaSessionBean.retrieve(getUser().getId());

		if (statusAgendaSelecionado != null) {
			return eventoSessionBean.findUserStatusEvents(statusAgendaSelecionado, pessoa, start, end, getStateManager().getEmpresaSelecionada().getId());
		}
		return eventoSessionBean.findUserEvents(pessoa, start, end, getStateManager().getEmpresaSelecionada().getId());
	}

	public void onDateCalendarSelect(SelectEvent event) {
		setInitialDate((Date) event.getObject());
		setViewType(AGENDA_DAY_TYPE);
	}

	public void calendarSelectPorEvento(){
		setInitialDate(getDataSelect());
		setViewType(AGENDA_DAY_TYPE);
	}

	public void onEventSelect(SelectEvent selectEvent) {
		event = (ScheduleEvent) selectEvent.getObject();
		setEventoSelecionado(eventoSessionBean.findByChaveId((String) event.getData()));
		RequestContext.getCurrentInstance().execute("PF('eventDialog').show();");
	}

	public void onEventMove(ScheduleEntryMoveEvent event) {
		LOGGER.info("Moveu evento >> Data:"+ event.getDayDelta()+ " Minuto: "+event.getMinuteDelta());
	}

	public void onEventResize(ScheduleEntryResizeEvent event) {
		LOGGER.info("Resize evento >> Data:"+ event.getDayDelta()+ " Minuto: "+event.getMinuteDelta());
	}

	public void addEvent(ActionEvent actionEvent) {
		try {
			atualizarEvento();
			RequestContext.getCurrentInstance().execute("PF('myschedule').update(); PF('eventDialog').hide();");
		} catch (ValidationException e) {
			LOGGER.error(e, e.getCause());
		}
	}

	public void excluir() throws ValidationException {
		getLazyEventModel().updateEvent(event);
		if (getEventoSelecionado() != null) {
			eventoSessionBean.clearSession();
			eventoSessionBean.delete(getEventoSelecionado());
		}
		enviarEmailRemocao(getEventoSelecionado());
	}

	private void carregarEventoAnterior() throws ValidationException {
		Evento evento = new Evento();
		eventoSessionBean.clearSession();
		if (this.eventoSelecionado != null && this.eventoSelecionado.getId() != null) {
			evento = eventoSessionBean.retrieve(this.eventoSelecionado.getId());
		}
		this.eventoAnterior = evento;
		eventoSessionBean.clearSession();
	}

	private void createEvento(Date dataInicio, Date dataFim, StatusAgenda statusAgenda) throws ValidationException {
		getLazyEventModel().addEvent(event);
		Pessoa pessoa = pessoaSelecionada;
		Evento evento = new Evento(event.getId(), event.getTitle(),statusAgenda, dataInicio, dataFim, getStyleClassSelect(), pessoa, empresa);
		evento.setNomeIniciais(pessoa.getNomeIniciais());
		evento.setHorarioIndefinido(event.isAllDay());
		evento.setLocalizacao(this.localizacao);
		evento.setObservacao(event.getDescription());
		evento.setProcesso(processoSelecionado);
		HistoricoEvento historico = new HistoricoEvento(evento, getUser().getName());
		historico.setTipoOperacao(TipoOperacaoIndicador.INC);
		evento.addHistorico(historico);
		carregarEventoAnterior();
		adicionaControladoria(Boolean.FALSE, evento);
		eventoSessionBean.create(evento);
		enviarEmailCriacao(evento);
	}

	@Override
	protected void updateEvento() {
		try {
			if(getEventoSelecionado().getPessoa() != null) {
				getEventoSelecionado().setNomeIniciais(getEventoSelecionado().getPessoa().getNomeIniciais());
			}
			if(getEventoSelecionado().getAndamento() != null){
				getEventoSelecionado().getAndamento().setStatusAgenda(getEventoSelecionado().getStatusAgenda());
			}

			adicionaControladoria(Boolean.TRUE, this.eventoSelecionado);
			HistoricoEvento historico = new HistoricoEvento(getEventoSelecionado(), getUser().getName());
			historico.setTipoOperacao(TipoOperacaoIndicador.ALT);
			getEventoSelecionado().addHistorico(historico);
			eventoSessionBean.update(getEventoSelecionado());
			enviarEmailAlteracao();
		} catch (ValidationException e) {
			LOGGER.error(e, e.getCause());
		}
	}

	private void adicionaControladoria(boolean edicao, Evento evento) {
		boolean possuiConfControladoria = getUser().getEmpresa() == null ? Boolean.FALSE : getUser().getEmpresa().isConfiguracaoControladoria();
		ControladoriaContext context = new ControladoriaContext();
		ControladoriaState aprovacaoState = new AprovaControladoria();
		context.setState(aprovacaoState);

		evento.setControladoriaStatus(context.controladoriaAction(
				edicao, evento.getStatusAgenda(),
				possuiConfControladoria,
				ControladoriaIndicador.fromValue(evento.getControladoriaStatus())).getStatus());
	}

	private void atualizarEvento() throws ValidationException {
		super.atualizarEvento(getEventoSelecionado(), this.event);
		setEventoSelecionado(null);
	}

	private void enviarEmailCriacao(Evento evento){
		Email email = emailSessionBean.findBySigla(AGENDA_ADICIONAR_EVENTO, getStateManager().getEmpresaSelecionada().getId());
		Map<String, String> parametersValues = Maps.newHashMap();

		if (email == null) {
			exibirMensagem(FacesMessage.SEVERITY_INFO, "Email para Criação de evento não cadastrado...");
			return;
		}

		for (EmailVinculoParametro parametro : email.getParametroEmails()) {
			String valor = AgendaEmailBusiness.execute(parametro.getParametroEmail(), evento, evento, getUser());
			parametersValues.put(parametro.getParametroEmail().getSigla(), valor);
		}

		String textoFormatado = EmailTemplateBusiness.criarEmailTemplate(email.getAssunto(), email.getDescricao(), parametersValues);
		enviarEmail(email, textoFormatado);
	}

	@Override
	protected boolean enviarEmail(Email email, String textoFormatado) {
		try {
			EmailCaixaSaida emailCaixaSaida = new EmailCaixaSaida(getUser().getName(), email.getAssunto(), textoFormatado, getStateManager().getEmpresaSelecionada().getId());
			addDestinatarios(emailCaixaSaida);
			sendEmailCaixaSaida(email, emailCaixaSaida);
			return true;

		} catch (ValidationException e) {
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	private void enviarEmailAlteracao(){
		Email email = emailSessionBean.findBySigla(AGENDA_ALTERACAO_AGENDA, getStateManager().getEmpresaSelecionada().getId());
		Map<String, String> parametersValues = Maps.newHashMap();

		if (email == null) {
			exibirMensagem(FacesMessage.SEVERITY_INFO, "Email para Alteração de agenda não cadastrado...");
			return;
		}

		for (EmailVinculoParametro parametro : email.getParametroEmails()) {
			String valor = AgendaEmailBusiness.execute(parametro.getParametroEmail(), (Evento) getSessionAttribute(EVENTO_SELECIONADO_SESSION), getEventoSelecionado(), getUser());
			parametersValues.put(parametro.getParametroEmail().getSigla(), valor);
		}

		String textoFormatado = EmailTemplateBusiness.criarEmailTemplate(email.getAssunto(), email.getDescricao(), parametersValues);
		enviarEmail(email, textoFormatado);
		removeSessionAttribute(EVENTO_SELECIONADO_SESSION);
	}

	private void enviarEmailRemocao(Evento evento){
		Email email = emailSessionBean.findBySigla(AGENDA_EXCLUIR_EVENTO, getStateManager().getEmpresaSelecionada().getId());
		Map<String, String> parametersValues = Maps.newHashMap();

		if (email == null) {
			exibirMensagem(FacesMessage.SEVERITY_INFO, "Email para Exclusão de evento não cadastrado...");
			return;
		}

		for (EmailVinculoParametro parametro : email.getParametroEmails()) {
			String valor = AgendaEmailBusiness.execute(parametro.getParametroEmail(), evento, new Evento(), getUser());
			parametersValues.put(parametro.getParametroEmail().getSigla(), valor);
		}

		String textoFormatado = EmailTemplateBusiness.criarEmailTemplate(email.getAssunto(), email.getDescricao(), parametersValues);
		enviarEmail(email, textoFormatado);
	}

	private void sendEmailCaixaSaida(Email email, EmailCaixaSaida emailCaixaSaida) throws ValidationException {
		if(!email.getNecessarioAprovacao()){
			emailCaixaSaidaSessionBean.createSemAprovacao(emailCaixaSaida);
		} else {
			emailCaixaSaidaSessionBean.create(emailCaixaSaida);
		}
	}

	private void addDestinatarios(EmailCaixaSaida caixaSaida) {
		List<Pessoa> administradores = pessoaSessionBean.findByAdministradores(getStateManager().getEmpresaSelecionada().getId());
		super.addDestinatarios(caixaSaida, administradores);
	}

	public String goToMinhaAgenda() {
		criarAgenda = Boolean.FALSE;
		getFlashScope().put(TOGGLE_SELECT_FLASH, Boolean.TRUE);
		return PAGINA_FORMULARIO;
	}

	public String salvarEvento() throws ValidationException {
		Calendar inicio = Calendar.getInstance();
		inicio.setTime(event.getStartDate());
		inicio.set(Calendar.HOUR_OF_DAY, getHora(horaInicio));
		inicio.set(Calendar.MINUTE, getMinuto(horaInicio));

		Calendar fim = Calendar.getInstance();
		fim.setTime(event.getEndDate());
		fim.set(Calendar.HOUR_OF_DAY, getHora(horaFim));
		fim.set(Calendar.MINUTE, getMinuto(horaFim));

		StatusAgenda statusAgenda = statusAgendaSessionBean.findByDescricao(A_CUMPRIR, getStateManager().getEmpresaSelecionada().getId());
		EventoColorTypes colorType = EventoColorTypes.fromTextColor(statusAgenda.getStyleColor());
		setStyleClassSelect(colorType == null ? null : colorType.getValue());
		createEvento(inicio.getTime(), fim.getTime(), statusAgenda);
		return goToMinhaAgenda();
	}

	public void horaInicioChanged(ValueChangeEvent event) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		String hora = (String) event.getNewValue();
		Date dataFimComUmaHoraMais = DateUtils.somaHoras(sdf.parse(hora), 1);
		this.horaFim = DateUtils.formataHoraBrasil(dataFimComUmaHoraMais);
	}

	public void transferenciaChanged(ValueChangeEvent event) {
		boolean transferir = (boolean) event.getNewValue();
		if (!transferir) {
			Pessoa pessoa = pessoaSessionBean.retrieve(getUser().getId());
			this.eventoSelecionado.setPessoa(pessoa);
		}
	}

	public void semHorarioChanged(ValueChangeEvent event) {
		boolean checked = (boolean) event.getNewValue();
		if (checked) {
			this.horaInicio = "";
			this.horaFim = "";
		}
	}

	public void onGeocode(GeocodeEvent event) {
		List<GeocodeResult> results = event.getResults();

		if (results != null && !results.isEmpty()) {
			LatLng center = results.get(0).getLatLng();
			centerGeoMap = center.getLat() + "," + center.getLng();

			for (int i = 0; i < results.size(); i++) {
				GeocodeResult result = results.get(i);
				geoModel.addOverlay(new Marker(result.getLatLng(), result.getAddress()));
			}
		}
	}

	public void statusChanged(ValueChangeEvent event){
		StatusAgenda statusAgenda = (StatusAgenda) event.getNewValue();
		this.eventoSelecionado.setStatusAgenda(statusAgenda);
	}

	private StatusAgenda getStatusAgendaCumprido() {
		return statusAgendaSessionBean.findByDescricao(CUMPRIDO, getStateManager().getEmpresaSelecionada().getId());
	}

	public boolean isRenderizaHouveAcordoAndamento() {
		if(eventoSelecionado == null || eventoSelecionado.getAndamento() == null) {
			return false;
		}
		StatusAgenda prazoCumprido = getStatusAgendaCumprido();
		boolean prazoAtendido = eventoSelecionado.getAndamento().getStatusAgenda() != null
				&& prazoCumprido != null
				&& prazoCumprido.getId().equals(eventoSelecionado.getStatusAgenda().getId());

		return prazoAtendido && (TipoAndamentoBusiness.isAudienciaConciliacao(eventoSelecionado.getAndamento().getTipoAndamento())
				|| TipoAndamentoBusiness.isAudienciaInstrucao(eventoSelecionado.getAndamento().getTipoAndamento())
				|| TipoAndamentoBusiness.isAudienciaOitiva(eventoSelecionado.getAndamento().getTipoAndamento()));
	}

	public List<PessoaFisica> getAdvogadosColaboradoresEscritorioList() {
		return pessoaFisicaSessionBean.findByAdvogadosAndColaboradores(getStateManager().getEmpresaSelecionada().getId());
	}

	public List<StatusAgenda> getStatus(){
		return statusAgendaSessionBean.findAllStatusAtivos(getStateManager().getEmpresaSelecionada().getId());
	}

	public Date getDataSelect() {
		return dataSelect;
	}

	public void setDataSelect(Date dataSelect) {
		this.dataSelect = dataSelect;
	}

	public ScheduleEvent getEvent() {
		return event;
	}

	public void setEvent(ScheduleEvent event) {
		this.event = event;
	}

	public String getTimeZone() {
		return TIME_ZONE;
	}

	public String getViewType() {
		return viewType;
	}

	public void setViewType(String viewType) {
		this.viewType = viewType;
	}

	public Date getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	public boolean isCriarAgenda() {
		return criarAgenda;
	}

	public void setCriarAgenda(boolean criarAgenda) {
		this.criarAgenda = criarAgenda;
	}

	public String getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(String horaInicio) {
		this.horaInicio = horaInicio;
	}

	public String getHoraFim() {
		return horaFim;
	}

	public void setHoraFim(String horaFim) {
		this.horaFim = horaFim;
	}

	public String getLocalizacao() {
		return localizacao;
	}

	public void setLocalizacao(String localizacao) {
		this.localizacao = localizacao;
	}

	public PessoaFisica getPessoaFisicaSelecionada() {
		return pessoaFisicaSelecionada;
	}

	public void setPessoaFisicaSelecionada(PessoaFisica pessoaFisicaSelecionada) {
		this.pessoaFisicaSelecionada = pessoaFisicaSelecionada;
	}

	public Evento getEventoSelecionado() {
		return eventoSelecionado;
	}

	public void setEventoSelecionado(Evento eventoSelecionado) {
		this.eventoSelecionado = eventoSelecionado;
	}

	public String getEventoColorSelecionado() {
		return eventoColorSelecionado;
	}

	public void setEventoColorSelecionado(String eventoColorSelecionado) {
		this.eventoColorSelecionado = eventoColorSelecionado;
	}

	public boolean isHasAccess(){
		return possuiAcesso(CHAVE_ACESSO);
	}

	public boolean isHasAccessTransferir(){
		return possuiAcesso(TRANSFERIR_AGENDA);
	}

	public StatusAgenda getStatusAgendaSelecionado() {
		return statusAgendaSelecionado;
	}

	public String getCenterGeoMap() {
		return centerGeoMap;
	}

	public void setCenterGeoMap(String centerGeoMap) {
		this.centerGeoMap = centerGeoMap;
	}

	public MapModel getGeoModel() {
		return geoModel;
	}

	public void setGeoModel(MapModel geoModel) {
		this.geoModel = geoModel;
	}

	public Processo getProcessoSelecionado() {
		return processoSelecionado;
	}

	public void setProcessoSelecionado(Processo processoSelecionado) {
		this.processoSelecionado = processoSelecionado;
	}

	public Pessoa getAdvogadoSelecionado() {
		return advogadoSelecionado;
	}

	public void setAdvogadoSelecionado(Pessoa advogadoSelecionado) {
		this.advogadoSelecionado = advogadoSelecionado;
	}

	public boolean isHasAccessPesquisarTodos(){
		return possuiAcesso(PROCESSOS_PESQUISA_TODOS);
	}

	public boolean isHasAccessControladoria() {
		return possuiAcesso(CONTROLADORIA_LIST_CONTROLADORIA);
	}

	public boolean isPermiteInformarResponsavel() {
		Pessoa pessoa = pessoaSessionBean.retrieve(getUser().getId());
		return pessoa.getPerfil().isAdmin() && isHasAccessControladoria();
	}

	public Pessoa getPessoaSelecionada() {
		return pessoaSelecionada;
	}

	public void setPessoaSelecionada(Pessoa pessoaSelecionada) {
		this.pessoaSelecionada = pessoaSelecionada;
	}
}
