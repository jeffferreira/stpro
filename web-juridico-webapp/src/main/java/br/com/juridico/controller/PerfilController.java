package br.com.juridico.controller;

import br.com.juridico.datamodel.LazyEntityDataModel;
import br.com.juridico.entidades.Direito;
import br.com.juridico.entidades.Perfil;
import br.com.juridico.exception.CrudException;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.impl.DireitoSessionBean;
import br.com.juridico.impl.PerfilAcessoSessionBean;
import br.com.juridico.impl.PerfilSessionBean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Jefferson
 *
 */
@ManagedBean
@SessionScoped
public class PerfilController extends CrudController<Perfil> {

	/**
	 *
	 */
	private static final long serialVersionUID = -4015121345780711405L;
	private static final String PERFIL_FORM = "perfilForm";
	private static final String CHAVE_ACESSO = "PERFIL_PERFIL_TEMPLATE";
	private static final String PAGINA_FORMULARIO = "/pages/cadastros/perfil/perfil-template.xhtml?faces-redirect=true";


	@Inject
	private PerfilSessionBean perfilSessionBean;
	@Inject
	private PerfilAcessoSessionBean perfilAcessoSessionBean;
	@Inject
	private DireitoSessionBean direitoSessionBean;

	public void init(){
		carregaLazyDataModel();
	}
	@Override
	protected void aposCancelar() throws CrudException {
		getObjetoSelecionado().setDescricao(null);
		updateContext(PERFIL_FORM);
		super.aposCancelar();
	}

	@Override
	protected void create(Perfil perfil) throws ValidationException, CrudException {
		perfilSessionBean.create(perfil);

	}

	@Override
	protected void carregaLazyDataModel() {
		setLazyDataModel(new LazyEntityDataModel<Perfil>() {
			@Override
			protected List<Perfil> consultaFiltrada(ConsultaLazyFilter filtro) {
				filtro.setDescricao(getDescricaoFiltro());
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return perfilSessionBean.filtrados(filtro);
			}

			@Override
			protected int countTotalRegistros(ConsultaLazyFilter filtro) {
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return perfilSessionBean.quantidadeFiltrados(filtro);
			}
		});
	}

	@Override
	protected void validar() throws ValidationException {
		perfilSessionBean.clearSession();
		boolean jaExisteDescricaoPerfil = perfilSessionBean.isDescricaoJaCadastrada(getObjetoSelecionado(), getStateManager().getEmpresaSelecionada().getId());
		boolean jaExisteSiglaPerfil = perfilSessionBean.isSiglaJaCadastrada(getObjetoSelecionado(), getStateManager().getEmpresaSelecionada().getId());
		getObjetoSelecionado().validar(jaExisteDescricaoPerfil, jaExisteSiglaPerfil);
	}

	@Override
	protected Perfil inicializaObjetosLazy(Perfil objetoSelecionado) {
		perfilSessionBean.clearSession();
		return perfilSessionBean.retrieve(objetoSelecionado.getId());
	}

	@Override
	protected Perfil instanciarObjetoNovo() {
		return new Perfil();
	}

	@Override
	protected void setEmpresaDeUsuarioLogado(Perfil objetoSelecionado) {
		objetoSelecionado.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());

	}

	@Override
	protected void addChaveAcesso() {
		setChaveAcesso(CHAVE_ACESSO);
	}

	@Override
	protected void antesDeExcluir() throws CrudException {
		getObjetoSelecionado().setDataExclusao(new Date());
	}

	@Override
	protected void aposExcluir() throws CrudException {
		super.buscarPorLazy();
	}

	/**
	 *
	 * @return
	 */
	public String goToFormulario() {
		return PAGINA_FORMULARIO;
	}

	@Override
	protected boolean isPodeAlterar(){
		Direito direito = direitoSessionBean.retrieve(ALTERAR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

	@Override
	protected boolean isPodeExcluir(){
		Direito direito = direitoSessionBean.retrieve(EXCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

	@Override
	protected boolean isPodeIncluir(){
		Direito direito = direitoSessionBean.retrieve(INCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

}
