package br.com.juridico.controller;

import br.com.juridico.datamodel.LazyEntityDataModel;
import br.com.juridico.entidades.Direito;
import br.com.juridico.entidades.Funcao;
import br.com.juridico.exception.CrudException;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.impl.DireitoSessionBean;
import br.com.juridico.impl.FuncaoSessionBean;
import br.com.juridico.impl.PerfilAcessoSessionBean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;

/**
 * Created by jeffersonl2009_00 on 17/08/2016.
 */
@ManagedBean
@SessionScoped
public class FuncaoController extends CrudController<Funcao> {

    private static final String FUNCAO_FORM = "funcaoForm";
    private static final String CHAVE_ACESSO = "FUNCAO_FUNCAO_TEMPLATE";
    private static final String PAGINA_FORMULARIO = "/pages/cadastros/funcao/funcao-template.xhtml?faces-redirect=true";

    @Inject
    private FuncaoSessionBean funcaoSessionBean;
    @Inject
    private PerfilAcessoSessionBean perfilAcessoSessionBean;
    @Inject
    private DireitoSessionBean direitoSessionBean;

    public void init(){
        carregaLazyDataModel();
    }

    @Override
    protected void validar() throws ValidationException {
        funcaoSessionBean.clearSession();
        boolean jaExisteFuncao = funcaoSessionBean.isDescricaoJaCadastrada(getObjetoSelecionado(), getStateManager().getEmpresaSelecionada().getId());
        getObjetoSelecionado().validar(jaExisteFuncao);
    }

    @Override
    protected void carregaLazyDataModel() {
        setLazyDataModel(new LazyEntityDataModel<Funcao>() {
            @Override
            protected List<Funcao> consultaFiltrada(ConsultaLazyFilter filtro) {
                filtro.setDescricao(getDescricaoFiltro());
                filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
                return funcaoSessionBean.filtrados(filtro);
            }

            @Override
            protected int countTotalRegistros(ConsultaLazyFilter filtro) {
                filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
                return funcaoSessionBean.quantidadeFiltrados(filtro);
            }
        });
    }

    @Override
    protected void aposCancelar() throws CrudException {
        getObjetoSelecionado().setDescricao(null);
        updateContext(FUNCAO_FORM);
        super.aposCancelar();
    }

    @Override
    protected void create(Funcao funcao) throws ValidationException, CrudException {
        Funcao funcaoExclusao = getRegistroComExclusaoLogica();

        if (isEditavel() && funcaoExclusao != null) {
            funcaoExclusao.setDataExclusao(null);
            funcaoSessionBean.update(funcaoExclusao);
        } else if (funcao != null && funcao.getDescricao() != null && !funcao.getDescricao().isEmpty()) {
            funcaoSessionBean.create(funcao);
        } else {
            throw new CrudException();
        }

    }

    private Funcao getRegistroComExclusaoLogica() {
        return funcaoSessionBean.findByDescricao(getObjetoSelecionado().getDescricao(), getStateManager().getEmpresaSelecionada().getId());
    }

    @Override
    protected void antesDeExcluir() throws CrudException {
        getObjetoSelecionado().setDataExclusao(new Date());
    }

    @Override
    protected void aposExcluir() throws CrudException {
        super.buscarPorLazy();
    }

    /**
     *
     * @return
     */
    public String goToFormulario() {
        return PAGINA_FORMULARIO;
    }

    @Override
    protected Funcao inicializaObjetosLazy(Funcao objeto) {
        funcaoSessionBean.clearSession();
        return funcaoSessionBean.retrieve(objeto.getId());
    }

    @Override
    protected Funcao instanciarObjetoNovo() {
        return new Funcao();
    }

    @Override
    protected void setEmpresaDeUsuarioLogado(Funcao objetoSelecionado) {
        objetoSelecionado.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());

    }

    @Override
    protected void addChaveAcesso() {
        setChaveAcesso(CHAVE_ACESSO);
    }

    @Override
    protected boolean isPodeAlterar(){
        Direito direito = direitoSessionBean.retrieve(ALTERAR);
        return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
    }

    @Override
    protected boolean isPodeExcluir(){
        Direito direito = direitoSessionBean.retrieve(EXCLUIR);
        return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
    }

    @Override
    protected boolean isPodeIncluir(){
        Direito direito = direitoSessionBean.retrieve(INCLUIR);
        return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
    }
}
