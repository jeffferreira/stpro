package br.com.juridico.controller;

import br.com.juridico.datamodel.LazyEntityDataModel;
import br.com.juridico.entidades.*;
import br.com.juridico.enums.CrudStatus;
import br.com.juridico.enums.TipoEndereco;
import br.com.juridico.exception.CrudException;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.impl.*;
import br.com.juridico.util.EncryptUtil;
import br.com.juridico.util.Util;
import com.google.common.collect.Lists;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Marcos Melo
 *
 */
@ManagedBean
@SessionScoped
public class ColaboradorController extends PessoasController<PessoaFisica> {


	/**
	 *
	 */
	private static final long serialVersionUID = -4486639561815747658L;

	private static final Logger LOGGER = Logger.getLogger(ColaboradorController.class);
	private static final Character PESSOA_FISICA = 'F';
	private static final String PAGES_PESSOAS_COLABORADOR_LIST = "/pages/pessoas/colaborador/colaborador-list.xhtml";
	private static final String PAGES_PESSOAS_COLABORADOR_FORM = "/pages/pessoas/colaborador/colaborador-form.xhtml";
	private static final String COLABORADOR_FORM = "colaboradorForm";
	private static final String COLABORADOR_COLABORADOR_FORM = "COLABORADOR_COLABORADOR_FORM";
	private static final String COLABORADOR_COLABORADOR_LIST = "COLABORADOR_COLABORADOR_LIST";
	private static final String COLABORADOR_EMAILS_FORM = "COLABORADOR_EMAILS_FORM";
	private static final String COLABORADOR_CONTATOS_FORM = "COLABORADOR_CONTATOS_FORM";
	private static final String COLABORADOR_ENDERECOS_FORM = "COLABORADOR_ENDERECOS_FORM";
	private static final String CHAVE_ACESSO = "PESSOAS_LIST_PESSOAS";
	private static final String ADMIN = "ADMIN";

	@Inject
	private PessoaFisicaSessionBean pessoaFisicaSessionBean;
	@Inject
	private ColaboradorSessionBean colaboradorSessionBean;
	@Inject
	private EstadoCivilSessionBean estadoCivilSessionBean;
	@Inject
	private TipoContatoSessionBean tipoContatoSessionBean;
	@Inject
	private UfSessionBean ufSessionBean;
	@Inject
	private PerfilSessionBean perfilSessionBean;
	@Inject
	private FuncaoSessionBean funcaoSessionBean;
	@Inject
	private PessoaSessionBean pessoaSessionBean;

	private PessoaFisica pessoaFisicaSelecionada;

	private List<EstadoCivil> estadosCivis;
	private List<TipoContato> tiposContatos;
	private List<TipoEndereco> tiposEnderecos;
	private List<Uf> ufs;
	private List<Funcao> funcoes;

	private String filter;

	@PostConstruct
	public void initialize() {
		verificaCliqueBreadCrumb();
		carregaPermissoes();
	}

	public void init(){
		// TODO Rever e retirar do postConstruct pois pode prejudicar performance
		this.estadosCivis = estadoCivilSessionBean.retrieveAll();
		this.tiposContatos = tipoContatoSessionBean.retrieveAll(getStateManager().getEmpresaSelecionada().getId());
		this.funcoes = funcaoSessionBean.retrieveAll(getStateManager().getEmpresaSelecionada().getId());
		this.setUfs(ufSessionBean.findAll());
		carregaTiposEndereco();
	}

	private void carregaPermissoes() {
		setPermiteAlteracao(isPodeAlterar());
		setPermiteExclusao(isPodeExcluir());
		setPermiteInclusao(isPodeIncluir());
		setPermiteVisualizacaoEmail(isPodeVisualizarEmails());
		setPermiteInclusaoEmail(isPodeIncluirEmails());
		setPermiteExclusaoEmail(isPodeExcluirEmails());
		setPermiteVisualizacaoContato(isPodeVisualizarContatos());
		setPermiteInclusaoContato(isPodeIncluirContatos());
		setPermiteExclusaoContato(isPodeExcluirContatos());
		setPermiteVisualizacaoEndereco(isPodeVisualizarEnderecos());
		setPermiteExclusaoEndereco(isPodeExcluirEnderecos());
		setPermiteInclusaoEndereco(isPodeIncluirEnderecos());

	}

	private void verificaCliqueBreadCrumb() {
		Map<String,String> params = getExternalContext().getRequestParameterMap();
		String pesquisaPorBreadCrumb = params.get("paramLink");
		if("true".equals(pesquisaPorBreadCrumb)) {
			super.buscarPorLazy();
			getFlashScope().put(LIST_FLASH, getLazyDataModel());
			carregaPermissoes();
		}
	}

	private void initObjetoCollectors() {
		super.initObjetoPessoaCollectors(getObjetoSelecionado().getPessoa());
	}

	@Override
	protected void carregaLazyDataModel() {
		setLazyDataModel(new LazyEntityDataModel<PessoaFisica>() {
			@Override
			protected List<PessoaFisica> consultaFiltrada(ConsultaLazyFilter filtro) {
				filtro.setDescricao(getDescricaoFiltro());
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				filtro.setPropriedadeOrdenacao("pessoa.descricao");
				return pessoaFisicaSessionBean.filtrados(filtro, false, true);
			}

			@Override
			protected int countTotalRegistros(ConsultaLazyFilter filtro) {
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return pessoaFisicaSessionBean.quantidadeFiltrados(filtro, false, true);
			}
		});
	}

	public void goToDetalhes(){
		RequestContext.getCurrentInstance().execute("openModal('#colaboradorModal')");
	}

	private void carregaTiposEndereco() {
		this.tiposEnderecos = Lists.newArrayList();
		for (TipoEndereco tipoEndereco : TipoEndereco.values()) {
			this.tiposEnderecos.add(tipoEndereco);
		}
	}

	public boolean isHasAccessCadastro(){
		return possuiAcesso(COLABORADOR_COLABORADOR_FORM);
	}

	public boolean isHasAccess(){
		return possuiAcesso(COLABORADOR_COLABORADOR_LIST);
	}

	@Override
	protected void antesInserir() throws CrudException {
		getObjetoSelecionado().setPessoa(new Pessoa());
		initObjetoCollectors();
	}

	@Override
	protected void aposCancelar() throws CrudException {
		updateContext(COLABORADOR_FORM);
		super.aposCancelar();
		super.novo();
	}

	public String novoColaborador() {
		novo();
		return PAGES_PESSOAS_COLABORADOR_FORM;
	}

	/**
	 *
	 * @return
	 */
	public String adicionarEndereco() {
		super.adicionarEnderecoPessoaFisica(getObjetoSelecionado());
		return null;
	}

	/**
	 *
	 * @return
	 */
	public String adicionarEmail() {
		super.adicionarEmailPessoaFisica(getObjetoSelecionado());
		return null;
	}

	/**
	 *
	 * @return
	 */
	public String adicionarContato() {
		super.adicionarContatoPessoaFisica(getObjetoSelecionado());
		return null;
	}


	@Override
	protected void antesDeEditar(PessoaFisica objeto) throws CrudException {
		initObjetoCollectors();
	}

	public void limpaNumeroEndereco() {
		if (getPessoaEndereco().getEndereco().getSemNumero()) {
			getPessoaEndereco().getEndereco().setNumero(null);
			return;
		}
		getPessoaEndereco().getEndereco().setSemNumero(false);
	}

	/**
	 *
	 */
	public void buscarCep(ValueChangeEvent event) {
		String cep = (String) event.getNewValue();
		super.buscarCep(cep);
	}

	@Override
	protected void validar() throws ValidationException {
		PessoaFisica colaborador = colaboradorSessionBean.findNumeroOabExistente(getObjetoSelecionado());
		getObjetoSelecionado().validarColaborador(colaborador != null);
	}

	@Override
	protected void antesDeSalvar() throws CrudException {
		try {
			String cpf = getObjetoSelecionado().getCpf().replaceAll("[.-]", "");
			getObjetoSelecionado().setCpf(cpf);
			String rg = getObjetoSelecionado().getRg().replaceAll("[.-]", "");
			getObjetoSelecionado().setRg(rg);
			getObjetoSelecionado().setAdvogado(false);
			getObjetoSelecionado().setColaborador(true);
			getObjetoSelecionado().getPessoa().setTipo(PESSOA_FISICA);
			getObjetoSelecionado().getPessoa().setCliente(false);
			getObjetoSelecionado().getPessoa().setAtivo(true);
			getObjetoSelecionado().getPessoa().setDataCadastro(new Date());
			adicionaLoginSenha();

			if(!getObjetoSelecionado().getPessoa().isClienteEmpresa()) {
				getObjetoSelecionado().getPessoa().setNomeIniciais(null);
			}

		} catch (Exception e) {
			LOGGER.error(e, e.getCause());
		}
	}

	private void adicionaLoginSenha() throws Exception {
		if(getObjetoSelecionado().getPessoa().getId() == null && getObjetoSelecionado().isColaboradorEscritorio()) {
			List<Pessoa> usuariosCadastrados = pessoaSessionBean.findUserList(getObjetoSelecionado().getCpf());
			String usuario = getObjetoSelecionado().getCpf().replaceAll("[.-]", "");

			if(usuariosCadastrados.isEmpty()) {
				byte[] senha = EncryptUtil.execute(Util.randomGenerateValue(10));
				createUpdateLogin(getObjetoSelecionado(), usuario, senha);
			} else {
				createUpdateLogin(getObjetoSelecionado(), usuario, usuariosCadastrados.get(0).getLogin().getSenha());
			}
		}
	}

	@Override
	protected void antesDeExcluir() throws CrudException {
		getObjetoSelecionado().setDataExclusao(new Date());
		getObjetoSelecionado().getPessoa().setDataExclusao(new Date());
	}

	@Override
	protected void aposExcluir() throws CrudException {
		super.buscarPorLazy();
	}

	@Override
	protected void create(PessoaFisica pessoaFisica) throws ValidationException, CrudException {
		pessoaFisica.getPessoa().setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
		pessoaFisicaSessionBean.create(pessoaFisica);

		if(getObjetoSelecionado().isColaboradorEscritorio() && CrudStatus.CREATE.equals(crudStatus)) {
			enviaEmailNovoUsuarioPessoa(getObjetoSelecionado().getPessoa());
		}
	}

	public String goToEditarPessoa(PessoaFisica pessoaFisica) {
		setObjetoSelecionado(pessoaFisica);
		editar();
		return PAGES_PESSOAS_COLABORADOR_FORM;
	}

	public String goToList() {
		pessoaFisicaSessionBean.clearSession();
		colaboradorSessionBean.clearSession();
		super.buscarPorLazy();
		return PAGES_PESSOAS_COLABORADOR_LIST;
	}

	public void listFromSimpleSearch() {
		pessoaFisicaSessionBean.clearSession();
		super.buscarPorLazy();
	}

	@Override
	protected PessoaFisica inicializaObjetosLazy(PessoaFisica objeto) {
		pessoaFisicaSessionBean.clearSession();
		return pessoaFisicaSessionBean.retrieve(objeto.getId());
	}

	@Override
	protected void setEmpresaDeUsuarioLogado(PessoaFisica objetoSelecionado) {
		objetoSelecionado.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
	}

	@Override
	protected void addChaveAcesso() {
		setChaveAcesso(CHAVE_ACESSO);
	}

	@Override
	protected PessoaFisica instanciarObjetoNovo() {
		return new PessoaFisica();
	}

	public List<EstadoCivil> getEstadosCivis() {
		return estadosCivis;
	}

	public void setEstadosCivis(List<EstadoCivil> estadosCivis) {
		this.estadosCivis = estadosCivis;
	}

	public List<TipoContato> getTiposContatos() {
		return tiposContatos;
	}

	public void setTiposContatos(List<TipoContato> tiposContatos) {
		this.tiposContatos = tiposContatos;
	}

	public List<Uf> getUfs() {
		return ufs;
	}

	public void setUfs(List<Uf> ufs) {
		this.ufs = ufs;
	}

	public List<Funcao> getFuncoes() {
		return funcoes;
	}

	public void setFuncoes(List<Funcao> funcoes) {
		this.funcoes = funcoes;
	}

	public List<Perfil> getPerfis() {
		boolean admin = ADMIN.equals(getUser().getPerfil().getSigla());
		return perfilSessionBean.findAllByUsuario(getStateManager().getEmpresaSelecionada().getId(), admin);
	}

	public String getFilter() {
		return filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}

	public List<TipoEndereco> getTiposEnderecos() {
		return tiposEnderecos;
	}

	public void setTiposEnderecos(List<TipoEndereco> tiposEnderecos) {
		this.tiposEnderecos = tiposEnderecos;
	}

	@Override
	protected boolean isPodeAlterar(){
		return super.isPodeAlterar(COLABORADOR_COLABORADOR_FORM);
	}

	@Override
	protected boolean isPodeExcluir(){
		return super.isPodeExcluir(COLABORADOR_COLABORADOR_FORM);
	}

	@Override
	protected boolean isPodeIncluir(){
		return super.isPodeIncluir(COLABORADOR_COLABORADOR_FORM);
	}

	public boolean isPodeSomenteVisualizar() {
		return isHasAccess() && !isPodeAlterar() && !isPodeIncluir();
	}

	public boolean isPodeVisualizarEmails() {
		return super.isPodeVisualizarEmails(COLABORADOR_EMAILS_FORM);
	}

	public boolean isPodeIncluirEmails() {
		return super.isPodeIncluirEmails(COLABORADOR_EMAILS_FORM);
	}

	public boolean isPodeExcluirEmails() {
		return super.isPodeExcluirEmails(COLABORADOR_EMAILS_FORM);
	}

	public boolean isPodeVisualizarContatos() {
		return super.isPodeVisualizarContatos(COLABORADOR_CONTATOS_FORM);
	}

	public boolean isPodeIncluirContatos() {
		return super.isPodeIncluirContatos(COLABORADOR_CONTATOS_FORM);
	}

	public boolean isPodeExcluirContatos() {
		return super.isPodeExcluirContatos(COLABORADOR_CONTATOS_FORM);
	}

	public boolean isPodeVisualizarEnderecos() {
		return super.isPodeVisualizarEnderecos(COLABORADOR_ENDERECOS_FORM);
	}

	public boolean isPodeIncluirEnderecos() {
		return super.isPodeIncluirEnderecos(COLABORADOR_ENDERECOS_FORM);
	}

	public boolean isPodeExcluirEnderecos() {
		return super.isPodeExcluirEnderecos(COLABORADOR_ENDERECOS_FORM);
	}

	public PessoaFisica getPessoaFisicaSelecionada() {
		return pessoaFisicaSelecionada;
	}

	public void setPessoaFisicaSelecionada(PessoaFisica pessoaFisicaSelecionada) {
		this.pessoaFisicaSelecionada = pessoaFisicaSelecionada;
	}
}
