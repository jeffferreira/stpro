package br.com.juridico.controller;

import br.com.juridico.datamodel.LazyEntityDataModel;
import br.com.juridico.entidades.Direito;
import br.com.juridico.entidades.RamoAtividade;
import br.com.juridico.exception.CrudException;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.impl.DireitoSessionBean;
import br.com.juridico.impl.PerfilAcessoSessionBean;
import br.com.juridico.impl.RamoAtividadeSessionBean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;

/**
 * Created by Marcos Melo on 15/12/2016.
 */
@ManagedBean
@SessionScoped
public class RamoAtividadeController extends CrudController<RamoAtividade> {

    private static final String RAMO_ATIVIDADE_FORM = "ramoAtividadeForm";
    private static final String CHAVE_ACESSO = "RAMOATIVIDADE_RAMO_ATIVIDADE_TEMPLATE";
    private static final String PAGINA_FORMULARIO = "/pages/cadastros/ramoAtividade/ramo-atividade-template.xhtml?faces-redirect=true";

    @Inject
    private RamoAtividadeSessionBean ramoAtividadeSessionBean;
    @Inject
    private PerfilAcessoSessionBean perfilAcessoSessionBean;
    @Inject
    private DireitoSessionBean direitoSessionBean;

    public void init(){
        carregaLazyDataModel();
    }

    @Override
    protected void validar() throws ValidationException {
        ramoAtividadeSessionBean.clearSession();
        boolean jaExisteRamoAtividade = ramoAtividadeSessionBean.isDescricaoJaCadastrada(getObjetoSelecionado(), getStateManager().getEmpresaSelecionada().getId());
        getObjetoSelecionado().validar(jaExisteRamoAtividade);
    }

    @Override
    protected void carregaLazyDataModel() {
        setLazyDataModel(new LazyEntityDataModel<RamoAtividade>() {
            @Override
            protected List<RamoAtividade> consultaFiltrada(ConsultaLazyFilter filtro) {
                filtro.setDescricao(getDescricaoFiltro());
                filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
                return ramoAtividadeSessionBean.filtrados(filtro);
            }

            @Override
            protected int countTotalRegistros(ConsultaLazyFilter filtro) {
                filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
                return ramoAtividadeSessionBean.quantidadeFiltrados(filtro);
            }
        });
    }

    @Override
    protected void aposCancelar() throws CrudException {
        getObjetoSelecionado().setDescricao(null);
        updateContext(RAMO_ATIVIDADE_FORM);
        super.aposCancelar();
    }

    @Override
    protected void create(RamoAtividade ramoAtividade) throws ValidationException, CrudException {
        RamoAtividade ramoAtividadeExclusao = getRegistroComExclusaoLogica();

        if (isEditavel() && ramoAtividadeExclusao != null) {
            ramoAtividadeExclusao.setDataExclusao(null);
            ramoAtividadeSessionBean.update(ramoAtividadeExclusao);
        } else if (ramoAtividade != null && ramoAtividade.getDescricao() != null && !ramoAtividade.getDescricao().isEmpty()) {
            ramoAtividadeSessionBean.create(ramoAtividade);
        } else {
            throw new CrudException();
        }

    }

    private RamoAtividade getRegistroComExclusaoLogica() {
        return ramoAtividadeSessionBean.findByDescricao(getObjetoSelecionado().getDescricao(), getStateManager().getEmpresaSelecionada().getId());
    }

    @Override
    protected void antesDeExcluir() throws CrudException {
        getObjetoSelecionado().setDataExclusao(new Date());
    }

    @Override
    protected void aposExcluir() throws CrudException {
        super.buscarPorLazy();
    }

    /**
     *
     * @return
     */
    public String goToFormulario() {
        return PAGINA_FORMULARIO;
    }

    @Override
    protected RamoAtividade inicializaObjetosLazy(RamoAtividade objeto) {
        ramoAtividadeSessionBean.clearSession();
        return ramoAtividadeSessionBean.retrieve(objeto.getId());
    }

    @Override
    protected RamoAtividade instanciarObjetoNovo() {
        return new RamoAtividade();
    }

    @Override
    protected void setEmpresaDeUsuarioLogado(RamoAtividade objetoSelecionado) {
        objetoSelecionado.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());

    }

    @Override
    protected void addChaveAcesso() {
        setChaveAcesso(CHAVE_ACESSO);
    }

    @Override
    protected boolean isPodeAlterar(){
        Direito direito = direitoSessionBean.retrieve(ALTERAR);
        return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
    }

    @Override
    protected boolean isPodeExcluir(){
        Direito direito = direitoSessionBean.retrieve(EXCLUIR);
        return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
    }

    @Override
    protected boolean isPodeIncluir(){
        Direito direito = direitoSessionBean.retrieve(INCLUIR);
        return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
    }
}
