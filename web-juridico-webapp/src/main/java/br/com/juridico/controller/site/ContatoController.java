package br.com.juridico.controller.site;

import br.com.juridico.controller.AbstractController;
import br.com.juridico.entidades.ConfiguracaoEmail;
import br.com.juridico.mail.MailBusiness;
import org.apache.log4j.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.mail.MessagingException;

/**
 * Created by jefferson on 05/03/2017.
 */
@ManagedBean
@ViewScoped
public class ContatoController extends AbstractController {

    private static final Logger LOGGER = Logger.getLogger(ContatoController.class);

    private String email;
    private String nome;
    private String telefone;
    private String texto;

    public void solicitarContato(){
        try {
            String templateFormat = "%1$s | %2$s | %3$s";
            String textoFormatado = String.format(templateFormat, this.nome, this.email, this.telefone);
            String textoEmail = textoFormatado.concat("\n \n").concat(this.texto);
            MailBusiness.enviarEmail(getConfiguracaoEmail(), "Solicitação de Contato", textoEmail, "contato@stpro.com.br");
            exibirMensagem(FacesMessage.SEVERITY_INFO, "Solicitação enviada com sucesso. Em breve entraremos em contato.");
        } catch (MessagingException e) {
           LOGGER.error(e, e.getCause());
        }
    }

    private ConfiguracaoEmail getConfiguracaoEmail() {
        ConfiguracaoEmail configuracaoEmail = new ConfiguracaoEmail();
        configuracaoEmail.setHost("mail.stpro.com.br");
        configuracaoEmail.setPort(587);
        configuracaoEmail.setUsername("site@stpro.com.br");
        configuracaoEmail.setPassword("hN$qt4LH!st}");
        configuracaoEmail.setProtocol("SMTP");
        configuracaoEmail.setFrom("site@stpro.com.br");
        return configuracaoEmail;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }
}
