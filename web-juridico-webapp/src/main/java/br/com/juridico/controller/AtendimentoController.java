package br.com.juridico.controller;

import br.com.juridico.entidades.*;
import br.com.juridico.enums.StatusAtendimentoIndicador;
import br.com.juridico.exception.CrudException;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.impl.AtendimentoSessionBean;
import br.com.juridico.impl.PessoaFisicaSessionBean;
import br.com.juridico.util.DownloadUtil;
import com.google.common.collect.Lists;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.servlet.http.Part;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * 
 * @author Jefferson
 *
 */
@ManagedBean
@ViewScoped
public class AtendimentoController extends CrudController<Atendimento> {

	/**
     * 
     */
	private static final long serialVersionUID = 843505264485399749L;

	private static final Logger LOGGER = Logger.getLogger(AtendimentoController.class);
	private static final String ADMIN = "ADMIN";
	private static final String CHAVE_ACESSO = "";

	@Inject
	private AtendimentoSessionBean atendimentoSessionBean;
	@Inject
	private PessoaFisicaSessionBean pessoaFisicaSessionBean;

	private List<TipoAtendimento> tipos;
	private List<AndamentoAtendimento> andamentos;
	private List<PessoaFisica> advogados = Lists.newArrayList();

	private Part arquivo;
	private String descricaoAtendimento;
	private int rowAnexoNumber;

	@PostConstruct
	public void initialize(){
//		if(ADMIN.equals(getUser().getPerfil().getSigla())) {
//			setLista(pesquisar());
//		} else {
//			setLista(atendimentoSessionBean.findByMeusAtendimentos(getUser()));
//		}
	}

	@Override
	protected void carregaLazyDataModel() {
		// LAZY
	}

	/**
	 * 
	 * @return
	 */
	public String goToClienteAtendimento() {
		return "/pages/atendimento/cliente/atendimento-cliente-template?faces-redirect=true";
	}

	protected Atendimento instanciarObjetoNovo() {
		return new Atendimento();
	}

	protected Atendimento inicializaObjetosLazy(Atendimento objeto) {
		atendimentoSessionBean.clearSession();
		return atendimentoSessionBean.retrieve(objeto.getId());
	}

	public void importar() {
		try {
			byte[] bytes = IOUtils.toByteArray(arquivo.getInputStream());
			String nomeArquivo = getFileName(arquivo);
			String extensao = nomeArquivo.substring(nomeArquivo.lastIndexOf("."), nomeArquivo.length());
			AtendimentoDocumento documento = new AtendimentoDocumento(getObjetoSelecionado().getAndamentos().get(0), nomeArquivo, extensao, bytes, getUser().getCodEmpresa());
			getObjetoSelecionado().getAndamentos().get(0).addDocumento(documento);
			updateContext("atendimentoForm");
		} catch (IOException e) {
			LOGGER.error(e, e.getCause());
		}
	}

	private String getFileName(Part part){
		String header = part.getHeader( "content-disposition" );
		for(String tmp : header.split(";") ){
			if(tmp.trim().startsWith("filename") ){
				return tmp.substring( tmp.indexOf("=")+2 , tmp.length()-1 );
			}
		}
		return null;
	}

	public void excluirAnexo() {
		getObjetoSelecionado().getAndamentos().get(0).getDocumentos().remove(rowAnexoNumber);
		updateContext("atendimentoForm");
	}

	/**
	 *
	 * @param documento
     */
	public void downloadAnexo(AtendimentoDocumento documento){
		DownloadUtil.downloadFile(documento.getArquivo(), documento.getNome());
	}

	@Override
	protected void create(Atendimento objetoSelecionado) throws ValidationException, CrudException {
		atendimentoSessionBean.create(objetoSelecionado);
	}

	@Override
	protected void setEmpresaDeUsuarioLogado(Atendimento objetoSelecionado) {
		objetoSelecionado.setCodigoEmpresa(getUser().getCodEmpresa());
	}

	@Override
	protected void antesDeSalvar() throws CrudException {
		getObjetoSelecionado().getAndamentos().get(0).setDescricao(descricaoAtendimento);
		super.antesDeSalvar();
	}

	@Override
	protected void antesInserir() throws CrudException {
		AndamentoAtendimento andamentoAtendimento = new AndamentoAtendimento(StatusAtendimentoIndicador.PENDENTE.getValue(), new Date());
		getObjetoSelecionado().getAndamentos().add(andamentoAtendimento);
		super.antesInserir();
	}

	@Override
	protected void antesDeEditar(Atendimento objeto) throws CrudException {
		this.advogados = pessoaFisicaSessionBean.getAdvogados(true, getUser().getCodEmpresa());
		super.antesDeEditar(objeto);
	}

	public List<TipoAtendimento> getTipos() {
		return tipos;
	}

	public void setTipos(List<TipoAtendimento> tipos) {
		this.tipos = tipos;
	}

	public List<AndamentoAtendimento> getAndamentos() {
		return andamentos;
	}

	public void setAndamentos(List<AndamentoAtendimento> andamentos) {
		this.andamentos = andamentos;
	}

	public Part getArquivo() {
		return arquivo;
	}

	public void setArquivo(Part arquivo) {
		this.arquivo = arquivo;
	}

	public int getRowAnexoNumber() {
		return rowAnexoNumber;
	}

	public void setRowAnexoNumber(int rowAnexoNumber) {
		this.rowAnexoNumber = rowAnexoNumber;
	}

	public List<PessoaFisica> getAdvogados() {
		return advogados;
	}

	public void setAdvogados(List<PessoaFisica> advogados) {
		this.advogados = advogados;
	}

	public String getDescricaoAtendimento() {
		return descricaoAtendimento;
	}

	public void setDescricaoAtendimento(String descricaoAtendimento) {
		this.descricaoAtendimento = descricaoAtendimento;
	}

	@Override
	protected void addChaveAcesso() {
		setChaveAcesso(CHAVE_ACESSO);
	}

	@Override
	protected boolean isPodeAlterar() {
		return false;
	}

	@Override
	protected boolean isPodeExcluir() {
		return false;
	}

	@Override
	protected boolean isPodeIncluir() {
		return false;
	}
}
