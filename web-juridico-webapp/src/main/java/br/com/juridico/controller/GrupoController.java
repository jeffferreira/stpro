package br.com.juridico.controller;

import br.com.juridico.datamodel.LazyEntityDataModel;
import br.com.juridico.entidades.Direito;
import br.com.juridico.entidades.Grupo;
import br.com.juridico.entidades.GrupoAdvogado;
import br.com.juridico.entidades.PessoaFisica;
import br.com.juridico.enums.CrudStatus;
import br.com.juridico.exception.CrudException;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.impl.DireitoSessionBean;
import br.com.juridico.impl.GrupoSessionBean;
import br.com.juridico.impl.PerfilAcessoSessionBean;
import com.google.common.collect.Lists;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author Jefferson
 *
 */
@ManagedBean
@SessionScoped
public class GrupoController extends CrudController<Grupo> {

	/**
	 *
	 */
	private static final long serialVersionUID = -2200774353963640279L;
	private static final String GRUPO_FORM = "grupoForm";
	private static final String CHAVE_ACESSO = "GRUPO_GRUPO_TEMPLATE";
	private static final String PAGINA_FORMULARIO = "/pages/cadastros/grupo/grupo-template.xhtml?faces-redirect=true";

	@Inject
	private GrupoSessionBean grupoSessionBean;
	@Inject
	private PerfilAcessoSessionBean perfilAcessoSessionBean;
	@Inject
	private DireitoSessionBean direitoSessionBean;

	private Set<PessoaFisica> selectedAdvogadosDoGrupo = new HashSet<>();
	private List<PessoaFisica> advogados = Lists.newArrayList();

	public void init(){
		carregaLazyDataModel();
	}

	@Override
	protected void create(Grupo objeto) throws ValidationException, CrudException {
		Grupo grupoExclusao = getRegistroComExclusaoLogica();

		if (isEditavel() && grupoExclusao != null) {
			if(grupoExclusao.getDataExclusao() != null){
				grupoExclusao.setDataExclusao(null);
				grupoExclusao.getGruposAdvogados().clear();

				for (PessoaFisica pessoaFisica : selectedAdvogadosDoGrupo) {
					grupoExclusao.addGruposAdvogados(new GrupoAdvogado(grupoExclusao, pessoaFisica, getStateManager().getEmpresaSelecionada().getId()));
				}
				grupoSessionBean.update(grupoExclusao);
				return;
			}
			grupoSessionBean.update(getObjetoSelecionado());

		} else if (objeto != null && objeto.getDescricao() != null && !objeto.getDescricao().isEmpty()) {
			grupoSessionBean.create(objeto);
		} else {
			throw new CrudException();
		}
	}

	@Override
	protected void aposSalvar() throws CrudException {
		selectedAdvogadosDoGrupo.clear();
	}

	@Override
	protected void carregaLazyDataModel() {
		setLazyDataModel(new LazyEntityDataModel<Grupo>() {
			@Override
			protected List<Grupo> consultaFiltrada(ConsultaLazyFilter filtro) {
				filtro.setDescricao(getDescricaoFiltro());
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return grupoSessionBean.filtrados(filtro);
			}

			@Override
			protected int countTotalRegistros(ConsultaLazyFilter filtro) {
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return grupoSessionBean.quantidadeFiltrados(filtro);
			}
		});
	}

	@Override
	protected void validar() throws ValidationException {
		grupoSessionBean.clearSession();
		boolean jaExisteGrupo = grupoSessionBean.isDescricaoJaCadastrada(getObjetoSelecionado(), getStateManager().getEmpresaSelecionada().getId());
		getObjetoSelecionado().validar(jaExisteGrupo);
	}

	@Override
	protected void antesDeExcluir() throws CrudException {
		getObjetoSelecionado().setDataExclusao(new Date());
	}

	@Override
	protected void aposCancelar() throws CrudException {
		getObjetoSelecionado().setDescricao(null);
		updateContext(GRUPO_FORM);
	}

	@Override
	protected void aposExcluir() throws CrudException {
		super.buscarPorLazy();
	}

	@Override
	protected Grupo inicializaObjetosLazy(Grupo objeto) {
		grupoSessionBean.clearSession();
		return grupoSessionBean.retrieve(objeto.getId());
	}

	@Override
	protected Grupo instanciarObjetoNovo() {
		return new Grupo();
	}

	@Override
	protected void antesDeEditar(Grupo grupo) throws CrudException {
		selectedAdvogadosDoGrupo.clear();
		selectedAdvogadosDoGrupo.addAll(grupo.getGruposAdvogados().stream().map(GrupoAdvogado::getAdvogadoPessoa).collect(Collectors.toList()));
	}

	@Override
	protected void antesDeSalvar() throws CrudException {
		if(CrudStatus.UPDATE.equals(crudStatus)){
			getObjetoSelecionado().getGruposAdvogados().clear();
		}

		for (PessoaFisica pessoaFisica : selectedAdvogadosDoGrupo) {
			getObjetoSelecionado().addGruposAdvogados(new GrupoAdvogado(getObjetoSelecionado(), pessoaFisica, getStateManager().getEmpresaSelecionada().getId()));
		}
	}

	/**
	 *
	 * @return
	 */
	public String goToFormulario() {
		return PAGINA_FORMULARIO;
	}

	private Grupo getRegistroComExclusaoLogica() {
		return grupoSessionBean.findByDescricao(getObjetoSelecionado().getDescricao(), getStateManager().getEmpresaSelecionada().getId());
	}

	@Override
	protected void setEmpresaDeUsuarioLogado(Grupo objetoSelecionado) {
		objetoSelecionado.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());

	}

	public Set<PessoaFisica> getSelectedAdvogadosDoGrupo() {
		return selectedAdvogadosDoGrupo;
	}

	public void setSelectedAdvogadosDoGrupo(Set<PessoaFisica> selectedAdvogadosDoGrupo) {
		this.selectedAdvogadosDoGrupo = selectedAdvogadosDoGrupo;
	}

	public List<PessoaFisica> getAdvogados() {
		return advogados;
	}

	public void setAdvogados(List<PessoaFisica> advogados) {
		this.advogados = advogados;
	}

	@Override
	protected void addChaveAcesso() {
		setChaveAcesso(CHAVE_ACESSO);
	}

	@Override
	protected boolean isPodeAlterar(){
		Direito direito = direitoSessionBean.retrieve(ALTERAR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

	@Override
	protected boolean isPodeExcluir(){
		Direito direito = direitoSessionBean.retrieve(EXCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

	@Override
	protected boolean isPodeIncluir(){
		Direito direito = direitoSessionBean.retrieve(INCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}
}
