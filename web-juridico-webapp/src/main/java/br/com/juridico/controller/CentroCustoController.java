package br.com.juridico.controller;

import br.com.juridico.datamodel.LazyEntityDataModel;
import br.com.juridico.entidades.CentroCusto;
import br.com.juridico.entidades.Direito;
import br.com.juridico.exception.CrudException;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.impl.CentroCustoSessionBean;
import br.com.juridico.impl.DireitoSessionBean;
import br.com.juridico.impl.PerfilAcessoSessionBean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import javax.persistence.OptimisticLockException;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Marcos
 *
 */
@ManagedBean
@SessionScoped
public class CentroCustoController extends CrudController<CentroCusto> {

	/**
	 *
	 */
	private static final long serialVersionUID = -3576844242935007490L;
	private static final String CENTRO_CUSTO_FORM = "centroCustoForm";
	private static final String CHAVE_ACESSO = "CENTROCUSTO_CENTRO_CUSTO_TEMPLATE";
	private static final String PAGINA_FORMULARIO = "/pages/cadastros/centroCusto/centro-custo-template.xhtml?faces-redirect=true";

	@Inject
	private CentroCustoSessionBean centroCustoSessionBean;
	@Inject
	private PerfilAcessoSessionBean perfilAcessoSessionBean;
	@Inject
	private DireitoSessionBean direitoSessionBean;

	public void init(){
		carregaLazyDataModel();
	}

	@Override
	protected void validar() throws ValidationException {
		centroCustoSessionBean.clearSession();
		boolean jaExisteCentroCusto = centroCustoSessionBean.isDescricaoJaCadastrada(getObjetoSelecionado(), getStateManager().getEmpresaSelecionada().getId());
		getObjetoSelecionado().validar(jaExisteCentroCusto);
	}

	@Override
	protected void aposCancelar() throws CrudException {
		getObjetoSelecionado().setDescricao(null);
		updateContext(CENTRO_CUSTO_FORM);
		super.aposCancelar();
	}

	@Override
	protected void create(CentroCusto centroCusto) throws OptimisticLockException, ValidationException, CrudException {
		CentroCusto centroCustoExclusao = getRegistroComExclusaoLogica();

		if (isEditavel() && centroCustoExclusao != null) {
			centroCustoExclusao.setDataExclusao(null);
			centroCustoSessionBean.update(centroCustoExclusao);
		} else if (centroCusto != null && centroCusto.getDescricao() != null && !centroCusto.getDescricao().isEmpty()) {
			centroCustoSessionBean.create(centroCusto);
		} else {
			throw new CrudException();
		}

	}

	private CentroCusto getRegistroComExclusaoLogica() {
		return centroCustoSessionBean.findByDescricao(getObjetoSelecionado().getDescricao(), getStateManager().getEmpresaSelecionada().getId());
	}

	@Override
	protected void antesDeExcluir() throws CrudException {
		getObjetoSelecionado().setDataExclusao(new Date());
	}

	@Override
	protected void aposExcluir() throws CrudException {
		super.buscarPorLazy();
	}

	@Override
	protected void carregaLazyDataModel() {
		setLazyDataModel(new LazyEntityDataModel<CentroCusto>() {
			@Override
			protected List<CentroCusto> consultaFiltrada(ConsultaLazyFilter filtro) {
				filtro.setDescricao(getDescricaoFiltro());
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return centroCustoSessionBean.filtrados(filtro);
			}

			@Override
			protected int countTotalRegistros(ConsultaLazyFilter filtro) {
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return centroCustoSessionBean.quantidadeFiltrados(filtro);
			}
		});
	}

	/**
	 *
	 * @return
	 */
	public String goToFormulario() {
		return PAGINA_FORMULARIO;
	}

	@Override
	protected CentroCusto inicializaObjetosLazy(CentroCusto objeto) {
		centroCustoSessionBean.clearSession();
		return centroCustoSessionBean.retrieve(objeto.getId());
	}

	@Override
	protected CentroCusto instanciarObjetoNovo() {
		return new CentroCusto();
	}

	@Override
	protected void setEmpresaDeUsuarioLogado(CentroCusto objetoSelecionado) {
		objetoSelecionado.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
	}

	@Override
	protected void addChaveAcesso() {
		setChaveAcesso(CHAVE_ACESSO);
	}

	@Override
	protected boolean isPodeAlterar(){
		Direito direito = direitoSessionBean.retrieve(ALTERAR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

	@Override
	protected boolean isPodeExcluir(){
		Direito direito = direitoSessionBean.retrieve(EXCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

	@Override
	protected boolean isPodeIncluir(){
		Direito direito = direitoSessionBean.retrieve(INCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

}
