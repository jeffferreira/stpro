package br.com.juridico.controller;

import br.com.juridico.datamodel.LazyEntityDataModel;
import br.com.juridico.entidades.Direito;
import br.com.juridico.entidades.Pedido;
import br.com.juridico.exception.CrudException;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.impl.DireitoSessionBean;
import br.com.juridico.impl.PedidoSessionBean;
import br.com.juridico.impl.PerfilAcessoSessionBean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Marcos
 *
 */
@ManagedBean
@SessionScoped
public class PedidoController extends CrudController<Pedido> {

	/**
	 *
	 */
	private static final long serialVersionUID = 4224621869352515530L;
	private static final String PEDIDO_FORM = "pedidoForm";
	private static final String CHAVE_ACESSO = "PEDIDO_PEDIDO_TEMPLATE";
	private static final String PAGINA_FORMULARIO = "/pages/cadastros/pedido/pedido-template.xhtml?faces-redirect=true";

	@Inject
	private PedidoSessionBean pedidoSessionBean;
	@Inject
	private PerfilAcessoSessionBean perfilAcessoSessionBean;
	@Inject
	private DireitoSessionBean direitoSessionBean;

	public void init(){
		carregaLazyDataModel();
	}

	@Override
	protected void validar() throws ValidationException {
		pedidoSessionBean.clearSession();
		boolean jaExistePedido = pedidoSessionBean.isDescricaoJaCadastrada(getObjetoSelecionado(), getStateManager().getEmpresaSelecionada().getId());
		getObjetoSelecionado().validar(jaExistePedido);
	}

	@Override
	protected void create(Pedido pedido) throws ValidationException, CrudException {
		Pedido pedidoExclusao = getRegistroComExclusaoLogica();

		if (isEditavel() && pedidoExclusao != null) {
			pedidoExclusao.setDataExclusao(null);
			pedidoSessionBean.update(pedidoExclusao);
		} else if (pedido != null && pedido.getDescricao() != null && !pedido.getDescricao().isEmpty()) {
			pedidoSessionBean.create(pedido);
		} else {
			throw new CrudException();
		}

	}

	@Override
	protected void carregaLazyDataModel() {
		setLazyDataModel(new LazyEntityDataModel<Pedido>() {
			@Override
			protected List<Pedido> consultaFiltrada(ConsultaLazyFilter filtro) {
				filtro.setDescricao(getDescricaoFiltro());
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return pedidoSessionBean.filtrados(filtro);
			}

			@Override
			protected int countTotalRegistros(ConsultaLazyFilter filtro) {
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return pedidoSessionBean.quantidadeFiltrados(filtro);
			}
		});
	}

	@Override
	protected void antesDeExcluir() throws CrudException {
		getObjetoSelecionado().setDataExclusao(new Date());
	}

	@Override
	protected void aposExcluir() throws CrudException {
		super.buscarPorLazy();
	}

	@Override
	protected void aposCancelar() throws CrudException {
		getObjetoSelecionado().setDescricao(null);
		updateContext(PEDIDO_FORM);
		super.aposCancelar();
	}

	@Override
	protected Pedido inicializaObjetosLazy(Pedido objeto) {
		pedidoSessionBean.clearSession();
		return pedidoSessionBean.retrieve(objeto.getId());
	}

	@Override
	protected Pedido instanciarObjetoNovo() {
		return new Pedido();
	}

	@Override
	protected void setEmpresaDeUsuarioLogado(Pedido objetoSelecionado) {
		objetoSelecionado.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
	}

	private Pedido getRegistroComExclusaoLogica() {
		return pedidoSessionBean.findByDescricao(getObjetoSelecionado().getDescricao(), getStateManager().getEmpresaSelecionada().getId());
	}

	/**
	 *
	 * @return
	 */
	public String goToFormulario() {
		return PAGINA_FORMULARIO;
	}

	@Override
	protected void addChaveAcesso() {
		setChaveAcesso(CHAVE_ACESSO);
	}

	@Override
	protected boolean isPodeAlterar(){
		Direito direito = direitoSessionBean.retrieve(ALTERAR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

	@Override
	protected boolean isPodeExcluir(){
		Direito direito = direitoSessionBean.retrieve(EXCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

	@Override
	protected boolean isPodeIncluir(){
		Direito direito = direitoSessionBean.retrieve(INCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

}
