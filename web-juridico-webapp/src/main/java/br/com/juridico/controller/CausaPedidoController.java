package br.com.juridico.controller;

import br.com.juridico.datamodel.LazyEntityDataModel;
import br.com.juridico.entidades.CausaPedido;
import br.com.juridico.entidades.Direito;
import br.com.juridico.exception.CrudException;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.impl.CausaPedidoSessionBean;
import br.com.juridico.impl.DireitoSessionBean;
import br.com.juridico.impl.PerfilAcessoSessionBean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Marcos
 *
 */
@ManagedBean
@SessionScoped
public class CausaPedidoController extends CrudController<CausaPedido> {

	/**
	 *
	 */
	private static final long serialVersionUID = 6534052822350340302L;
	private static final String CAUSA_PEDIDO_FORM = "causaPedidoForm";
	private static final String CHAVE_ACESSO = "CAUSAPEDIDO_CAUSA_PEDIDO_TEMPLATE";
	private static final String PAGINA_FORMULARIO = "/pages/cadastros/causaPedido/causa-pedido-template.xhtml?faces-redirect=true";

	@Inject
	private CausaPedidoSessionBean causaPedidoSessionBean;
	@Inject
	private PerfilAcessoSessionBean perfilAcessoSessionBean;
	@Inject
	private DireitoSessionBean direitoSessionBean;

	public void init(){
		carregaLazyDataModel();
	}

	@Override
	protected void validar() throws ValidationException {
		causaPedidoSessionBean.clearSession();
		boolean jaExisteCausaPedido = causaPedidoSessionBean.isDescricaoJaCadastrada(getObjetoSelecionado(), getStateManager().getEmpresaSelecionada().getId());
		getObjetoSelecionado().validar(jaExisteCausaPedido);
	}


	@Override
	protected void create(CausaPedido causaPedido) throws ValidationException, CrudException {
		CausaPedido classeExclusao = getRegistroComExclusaoLogica();

		if (isEditavel() && classeExclusao != null) {
			classeExclusao.setDataExclusao(null);
			causaPedidoSessionBean.update(classeExclusao);
		} else if (causaPedido != null && causaPedido.getDescricao() != null && !causaPedido.getDescricao().isEmpty()) {
			causaPedidoSessionBean.create(causaPedido);
		} else {
			throw new CrudException();
		}

	}

	@Override
	protected void aposCancelar() throws CrudException {
		getObjetoSelecionado().setDescricao(null);
		updateContext(CAUSA_PEDIDO_FORM);
		super.aposCancelar();
	}

	public boolean isHasAccess(){
		return possuiAcesso(CHAVE_ACESSO);
	}

	private CausaPedido getRegistroComExclusaoLogica() {
		return causaPedidoSessionBean.findByDescricao(getObjetoSelecionado().getDescricao(), getStateManager().getEmpresaSelecionada().getId());
	}

	@Override
	protected void antesDeExcluir() throws CrudException {
		getObjetoSelecionado().setDataExclusao(new Date());
	}

	@Override
	protected void aposExcluir() throws CrudException {
		carregaLazyDataModel();
	}

	@Override
	protected void carregaLazyDataModel() {
		setLazyDataModel(new LazyEntityDataModel<CausaPedido>() {
			@Override
			protected List<CausaPedido> consultaFiltrada(ConsultaLazyFilter filtro) {
				filtro.setDescricao(getDescricaoFiltro());
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return causaPedidoSessionBean.filtrados(filtro);
			}

			@Override
			protected int countTotalRegistros(ConsultaLazyFilter filtro) {
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return causaPedidoSessionBean.quantidadeFiltrados(filtro);
			}
		});
	}

	/**
	 *
	 * @return
	 */
	public String goToFormulario() {
		return PAGINA_FORMULARIO;
	}

	@Override
	protected CausaPedido inicializaObjetosLazy(CausaPedido objeto) {
		causaPedidoSessionBean.clearSession();
		return causaPedidoSessionBean.retrieve(objeto.getId());
	}

	@Override
	protected CausaPedido instanciarObjetoNovo() {
		return new CausaPedido();
	}

	@Override
	protected void setEmpresaDeUsuarioLogado(CausaPedido objetoSelecionado) {
		objetoSelecionado.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
	}

	@Override
	protected void addChaveAcesso() {
		setChaveAcesso(CHAVE_ACESSO);
	}

	@Override
	protected boolean isPodeAlterar(){
		Direito direito = direitoSessionBean.retrieve(ALTERAR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

	@Override
	protected boolean isPodeExcluir(){
		Direito direito = direitoSessionBean.retrieve(EXCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

	@Override
	protected boolean isPodeIncluir(){
		Direito direito = direitoSessionBean.retrieve(INCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}
}
