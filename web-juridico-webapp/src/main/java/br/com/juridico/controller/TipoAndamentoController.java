package br.com.juridico.controller;

import br.com.juridico.datamodel.LazyEntityDataModel;
import br.com.juridico.entidades.Direito;
import br.com.juridico.entidades.SubAndamento;
import br.com.juridico.entidades.TipoAndamento;
import br.com.juridico.entidades.TipoAndamentoSubAndamento;
import br.com.juridico.exception.CrudException;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.impl.DireitoSessionBean;
import br.com.juridico.impl.PerfilAcessoSessionBean;
import br.com.juridico.impl.SubAndamentoSessionBean;
import br.com.juridico.impl.TipoAndamentoSessionBean;
import com.google.common.collect.Lists;
import org.primefaces.model.DualListModel;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author Marcos
 *
 */
@ManagedBean
@SessionScoped
public class TipoAndamentoController extends CrudController<TipoAndamento> {

	private static final String TIPO_ANDAMENTO_FORM = "tipoAndamentoForm";
	private static final String CHAVE_ACESSO = "TIPOANDAMENTO_TIPO_ANDAMENTO_TEMPLATE";
	private static final String PAGINA_FORMULARIO = "/pages/cadastros/tipoAndamento/tipo-andamento-template.xhtml?faces-redirect=true";

	@Inject
	private TipoAndamentoSessionBean tipoAndamentoSessionBean;
	@Inject
	private PerfilAcessoSessionBean perfilAcessoSessionBean;
	@Inject
	private DireitoSessionBean direitoSessionBean;
	@Inject
	private SubAndamentoSessionBean subAndamentoSessionBean;

	private DualListModel<SubAndamento> subAndamentos;
	private List<SubAndamento> paramsSource;
	private List<SubAndamento> paramsTarget;

	public void init(){
		carregaLazyDataModel();
	}

	@Override
	protected void validar() throws ValidationException {
		tipoAndamentoSessionBean.clearSession();
		boolean jaExisteTipoAndamento = tipoAndamentoSessionBean.isDescricaoJaCadastrada(getObjetoSelecionado(), getStateManager().getEmpresaSelecionada().getId());
		getObjetoSelecionado().validar(jaExisteTipoAndamento);
	}

	@Override
	protected void create(TipoAndamento tipoAndamento) throws ValidationException, CrudException {
		TipoAndamento tipoAndamentoExclusao = getRegistroComExclusaoLogica();
		if (isEditavel() && tipoAndamentoExclusao != null) {
			getObjetoSelecionado().setDataExclusao(null);
			adicionaSubAndamentos();
			tipoAndamentoSessionBean.update(getObjetoSelecionado());
			tipoAndamentoSessionBean.clearSession();
		} else if (tipoAndamento != null && tipoAndamento.getDescricao() != null && !tipoAndamento.getDescricao().isEmpty()) {
			adicionaSubAndamentos();
			tipoAndamentoSessionBean.create(tipoAndamento);
		} else {
			throw new CrudException();
		}
		carregaLazyDataModel();

	}

	private void adicionaSubAndamentos(){
		int posicao = 0;

		for (SubAndamento subAndamento : subAndamentos.getTarget()) {
			TipoAndamentoSubAndamento sub = new TipoAndamentoSubAndamento(getObjetoSelecionado(), subAndamento, posicao);
			getObjetoSelecionado().getSubAndamentos().add(posicao, sub);
			posicao++;
		}

		int contador = 1;
		Iterator itr = getObjetoSelecionado().getSubAndamentos().iterator();
		while (itr.hasNext()){
			TipoAndamentoSubAndamento sub = (TipoAndamentoSubAndamento) itr.next();
			if(contador > subAndamentos.getTarget().size()){
				itr.remove();
			}
			contador++;
		}
	}

	private TipoAndamento getRegistroComExclusaoLogica() {
		return tipoAndamentoSessionBean.findByDescricao(getObjetoSelecionado().getDescricao(), getStateManager().getEmpresaSelecionada().getId());
	}

	@Override
	protected void antesInserir() throws CrudException {
		initSubAndamentos();
		getObjetoSelecionado().setCriaEventoFlag(Boolean.FALSE);
		super.antesInserir();
	}

	@Override
	protected void antesDeEditar(TipoAndamento objeto) throws CrudException {
		initSubAndamentos();
		this.paramsTarget.addAll(objeto.getSubAndamentos().stream().map(TipoAndamentoSubAndamento::getSubAndamento).collect(Collectors.toList()));
		for (TipoAndamentoSubAndamento item : objeto.getSubAndamentos()) {
			this.paramsSource.remove(item.getSubAndamento());
		}
		super.antesDeEditar(objeto);
	}

	@Override
	protected void carregaLazyDataModel() {
		tipoAndamentoSessionBean.clearSession();
		setLazyDataModel(new LazyEntityDataModel<TipoAndamento>() {
			@Override
			protected List<TipoAndamento> consultaFiltrada(ConsultaLazyFilter filtro) {
				filtro.setDescricao(getDescricaoFiltro());
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return tipoAndamentoSessionBean.filtrados(filtro);
			}

			@Override
			protected int countTotalRegistros(ConsultaLazyFilter filtro) {
				filtro.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());
				return tipoAndamentoSessionBean.quantidadeFiltrados(filtro);
			}
		});
	}

	private void initSubAndamentos() {
		this.paramsSource = subAndamentoSessionBean.retrieveAll(getStateManager().getEmpresaSelecionada().getId());
		this.paramsTarget = Lists.newArrayList();
		subAndamentos = new DualListModel<>(paramsSource, paramsTarget);
	}

	@Override
	protected void antesDeExcluir() throws CrudException {
		getObjetoSelecionado().getSubAndamentos().clear();
		getObjetoSelecionado().setDataExclusao(new Date());
	}

	@Override
	protected void aposExcluir() throws CrudException {
		super.buscarPorLazy();
	}

	@Override
	protected void aposCancelar() throws CrudException {
		getObjetoSelecionado().setDescricao(null);
		updateContext(TIPO_ANDAMENTO_FORM);
		super.aposCancelar();
	}

	/**
	 *
	 * @return
	 */
	public String goToFormulario() {
		return PAGINA_FORMULARIO;
	}

	public void criarEventoChanged(ValueChangeEvent event){
		getObjetoSelecionado().setCriaEventoFlag((Boolean) event.getNewValue());
	}

	@Override
	protected TipoAndamento inicializaObjetosLazy(TipoAndamento objeto) {
		tipoAndamentoSessionBean.clearSession();
		return tipoAndamentoSessionBean.retrieve(objeto.getId());
	}

	@Override
	protected TipoAndamento instanciarObjetoNovo() {
		return new TipoAndamento();
	}

	@Override
	protected void setEmpresaDeUsuarioLogado(TipoAndamento objetoSelecionado) {
		objetoSelecionado.setCodigoEmpresa(getStateManager().getEmpresaSelecionada().getId());

	}

	@Override
	protected void addChaveAcesso() {
		setChaveAcesso(CHAVE_ACESSO);
	}

	@Override
	protected boolean isPodeAlterar(){
		Direito direito = direitoSessionBean.retrieve(ALTERAR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

	@Override
	protected boolean isPodeExcluir(){
		Direito direito = direitoSessionBean.retrieve(EXCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

	@Override
	protected boolean isPodeIncluir(){
		Direito direito = direitoSessionBean.retrieve(INCLUIR);
		return perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), CHAVE_ACESSO, direito, getUser().getCodEmpresa());
	}

	public DualListModel<SubAndamento> getSubAndamentos() {
		return subAndamentos;
	}

	public void setSubAndamentos(DualListModel<SubAndamento> subAndamentos) {
		this.subAndamentos = subAndamentos;
	}
}
