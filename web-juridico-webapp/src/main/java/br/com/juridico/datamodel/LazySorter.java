package br.com.juridico.datamodel;

import br.com.juridico.dao.IEntity;
import org.primefaces.model.SortOrder;

import java.util.Comparator;

/**
 * Created by jeffersonl2009_00 on 12/07/2016.
 */
public class LazySorter<T extends IEntity> implements Comparator<T> {

    private String sortField;

    private SortOrder sortOrder;

    public LazySorter(String sortField, SortOrder sortOrder) {
        this.sortField = sortField;
        this.sortOrder = sortOrder;
    }

    public int compare(T o1, T o2) {
        try {
            Object value1 = IEntity.class.getField(this.sortField).get(o1);
            Object value2 = IEntity.class.getField(this.sortField).get(o2);

            int value = ((Comparable)value1).compareTo(value2);

            return SortOrder.ASCENDING.equals(sortOrder) ? value : -1 * value;
        }
        catch(Exception e) {
            throw new RuntimeException();
        }
    }
}
