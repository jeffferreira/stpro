package br.com.juridico.datamodel;

import br.com.juridico.dao.IEntity;
import br.com.juridico.filter.ConsultaLazyFilter;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortMeta;
import org.primefaces.model.SortOrder;

import java.util.List;
import java.util.Map;

/**
 * Created by jeffersonl2009_00 on 12/07/2016.
 */
public abstract class LazyEntityDataModel<T extends IEntity> extends LazyDataModel<T> {

    private List<T> dataSource;
    private ConsultaLazyFilter filtro = new ConsultaLazyFilter();

    @Override
    public T getRowData(String rowKey) {
        for(T object : dataSource) {
            if(object.getId().equals(rowKey))
                return object;
        }

        return null;
    }

    @Override
    public Object getRowKey(T object) {
        return object.getId();
    }

    @Override
    public List<T> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
        filtro.setPrimeiroRegistro(first);
        filtro.setQuantidadeRegistros(pageSize);
        filtro.setAscendente(SortOrder.ASCENDING.equals(sortOrder));
        filtro.setPropriedadeOrdenacao(sortField);
        setRowCount(countTotalRegistros(filtro));

        return consultaFiltrada(filtro);
    }

    protected abstract List<T> consultaFiltrada(ConsultaLazyFilter filtro);
    protected abstract int countTotalRegistros(ConsultaLazyFilter filtro);

    @Override
    public List<T> load(int first, int pageSize, List<SortMeta> multiSortMeta, Map<String, Object> filters) {
        throw new UnsupportedOperationException("Lazy loading is not implemented.");
    }
}
