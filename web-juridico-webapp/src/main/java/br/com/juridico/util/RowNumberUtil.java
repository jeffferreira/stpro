package br.com.juridico.util;

/**
 * 
 * @author Jefferson
 *
 */
public class RowNumberUtil {

    public static final Integer VALOR_UM = Integer.valueOf(1);
    public static final Integer VALOR_DOIS = Integer.valueOf(2);
    public static final Integer VALOR_TRES = Integer.valueOf(3);
    public static final Integer VALOR_QUATRO = Integer.valueOf(4);
    public static final Integer VALOR_CINCO = Integer.valueOf(5);
    
    private RowNumberUtil(){}

}
