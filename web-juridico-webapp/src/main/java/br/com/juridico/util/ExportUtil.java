package br.com.juridico.util;

import br.com.juridico.exception.StproException;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.oasis.JROdsExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import org.apache.log4j.Logger;

import javax.faces.context.FacesContext;
import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author jefferson.ferreira
 *
 */
public class ExportUtil {

	private static final Logger LOGGER = Logger.getLogger(ExportUtil.class);

	private static final String APPLICATION_PDF = "application/pdf";
	private static final String CONTENT_DISPOSITION = "Content-Disposition";
	private static final String FILE_LOCATION = "fileLocation";
	private static final String NAO_FOI_POSSIVEL_GERAR_O_RELATORIO = "Nao foi possivel gerar o relatorio.";
	private static final String ARQUIVO_DO_RELATORIO_NAO_ENCONTRADO = "Arquivo do relatorio nao encontrado.";
	private static final String FALHA_AO_GERAR_RELATORIO = "Falha ao gerar relatorio.";
	private static final String JASPER = ".jasper";
	private static final String PDF = ".pdf";
	private static final String ERROR_MSG = "Error: ";
	private static final String JRXML = ".jrxml";

	private ExportUtil() {
		super();
	}

	/**
	 *
	 * @param relatorioJrxml
	 * @param nomeRelatorio
	 * @param lista
	 * @param pathRel
	 * @param parameters
	 * @throws StproException
	 */
	public static void exportarRelatorioPdf(String relatorioJrxml, String nomeRelatorio, List lista, String pathRel,
											Map<String, Object> parameters) throws StproException {

		try {

			JRPdfExporter pdfExporter = new JRPdfExporter();
			String caminhoArquivoJasper = pathRel + File.separator + relatorioJrxml + JRXML;

			FacesContext context = FacesContext.getCurrentInstance();
			HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
			ServletOutputStream responseStream = response.getOutputStream();

			JasperReport pathReport = JasperCompileManager.compileReport(caminhoArquivoJasper);
			JRBeanCollectionDataSource jrb = new JRBeanCollectionDataSource(lista);
			JasperPrint print = JasperFillManager.fillReport(pathReport, parameters, jrb);

			response.setContentType(APPLICATION_PDF);
			response.setHeader(CONTENT_DISPOSITION, "attachment; filename=\"" + nomeRelatorio + ".pdf\" ");

			pdfExporter.setExporterInput(new SimpleExporterInput(print));
			pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(responseStream));
			pdfExporter.exportReport();

			context.getApplication().getStateManager().saveView(context);
			context.responseComplete();

		} catch (JRException e) {
			LOGGER.error(ERROR_MSG + e);
			throw new StproException(NAO_FOI_POSSIVEL_GERAR_O_RELATORIO);
		} catch (FileNotFoundException e) {
			LOGGER.error(ERROR_MSG + e);
			throw new StproException(ARQUIVO_DO_RELATORIO_NAO_ENCONTRADO);
		} catch (Exception e) {
			LOGGER.error(ERROR_MSG + e);
			throw new StproException(FALHA_AO_GERAR_RELATORIO);
		}
	}

	/**
	 *
	 * @param relatorio
	 * @param lista
	 * @param pathRel
	 * @param fileName
	 * @param parameters
	 * @throws StproException
	 */
	public static void gerarPdfComSubreport(String[] relatorio, Collection lista, String pathRel, String fileName,
											Map<String, Object> parameters) throws StproException {

		StringBuilder jrxmlFile = new StringBuilder();
		jrxmlFile.append(pathRel);
		jrxmlFile.append(File.separator);
		jrxmlFile.append(relatorio[0]);
		jrxmlFile.append(JRXML);

		try {
			JasperReport compiledReport;
			compiledReport = JasperCompileManager.compileReport(jrxmlFile.toString());

			for (String file : relatorio) {
				if (!file.equals(relatorio[0])) {
					JasperCompileManager.compileReportToFile(pathRel + File.separator + file + JRXML,
							pathRel + File.separator + file + JASPER);
				}
			}

			parameters.put(FILE_LOCATION, pathRel.concat(File.separator));

			JRPdfExporter pdfExporter = new JRPdfExporter();
			FacesContext context = FacesContext.getCurrentInstance();
			JRDataSource dataSource = getJRDataSource(lista);
			JasperPrint print = JasperFillManager.fillReport(compiledReport, parameters, dataSource);
			HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();

			File documentoPdf = File.createTempFile(fileName, PDF);
			JasperExportManager.exportReportToPdfStream(print, new FileOutputStream(documentoPdf));
			ServletOutputStream responseStream = response.getOutputStream();

			response.setContentType(APPLICATION_PDF);
			response.setHeader(CONTENT_DISPOSITION, "attachment; filename=\"" + fileName + ".pdf\" ");

			pdfExporter.setExporterInput(new SimpleExporterInput(print));
			pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(responseStream));
			pdfExporter.exportReport();

			context.getApplication().getStateManager().saveView(context);
			context.responseComplete();

		} catch (JRException e) {
			LOGGER.debug(e, e.getCause());
			throw new StproException(NAO_FOI_POSSIVEL_GERAR_O_RELATORIO);
		} catch (FileNotFoundException e) {
			LOGGER.debug(e, e.getCause());
			throw new StproException(ARQUIVO_DO_RELATORIO_NAO_ENCONTRADO);
		} catch (Exception e) {
			LOGGER.debug(e, e.getCause());
			throw new StproException(FALHA_AO_GERAR_RELATORIO);
		}
	}

	private static JRDataSource getJRDataSource(Object object) throws SQLException {

		if (object instanceof ResultSet) {
			return ((ResultSet) object).first() ? new JRResultSetDataSource((ResultSet) object) : null;
		}
		if (object instanceof Collection) {
			return !(((Collection) object).isEmpty()) ? new JRBeanCollectionDataSource((Collection) object, false)
					: null;
		}
		return null;
	}

	/**
	 *
	 * @return
	 */
	public static BufferedImage getLogo(File file) throws IOException {
		return ImageIO.read(file);
	}

}
