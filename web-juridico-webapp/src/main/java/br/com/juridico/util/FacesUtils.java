package br.com.juridico.util;

import com.google.common.base.Strings;
import com.sun.faces.component.visit.FullVisitContext;
import org.primefaces.context.RequestContext;

import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.component.visit.VisitCallback;
import javax.faces.component.visit.VisitContext;
import javax.faces.component.visit.VisitResult;
import javax.faces.context.FacesContext;

/**
 * Created by Jefferson on 30/03/2016.
 */
public class FacesUtils {

    private static final String STYLE_CLASS = "styleClass";

    protected FacesUtils() {

    }

    public static UIComponent findComponent(final String component) {
        if (Strings.isNullOrEmpty(component)) {
            return null;
        }

        UIViewRoot viewRoot = FacesContext.getCurrentInstance().getViewRoot();
        final UIComponent[] res = new UIComponent[1];
        viewRoot.visitTree(new FullVisitContext(FacesContext.getCurrentInstance()), new VisitCallback() {
            @Override
            public VisitResult visit(VisitContext context, UIComponent target) {
                if (component.equals(target.getId()) || component.equals(target.getAttributes().get("name"))) {
                    res[0] = target;
                    return VisitResult.COMPLETE;
                }
                return VisitResult.ACCEPT;
            }
        });
        return res[0];
    }

    public static UIComponent addCSSClass(UIComponent component, String cssClass) {
        String className = (String) component.getAttributes().get(STYLE_CLASS);
        component.getAttributes().put(STYLE_CLASS, className + " " + cssClass);
        return component;
    }

    public static UIComponent removeCSSClass(UIComponent component, String cssClass) {
        String className = (String) component.getAttributes().get(STYLE_CLASS);
        component.getAttributes().put(STYLE_CLASS, className.replace(cssClass, ""));
        return component;
    }

    public static void update(UIComponent uiComponent) {
        RequestContext.getCurrentInstance().update(uiComponent.getId());
    }

    public static void update(String componentId) {
        RequestContext.getCurrentInstance().update(componentId);
    }

    public static void execute(String command) {
        RequestContext.getCurrentInstance().execute(command);
    }

    public static void scrollToTop() {
        RequestContext currentInstance = RequestContext.getCurrentInstance();
        if (currentInstance != null){
            currentInstance.scrollTo("navbar-header");
        }
    }

}
