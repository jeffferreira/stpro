package br.com.juridico.util;

import java.util.List;

import com.google.common.collect.Lists;

public class GeradorScript {

	private GeradorScript() {}

	public static void main(String[] args) {

		List<String> cidades = Lists.newArrayList();

		cidades.add("Santa Luzia do Paruá");
		cidades.add("Santa Quitéria do Maranhão");
		cidades.add("Santa Rita");
		cidades.add("Santana do Maranhão");
		cidades.add("Santo Amaro do Maranhão");
		cidades.add("Santo Antônio dos Lopes");
		cidades.add("São Benedito do Rio Preto");
		cidades.add("São Bento");
		cidades.add("São Bernardo");
		cidades.add("São Domingos do Azeitão");
		cidades.add("São Domingos do Maranhão");
		cidades.add("São Félix de Balsas");
		cidades.add("São Francisco do Brejão");
		cidades.add("São Francisco do Maranhão");
		cidades.add("São João Batista");
		cidades.add("São João do Carú");
		cidades.add("São João do Paraíso");
		cidades.add("São João do Soter");
		cidades.add("São João dos Patos");
		cidades.add("São José de Ribamar");
		cidades.add("São José dos Basílios");
		cidades.add("São Luís");
		cidades.add("São Luís Gonzaga do Maranhão");
		cidades.add("São Mateus do Maranhão");
		cidades.add("São Pedro da Água Branca");
		cidades.add("São Pedro dos Crentes");
		cidades.add("São Raimundo das Mangabeiras");
		cidades.add("São Raimundo do Doca Bezerra");
		cidades.add("São Roberto");
		cidades.add("São Vicente Ferrer");
		cidades.add("Satubinha");
		cidades.add("Senador Alexandre Costa");
		cidades.add("Senador La Rocque");
		cidades.add("Serrano do Maranhão");
		cidades.add("Sítio Novo");
		cidades.add("Sucupira do Norte");
		cidades.add("Sucupira do Riachão");
		cidades.add("Tasso Fragoso");
		cidades.add("Timbiras");
		cidades.add("Timon");
		cidades.add("Trizidela do Vale");
		cidades.add("Tufilândia");
		cidades.add("Tuntum");
		cidades.add("Turiaçu");
		cidades.add("Turilândia");
		cidades.add("Tutóia");
		cidades.add("Urbano Santos");
		cidades.add("Vargem Grande");
		cidades.add("Viana");
		cidades.add("Vila Nova dos Martírios");
		cidades.add("Vitória do Mearim");
		cidades.add("Vitorino Freire");
		cidades.add("Zé Doca");

		geraSql(cidades, 10, 1692);

	}

	private static void geraSql(List<String> valores, int idUf, int nextVal) {
		StringBuilder sb = new StringBuilder();

		for (String value : valores) {
			sb.append("INSERT INTO `tb_cidade` VALUES (" + nextVal + ", '" + value + "', " + idUf + "); \n");
			nextVal++;
		}
	}
}
