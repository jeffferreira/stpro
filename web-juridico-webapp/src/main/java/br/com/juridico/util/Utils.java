package br.com.juridico.util;

import javax.servlet.http.Part;

/**
 * 
 * @author Jefferson
 *
 */
public class Utils {

	private Utils() {
	}

	/**
	 * 
	 * @param part
	 * @return
	 */
	public static String getFileNameFromPart(Part part) {
		final String partHeader = part.getHeader("content-disposition");
		for (String content : partHeader.split(";")) {
			if (content.trim().startsWith("filename")) {
				return content.substring(content.indexOf('=') + 1).trim().replace("\"", "");
			}
		}
		return null;
	}
}
