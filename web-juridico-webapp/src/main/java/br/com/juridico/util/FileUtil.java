package br.com.juridico.util;

import org.apache.commons.io.output.ByteArrayOutputStream;

import javax.servlet.http.Part;
import java.io.*;
import java.util.Iterator;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 *
 * @author Jefferson
 *
 */
public class FileUtil {

	private FileUtil() {
	}

	/**
	 *
	 * @param bytes
	 * @param nomeArquivo
	 * @param extensao
	 * @return
	 * @throws IOException
	 */
	public static File getArquivo(byte[] bytes, String nomeArquivo, String extensao) throws IOException {
		File arquivo = File.createTempFile(nomeArquivo, extensao);

		BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(arquivo));
		bos.write(bytes);
		bos.close();

		return arquivo;
	}

	/**
	 *
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public static byte[] getContent(File file) throws IOException {
		ByteArrayOutputStream ous = null;
		InputStream fis = null;
		try {
			byte[] content = new byte[(int) file.length()];
			ous = new ByteArrayOutputStream();
			fis = new FileInputStream(file);
			int read = 0;
			while ((read = fis.read(content)) != -1) {
				ous.write(content, 0, read);
			}
		} finally {
			if (ous != null)
				ous.close();
			if (fis != null)
				fis.close();

		}
		return ous.toByteArray();
	}

	/**
	 *
	 * @param part
	 * @return
	 */
	public static String getFileName(Part part) throws UnsupportedEncodingException {
		String header = part.getHeader( "content-disposition" );
		String str = new String(header.getBytes("ISO-8859-1"), "UTF-8");
		for( String tmp : str.split(";") ){
			if( tmp.trim().startsWith("filename") ){

				return tmp.substring( tmp.indexOf("=")+2 , tmp.length()-1 );
			}
		}
		return null;
	}

	public static byte[] createZip(Map files) throws IOException {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ZipOutputStream zipfile = new ZipOutputStream(bos);
		Iterator i = files.keySet().iterator();

		while (i.hasNext()) {
			String fileName = (String) i.next();
			ZipEntry zipentry = new ZipEntry(fileName);
			zipfile.putNextEntry(zipentry);
			zipfile.write((byte[]) files.get(fileName));
		}
		zipfile.close();
		return bos.toByteArray();
	}

}
