/**
 * 
 */
package br.com.juridico.util;

import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.*;

/**
 * @author jefferson.ferreira
 *
 */
public abstract class XlsUtil {

    private XlsUtil() {
    }

    /**
     * 
     * @param wb
     * @param fonte
     * @param tamanhoFonte
     * @param isBold
     * @return
     */
    public static XSSFCellStyle styleCells(
            XSSFWorkbook wb, String fonte, int tamanhoFonte, boolean isBold) {

        XSSFCellStyle cellStyle = wb.createCellStyle();
        XSSFFont font = wb.createFont();
        font.setFontName(fonte);

        font.setFontHeightInPoints((short) tamanhoFonte);

        if (isBold) {
            font.setBold(true);
        }
        cellStyle.setFont(font);

        return cellStyle;
    }

    /**
     * 
     * @param row
     * @param styleSubHeader
     * @param row1
     * @param row2
     * @param column1
     * @param column2
     */
    public static void mergedRegion(XSSFRow row, XSSFCellStyle styleSubHeader, int row1, int row2, int column1, int column2) {
        CellRangeAddress region = new CellRangeAddress(row1, row2, column1, column2);
        cleanBeforeMergeOnValidCells(row.getSheet(), region, styleSubHeader);
        row.getSheet().addMergedRegion(region);

    }

    private static void cleanBeforeMergeOnValidCells(XSSFSheet sheet, CellRangeAddress region, XSSFCellStyle cellStyle) {

        for (int rowNum = region.getFirstRow(); rowNum <= region.getLastRow(); rowNum++) {
            XSSFRow row = sheet.getRow(rowNum);
            if (row == null) {
                sheet.createRow(rowNum);
            }
            for (int colNum = region.getFirstColumn(); colNum <= region.getLastColumn(); colNum++) {
                XSSFCell currentCell = row.getCell(colNum);
                if (currentCell == null) {
                    currentCell = row.createCell(colNum);
                }

                currentCell.setCellStyle(cellStyle);
            }
        }

    }
}
