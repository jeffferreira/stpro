package br.com.juridico.types;

import com.google.common.base.Strings;

/**
 * Created by jefferson on 26/11/2016.
 */
public enum DetalheProcessoStepsTypes {

    STEP_ANDAMENTO("STEP_ANDAMENTO", "active", "disabled"),
    STEP_PRAZOS("STEP_PRAZOS", "complete", "active");

    private String value;
    private String wizardAndamento;
    private String wizardPrazos;

    DetalheProcessoStepsTypes(String value, String wizardAndamento, String wizardPrazos) {
        this.value = value;
        this.wizardAndamento = wizardAndamento;
        this.wizardPrazos = wizardPrazos;
    }

    /**
     *
     * @param step
     * @return
     */
    public static DetalheProcessoStepsTypes fromStep(String step){
        if(Strings.isNullOrEmpty(step)){
            return null;
        }

        for (DetalheProcessoStepsTypes type : values()) {
            if(type.getValue().equals(step)){
                return type;
            }
        }
        return null;
    }

    public final String getValue() {
        return value;
    }

    public String getWizardAndamento() {
        return wizardAndamento;
    }

    public String getWizardPrazos() {
        return wizardPrazos;
    }
}
