package br.com.juridico.types;

import com.google.common.base.Strings;

/**
 * Created by marcos on 22/04/2018.
 */
public enum EmailStepsTypes {

    STEP_CONFIGURACAO("STEP_CONFIGURACAO", "active", "disabled"),
    STEP_EMAIL("STEP_EMAIL", "complete", "active");

    private String value;
    private String wizardConfiguracao;
    private String wizardEmail;

    EmailStepsTypes(String value, String wizardConfiguracao, String wizardEmail) {
        this.value = value;
        this.wizardConfiguracao = wizardConfiguracao;
        this.wizardEmail = wizardEmail;
    }

    /**
     *
     * @param step
     * @return
     */
    public static EmailStepsTypes fromStep(String step){
        if(Strings.isNullOrEmpty(step)){
            return null;
        }

        for (EmailStepsTypes type : values()) {
            if(type.getValue().equals(step)){
                return type;
            }
        }
        return null;
    }

    public final String getValue() {
        return value;
    }

    public String getWizardConfiguracao() {
        return wizardConfiguracao;
    }

    public String getWizardEmail() {
        return wizardEmail;
    }
}
