package br.com.juridico.types;

import com.google.common.base.Strings;

/**
 * Created by jefferson on 26/11/2016.
 */
public enum ProcessoStepsTypes {

    STEP_PARTES("STEP_PARTES", "active", "disabled", "disabled", "disabled", "disabled", "disabled"),
    STEP_DADOS_PROCESSO("STEP_DADOS_PROCESSO", "complete", "active", "disabled", "disabled", "disabled", "disabled"),
    STEP_RESUMO_ACAO("STEP_RESUMO_ACAO", "complete", "complete", "active", "disabled", "disabled", "disabled"),
    STEP_VALORES("STEP_VALORES", "complete", "complete", "complete", "active", "disabled", "disabled"),
    STEP_HONORARIOS("STEP_HONORARIOS", "complete", "complete", "complete", "complete", "active", "disabled"),
    STEP_CONFIRMACAO("STEP_CONFIRMACAO", "complete", "complete", "complete", "complete", "complete", "active"),
    STEP_EDICAO("STEP_EDICAO", "complete", "complete", "complete", "complete", "complete", "complete");

    private String value;
    private String wizardParte;
    private String wizardDadosProcesso;
    private String wizardResumoAcao;
    private String wizardValores;
    private String wizardHonorarios;
    private String wizardConfirmacao;

    ProcessoStepsTypes(String value, String wizardParte, String wizardDadosProcesso, String wizardResumoAcao, String wizardValores, String wizardHonorarios, String wizardConfirmacao) {
        this.value = value;
        this.wizardParte = wizardParte;
        this.wizardDadosProcesso = wizardDadosProcesso;
        this.wizardResumoAcao = wizardResumoAcao;
        this.wizardValores = wizardValores;
        this.wizardHonorarios = wizardHonorarios;
        this.wizardConfirmacao = wizardConfirmacao;
    }

    /**
     *
     * @param step
     * @return
     */
    public static ProcessoStepsTypes fromStep(String step){
        if(Strings.isNullOrEmpty(step)){
            return null;
        }

        for (ProcessoStepsTypes type : values()) {
            if(type.getValue().equals(step)){
                return type;
            }
        }
        return null;
    }

    public final String getValue() {
        return value;
    }

    public String getWizardParte() {
        return wizardParte;
    }

    public String getWizardDadosProcesso() {
        return wizardDadosProcesso;
    }

    public String getWizardResumoAcao() {
        return wizardResumoAcao;
    }

    public String getWizardValores() {
        return wizardValores;
    }

    public String getWizardHonorarios() {
        return wizardHonorarios;
    }

    public String getWizardConfirmacao() {
        return wizardConfirmacao;
    }
}
