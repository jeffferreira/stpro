/**
 * 
 */
package br.com.juridico.report.util;

/**
 * @author jefferson.ferreira
 * 
 * Valores representam colunas do xls 
 */
public class ColunaTemplate {

    public static final int COLUNA_A = 0;
    public static final int COLUNA_B = 1;
    public static final int COLUNA_C = 2;
    public static final int COLUNA_D = 3;
    public static final int COLUNA_E = 4;
    public static final int COLUNA_F = 5;
    public static final int COLUNA_G = 6;
    public static final int COLUNA_H = 7;
    public static final int COLUNA_I = 8;
    public static final int COLUNA_J = 9;
    public static final int COLUNA_K = 10;
    
    private ColunaTemplate(){
    }
}
