/**
 *
 */
package br.com.juridico.report;

import br.com.juridico.builder.SimpleHtmlPdfReportBuilder;
import br.com.juridico.dto.ParteRelatorioDto;
import br.com.juridico.dto.PedidoRelatorioDto;
import br.com.juridico.entidades.RelatorioCriterioProcesso;
import br.com.juridico.entidades.ViewProcesso;
import br.com.juridico.enums.RelatorioProcessoCampoType;
import br.com.juridico.htmlfactory.HtmlFactory;
import br.com.juridico.util.DateUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.apache.log4j.Logger;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import static br.com.juridico.enums.RelatorioProcessoCampoType.*;

/**
 * @author jefferson.ferreira
 *
 */
public class ProcessoPdfFileReport {

    private static final Logger LOGGER = Logger.getLogger(ProcessoPdfFileReport.class);
    private static final String TEXT = "text";
    private static final String PERIODO_VALOR = "periodoValor";
    private static final String PERIODO_DATA = "periodoData";
    private static final DateFormat FORMATADOR_DATA_HORA_BRASIL = new SimpleDateFormat("dd/MM/yyyy HH:mm");

    private HtmlFactory factory;

    private final List<RelatorioCriterioProcesso> criterios;

    public ProcessoPdfFileReport(List<RelatorioCriterioProcesso> criterios){
        this.criterios = criterios;
    }

    public byte[] generateFile(List<ViewProcesso> lista, String titulo, boolean landscape, Map<String, String> parameters) throws Exception {
        String nomeRelatorio = "Relatório de processos";
        String data = FORMATADOR_DATA_HORA_BRASIL.format(new Date());
        String impressoPor = parameters.get("USUARIO").concat(" - ").concat(data);
        StringBuilder html = escreverHtmlInput(titulo, landscape, impressoPor, lista);
        SimpleHtmlPdfReportBuilder reportBuilder = new SimpleHtmlPdfReportBuilder(nomeRelatorio, html);
        return reportBuilder.build();
    }

    private StringBuilder escreverHtmlInput(String titulo, boolean landscape, String impressoPor, List<ViewProcesso> lista)  {
        this.factory = new HtmlFactory(titulo, landscape, impressoPor);
        criarTabelasDetalhes(lista);
        return this.factory.getHtml();
    }

    private void criarTabelasDetalhes(List<ViewProcesso> lista) {
        for (ViewProcesso dto : lista) {
            printDadosProcesso(dto);
            printPartesProcesso(dto);
            printResumoAcao(dto);
            printValoresProcesso(dto);
            printPedidosProcesso(dto);
        }
    }


    private void printValoresProcesso(ViewProcesso dto) {
        List<String> headers = headersValoresProcesso();
        if(headers.isEmpty()){
            return;
        }
        List<String> dados = criarValoresProcesso(dto);
        this.factory.addValores(headers, dados);
    }

    private List<String> headersValoresProcesso() {
        List<String> headers = Lists.newArrayList();
        headers.addAll(this.criterios.stream().filter(criterio -> RelatorioProcessoCampoType.isValoresProcesso(criterio.getNomeCampoView())
                && criterio.getMostrarNoRelatorio()
                && !Strings.isNullOrEmpty(criterio.getNomeCampoView())).map(RelatorioCriterioProcesso::getDescricao).collect(Collectors.toList()));
        return headers;
    }

    private List<String> criarValoresProcesso(ViewProcesso dto) {
        List<String> campos = Lists.newArrayList();

        campos.addAll(this.criterios.stream().filter(criterio -> RelatorioProcessoCampoType.isValoresProcesso(criterio.getNomeCampoView())
                && criterio.getMostrarNoRelatorio()
                && !Strings.isNullOrEmpty(criterio.getNomeCampoView())).map(criterio -> adicionarCamposPorTipo(dto, criterio)).collect(Collectors.toList()));
        return campos;
    }


    private void printPedidosProcesso(ViewProcesso dto) {
        List<String> headers = headersPedidosProcesso();
        if(headers.isEmpty()){
            return;
        }
        List<PedidoRelatorioDto> pedidos = criarPedidosProcesso(dto);
        this.factory.addPedidos(headers, pedidos);
    }

    private List<PedidoRelatorioDto> criarPedidosProcesso(ViewProcesso dto) {
        List<PedidoRelatorioDto> pedidos = Lists.newArrayList();


        this.criterios.stream().filter(criterio -> RelatorioProcessoCampoType.isPedidosProcesso(criterio.getNomeCampoView())
                && criterio.getMostrarNoRelatorio()
                && !Strings.isNullOrEmpty(criterio.getNomeCampoView())).forEach(criterio -> {

        });
        return pedidos;
    }

    private List<String> headersPedidosProcesso() {
        List<String> headers = Lists.newArrayList();
        headers.addAll(this.criterios.stream().filter(criterio -> RelatorioProcessoCampoType.isPedidosProcesso(criterio.getNomeCampoView())
                && criterio.getMostrarNoRelatorio()
                && !Strings.isNullOrEmpty(criterio.getNomeCampoView())).map(RelatorioCriterioProcesso::getDescricao).collect(Collectors.toList()));
        return headers;
    }

    private void printDadosProcesso(ViewProcesso dto) {
        List<String> headers = headersDadosProcesso();
        if(headers.isEmpty()){
            return;
        }
        List<String> dados = criarDadosProcesso(dto);
        this.factory.addDadosProcesso(headers, dados);
    }

    private List<String> headersDadosProcesso(){
        List<String> headers = Lists.newArrayList();
        headers.addAll(this.criterios.stream().filter(criterio -> RelatorioProcessoCampoType.isDadosProcesso(criterio.getNomeCampoView())
                && criterio.getMostrarNoRelatorio()
                && !Strings.isNullOrEmpty(criterio.getNomeCampoView())).map(RelatorioCriterioProcesso::getDescricao).collect(Collectors.toList()));
        return headers;
    }

    private List<String> criarDadosProcesso(ViewProcesso dto) {
        List<String> campos = Lists.newArrayList();

        campos.addAll(this.criterios.stream().filter(criterio -> RelatorioProcessoCampoType.isDadosProcesso(criterio.getNomeCampoView())
                && criterio.getMostrarNoRelatorio()
                && !Strings.isNullOrEmpty(criterio.getNomeCampoView())).map(criterio -> adicionarCamposPorTipo(dto, criterio)).collect(Collectors.toList()));
        return campos;
    }

    private void printResumoAcao(ViewProcesso dto) {
        this.criterios.stream().filter(criterio -> RelatorioProcessoCampoType.isResumoProcesso(criterio.getNomeCampoView())
                && criterio.getMostrarNoRelatorio()
                && !Strings.isNullOrEmpty(criterio.getNomeCampoView())).forEach(criterio -> {
            this.factory.addResumo(adicionarCamposPorTipo(dto, criterio));
        });
    }



    private void printPartesProcesso(ViewProcesso dto) {
        List<String> headers = headersPartesProcesso();
        if(headers.isEmpty()){
            return;
        }
        List<ParteRelatorioDto> partes = criarPartesProcesso(dto);
        this.factory.addPartes(headers, partes);
    }

    private List<String> headersPartesProcesso(){
        List<String> headers = Lists.newArrayList();
        headers.addAll(this.criterios.stream().filter(criterio -> RelatorioProcessoCampoType.isPartesProcesso(criterio.getNomeCampoView())
                && criterio.getMostrarNoRelatorio()
                && !Strings.isNullOrEmpty(criterio.getNomeCampoView())).map(RelatorioCriterioProcesso::getDescricao).collect(Collectors.toList()));
        return headers;
    }

    private List<ParteRelatorioDto> criarPartesProcesso(ViewProcesso dto) {
        List<ParteRelatorioDto> campos = Lists.newArrayList();

        this.criterios.stream().filter(criterio -> RelatorioProcessoCampoType.isPartesProcesso(criterio.getNomeCampoView())
                && criterio.getMostrarNoRelatorio()
                && !Strings.isNullOrEmpty(criterio.getNomeCampoView())).forEach(criterio -> {
            List<RelatorioCriterioProcesso> criterioAdvogados = this.criterios.stream().filter(c -> ADVOGADO.getNomeCampoView().equals(c.getNomeCampoView())).collect(Collectors.toList());

            if (CLIENTE.getDescricao().equals(criterio.getDescricao())) {
                dto.getClientes().stream().forEach(c -> campos.add(new ParteRelatorioDto(c.getCliente(), c.getPosicao(), "SIM", criterioAdvogados.isEmpty() ? "" : dto.getAdvogadosText())));
            } else if (ADVERSO.getDescricao().equals(criterio.getDescricao())) {
                dto.getAdversos().stream().forEach(a -> campos.add(new ParteRelatorioDto(a.getAdverso(), a.getPosicao(), "NÃO", "")));
            }
        });
        return campos;
    }

    private String adicionarCamposPorTipo(ViewProcesso dto, RelatorioCriterioProcesso c) {
        try {
            Field field = dto.getClass().getDeclaredField(c.getNomeCampoView());
            field.setAccessible(true);

            if(TEXT.equals(c.getTipoCampo())) {
                String result = (String)field.get(dto);
                return result == null ? "" : result.trim();
            } else if(PERIODO_VALOR.equals(c.getTipoCampo())) {
                return currencyFormat((BigDecimal) field.get(dto));
            } else if(PERIODO_DATA.equals(c.getTipoCampo())) {
                return DateUtils.formataDataBrasil((Date) field.get(dto));
            }

        } catch (NoSuchFieldException e) {
            LOGGER.error(e, e.getCause());
        } catch (IllegalAccessException e) {
            LOGGER.error(e, e.getCause());
        }
        return null;
    }

    private String currencyFormat(BigDecimal valor) {
        if (valor != null) {
            Locale meuLocal = new Locale("pt", "BR");
            NumberFormat formatter = NumberFormat.getCurrencyInstance(meuLocal);
            return formatter.format(valor);
        }
        return "";

    }

}
