$(function() {
	$('#mascaraTelefone').mask("(99) 9999-9999?9").keydown(function() {
		var $elem = $(this);
		var tamanhoAnterior = this.value.replace(/\D/g, '').length;

		setTimeout(function() {
			var novoTamanho = $elem.val().replace(/\D/g, '').length;
			if (novoTamanho !== tamanhoAnterior) {
				if (novoTamanho === 11) {
					$elem.unmask();
					$elem.mask("(99) 99999-9999");
				} else if (novoTamanho === 10) {
					$elem.unmask();
					$elem.mask("(99) 9999-9999?9");
				}
			}
		}, 1);
	});
});

$(function() {
	$('#mascaraTelefoneSemDDD').mask("9999-9999?9").keydown(function() {
		var $elem = $(this);
		var tamanhoAnterior = this.value.replace(/\D/g, '').length;

		setTimeout(function() {
			var novoTamanho = $elem.val().replace(/\D/g, '').length;
			if (novoTamanho !== tamanhoAnterior) {
				if (novoTamanho === 9) {
					$elem.unmask();
					$elem.mask("99999-9999");
				} else if (novoTamanho === 8) {
					$elem.unmask();
					$elem.mask("9999-9999?9");
				}
			}
		}, 1);
	});
});

// modal
function openModal(modalName) {
	$(modalName).modal('show');
	return false;
}

$(".alert").delay(4000).addClass("in").fadeOut(800);

/**
 * 
 * @param c
 * @returns {Boolean}
 */
function ValidarCPF(ObjCpf) {

	var cpf = ObjCpf.value;

	exp = /\.|\-|\_|\//g
	s = cpf.toString().replace(exp, "");

	if (s == '') {
		return true;
	} else {

		var i;
		var c = s.substr(0, 9);
		var dv = s.substr(9, 2);
		var d1 = 0;
		var v = false;

		for (i = 0; i < 9; i++) {
			d1 += c.charAt(i) * (10 - i);
		}
		if (d1 == 0) {
			alert("CPF Invalido");
			v = true;
			ObjCpf.focus();
			ObjCpf.select();
		}
		d1 = 11 - (d1 % 11);
		if (d1 > 9)
			d1 = 0;
		if (dv.charAt(0) != d1) {
			alert("CPF Invalido");
			v = true;
			ObjCpf.focus();
			ObjCpf.select();
		}

		d1 *= 2;
		for (i = 0; i < 9; i++) {
			d1 += c.charAt(i) * (11 - i);
		}
		d1 = 11 - (d1 % 11);
		if (d1 > 9)
			d1 = 0;
		if (dv.charAt(1) != d1) {
			alert("CPF Invalido");
			v = true;
			ObjCpf.focus();
			ObjCpf.select();
		}
	}
}

/**
 * 
 * @param ObjCnpj
 */
function ValidarCNPJ(ObjCnpj) {

	var cnpj = ObjCnpj.value;
	exp = /\.|\-|\_|\//g
	cnpj = cnpj.toString().replace(exp, "");
	var valida = new Array(6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2);
	var dig1 = new Number;
	var dig2 = new Number;

	if (cnpj == '') {
		return true;
	} else {

		var digito = new Number(eval(cnpj.charAt(12) + cnpj.charAt(13)));

		for (i = 0; i < valida.length; i++) {
			dig1 += (i > 0 ? (cnpj.charAt(i - 1) * valida[i]) : 0);
			dig2 += cnpj.charAt(i) * valida[i];
		}
		dig1 = (((dig1 % 11) < 2) ? 0 : (11 - (dig1 % 11)));
		dig2 = (((dig2 % 11) < 2) ? 0 : (11 - (dig2 % 11)));

		if (((dig1 * 10) + dig2) != digito) {
			alert('CNPJ Invalido!');
			ObjCnpj.focus();
			ObjCnpj.select();
		} else {
			return true;
		}
	}
}

$(function() {
	/* ADDING EVENTS */
	var currColor = "rgb(84, 132, 237)"; // Blue by default
	// Color chooser button
	var colorChooser = $("#color-chooser-btn");
	$("#color-chooser > li > a").click(function(e) {
		document.getElementById('colorEvent').value = $(this).attr('id');
		e.preventDefault();
		currColor = $(this).css("color");

		// Add color effect to button
		$('#add-new-event').css({
			"background-color" : currColor,
			"border-color" : currColor
		});
	});
});

function setConfirmUnload(on) {
	var message = "Deseja descartar as alterações?";
	window.onbeforeunload = (on) ? function () {
		return message;
	} : null;
}

function geocode() {
	PF('geoMap').geocode(document.getElementById('autocomplete').value);
}

function refreshEditorText(){
	var myValue = PF('editorWidget').jq.find("iframe").contents().find('body').html();
	document.getElementById('textoAtualizado').value = myValue;
}

