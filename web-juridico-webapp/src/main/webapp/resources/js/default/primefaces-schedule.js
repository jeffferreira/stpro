
PrimeFaces.widget.Schedule.prototype._oldInit = PrimeFaces.widget.Schedule.prototype.init;
PrimeFaces.widget.Schedule.prototype.init = function (cfg){
    cfg.columnFormat = {
        month: 'ddd',
        week: 'ddd, DD/M',
        day: 'dddd, DD/M'
    };
    this._oldInit.apply(this, arguments);
};

PrimeFaces.locales['pt'] = {
    closeText : 'Fechar',
    prevText : 'Anterior',
    nextText : 'Próximo',
    currentText : 'Começo',
    monthNames : [ 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro' ],
    monthNamesShort : [ 'Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez' ],
    dayNames : [ 'Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado' ],
    dayNamesShort : [ 'Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb' ],
    dayNamesMin : [ 'D', 'S', 'T', 'Q', 'Q', 'S', 'S' ],
    weekHeader : 'Semana',
    firstDay : 0,
    isRTL : false,
    showMonthAfterYear : false,
    yearSuffix : '',
    timeOnlyTitle : 'Selecione o Horário',
    timeText : 'Horário',
    hourText : 'Hora',
    minuteText : 'Minuto',
    secondText : 'Segundo',
    ampm: false,
    month : 'Mês',
    week : 'Semana',
    day : 'Dia',
    allDayText : 'Sem horário definido'
};