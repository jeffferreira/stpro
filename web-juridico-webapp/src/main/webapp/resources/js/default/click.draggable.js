/**
 * Created by jeffersonl2009_00 on 27/10/2016.
 */
/** http://skfox.com/jqExamples/insertAtCaret.html */

$(document).ready( function()
{
    $('#ClickListInEditorPessoa li').click(function() {
        var myValue = PF('editorWidget').jq.find("iframe").contents().find('body').html() + ' ' + $(this).attr("data-val");
        PF('editorWidget').jq.find("iframe").contents().find('body').html(myValue);
        document.getElementById('textoAtualizado').value = myValue;
        return false
    });
    $('#ClickListInEditorProcesso li').click(function() {
        var myValue = PF('editorWidget').jq.find("iframe").contents().find('body').html() + ' ' + $(this).attr("data-val");
        PF('editorWidget').jq.find("iframe").contents().find('body').html(myValue);
        document.getElementById('textoAtualizado').value = myValue;
        return false
    });
    $('#ClickListInEditorAgenda li').click(function() {
        var myValue = PF('editorWidget').jq.find("iframe").contents().find('body').html() + ' ' + $(this).attr("data-val");
        PF('editorWidget').jq.find("iframe").contents().find('body').html(myValue);
        document.getElementById('textoAtualizado').value = myValue;
        return false
    });
    $('#ClickListInTextArea li').click(function() {
        $("#txtMessage").insertAtCaret($(this).text());
        return false
    });
    $("#DragListInTextArea li").draggable({helper: 'clone'});
    $(".txtDropTarget").droppable({
        accept: "#DragWordList li",
        drop: function(ev, ui) {
            $(this).insertAtCaret(ui.draggable.text());
        }
    });
});

$.fn.insertAtCaret = function (myValue) {
    return this.each(function(){
        //IE support
        if (document.selection) {
            this.focus();
            sel = document.selection.createRange();
            sel.text = myValue;
            this.focus();
        }
        //MOZILLA / NETSCAPE support
        else if (this.selectionStart || this.selectionStart == '0') {
            var startPos = this.selectionStart;
            var endPos = this.selectionEnd;
            var scrollTop = this.scrollTop;
            this.value = this.value.substring(0, startPos)+ myValue+ this.value.substring(endPos,this.value.length);
            this.focus();
            this.selectionStart = startPos + myValue.length;
            this.selectionEnd = startPos + myValue.length;
            this.scrollTop = scrollTop;
        } else {
            this.value += myValue;
            this.focus();
        }
    });
};