$(addLoaderListeners);

function addLoaderListeners(){
	if(typeof jsf != 'undefined'){
		jsf.ajax.addOnEvent(handleAjaxCall);
		jsf.ajax.addOnError(handleAjaxError);
	}
	$('[data-loader]').each(addClickLoaderListener);

}

function addClickLoaderListener(){
	if( $(this).data("loader") == "on"){
		 $(this).click(invokeLoaderOn)
	}
}

function invokeLoaderOn(){
	var message = $(this).data("loader-message")
	showLoader(message);
}

function handleAjaxError(data){
	showMessage("Houve uma falha de comunica\u00e7\u00e3o com o servidor, tente novamente mais tarde!")
	console.log(data.description);

}

function handleAjaxCall(data){
	var loaderIs = $(data.source).data("ajax-loader");
	if(loaderIs == "off"){
		return;
	}

	var ajaxstatus = data.status;

	if(ajaxstatus == 'begin'){
		showLoader();
	}else{
		hideLoader();
	}


}

function showLoader(message){
	if(message == 'undefined' || message == null){
		message = "Aguarde...";
	}

	$('#stproLoaderMessage').text(message);
	$('#stproLoader').modal({ keyboard: false,
							   backdrop:"static"})
}

function hideLoader(){
	$('#stproLoader').modal('hide');
	//$('.modal-backdrop').remove()
}