package br.com.juridico.util;

import br.com.juridico.entidades.EstadoCivil;
import br.com.juridico.entidades.TipoContato;
import com.google.common.collect.Lists;
import com.google.gson.*;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

/**
 * Created by jeffersonl2009_00 on 17/10/2016.
 */
public class DadosMockUtils {

    private static final Logger LOGGER = Logger.getLogger(DadosMockUtils.class);
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();

    private DadosMockUtils() {}

    /**
     *
     * @return
     */
    public static List<TipoContato> getTipoContatos(){
        List<TipoContato> tipoContatos = Lists.newArrayList();
        JsonArray jsonArray = listaByJson("mocks/tipo_contato_mock.json");
        for (JsonElement element : jsonArray) {
            TipoContato tipoContato = gson.fromJson(element, TipoContato.class);

            tipoContatos.add(tipoContato);
        }
        return tipoContatos;
    }

    /**
     *
     * @return
     */
    public static List<EstadoCivil> getEstatosCivis(){
        List<EstadoCivil> estadosCivis = Lists.newArrayList();
        JsonArray jsonArray = listaByJson("mocks/estado_civil_mock.json");
        for (JsonElement element : jsonArray) {
            EstadoCivil estadoCivil = gson.fromJson(element, EstadoCivil.class);

            estadosCivis.add(estadoCivil);
        }
        return estadosCivis;
    }



    private static JsonArray listaByJson(String path){
        JsonArray jsonArray = new JsonParser().parse(reader(path)).getAsJsonArray();
        return jsonArray;
    }

    private static BufferedReader reader(String path){
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new FileReader(path));
        } catch (FileNotFoundException e) {
            LOGGER.error(e, e.getCause());
        }

        return bufferedReader;
    }

}
