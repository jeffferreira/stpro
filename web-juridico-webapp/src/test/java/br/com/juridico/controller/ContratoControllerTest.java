package br.com.juridico.controller;

import br.com.juridico.entidades.*;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.util.DateUtils;
import com.google.common.collect.Maps;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.modules.junit4.PowerMockRunner;

import java.math.BigDecimal;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created by jeffersonl2009_00 on 19/10/2016.
 */
@RunWith(PowerMockRunner.class)
public class ContratoControllerTest extends AbstractControllerTest implements ICrudTest {

    private static final String CHAVE_ACESSO = "CONTRATO_CONTRATO_TEMPLATE";
    private static final String PESSOA_FISICA = "PF";
    private static final String PAGINA_FORMULARIO = "/pages/cadastros/contrato/contrato-template.xhtml?faces-redirect=true";

    @Mock
    private User user;

    @InjectMocks
    private ContratoController controller;


    @Before
    public void up() {
        MockitoAnnotations.initMocks(this);
        mockObjects();
    }

    @Test
    public void testGoToFormulario() throws Exception {
        assertEquals(PAGINA_FORMULARIO, controller.goToFormulario());
    }

    @Test
    public void testSetEmpresaDeUsuarioLogado() throws Exception {
        when(getUser()).thenReturn(this.user);
        when(getUser().getCodEmpresa()).thenReturn(1L);
        this.controller.setEmpresaDeUsuarioLogado(this.controller.getObjetoSelecionado());
        assertEquals(Long.valueOf(1), this.controller.getObjetoSelecionado().getCodigoEmpresa());
    }

    @Test
    public void testValidar_passando_na_validacao() throws Exception {
        ContratoPessoaVigencia vigencia = new ContratoPessoaVigencia();
        vigencia.setValorAcompanhamentoMensal(new BigDecimal(1000));
        vigencia.setValorMensal(new BigDecimal(1000));
        ContratoPessoa contrato = new ContratoPessoa();
        contrato.setPessoa(new Pessoa(1L, "JAO DO TESTE", 'F', true));
        contrato.setDescricao("Novo contrato");
        contrato.setAcompanhamentoMensal(true);
        contrato.validar(vigencia, PESSOA_FISICA);
        assertNotNull(contrato);
    }

    @Test(expected = ValidationException.class)
    public void validando_pessoa_nula() throws Exception {
        ContratoPessoaVigencia vigencia = new ContratoPessoaVigencia();
        vigencia.setValorAcompanhamentoMensal(new BigDecimal(1000));
        vigencia.setValorMensal(new BigDecimal(1000));
        ContratoPessoa contrato = new ContratoPessoa();
        contrato.setPessoa(null);
        contrato.setDescricao("Novo contrato");
        contrato.setAcompanhamentoMensal(true);

        contrato.validar(vigencia, PESSOA_FISICA);
    }

    @Test(expected = ValidationException.class)
    public void validandoDescricao_nula() throws Exception {
        ContratoPessoaVigencia vigencia = new ContratoPessoaVigencia();
        vigencia.setValorAcompanhamentoMensal(new BigDecimal(1000));
        vigencia.setValorMensal(new BigDecimal(1000));
        ContratoPessoa contrato = new ContratoPessoa();
        contrato.setPessoa(new Pessoa(1L, "JAO DO TESTE", 'F', true));
        contrato.setDescricao("");
        contrato.setAcompanhamentoMensal(true);

        contrato.validar(vigencia, PESSOA_FISICA);
    }

    @Test(expected = ValidationException.class)
    public void validandoValorMensalAto_nulo() throws Exception {
        ContratoPessoaVigencia vigencia = new ContratoPessoaVigencia();
        vigencia.setValorAcompanhamentoMensal(new BigDecimal(1000));
        vigencia.setValorMensal(null);
        ContratoPessoa contrato = new ContratoPessoa();
        contrato.setPessoa(new Pessoa(1L, "JAO DO TESTE", 'F', true));
        contrato.setDescricao("Novo Contrato");
        contrato.setAcompanhamentoMensal(true);

        contrato.validar(vigencia, PESSOA_FISICA);
    }

    @Test(expected = ValidationException.class)
    public void validandoValorAcompanhamentoMensal_nulo() throws Exception {
        ContratoPessoaVigencia vigencia = new ContratoPessoaVigencia();
        vigencia.setValorAcompanhamentoMensal(null);
        vigencia.setValorMensal(new BigDecimal(1000));
        ContratoPessoa contrato = new ContratoPessoa();
        contrato.setPessoa(new Pessoa(1L, "JAO DO TESTE", 'F', true));
        contrato.setDescricao("Novo Contrato");
        contrato.setAcompanhamentoMensal(true);

        contrato.validar(vigencia, PESSOA_FISICA);
    }

    @Test(expected = ValidationException.class)
    public void validandoDataInicialVigencia_nulo() throws Exception {
        ContratoPessoaVigencia vigencia = new ContratoPessoaVigencia();
        vigencia.setDataInicio(null);
        vigencia.setValorAcompanhamentoMensal(new BigDecimal(1000));
        vigencia.setValorMensal(new BigDecimal(1000));
        ContratoPessoa contrato = new ContratoPessoa();
        contrato.setPessoa(new Pessoa(1L, "JAO DO TESTE", 'F', true));
        contrato.setDescricao("Novo Contrato");
        contrato.setAcompanhamentoMensal(true);

        contrato.validar(vigencia, PESSOA_FISICA);
    }

    @Test(expected = ValidationException.class)
    public void validandoDataInicialVigencia_maior_que_dataFinal() throws Exception {
        ContratoPessoaVigencia vigencia = new ContratoPessoaVigencia();
        vigencia.setDataInicio(DateUtils.geraDataUltimoDiaMes(1,2016));
        vigencia.setDataFim(DateUtils.geraPrimeiroDiaDoMes(1,2016));
        vigencia.setValorAcompanhamentoMensal(new BigDecimal(1000));
        vigencia.setValorMensal(new BigDecimal(1000));
        ContratoPessoa contrato = new ContratoPessoa();
        contrato.setPessoa(new Pessoa(1L, "JAO DO TESTE", 'F', true));
        contrato.setDescricao("Novo Contrato");
        contrato.setAcompanhamentoMensal(true);

        contrato.validar(vigencia, PESSOA_FISICA);
    }

    @Test(expected = ValidationException.class)
    public void testValidar_nao_passando_na_validacao() throws Exception {
        this.controller.validar();
    }

    @Test
    public void testAposCancelar() throws Exception {
        this.controller.getObjetoSelecionado().setDescricao("BLA BLA BLA...");
        this.controller.aposCancelar();
        assertNull(this.controller.getObjetoSelecionado().getDescricao());
    }

    @Test
    public void testIsHasAccess() throws Exception {
        Map<String, String> mapaAcessos = Maps.newHashMap();
        mapaAcessos.put(CHAVE_ACESSO, CHAVE_ACESSO);
        this.user = new User(1L, "User Contrato do teste");
        this.user.getAcessosMap().put(CHAVE_ACESSO, CHAVE_ACESSO);
        when(getSessionAttribute(USER_LOGGED)).thenReturn(this.user);
        assertTrue(controller.isHasAccess());
    }

    @Test
    public void testInstanciarObjetoNovo() throws Exception {
        assertNotNull(this.controller.instanciarObjetoNovo());
    }
}