package br.com.juridico.controller;

import br.com.juridico.ejb.StateManager;
import br.com.juridico.entidades.Empresa;
import br.com.juridico.entidades.User;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.primefaces.context.RequestContext;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.application.NavigationHandler;
import javax.faces.component.UIViewRoot;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Locale;
import java.util.ResourceBundle;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

/**
 * Created by jeffersonl2009_00 on 14/10/2016.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ FacesContext.class, FacesMessage.class, RequestContext.class, ResourceBundle.class})
public class AbstractControllerTest extends AbstractController {

    private static  final Logger LOGGER = Logger.getLogger(AbstractControllerTest.class);

    protected static final String USER_LOGGED = "userLogged";

    @Mock
    protected FacesContext facesContext;
    @Mock
    protected FacesMessage facesMessage;
    @Mock
    protected HttpSession httpSession;
    @Mock
    protected RequestContext requestContext;
    @Mock
    protected ResourceBundle resourceBundle;
    @Mock
    protected Flash flash;
    @Mock
    protected UIViewRoot uiViewRoot;
    @Mock
    protected Application application;
    @Mock
    protected NavigationHandler navigationHandler;
    @Mock
    protected LoginController loginController;
    @Mock
    private StateManager stateManager;

    protected Empresa empresa;

    @Test
    public void init_test(){
        assertTrue(facesContext != null);
        assertTrue(facesMessage != null);
        assertTrue(requestContext != null);
        assertTrue(resourceBundle != null);
        assertTrue(httpSession != null);
        assertTrue(flash != null);
        assertTrue(uiViewRoot != null);
    }

    protected BufferedReader reader(String path){
        BufferedReader bufferedReader = null;

        try {
            bufferedReader = new BufferedReader(new FileReader(path));
        } catch (FileNotFoundException e) {
            LOGGER.error(e, e.getCause());
        }

        return bufferedReader;
    }

    @Override
    protected Object getBeanInSession(String name) {
        return null;
    }

    protected void mockObjects(){

        mockStatic(FacesContext.class);
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(getFacesContext().getViewRoot()).thenReturn(uiViewRoot);
        when(getFacesContext().getApplication()).thenReturn(application);
        when(application.getNavigationHandler()).thenReturn(navigationHandler);
        when(getFacesContext().getApplication().evaluateExpressionGet(getFacesContext(), "#{loginController}", LoginController.class)).thenReturn(loginController);
        when(loginController.getStateManager()).thenReturn(stateManager);

        mockStatic(FacesMessage.class);
        mockStatic(RequestContext.class);
        when(RequestContext.getCurrentInstance()).thenReturn(requestContext);

        mockStatic(ResourceBundle.class);
        when(getFacesContext().getViewRoot().getLocale()).thenReturn(new Locale("pt", "BR"));

        ExternalContext ext = Mockito.mock(ExternalContext.class);
        when(getExternalContext()).thenReturn(ext);
        when((HttpSession) getExternalContext().getSession(false)).thenReturn(httpSession);
        when(getFlashScope()).thenReturn(flash);
        setSessionAttribute(USER_LOGGED, new User(1L, "Jao do teste"));
        when(getSessionAttribute(USER_LOGGED)).thenReturn(new User(1L, "Jao do teste"));
        this.empresa = new Empresa(1L);
        when(loginController.getStateManager().getEmpresaSelecionada()).thenReturn(empresa);

    }

}
