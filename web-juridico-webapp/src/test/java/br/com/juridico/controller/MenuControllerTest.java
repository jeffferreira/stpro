package br.com.juridico.controller;

import br.com.juridico.entidades.Empresa;
import br.com.juridico.entidades.User;
import br.com.juridico.enums.MenuIndicador;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created by jefferson on 22/10/2016.
 */
@RunWith(PowerMockRunner.class)
public class MenuControllerTest extends AbstractControllerTest {

    private static final String PAGINA_HOME = "/pages/index.xhtml?faces-redirect=true";
    private static final String PAGINA_ATENDIMENTO = "/pages/atendimento/atendimento-template.xhtml?faces-redirect=true";
    private static final String PAGINA_CADASTROS = "/pages/cadastros/list-cadastros.xhtml?faces-redirect=true";
    private static final String PAGINA_CONTRATOS = "/pages/contratos/list-contratos.xhtml?faces-redirect=true";
    private static final String PAGINA_PROCESSOS = "/pages/processos/processo-list.xhtml?faces-redirect=true";
    private static final String PAGINA_RELATORIOS = "/pages/relatorios/filtros/report-filter-form.xhtml?faces-redirect=true";
    private static final String PAGINA_ACESSOS = "/pages/acessos/acessos-template.xhtml?faces-redirect=true";
    private static final String PAGINA_PERFIL = "/pages/profile.xhtml?faces-redirect=true";

    @Mock
    private User user;
    @Mock
    private Empresa empresa;

    @InjectMocks
    private MenuController controller;

    @Before
    public void up() {
        MockitoAnnotations.initMocks(this);
        mockObjects();
        this.user = new User(1L, "User");
    }

    @Test
    public void testGoToHome() throws Exception {
        assertEquals(PAGINA_HOME, controller.goToHome());
    }

    @Test
    public void testGoToAtendimento() throws Exception {
        assertEquals(PAGINA_ATENDIMENTO, controller.goToAtendimento());
    }

    @Test
    public void testGoToCadastros() throws Exception {
        assertEquals(PAGINA_CADASTROS, controller.goToCadastros());
    }

    @Test
    public void testGoToContratos() throws Exception {
        assertEquals(PAGINA_CONTRATOS, controller.goToContratos());
    }

    @Test
    public void testGoToProcessos() throws Exception {
        assertEquals(PAGINA_PROCESSOS, controller.goToProcessos());
    }

    @Test
    public void testGoToRelatorios() throws Exception {
        assertEquals(PAGINA_RELATORIOS, controller.goToRelatorios());
    }

    @Test
    public void testGoToAcessos() throws Exception {
        assertEquals(PAGINA_ACESSOS, controller.goToAcessos());
    }

    @Test
    public void testGoToMeuPerfil() throws Exception {
        assertEquals(PAGINA_PERFIL, controller.goToMeuPerfil());
    }

    @Test
    public void testIsRoleMenuAdvogados_false() throws Exception {
        assertFalse(this.controller.isRoleMenuAdvogados());
    }

    @Test
    public void testIsRoleMenuAtendimento_false() throws Exception {
        assertFalse(this.controller.isRoleMenuAtendimento());
    }

    @Test
     public void testIsRoleMenuCadastros_false() throws Exception {
        assertFalse(this.controller.isRoleMenuCadastros());
    }

    @Test
    public void testIsRoleMenuContratos_false() throws Exception {
        assertFalse(this.controller.isRoleMenuContratos());
    }

    @Test
    public void testIsRoleMenuControladoria_false() throws Exception {
        assertFalse(this.controller.isRoleMenuControladoria());
    }

    @Test
    public void testIsRoleMenuPessoas_false() throws Exception {
        assertFalse(this.controller.isRoleMenuPessoas());
    }

    @Test
    public void testIsRoleMenuProcessos_false() throws Exception {
        assertFalse(this.controller.isRoleMenuProcessos());
    }

    @Test
    public void testIsRoleMenuRelatorios_false() throws Exception {
        assertFalse(this.controller.isRoleMenuRelatorios());
    }

    @Test
    public void testIsRoleMenuAcessos_false() throws Exception {
        assertFalse(this.controller.isRoleMenuAcessos());
    }

    @Test
    public void testIsRoleMenuAgenda_false() throws Exception {
        assertFalse(this.controller.isRoleMenuAgenda());
    }

    @Test
    public void testIsRoleMenuAdvogados_true() throws Exception {
        this.user.getAcessosMap().put(MenuIndicador.ADVOGADOS.getDescricao(), MenuIndicador.ADVOGADOS.getDescricao());
        when(getSessionAttribute(USER_LOGGED)).thenReturn(this.user);
        assertTrue(this.controller.isRoleMenuAdvogados());
    }

    @Test
    public void testIsRoleMenuAtendimento_true() throws Exception {
        this.user.getAcessosMap().put(MenuIndicador.ATENDIMENTO.getDescricao(), MenuIndicador.ATENDIMENTO.getDescricao());
        when(getSessionAttribute(USER_LOGGED)).thenReturn(this.user);
        assertTrue(this.controller.isRoleMenuAtendimento());
    }

    @Test
    public void testIsRoleMenuCadastros_true() throws Exception {
        this.user.getAcessosMap().put(MenuIndicador.CADASTROS.getDescricao(), MenuIndicador.CADASTROS.getDescricao());
        when(getSessionAttribute(USER_LOGGED)).thenReturn(this.user);
        assertTrue(this.controller.isRoleMenuCadastros());
    }

    @Test
    public void testIsRoleMenuPessoas_true() throws Exception {
        this.user.getAcessosMap().put(MenuIndicador.PESSOAS.getDescricao(), MenuIndicador.PESSOAS.getDescricao());
        when(getSessionAttribute(USER_LOGGED)).thenReturn(this.user);
        assertTrue(this.controller.isRoleMenuPessoas());
    }

    @Test
    public void testIsRoleMenuProcessos_true() throws Exception {
        this.user.getAcessosMap().put(MenuIndicador.PROCESSOS.getDescricao(), MenuIndicador.PROCESSOS.getDescricao());
        when(getSessionAttribute(USER_LOGGED)).thenReturn(this.user);
        assertTrue(this.controller.isRoleMenuProcessos());
    }

    @Test
    public void testIsRoleMenuRelatorios_true() throws Exception {
        this.user.getAcessosMap().put(MenuIndicador.RELATORIOS.getDescricao(), MenuIndicador.RELATORIOS.getDescricao());
        when(getSessionAttribute(USER_LOGGED)).thenReturn(this.user);
        assertTrue(this.controller.isRoleMenuRelatorios());
    }

    @Test
    public void testIsRoleMenuAcessos_true() throws Exception {
        this.user.getAcessosMap().put(MenuIndicador.ACESSOS.getDescricao(), MenuIndicador.ACESSOS.getDescricao());
        when(getSessionAttribute(USER_LOGGED)).thenReturn(this.user);
        assertTrue(this.controller.isRoleMenuAcessos());
    }

    @Test
    public void testIsRoleMenuAgenda_true() throws Exception {
        this.user.getAcessosMap().put(MenuIndicador.AGENDA.getDescricao(), MenuIndicador.AGENDA.getDescricao());
        when(getSessionAttribute(USER_LOGGED)).thenReturn(this.user);
        assertTrue(this.controller.isRoleMenuAgenda());
    }
}