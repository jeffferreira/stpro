package br.com.juridico.controller;

import br.com.juridico.entidades.*;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.impl.*;
import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.modules.junit4.PowerMockRunner;
import org.primefaces.event.ScheduleEntryMoveEvent;
import org.primefaces.event.ScheduleEntryResizeEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

/**
 * Created by jeffersonl2009_00 on 20/10/2016.
 */
@RunWith(PowerMockRunner.class)
public class AgendaGeralControllerTest extends AbstractControllerTest {

    private static final String TODOS = "Todos";
    private static final String AGENDA_ALTERACAO_AGENDA = "AGENDA_ALTERACAO_AGENDA";
    private static final String AGENDA_ADICIONAR_EVENTO = "AGENDA_ADICIONAR_EVENTO";
    private static final String PAGINA_FORMULARIO = "/pages/agenda/geral/agenda-geral-form.xhtml?faces-redirect=true";

    @Mock
    private EventoSessionBean eventoSessionBean;
    @Mock
    private PessoaSessionBean pessoaSessionBean;
    @Mock
    private PessoaFisicaSessionBean pessoaFisicaSessionBean;
    @Mock
    private EmailSessionBean emailSessionBean;
    @Mock
    private EmailCaixaSaidaSessionBean emailCaixaSaidaSessionBean;
    @Mock
    private StatusAgendaSessionBean statusAgendaSessionBean;
    @Mock
    private Pessoa pessoa;
    @Mock
    private SelectEvent selectEvent;
    @Mock
    private ScheduleEvent event;
    @Mock
    private ScheduleModel lazyEventModel;
    @Mock
    private ScheduleEntryMoveEvent scheduleEntryMoveEvent;
    @Mock
    private ScheduleEntryResizeEvent scheduleEntryResizeEvent;
    @Mock
    private ActionEvent actionEvent;
    @Mock
    private ValueChangeEvent valueChangeEvent;

    @InjectMocks
    private AgendaGeralController controller;

    @Before
    public void up() {
        MockitoAnnotations.initMocks(this);
        mockObjects();
    }

    @Test
    public void testInitializeAgendas() throws Exception {
        when(pessoaFisicaSessionBean.findByAdvogadosAndColaboradores(getUser().getCodEmpresa())).thenReturn(Lists.newArrayList());
        this.controller.setLazyEventModel(lazyEventModel);
        this.controller.initializeAgendas();
        Assert.assertEquals(TODOS, this.controller.getPessoaFisicaSelecionada());
    }

    @Test
    public void testListaDeEventos() throws Exception {
        List<Evento> eventos = Lists.newArrayList();
        eventos.add(new Evento());
        when(eventoSessionBean.findByEventosDaEmpresa(controller.getDataInicioAgenda(), controller.getDataFimAgenda(), getStateManager().getEmpresaSelecionada().getId(), controller.isHasAccessAgendaSemResponsavel())).thenReturn(eventos);
        when(pessoaSessionBean.findByNome(controller.getPessoaFisicaSelecionada(), getStateManager().getEmpresaSelecionada().getId())).thenReturn(pessoa);
        when(eventoSessionBean.findUserEvents(pessoa, controller.getDataInicioAgenda(), controller.getDataFimAgenda(), getStateManager().getEmpresaSelecionada().getId())).thenReturn(eventos);

        assertNotNull(this.controller.listaDeEventos());
        assertEquals(1, this.controller.listaDeEventos().size());
    }

    @Test
    public void testListaDeEventos_todos() throws Exception {
        this.controller.setPessoaFisicaSelecionada(TODOS);
        List<Evento> eventos = Lists.newArrayList();
        eventos.add(new Evento());
        eventos.add(new Evento());
        eventos.add(new Evento());
        when(eventoSessionBean.findByEventosDaEmpresa(controller.getDataInicioAgenda(), controller.getDataFimAgenda(), getStateManager().getEmpresaSelecionada().getId(), controller.isHasAccessAgendaSemResponsavel())).thenReturn(eventos);

        assertNotNull(this.controller.listaDeEventos());
        assertEquals(3, this.controller.listaDeEventos().size());
    }

    @Test
    public void testOnEventSelect() throws Exception {
        mockSelectEvento();
        assertNotNull(this.controller.getEventoSelecionado());
    }

    @Test
    public void testSelectPessoa() throws Exception {
        this.controller.selectPessoa("Joao do Teste");
        assertNotNull(this.controller.getPessoaFisicaSelecionada());
        assertEquals("Joao do Teste", this.controller.getPessoaFisicaSelecionada());
    }

    @Test
    public void testGetStatus() throws Exception {
        List<StatusAgenda> status = Lists.newArrayList();
        status.add(new StatusAgenda());
        status.add(new StatusAgenda());
        when(statusAgendaSessionBean.findAllStatusAtivos(getStateManager().getEmpresaSelecionada().getId())).thenReturn(status);
        assertEquals(2, this.controller.getStatus().size());
    }

    @Test
    public void testAddEvent() throws Exception {
        mockSelectEvento();
        doNothing().when(eventoSessionBean).update(controller.getEventoSelecionado());
        doNothing().when(eventoSessionBean).clearSession();
        mockEnviarEmail();
        this.controller.getEventoSelecionado().setPessoa(pessoa);
        StatusAgenda statusAgenda = new StatusAgenda(1L, "À cumprir", 1L);
        statusAgenda.setStyleColor("event-text-color-green");
        this.controller.getEventoSelecionado().setStatusAgenda(statusAgenda);
        this.controller.addEvent(actionEvent);
        assertNull(this.controller.getEventoSelecionado());

    }

    @Test
    public void testTransferenciaChanged() throws Exception {
        when((Boolean) valueChangeEvent.getNewValue()).thenReturn(false);
        when(pessoaSessionBean.retrieve(getUser().getId())).thenReturn(new Pessoa());
        this.controller.transferenciaChanged(valueChangeEvent);
        assertNotNull(this.controller.getEventoSelecionado().getPessoa());

    }

    @Test
    public void testRemoveEvent() throws Exception {
        mockEnviarEmail();
        when((ScheduleEvent) selectEvent.getObject()).thenReturn(event);
        this.controller.setLazyEventModel(lazyEventModel);
        doNothing().when(eventoSessionBean).delete(this.controller.getEventoSelecionado());
        this.controller.excluir();
    }

    @Test
    public void testUpdateEvento() throws Exception {
        mockSelectEvento();
        mockEnviarEmail();
        doNothing().when(eventoSessionBean).update(this.controller.getEventoSelecionado());
        doNothing().when(eventoSessionBean).clearSession();
        StatusAgenda statusAgenda = new StatusAgenda(1L, "À cumprir", 1L);
        statusAgenda.setStyleColor("event-text-color-green");
        this.controller.getEventoSelecionado().setStatusAgenda(statusAgenda);
        this.controller.getEventoSelecionado().setPessoa(pessoa);
        this.controller.updateEvento();
        assertNotNull(this.controller.getEventoSelecionado());
    }

    @Test
    public void testGoToAgendaGeral() throws Exception {
        assertEquals(PAGINA_FORMULARIO, controller.goToAgendaGeral());
    }

    /**
     *
     */
    private void mockSelectEvento(){
        when((ScheduleEvent) selectEvent.getObject()).thenReturn(event);
        when(eventoSessionBean.findByChaveId((String) event.getData())).thenReturn(new Evento());
        Evento evento = new Evento();
        this.controller.setEventoSelecionado(evento);
        when(eventoSessionBean.findByChaveId((String) event.getData())).thenReturn(evento);
        this.controller.onEventSelect(selectEvent);
    }

    /**
     *
     * @throws ValidationException
     */
    private void mockEnviarEmail() throws ValidationException {
        Email email = new Email(AGENDA_ALTERACAO_AGENDA, "Alteracao agenda", "Este teste altera agenda geral....", 1L, null, true);
        when(emailSessionBean.findBySigla(AGENDA_ALTERACAO_AGENDA, getUser().getCodEmpresa())).thenReturn(email);
        when(emailSessionBean.findBySigla(AGENDA_ADICIONAR_EVENTO, getUser().getCodEmpresa())).thenReturn(email);
        when(pessoaSessionBean.findByAdministradores(getUser().getCodEmpresa())).thenReturn(Lists.newArrayList());
        this.controller.setLazyEventModel(lazyEventModel);
        doNothing().when(emailCaixaSaidaSessionBean).create(new EmailCaixaSaida());
    }
}