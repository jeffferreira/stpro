package br.com.juridico.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.modules.junit4.PowerMockRunner;

import br.com.juridico.entidades.User;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.impl.RamoAtividadeSessionBean;

import com.google.common.collect.Maps;

/**
 * Created by Marcos Melo on 15/12/2016.
 */
@RunWith(PowerMockRunner.class)
public class RamoAtividadeControllerTest extends AbstractControllerTest implements ICrudTest {

    private static final String CHAVE_ACESSO = "RAMOATIVIDADE_RAMO_ATIVIDADE_TEMPLATE";
    private static final String PAGINA_FORMULARIO = "/pages/cadastros/ramoAtividade/ramo-atividade-template.xhtml?faces-redirect=true";

    @Mock
    private User user;
    @Mock
    private RamoAtividadeSessionBean ramoAtividadeSessionBean;

    @InjectMocks
    private RamoAtividadeController controller;


    @Before
    public void up() {
        MockitoAnnotations.initMocks(this);
        mockObjects();
    }

    @Test
    public void testGoToFormulario() throws Exception {
        assertEquals(PAGINA_FORMULARIO, controller.goToFormulario());
    }

    @Test
    public void testSetEmpresaDeUsuarioLogado() throws Exception {
        when(getUser()).thenReturn(this.user);
        when(getUser().getCodEmpresa()).thenReturn(1L);
        this.controller.setEmpresaDeUsuarioLogado(this.controller.getObjetoSelecionado());
        assertEquals(Long.valueOf(1), this.controller.getObjetoSelecionado().getCodigoEmpresa());
    }

    @Test
    public void testValidar_passando_na_validacao() throws Exception {
        boolean exceptionNaValidacao = false;
        this.controller.getObjetoSelecionado().setDescricao("RAMO_ATIVIDADE");
        when(ramoAtividadeSessionBean.isDescricaoJaCadastrada(controller.getObjetoSelecionado(), getUser().getCodEmpresa())).thenReturn(exceptionNaValidacao);
        this.controller.validar();
        assertEquals("RAMO_ATIVIDADE", this.controller.getObjetoSelecionado().getDescricao());
    }

    @Test(expected = ValidationException.class)
    public void testValidar_nao_passando_na_validacao() throws Exception {
        boolean exceptionNaValidacao = true;
        when(ramoAtividadeSessionBean.isDescricaoJaCadastrada(controller.getObjetoSelecionado(), this.empresa.getId())).thenReturn(exceptionNaValidacao);
        this.controller.validar();
    }

    @Test
    public void testAposCancelar() throws Exception {
        this.controller.getObjetoSelecionado().setDescricao("BLA BLA BLA...");
        this.controller.aposCancelar();
        assertNull(this.controller.getObjetoSelecionado().getDescricao());
    }

    @Test
    public void testIsHasAccess() throws Exception {
        Map<String, String> mapaAcessos = Maps.newHashMap();
        mapaAcessos.put(CHAVE_ACESSO, CHAVE_ACESSO);
        this.user = new User(1L, "User DA RAMO_ATIVIDADE teste");
        this.user.getAcessosMap().put(CHAVE_ACESSO, CHAVE_ACESSO);
        when(getSessionAttribute(USER_LOGGED)).thenReturn(this.user);
        assertTrue(controller.isHasAccess());
    }

    @Test
    public void testInstanciarObjetoNovo() throws Exception {
        assertNotNull(this.controller.instanciarObjetoNovo());
    }
}