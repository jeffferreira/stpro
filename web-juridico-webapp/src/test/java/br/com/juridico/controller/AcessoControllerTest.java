package br.com.juridico.controller;

import br.com.juridico.entidades.Acesso;
import br.com.juridico.entidades.Menu;
import br.com.juridico.entidades.Perfil;
import br.com.juridico.enums.MenuIndicador;
import br.com.juridico.impl.*;
import com.google.common.collect.Lists;
import com.google.gson.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.faces.context.FacesContext;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Created by jeffersonl2009_00 on 14/10/2016.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ FacesContext.class })
public class AcessoControllerTest extends AbstractControllerTest {

    @Mock
    private PerfilSessionBean perfilSessionBean;
    @Mock
    private PerfilAcessoSessionBean perfilAcessoSessionBean;
    @Mock
    private AcessoSessionBean acessoSessionBean;
    @Mock
    private DireitoPerfilSessionBean direitoPerfilSessionBean;
    @Mock
    private DireitoSessionBean direitoSessionBean;

    @InjectMocks
    private AcessoController controller;


    @Before
    public void up() {
        MockitoAnnotations.initMocks(this);
        mockObjects();
        when(perfilSessionBean.retrieveAll(getStateManager().getEmpresaSelecionada().getId())).thenReturn(Arrays.asList(new Perfil("ADMIN", "Administrador do sistema", 1L, Boolean.TRUE)));
    }

    @Test
    public void carrega_lista_acessos_cadastros_test(){
        Menu menu = new Menu(4L, "Menu de Cadastros");
        List<Acesso> acessos = listaDeAcessosByJson(menu, "mocks/acessos_cadastro_mock.json");
        when(acessoSessionBean.findByMenu(MenuIndicador.CADASTROS.getValue())).thenReturn(acessos);
        this.controller.initialize();
        assertEquals(3, controller.getPerfilCadastroAcessos().size());
    }

    private List<Acesso> listaDeAcessosByJson(Menu menu, String path){
        List<Acesso> acessos = Lists.newArrayList();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        JsonArray jsonArray = new JsonParser().parse(reader(path)).getAsJsonArray();

        for (JsonElement element : jsonArray) {
            Acesso acesso = gson.fromJson(element, Acesso.class);
            acesso.setMenu(menu);
            acessos.add(acesso);
        }
        return acessos;
    }
}
