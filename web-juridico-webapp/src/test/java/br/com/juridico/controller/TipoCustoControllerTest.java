package br.com.juridico.controller;

import br.com.juridico.entidades.User;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.impl.TipoCustoSessionBean;
import com.google.common.collect.Maps;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created by jeffersonl2009_00 on 19/10/2016.
 */
@RunWith(PowerMockRunner.class)
public class TipoCustoControllerTest extends AbstractControllerTest implements ICrudTest {

    private static final String CHAVE_ACESSO = "TIPOCUSTO_TIPO_CUSTO_TEMPLATE";
    private static final String PAGINA_FORMULARIO = "/pages/cadastros/tipoCusto/tipo-custo-template.xhtml?faces-redirect=true";

    @Mock
    private User user;
    @Mock
    private TipoCustoSessionBean tipoCustoSessionBean;

    @InjectMocks
    private TipoCustoController controller;


    @Before
    public void up() {
        MockitoAnnotations.initMocks(this);
        mockObjects();
    }

    @Test
    public void testGoToFormulario() throws Exception {
        assertEquals(PAGINA_FORMULARIO, controller.goToFormulario());
    }

    @Test
    public void testSetEmpresaDeUsuarioLogado() throws Exception {
        when(getUser()).thenReturn(this.user);
        when(getUser().getCodEmpresa()).thenReturn(1L);
        this.controller.setEmpresaDeUsuarioLogado(this.controller.getObjetoSelecionado());
        assertEquals(Long.valueOf(1), this.controller.getObjetoSelecionado().getCodigoEmpresa());
    }

    @Test
    public void testValidar_passando_na_validacao() throws Exception {
        boolean exceptionNaValidacao = false;
        this.controller.getObjetoSelecionado().setDescricao("TIPO_CUSTO");
        when(tipoCustoSessionBean.isDescricaoJaCadastrada(controller.getObjetoSelecionado(), getUser().getCodEmpresa())).thenReturn(exceptionNaValidacao);
        this.controller.validar();
        assertEquals("TIPO_CUSTO", this.controller.getObjetoSelecionado().getDescricao());
    }

    @Test(expected = ValidationException.class)
    public void testValidar_nao_passando_na_validacao() throws Exception {
        boolean exceptionNaValidacao = true;
        when(tipoCustoSessionBean.isDescricaoJaCadastrada(controller.getObjetoSelecionado(), this.empresa.getId())).thenReturn(exceptionNaValidacao);
        this.controller.validar();
    }

    @Test
    public void testAposCancelar() throws Exception {
        this.controller.getObjetoSelecionado().setDescricao("BLA BLA BLA...");
        this.controller.aposCancelar();
        assertNull(this.controller.getObjetoSelecionado().getDescricao());
    }

    @Test
    public void testIsHasAccess() throws Exception {
        Map<String, String> mapaAcessos = Maps.newHashMap();
        mapaAcessos.put(CHAVE_ACESSO, CHAVE_ACESSO);
        this.user = new User(1L, "User TIPO_CUSTO teste");
        this.user.getAcessosMap().put(CHAVE_ACESSO, CHAVE_ACESSO);
        when(getSessionAttribute(USER_LOGGED)).thenReturn(this.user);
        assertTrue(controller.isHasAccess());
    }

    @Test
    public void testInstanciarObjetoNovo() throws Exception {
        assertNotNull(this.controller.instanciarObjetoNovo());
    }
}