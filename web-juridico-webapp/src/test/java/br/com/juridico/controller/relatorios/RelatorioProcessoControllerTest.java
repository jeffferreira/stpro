package br.com.juridico.controller.relatorios;

import br.com.juridico.controller.AbstractControllerTest;
import br.com.juridico.entidades.RelatorioCriterioProcesso;
import br.com.juridico.entidades.RelatorioFiltroProcesso;
import br.com.juridico.enums.RelatorioProcessoCampoType;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.powermock.modules.junit4.PowerMockRunner;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.Assert.*;

/**
 * Created by jefferson.ferreira on 13/09/2017.
 */
@RunWith(PowerMockRunner.class)
public class RelatorioProcessoControllerTest extends AbstractControllerTest {

    private RelatorioProcessoController controller;

    @Before
    public void up(){
        MockitoAnnotations.initMocks(this);
        mockObjects();
        this.controller = new RelatorioProcessoController();
    }

    @Test
    public void validarCriteriosAtivos_data_cadastro_date_retornaMsg_true(){
        RelatorioFiltroProcesso relatorio = new RelatorioFiltroProcesso("Relatorio 1", 1L);
        RelatorioCriterioProcesso criterio = new RelatorioCriterioProcesso(relatorio,
                RelatorioProcessoCampoType.DATA_CADASTRO.getDescricao(),
                RelatorioProcessoCampoType.DATA_CADASTRO.getValorParaQuery(),
                RelatorioProcessoCampoType.DATA_CADASTRO.getTipoCampo(),
                RelatorioProcessoCampoType.DATA_CADASTRO.getNomeCampoView());
        criterio.setFiltroObrigatorio(Boolean.TRUE);
        controller.setCriteriosAtivos(Lists.newArrayList(criterio));
        assertFalse(controller.validarCriteriosAtivos().isEmpty());
        assertEquals("Campo Dt. Cadastro \u00e9 de preenchimento obrigat\u00f3rio.", controller.validarCriteriosAtivos().get(0));
    }

    @Test
    public void validarCriteriosAtivos_data_cadastro_date_retornaMsg_false(){
        RelatorioFiltroProcesso relatorio = new RelatorioFiltroProcesso("Relatorio 1", 1L);
        RelatorioCriterioProcesso criterio = new RelatorioCriterioProcesso(relatorio,
                RelatorioProcessoCampoType.DATA_CADASTRO.getDescricao(),
                RelatorioProcessoCampoType.DATA_CADASTRO.getValorParaQuery(),
                RelatorioProcessoCampoType.DATA_CADASTRO.getTipoCampo(),
                RelatorioProcessoCampoType.DATA_CADASTRO.getNomeCampoView());
        criterio.setFiltroObrigatorio(Boolean.TRUE);
        criterio.setValorPeriodoUm(new Date());
        controller.setCriteriosAtivos(Lists.newArrayList(criterio));
        assertTrue(controller.validarCriteriosAtivos().isEmpty());
    }

    @Test
    public void validarCriteriosAtivos_data_abertura_date_retornaMsg_true(){
        RelatorioFiltroProcesso relatorio = new RelatorioFiltroProcesso("Relatorio 1", 1L);
        RelatorioCriterioProcesso criterio = new RelatorioCriterioProcesso(relatorio,
                RelatorioProcessoCampoType.DATA_ABERTURA.getDescricao(),
                RelatorioProcessoCampoType.DATA_ABERTURA.getValorParaQuery(),
                RelatorioProcessoCampoType.DATA_ABERTURA.getTipoCampo(),
                RelatorioProcessoCampoType.DATA_ABERTURA.getNomeCampoView());
        criterio.setFiltroObrigatorio(Boolean.TRUE);
        controller.setCriteriosAtivos(Lists.newArrayList(criterio));
        assertFalse(controller.validarCriteriosAtivos().isEmpty());
        assertEquals("Campo Dt. Abertura \u00e9 de preenchimento obrigat\u00f3rio.", controller.validarCriteriosAtivos().get(0));
    }

    @Test
    public void validarCriteriosAtivos_data_abertura_date_retornaMsg_false(){
        RelatorioFiltroProcesso relatorio = new RelatorioFiltroProcesso("Relatorio 1", 1L);
        RelatorioCriterioProcesso criterio = new RelatorioCriterioProcesso(relatorio,
                RelatorioProcessoCampoType.DATA_ABERTURA.getDescricao(),
                RelatorioProcessoCampoType.DATA_ABERTURA.getValorParaQuery(),
                RelatorioProcessoCampoType.DATA_ABERTURA.getTipoCampo(),
                RelatorioProcessoCampoType.DATA_ABERTURA.getNomeCampoView());
        criterio.setFiltroObrigatorio(Boolean.TRUE);
        criterio.setValorPeriodoDois(new Date());
        controller.setCriteriosAtivos(Lists.newArrayList(criterio));
        assertTrue(controller.validarCriteriosAtivos().isEmpty());
    }

    @Test
    public void validarCriteriosAtivos_cliente_text_retornaMsg_true(){
        RelatorioFiltroProcesso relatorio = new RelatorioFiltroProcesso("Relatorio 1", 1L);
        RelatorioCriterioProcesso criterio = new RelatorioCriterioProcesso(relatorio,
                RelatorioProcessoCampoType.CLIENTE.getDescricao(),
                RelatorioProcessoCampoType.CLIENTE.getValorParaQuery(),
                RelatorioProcessoCampoType.CLIENTE.getTipoCampo(),
                RelatorioProcessoCampoType.CLIENTE.getNomeCampoView());
        criterio.setFiltroObrigatorio(Boolean.TRUE);
        controller.setCriteriosAtivos(Lists.newArrayList(criterio));
        assertFalse(controller.validarCriteriosAtivos().isEmpty());
        assertEquals("Campo Cliente \u00e9 de preenchimento obrigat\u00f3rio.", controller.validarCriteriosAtivos().get(0));
    }

    @Test
    public void validarCriteriosAtivos_cliente_text_retornaMsg_false(){
        RelatorioFiltroProcesso relatorio = new RelatorioFiltroProcesso("Relatorio 1", 1L);
        RelatorioCriterioProcesso criterio = new RelatorioCriterioProcesso(relatorio,
                RelatorioProcessoCampoType.CLIENTE.getDescricao(),
                RelatorioProcessoCampoType.CLIENTE.getValorParaQuery(),
                RelatorioProcessoCampoType.CLIENTE.getTipoCampo(),
                RelatorioProcessoCampoType.CLIENTE.getNomeCampoView());
        criterio.setFiltroObrigatorio(Boolean.TRUE);
        criterio.setValorString("Cliente 123");
        controller.setCriteriosAtivos(Lists.newArrayList(criterio));
        assertTrue(controller.validarCriteriosAtivos().isEmpty());
    }

    @Test
    public void validarCriteriosAtivos_adverso_text_retornaMsg_true(){
        RelatorioFiltroProcesso relatorio = new RelatorioFiltroProcesso("Relatorio 1", 1L);
        RelatorioCriterioProcesso criterio = new RelatorioCriterioProcesso(relatorio,
                RelatorioProcessoCampoType.ADVERSO.getDescricao(),
                RelatorioProcessoCampoType.ADVERSO.getValorParaQuery(),
                RelatorioProcessoCampoType.ADVERSO.getTipoCampo(),
                RelatorioProcessoCampoType.ADVERSO.getNomeCampoView());
        criterio.setFiltroObrigatorio(Boolean.TRUE);
        controller.setCriteriosAtivos(Lists.newArrayList(criterio));
        assertFalse(controller.validarCriteriosAtivos().isEmpty());
        assertEquals("Campo Adverso \u00e9 de preenchimento obrigat\u00f3rio.", controller.validarCriteriosAtivos().get(0));
    }

    @Test
    public void validarCriteriosAtivos_adverso_text_retornaMsg_false(){
        RelatorioFiltroProcesso relatorio = new RelatorioFiltroProcesso("Relatorio 1", 1L);
        RelatorioCriterioProcesso criterio = new RelatorioCriterioProcesso(relatorio,
                RelatorioProcessoCampoType.ADVERSO.getDescricao(),
                RelatorioProcessoCampoType.ADVERSO.getValorParaQuery(),
                RelatorioProcessoCampoType.ADVERSO.getTipoCampo(),
                RelatorioProcessoCampoType.ADVERSO.getNomeCampoView());
        criterio.setFiltroObrigatorio(Boolean.TRUE);
        criterio.setValorString("Adverso 123");
        controller.setCriteriosAtivos(Lists.newArrayList(criterio));
        assertTrue(controller.validarCriteriosAtivos().isEmpty());
    }


    @Test
    public void validarCriteriosAtivos_advogado_text_retornaMsg_true(){
        RelatorioFiltroProcesso relatorio = new RelatorioFiltroProcesso("Relatorio 1", 1L);
        RelatorioCriterioProcesso criterio = new RelatorioCriterioProcesso(relatorio,
                RelatorioProcessoCampoType.ADVOGADO.getDescricao(),
                RelatorioProcessoCampoType.ADVOGADO.getValorParaQuery(),
                RelatorioProcessoCampoType.ADVOGADO.getTipoCampo(),
                RelatorioProcessoCampoType.ADVOGADO.getNomeCampoView());
        criterio.setFiltroObrigatorio(Boolean.TRUE);
        controller.setCriteriosAtivos(Lists.newArrayList(criterio));
        assertFalse(controller.validarCriteriosAtivos().isEmpty());
        assertEquals("Campo Advogado \u00e9 de preenchimento obrigat\u00f3rio.", controller.validarCriteriosAtivos().get(0));
    }

    @Test
    public void validarCriteriosAtivos_advogado_text_retornaMsg_false(){
        RelatorioFiltroProcesso relatorio = new RelatorioFiltroProcesso("Relatorio 1", 1L);
        RelatorioCriterioProcesso criterio = new RelatorioCriterioProcesso(relatorio,
                RelatorioProcessoCampoType.ADVOGADO.getDescricao(),
                RelatorioProcessoCampoType.ADVOGADO.getValorParaQuery(),
                RelatorioProcessoCampoType.ADVOGADO.getTipoCampo(),
                RelatorioProcessoCampoType.ADVOGADO.getNomeCampoView());
        criterio.setFiltroObrigatorio(Boolean.TRUE);
        criterio.setValorString("Advogado 123");
        controller.setCriteriosAtivos(Lists.newArrayList(criterio));
        assertTrue(controller.validarCriteriosAtivos().isEmpty());
    }

    @Test
    public void validarCriteriosAtivos_meio_tramitacao_text_retornaMsg_true(){
        RelatorioFiltroProcesso relatorio = new RelatorioFiltroProcesso("Relatorio 1", 1L);
        RelatorioCriterioProcesso criterio = new RelatorioCriterioProcesso(relatorio,
                RelatorioProcessoCampoType.MEIO_TRAMITACAO.getDescricao(),
                RelatorioProcessoCampoType.MEIO_TRAMITACAO.getValorParaQuery(),
                RelatorioProcessoCampoType.MEIO_TRAMITACAO.getTipoCampo(),
                RelatorioProcessoCampoType.MEIO_TRAMITACAO.getNomeCampoView());
        criterio.setFiltroObrigatorio(Boolean.TRUE);
        controller.setCriteriosAtivos(Lists.newArrayList(criterio));
        assertFalse(controller.validarCriteriosAtivos().isEmpty());
        assertEquals("Campo Meio de tramita\u00e7\u00e3o \u00e9 de preenchimento obrigat\u00f3rio.", controller.validarCriteriosAtivos().get(0));
    }

    @Test
    public void validarCriteriosAtivos_meio_tramitacao_text_retornaMsg_false(){
        RelatorioFiltroProcesso relatorio = new RelatorioFiltroProcesso("Relatorio 1", 1L);
        RelatorioCriterioProcesso criterio = new RelatorioCriterioProcesso(relatorio,
                RelatorioProcessoCampoType.MEIO_TRAMITACAO.getDescricao(),
                RelatorioProcessoCampoType.MEIO_TRAMITACAO.getValorParaQuery(),
                RelatorioProcessoCampoType.MEIO_TRAMITACAO.getTipoCampo(),
                RelatorioProcessoCampoType.MEIO_TRAMITACAO.getNomeCampoView());
        criterio.setFiltroObrigatorio(Boolean.TRUE);
        criterio.setValorString("Meio tramitacao 123");
        controller.setCriteriosAtivos(Lists.newArrayList(criterio));
        assertTrue(controller.validarCriteriosAtivos().isEmpty());
    }

    @Test
    public void validarCriteriosAtivos_centro_custo_text_retornaMsg_true(){
        RelatorioFiltroProcesso relatorio = new RelatorioFiltroProcesso("Relatorio 1", 1L);
        RelatorioCriterioProcesso criterio = new RelatorioCriterioProcesso(relatorio,
                RelatorioProcessoCampoType.CENTRO_CUSTO.getDescricao(),
                RelatorioProcessoCampoType.CENTRO_CUSTO.getValorParaQuery(),
                RelatorioProcessoCampoType.CENTRO_CUSTO.getTipoCampo(),
                RelatorioProcessoCampoType.CENTRO_CUSTO.getNomeCampoView());
        criterio.setFiltroObrigatorio(Boolean.TRUE);
        controller.setCriteriosAtivos(Lists.newArrayList(criterio));
        assertFalse(controller.validarCriteriosAtivos().isEmpty());
        assertEquals("Campo Centro de custo \u00e9 de preenchimento obrigat\u00f3rio.", controller.validarCriteriosAtivos().get(0));
    }

    @Test
    public void validarCriteriosAtivos_natureza_acao_text_retornaMsg_true(){
        RelatorioFiltroProcesso relatorio = new RelatorioFiltroProcesso("Relatorio 1", 1L);
        RelatorioCriterioProcesso criterio = new RelatorioCriterioProcesso(relatorio,
                RelatorioProcessoCampoType.NATUREZA_ACAO.getDescricao(),
                RelatorioProcessoCampoType.NATUREZA_ACAO.getValorParaQuery(),
                RelatorioProcessoCampoType.NATUREZA_ACAO.getTipoCampo(),
                RelatorioProcessoCampoType.NATUREZA_ACAO.getNomeCampoView());
        criterio.setFiltroObrigatorio(Boolean.TRUE);
        controller.setCriteriosAtivos(Lists.newArrayList(criterio));
        assertFalse(controller.validarCriteriosAtivos().isEmpty());
        assertEquals("Campo Natureza da a\u00e7\u00e3o \u00e9 de preenchimento obrigat\u00f3rio.", controller.validarCriteriosAtivos().get(0));
    }

    @Test
    public void validarCriteriosAtivos_forum_text_retornaMsg_true() throws Exception {
        RelatorioFiltroProcesso relatorio = new RelatorioFiltroProcesso("Relatorio 1", 1L);
        RelatorioCriterioProcesso criterio = new RelatorioCriterioProcesso(relatorio,
                RelatorioProcessoCampoType.FORUM.getDescricao(),
                RelatorioProcessoCampoType.FORUM.getValorParaQuery(),
                RelatorioProcessoCampoType.FORUM.getTipoCampo(),
                RelatorioProcessoCampoType.FORUM.getNomeCampoView());
        criterio.setFiltroObrigatorio(Boolean.TRUE);
        controller.setCriteriosAtivos(Lists.newArrayList(criterio));
        assertFalse(controller.validarCriteriosAtivos().isEmpty());
        assertEquals("Campo F\u00f3rum \u00e9 de preenchimento obrigat\u00f3rio.", controller.validarCriteriosAtivos().get(0));
    }

    @Test
    public void validarCriteriosAtivos_comarca_text_retornaMsg_true() throws Exception {
        RelatorioFiltroProcesso relatorio = new RelatorioFiltroProcesso("Relatorio 1", 1L);
        RelatorioCriterioProcesso criterio = new RelatorioCriterioProcesso(relatorio,
                RelatorioProcessoCampoType.COMARCA.getDescricao(),
                RelatorioProcessoCampoType.COMARCA.getValorParaQuery(),
                RelatorioProcessoCampoType.COMARCA.getTipoCampo(),
                RelatorioProcessoCampoType.COMARCA.getNomeCampoView());
        criterio.setFiltroObrigatorio(Boolean.TRUE);
        controller.setCriteriosAtivos(Lists.newArrayList(criterio));
        assertFalse(controller.validarCriteriosAtivos().isEmpty());
        assertEquals("Campo Comarca \u00e9 de preenchimento obrigat\u00f3rio.", controller.validarCriteriosAtivos().get(0));
    }

    @Test
    public void validarCriteriosAtivos_vara_text_retornaMsg_true() throws Exception {
        RelatorioFiltroProcesso relatorio = new RelatorioFiltroProcesso("Relatorio 1", 1L);
        RelatorioCriterioProcesso criterio = new RelatorioCriterioProcesso(relatorio,
                RelatorioProcessoCampoType.VARA.getDescricao(),
                RelatorioProcessoCampoType.VARA.getValorParaQuery(),
                RelatorioProcessoCampoType.VARA.getTipoCampo(),
                RelatorioProcessoCampoType.VARA.getNomeCampoView());
        criterio.setFiltroObrigatorio(Boolean.TRUE);
        controller.setCriteriosAtivos(Lists.newArrayList(criterio));
        assertFalse(controller.validarCriteriosAtivos().isEmpty());
        assertEquals("Campo Vara \u00e9 de preenchimento obrigat\u00f3rio.", controller.validarCriteriosAtivos().get(0));
    }

    @Test
    public void validarCriteriosAtivos_resumo_processo_text_retornaMsg_true() throws Exception {
        RelatorioFiltroProcesso relatorio = new RelatorioFiltroProcesso("Relatorio 1", 1L);
        RelatorioCriterioProcesso criterio = new RelatorioCriterioProcesso(relatorio,
                RelatorioProcessoCampoType.RESUMO_PROCESSO.getDescricao(),
                RelatorioProcessoCampoType.RESUMO_PROCESSO.getValorParaQuery(),
                RelatorioProcessoCampoType.RESUMO_PROCESSO.getTipoCampo(),
                RelatorioProcessoCampoType.RESUMO_PROCESSO.getNomeCampoView());
        criterio.setFiltroObrigatorio(Boolean.TRUE);
        controller.setCriteriosAtivos(Lists.newArrayList(criterio));
        assertFalse(controller.validarCriteriosAtivos().isEmpty());
        assertEquals("Campo Resumo da a\u00e7\u00e3o \u00e9 de preenchimento obrigat\u00f3rio.", controller.validarCriteriosAtivos().get(0));
    }

    @Test
    public void validarCriteriosAtivos_causa_pedido_text_retornaMsg_true() throws Exception {
        RelatorioFiltroProcesso relatorio = new RelatorioFiltroProcesso("Relatorio 1", 1L);
        RelatorioCriterioProcesso criterio = new RelatorioCriterioProcesso(relatorio,
                RelatorioProcessoCampoType.CAUSA_PEDIDO.getDescricao(),
                RelatorioProcessoCampoType.CAUSA_PEDIDO.getValorParaQuery(),
                RelatorioProcessoCampoType.CAUSA_PEDIDO.getTipoCampo(),
                RelatorioProcessoCampoType.CAUSA_PEDIDO.getNomeCampoView());
        criterio.setFiltroObrigatorio(Boolean.TRUE);
        controller.setCriteriosAtivos(Lists.newArrayList(criterio));
        assertFalse(controller.validarCriteriosAtivos().isEmpty());
        assertEquals("Campo Causa do pedido \u00e9 de preenchimento obrigat\u00f3rio.", controller.validarCriteriosAtivos().get(0));
    }

    @Test
    public void validarCriteriosAtivos_pedido_text_retornaMsg_true() throws Exception {
        RelatorioFiltroProcesso relatorio = new RelatorioFiltroProcesso("Relatorio 1", 1L);
        RelatorioCriterioProcesso criterio = new RelatorioCriterioProcesso(relatorio,
                RelatorioProcessoCampoType.PEDIDO.getDescricao(),
                RelatorioProcessoCampoType.PEDIDO.getValorParaQuery(),
                RelatorioProcessoCampoType.PEDIDO.getTipoCampo(),
                RelatorioProcessoCampoType.PEDIDO.getNomeCampoView());
        criterio.setFiltroObrigatorio(Boolean.TRUE);
        controller.setCriteriosAtivos(Lists.newArrayList(criterio));
        assertFalse(controller.validarCriteriosAtivos().isEmpty());
        assertEquals("Campo Pedido \u00e9 de preenchimento obrigat\u00f3rio.", controller.validarCriteriosAtivos().get(0));
    }

    @Test
    public void validarCriteriosAtivos_valor_pedido_text_retornaMsg_true() throws Exception {
        RelatorioFiltroProcesso relatorio = new RelatorioFiltroProcesso("Relatorio 1", 1L);
        RelatorioCriterioProcesso criterio = new RelatorioCriterioProcesso(relatorio,
                RelatorioProcessoCampoType.VALOR_PEDIDO.getDescricao(),
                RelatorioProcessoCampoType.VALOR_PEDIDO.getValorParaQuery(),
                RelatorioProcessoCampoType.VALOR_PEDIDO.getTipoCampo(),
                RelatorioProcessoCampoType.VALOR_PEDIDO.getNomeCampoView());
        criterio.setFiltroObrigatorio(Boolean.TRUE);
        controller.setCriteriosAtivos(Lists.newArrayList(criterio));
        assertFalse(controller.validarCriteriosAtivos().isEmpty());
        assertEquals("Campo Valor do pedido \u00e9 de preenchimento obrigat\u00f3rio.", controller.validarCriteriosAtivos().get(0));
    }

    @Test
    public void validarCriteriosAtivos_valor_provavel_number_retornaMsg_true() throws Exception {
        RelatorioFiltroProcesso relatorio = new RelatorioFiltroProcesso("Relatorio 1", 1L);
        RelatorioCriterioProcesso criterio = new RelatorioCriterioProcesso(relatorio,
                RelatorioProcessoCampoType.VALOR_PROVAVEL.getDescricao(),
                RelatorioProcessoCampoType.VALOR_PROVAVEL.getValorParaQuery(),
                RelatorioProcessoCampoType.VALOR_PROVAVEL.getTipoCampo(),
                RelatorioProcessoCampoType.VALOR_PROVAVEL.getNomeCampoView());
        criterio.setFiltroObrigatorio(Boolean.TRUE);
        controller.setCriteriosAtivos(Lists.newArrayList(criterio));
        assertFalse(controller.validarCriteriosAtivos().isEmpty());
        assertEquals("Campo Valor prov\u00e1vel \u00e9 de preenchimento obrigat\u00f3rio.", controller.validarCriteriosAtivos().get(0));
    }

    @Test
    public void validarCriteriosAtivos_grau_de_risco_text_retornaMsg_true() throws Exception {
        RelatorioFiltroProcesso relatorio = new RelatorioFiltroProcesso("Relatorio 1", 1L);
        RelatorioCriterioProcesso criterio = new RelatorioCriterioProcesso(relatorio,
                RelatorioProcessoCampoType.GRAU_RISCO.getDescricao(),
                RelatorioProcessoCampoType.GRAU_RISCO.getValorParaQuery(),
                RelatorioProcessoCampoType.GRAU_RISCO.getTipoCampo(),
                RelatorioProcessoCampoType.GRAU_RISCO.getNomeCampoView());
        criterio.setFiltroObrigatorio(Boolean.TRUE);
        controller.setCriteriosAtivos(Lists.newArrayList(criterio));
        assertFalse(controller.validarCriteriosAtivos().isEmpty());
        assertEquals("Campo Grau de risco \u00e9 de preenchimento obrigat\u00f3rio.", controller.validarCriteriosAtivos().get(0));
    }

    @Test
    public void validarCriteriosAtivos_valor_causa_number_retornaMsg_true() throws Exception {
        RelatorioFiltroProcesso relatorio = new RelatorioFiltroProcesso("Relatorio 1", 1L);
        RelatorioCriterioProcesso criterio = new RelatorioCriterioProcesso(relatorio,
                RelatorioProcessoCampoType.VALOR_CAUSA.getDescricao(),
                RelatorioProcessoCampoType.VALOR_CAUSA.getValorParaQuery(),
                RelatorioProcessoCampoType.VALOR_CAUSA.getTipoCampo(),
                RelatorioProcessoCampoType.VALOR_CAUSA.getNomeCampoView());
        criterio.setFiltroObrigatorio(Boolean.TRUE);
        controller.setCriteriosAtivos(Lists.newArrayList(criterio));
        assertFalse(controller.validarCriteriosAtivos().isEmpty());
        assertEquals("Campo Valor da causa \u00e9 de preenchimento obrigat\u00f3rio.", controller.validarCriteriosAtivos().get(0));
    }

    @Test
    public void validarCriteriosAtivos_valor_total_provavel_number_retornaMsg_true() throws Exception {
        RelatorioFiltroProcesso relatorio = new RelatorioFiltroProcesso("Relatorio 1", 1L);
        RelatorioCriterioProcesso criterio = new RelatorioCriterioProcesso(relatorio,
                RelatorioProcessoCampoType.VALOR_TOTAL_PROVAVEL.getDescricao(),
                RelatorioProcessoCampoType.VALOR_TOTAL_PROVAVEL.getValorParaQuery(),
                RelatorioProcessoCampoType.VALOR_TOTAL_PROVAVEL.getTipoCampo(),
                RelatorioProcessoCampoType.VALOR_TOTAL_PROVAVEL.getNomeCampoView());
        criterio.setFiltroObrigatorio(Boolean.TRUE);
        controller.setCriteriosAtivos(Lists.newArrayList(criterio));
        assertFalse(controller.validarCriteriosAtivos().isEmpty());
        assertEquals("Campo Valor total prov\u00e1vel \u00e9 de preenchimento obrigat\u00f3rio.", controller.validarCriteriosAtivos().get(0));
    }

    @Test
    public void validarCriteriosAtivos_valor_total_provavel_number_retornaMsg_false() throws Exception {
        RelatorioFiltroProcesso relatorio = new RelatorioFiltroProcesso("Relatorio 1", 1L);
        RelatorioCriterioProcesso criterio = new RelatorioCriterioProcesso(relatorio,
                RelatorioProcessoCampoType.VALOR_TOTAL_PROVAVEL.getDescricao(),
                RelatorioProcessoCampoType.VALOR_TOTAL_PROVAVEL.getValorParaQuery(),
                RelatorioProcessoCampoType.VALOR_TOTAL_PROVAVEL.getTipoCampo(),
                RelatorioProcessoCampoType.VALOR_TOTAL_PROVAVEL.getNomeCampoView());
        criterio.setFiltroObrigatorio(Boolean.TRUE);
        criterio.setValorPeriodoNumberDois(BigDecimal.ONE);
        controller.setCriteriosAtivos(Lists.newArrayList(criterio));
        assertTrue(controller.validarCriteriosAtivos().isEmpty());
    }

    @Test
    public void validarCriteriosAtivos_valor_total_pedido_number_retornaMsg_true() throws Exception {
        RelatorioFiltroProcesso relatorio = new RelatorioFiltroProcesso("Relatorio 1", 1L);
        RelatorioCriterioProcesso criterio = new RelatorioCriterioProcesso(relatorio,
                RelatorioProcessoCampoType.VALOR_TOTAL_PEDIDO.getDescricao(),
                RelatorioProcessoCampoType.VALOR_TOTAL_PEDIDO.getValorParaQuery(),
                RelatorioProcessoCampoType.VALOR_TOTAL_PEDIDO.getTipoCampo(),
                RelatorioProcessoCampoType.VALOR_TOTAL_PEDIDO.getNomeCampoView());
        criterio.setFiltroObrigatorio(Boolean.TRUE);
        controller.setCriteriosAtivos(Lists.newArrayList(criterio));
        assertFalse(controller.validarCriteriosAtivos().isEmpty());
        assertEquals("Campo Valor total pedido \u00e9 de preenchimento obrigat\u00f3rio.", controller.validarCriteriosAtivos().get(0));
    }

    @Test
    public void validarCriteriosAtivos_valor_total_pedido_number_retornaMsg_false() throws Exception {
        RelatorioFiltroProcesso relatorio = new RelatorioFiltroProcesso("Relatorio 1", 1L);
        RelatorioCriterioProcesso criterio = new RelatorioCriterioProcesso(relatorio,
                RelatorioProcessoCampoType.VALOR_TOTAL_PEDIDO.getDescricao(),
                RelatorioProcessoCampoType.VALOR_TOTAL_PEDIDO.getValorParaQuery(),
                RelatorioProcessoCampoType.VALOR_TOTAL_PEDIDO.getTipoCampo(),
                RelatorioProcessoCampoType.VALOR_TOTAL_PEDIDO.getNomeCampoView());
        criterio.setFiltroObrigatorio(Boolean.TRUE);
        criterio.setValorPeriodoNumberUm(BigDecimal.TEN);
        controller.setCriteriosAtivos(Lists.newArrayList(criterio));
        assertTrue(controller.validarCriteriosAtivos().isEmpty());
    }

}