package br.com.juridico.controller;

import br.com.juridico.entidades.Imagem;
import br.com.juridico.entidades.Pessoa;
import br.com.juridico.entidades.User;
import br.com.juridico.impl.ImagemSessionBean;
import br.com.juridico.impl.PessoaSessionBean;
import br.com.juridico.util.FileUtil;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.servlet.http.Part;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

/**
 * Created by jeffersonl2009_00 on 21/10/2016.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ IOUtils.class, FileUtil.class})
public class ProfileControllerTest extends AbstractControllerTest {

    private static final String PAGINA_PERFIL = "/pages/profile.xhtml?faces-redirect=true";

    @Mock
    private Pessoa pessoa;
    @Mock
    private User user;
    @Mock
    private Part fotoSelecionada;
    @Mock
    private IOUtils ioUtils;
    @Mock
    private FileUtil fileUtil;
    @Mock
    private ImagemSessionBean imagemSessionBean;
    @Mock
    private PessoaSessionBean pessoaSessionBean;

    @InjectMocks
    private ProfileController controller;

    @Before
    public void up() {
        MockitoAnnotations.initMocks(this);
        mockObjects();
    }

    @Test
    public void testImportar() throws Exception {
        PowerMockito.mockStatic(FileUtil.class);
        PowerMockito.mockStatic(IOUtils.class);
        this.controller.setFotoSelecionada(fotoSelecionada);
        this.controller.setImagem(new Imagem());
        when(fileUtil.getFileName(fotoSelecionada)).thenReturn("arquivo_teste.png");
        doNothing().when(imagemSessionBean).create(this.controller.getImagem());
        doNothing().when(imagemSessionBean).update(this.controller.getImagem());
        when(pessoaSessionBean.retrieve(getUser().getId())).thenReturn(pessoa);
        doNothing().when(pessoaSessionBean).update(pessoa);
        doNothing().when(navigationHandler).handleNavigation(facesContext, null, PAGINA_PERFIL);
        this.controller.importar();
        Assert.assertEquals("arquivo_teste.png", this.controller.getImagem().getNomeArquivo());
        Assert.assertEquals(".png", this.controller.getImagem().getExtensaoArquivo());
    }

    @Test
    public void testImportar_sem_selecionar_foto() throws Exception {
        this.controller.setFotoSelecionada(null);
        this.controller.importar();
        Assert.assertNull(this.controller.getImagem());
    }



}