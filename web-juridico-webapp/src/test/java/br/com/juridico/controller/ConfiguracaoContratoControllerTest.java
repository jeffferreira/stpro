package br.com.juridico.controller;

import br.com.juridico.entidades.Contrato;
import br.com.juridico.entidades.ContratoSubContrato;
import br.com.juridico.entidades.User;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.impl.ContratoSubContratoSessionBean;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

/**
 * Created by jefferson.ferreira on 13/03/2017.
 */
@RunWith(PowerMockRunner.class)
public class ConfiguracaoContratoControllerTest extends AbstractControllerTest implements ICrudTest {

    private static final String PAGINA_FORMULARIO = "/pages/contratos/configuracao/configuracao-contrato-template.xhtml?faces-redirect=true";

    @Mock
    private User user;
    @Mock
    private ContratoSubContratoSessionBean contratoSubContratoSessionBean;

    @InjectMocks
    private ConfiguracaoContratoController controller;

    @Before
    public void up() {
        MockitoAnnotations.initMocks(this);
        mockObjects();
    }

    @Test
    public void testGoToFormulario() throws Exception {
        assertEquals(PAGINA_FORMULARIO, controller.goToFormulario());
    }

    @Test
    public void testSetEmpresaDeUsuarioLogado() throws Exception {
        when(getUser()).thenReturn(this.user);
        when(getUser().getCodEmpresa()).thenReturn(1L);
        this.controller.setEmpresaDeUsuarioLogado(this.controller.getObjetoSelecionado());
        assertEquals(Long.valueOf(1), this.controller.getObjetoSelecionado().getCodigoEmpresa());
    }

    @Test
    public void testValidar_passando_na_validacao() throws Exception {
        Contrato contrato = new Contrato();
        boolean existeContratoCadastrado = Boolean.FALSE;
        contrato.validar(existeContratoCadastrado);
        assertNotNull(contrato);
    }

    @Test(expected = ValidationException.class)
    public void testValidar_nao_passando_na_validacao() throws Exception {
        Contrato contrato = new Contrato();
        boolean existeContratoCadastrado = Boolean.TRUE;
        contrato.validar(existeContratoCadastrado);
    }

    @Test
    public void testInstanciarObjetoNovo() throws Exception {
        assertNotNull(this.controller.instanciarObjetoNovo());
    }

    @Test
    public void ultimoNoAbaixoRaiz_test() throws Exception {
        Contrato contrato = new Contrato();
        contrato.setId(1L);
        List<ContratoSubContrato> subContratos = Lists.newArrayList();
        ContratoSubContrato subContrato = new ContratoSubContrato();
        subContrato.setId(1L);
        subContrato.setContrato(contrato);
        subContrato.setPosicao("2");
        subContratos.add(subContrato);
        when(contratoSubContratoSessionBean.findByContrato(contrato.getId())).thenReturn(subContratos);
        assertEquals("2", controller.ultimoNoAbaixoRaiz(contrato));
    }

    @Test
    public void getContratoSubContrato_test(){
        Contrato contrato = new Contrato();
        contrato.setId(1L);
        List<ContratoSubContrato> subContratos = Lists.newArrayList();
        ContratoSubContrato subContrato = new ContratoSubContrato();
        subContrato.setId(1L);
        controller.setSubContrato("Fabrica");
        subContrato.setContrato(contrato);
        subContrato.setPosicao("2");
        subContratos.add(subContrato);
        when(contratoSubContratoSessionBean.findByContrato(contrato.getId())).thenReturn(subContratos);
        assertEquals("3. Fabrica", controller.getContratoSubContrato(contrato).toString());
    }

    @Test
    public void montarOrganograma_somente_no_raiz_test(){
        doNothing().when(contratoSubContratoSessionBean).clearSession();
        Contrato contrato = new Contrato();
        contrato.setId(1L);
        contrato.setDescricao("Venda de Veículos");
        List<ContratoSubContrato> subContratos = Lists.newArrayList();
        when(contratoSubContratoSessionBean.findByContrato(contrato.getId())).thenReturn(subContratos);
        controller.montarOrganograma(contrato);
        assertEquals("<ul><li><a href=\"#\">Venda de Veículos</a></li></ul>", controller.getHtml());
    }

    @Test
    public void montarOrganograma_um_noh_filho_test(){
        doNothing().when(contratoSubContratoSessionBean).clearSession();
        Contrato contrato = new Contrato();
        contrato.setId(1L);
        contrato.setDescricao("Venda de Veículos");
        List<ContratoSubContrato> subContratos = Lists.newArrayList();
        ContratoSubContrato subContrato = new ContratoSubContrato();
        subContrato.setId(1L);
        subContrato.setDescricao("Estoque");
        subContrato.setContrato(contrato);
        subContrato.setPosicao("1");
        subContratos.add(subContrato);
        when(contratoSubContratoSessionBean.findByContrato(contrato.getId())).thenReturn(subContratos);
        controller.montarOrganograma(contrato);
        assertEquals("<ul><li><a href=\"#\">Venda de Veículos</a><ul><li><a href=\"#\">1. Estoque</a></li></ul></li></ul>", controller.getHtml());
    }

    @Test
    public void montarOrganograma_dois_nos_filhos_test(){
        doNothing().when(contratoSubContratoSessionBean).clearSession();
        Contrato contrato = new Contrato();
        contrato.setId(1L);
        contrato.setDescricao("Venda de Veículos");
        List<ContratoSubContrato> subContratos = Lists.newArrayList();
        ContratoSubContrato subContrato = new ContratoSubContrato();
        subContrato.setId(1L);
        subContrato.setDescricao("Estoque");
        subContrato.setContrato(contrato);
        subContrato.setPosicao("1");

        ContratoSubContrato subContrato2 = new ContratoSubContrato();
        subContrato2.setId(2L);
        subContrato2.setDescricao("Fabrica");
        subContrato2.setContrato(contrato);
        subContrato2.setPosicao("2");

        subContratos.add(subContrato);
        subContratos.add(subContrato2);

        when(contratoSubContratoSessionBean.findByContrato(contrato.getId())).thenReturn(subContratos);
        controller.montarOrganograma(contrato);
        assertEquals("<ul><li><a href=\"#\">Venda de Veículos</a><ul><li><a href=\"#\">1. Estoque</a></li><li><a href=\"#\">2. Fabrica</a></li></ul></li></ul>", controller.getHtml());
    }

    @Test
    public void montarOrganograma_noh_filho_que_tem_filho_test(){
        doNothing().when(contratoSubContratoSessionBean).clearSession();
        Contrato contrato = new Contrato();
        contrato.setId(1L);
        contrato.setDescricao("Venda de Veículos");
        List<ContratoSubContrato> subContratos = Lists.newArrayList();
        ContratoSubContrato subContrato = new ContratoSubContrato();
        subContrato.setId(1L);
        subContrato.setDescricao("Estoque");
        subContrato.setContrato(contrato);
        subContrato.setPosicao("1");

        ContratoSubContrato filho = new ContratoSubContrato();
        filho.setId(2L);
        filho.setContratoSubContrato(subContrato);
        filho.setDescricao("Garantia");
        filho.setContrato(contrato);
        filho.setPosicao("1.1");
        subContrato.getFilhos().add(filho);

        subContratos.add(subContrato);
        subContratos.add(filho);

        when(contratoSubContratoSessionBean.findByContrato(contrato.getId())).thenReturn(subContratos);
        controller.montarOrganograma(contrato);
        assertEquals("<ul><li><a href=\"#\">Venda de Veículos</a><ul><li><a href=\"#\">1. Estoque</a><ul><li><a href=\"#\">1.1. Garantia</a></li></ul></li></ul></li></ul>", controller.getHtml());
    }

    @Test
    public void montarOrganograma_noh_filho_que_tem_filho_que_tambem_tem_filho_test(){
        doNothing().when(contratoSubContratoSessionBean).clearSession();
        Contrato contrato = new Contrato();
        contrato.setId(1L);
        contrato.setDescricao("Venda de Veículos");
        List<ContratoSubContrato> subContratos = Lists.newArrayList();
        ContratoSubContrato subContrato = new ContratoSubContrato();
        subContrato.setId(1L);
        subContrato.setDescricao("Estoque");
        subContrato.setContrato(contrato);
        subContrato.setPosicao("1");

        ContratoSubContrato filho = new ContratoSubContrato();
        filho.setId(2L);
        filho.setContratoSubContrato(subContrato);
        filho.setDescricao("Garantia");
        filho.setContrato(contrato);
        filho.setPosicao("1.1");

        ContratoSubContrato filhoDoFilho = new ContratoSubContrato();
        filhoDoFilho.setId(3L);
        filhoDoFilho.setContratoSubContrato(filho);
        filhoDoFilho.setDescricao("Confissão de dívida");
        filhoDoFilho.setContrato(contrato);
        filhoDoFilho.setPosicao("1.1.1");

        subContrato.getFilhos().add(filho);
        filho.getFilhos().add(filhoDoFilho);

        subContratos.add(subContrato);
        subContratos.add(filho);
        subContratos.add(filhoDoFilho);

        when(contratoSubContratoSessionBean.findByContrato(contrato.getId())).thenReturn(subContratos);
        controller.montarOrganograma(contrato);
        assertEquals("<ul><li><a href=\"#\">Venda de Veículos</a><ul><li><a href=\"#\">1. Estoque</a><ul><li><a href=\"#\">1.1. Garantia</a><ul><li><a href=\"#\">1.1.1. Confissão de dívida</a></li></ul></li></ul></li></ul></li></ul>", controller.getHtml());
    }

    @Ignore
    public void testAposCancelar() throws Exception {}

    @Ignore
    public void testIsHasAccess() throws Exception {}

}