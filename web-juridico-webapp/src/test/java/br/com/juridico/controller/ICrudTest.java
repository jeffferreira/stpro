package br.com.juridico.controller;

/**
 * Created by jeffersonl2009_00 on 19/10/2016.
 */
public interface ICrudTest {

    /**
     *
     */
    void up();

    /**
     *
     * @throws Exception
     */
    void testGoToFormulario() throws Exception;

    /**
     *
     * @throws Exception
     */
    void testSetEmpresaDeUsuarioLogado() throws Exception;

    /**
     *
     * @throws Exception
     */
    void testValidar_passando_na_validacao() throws Exception;

    /**
     *
     * @throws Exception
     */
    void testValidar_nao_passando_na_validacao() throws Exception;

    /**
     *
     * @throws Exception
     */
    void testAposCancelar() throws Exception;

    /**
     *
     * @throws Exception
     */
    void testIsHasAccess() throws Exception;

    /**
     * 
     * @throws Exception
     */
    void testInstanciarObjetoNovo() throws Exception;

}
