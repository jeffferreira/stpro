package br.com.juridico.controller;

import br.com.juridico.entidades.Empresa;
import br.com.juridico.entidades.Perfil;
import br.com.juridico.entidades.Pessoa;
import br.com.juridico.impl.EmpresaSessionBean;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.List;

import static junit.framework.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created by jeffersonl2009_00 on 24/10/2016.
 */
@RunWith(PowerMockRunner.class)
public class LoginControllerTest extends AbstractControllerTest {

    private static final String PAGINA_LOGIN = "/login/login.xhtml?faces-redirect=true";
    private static final String PAGES_LOGIN_RECUPERA_SENHA = "/login/recupera-senha.xhtml";
    private static final String PAGES_LOGIN_ALTERA_SENHA = "/login/altera-senha.xhtml";

    @InjectMocks
    private LoginController controller;

    @Mock
    private EmpresaSessionBean empresaSessionBean;

    @Before
    public void up() {
        MockitoAnnotations.initMocks(this);
        mockObjects();
    }

    @Test
    public void testAcessarSistema() throws Exception {

    }

    @Test
    public void testLogout() throws Exception {
        assertEquals(PAGINA_LOGIN, this.controller.logout());
    }

    @Test
    public void testAlterarSenha() throws Exception {
        assertEquals(PAGES_LOGIN_ALTERA_SENHA, this.controller.goToAlterarSenha());
    }

    @Test
    public void testRecuperarSenha() throws Exception {
        assertEquals(PAGES_LOGIN_RECUPERA_SENHA, this.controller.goToRecuperarSenha());
    }

    @Test
    public void testUpdateSenha() throws Exception {

    }

    @Test
    public void testCarregaPessoa_somente_um_cadastro() throws Exception {
        Long idPessoa = 7L;
        Long codigoEmpresa = 1L;
        List<Pessoa> pessoas = Lists.newArrayList();
        Pessoa pessoa = new Pessoa(idPessoa);
        pessoa.setCodigoEmpresa(codigoEmpresa);
        pessoas.add(pessoa);
        when(empresaSessionBean.retrieve(pessoa.getCodigoEmpresa())).thenReturn(new Empresa(1L));
        this.controller.carregaPessoa(pessoas);
        assertEquals(codigoEmpresa, this.controller.getEmpresaSelecionada());
        assertEquals(idPessoa, this.controller.pessoa.getId());
    }

    @Test
    public void verificaPessoaCadastradaEmMaisDeUmaEmpresa_true_test(){
        List<Pessoa> pessoas = Lists.newArrayList();

        Pessoa pessoaEmpresaUm = new Pessoa();
        pessoaEmpresaUm.setCodigoEmpresa(1L);

        Pessoa pessoaEmpresaDois = new Pessoa();
        pessoaEmpresaDois.setCodigoEmpresa(2L);

        pessoas.add(pessoaEmpresaUm);
        pessoas.add(pessoaEmpresaDois);

        this.controller.verificaPessoaCadastradaEmMaisDeUmaEmpresa(pessoas);
        assertTrue(this.controller.isPessoaCadastradaEmMaisDeUmaEmpresa());
    }

    @Test
    public void verificaPessoaCadastradaEmMaisDeUmaEmpresa_dois_usuarios_mesma_empresa_test(){
        List<Pessoa> pessoas = Lists.newArrayList();

        Pessoa pessoaEmpresaUm = new Pessoa();
        pessoaEmpresaUm.setCodigoEmpresa(1L);

        Pessoa outraPessoaEmpresaUm = new Pessoa();
        outraPessoaEmpresaUm.setCodigoEmpresa(1L);

        pessoas.add(pessoaEmpresaUm);
        pessoas.add(outraPessoaEmpresaUm);

        this.controller.verificaPessoaCadastradaEmMaisDeUmaEmpresa(pessoas);
        assertFalse(this.controller.isPessoaCadastradaEmMaisDeUmaEmpresa());
    }

    @Test
    public void verificaPessoaCadastradaEmMaisDeUmaEmpresa_false_test(){
        List<Pessoa> pessoas = Lists.newArrayList();
        this.controller.verificaPessoaCadastradaEmMaisDeUmaEmpresa(pessoas);
        assertFalse(this.controller.isPessoaCadastradaEmMaisDeUmaEmpresa());
    }

    @Test
    public void verificaPessoaCadastradaEmMaisDeUmPerfil_true_test(){
        List<Pessoa> pessoas = Lists.newArrayList();

        Pessoa pessoaPerilUm = new Pessoa();
        Perfil perfilAdmin = new Perfil("ADMIN", "ADMINISTRADOR", 1L, true);
        perfilAdmin.setId(1L);
        pessoaPerilUm.setPerfil(perfilAdmin);

        Pessoa pessoaPerfilDois = new Pessoa();
        Perfil perfilCliente = new Perfil("CLIENTE", "CLIENTE", 1L, true);
        perfilCliente.setId(2L);
        pessoaPerfilDois.setPerfil(perfilCliente);

        pessoas.add(pessoaPerilUm);
        pessoas.add(pessoaPerfilDois);

        this.controller.verificaPessaoCadastradaEmMaisDeUmPerfil(pessoas);
        assertTrue(this.controller.isPessoaCadastradaEmMaisDeUmPerfil());
    }

    @Test
    public void verificaPessoaCadastradaEmMaisDeUmPerfil_false_test(){
        List<Pessoa> pessoas = Lists.newArrayList();
        this.controller.verificaPessaoCadastradaEmMaisDeUmPerfil(pessoas);
        assertFalse(this.controller.isPessoaCadastradaEmMaisDeUmPerfil());
    }
}