package br.com.juridico.controller;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.*;

/**
 * Created by jeffersonl2009_00 on 12/12/2016.
 */
@RunWith(PowerMockRunner.class)
public class EmailCaixaSaidaControllerTest extends AbstractControllerTest {

    @InjectMocks
    EmailCaixaSaidaController controller;

    @Before
    public void up() {
        MockitoAnnotations.initMocks(this);
        mockObjects();
    }

}