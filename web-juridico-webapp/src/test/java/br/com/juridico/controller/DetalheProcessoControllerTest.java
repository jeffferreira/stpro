package br.com.juridico.controller;

import br.com.juridico.entidades.*;
import br.com.juridico.impl.*;
import br.com.juridico.types.DetalheProcessoStepsTypes;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.faces.event.ValueChangeEvent;
import java.util.ArrayList;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

/**
 * Created by jeffersonl2009_00 on 01/12/2016.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(ValueChangeEvent.class)
public class DetalheProcessoControllerTest  extends AbstractControllerTest {

    private static final String DETAIL_PROCESSO = "DETAIL_PROCESSO";
    private static final String PROCESSOS_ANDAMENTOS = "PROCESSOS_ANDAMENTOS";
    private static final String PROCESSOS_VINCULADOS = "PROCESSOS_VINCULADOS";
    private static final String PROCESSOS_CUSTAS = "PROCESSOS_CUSTAS";
    private static final String STEP_ANDAMENTO = "STEP_ANDAMENTO";
    private static final Long AUDIENCIA_CONCILIACAO = 1L;
    private static final String CUMPRIDO = "Cumprido";

    @Mock
    private ProcessoSessionBean processoSessionBean;
    @Mock
    private DireitoSessionBean direitoSessionBean;
    @Mock
    private PerfilAcessoSessionBean perfilAcessoSessionBean;
    @Mock
    private ProcessoVinculoSessionBean processoVinculoSessionBean;
    @Mock
    private StatusAgendaSessionBean statusAgendaSessionBean;
    @Mock
    private ValueChangeEvent valueChangeEvent;

    @InjectMocks
    DetalheProcessoController controller;

    @Before
    public void up() {
        MockitoAnnotations.initMocks(this);
        mockObjects();

        Direito alterar = new Direito("ALT", "Alterar");
        Direito excluir = new Direito("EXC", "Excluir");
        Direito incluir = new Direito("INC", "Incluir");
        when(direitoSessionBean.retrieve(ALTERAR)).thenReturn(alterar);
        when(direitoSessionBean.retrieve(EXCLUIR)).thenReturn(excluir);
        when(direitoSessionBean.retrieve(INCLUIR)).thenReturn(excluir);

        when(perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), PROCESSOS_ANDAMENTOS, alterar, getUser().getCodEmpresa())).thenReturn(true);
        when(perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), PROCESSOS_ANDAMENTOS, excluir, getUser().getCodEmpresa())).thenReturn(true);
        when(perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), PROCESSOS_ANDAMENTOS, incluir, getUser().getCodEmpresa())).thenReturn(true);

        when(perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), PROCESSOS_VINCULADOS, alterar, getUser().getCodEmpresa())).thenReturn(true);
        when(perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), PROCESSOS_VINCULADOS, excluir, getUser().getCodEmpresa())).thenReturn(true);
        when(perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), PROCESSOS_VINCULADOS, incluir, getUser().getCodEmpresa())).thenReturn(true);

        when(perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), PROCESSOS_CUSTAS, alterar, getUser().getCodEmpresa())).thenReturn(true);
        when(perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), PROCESSOS_CUSTAS, excluir, getUser().getCodEmpresa())).thenReturn(true);
        when(perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), PROCESSOS_CUSTAS, incluir, getUser().getCodEmpresa())).thenReturn(true);
    }

    @Test
    public void testInitWizardAndamento() throws Exception {
        controller.initWizardAndamento();
        assertEquals(DetalheProcessoStepsTypes.STEP_ANDAMENTO, controller.getStepsTypesSelecionado());
    }

    @Test
    public void testCarregaProcessoSelecionado() throws Exception {
        controller.setProcesso(new Processo(1L, "Processo ABC"));
        doNothing().when(processoSessionBean).clearSession();
        when(processoVinculoSessionBean.findListByProcesso(1L, getUser().getCodEmpresa())).thenReturn(new ArrayList<>());
        controller.carregaProcessoSelecionado();
        assertEquals(1L, controller.getProcesso().getId().longValue());
        assertEquals("Processo ABC", controller.getProcesso().getDescricao());
    }

    @Test
    public void testIsPodeAlterarAndamento(){
        assertTrue(controller.isPodeAlterarAndamento());
    }

    @Test
    public void testIsPodeExcluirAndamento(){
        assertTrue(controller.isPodeExcluirAndamento());
    }

    @Test
    public void testIsPodeIncluirAndamento(){
        assertTrue(controller.isPodeIncluirAndamento());
    }

    @Test
    public void testIsPodeAlterarVinculado(){
        assertTrue(controller.isPodeAlterarVinculado());
    }

    @Test
    public void testIsPodeExcluirVinculado(){
        assertTrue(controller.isPodeExcluirVinculado());
    }

    @Test
    public void testIsPodeIncluirVinculado(){
        assertTrue(controller.isPodeIncluirVinculado());
    }

    @Test
    public void testIsPodeAlterarCusta(){
        assertTrue(controller.isPodeAlterarCusta());
    }

    @Test
    public void testIsPodeExcluirCusta(){
        assertTrue(controller.isPodeExcluirCusta());
    }

    @Test
    public void testIsPodeIncluirCusta(){
        assertTrue(controller.isPodeIncluirCusta());
    }

    @Test
    public void testInstanciarObjetoNovo() throws Exception {
        assertNotNull(controller.instanciarObjetoNovo());
    }

    @Test
    public void testStepNext() throws Exception {
        controller.stepSelect(STEP_ANDAMENTO);
        controller.setProcesso(new Processo());
        controller.stepNext();
        assertEquals(DetalheProcessoStepsTypes.STEP_PRAZOS, controller.getStepsTypesSelecionado());
    }

    @Test
    public void testStepSelect() throws Exception {
        controller.stepSelect(STEP_ANDAMENTO);
        assertNotNull(controller.getStepClick());
        assertEquals(STEP_ANDAMENTO, controller.getStepClick());
    }

    @Test
    public void testIsRenderizaHouveAcordoAndamento_true(){
        StatusAgenda statusAgenda = new StatusAgenda(1L, CUMPRIDO, 1L);
        when(statusAgendaSessionBean.findByDescricao(statusAgenda.getDescricao(), getStateManager().getEmpresaSelecionada().getId())).thenReturn(statusAgenda);
        Andamento andamento = new Andamento();
        andamento.setStatusAgenda(statusAgenda);
        andamento.setTipoAndamento(new TipoAndamento(AUDIENCIA_CONCILIACAO));
        controller.setNovoAndamento(andamento);
        assertTrue(controller.isRenderizaHouveAcordoAndamento());
    }

    @Test
    public void testIsRenderizaHouveAcordoAndamento_false(){
        assertFalse(controller.isRenderizaHouveAcordoAndamento());
    }

    @Test
    public void testDefinePrazoChanged(){
        controller.setNovoAndamento(new Andamento());
        PowerMockito.mockStatic(ValueChangeEvent.class);
        when(valueChangeEvent.getNewValue()).thenReturn(true);
        controller.definePrazoChanged(valueChangeEvent);
        assertEquals("", controller.getNovoAndamento().getHoraInicio());
        assertEquals("", controller.getNovoAndamento().getHoraFim());
        assertTrue(controller.getNovoAndamento().getDefinePrazoFinal());
    }

}