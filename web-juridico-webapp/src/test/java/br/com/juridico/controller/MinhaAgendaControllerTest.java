package br.com.juridico.controller;

import br.com.juridico.entidades.*;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.impl.*;
import br.com.juridico.enums.EventoColorTypes;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.modules.junit4.PowerMockRunner;
import org.primefaces.event.ScheduleEntryMoveEvent;
import org.primefaces.event.ScheduleEntryResizeEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

/**
 * Created by jeffersonl2009_00 on 20/10/2016.
 */
@RunWith(PowerMockRunner.class)
public class MinhaAgendaControllerTest extends AbstractControllerTest {

    private static final String AGENDA_DAY_TYPE = "agendaDay";
    private static final String PAGINA_FORMULARIO = "/pages/agenda/unica/minha-agenda-form.xhtml?faces-redirect=true";
    private static final String AGENDA_ALTERACAO_AGENDA = "AGENDA_ALTERACAO_AGENDA";
    private static final String AGENDA_ADICIONAR_EVENTO = "AGENDA_ADICIONAR_EVENTO";
    private static final String A_CUMPRIR = "À cumprir";

    @Mock
    private EventoSessionBean eventoSessionBean;
    @Mock
    private PessoaSessionBean pessoaSessionBean;
    @Mock
    private EmailSessionBean emailSessionBean;
    @Mock
    private EmailCaixaSaidaSessionBean emailCaixaSaidaSessionBean;
    @Mock
    private StatusAgendaSessionBean statusAgendaSessionBean;
    @Mock
    private Pessoa pessoa;
    @Mock
    private SelectEvent selectEvent;
    @Mock
    private ScheduleEvent event;
    @Mock
    private ScheduleModel lazyEventModel;
    @Mock
    private ScheduleEntryMoveEvent scheduleEntryMoveEvent;
    @Mock
    private ScheduleEntryResizeEvent scheduleEntryResizeEvent;
    @Mock
    private ActionEvent actionEvent;
    @Mock
    private ValueChangeEvent valueChangeEvent;

    @InjectMocks
    private MinhaAgendaController controller;

    @Before
    public void up() {
        MockitoAnnotations.initMocks(this);
        mockObjects();
    }

    @Test
    public void testGoToMinhaAgenda() throws Exception {
        assertEquals(PAGINA_FORMULARIO, controller.goToMinhaAgenda());
    }

    @Test
    public void testCriarNovaAgenda() throws Exception {
        assertEquals("", this.controller.criarNovaAgenda());
        assertNotNull(this.controller.getEvent());
    }

    @Test
    public void testSelectAgenda() throws Exception {
        StatusAgenda cumprido = new StatusAgenda(1L, "Cumprido", 1L);
        this.controller.selectAgenda(cumprido);
        assertNotNull(this.controller.getStatusAgendaSelecionado());
        assertEquals(cumprido.getDescricao(), this.controller.getStatusAgendaSelecionado().getDescricao());
    }

    @Test
    public void testListaDeEventos_selecionando_status() throws Exception {
        when(pessoaSessionBean.retrieve(getUser().getId())).thenReturn(pessoa);
        List<Evento> eventos = Lists.newArrayList();
        eventos.add(new Evento());
        eventos.add(new Evento());
        StatusAgenda cumprido = new StatusAgenda(1L, "Cumprido", 1L);
        this.controller.selectAgenda(cumprido);
        this.controller.setDataInicioAgenda(new Date());
        this.controller.setDataFimAgenda(new Date());
        when(eventoSessionBean.findUserStatusEvents(controller.getStatusAgendaSelecionado(), pessoa, controller.getDataInicioAgenda(), controller.getDataFimAgenda(), getStateManager().getEmpresaSelecionada().getId())).thenReturn(eventos);
        assertEquals(2, this.controller.listaDeEventos().size());
    }

    @Test
    public void testListaDeEventos_nao_selecionando_status() throws Exception {
        when(pessoaSessionBean.retrieve(getUser().getId())).thenReturn(pessoa);
        List<Evento> eventos = Lists.newArrayList();
        eventos.add(new Evento());
        eventos.add(new Evento());
        eventos.add(new Evento());
        this.controller.setDataInicioAgenda(new Date());
        this.controller.setDataFimAgenda(new Date());
        when(eventoSessionBean.findUserEvents(new Pessoa(), controller.getDataInicioAgenda(), controller.getDataFimAgenda(), getStateManager().getEmpresaSelecionada().getId())).thenReturn(eventos);
        assertEquals(3, this.controller.listaDeEventos().size());

    }

    @Test
    public void testOnDateCalendarSelect() throws Exception {
        this.controller.onDateCalendarSelect(selectEvent);
        assertEquals(AGENDA_DAY_TYPE, this.controller.getViewType());
    }

    @Test
    public void testOnEventSelect() throws Exception {
        mockSelectEvento();
        assertNotNull(this.controller.getEventoSelecionado());
    }

    @Test
    public void testOnEventMove() throws Exception {
        this.controller.onEventMove(scheduleEntryMoveEvent);
        // implementar assert
    }

    @Test
    public void testOnEventResize() throws Exception {
        this.controller.onEventResize(scheduleEntryResizeEvent);
        // implementar assert

    }

    @Test
    public void testAddEvent() throws Exception {
        mockSelectEvento();
        mockEnviarEmail();
        StatusAgenda statusAgenda = new StatusAgenda(2L, "À Cumprir", 1L);
        statusAgenda.setStyleColor(EventoColorTypes.GREEN.getTextColor());
        doNothing().when(eventoSessionBean).update(controller.getEventoSelecionado());
        this.controller.getEventoSelecionado().setStatusAgenda(statusAgenda);
        this.controller.getEventoSelecionado().setPessoa(pessoa);
        HistoricoEvento historico = new HistoricoEvento(this.controller.getEventoSelecionado(), getUser().getName());
        this.controller.getEventoSelecionado().addHistorico(historico);
        this.controller.addEvent(actionEvent);
        assertNull(this.controller.getEventoSelecionado());
    }

    @Test
    public void testUpdateEvento() throws Exception {
        mockSelectEvento();
        doNothing().when(eventoSessionBean).update(this.controller.getEventoSelecionado());
        mockEnviarEmail();
        StatusAgenda statusAgenda = new StatusAgenda(2L, "À Cumprir", 1L);
        this.controller.getEventoSelecionado().setStatusAgenda(statusAgenda);
        this.controller.getEventoSelecionado().setPessoa(pessoa);
        HistoricoEvento historico = new HistoricoEvento(this.controller.getEventoSelecionado(), getUser().getName());
        this.controller.getEventoSelecionado().addHistorico(historico);
        this.controller.updateEvento();
        assertNotNull(this.controller.getEventoSelecionado());
    }

    @Test
    public void testRemoveEvent() throws Exception {
        when((ScheduleEvent) selectEvent.getObject()).thenReturn(event);
        this.controller.setLazyEventModel(lazyEventModel);
        doNothing().when(eventoSessionBean).delete(this.controller.getEventoSelecionado());
        mockEnviarEmail();
        this.controller.excluir();
    }

    @Test
    public void testSalvarEvento() throws Exception {
        mockSelectEvento();
        this.controller.setHoraInicio("10:00");
        this.controller.setHoraFim("11:00");
        when(event.getStartDate()).thenReturn(new Date());
        when(event.getEndDate()).thenReturn(new Date());

        StatusAgenda statusAgenda = new StatusAgenda(2L, "À Cumprir", 1L);
        statusAgenda.setStyleColor("event-text-color-green");
        when(statusAgendaSessionBean.findByDescricao(A_CUMPRIR, getStateManager().getEmpresaSelecionada().getId())).thenReturn(statusAgenda);
        when(pessoaSessionBean.findUser(getUser().getLogin(), getStateManager().getEmpresaSelecionada().getId())).thenReturn(pessoa);
        doNothing().when(eventoSessionBean).create(new Evento());
        mockEnviarEmail();

        assertEquals(PAGINA_FORMULARIO, this.controller.salvarEvento());
        assertEquals("event-style-green", this.controller.getStyleClassSelect());
    }

    @Test
    public void testHoraInicioChanged() throws Exception {
        when((String) valueChangeEvent.getNewValue()).thenReturn("11:00");
        this.controller.horaInicioChanged(valueChangeEvent);
        assertNotNull(this.controller.getHoraFim());
        assertEquals("12:00", this.controller.getHoraFim());
    }

    @Test
    public void testTransferenciaChanged() throws Exception {
        when((Boolean) valueChangeEvent.getNewValue()).thenReturn(false);
        when(pessoaSessionBean.retrieve(getUser().getId())).thenReturn(new Pessoa());
        this.controller.transferenciaChanged(valueChangeEvent);
        assertNotNull(this.controller.getEventoSelecionado().getPessoa());
    }

    @Test
    public void testSemHorarioChanged() throws Exception {
        this.controller.setHoraInicio("11:00");
        this.controller.setHoraFim("18:00");
        when((Boolean) valueChangeEvent.getNewValue()).thenReturn(true);
        this.controller.semHorarioChanged(valueChangeEvent);
        assertEquals("", this.controller.getHoraInicio());
        assertEquals("", this.controller.getHoraInicio());
    }

    /**
     *
     * @throws ValidationException
     */
    private void mockEnviarEmail() throws ValidationException {
        Email email = new Email(AGENDA_ALTERACAO_AGENDA, "Alteracao agenda", "Este teste altera agenda....", 1L, null, true);
        when(emailSessionBean.findBySigla(AGENDA_ALTERACAO_AGENDA, getUser().getCodEmpresa())).thenReturn(email);
        when(emailSessionBean.findBySigla(AGENDA_ADICIONAR_EVENTO, getUser().getCodEmpresa())).thenReturn(email);
        when(pessoaSessionBean.findByAdministradores(getUser().getCodEmpresa())).thenReturn(Lists.newArrayList());
        this.controller.setLazyEventModel(lazyEventModel);
        doNothing().when(emailCaixaSaidaSessionBean).create(new EmailCaixaSaida());
    }

    /**
     *
     */
    private void mockSelectEvento(){
        when((ScheduleEvent) selectEvent.getObject()).thenReturn(event);
        when(eventoSessionBean.findByChaveId((String) event.getData())).thenReturn(new Evento());
        this.controller.onEventSelect(selectEvent);
    }

}