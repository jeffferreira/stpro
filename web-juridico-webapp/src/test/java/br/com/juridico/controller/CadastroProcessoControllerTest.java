package br.com.juridico.controller;

import br.com.juridico.entidades.*;
import br.com.juridico.types.ProcessoStepsTypes;
import br.com.juridico.util.DateUtils;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Date;

import static org.junit.Assert.assertEquals;

/**
 * Created by jefferson on 26/11/2016.
 */
@RunWith(PowerMockRunner.class)
public class CadastroProcessoControllerTest extends AbstractControllerTest {

    private static final String STEP_DADOS_PROCESSO = "STEP_DADOS_PROCESSO";
    private static final String STEP_PARTES = "STEP_PARTES";
    private static final String STEP_RESUMO_ACAO = "STEP_RESUMO_ACAO";
    private static final String STEP_VALORES = "STEP_VALORES";
    private static final String STEP_HONORARIOS = "STEP_HONORARIOS";
    private static final String STEP_CONFIRMACAO = "STEP_CONFIRMACAO";
    private static final String DISABLED = "disabled";
    private static final String ACTIVE = "active";
    private static final String COMPLETE = "complete";

    private boolean possuiAcessoAosHonorarios;

    @InjectMocks
    CadastroProcessoController controller;

    @Before
    public void up() {
        MockitoAnnotations.initMocks(this);
        mockObjects();
    }

    @Test
    public void testStepSelect_partes() throws Exception {
        controller.stepSelect(ProcessoStepsTypes.STEP_PARTES.getValue());
        assertEquals(STEP_PARTES, controller.getStepClick());
    }

    @Test
    public void testStepSelect_dados_processo() throws Exception {
        controller.stepSelect(ProcessoStepsTypes.STEP_DADOS_PROCESSO.getValue());
        assertEquals(STEP_DADOS_PROCESSO, controller.getStepClick());
    }

    @Test
    public void testStepSelect_resumo_acao() throws Exception {
        controller.stepSelect(ProcessoStepsTypes.STEP_RESUMO_ACAO.getValue());
        assertEquals(STEP_RESUMO_ACAO, controller.getStepClick());
    }

    @Test
    public void testStepSelect_valores() throws Exception {
        controller.stepSelect(ProcessoStepsTypes.STEP_VALORES.getValue());
        assertEquals(STEP_VALORES, controller.getStepClick());
    }

    @Test
    public void testStepSelect_honorarios() throws Exception {
        controller.stepSelect(ProcessoStepsTypes.STEP_HONORARIOS.getValue());
        assertEquals(STEP_HONORARIOS, controller.getStepClick());
    }

    @Test
    public void testStepSelect_confirmacao() throws Exception {
        controller.stepSelect(ProcessoStepsTypes.STEP_CONFIRMACAO.getValue());
        assertEquals(STEP_CONFIRMACAO, controller.getStepClick());
    }

    @Test
    public void testStepNext_de_partes_para_dados_processo() throws Exception {
        controller.stepSelect(ProcessoStepsTypes.STEP_PARTES.getValue());
        controller.stepNext(possuiAcessoAosHonorarios);
        assertEquals(COMPLETE, controller.getProcessoStepSelecionado().getWizardParte());
        assertEquals(ACTIVE, controller.getProcessoStepSelecionado().getWizardDadosProcesso());
        assertEquals(DISABLED, controller.getProcessoStepSelecionado().getWizardResumoAcao());
        assertEquals(DISABLED, controller.getProcessoStepSelecionado().getWizardValores());
        assertEquals(DISABLED, controller.getProcessoStepSelecionado().getWizardHonorarios());
        assertEquals(DISABLED, controller.getProcessoStepSelecionado().getWizardConfirmacao());
        assertEquals(STEP_DADOS_PROCESSO, controller.getProcessoStepSelecionado().getValue());
    }

    @Test
    public void testStepNext_de_dados_processo_para_resumo_acao() throws Exception {
        controller.stepSelect(ProcessoStepsTypes.STEP_DADOS_PROCESSO.getValue());
        controller.stepNext(possuiAcessoAosHonorarios);
        assertEquals(COMPLETE, controller.getProcessoStepSelecionado().getWizardParte());
        assertEquals(COMPLETE, controller.getProcessoStepSelecionado().getWizardDadosProcesso());
        assertEquals(ACTIVE, controller.getProcessoStepSelecionado().getWizardResumoAcao());
        assertEquals(DISABLED, controller.getProcessoStepSelecionado().getWizardValores());
        assertEquals(DISABLED, controller.getProcessoStepSelecionado().getWizardHonorarios());
        assertEquals(DISABLED, controller.getProcessoStepSelecionado().getWizardConfirmacao());
        assertEquals(STEP_RESUMO_ACAO, controller.getProcessoStepSelecionado().getValue());
    }

    @Test
    public void testStepNext_de_resumo_acao_para_valores() throws Exception {
        controller.stepSelect(ProcessoStepsTypes.STEP_RESUMO_ACAO.getValue());
        controller.stepNext(possuiAcessoAosHonorarios);
        assertEquals(COMPLETE, controller.getProcessoStepSelecionado().getWizardParte());
        assertEquals(COMPLETE, controller.getProcessoStepSelecionado().getWizardDadosProcesso());
        assertEquals(COMPLETE, controller.getProcessoStepSelecionado().getWizardResumoAcao());
        assertEquals(ACTIVE, controller.getProcessoStepSelecionado().getWizardValores());
        assertEquals(DISABLED, controller.getProcessoStepSelecionado().getWizardHonorarios());
        assertEquals(DISABLED, controller.getProcessoStepSelecionado().getWizardConfirmacao());
        assertEquals(STEP_VALORES, controller.getProcessoStepSelecionado().getValue());
    }

    @Test
    public void testStepNext_de_valores_para_confirmacao_pois_nao_possui_permissao_honorarios() throws Exception {
        controller.stepSelect(ProcessoStepsTypes.STEP_VALORES.getValue());
        controller.stepNext(possuiAcessoAosHonorarios);
        assertEquals(COMPLETE, controller.getProcessoStepSelecionado().getWizardParte());
        assertEquals(COMPLETE, controller.getProcessoStepSelecionado().getWizardDadosProcesso());
        assertEquals(COMPLETE, controller.getProcessoStepSelecionado().getWizardResumoAcao());
        assertEquals(COMPLETE, controller.getProcessoStepSelecionado().getWizardValores());
        assertEquals(COMPLETE, controller.getProcessoStepSelecionado().getWizardHonorarios());
        assertEquals(ACTIVE, controller.getProcessoStepSelecionado().getWizardConfirmacao());
        assertEquals(STEP_CONFIRMACAO, controller.getProcessoStepSelecionado().getValue());
    }

    @Test
    public void testStepNext_de_valores_para_honorarios() throws Exception {
        possuiAcessoAosHonorarios = Boolean.TRUE;
        controller.stepSelect(ProcessoStepsTypes.STEP_VALORES.getValue());
        controller.stepNext(possuiAcessoAosHonorarios);
        assertEquals(COMPLETE, controller.getProcessoStepSelecionado().getWizardParte());
        assertEquals(COMPLETE, controller.getProcessoStepSelecionado().getWizardDadosProcesso());
        assertEquals(COMPLETE, controller.getProcessoStepSelecionado().getWizardResumoAcao());
        assertEquals(COMPLETE, controller.getProcessoStepSelecionado().getWizardValores());
        assertEquals(ACTIVE, controller.getProcessoStepSelecionado().getWizardHonorarios());
        assertEquals(DISABLED, controller.getProcessoStepSelecionado().getWizardConfirmacao());
        assertEquals(STEP_HONORARIOS, controller.getProcessoStepSelecionado().getValue());
    }

    @Test
    public void testStepNext_de_honorarios_para_confirmacao() throws Exception {
        possuiAcessoAosHonorarios = Boolean.TRUE;
        controller.stepSelect(ProcessoStepsTypes.STEP_HONORARIOS.getValue());
        controller.stepNext(possuiAcessoAosHonorarios);
        assertEquals(COMPLETE, controller.getProcessoStepSelecionado().getWizardParte());
        assertEquals(COMPLETE, controller.getProcessoStepSelecionado().getWizardDadosProcesso());
        assertEquals(COMPLETE, controller.getProcessoStepSelecionado().getWizardResumoAcao());
        assertEquals(COMPLETE, controller.getProcessoStepSelecionado().getWizardValores());
        assertEquals(COMPLETE, controller.getProcessoStepSelecionado().getWizardHonorarios());
        assertEquals(ACTIVE, controller.getProcessoStepSelecionado().getWizardConfirmacao());
        assertEquals(STEP_CONFIRMACAO, controller.getProcessoStepSelecionado().getValue());
    }

    @Test
    public void testCarregaTooltip_empty() throws Exception {
        assertEquals(null, controller.carregaTooltip(new Pessoa()));
    }

    @Test
    public void testCarregaTooltip_lista_tamanho_tres() throws Exception {
        Pessoa pessoa = new Pessoa(1L, "Joao do teste", 'F', true);
        HistoricoPessoaCliente historico = new HistoricoPessoaCliente(true, pessoa, new Date(), "", 1L);
        pessoa.addHistorico(historico);
        assertEquals(3, controller.carregaTooltip(pessoa).size());

        assertEquals(DateUtils.formataDataBrasil(new Date()), controller.carregaTooltip(pessoa).get(0));
        assertEquals("", controller.carregaTooltip(pessoa).get(1));
        assertEquals("", controller.carregaTooltip(pessoa).get(2));
    }

    @Test
    public void testCarregaTooltip() throws Exception {
        Pessoa pessoa = new Pessoa(1L, "Joao do teste", 'F', true);
        HistoricoPessoaCliente historico = new HistoricoPessoaCliente(true, pessoa, new Date(), "", 1L);
        pessoa.addHistorico(historico);
        pessoa.addPessoasEmails(new PessoaEmail(pessoa, "teste@gmail.com", 1L));
        pessoa.addPessoasContatos(new PessoaContato(new Contato(44, "91111111"), pessoa, 1L));
        assertEquals(3, controller.carregaTooltip(pessoa).size());

        assertEquals(DateUtils.formataDataBrasil(new Date()), controller.carregaTooltip(pessoa).get(0));
        assertEquals("(44) 9111-1111;", controller.carregaTooltip(pessoa).get(1));
        assertEquals("teste@gmail.com;", controller.carregaTooltip(pessoa).get(2));
    }
}