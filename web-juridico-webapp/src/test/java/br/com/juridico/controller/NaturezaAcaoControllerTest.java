package br.com.juridico.controller;

import br.com.juridico.entidades.User;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.impl.NaturezaAcaoSessionBean;
import com.google.common.collect.Maps;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created by jeffersonl2009_00 on 19/10/2016.
 */
@RunWith(PowerMockRunner.class)
public class NaturezaAcaoControllerTest extends AbstractControllerTest implements ICrudTest {

    private static final String CHAVE_ACESSO = "NATUREZAACAO_NATUREZA_ACAO_TEMPLATE";
    private static final String PAGINA_FORMULARIO = "/pages/cadastros/naturezaAcao/natureza-acao-template.xhtml?faces-redirect=true";

    @Mock
    private User user;
    @Mock
    private NaturezaAcaoSessionBean naturezaAcaoSessionBean;

    @InjectMocks
    private NaturezaAcaoController controller;


    @Before
    public void up() {
        MockitoAnnotations.initMocks(this);
        mockObjects();
    }

    @Test
    public void testGoToFormulario() throws Exception {
        assertEquals(PAGINA_FORMULARIO, controller.goToFormulario());
    }

    @Test
    public void testSetEmpresaDeUsuarioLogado() throws Exception {
        when(getUser()).thenReturn(this.user);
        when(getUser().getCodEmpresa()).thenReturn(1L);
        this.controller.setEmpresaDeUsuarioLogado(this.controller.getObjetoSelecionado());
        assertEquals(Long.valueOf(1), this.controller.getObjetoSelecionado().getCodigoEmpresa());
    }

    @Test
    public void testValidar_passando_na_validacao() throws Exception {
        boolean exceptionNaValidacao = false;
        this.controller.getObjetoSelecionado().setDescricao("NATUREZA_ACAO");
        when(naturezaAcaoSessionBean.isDescricaoJaCadastrada(controller.getObjetoSelecionado(), getUser().getCodEmpresa())).thenReturn(exceptionNaValidacao);
        this.controller.validar();
        assertEquals("NATUREZA_ACAO", this.controller.getObjetoSelecionado().getDescricao());
    }

    @Test(expected = ValidationException.class)
    public void testValidar_nao_passando_na_validacao() throws Exception {
        boolean exceptionNaValidacao = true;
        when(naturezaAcaoSessionBean.isDescricaoJaCadastrada(controller.getObjetoSelecionado(), getUser().getCodEmpresa())).thenReturn(exceptionNaValidacao);
        this.controller.validar();
    }

    @Test
    public void testAposCancelar() throws Exception {
        this.controller.getObjetoSelecionado().setDescricao("BLA BLA BLA...");
        this.controller.aposCancelar();
        assertNull(this.controller.getObjetoSelecionado().getDescricao());
    }

    @Test
    public void testIsHasAccess() throws Exception {
        Map<String, String> mapaAcessos = Maps.newHashMap();
        mapaAcessos.put(CHAVE_ACESSO, CHAVE_ACESSO);
        this.user = new User(1L, "User NATUREZA_ACAO teste");
        this.user.getAcessosMap().put(CHAVE_ACESSO, CHAVE_ACESSO);
        when(getSessionAttribute(USER_LOGGED)).thenReturn(this.user);
        assertTrue(controller.isHasAccess());
    }

    @Test
    public void testInstanciarObjetoNovo() throws Exception {
        assertNotNull(this.controller.instanciarObjetoNovo());
    }
}