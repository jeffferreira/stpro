package br.com.juridico.controller;

import br.com.juridico.entidades.*;
import br.com.juridico.enums.TipoEndereco;
import br.com.juridico.impl.*;
import br.com.juridico.util.DadosMockUtils;
import com.google.common.collect.Maps;
import org.apache.commons.fileupload.RequestContext;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.util.Map;
import java.util.ResourceBundle;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

/**
 * Created by jeffersonl2009_00 on 17/10/2016.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ FacesContext.class, FacesMessage.class, RequestContext.class, ResourceBundle.class})
public class PessoaFisicaControllerTest extends AbstractControllerTest {

    private static final String CLIENTE = "CLIENTE";
    private static final String PESSOAFISICA_PESSOA_FISICA_FORM = "PESSOAFISICA_PESSOA_FISICA_FORM";
    private static final String PAGES_PESSOAS_PESSOA_FISICA_LIST = "/pages/pessoas/pessoaFisica/pessoa-fisica-list.xhtml";
    private static final String PAGES_PESSOAS_PESSOA_FISICA_FORM = "/pages/pessoas/pessoaFisica/pessoa-fisica-form.xhtml";

    @Mock
    private PerfilSessionBean perfilSessionBean;
    @Mock
    private EstadoCivilSessionBean estadoCivilSessionBean;
    @Mock
    private TipoContatoSessionBean tipoContatoSessionBean;
    @Mock
    private User user;
    @Mock
    private PessoaSessionBean pessoaSessionBean;
    @Mock
    private PessoaFisicaSessionBean pessoaFisicaSessionBean;
    @Mock
    private DireitoSessionBean direitoSessionBean;
    @Mock
    private PerfilAcessoSessionBean perfilAcessoSessionBean;

    @InjectMocks
    private PessoaFisicaController controller;

    private Pessoa pessoa;


    @Before
    public void up() {
        MockitoAnnotations.initMocks(this);
        mockObjects();
        when(perfilSessionBean.findBySigla(CLIENTE, getUser().getCodEmpresa())).thenReturn(new Perfil("ADMIN", "Administrador do Sistema", 1L, Boolean.TRUE));
        when(estadoCivilSessionBean.retrieveAll()).thenReturn(DadosMockUtils.getEstatosCivis());
        when(tipoContatoSessionBean.retrieveAll(getUser().getCodEmpresa())).thenReturn(DadosMockUtils.getTipoContatos());

        Direito alterar = new Direito("ALT", "Alterar");
        Direito excluir = new Direito("EXC", "Excluir");
        Direito incluir = new Direito("INC", "Incluir");
        when(direitoSessionBean.retrieve(ALTERAR)).thenReturn(alterar);
        when(direitoSessionBean.retrieve(EXCLUIR)).thenReturn(excluir);
        when(direitoSessionBean.retrieve(INCLUIR)).thenReturn(excluir);
        when(perfilAcessoSessionBean.possuiDireitoAcesso(getUser().getPerfil(), PESSOAFISICA_PESSOA_FISICA_FORM, alterar, getUser().getCodEmpresa())).thenReturn(true);

        this.pessoa =  new Pessoa(1L, "Joao do Teste", 'F', true);
        controller.initialize();

    }

    @Test
    public void testIsHasAccessCadastro() throws Exception {
        Map<String, String> mapaAcessos = Maps.newHashMap();
        mapaAcessos.put(PESSOAFISICA_PESSOA_FISICA_FORM, PESSOAFISICA_PESSOA_FISICA_FORM);
        this.user = new User(1L, "Jao do teste");
        this.user.getAcessosMap().put(PESSOAFISICA_PESSOA_FISICA_FORM, PESSOAFISICA_PESSOA_FISICA_FORM);
        when(getSessionAttribute(USER_LOGGED)).thenReturn(this.user);

        assertTrue(controller.isHasAccessCadastro());
    }

    @Test
    public void passando_email_vazio_ou_branco_test() throws Exception {
        this.controller.adicionarEmail();
        assertTrue(controller.getObjetoSelecionado().getPessoa().getPessoasEmails().isEmpty());
    }

    @Test
    public void testAdicionarEmail() throws Exception {
        this.controller.setPessoaEmail(new PessoaEmail(this.pessoa, "teste@gmail.com", 1L));
        this.controller.adicionarEmail();
        assertFalse(controller.getObjetoSelecionado().getPessoa().getPessoasEmails().isEmpty());
        assertEquals(1, controller.getObjetoSelecionado().getPessoa().getPessoasEmails().size());
    }

    @Test
    public void passando_endereco_vazio_ou_branco_test() throws Exception {
        this.controller.adicionarEndereco();
        assertTrue(controller.getObjetoSelecionado().getPessoa().getPessoasEnderecos().isEmpty());
    }

    @Test
    public void testAdicionarEndereco() throws Exception {
        Endereco endereco = new Endereco("Rua XYZ...", "87015-440", new Cidade(), TipoEndereco.RESIDENCIAL.getDescricao());
        this.controller.setPessoaEndereco(new PessoaEndereco(endereco, this.pessoa, 1L));
        this.controller.adicionarEndereco();
        assertFalse(controller.getObjetoSelecionado().getPessoa().getPessoasEnderecos().isEmpty());
        assertEquals(1, controller.getObjetoSelecionado().getPessoa().getPessoasEnderecos().size());
    }

    @Test
    public void passando_contato_vazio_ou_branco_test() throws Exception {
        this.controller.adicionarContato();
        assertTrue(controller.getObjetoSelecionado().getPessoa().getPessoasContatos().isEmpty());
    }

    @Test
    public void testAdicionarContato() throws Exception {
        Contato contato = new Contato(new TipoContato(1l, "Telefone"), "Descricao", "91281273", 44);
        this.controller.setPessoaContato(new PessoaContato(contato, this.pessoa, 1L));
        this.controller.adicionarContato();
        assertFalse(controller.getObjetoSelecionado().getPessoa().getPessoasContatos().isEmpty());
        assertEquals(1, controller.getObjetoSelecionado().getPessoa().getPessoasContatos().size());
    }

    @Test
    public void testGoToList() throws Exception {
        doNothing().when(pessoaSessionBean).clearSession();
        doNothing().when(pessoaFisicaSessionBean).clearSession();
        assertEquals(PAGES_PESSOAS_PESSOA_FISICA_LIST, controller.goToList());
    }

    @Test
    public void testGoToEditarPessoa() throws Exception {
        assertEquals(PAGES_PESSOAS_PESSOA_FISICA_FORM, controller.goToEditarPessoa(this.controller.getObjetoSelecionado()));
    }

    @Test
    public void testNovaPessoaFisica() throws Exception {
        assertEquals(PAGES_PESSOAS_PESSOA_FISICA_FORM, controller.novaPessoaFisica());
    }

    @Test
    public void testLimpaNumeroEndereco() throws Exception {
        Endereco endereco = new Endereco("Rua XYZ...", "87015-440", new Cidade(), TipoEndereco.RESIDENCIAL.getDescricao());
        endereco.setSemNumero(true);
        this.controller.setPessoaEndereco(new PessoaEndereco(endereco, this.pessoa, 1L));
        this.controller.limpaNumeroEndereco();

        assertNull(this.controller.getPessoaEndereco().getEndereco().getNumero());
        assertTrue(this.controller.getPessoaEndereco().getEndereco().getSemNumero());
    }

    @Test
    public void testLimpaNumeroEndereco_mas_nao_selecionou_sem_numero() throws Exception {
        Endereco endereco = new Endereco("Rua XYZ...", "87015-440", new Cidade(), TipoEndereco.RESIDENCIAL.getDescricao());
        endereco.setSemNumero(false);
        endereco.setNumero("644");
        this.controller.setPessoaEndereco(new PessoaEndereco(endereco, this.pessoa, 1L));
        this.controller.limpaNumeroEndereco();

        assertEquals("644", this.controller.getPessoaEndereco().getEndereco().getNumero());
        assertFalse(this.controller.getPessoaEndereco().getEndereco().getSemNumero());
    }

    @Test
    public void testSetEmpresaDeUsuarioLogado() throws Exception {
        when(getUser()).thenReturn(this.user);
        when(getUser().getCodEmpresa()).thenReturn(1L);
        this.controller.setEmpresaDeUsuarioLogado(this.controller.getObjetoSelecionado());

        assertEquals(Long.valueOf(1), this.controller.getObjetoSelecionado().getCodigoEmpresa());
        assertEquals(Long.valueOf(1), this.controller.getObjetoSelecionado().getPessoa().getCodigoEmpresa());
    }

}
