package br.com.juridico.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.assertEquals;


/**
 * Created by jeffersonl2009_00 on 07/12/2016.
 */
@RunWith(PowerMockRunner.class)
public class ControladoriaControllerTest extends AbstractControllerTest {

    private static final String PAGINA_FORMULARIO_PRAZO_PROCESSO = "/pages/controladoria/prazoprocesso/prazo-processo-template.xhtml?faces-redirect=true";

    @InjectMocks
    ControladoriaController controller;

    @Before
    public void up() {
        MockitoAnnotations.initMocks(this);
        mockObjects();
    }

    @Test
    public void testGoToPrazosProcessos() throws Exception {
        assertEquals(PAGINA_FORMULARIO_PRAZO_PROCESSO, controller.goToPrazosProcessos());
    }
}