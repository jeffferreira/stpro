package br.com.juridico.business;

import br.com.juridico.dto.ParteDto;
import br.com.juridico.entidades.Processo;
import br.com.juridico.exception.ValidationException;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class ParteProcessoBusinessTest {

	@Before
	public void up() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
    public void adiciona_parte_test() throws ValidationException {
		ParteDto parteDto = new ParteDto();
		List<ParteDto> partes = Lists.newArrayList();

		ParteProcessoBusiness.adicionarParte(parteDto, partes);
		assertEquals(true, !partes.isEmpty());
	}

}
