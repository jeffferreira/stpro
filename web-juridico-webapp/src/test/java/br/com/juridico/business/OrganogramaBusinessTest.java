package br.com.juridico.business;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by jefferson.ferreira on 09/03/2017.
 */
public class OrganogramaBusinessTest {

    private static final String NO_UL_LI_PREFIX = "<ul><li>";
    private static final String NO_UL_LI_SUFIX = "</li></ul>";
    private static final String NO_VERTICAL_PREFIX = "<ul class=\"vertical\">";
    private static final String NO_VERTICAL_SUFIX = "</ul>";
    private static final String HREF_PREFIX = "<a href=\"#\">";
    private static final String HREF_SUFIX = "</a>";
    private static final String LI_PREFIX = "<li>";
    private static final String LI_SUFIX = "</li>";
    private static final String UL_PREFIX = "<ul>";
    private static final String UL_SUFIX = "</ul>";

    private StringBuilder html;

    @Before
    public void up(){
        this.html = new StringBuilder();
    }

    @Test
    public void abrirTag_test() throws Exception {
        StringBuilder result = OrganogramaBusiness.abrirTag(this.html);
        assertEquals(NO_UL_LI_PREFIX, result.toString());
    }

    @Test
    public void abrirTagItem_test() throws Exception {
        StringBuilder result = OrganogramaBusiness.abrirTagItem(this.html, "Venda Veiculo");
        assertEquals(NO_UL_LI_PREFIX + HREF_PREFIX +"Venda Veiculo"+HREF_SUFIX, result.toString());
    }

    @Test
    public void fecharTagItem_test() throws Exception {
        StringBuilder result = OrganogramaBusiness.fecharTagItem(this.html);
        assertEquals(NO_UL_LI_SUFIX, result.toString());
    }

    @Test
    public void abrirFecharItem_test() throws Exception {
        this.html = OrganogramaBusiness.abrirTagItem(this.html, "Venda Veiculo");
        this.html =  OrganogramaBusiness.fecharTagItem(this.html);
        assertEquals(NO_UL_LI_PREFIX + HREF_PREFIX + "Venda Veiculo"+ HREF_SUFIX + NO_UL_LI_SUFIX, this.html.toString());
    }

    @Test
    public void abrirTagFilho_test() throws Exception {
        StringBuilder result = OrganogramaBusiness.abrirTagFilho(this.html, "Fabrica");
        assertEquals(LI_PREFIX + HREF_PREFIX + "Fabrica", result.toString());
    }

    @Test
    public void fecharTagFilho_test() throws Exception {
        StringBuilder result = OrganogramaBusiness.fecharTagFilho(this.html);
        assertEquals( HREF_SUFIX + LI_SUFIX, result.toString());
    }

    @Test
    public void abrirFecharTagFilho_test()throws Exception {
        this.html = OrganogramaBusiness.abrirTagFilho(this.html, "Fabrica");
        this.html = OrganogramaBusiness.fecharTagFilho(this.html);
        assertEquals(LI_PREFIX + HREF_PREFIX + "Fabrica" + HREF_SUFIX + LI_SUFIX, this.html.toString());
    }

    @Test
    public void abrirNoVertical_test()throws Exception {
        StringBuilder result = OrganogramaBusiness.abrirNoVertical(this.html);
        assertEquals(NO_VERTICAL_PREFIX, result.toString());
    }

    @Test
    public void fecharNoVertical_test()throws Exception {
        StringBuilder result = OrganogramaBusiness.fecharNoVertical(this.html);
        assertEquals(NO_VERTICAL_SUFIX, result.toString());
    }

    @Test
    public void fecharSomenteNoFilho_test()throws Exception {
        StringBuilder result = OrganogramaBusiness.fecharSomenteNoFilho(this.html);
        assertEquals(LI_SUFIX, result.toString());
    }

    @Test
    public void abrir_ul_test()throws Exception {
        StringBuilder result = OrganogramaBusiness.abrirUl(this.html);
        assertEquals(UL_PREFIX, result.toString());
    }

    @Test
    public void fechar_ul_test()throws Exception {
        StringBuilder result = OrganogramaBusiness.fecharUl(this.html);
        assertEquals(UL_SUFIX, result.toString());
    }

    @Test
    public void fechar_href_test()throws Exception {
        StringBuilder result = OrganogramaBusiness.fecharHref(this.html);
        assertEquals(HREF_SUFIX, result.toString());
    }

    @Test
    public void casoNinjaBoard_test(){
        this.html = OrganogramaBusiness.abrirTagItem(this.html, "Ninja Board");
        this.html = OrganogramaBusiness.abrirTagItem(this.html, "Staff");
        this.html = OrganogramaBusiness.abrirNoVertical(this.html);
        this.html = OrganogramaBusiness.abrirTagFilho(this.html, "Human Resources");
        this.html = OrganogramaBusiness.fecharTagFilho(this.html);
        this.html = OrganogramaBusiness.abrirTagFilho(this.html, "Finance");
        this.html = OrganogramaBusiness.fecharTagFilho(this.html);
        this.html = OrganogramaBusiness.abrirTagFilho(this.html, "Marketing");
        this.html = OrganogramaBusiness.fecharTagFilho(this.html);
        this.html = OrganogramaBusiness.abrirTagFilho(this.html, "IT/IS");
        this.html = OrganogramaBusiness.fecharTagFilho(this.html);
        this.html = OrganogramaBusiness.abrirTagFilho(this.html, "Service Delivery");
        this.html = OrganogramaBusiness.fecharTagFilho(this.html);
        this.html = OrganogramaBusiness.fecharNoVertical(this.html);
        this.html = OrganogramaBusiness.fecharTagItem(this.html);
        this.html = OrganogramaBusiness.fecharTagItem(this.html);

        StringBuilder expected = new StringBuilder();
        expected.append("<ul><li><a href=\"#\">Ninja Board</a><ul><li><a href=\"#\">Staff</a><ul class=\"vertical\">")
                .append("<li><a href=\"#\">Human Resources</a></li><li><a href=\"#\">Finance</a></li><li><a href=\"#\">Marketing</a></li>")
                .append("<li><a href=\"#\">IT/IS</a></li><li><a href=\"#\">Service Delivery</a></li></ul></li></ul></li></ul>");

        assertEquals(expected.toString(), this.html.toString());
    }

}