package br.com.juridico.htmlfactory;

import br.com.juridico.dto.ParteRelatorioDto;
import br.com.juridico.dto.PedidoRelatorioDto;
import com.google.common.base.Strings;

import java.util.List;

/**
 * Created by jefferson.ferreira on 26/10/2017.
 */
public class HtmlFactory {

    private StringBuilder htmlBuilder;

    public HtmlFactory(String titulo, boolean landscape, String impressoPor) {
        this.htmlBuilder = new StringBuilder();
        this.htmlBuilder.append("<?xml version='1.0' encoding='UTF-8'?>\n")
                .append("<!DOCTYPE html>\n")
                .append("<HTML><HEAD><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">")
                .append("<style>");
        if (landscape){
            this.htmlBuilder.append("@page { size: A4 landscape;}");
        }
        this.htmlBuilder.append("table{table-layout:fixed; padding:3px; width:100%;} ")
                .append("td, th, tfoot{border:solid 1px #f0f1f4;}")
                .append("th {background-color:#f0f1f4; font-size: 8px;} ")
                .append("td {font-size: 7px;} caption {font-size:x-large;} ")
                .append("hr {border-width: 0.3px;} ")
                .append("H4 {text-align: left} ")
                .append(".fieldsetClass {font-size: 7px;} ")
                .append(".topright {position: absolute;top: 8px;right: 16px;font-size: 8px;}")
                .append("</style>")
                .append("</HEAD>")
                .append("<BODY>")
                .append("<div class=\"topright\">Impresso por: ").append(impressoPor).append("</div><br/>")
                .append("<CENTER>")
                .append("<IMG style=\"height:150px; width:70%;\" SRC=\"https://gallery.mailchimp.com/d8a1664d58a9274ab6f7370ac/images/93a0e420-8ee5-4bc3-8c29-57ed6ac17034.png\" ALIGN=\"BOTTOM\">")
                .append("</CENTER>")
                .append("<br/>")
                .append("<CENTER><HR>").append(titulo).append("<HR>")
                .append("<br/>")
        ;
        finalizaHtml();
    }

    public void addDadosProcesso(List<String> headers, List<String> dados){
        this.htmlBuilder.append("<H4>Dados do Processo</H4><table><tr>");
        createHeaders(headers);
        this.htmlBuilder.append("</tr><tr>");
        for (String dado : dados) {
            this.htmlBuilder.append("<td>").append(dado).append("</td>");
        }
        this.htmlBuilder.append("</tr>").append("</table><HR>");
    }

    public void addPartes(List<String> headers, List<ParteRelatorioDto> partes){
        this.htmlBuilder.append("<H4>Partes do Processo</H4><table><tr>");

        if(!headers.isEmpty() && (headers.contains("Cliente") || headers.contains("Adverso"))){
            this.htmlBuilder.append("<th>").append("Nome").append("</th>");
            this.htmlBuilder.append("<th>").append("Cliente?").append("</th>");
        }
        if(headers.contains("Advogado")){
            this.htmlBuilder.append("<th>").append("Advogado(s)").append("</th>");
        }
        this.htmlBuilder.append("</tr>");

        for (ParteRelatorioDto parte : partes) {
            this.htmlBuilder.append("<tr>");
            this.htmlBuilder.append("<td>").append(Strings.isNullOrEmpty(parte.getNome()) ? "" : parte.getNome().concat(" - ").concat(parte.getPosicao())).append("</td>");
            this.htmlBuilder.append("<td>").append(Strings.isNullOrEmpty(parte.getCliente()) ? "" : parte.getCliente()).append("</td>");
            this.htmlBuilder.append("<td>").append(Strings.isNullOrEmpty(parte.getAdvogados()) ? "" : parte.getAdvogados()).append("</td>");
            this.htmlBuilder.append("</tr>");
        }
        this.htmlBuilder.append("</table><HR>");
    }

    public void addResumo(String resumoAcao){
        this.htmlBuilder.append("<H4>Resumo da Ação</H4>");
        this.htmlBuilder.append("<div class=\"fieldsetClass\"><fieldset align:justify;>").append(resumoAcao).append("</fieldset></div><HR>");
    }

    public void addValores(List<String> headers, List<String> dados){
        this.htmlBuilder.append("<H4>Valores</H4><table><tr>");
        createHeaders(headers);
        this.htmlBuilder.append("</tr><tr>");
        for (String dado : dados) {
            this.htmlBuilder.append("<td>").append(dado).append("</td>");
        }
        this.htmlBuilder.append("</tr>").append("</table>");
    }

    public void addPedidos(List<String> headers, List<PedidoRelatorioDto> pedidos){
        this.htmlBuilder.append("<H4>Pedidos</H4><table><tr>");
        createHeaders(headers);
        this.htmlBuilder.append("</tr>");
        for (PedidoRelatorioDto pedido : pedidos) {
            this.htmlBuilder.append("<tr>");
            this.htmlBuilder.append("<td>").append(Strings.isNullOrEmpty(pedido.getPedido()) ? "" : pedido.getPedido()).append("</td>");
            this.htmlBuilder.append("<td>").append(Strings.isNullOrEmpty(pedido.getCausaPedido()) ? "" : pedido.getPedido()).append("</td>");
            this.htmlBuilder.append("<td>").append(Strings.isNullOrEmpty(pedido.getValorPedido()) ? "" : pedido.getPedido()).append("</td>");
            this.htmlBuilder.append("<td>").append(Strings.isNullOrEmpty(pedido.getValorProvavel()) ? "" : pedido.getPedido()).append("</td>");
            this.htmlBuilder.append("</tr>");
        }
        this.htmlBuilder.append("</table><HR>");
    }

    private void createHeaders(List<String> headers) {
        for (String header : headers) {
            this.htmlBuilder.append("<th>").append(header).append("</th>");
        }
    }

    public StringBuilder getHtml(){
        return htmlBuilder;
    }

    private void finalizaHtml() {
        this.htmlBuilder.append("</BODY></HTML>");
    }

}
