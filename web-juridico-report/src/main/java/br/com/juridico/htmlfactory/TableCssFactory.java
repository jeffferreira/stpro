package br.com.juridico.htmlfactory;

/**
 * Created by jefferson.ferreira on 31/10/2017.
 */
public class TableCssFactory {

    private StringBuilder css;

    public TableCssFactory(){
        this.css = new StringBuilder();
    }

    public StringBuilder tableCss(){
        this.css.append("table.blueTable {")
                .append("    border: 1px solid #1C6EA4;")
                .append("    background-color: #EEEEEE;")
                .append("    width: 100%;")
                .append("    text-align: left;")
                .append("    border-collapse: collapse;")
                .append("}")
                .append("table.blueTable td, table.blueTable th {")
                .append("    border: 1px solid #AAAAAA;")
                .append("    padding: 3px 2px;")
                .append("}")
                .append("table.blueTable tbody td {")
                .append("    font-size: 9px;")
                .append("}")
                .append("table.blueTable tr:nth-child(even) {")
                .append("    background: #D0E4F5;")
                .append("}")
                .append("table.blueTable thead {")
                .append("    background: #1C6EA4;")
                .append("    background: -moz-linear-gradient(top, #5592bb 0%, #327cad 66%, #1C6EA4 100%);")
                .append("    background: -webkit-linear-gradient(top, #5592bb 0%, #327cad 66%, #1C6EA4 100%);")
                .append("    background: linear-gradient(to bottom, #5592bb 0%, #327cad 66%, #1C6EA4 100%);")
                .append("    border-bottom: 2px solid #444444;")
                .append("}")
                .append("table.blueTable thead th {")
                .append("    font-size: 10px;")
                .append("    font-weight: bold;")
                .append("color: #FFFFFF;background-color:#3a81b0;")
                .append("}")
                .append("table.blueTable thead th:first-child {")
                .append("    border-left: none;")
                .append("}")
                .append("table.blueTable tfoot {")
                .append("    font-size: 10px;")
                .append("    font-weight: bold;")
                .append("    color: #FFFFFF;")
                .append("    background: #D0E4F5;")
                .append("    background: -moz-linear-gradient(top, #dcebf7 0%, #d4e6f6 66%, #D0E4F5 100%);")
                .append("    background: -webkit-linear-gradient(top, #dcebf7 0%, #d4e6f6 66%, #D0E4F5 100%);")
                .append("    background: linear-gradient(to bottom, #dcebf7 0%, #d4e6f6 66%, #D0E4F5 100%);")
                .append("    border-top: 2px solid #444444;")
                .append("}")
                .append("table.blueTable tfoot td {")
                .append("    font-size: 10px;")
                .append("}");

        return this.css;
    }


}
