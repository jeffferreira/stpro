package br.com.juridico.util;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by jefferson.ferreira on 26/10/2017.
 */
public class FileReportUtil {

    private static final DateFormat FORMATADOR_DATA_HORA_BRASIL = new SimpleDateFormat("dd/MM/yyyy HH:mm");

    private FileReportUtil(){}

    /**
     *
     * @param file
     * @return
     * @throws IOException
     */
    public static byte[] getContent(File file) throws IOException {
        ByteArrayOutputStream ous = null;
        InputStream fis = null;
        try {
            byte[] content = new byte[(int) file.length()];
            ous = new ByteArrayOutputStream();
            fis = new FileInputStream(file);
            int read = 0;
            while ((read = fis.read(content)) != -1) {
                ous.write(content, 0, read);
            }
        } finally {
            if (ous != null)
                ous.close();
            if (fis != null)
                fis.close();

        }
        return ous.toByteArray();
    }

}
