package br.com.juridico.builder;

import br.com.juridico.util.FileReportUtil;
import br.com.juridico.util.HtmlToPdfUtil;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by jefferson.ferreira on 26/10/2017.
 */
public class SimpleHtmlPdfReportBuilder {

    private static final DateFormat FORMATADOR_DATA_HORA_BRASIL = new SimpleDateFormat("dd/MM/yyyy HH:mm");

    private final String nomeRelatorio;
    private final StringBuilder html;

    public SimpleHtmlPdfReportBuilder(String nomeRelatorio, StringBuilder html){
        this.nomeRelatorio = nomeRelatorio;
        this.html = html;
    }

    /**
     *
     * @return
     * @throws Exception
     */
    public byte[] build() throws Exception {
        String data = FORMATADOR_DATA_HORA_BRASIL.format(new Date());
        File htmlTemporario = File.createTempFile(this.nomeRelatorio+data,".tmp");
        htmlTemporario.deleteOnExit();

        BufferedWriter writer = new BufferedWriter(new FileWriter(htmlTemporario));
        writer.write(this.html.toString());
        writer.flush();
        writer.close();

        String text = "";
        FileReader in = new FileReader(htmlTemporario);
        BufferedReader buffer = new BufferedReader(in);
        while(buffer.ready()) {
            text += buffer.readLine();
        }

        File tempFile = File.createTempFile(this.nomeRelatorio+data,".pdf");
        OutputStream os = new FileOutputStream(tempFile);
        HtmlToPdfUtil.convert(text, os);
        os.close();
        return FileReportUtil.getContent(tempFile);
    }


}
