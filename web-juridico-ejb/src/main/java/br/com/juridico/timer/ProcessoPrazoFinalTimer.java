package br.com.juridico.timer;

import br.com.juridico.business.EmailTemplateBusiness;
import br.com.juridico.business.ParametroEmailBusiness;
import br.com.juridico.business.PrazoFinalEmailBusiness;
import br.com.juridico.entidades.*;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.impl.AndamentoSessionBean;
import br.com.juridico.impl.EmailCaixaSaidaSessionBean;
import br.com.juridico.impl.EmailSessionBean;
import br.com.juridico.util.DateUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.log4j.Logger;

import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

/**
 * Created by jeffersonl2009_00 on 06/12/2016.
 */
@Singleton
public class ProcessoPrazoFinalTimer {

    private static final Logger LOGGER = Logger.getLogger(ProcessoPrazoFinalTimer.class);
    private static final String PROCESSO_PRAZO_FINAL = "PROCESSO_PRAZO_FINAL";

    @EJB
    private AndamentoSessionBean andamentoSessionBean;
    @EJB
    private EmailSessionBean emailSessionBean;
    @EJB
    private EmailCaixaSaidaSessionBean emailCaixaSaidaSessionBean;

    @Schedule(second = "0", minute = "10", hour = "6")
    public void execute() throws InterruptedException {
        try {
            LOGGER.info("EXECUTANDO >>>> ProcessoPrazoFinalTimer"+ DateUtils.formataDataBrasil(Calendar.getInstance().getTime()));
            andamentoSessionBean.clearSession();
            List<BigInteger> andamentoIds = andamentoSessionBean.findByAndamentosAtingiramDiasAviso();
            List<Andamento> andamentos = Lists.newArrayList();
            for (BigInteger id : andamentoIds) {
                Andamento andamento = andamentoSessionBean.retrieve(id.longValue());
                andamentos.add(andamento);
            }

            emailSessionBean.clearSession();
            enviarEmailPrazoFinal(andamentos);

        } catch (Exception e) {
            LOGGER.error(e, e.getCause());
        }
    }

    private void enviarEmailPrazoFinal(List<Andamento> andamentos) throws ValidationException {
        for (Andamento andamento : andamentos) {
            Email email = emailSessionBean.findBySigla(PROCESSO_PRAZO_FINAL, andamento.getCodigoEmpresa());

            if(email == null || (email != null && !email.getAtivo())) {
                continue;
            }

            Pessoa pessoa = andamento.getPessoaFisica().getPessoa();
            String enderecoEmail = !pessoa.getPessoasEmails().isEmpty() ? pessoa.getPessoasEmails().get(0).getEmail() : "";
            Map<String, String> parameters = carregaParametrosEmail(andamento, pessoa, email);
            String textoEmail = EmailTemplateBusiness.criarEmailTemplate(email.getAssunto(), email.getDescricao(), parameters);
            EmailCaixaSaida emailCaixaSaida = new EmailCaixaSaida(enderecoEmail, email.getAssunto(), textoEmail, pessoa.getCodigoEmpresa());
            addDestinatarios(emailCaixaSaida, pessoa);

            if(email.getNecessarioAprovacao()) {
                emailCaixaSaidaSessionBean.create(emailCaixaSaida);
                LOGGER.info("EMAIL PROCESSO_PRAZO_FINAL ENVIADO PARA CAIXA SAIDA >>>> "+ DateUtils.formataDataBrasil(Calendar.getInstance().getTime()));
                continue;
            }

            emailCaixaSaidaSessionBean.createSemAprovacao(emailCaixaSaida);
            LOGGER.info("EMAIL PROCESSO_PRAZO_FINAL ENVIADO >>>> "+ DateUtils.formataDataBrasil(Calendar.getInstance().getTime()));
        }
    }

    private void addDestinatarios(EmailCaixaSaida caixaSaida, Pessoa pessoa) {
        for (PessoaEmail pessoaEmail : pessoa.getPessoasEmails()) {
            EmailDestinatario destinatario = new EmailDestinatario(caixaSaida, pessoaEmail.getEmail());
            caixaSaida.addEmails(destinatario);
        }
    }

    private  Map<String, String> carregaParametrosEmail(Andamento andamento, Pessoa pessoa, Email email) {
        Map<String, String> parametersValues = Maps.newHashMap();
        for (EmailVinculoParametro parametro : email.getParametroEmails()) {
            User user = new User(pessoa.getId(), pessoa.getDescricao());
            String valor = PrazoFinalEmailBusiness.execute(parametro.getParametroEmail(), andamento, user);
            parametersValues.put(parametro.getParametroEmail().getSigla(), valor);
        }
        return parametersValues;
    }
}
