package br.com.juridico.timer;

import br.com.juridico.business.EmailTemplateBusiness;
import br.com.juridico.business.ParametroEmailBusiness;
import br.com.juridico.business.PrazoVencidoEmailBusiness;
import br.com.juridico.entidades.*;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.impl.AndamentoSessionBean;
import br.com.juridico.impl.EmailCaixaSaidaSessionBean;
import br.com.juridico.impl.EmailSessionBean;
import br.com.juridico.util.DateUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.log4j.Logger;

import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

/**
 * Created by marcos on 04/02/2017.
 */
@Singleton
public class PrazoVencidoTimer {

    private static final Logger LOGGER = Logger.getLogger(PrazoVencidoTimer.class);
    private static final String PRAZO_ANDAMENTO_VENCIDO = "PRAZO_ANDAMENTO_VENCIDO";

    @EJB
    private AndamentoSessionBean andamentoSessionBean;
    @EJB
    private EmailSessionBean emailSessionBean;
    @EJB
    private EmailCaixaSaidaSessionBean emailCaixaSaidaSessionBean;

    @Schedule(second = "00", minute = "15", hour = "06")
    public void execute() throws InterruptedException {
        try {
            LOGGER.info("EXECUTANDO >>>> PrazoVencidoTimer"+ DateUtils.formataDataBrasil(Calendar.getInstance().getTime()));
            andamentoSessionBean.clearSession();
            List<BigInteger> andamentoIds = andamentoSessionBean.findByAndamentosPrazoVencido();
            List<Andamento> andamentos = Lists.newArrayList();
            for (BigInteger id : andamentoIds) {
                Andamento andamento = andamentoSessionBean.retrieve(id.longValue());
                andamentos.add(andamento);
            }

            emailSessionBean.clearSession();
            enviarEmailPrazoVencido(andamentos);

        } catch (Exception e) {
            LOGGER.error(e, e.getCause());
        }
    }

    private void enviarEmailPrazoVencido(List<Andamento> andamentos) throws ValidationException {
        for (Andamento andamento : andamentos) {
            Email email = emailSessionBean.findBySigla(PRAZO_ANDAMENTO_VENCIDO, andamento.getCodigoEmpresa());

            if(email == null || (email != null && !email.getAtivo())) {
                continue;
            }

            Pessoa pessoa = andamento.getPessoaFisica().getPessoa();

            String enderecoEmail = getEnderecoEmail(pessoa);
            Map<String, String> parameters = carregaParametrosEmail(andamento, pessoa, email);
            String textoEmail = EmailTemplateBusiness.criarEmailTemplate(email.getAssunto(), email.getDescricao(), parameters);

            EmailCaixaSaida emailCaixaSaida = new EmailCaixaSaida(enderecoEmail, email.getAssunto(), textoEmail, pessoa.getCodigoEmpresa());
            addDestinatarios(emailCaixaSaida, pessoa);

            if(email.getNecessarioAprovacao()) {
                emailCaixaSaidaSessionBean.create(emailCaixaSaida);
                LOGGER.info("EMAIL PRAZO_ANDAMENTO_VENCIDO ENVIADO PARA CAIXA SAIDA >>>> "+ DateUtils.formataDataBrasil(Calendar.getInstance().getTime()));
                continue;
            }

            emailCaixaSaidaSessionBean.createSemAprovacao(emailCaixaSaida);
            LOGGER.info("EMAIL PRAZO_ANDAMENTO_VENCIDO ENVIADO >>>> "+ DateUtils.formataDataBrasil(Calendar.getInstance().getTime()));
        }
    }

    private String getEnderecoEmail(Pessoa pessoa) {
        if(pessoa.getDataExclusao() == null && !pessoa.getPessoasEmails().isEmpty()){
            return pessoa.getPessoasEmails().get(0).getEmail();
        }
        return "";
    }

    private void addDestinatarios(EmailCaixaSaida caixaSaida, Pessoa pessoa) {
        for (PessoaEmail pessoaEmail : pessoa.getPessoasEmails()) {
            EmailDestinatario destinatario = new EmailDestinatario(caixaSaida, pessoaEmail.getEmail());
            caixaSaida.addEmails(destinatario);
        }
    }

    private  Map<String, String> carregaParametrosEmail(Andamento andamento, Pessoa pessoa, Email email) {
        Map<String, String> parametersValues = Maps.newHashMap();
        for (EmailVinculoParametro parametro : email.getParametroEmails()) {
            User user = new User(pessoa.getId(), pessoa.getDescricao());
            String valor = PrazoVencidoEmailBusiness.execute(parametro.getParametroEmail(), andamento, user);
            parametersValues.put(parametro.getParametroEmail().getSigla(), valor);
        }
        return parametersValues;
    }
}
