package br.com.juridico.enums;

/**
 * Created by jefferson.ferreira on 03/02/2017.
 */
public enum UfType {

    ac,al,am,ap,ba,ce,df,es,go,ma,mg,ms,mt,pa,pb,pe,pi,pr,rj,rn,ro,rr,rs,sc,se,sp,to;

}
