package br.com.juridico.enums;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jefferson.ferreira on 03/02/2017.
 */
public enum StatusContasPagarType {

    TO("TO", "Todos"),

    CA("CA", "Cadastrado"),

    RJ("RJ", "Rejeitado"),

    DE("DE", "Descartado"),

    AP("AP", "Aprovado"),

    AR("AR", "Aguardando Recibo Pagamento"),

    PP("PP", "Parcela Paga"),

    PG("PG", "Pago");

    private final String value;

    private final String descricao;

    StatusContasPagarType(String value, String descricao) {
        this.value = value;
        this.descricao = descricao;
    }

    public String getValue() {
        return value;
    }

    public String getDescricao() {
        return descricao;
    }

    public static StatusContasPagarType fromClass(StatusContasPagarType clazz) {
        for (StatusContasPagarType tipo : values()) {
            if (tipo.descricao.equals(clazz)) {
                return tipo;
            }
        }
        return null;
    }

    public static StatusContasPagarType fromString(String clazz) {
        for (StatusContasPagarType tipo : values()) {
            if (tipo.value.equals(clazz)) {
                return tipo;
            }
        }
        return null;
    }

    public static List<StatusContasPagarType> fromList(List<String> lists) {
        List<StatusContasPagarType> listStatus = new ArrayList<>();
        for (String tipo : lists) {
            for (StatusContasPagarType st : values()) {
                if (tipo.equals(st.getValue())) {
                    listStatus.add(st);
                }
            }
        }
        return listStatus;
    }
}
