package br.com.juridico.enums;

import br.com.juridico.interfaces.Descriptable;

/**
 * Created by jefferson on 21/04/2018.
 */
public enum TipoPessoaParteControladoriaType implements Descriptable {

    PJ("PJ"),
    PF("PF");

    private final String descricao;

    TipoPessoaParteControladoriaType(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    @Override
    public String getDescription() {
        return descricao;
    }
}
