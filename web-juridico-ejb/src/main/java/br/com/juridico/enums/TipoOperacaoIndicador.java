package br.com.juridico.enums;

/**
 * Created by jefferson on 19/03/2017.
 */
public enum TipoOperacaoIndicador {

    ALT("ALTERAÇÃO"), INC("INCLUSÃO"), EXC("EXCLUSÃO"), APV("APROVADO"), RJT("REJEITADO");

    private String descricao;

    TipoOperacaoIndicador(String descricao){
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }
}
