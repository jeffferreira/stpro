package br.com.juridico.enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by jefferson.ferreira on 03/02/2017.
 */
public enum ContasPagarType {

    B("B", "Boleto", "Boleto"),

    A("A", "D\u00e9bito Autom\u00e1tico", "D\u00e9bito Autom\u00e1tico"),

    D("D", "Dep\u00f3sito", "Dep\u00f3sito");

    private final String value;

    private final String descricao;

    private final String descricaoComboBox;

    ContasPagarType(String value, String descricao, String descricaoComboBox) {
        this.value = value;
        this.descricao = descricao;
        this.descricaoComboBox = descricaoComboBox;
    }

    public String getValue() {
        return value;
    }

    public String getDescription() {
        return descricao;
    }

    public String getDescricaoComboBox() {
        return descricaoComboBox;
    }


    public static List<ContasPagarType> getTipos(){
        return Arrays.asList(values());
    }

    public static List<ContasPagarType> getTiposParaCadastroDeContasAPagar(){
        List<ContasPagarType> contasPagarType = new ArrayList<>();
        contasPagarType.add(ContasPagarType.B);
        contasPagarType.add(ContasPagarType.A);
        contasPagarType.add(ContasPagarType.D);
        return contasPagarType;
    }

    public static ContasPagarType fromString(String clazz) {
        for (ContasPagarType tipo : values()) {
            if (tipo.value.equals(clazz)) {
                return tipo;
            }
        }
        return null;
    }

    public static ContasPagarType fromStringValue(String value){
        for (ContasPagarType tipo : values()){
            if(tipo.getValue().equals(value)){
                return tipo;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return descricaoComboBox;
    }
}
