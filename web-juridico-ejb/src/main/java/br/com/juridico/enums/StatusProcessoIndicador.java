package br.com.juridico.enums;

import com.google.common.base.Strings;

public enum StatusProcessoIndicador {

    ATIVO("A", "ATIVO"), SUSPENSO("S", "SUSPENSO"), FINALIZADO("F", "FINALIZADO");

    private String value;

    private String descricao;

    StatusProcessoIndicador(String value, String descricao) {
        this.value = value;
        this.descricao = descricao;
    }

    public String getValue() {
        return this.value;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public static StatusProcessoIndicador fromString(String status){
        for (StatusProcessoIndicador indicador : values()) {
            if(!Strings.isNullOrEmpty(status) && indicador.getValue().equals(status)) {
                return indicador;
            }
        }
        return null;
    }
}
