package br.com.juridico.enums;

public enum TipoComprovanteType {

	CONVENIO("Convênio"),
	TITULOS("Títulos"),
	TRANSFERENCIA("Transferência"),
	OUTROS("Outros");

	private final String description;

	TipoComprovanteType(String description) {
		this.description = description;
	}


	public String getDescription() {
		return description;
	}
}
