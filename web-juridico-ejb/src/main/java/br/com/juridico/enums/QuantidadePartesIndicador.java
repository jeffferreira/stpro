package br.com.juridico.enums;

public enum QuantidadePartesIndicador {

    UM(1), DOIS(2), TRES(3), QUATRO(4), CINCO(5);

    private Integer value;

    private QuantidadePartesIndicador(int value) {
        this.value = value;
    }

    public Integer getValue() {
        return this.value;
    }

}
