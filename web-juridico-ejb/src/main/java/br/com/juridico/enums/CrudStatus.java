package br.com.juridico.enums;

import br.com.juridico.interfaces.Descriptable;

public enum CrudStatus implements Descriptable {

    CREATE("create"), READ("read"), UPDATE("update"), DELETE("delete");
	
	private String descricao;

    CrudStatus(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public String getDescription() {
        return descricao;
    }

}
