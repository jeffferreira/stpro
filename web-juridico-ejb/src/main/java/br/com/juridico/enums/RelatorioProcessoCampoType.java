package br.com.juridico.enums;


public enum RelatorioProcessoCampoType {

    PROCESSO("processo", "N. Processo", "text", "processo"),
    DATA_CADASTRO("dataCadastro", "Dt. Cadastro", "periodoData", "dataCadastro"),
    DATA_ABERTURA("dataAbertura", "Dt. Abertura", "periodoData", "dataAbertura"),
    CLIENTE("clientesText", "Cliente", "text", "clientesText"),
    ADVERSO("adversosText", "Adverso", "text", "adversosText"),
    ADVOGADO("advogadosText", "Advogado", "text", "advogadosText"),
    MEIO_TRAMITACAO("meioTramitacao", "Meio de tramitação", "text", "meioTramitacao"),
    CENTRO_CUSTO("centroCusto", "Centro de custo", "text", "centroCusto"),
    NATUREZA_ACAO("naturezaAcao", "Natureza da ação", "text", "naturezaAcao"),
    CLASSE_PROCESSUAL("classeProcessual", "Classe processual", "text", "classeProcessual"),
    FASE_PROCESSO("fase", "Fase", "text", "fase"),
    FORUM("forum", "Fórum", "text", "forum"),
    COMARCA("comarca", "Comarca", "text", "comarca"),
    VARA("vara", "Vara", "text", "vara"),
    RESUMO_PROCESSO("resumo", "Resumo da ação", "text", "resumo"),
    PEDIDO("pedidosText", "Pedido", "text", "pedidosText"),
    CAUSA_PEDIDO("causasPedidoText", "Causa do pedido", "text", "causasPedidoText"),
    GRAU_RISCO("grauRisco", "Grau de risco", "text", "grauRisco"),
    VALOR_PEDIDO("valorPedido", "Valor do pedido", "periodoValor", "valorPedido"),
    VALOR_PROVAVEL("valorProvavel", "Valor provável", "periodoValor", "valorProvavel"),
    VALOR_CAUSA("valorCausa", "Valor da causa", "periodoValor", "valorCausa"),
    VALOR_TOTAL_PROVAVEL("valorTotalProvavel", "Valor total provável", "periodoValor", "valorTotalProvavel"),
    VALOR_TOTAL_PEDIDO("valorTotalPedido", "Valor total pedido", "periodoValor", "valorTotalPedido");

    private final String valorParaQuery;

    private final String descricao;

    private final String tipoCampo;

    private final String nomeCampoView;

    RelatorioProcessoCampoType(String valorParaQuery, String descricao, String tipoCampo, String nomeCampoView) {
        this.valorParaQuery = valorParaQuery;
        this.descricao = descricao;
        this.tipoCampo = tipoCampo;
        this.nomeCampoView = nomeCampoView;
    }

    public static RelatorioProcessoCampoType[] getDadosProcesso(){
        return new RelatorioProcessoCampoType[]{
                PROCESSO, DATA_CADASTRO, DATA_ABERTURA,
                MEIO_TRAMITACAO, NATUREZA_ACAO, CLASSE_PROCESSUAL,
                FASE_PROCESSO, FORUM, COMARCA, VARA
        };
    }

    public static RelatorioProcessoCampoType[] getPartesProcesso(){
        return new RelatorioProcessoCampoType[]{
                CLIENTE, ADVERSO, ADVOGADO
        };
    }

    public static RelatorioProcessoCampoType[] getResumoProcesso(){
        return new RelatorioProcessoCampoType[]{RESUMO_PROCESSO};
    }

    public static RelatorioProcessoCampoType[] getValoresProcesso(){
        return new RelatorioProcessoCampoType[]{
                GRAU_RISCO, VALOR_CAUSA,
                VALOR_TOTAL_PROVAVEL, VALOR_TOTAL_PEDIDO
        };
    }

    public static RelatorioProcessoCampoType[] getPedidosProcesso(){
        return new RelatorioProcessoCampoType[]{
                PEDIDO, CAUSA_PEDIDO,
                VALOR_PEDIDO,
                VALOR_PROVAVEL
        };
    }

    public static  boolean isDadosProcesso(String nomeCampo) {
        return isCampoPertenceAoGrupo(getDadosProcesso(), nomeCampo);
    }

    public static  boolean isPartesProcesso(String nomeCampo) {
        return isCampoPertenceAoGrupo(getPartesProcesso(), nomeCampo);
    }

    public static  boolean isResumoProcesso(String nomeCampo) {
        return isCampoPertenceAoGrupo(getResumoProcesso(), nomeCampo);
    }

    public static  boolean isValoresProcesso(String nomeCampo) {
        return isCampoPertenceAoGrupo(getValoresProcesso(), nomeCampo);
    }

    public static  boolean isPedidosProcesso(String nomeCampo) {
        return isCampoPertenceAoGrupo(getValoresProcesso(), nomeCampo);
    }

    private static boolean isCampoPertenceAoGrupo(RelatorioProcessoCampoType[] group, String nomeCampo) {
        boolean result = Boolean.FALSE;
        for (RelatorioProcessoCampoType tipo : group) {
            if (tipo.getNomeCampoView().equals(nomeCampo)) {
                result = Boolean.TRUE;
                break;
            }
        }
        return result;
    }

    public String getValorParaQuery() {
        return valorParaQuery;
    }

    public String getDescricao() {
        return descricao;
    }

    public String getTipoCampo() {
        return tipoCampo;
    }

    public String getNomeCampoView() {
        return nomeCampoView;
    }
}
