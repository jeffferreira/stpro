package br.com.juridico.enums;

/**
 * Created by jefferson on 19/03/2017.
 */
public enum TipoRetornoRoboIndicador {

    SUCESSO("SUCESSO"), ERRO("ERRO");

    private String descricao;

    TipoRetornoRoboIndicador(String descricao){
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }
}
