package br.com.juridico.enums;

/**
 * Created by Jefferson on 30/07/2016.
 */
public enum MenuIndicador {

    AGENDA(1L, "MENU_AGENDA"),
    ADVOGADOS(2L, "MENU_ADVOGADOS"),
    ATENDIMENTO(3L, "MENU_ATENDIMENTO"),
    CADASTROS(4L, "MENU_CADASTROS"),
    PESSOAS(5L, "MENU_PESSOAS"),
    PROCESSOS(6L, "MENU_PROCESSOS"),
    RELATORIOS(7L, "MENU_RELATORIOS"),
    ACESSOS(8L, "MENU_ACESSOS"),
    HOME(9L, "MENU_HOME"),
    EMAILS(10L, "MENU_EMAILS"),
    DASHBOARDS(11L, "DASHBOARDS"),
    CONTROLADORIA(12L, "MENU_CONTROLADORIA"),
    CONTRATOS(13L, "MENU_CONTRATOS"),
    FINANCEIRO(14L, "MENU_FINANCEIRO");

    private Long id;

    private String descricao;

    MenuIndicador(Long id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    public Long getValue() {
        return this.id;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public static MenuIndicador findByDescricao(String value) {
        for (MenuIndicador menuIndicador : values()) {
            if(menuIndicador.getDescricao().equals(value)){
                return menuIndicador;
            }
        }
        return null;
    }
}
