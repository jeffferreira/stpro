package br.com.juridico.enums;


public enum RelatorioCriterioType {

    E("E", "e", "AND"),
    OU("OU", "ou", "OR");

    private final String value;

    private final String descricao;

    private final String comando;

    RelatorioCriterioType(String value, String descricao, String comando) {
        this.value = value;
        this.descricao = descricao;
        this.comando = comando;
    }

    public String getValue() {
        return value;
    }

    public String getDescricao() {
        return descricao;
    }

    public String getComando() {
        return comando;
    }

    @Override
    public String toString() {
        return this.descricao;
    }
}
