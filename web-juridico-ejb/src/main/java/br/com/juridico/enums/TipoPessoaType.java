package br.com.juridico.enums;

import br.com.juridico.interfaces.Descriptable;

/**
 * Created by jefferson.ferreira on 03/02/2017.
 */
public enum TipoPessoaType implements Descriptable {

    F("Pessoa Física"),

    J("Pessoa Jurídica");


    private final String descricao;

    TipoPessoaType(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public String getDescription() {
        return descricao;
    }

    public String getValue(){
        return name();
    }

    @Override
    public String toString() {
        return "F";
    }
}
