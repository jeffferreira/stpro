package br.com.juridico.enums;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 * Created by Jefferson on 24/08/2016.
 */
public enum EventoColorTypes {
	
	GAINSBORO("gainsboro", "event-style-gainsboro", "event-text-color-gainsboro"),
	SLATEBLUE("slateBlue", "event-style-slateBlue", "event-text-color-slateBlue"),
	MEDIUMSEAGREEN("mediumSeaGreen", "event-style-mediumSeaGreen,", "event-text-color-mediumSeaGreen"),
	MEDIUMTORQUOISE("mediumTurquoise", "event-style-mediumTurquoise", "event-text-color-mediumTurquoise"),
	LIGHTSLATEGRAY("lightSlateGray", "event-style-lightSlateGray", "event-text-color-lightSlateGray"),
	PALEGREEN("paleGreen", "event-style-paleGreen", "event-text-color-paleGreen"),
	DARKGOLDENROD("darkGoldenrod", "event-style-darkGoldenrod", "event-text-color-darkGoldenrod"),
	FIREBRICK("firebrick", "event-style-firebrick", "event-text-color-firebrick"),
	INDIANRED2("indianRed2", "event-style-indianRed2", "event-text-color-indianRed2"),
	CHOCOLATE2("chocolate2", "event-style-chocolate2", "event-text-color-chocolate2"),
	DARKMAGENTA("darkMagenta", "event-style-darkMagenta", "event-text-color-darkMagenta"),
    GREEN("green", "event-style-green", "event-text-color-green"),
    BLUE("blue", "event-style-blue", "event-text-color-blue"),
    PURPLE("purple", "event-style-purple", "event-text-color-purple"),
    YELLOW("yellow", "event-style-yellow", "event-text-color-yellow"),
    ORANGE("orange", "event-style-orange", "event-text-color-orange"),
    RED("red", "event-style-red", "event-text-color-red"),
	BLACK("black", "event-style-black", "event-text-color-black");
	
	private static final List<EventoColorTypes> VALUES = Collections.unmodifiableList(Arrays.asList(values()));
	private static final int SIZE = VALUES.size();
	private static final Random RANDOM = new Random();

	private final String componentId;
		
    private final String value;
    
    private final String textColor;

    EventoColorTypes(String componentId, String value, String textColor) {
    	this.componentId = componentId;
        this.value = value;
        this.textColor = textColor;
    }

    public String getValue() {
        return value;
    }

	public String getTextColor() {
		return textColor;
	}
	
	public String getComponentId() {
		return componentId;
	}
	
	public static EventoColorTypes randomEventoColorTypes(Set<EventoColorTypes> types){
		for (int i = 0; i <= VALUES.size(); i++) {
			EventoColorTypes colorTypes = VALUES.get(RANDOM.nextInt(SIZE));
			if(types.add(colorTypes)) {
				return colorTypes;
			}
		}
		return RED;
	}

	public static EventoColorTypes fromComponentId(String id) {
		for (EventoColorTypes type : VALUES) {
			if (type.componentId.equals(id)) {
				return type;
			}
		}
		return null;
	}
	
	public static EventoColorTypes fromTextColor(String textColor) {
		for (EventoColorTypes type : VALUES) {
			if (type.textColor.equals(textColor)) {
				return type;
			}
		}
		return null;
	}

}
