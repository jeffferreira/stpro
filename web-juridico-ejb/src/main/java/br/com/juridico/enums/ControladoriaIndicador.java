package br.com.juridico.enums;

/**
 * Created by jefferson.ferreira on 03/02/2017.
 */
public enum ControladoriaIndicador {

    APROVADO("A"),
    REJEITADO("R"),
    PENDENTE("P"),
    NAO_SE_APLICA("N");

    private String status;

    ControladoriaIndicador(String status){
        this.status = status;
    }

    public static ControladoriaIndicador fromValue(String value) {
        if (value != null) {
            for (ControladoriaIndicador indicador : values()) {
                if (indicador.status.equals(value)) {
                    return indicador;
                }
            }
        }
        return null;
    }

    public String getStatus() {
        return status;
    }
}
