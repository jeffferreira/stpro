package br.com.juridico.enums;

/**
 * Created by Jefferson on 07/08/2016.
 */
public enum StatusEmailCaixaSaidaIndicador {

    PENDENTE("P", "PENDENTE"), APROVADO("A", "APROVADO"), REJEITADO("R", "REJEITADO");

    private String value;

    private String descricao;

    StatusEmailCaixaSaidaIndicador(String value, String descricao) {
        this.value = value;
        this.descricao = descricao;
    }

    public String getValue() {
        return this.value;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public Character getCharValue(){
        return value.charAt(0);
    }
}
