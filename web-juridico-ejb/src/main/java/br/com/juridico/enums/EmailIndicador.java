package br.com.juridico.enums;

/**
 * Created by Jefferson on 07/08/2016.
 */
public enum EmailIndicador {

    PROCESSO_PRAZO_FINAL("PROCESSO_PRAZO_FINAL"),
    PROCESSO_RESUMO_PROCESSO("PROCESSO_RESUMO_PROCESSO"),
    SENHA_ALTERACAO_SENHA("SENHA_ALTERACAO_SENHA"),
    SENHA_NOVO_USUARIO("SENHA_NOVO_USUARIO"),
    AGENDA_ALTERACAO_AGENDA("AGENDA_ALTERACAO_AGENDA"),
    AGENDA_ADICIONAR_EVENTO("AGENDA_ADICIONAR_EVENTO"),
    AGENDA_TRANSFERIR_TAREFA("AGENDA_TRANSFERIR_TAREFA"),
    AGENDA_ALTERACAO_STATUS("AGENDA_ALTERACAO_STATUS"),
    AGENDA_ALTERACAO_PRAZO("AGENDA_ALTERACAO_PRAZO");

    private String value;

    EmailIndicador(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

}
