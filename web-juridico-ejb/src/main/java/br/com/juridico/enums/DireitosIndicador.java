package br.com.juridico.enums;

/**
 * Created by jeffersonl2009_00 on 28/09/2016.
 */
public enum DireitosIndicador {

    ALT(1L, "ALT"), CON(2L, "CON"), INC(3L, "INC"), EXC(4L, "EXC");

    private final Long codigo;
    private final String sigla;

    DireitosIndicador(Long codigo, String sigla){
        this.codigo = codigo;
        this.sigla = sigla;
    }

    public String getSigla() {
        return sigla;
    }

    public Long getCodigo() {
        return codigo;
    }

    public static DireitosIndicador fromTextSigla(String sigla) {
        for (DireitosIndicador type : values()) {
            if (type.sigla.equals(sigla)) {
                return type;
            }
        }
        return null;
    }
}
