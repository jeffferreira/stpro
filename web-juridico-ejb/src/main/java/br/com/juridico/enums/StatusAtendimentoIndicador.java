package br.com.juridico.enums;

/**
 * Created by jeffersonl2009_00 on 22/06/2016.
 */
public enum StatusAtendimentoIndicador {

    PENDENTE("P", "PENDENTE"), ANDAMENTO("A", "EM ANDAMENTO"),CANCELADO("C", "CANCELADO"), FECHADO("F", "FECHADO");

    private String value;

    private String descricao;

    private StatusAtendimentoIndicador(String value, String descricao) {
        this.value = value;
        this.descricao = descricao;
    }

    public String getValue() {
        return this.value;
    }

    public String getDescricao() {
        return this.descricao;
    }
}
