package br.com.juridico.enums;

public enum TipoPessoaIndicador {

    CLIENTE("CLIENTE"), PROCESSO("PROCESSO"),
    ADVOGADO("ADVOGADO");

    private String value;

    TipoPessoaIndicador(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    public String getNome() {
        return this.name();
    }
}
