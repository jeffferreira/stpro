package br.com.juridico.enums;

public enum ProcessoIndicador {
    
    
    TODOS("TODOS"), ATIVOS("ATIVOS"),
    SUSPENSOS("SUSPENSOS"), FINALIZADOS("FINALIZADOS") ;

    private String value;

    private ProcessoIndicador(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    public String getNome() {
        return this.name();
    }
}
