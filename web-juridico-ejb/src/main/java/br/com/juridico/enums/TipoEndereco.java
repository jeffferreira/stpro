package br.com.juridico.enums;

public enum TipoEndereco {

	RESIDENCIAL("R", "Residencial"), COMERCIAL("C", "Comercial");
	
	private String value;

	private String descricao;

    TipoEndereco(String value, String descricao) {
		this.value = value;
		this.descricao = descricao;
	}

	public String getValue() {
        return this.value;
    }

    public String getDescricao() {
        return this.descricao;
    }

}
