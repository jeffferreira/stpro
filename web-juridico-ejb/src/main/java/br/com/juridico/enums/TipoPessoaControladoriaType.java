package br.com.juridico.enums;

import br.com.juridico.interfaces.Descriptable;

/**
 * Created by jefferson.ferreira on 03/02/2017.
 */
public enum TipoPessoaControladoriaType implements Descriptable {

    ADVOGADO("Responsável"),

    PARTE("Parte");

    private final String descricao;

    TipoPessoaControladoriaType(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public String getDescription() {
        return descricao;
    }

    public String getValue(){
        return name();
    }
}
