package br.com.juridico.enums;


public enum StatusParcelaContasType {


    PAGO("PG", "Pago");




    private final String value;

    private final String descricao;

    StatusParcelaContasType(String value, String descricao) {
        this.value = value;
        this.descricao = descricao;
    }

    public String getValue() {
        return value;
    }

    public String getDescricao() {
        return descricao;
    }

    public static StatusParcelaContasType fromClass(StatusContasPagarType clazz) {
        for (StatusParcelaContasType tipo : values()) {
            if (tipo.descricao.equals(clazz)) {
                return tipo;
            }
        }
        return null;
    }

    public static StatusParcelaContasType fromString(String clazz) {
        for (StatusParcelaContasType tipo : values()) {
            if (tipo.value.equals(clazz)) {
                return tipo;
            }
        }
        return null;
    }
}
