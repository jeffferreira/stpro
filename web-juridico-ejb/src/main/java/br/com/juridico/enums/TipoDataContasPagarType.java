package br.com.juridico.enums;

/**
 * Created by jefferson.ferreira on 03/02/2017.
 */
public enum TipoDataContasPagarType {

    EMISSAO("Dt. Emissão"),

    LANCAMENTO("Dt. Cad. Lançamento"),

    VENCIMENTO("Dt. Vencimento");

    private final String descricao;

    TipoDataContasPagarType(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    public static TipoDataContasPagarType fromClass(TipoDataContasPagarType clazz) {
        for (TipoDataContasPagarType tipo : values()) {
            if (tipo.descricao.equals(clazz)) {
                return tipo;
            }
        }
        return null;
    }
}
