package br.com.juridico.dto;

import br.com.juridico.entidades.PessoaJuridicaCentroCusto;

import java.math.BigDecimal;

/**
 * Created by Jefferson on 19/03/2016.
 */
public class CentroCustoDto {

    private PessoaJuridicaCentroCusto centroCusto;

    private BigDecimal percentual;

    private int rowNumber;

    public CentroCustoDto(int rowNumber) {
        this.rowNumber = rowNumber;
    }

    public PessoaJuridicaCentroCusto getCentroCusto() {
        return centroCusto;
    }

    public void setCentroCusto(PessoaJuridicaCentroCusto centroCusto) {
        this.centroCusto = centroCusto;
    }

    public int getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(int rowNumber) {
        this.rowNumber = rowNumber;
    }

    public BigDecimal getPercentual() {
        return percentual;
    }

    public void setPercentual(BigDecimal percentual) {
        this.percentual = percentual;
    }
}
