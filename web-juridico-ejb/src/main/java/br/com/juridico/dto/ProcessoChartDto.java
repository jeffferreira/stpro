package br.com.juridico.dto;

/**
 * Created by Jefferson on 28/02/2016.
 */
public class ProcessoChartDto {

    private String posicao;

    private Double porcentagem;

    public ProcessoChartDto(String posicao, Double porcentagem){
        this.posicao = posicao;
        this.porcentagem = porcentagem;
    }

    public String getPosicao() {
        return posicao;
    }

    public void setPosicao(String posicao) {
        this.posicao = posicao;
    }

    public Double getPorcentagem() {
        return porcentagem;
    }

    public void setPorcentagem(Double porcentagem) {
        this.porcentagem = porcentagem;
    }
}
