package br.com.juridico.dto;

import br.com.juridico.entidades.*;
import com.google.common.collect.Lists;

import java.util.List;

public class ParteDto {

    private static final String TIPO_ADVOGADO_INDIVIDUAL = "I";
    private static final String TIPO_PESSOA_FISICA = "PF";

    private Pessoa pessoa;

    private Parte parte;

    private String tipoPessoa;

    private String tipoAdvogado;

    private Posicao posicao;

    private Grupo grupo;
    
    private boolean advogadoInterno;

    private List<Pessoa> advogados = Lists.newArrayList();



    /**
     * campos para preenchimento da tela
     */
    private PessoaFisica pessoaFisica;
    private PessoaJuridica pessoaJuridica;
    private List<PessoaFisica> selectedAdvogados;
    private List<CentroCustoDto> centroCustos;

    public ParteDto() {
        this.tipoPessoa = TIPO_PESSOA_FISICA;
        this.tipoAdvogado = TIPO_ADVOGADO_INDIVIDUAL;
        this.selectedAdvogados = Lists.newArrayList();
        this.centroCustos = Lists.newArrayList();
        this.advogadoInterno = Boolean.TRUE;
        this.parte = new Parte();
    }

    public boolean isTipoPessoaFisica(){
        return TIPO_PESSOA_FISICA.equals(this.tipoPessoa);
    }


    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public String getTipoPessoa() {
        return tipoPessoa;
    }

    public void setTipoPessoa(String tipoPessoa) {
        this.tipoPessoa = tipoPessoa;
    }

    public String getTipoAdvogado() {
        return tipoAdvogado;
    }

    public void setTipoAdvogado(String tipoAdvogado) {
        this.tipoAdvogado = tipoAdvogado;
    }

    public Posicao getPosicao() {
        return posicao;
    }

    public void setPosicao(Posicao posicao) {
        this.posicao = posicao;
    }

    public Grupo getGrupo() {
        return grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }

    public List<Pessoa> getAdvogados() {
        return advogados;
    }

    public void setAdvogados(List<Pessoa> advogados) {
        this.advogados = advogados;
    }

    public void addAdvogado(Pessoa pessoa){
        this.advogados.add(pessoa);
    }

    public PessoaFisica getPessoaFisica() {
        return pessoaFisica;
    }

    public Parte getParte() {
        return parte;
    }

    public void setParte(Parte parte) {
        this.parte = parte;
    }

    public void setPessoaFisica(PessoaFisica pessoaFisica) {
        this.pessoaFisica = pessoaFisica;
        if(this.pessoaFisica != null)
            this.pessoa = pessoaFisica.getPessoa();
    }

    public PessoaJuridica getPessoaJuridica() {
        return pessoaJuridica;
    }

    public void setPessoaJuridica(PessoaJuridica pessoaJuridica) {
        this.pessoaJuridica = pessoaJuridica;
        if(this.pessoaJuridica != null)
            this.pessoa = pessoaJuridica.getPessoa();
    }

    public List<PessoaFisica> getSelectedAdvogados() {
        return selectedAdvogados;
    }

    public void setSelectedAdvogados(List<PessoaFisica> selectedAdvogados) {
        this.selectedAdvogados = selectedAdvogados;
    }

    public List<CentroCustoDto> getCentroCustos() {
        return centroCustos;
    }

    public void setCentroCustos(List<CentroCustoDto> centroCustos) {
        this.centroCustos = centroCustos;
    }

    public boolean isTipoAdvogadoIndividual() {
        return TIPO_ADVOGADO_INDIVIDUAL.equals(tipoAdvogado);
    }

	public boolean isAdvogadoInterno() {
		return advogadoInterno;
	}

	public void setAdvogadoInterno(boolean advogadoInterno) {
		this.advogadoInterno = advogadoInterno;
	}
}
