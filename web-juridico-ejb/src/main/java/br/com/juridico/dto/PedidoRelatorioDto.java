package br.com.juridico.dto;

public class PedidoRelatorioDto {

    private String pedido;

    private String causaPedido;

    private String valorPedido;

    private String valorProvavel;

    public PedidoRelatorioDto(String pedido, String causaPedido, String valorPedido, String valorProvavel) {
        this.pedido = pedido;
        this.causaPedido = causaPedido;
        this.valorPedido = valorPedido;
        this.valorProvavel = valorProvavel;
    }

    public String getPedido() {
        return pedido;
    }

    public void setPedido(String pedido) {
        this.pedido = pedido;
    }

    public String getCausaPedido() {
        return causaPedido;
    }

    public void setCausaPedido(String causaPedido) {
        this.causaPedido = causaPedido;
    }

    public String getValorPedido() {
        return valorPedido;
    }

    public void setValorPedido(String valorPedido) {
        this.valorPedido = valorPedido;
    }

    public String getValorProvavel() {
        return valorProvavel;
    }

    public void setValorProvavel(String valorProvavel) {
        this.valorProvavel = valorProvavel;
    }
}