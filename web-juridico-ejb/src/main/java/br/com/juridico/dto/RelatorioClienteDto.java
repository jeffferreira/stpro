package br.com.juridico.dto;

import java.util.Date;

/**
 * 
 * @author Jefferson
 *
 */
public class RelatorioClienteDto {

    private String nome;

    private String cpf;

    private String cnpj;

    private Integer ddd;

    private Date dataNascimento;

    private String sexo;

    private String email;

    private String numero;

    private String telefone;

    public RelatorioClienteDto() {
        // Construtor
    }

    public RelatorioClienteDto(Object param[]) {
        setNome((String) param[0]);
        setCpf((String) param[1]);
        setCnpj((String) param[2]);
        setDdd((Integer) param[3]);
        setNumero((String) param[4]);
        Character s = (Character) param[5];
        setSexo(s == null ? "" : s.toString());
        setDataNascimento((Date) param[6]);
        setEmail((String) param[7]);

        setTelefone(this.ddd == null ? this.numero : "(".concat(this.ddd.toString()).concat(") ").concat(this.numero));

    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public Integer getDdd() {
        return ddd;
    }

    public void setDdd(Integer ddd) {
        this.ddd = ddd;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

}
