package br.com.juridico.dto;

import java.math.BigDecimal;

import br.com.juridico.entidades.CausaPedido;
import br.com.juridico.entidades.Pedido;
import br.com.juridico.entidades.ProcessoPedido;

/**
 * 
 * @author Jefferson
 *
 */
public class PedidoDto {

    private ProcessoPedido processoPedido;

	private Pedido pedido;

	private CausaPedido causaPedido;

	private BigDecimal valorPedido;

	private BigDecimal valorProvavel;

	private int rowNumber;

	public PedidoDto() {
		// construtor
	}

	public PedidoDto(int rowNumber) {
		this.rowNumber = rowNumber;
	}

	public PedidoDto(Pedido pedido, CausaPedido causaPedido, BigDecimal valorPedido, BigDecimal valorProvavel) {
		this.pedido = pedido;
		this.causaPedido = causaPedido;
		this.valorPedido = valorPedido;
		this.valorProvavel = valorProvavel;
	}

	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	public CausaPedido getCausaPedido() {
		return causaPedido;
	}

	public void setCausaPedido(CausaPedido causaPedido) {
		this.causaPedido = causaPedido;
	}

	public BigDecimal getValorPedido() {
		return valorPedido;
	}

	public void setValorPedido(BigDecimal valorPedido) {
		this.valorPedido = valorPedido;
	}

	public BigDecimal getValorProvavel() {
		return valorProvavel;
	}
	
	public void setValorProvavel(BigDecimal valorProvavel) {
		this.valorProvavel = valorProvavel;
	}

	public int getRowNumber() {
		return rowNumber;
	}

	public void setRowNumber(int rowNumber) {
		this.rowNumber = rowNumber;
	}

    public ProcessoPedido getProcessoPedido() {
        return processoPedido;
    }

    public void setProcessoPedido(ProcessoPedido processoPedido) {
        this.processoPedido = processoPedido;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PedidoDto pedidoDto = (PedidoDto) o;

        if (processoPedido != null ? !processoPedido.equals(pedidoDto.processoPedido) : pedidoDto.processoPedido != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return processoPedido != null ? processoPedido.hashCode() : 0;
    }
}
