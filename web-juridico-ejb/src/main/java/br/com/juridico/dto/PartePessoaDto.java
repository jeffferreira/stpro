package br.com.juridico.dto;

import br.com.juridico.entidades.Pessoa;
import br.com.juridico.entidades.Posicao;
import com.google.common.collect.Lists;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.util.List;

/**
 * Created by jeffersonl2009_00 on 16/11/2016.
 */
public class PartePessoaDto {

    private Long idParte;
    private List<PessoaPosicao> pessoaPosicoes = Lists.newArrayList();

    public Long getIdParte() {
        return idParte;
    }

    public void setIdParte(Long idParte) {
        this.idParte = idParte;
    }

    public List<PessoaPosicao> getPessoaPosicoes() {
        return pessoaPosicoes;
    }

    public void setPessoaPosicoes(List<PessoaPosicao> pessoaPosicoes) {
        this.pessoaPosicoes = pessoaPosicoes;
    }

    public void addPessoaPosicoes(Pessoa pessoa, Posicao posicao){
        if(pessoa != null && posicao != null) {
            pessoaPosicoes.add(new PessoaPosicao(pessoa, posicao));
        }
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(idParte).toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof PartePessoaDto) {
            final PartePessoaDto other = (PartePessoaDto) obj;
            return new EqualsBuilder().append(idParte, other.idParte).isEquals();
        } else {
            return false;
        }
    }

    /**
     * inner class
     */
    class PessoaPosicao {

        PessoaPosicao(Pessoa pessoa, Posicao posicao){
            this.pessoa = pessoa;
            this.posicao = posicao;
        }

        private Pessoa pessoa;

        private Posicao posicao;

        public Pessoa getPessoa() {
            return pessoa;
        }

        public Posicao getPosicao() {
            return posicao;
        }
    }
}
