package br.com.juridico.dto;

/**
 * Created by jefferson.ferreira on 22/05/2017.
 */
public class CepDto {

    private String cidade;
    private String logradouro;
    private String bairro;
    private String cep;
    private String tipoLogradouro;
    private String uf;
    private boolean cepUnico;

    public CepDto(Object[] cep, boolean cepUnico){
        this.cepUnico = cepUnico;

        if (cepUnico){
            this.cidade = convertToString(cep[0]);
            this.cep = convertToString(cep[1]);
            this.uf = convertToString(cep[2]);
        } else {
            this.cidade = convertToString(cep[0]);
            this.logradouro = convertToString(cep[1]);
            this.bairro = convertToString(cep[2]);
            this.cep = convertToString(cep[3]);
            this.tipoLogradouro = convertToString(cep[4]);
        }
    }

    private String convertToString(Object object) {
        return object == null ? "" : (String) object;
    }

    public String getCidade() {
        return cidade;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public String getBairro() {
        return bairro;
    }

    public String getCep() {
        return cep;
    }

    public String getTipoLogradouro() {
        return tipoLogradouro;
    }

    public boolean isCepUnico() {
        return cepUnico;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }
}
