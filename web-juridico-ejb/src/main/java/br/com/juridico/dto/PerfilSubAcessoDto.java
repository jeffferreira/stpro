package br.com.juridico.dto;


import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * Created by Marcos on 27/09/2016.
 */
public class PerfilSubAcessoDto {

    private String subAcesso;
    
    private boolean visualiza;

	private boolean insere;
    
    private boolean exclui;
    

    
	public PerfilSubAcessoDto(String subAcesso, boolean visualiza, boolean insere, boolean exclui) {
		this.subAcesso = subAcesso;
		this.visualiza = visualiza;
		this.insere = insere;
		this.exclui = exclui;
	}

	public String getSubAcesso() {
		return subAcesso;
	}

	public void setSubAcesso(String subAcesso) {
		this.subAcesso = subAcesso;
	}

	public boolean isVisualiza() {
		return visualiza;
	}

	public void setVisualiza(boolean visualiza) {
		this.visualiza = visualiza;
	}

	public boolean isExclui() {
		return exclui;
	}

	public void setExclui(boolean exclui) {
		this.exclui = exclui;
	}

	public boolean isInsere() {
		return insere;
	}

	public void setInsere(boolean insere) {
		this.insere = insere;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;

		if (o == null || getClass() != o.getClass()) return false;

		PerfilSubAcessoDto that = (PerfilSubAcessoDto) o;

		return new EqualsBuilder()
				.append(subAcesso, that.subAcesso)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(subAcesso)
				.toHashCode();
	}
}
