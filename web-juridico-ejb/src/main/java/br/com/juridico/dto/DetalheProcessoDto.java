/**
 * 
 */
package br.com.juridico.dto;

/**
 * @author Jefferson
 *
 */
public class DetalheProcessoDto {

    private String tipo;

    private String nome;

    private String advogado;

    public DetalheProcessoDto(String tipo, String nome, String advogado) {
        super();
        this.tipo = tipo;
        this.nome = nome;
        this.advogado = advogado;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getAdvogado() {
        return advogado;
    }

    public void setAdvogado(String advogado) {
        this.advogado = advogado;
    }

}
