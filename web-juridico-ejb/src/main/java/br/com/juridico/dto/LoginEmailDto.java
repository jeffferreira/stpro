package br.com.juridico.dto;

/**
 * Created by Jefferson on 27/06/2016.
 */
public class LoginEmailDto {

    private String login;

    private byte[] senha;

    private String nome;

    public LoginEmailDto(String login, byte[] senha, String nome){
        this.login = login;
        this.senha = senha;
        this.nome = nome;
    }

    public String getLogin() {
        return login;
    }

    public String getNome() {
        return nome;
    }

    public byte[] getSenha() {
        return senha;
    }

}
