package br.com.juridico.dto;

public class ParteRelatorioDto {

    private String nome;

    private String posicao;

    private String cliente;

    private String advogados;

    public ParteRelatorioDto(String nome, String posicao, String cliente, String advogados) {
        this.nome = nome;
        this.posicao = posicao;
        this.cliente = cliente;
        this.advogados = advogados;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getPosicao() {
        return posicao;
    }

    public void setPosicao(String posicao) {
        this.posicao = posicao;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getAdvogados() {
        return advogados;
    }

    public void setAdvogados(String advogados) {
        this.advogados = advogados;
    }
}