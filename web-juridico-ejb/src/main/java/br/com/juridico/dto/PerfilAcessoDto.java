package br.com.juridico.dto;

import br.com.juridico.entidades.Acesso;
import br.com.juridico.entidades.DireitoPerfil;
import br.com.juridico.entidades.Perfil;
import br.com.juridico.entidades.PerfilAcesso;
import br.com.juridico.enums.DireitosIndicador;
import com.google.common.collect.Lists;

import java.util.List;

/**
 * Created by Jefferson on 31/07/2016.
 */
public class PerfilAcessoDto {

    private static final String EDITAR = "ALT";
    private static final String INCLUIR = "INC";
    private static final String EXCLUIR = "EXC";
    private static final String CONSULTAR = "CON";
    private static final String CADASTRO_DE = "cadastro de";

    private Perfil perfil;

    private Acesso acesso;

    private PerfilAcesso perfilAcesso;

    private boolean menuAtivo;
    private boolean edita;
    private boolean exclui;
    private boolean insere;
    private boolean consulta;
    private List<PerfilSubAcessoDto> perfilSubAcessoDtos = Lists.newArrayList();

    public PerfilAcessoDto(Acesso acesso, PerfilAcesso perfilAcesso){
        this.acesso = acesso;
        this.perfilAcesso = perfilAcesso;
        this.perfil = perfilAcesso != null ? perfilAcesso.getPerfil() : null;
    }

    public Perfil getPerfil() {
        return perfil;
    }

    public void setPerfil(Perfil perfil) {
        this.perfil = perfil;
    }

    public Acesso getAcesso() {
        return acesso;
    }

    public void setAcesso(Acesso acesso) {
        this.acesso = acesso;
    }

    public PerfilAcesso getPerfilAcesso() {
        return perfilAcesso;
    }

    public void setPerfilAcesso(PerfilAcesso perfilAcesso) {
        this.perfilAcesso = perfilAcesso;
    }

    public boolean isMenuAtivo() {
        setMenuAtivo(perfilAcesso != null);
        return menuAtivo;
    }

    public void setMenuAtivo(boolean menuAtivo) {
        this.menuAtivo = menuAtivo;
    }

    public boolean isEdita() {
        setEdita(perfilAcesso != null && possuiDireito(perfilAcesso.getDireitoPerfis(), EDITAR));
        return edita;
    }

    public void setEdita(boolean edita) {
        this.edita = edita;
    }

    public boolean isExclui() {
        setExclui(perfilAcesso != null && possuiDireito(perfilAcesso.getDireitoPerfis(), EXCLUIR));
        return exclui;
    }

    public void setExclui(boolean exclui) {
        this.exclui = exclui;
    }
    
	public boolean isConsulta() {
		setConsulta(perfilAcesso != null && possuiDireito(perfilAcesso.getDireitoPerfis(), CONSULTAR));
		return consulta;
	}

	public void setConsulta(boolean consulta) {
		this.consulta = consulta;
	}

	public boolean isInsere() {
        setInsere(perfilAcesso != null && possuiDireito(perfilAcesso.getDireitoPerfis(), INCLUIR));
        return insere;
    }

    public void setInsere(boolean insere) {
        this.insere = insere;
    }

    private boolean possuiDireito(List<DireitoPerfil> direitos, String sigla){
        for (DireitoPerfil dp : direitos) {
            if (sigla.equals(dp.getDireito().getSigla())) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    public boolean isPerfilCadastroPessoas() {
        if(perfilAcesso != null) {
            String chave = CADASTRO_DE;
            return perfilAcesso.getAcesso() != null ? perfilAcesso.getAcesso().getDescricao().toLowerCase().contains(chave) : false;
        }
        return false;
    }

	public List<PerfilSubAcessoDto> getPerfilSubAcessoDtos() {
		return perfilSubAcessoDtos;
	}

	public void setPerfilSubAcessoDtos(List<PerfilSubAcessoDto> perfilSubAcessoDtos) {
		this.perfilSubAcessoDtos = perfilSubAcessoDtos;
	}

    public PerfilSubAcessoDto getSubAcesso(String funcionalidade){
        for (PerfilSubAcessoDto subAcessoDto : perfilSubAcessoDtos) {
            if(subAcessoDto.getSubAcesso().equals(funcionalidade)) {
                return subAcessoDto;
            }
        }
        return null;
    }

    public void addAcessosFilhos(PerfilAcesso perfilFilho, List<DireitoPerfil> direitoPerfils){
        if(perfilFilho == null || perfilFilho.getAcesso() == null) {
            return;
        }

        boolean consultar = Boolean.FALSE;
        boolean excluir = Boolean.FALSE;
        boolean incluir = Boolean.FALSE;

        String subAcesso = perfilFilho.getAcesso().getFuncionalidade();

        for (DireitoPerfil direitoPerfil : direitoPerfils) {
            DireitosIndicador direito = DireitosIndicador.fromTextSigla(direitoPerfil.getDireito().getSigla());

            switch (direito){
                case CON:
                    consultar = Boolean.TRUE;
                    break;
                case INC:
                    incluir = Boolean.TRUE;
                    break;
                case EXC:
                    excluir = Boolean.TRUE;
                    break;
            }
        }

        PerfilSubAcessoDto subAcessoDto = new PerfilSubAcessoDto(subAcesso, consultar, incluir, excluir);
        perfilSubAcessoDtos.add(subAcessoDto);
    }


}
