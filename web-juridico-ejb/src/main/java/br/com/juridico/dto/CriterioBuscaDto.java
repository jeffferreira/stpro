package br.com.juridico.dto;

/**
 * Created by Jefferson on 09/04/2016.
 */
public class CriterioBuscaDto {

    private int tipoBusca;

    private Object valorBusca;

    public CriterioBuscaDto(int tipoBusca, Object valorBusca){
        this.tipoBusca = tipoBusca;
        this.valorBusca = valorBusca;
    }

    public int getTipoBusca() {
        return tipoBusca;
    }

    public Object getValorBusca() {
        return valorBusca;
    }

}
