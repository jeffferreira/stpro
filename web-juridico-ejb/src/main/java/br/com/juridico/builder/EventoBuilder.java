package br.com.juridico.builder;

import java.util.Date;

import br.com.juridico.entidades.*;

public class EventoBuilder {

	private String descricao;

	private String localizacao;

	private Date dataInicio;

	private Date dataFim;

	private String observacao;

	private StatusAgenda statusAgenda;

	private Pessoa pessoa;

	private Andamento andamento;

	private Processo processo;

	private PrazoAndamento prazoAndamento;

	public EventoBuilder addDescricao(String descricao) {
		this.descricao = descricao;
		return this;
	}

	public EventoBuilder addLocalizacao(String localizacao) {
		this.localizacao = localizacao;
		return this;
	}

	public EventoBuilder addDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
		return this;
	}

	public EventoBuilder addDataFim(Date dataFim) {
		this.dataFim = dataFim;
		return this;
	}

	public EventoBuilder addObservacao(String observacao) {
		this.observacao = observacao;
		return this;
	}

	public EventoBuilder addStatusAgenda(StatusAgenda statusAgenda) {
		this.statusAgenda = statusAgenda;
		return this;
	}

	public EventoBuilder addPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
		return this;
	}

	public EventoBuilder addAndamento(Andamento andamento) {
		this.andamento = andamento;
		return this;
	}

	public EventoBuilder addProcesso(Processo processo) {
		this.processo = processo;
		return this;
	}

	public EventoBuilder addPrazoAndamento(PrazoAndamento prazoAndamento) {
		this.prazoAndamento = prazoAndamento;
		return this;
	}

	public Evento build() {
		Evento evento = new Evento();
		evento.setDescricao(this.descricao);
		evento.setLocalizacao(this.localizacao);
		evento.setDataInicio(this.dataInicio);
		evento.setDataFim(this.dataFim);
		evento.setObservacao(this.observacao);
		evento.setStatusAgenda(this.statusAgenda);
		evento.setPessoa(this.pessoa);
		evento.setAndamento(this.andamento);
		evento.setProcesso(this.processo);
		evento.setPrazoAndamento(this.prazoAndamento);

		return evento;
	}
}
