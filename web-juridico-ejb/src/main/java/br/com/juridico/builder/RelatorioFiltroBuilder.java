package br.com.juridico.builder;

import br.com.juridico.entidades.RelatorioCriterioProcesso;
import br.com.juridico.entidades.RelatorioFiltroProcesso;
import br.com.juridico.enums.RelatorioProcessoCampoType;

/**
 * Created by jefferson.ferreira on 31/08/2017.
 */
public class RelatorioFiltroBuilder {

    private Long numeroEmpresa;

    public RelatorioFiltroBuilder(Long numeroEmpresa){
        this.numeroEmpresa = numeroEmpresa;
    }

    public RelatorioFiltroProcesso build(){
        RelatorioFiltroProcesso relatorioFiltroProcesso = new RelatorioFiltroProcesso();
        relatorioFiltroProcesso.setCodigoEmpresa(this.numeroEmpresa);

        for (RelatorioProcessoCampoType type : RelatorioProcessoCampoType.values()) {
            RelatorioCriterioProcesso criterio =
                    new RelatorioCriterioProcesso(relatorioFiltroProcesso, type.getDescricao(), type.getValorParaQuery(), type.getTipoCampo(), type.getNomeCampoView());
            relatorioFiltroProcesso.getCriterios().add(criterio);
        }
        return relatorioFiltroProcesso;
    }
}
