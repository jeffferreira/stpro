package br.com.juridico.interfaces;

public interface Descriptable {

    String getDescription();
}
