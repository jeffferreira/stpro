package br.com.juridico.mail;

import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import br.com.juridico.entidades.ConfiguracaoEmail;
import org.apache.log4j.Logger;

import com.google.common.collect.Lists;

/**
 * 
 * @author Jefferson
 *
 */
public final class MailBusiness {

    private static final Logger LOGGER = Logger.getLogger(MailBusiness.class);

    private static boolean auth;
    private static boolean debug;

    static {
        auth = Boolean.parseBoolean(System.getProperty("stpro.mail.smtp.auth"));
        debug = Boolean.parseBoolean(System.getProperty("stpro.mail.debug"));
    }

    private MailBusiness() {
    }

    public static void enviarEmail(ConfiguracaoEmail configuracaoEmail, String assunto, String conteudo, String... destinatarios) throws MessagingException {
        if (configuracaoEmail == null) {
            LOGGER.info("As informacoes para envio de email nao foram configuradas no servidor de aplicacao");
            return;
        }

        Properties props = new Properties();
        props.put("mail.smtp.host", configuracaoEmail.getHost());
        props.put("mail.smtp.port", configuracaoEmail.getPort());
        if ("SMTPS".equals(configuracaoEmail.getProtocol())) {
            props.put("mail.smtp.ssl.enable", true);
        } else if ("TLS".equals(configuracaoEmail.getProtocol())) {
            props.put("mail.smtp.starttls.enable", true);
        }

        Authenticator authenticator = null;
        if (auth) {
            props.put("mail.smtp.auth", true);
            authenticator = new Authenticator() {
                private PasswordAuthentication pa = new PasswordAuthentication(configuracaoEmail.getUsername(), configuracaoEmail.getPassword());

                @Override
                public PasswordAuthentication getPasswordAuthentication() {
                    return pa;
                }
            };
        }

        Session session = Session.getInstance(props, authenticator);
        session.setDebug(debug);

        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(configuracaoEmail.getFrom()));

        send(assunto, conteudo, message, destinatarios);
    }

    private static void send(String assunto, String conteudo, MimeMessage message, String... destinatarios) throws AddressException,
            MessagingException {
        List<InternetAddress> addresses = Lists.newLinkedList();

        for (String destinatario : destinatarios) {
            if (destinatario != null) {
                addresses.add(new InternetAddress(destinatario));
            }
        }

        message.setRecipients(Message.RecipientType.TO, addresses.toArray(new InternetAddress[addresses.size()]));

        message.setSubject(assunto);
        message.setSentDate(new Date());

        Multipart multipart = new MimeMultipart("alternative");

        MimeBodyPart textPart = new MimeBodyPart();
        textPart.setText(assunto);

        MimeBodyPart htmlPart = new MimeBodyPart();
        htmlPart.setContent(conteudo, "text/html; charset=utf-8");

        multipart.addBodyPart(textPart);
        multipart.addBodyPart(htmlPart);
        message.setContent(multipart);

        Transport.send(message);
    }

}
