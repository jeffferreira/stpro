package br.com.juridico.validation.cadastros;

import br.com.juridico.validation.ValidationWrapper;

import javax.validation.constraints.Size;

/**
 * Created by jeffersonl2009_00 on 17/08/2016.
 */
public class FuncaoValidacaoWrapper implements ValidationWrapper {

    @Size(min = 1, message = "Já existe um registro cadastrado com esta descrição.")
    private String descricao;

    public FuncaoValidacaoWrapper(boolean funcaoJaEstaCadastrada) {
        this.descricao = validate(funcaoJaEstaCadastrada);
    }

}
