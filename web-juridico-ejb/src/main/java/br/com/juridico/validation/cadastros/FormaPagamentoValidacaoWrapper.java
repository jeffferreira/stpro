package br.com.juridico.validation.cadastros;

import br.com.juridico.validation.ValidationWrapper;

import javax.validation.constraints.Size;

public class FormaPagamentoValidacaoWrapper implements ValidationWrapper {

    @Size(min = 1, message = "Já existe um registro cadastrado com esta descrição.")
    private String descricao;

    public FormaPagamentoValidacaoWrapper(boolean formaPagamentoJaEstaCadastrado) {
        this.descricao = validate(formaPagamentoJaEstaCadastrado);
    }
}
