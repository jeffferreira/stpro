package br.com.juridico.validation.cadastros;

import br.com.juridico.entidades.Empresa;
import br.com.juridico.validation.ValidationWrapper;
import com.google.common.base.Strings;

import javax.validation.constraints.Size;

public class EmpresaValidacaoWrapper implements ValidationWrapper {

    @Size(min = 1, message = "Por favor informe a fantasia.")
    private String fantasia;
    @Size(min = 1, message = "Por favor informe a razão social.")
    private String razaoSocial;
    @Size(min = 1, message = "Por favor informe o CNPJ.")
    private String cnpj;
    @Size(min = 1, message = "Por favor informe o endereço.")
    private String endereco;
    @Size(min = 1, message = "Por favor informe o número.")
    private String numero;
    @Size(min = 1, message = "Por favor informe a cidade.")
    private String cidade;
    @Size(min = 1, message = "Por favor informe a UF.")
    private String uf;


    public EmpresaValidacaoWrapper(Empresa empresa) {
        this.fantasia = validate(Strings.isNullOrEmpty(empresa.getDescricao()));
        this.razaoSocial = validate(Strings.isNullOrEmpty(empresa.getRazaoSocial()));
        this.cnpj = validate(Strings.isNullOrEmpty(empresa.getCnpj()));
        this.endereco = validate(Strings.isNullOrEmpty(empresa.getLogradouro()));
        this.numero = validate(empresa.getNumero() == null);
        this.cidade = validate(Strings.isNullOrEmpty(empresa.getCidade()));
        this.uf = validate(Strings.isNullOrEmpty(empresa.getUf()));
    }
}
