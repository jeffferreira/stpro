package br.com.juridico.validation.cadastros;

import br.com.juridico.entidades.ContratoPessoa;
import br.com.juridico.entidades.ContratoPessoaVigencia;
import br.com.juridico.util.DateUtils;
import br.com.juridico.validation.ValidationWrapper;
import com.google.common.base.Strings;

import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by jeffersonl2009_00 on 04/08/2016.
 */
public class ContratoValidacaoWrapper implements ValidationWrapper {

    private static final String PESSOA_FISICA = "PF";

    @Size(min = 1, message = "Descrição: preenchimento obrigatório")
    private String descricao;
    @Size(min = 1, message = "Nome: preenchimento obrigatório")
    private String nomePessoaFisica;
    @Size(min = 1, message = "Razão Social: preenchimento obrigatório")
    private String nomePessoaJuridica;
    @Size(min = 1, message = "Valor Mensal do Ato: preenchimento obrigatório")
    private String valorMensalAto;
    @Size(min = 1, message = "Valor Acompanhamento Mensal: preenchimento obrigatório")
    private String valorAcompanhamentoMensal;
    @Size(min = 1, message = "Data Inicial: preenchimento obrigatório")
    private String dataInicial;
    @Size(min = 1, message = "A data inicial deve ser menor que a data final.")
    private String dataInicialMenorQueFinal;
    @Size(min = 1, message = "Já existe um contrato vigente no período informado.")
    private String existePeriodoVigente;

    public ContratoValidacaoWrapper(ContratoPessoa cp, String tipoPessoa, ContratoPessoaVigencia vigencia) {
        if(PESSOA_FISICA.equals(tipoPessoa)) {
            this.nomePessoaFisica = validate(cp.getPessoa() == null);
            this.nomePessoaJuridica = VALIDO;
        } else {
            this.nomePessoaJuridica = validate(cp.getPessoa() == null);
            this.nomePessoaFisica = VALIDO;
        }
        this.descricao = validate(Strings.isNullOrEmpty(cp.getDescricao()));
        this.valorMensalAto = validate(!isValorMensalValido(vigencia));
        this.valorAcompanhamentoMensal = validate(cp.isAcompMensal() && !isValorAcompMensalValido(vigencia));
        this.dataInicial = validate(vigencia.getDataInicio() == null);
        this.dataInicialMenorQueFinal = validate(DateUtils.dataFinalMenorQueInicial(vigencia.getDataInicio(), vigencia.getDataFim()));
        this.existePeriodoVigente = validate(isExistePeriodoVigente(vigencia, cp.getVigencias()));
    }

    private boolean isValorMensalValido(ContratoPessoaVigencia vigencia) {
        return vigencia.getValorMensal() != null && !vigencia.getValorMensal().equals(BigDecimal.ZERO);
    }

    private boolean isValorAcompMensalValido(ContratoPessoaVigencia vigencia) {
        return vigencia.getValorAcompanhamentoMensal() != null && !vigencia.getValorAcompanhamentoMensal().equals(BigDecimal.ZERO);
    }

    private boolean isExistePeriodoVigente(ContratoPessoaVigencia vigencia, List<ContratoPessoaVigencia> vigencias) {
        boolean result = false;
        for (ContratoPessoaVigencia item : vigencias) {
            if (!item.getId().equals(vigencia.getId()) && item.isContratoVigente()) {
                result = Boolean.TRUE;
                break;
            }
        }
        return result;
    }
}
