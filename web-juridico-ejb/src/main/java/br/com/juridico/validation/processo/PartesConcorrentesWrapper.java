package br.com.juridico.validation.processo;

import br.com.juridico.entidades.Parte;
import br.com.juridico.entidades.Processo;
import br.com.juridico.entidades.User;
import br.com.juridico.validation.ValidationWrapper;

import javax.validation.constraints.Size;

/**
 * Created by jefferson on 27/11/2016.
 */
public class PartesConcorrentesWrapper implements ValidationWrapper {

    @Size(min = 1, message = "Existe mais de uma parte cliente cadastrada. Favor contate o Administrador.")
    private String partesConcorrentes;

    public PartesConcorrentesWrapper(Processo processo, User user){
        if(!user.getPerfil().isAdmin()) {
            int qdteParteCliente = 0;
            for (Parte parte : processo.getPartes()) {
                if(parte.getPessoa().isClienteEmpresa()){
                    qdteParteCliente++;
                }
            }
            partesConcorrentes = validate(qdteParteCliente > 1);
            return;
        }
        partesConcorrentes = validate(!user.getPerfil().isAdmin());
    }
}
