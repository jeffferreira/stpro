package br.com.juridico.validation.processo;

import br.com.juridico.entidades.HistoricoProposta;
import br.com.juridico.validation.ValidationWrapper;

import javax.validation.constraints.Size;

/**
 * Created by jeffersonl2009_00 on 21/06/2016.
 */
public class PropostaValidacaoWrapper implements ValidationWrapper {

    private static final String VALIDO = "VALIDO";

    @Size(min = 1, message = "Favor preencher a data da proposta.")
    private String dataProposta;
    @Size(min = 1, message = "Favor preencher a data final do prazo.")
    private String dataFimPrazo;
    @Size(min = 1, message = "Favor preencher o proponente.")
    private String pessoa;
    @Size(min = 1, message = "Favor preencher o valor.")
    private String valor;

    public PropostaValidacaoWrapper(HistoricoProposta proposta){
        this.dataProposta = proposta.getDataProposta() == null ? "" : VALIDO;
        this.dataFimPrazo = proposta.getDataFimPrazo() == null ? "" : VALIDO;
        this.pessoa = proposta.getPessoa() == null ? "" : VALIDO;
        this.valor = proposta.getValorProposta() == null ? "" : VALIDO;
    }
}
