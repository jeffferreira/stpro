package br.com.juridico.validation.pessoas;

import br.com.juridico.validation.ValidationWrapper;

import javax.validation.constraints.Size;

/**
 * 
 * @author Jefferson
 *
 */
public class PessoaExistenteValidacaoWrapper implements ValidationWrapper {

	@Size(min = 1, message = "Já existe uma pessoa cadastrada com este CPF.")
	private String cpfExistente;
	@Size(min = 1, message = "Já existe uma pessoa cadastrada com este CNPJ.")
	private String cnpjExistente;
	@Size(min = 1, message = "Já existe uma pessoa cadastrada com este nome.")
	private String nomeExistente;


	/**
	 *
	 * @param pessoaFisica
	 * @param cpfCnpjExistente
	 * @param nomeJaExiste
	 */
	public PessoaExistenteValidacaoWrapper(boolean pessoaFisica, boolean cpfCnpjExistente, boolean nomeJaExiste) {
		if(pessoaFisica) {
			this.cpfExistente = validate(cpfCnpjExistente);
			this.cnpjExistente = VALIDO;
			this.nomeExistente = validate(nomeJaExiste);
			return;
		}

		this.cpfExistente = VALIDO;
		this.cnpjExistente = validate(cpfCnpjExistente);
		this.nomeExistente = validate(nomeJaExiste);
	}
}
