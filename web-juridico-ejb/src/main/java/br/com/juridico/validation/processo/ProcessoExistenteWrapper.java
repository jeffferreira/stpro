package br.com.juridico.validation.processo;

import br.com.juridico.validation.ValidationWrapper;

import javax.validation.constraints.Size;

/**
 * Created by jefferson on 27/11/2016.
 */
public class ProcessoExistenteWrapper implements ValidationWrapper {

    @Size(min = 1, message = "Já existe um processo cadastrado com este número.")
    private String processoExistente;

    public ProcessoExistenteWrapper(boolean existeProcessoCadastrado){
        processoExistente = validate(existeProcessoCadastrado);
    }
}
