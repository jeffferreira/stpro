package br.com.juridico.validation.processo.andamento;

import br.com.juridico.entidades.Andamento;
import br.com.juridico.validation.ValidationWrapper;
import com.google.common.base.Strings;

import javax.validation.constraints.Size;

/**
 * Created by Jefferson on 21/06/2016.
 */
public class AndamentoValidacaoWrapper implements ValidationWrapper {

    public static final String OUTRO_LOCAL = "O";
    @Size(min = 1, message = "Favor preencher a data do andamento.")
    private String dataAndamento;
    @Size(min = 1, message = "Favor preencher se informação confidencial.")
    private String informacaoConfidencial;
    @Size(min = 1, message = "Favor preencher o tipo do andamento.")
    private String tipoAndamento;
    @Size(min = 1, message = "Favor preencher o(s) advogado(s).")
    private String advogados;
    @Size(min = 1, message = "Favor preencher o(s) dias para aviso.")
    private String diasAviso;
    @Size(min = 1, message = "Favor preencher a localização.")
    private String localizacao;


    public AndamentoValidacaoWrapper(Andamento andamento){
        this.dataAndamento = validate(andamento.getDataAndamento() == null);
        this.informacaoConfidencial = validate(andamento.getConfidencial() == null);
        this.tipoAndamento = validate(andamento.getTipoAndamento() == null);

        validarEnviaEmailAviso(andamento);
        validarAdvogados(andamento);
        validarLocalizacao(andamento);
    }

    private void validarEnviaEmailAviso(Andamento andamento) {
        if(andamento.getEnviaEmailAviso()) {
            this.diasAviso = validate(andamento.getQuantidadeDiasEmailAvisoAntecedencia() == null);
        }
    }

    private void validarAdvogados(Andamento andamento) {
        if(!andamento.getEnviaResumoAdvogados() && andamento.getEnviaResumoAdvogado()) {
            this.advogados = validate(andamento.getAndamentoAdvogados().isEmpty());
        } else {
            this.advogados = VALIDO;
        }
    }

    private void validarLocalizacao(Andamento andamento){
        boolean isOutroLocal = OUTRO_LOCAL.equals(andamento.getOpcaoLocal());
        if (isOutroLocal && Strings.isNullOrEmpty(andamento.getLocalizacao())) {
            this.localizacao = "";
            return;
        }
        this.localizacao = validate(Strings.isNullOrEmpty(andamento.getOpcaoLocal()));
    }
}
