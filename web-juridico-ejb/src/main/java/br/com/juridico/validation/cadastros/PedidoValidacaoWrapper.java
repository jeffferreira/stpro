package br.com.juridico.validation.cadastros;

import br.com.juridico.entidades.Pedido;
import br.com.juridico.validation.ValidationWrapper;
import com.google.common.base.Strings;

import javax.validation.constraints.Size;

public class PedidoValidacaoWrapper implements ValidationWrapper {

    @Size(min = 1, message = "Descrição: preenchimento obrigatório")
    private String descricao;
    @Size(min = 1, message = "Já existe um registro cadastrado com esta descrição.")
    private String descricaoExistente;

    public PedidoValidacaoWrapper(Pedido pedido, boolean pedidoJaEstaCadastrado) {
        this.descricao = validate(Strings.isNullOrEmpty(pedido.getDescricao()));
        this.descricaoExistente = validate(pedidoJaEstaCadastrado);
    }
}
