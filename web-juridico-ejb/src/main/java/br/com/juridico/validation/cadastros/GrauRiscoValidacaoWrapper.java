package br.com.juridico.validation.cadastros;

import br.com.juridico.entidades.GrauRisco;
import br.com.juridico.validation.ValidationWrapper;
import com.google.common.base.Strings;

import javax.validation.constraints.Size;

public class GrauRiscoValidacaoWrapper implements ValidationWrapper {

    @Size(min = 1, message = "Descrição: preenchimento obrigatório")
    private String descricao;
    @Size(min = 1, message = "Já existe um registro cadastrado com esta descrição.")
    private String descricaoExistente;

    public GrauRiscoValidacaoWrapper(GrauRisco grauRisco, boolean grauRiscoJaEstaCadastrado) {
        this.descricao = validate(Strings.isNullOrEmpty(grauRisco.getDescricao()));
        this.descricaoExistente = validate(grauRiscoJaEstaCadastrado);
    }
}
