package br.com.juridico.validation.cadastros;

import br.com.juridico.entidades.Fase;
import br.com.juridico.validation.ValidationWrapper;
import com.google.common.base.Strings;

import javax.validation.constraints.Size;

public class FaseValidacaoWrapper implements ValidationWrapper {

    @Size(min = 1, message = "Descrição: preenchimento obrigatório")
    private String descricao;
    @Size(min = 1, message = "Já existe um registro cadastrado com esta descrição.")
    private String descricaoExistente;

    public FaseValidacaoWrapper(Fase fase, boolean faseJaEstaCadastrado) {
        this.descricao = validate(Strings.isNullOrEmpty(fase.getDescricao()));
        this.descricaoExistente = validate(faseJaEstaCadastrado);
    }
}
