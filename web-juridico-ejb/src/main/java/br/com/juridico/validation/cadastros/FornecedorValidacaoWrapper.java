package br.com.juridico.validation.cadastros;

import br.com.juridico.entidades.Fornecedor;
import br.com.juridico.validation.ValidationWrapper;
import com.google.common.base.Strings;

import javax.validation.constraints.Size;

public class FornecedorValidacaoWrapper implements ValidationWrapper {

    @Size(min = 1, message = "Nome: preenchimento obrigatório")
    private String nome;
    @Size(min = 1, message = "Já existe um registro cadastrado com este nome.")
    private String nomeExistente;
    @Size(min = 1, message = "CPF ou CNPJ: preenchimento obrigatório")
    private String cpfCnpj;

    @Size(min = 1, message = "Nome Favorecido: preenchimento obrigatório")
    private String nomeFavorecido;
    @Size(min = 1, message = "CPF ou CNPJ Favorecido: preenchimento obrigatório")
    private String cpfCnpjFavorecido;
    @Size(min = 1, message = "Banco Favorecido: preenchimento obrigatório")
    private String bancoFavorecido;
    @Size(min = 1, message = "Agência Favorecido: preenchimento obrigatório")
    private String agenciaFavorecido;
    @Size(min = 1, message = "Conta Corrente: preenchimento obrigatório")
    private String contaCorrenteFavorecido;


    public FornecedorValidacaoWrapper(Fornecedor fornecedor, boolean existeFornecedorCadastrado, boolean cadastrarFavorecido) {
        this.nome = validate(Strings.isNullOrEmpty(fornecedor.getNome()));
        this.nomeExistente = validate(existeFornecedorCadastrado);
        this.cpfCnpj = validate(Strings.isNullOrEmpty(fornecedor.getCpfCnpj()));

        if(cadastrarFavorecido){
            this.nomeFavorecido = validate(Strings.isNullOrEmpty(fornecedor.getNomeFavorecido()));
            this.cpfCnpjFavorecido = validate(Strings.isNullOrEmpty(fornecedor.getCpfCnpjFavorecido()));
            this.bancoFavorecido = validate(Strings.isNullOrEmpty(fornecedor.getBancoFavorecido()));
            this.agenciaFavorecido = validate(Strings.isNullOrEmpty(fornecedor.getAgenciaFavorecido()));
            this.contaCorrenteFavorecido = validate(Strings.isNullOrEmpty(fornecedor.getContaFavorecido()));
        }
    }
}
