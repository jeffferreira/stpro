package br.com.juridico.validation.cadastros;

import javax.validation.constraints.Size;

import br.com.juridico.validation.ValidationWrapper;

public class TipoCustoValidacaoWrapper implements ValidationWrapper {

    @Size(min = 1, message = "Já existe um registro cadastrado com esta descrição.")
    private String descricao;

    public TipoCustoValidacaoWrapper(boolean tipoCustoJaEstaCadastrado) {
        this.descricao = validate(tipoCustoJaEstaCadastrado);
    }
}
