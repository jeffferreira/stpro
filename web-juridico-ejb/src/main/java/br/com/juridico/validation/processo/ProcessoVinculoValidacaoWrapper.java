package br.com.juridico.validation.processo;

import javax.validation.constraints.Size;

import br.com.juridico.entidades.ProcessoVinculo;
import br.com.juridico.validation.ValidationWrapper;

/**
 * Created by Marcos Melo.
 */
public class ProcessoVinculoValidacaoWrapper implements ValidationWrapper {

    private static final String VALIDO = "VALIDO";

    @Size(min = 1, message = "Favor selecionar o processo.")
    private String processo;

    public ProcessoVinculoValidacaoWrapper(ProcessoVinculo processoVinculo){
        this.processo = processoVinculo.getProcessoFilho() == null ? "" : VALIDO;
    }
}
