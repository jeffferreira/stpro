package br.com.juridico.validation.cadastros;

import br.com.juridico.validation.ValidationWrapper;

import javax.validation.constraints.Size;

public class PosicaoValidacaoWrapper implements ValidationWrapper {

    @Size(min = 1, message = "Já existe um registro cadastrado com esta descrição.")
    private String descricao;

    public PosicaoValidacaoWrapper(boolean posicaoJaEstaCadastrado) {
        this.descricao = validate(posicaoJaEstaCadastrado);
    }
}
