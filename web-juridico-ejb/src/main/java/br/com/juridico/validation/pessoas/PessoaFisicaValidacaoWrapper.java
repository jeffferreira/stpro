package br.com.juridico.validation.pessoas;

import br.com.juridico.entidades.PessoaFisica;
import br.com.juridico.validation.ValidationWrapper;
import com.google.common.base.Strings;

import javax.validation.constraints.Size;

/**
 *
 * @author Jefferson
 *
 */
public class PessoaFisicaValidacaoWrapper implements ValidationWrapper {

	@Size(min = 1, message = "Favor preencher o campo Nome.")
	private String nomePessoaFisica;
	@Size(min = 1, message = "Favor selecionar o campo Sexo.")
	private String sexo;
	@Size(min = 1, message = "Favor selecionar o campo Envia e-mail.")
	private String flagEnviaEmail;
	@Size(min = 1, message = "Favor selecionar o campo Cliente.")
	private String flagCliente;
	@Size(min = 1, message = "Favor adicionar pelo menos um email.")
	private String emailObrigatorio;
	@Size(min = 1, message = "Favor adicionar pelo menos um contato.")
	private String contatoObrigatorio;
	@Size(min = 1, message = "Favor adicionar pelo menos um endere\u00e7o.")
	private String enderecoObrigatorio;
	@Size(min = 1, message = "Favor informar o CPF.")
	private String CPFObrigatorio;


	/**
	 *
	 * @param pessoaFisica
	 */
	public PessoaFisicaValidacaoWrapper(PessoaFisica pessoaFisica) {
		this.nomePessoaFisica = validate(existePessoaVazia(pessoaFisica));
		this.sexo = validate(pessoaFisica.getSexo() == null);
		this.flagEnviaEmail = validate(pessoaFisica.getPessoa().getEnviaEmail() == null);
		this.flagCliente = validate(pessoaFisica.getPessoa().getCliente() == null);
		this.emailObrigatorio = validate(isClienteInterno(pessoaFisica) && isEmailVazio(pessoaFisica) && pessoaFisica.getPessoa().getPossuiEmail());
		this.contatoObrigatorio = validate(isClienteInterno(pessoaFisica) && isContatoVazio(pessoaFisica));
		this.enderecoObrigatorio = validate(isClienteInterno(pessoaFisica) && isEnderecoVazio(pessoaFisica));
		this.CPFObrigatorio = validate(isClienteInterno(pessoaFisica) && isObrigatorioCPF(pessoaFisica));
	}

	private boolean isObrigatorioCPF(PessoaFisica pessoaFisica) {
		return pessoaFisica.getCpf() == null || pessoaFisica.getCpf().isEmpty();
	}

	private boolean existePessoaVazia(PessoaFisica pessoaFisica) {
		return pessoaFisica.getPessoa() == null || Strings.isNullOrEmpty(pessoaFisica.getPessoa().getDescricao());
	}

	private boolean isClienteInterno(PessoaFisica pessoaFisica) {
		return pessoaFisica.getPessoa() != null && pessoaFisica.getPessoa().isClienteEmpresa();
	}

	private boolean isEmailVazio(PessoaFisica pessoaFisica) {
		return pessoaFisica.getPessoa().getPessoasEmails().isEmpty();
	}

	private boolean isContatoVazio(PessoaFisica pessoaFisica) {
		return pessoaFisica.getPessoa().getPessoasContatos().isEmpty();
	}

	private boolean isEnderecoVazio(PessoaFisica pessoaFisica) {
		return pessoaFisica.getPessoa().getPessoasEnderecos().isEmpty();
	}

}
