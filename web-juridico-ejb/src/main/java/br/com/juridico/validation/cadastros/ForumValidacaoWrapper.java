package br.com.juridico.validation.cadastros;

import br.com.juridico.entidades.Forum;
import br.com.juridico.validation.ValidationWrapper;
import com.google.common.base.Strings;

import javax.validation.constraints.Size;

public class ForumValidacaoWrapper implements ValidationWrapper {

    @Size(min = 1, message = "Descrição: preenchimento obrigatório")
    private String descricao;
    @Size(min = 1, message = "Já existe um registro cadastrado com esta descrição.")
    private String descricaoExistente;

    public ForumValidacaoWrapper(Forum forum, boolean forumJaEstaCadastrado) {
        this.descricao = validate(Strings.isNullOrEmpty(forum.getDescricao()));
        this.descricaoExistente = validate(forumJaEstaCadastrado);
    }
}
