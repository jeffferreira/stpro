package br.com.juridico.validation.pessoas;

import br.com.juridico.entidades.PessoaJuridica;
import br.com.juridico.entidades.PessoaJuridicaCentroCusto;
import br.com.juridico.validation.ValidationWrapper;

import javax.validation.constraints.Size;

/**
 *
 * @author Jefferson
 *
 */
public class PessoaJuridicaValidacaoWrapper implements ValidationWrapper {

	@Size(min = 1, message = "Favor preencher a raz\u00e3o social.")
	private String razaoSocial;
	@Size(min = 1, message = "Favor selecionar o campo Envia e-mail.")
	private String flagEnviaEmail;
	@Size(min = 1, message = "Favor selecionar o campo Cliente.")
	private String flagCliente;
	@Size(min = 1, message = "Por favor, selecione um centro de custo.")
	private String centroCusto;
	@Size(min = 1, message = "Favor adicionar pelo menos um email.")
	private String emailObrigatorio;
	@Size(min = 1, message = "Favor adicionar pelo menos um contato.")
	private String contatoObrigatorio;
	@Size(min = 1, message = "Favor adicionar pelo menos um endere\u00e7o.")
	private String enderecoObrigatorio;
	@Size(min = 1, message = "Favor informar o CNPJ.")
	private String CNPJObrigatorio;
	@Size(min = 1, message = "Favor informar a Atividade Principal.")
	private String atividadePrincipalObrigatoria;

	/**
	 *
	 * @param pessoaJuridica
	 */
	public PessoaJuridicaValidacaoWrapper(PessoaJuridica pessoaJuridica) {
		this.razaoSocial = validate(existeRazaoVazia(pessoaJuridica));
		this.flagEnviaEmail = validate(pessoaJuridica.getPessoa().getEnviaEmail() == null);
		this.flagCliente = validate(pessoaJuridica.getPessoa().getCliente() == null);
		this.centroCusto = validate(centroCustoVazio(pessoaJuridica));
		this.emailObrigatorio = validate(isClienteInterno(pessoaJuridica) && isEmailVazio(pessoaJuridica) && pessoaJuridica.getPessoa().getPossuiEmail());
		this.contatoObrigatorio = validate(isClienteInterno(pessoaJuridica) && isContatoVazio(pessoaJuridica));
		this.enderecoObrigatorio = validate(isClienteInterno(pessoaJuridica) && isEnderecoVazio(pessoaJuridica));
		this.CNPJObrigatorio = validate(isClienteInterno(pessoaJuridica) && isObrigatorioCNPJ(pessoaJuridica));
		this.atividadePrincipalObrigatoria = validate(isClienteInterno(pessoaJuridica) && isObrigatorioAtividadePrincipal(pessoaJuridica));
	}

	private boolean isObrigatorioAtividadePrincipal(PessoaJuridica p) {
		return p.getRamoAtividade() == null;
	}

	private boolean isObrigatorioCNPJ(PessoaJuridica p) {
		return p.getCnpj() == null || p.getCnpj().isEmpty();
	}

	private static boolean existeRazaoVazia(PessoaJuridica pessoaJuridica) {
		return pessoaJuridica.getPessoa() == null
				|| pessoaJuridica.getPessoa().getDescricao() == null
				|| pessoaJuridica.getPessoa().getDescricao().isEmpty();
	}

	private static boolean centroCustoVazio(PessoaJuridica pessoaJuridica) {
		if (pessoaJuridica.getPessoasJuridicasCentrosCustos() == null)
			return Boolean.FALSE;
		for (PessoaJuridicaCentroCusto pessoaJuridicaCentroCusto : pessoaJuridica.getPessoasJuridicasCentrosCustos()) {
			if (pessoaJuridicaCentroCusto.getCentroCusto() == null)
				return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	private boolean isClienteInterno(PessoaJuridica pessoaJuridica) {
		return pessoaJuridica.getPessoa() != null && pessoaJuridica.getPessoa() != null && pessoaJuridica.getPessoa().isClienteEmpresa();
	}

	private boolean isEmailVazio(PessoaJuridica pessoaJuridica) {
		return pessoaJuridica.getPessoa().getPessoasEmails().isEmpty();
	}

	private boolean isContatoVazio(PessoaJuridica pessoaJuridica) {
		return pessoaJuridica.getPessoa().getPessoasContatos().isEmpty();
	}

	private boolean isEnderecoVazio(PessoaJuridica pessoaJuridica) {
		return pessoaJuridica.getPessoa().getPessoasEnderecos().isEmpty();
	}

}
