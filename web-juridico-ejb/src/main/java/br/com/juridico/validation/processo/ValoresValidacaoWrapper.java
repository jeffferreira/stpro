package br.com.juridico.validation.processo;

import br.com.juridico.entidades.Processo;
import br.com.juridico.entidades.ProcessoPedido;
import br.com.juridico.validation.ValidationWrapper;

import javax.validation.constraints.Size;
import java.util.List;

/**
 * Created by jeffersonl2009_00 on 08/07/2016.
 */
public class ValoresValidacaoWrapper implements ValidationWrapper {

    @Size(min = 1, message = "Favor selecionar o Grau de Risco.")
    private String grauDeRisco;
    @Size(min = 1, message = "Favor preencher o Valor da Causa.")
    private String valorDaCausa;
    @Size(min = 1, message = "Favor preencher o Valor Provável.")
    private String valorProvavel;
    @Size(min = 1, message = "Favor selecionar o Pedido.")
    private String pedido;
    @Size(min = 1, message = "Favor selecionar a Causa do Pedido.")
    private String causaPedido;

    public ValoresValidacaoWrapper(Processo processo){
        this.grauDeRisco = validate(processo.getGrauRisco() == null);
        this.valorDaCausa = validate(processo.getValorCausa() == null);
        this.valorProvavel = validate(processo.getValorProvavel() == null);
        this.pedido = validate(existePedidoVazio(processo.getPedidos()));
        this.causaPedido = validate(existeCausaPedidoVazio(processo.getPedidos()));
    }

    private boolean existeCausaPedidoVazio(final List<ProcessoPedido> pedidos) {
        if(pedidos == null || pedidos.isEmpty()) {
            return Boolean.TRUE;
        }
        for (ProcessoPedido pedido : pedidos) {
            if (pedido == null || pedido.getCausaPedido() == null) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    private boolean existePedidoVazio(final List<ProcessoPedido> pedidos) {
        if(pedidos == null || pedidos.isEmpty()) {
            return Boolean.TRUE;
        }
        for (ProcessoPedido pedido : pedidos) {
            if (pedido == null || pedido.getPedido() == null) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }
}
