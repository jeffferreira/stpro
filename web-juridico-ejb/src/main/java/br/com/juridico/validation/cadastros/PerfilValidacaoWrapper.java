package br.com.juridico.validation.cadastros;

import br.com.juridico.validation.ValidationWrapper;

import javax.validation.constraints.Size;

public class PerfilValidacaoWrapper implements ValidationWrapper {

    @Size(min = 1, message = "Já existe um registro cadastrado com esta descrição.")
    private String descricao;
    @Size(min = 1, message = "Já existe um registro cadastrado com esta sigla.")
    private String sigla;

    public PerfilValidacaoWrapper(boolean perfilJaEstaCadastrado, boolean existeSiglaPerfilCadastrado) {
        this.descricao = validate(perfilJaEstaCadastrado);
        this.sigla = validate(existeSiglaPerfilCadastrado);
    }
}
