package br.com.juridico.validation.processo;

import br.com.juridico.entidades.Processo;
import br.com.juridico.validation.ValidationWrapper;
import com.google.common.base.Strings;

import javax.validation.constraints.Size;

/**
 * Created by jeffersonl2009_00 on 25/07/2016.
 */
public class ResumoValidacaoWrapper implements ValidationWrapper {

    @Size(min = 1, message = "Favor preencher o resumo da ação.")
    private String resumo;

    public ResumoValidacaoWrapper(Processo processo) {
        this.resumo = validate(Strings.isNullOrEmpty(processo.getObservacao()));
    }
}
