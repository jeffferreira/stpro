package br.com.juridico.validation.relatorios;

import br.com.juridico.entidades.RelatorioCriterioProcesso;
import br.com.juridico.entidades.RelatorioFiltroProcesso;
import br.com.juridico.validation.ValidationWrapper;

import javax.validation.constraints.Size;

/**
 * 
 * @author Marcos
 *
 */
public class RelatorioFiltroProcessoValidacaoWrapper implements ValidationWrapper {

	@Size(min = 1, message = "Favor preencher o campo Descrição do relatório.")
	private String descricao;
	@Size(min = 1, message = "Favor selecionar uma opção para cada um dos Critérios ativos.")
	private String criterios;

	/**
	 *
	 * @param relatorioFiltroProcesso
     */
	public RelatorioFiltroProcessoValidacaoWrapper(RelatorioFiltroProcesso relatorioFiltroProcesso) {
		this.descricao = validate(relatorioFiltroProcesso.getDescricao() == null || relatorioFiltroProcesso.getDescricao().isEmpty());
		this.criterios = validate(validateCriteriosAtivosSelecionados(relatorioFiltroProcesso));
	}

	private boolean validateCriteriosAtivosSelecionados(RelatorioFiltroProcesso relatorioFiltroProcesso) {
		for (RelatorioCriterioProcesso criterio : relatorioFiltroProcesso.getCriterios()) {
			if (criterio.getFiltroAtivo() == null  || (criterio.getFiltroAtivo() && criterio.getCriterioType() == null)) {
			    return Boolean.TRUE;
            }
		}
        return Boolean.FALSE;
	}
}
