package br.com.juridico.validation.processo;

import br.com.juridico.entidades.Custa;
import br.com.juridico.validation.ValidationWrapper;

import javax.validation.constraints.Size;

/**
 * Created by jeffersonl2009_00 on 21/06/2016.
 */
public class CustaValidacaoWrapper implements ValidationWrapper {

    private static final String VALIDO = "VALIDO";

    @Size(min = 1, message = "Favor preencher o responsável.")
    private String responsavel;
    @Size(min = 1, message = "Favor preencher o tipo do custo.")
    private String tipoCusta;
    @Size(min = 1, message = "Favor preencher o valor.")
    private String valor;
    @Size(min = 1, message = "Favor preencher forma do pagamento.")
    private String formaPagamento;
    @Size(min = 1, message = "Favor preencher data do pagamento.")
    private String dataPagamento;

    public CustaValidacaoWrapper(Custa custa){
        this.responsavel = custa.getResponsavelPagamento() == null ? "" : VALIDO;
        this.tipoCusta = custa.getTipoCusto() == null ? "" : VALIDO;
        this.valor = custa.getValorCusta() == null ? "" : VALIDO;
        this.formaPagamento = custa.getFormaPagamento() == null ? "" : VALIDO;
        this.dataPagamento = custa.getDataPagamento() == null ? "" : VALIDO;
    }
}
