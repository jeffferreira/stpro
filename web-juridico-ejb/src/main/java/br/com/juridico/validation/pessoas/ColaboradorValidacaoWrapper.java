package br.com.juridico.validation.pessoas;

import javax.validation.constraints.Size;

import br.com.juridico.entidades.PessoaFisica;
import br.com.juridico.validation.ValidationWrapper;

import com.google.common.base.Strings;

/**
 *
 * @author Marcos
 *
 */
public class ColaboradorValidacaoWrapper implements ValidationWrapper {

	@Size(min = 1, message = "Favor preencher o campo Nome.")
	private String nomePessoaFisica;
	@Size(min = 1, message = "Favor selecionar o campo Sexo.")
	private String sexo;
	@Size(min = 1, message = "Favor selecionar um Perfil.")
	private String perfil;
	@Size(min = 1, message = "Favor selecionar uma Função.")
	private String funcao;
	@Size(min = 1, message = "Favor selecionar o campo Envia e-mail.")
	private String flagEnviaEmail;
	@Size(min = 1, message = "O N\u00famero de OAB fornecido j\u00e1 est\u00e1 sendo utilizado por outro advogado ou colaborador. Por favor, forne\u00e7a outro N\u00famero OAB.")
	private String numeroOabCadastrado;
	@Size(min = 1, message = "UF da OAB obrigatória.")
	private String ufOabObrigatoria;
	@Size(min = 1, message = "Favor adicionar pelo menos um email.")
	private String emailObrigatorio;
	@Size(min = 1, message = "Favor adicionar pelo menos um contato.")
	private String contatoObrigatorio;
	@Size(min = 1, message = "Favor adicionar pelo menos um endere\u00e7o.")
	private String enderecoObrigatorio;
	@Size(min = 1, message = "Favor informar o CPF.")
	private String CPFObrigatorio;

	/**
	 *
	 * @param pessoaFisica
	 * @param existeNumeroOabCadastrado
	 */
	public ColaboradorValidacaoWrapper(PessoaFisica pessoaFisica,
			boolean existeNumeroOabCadastrado) {
		this.nomePessoaFisica = validate(existePessoaVazia(pessoaFisica));
		this.sexo = validate(pessoaFisica.getSexo() == null);
		this.perfil = validate(isColaboradorEPerfilNulo(pessoaFisica));
		this.funcao = validate(isColaboradorEFuncaoNula(pessoaFisica));
		this.flagEnviaEmail = validate(isCampoEnvialEmailNulo(pessoaFisica));
		this.numeroOabCadastrado = validate(existeNumeroOabCadastrado);
		this.ufOabObrigatoria = validate(validaUfObrigatoria(pessoaFisica));
		this.emailObrigatorio = validate(isEmailVazio(pessoaFisica));
		this.contatoObrigatorio = validate(isContatoVazio(pessoaFisica));
		this.enderecoObrigatorio = validate(isEnderecoVazio(pessoaFisica));
		this.CPFObrigatorio = validate(isObrigatorioCPF(pessoaFisica));
	}

	private boolean validaUfObrigatoria(PessoaFisica pessoaFisica) {
		return pessoaFisica.getNrOab() != null 
				&& !pessoaFisica.getNrOab().isEmpty()
				&& pessoaFisica.getUfOab() == null;
	}

	private boolean isObrigatorioCPF(PessoaFisica pessoaFisica) {
		return pessoaFisica.getCpf() == null || pessoaFisica.getCpf().isEmpty();
	}

	private boolean isColaboradorEPerfilNulo(PessoaFisica p) {
		return p.isColaboradorEscritorio() && p.getPessoa().getPerfil() == null;
	}

	private boolean isColaboradorEFuncaoNula(PessoaFisica p) {
		return p.isColaboradorEscritorio() && p.getFuncao() == null;
	}

	private boolean existePessoaVazia(final PessoaFisica pessoaFisica) {
		if (pessoaFisica.getPessoa() == null
				|| Strings.isNullOrEmpty(pessoaFisica.getPessoa()
						.getDescricao())) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	private boolean isEmailVazio(PessoaFisica pessoaFisica) {
		if (pessoaFisica.getPessoa().getPessoasEmails().isEmpty()) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	private boolean isContatoVazio(PessoaFisica pessoaFisica) {
		if (pessoaFisica.getPessoa().getPessoasContatos().isEmpty()) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	private boolean isEnderecoVazio(PessoaFisica pessoaFisica) {
		if (pessoaFisica.getPessoa().getPessoasEnderecos().isEmpty()) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	private boolean isCampoEnvialEmailNulo(final PessoaFisica pessoaFisica) {
		if (pessoaFisica.getPessoa() != null
				&& pessoaFisica.getPessoa().getEnviaEmail() == null) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

}
