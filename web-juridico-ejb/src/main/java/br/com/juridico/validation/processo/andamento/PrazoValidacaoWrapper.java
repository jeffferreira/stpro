package br.com.juridico.validation.processo.andamento;

import br.com.juridico.entidades.Andamento;
import br.com.juridico.entidades.PrazoAndamento;
import br.com.juridico.validation.ValidationWrapper;

import javax.validation.constraints.Size;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by jeffersonl2009_00 on 30/08/2016.
 */
public class PrazoValidacaoWrapper implements ValidationWrapper {

    @Size(min = 1, message = "Favor preencher a data final do prazo.")
    private String dataFinalPrazo;
    @Size(min = 1, message = "Favor preencher o sub andamento.")
    private String subAndamento;
    @Size(min = 1, message = "Favor preencher status.")
    private String prazoAtendido;
    @Size(min = 1, message = "Favor preencher envia email de aviso.")
    private String emailAviso;
    @Size(min = 1, message = "Favor preencher dias para o aviso.")
    private String diasAviso;
    @Size(min = 1, message = "Data final do prazo deve ser posterior à data inicial.")
    private String datasValidas;
    @Size(min = 1, message = "Data e hora final do prazo deve ser posterior à data e hora inicial.")
    private String datasHorasValidas;


    public PrazoValidacaoWrapper(Andamento andamento){
        if(andamento.getDefinePrazoFinal()) {
            this.dataFinalPrazo = validate(andamento.getDataFinalPrazoFatal() == null);
            validaPrazos(andamento);
        }
    }

    private void validaPrazos(Andamento andamento) {
        for (PrazoAndamento prazoAndamento : andamento.getPrazos()) {
            this.subAndamento = validate(prazoAndamento.getSubAndamento() == null);
            this.prazoAtendido = validate(prazoAndamento.getStatusAgenda() == null);
            this.emailAviso = validate(prazoAndamento.getEnviaEmailAviso() == null);
            validaDiasAviso(prazoAndamento);
            validaDatasPrazo(prazoAndamento);
            validaDatasHorasPrazo(prazoAndamento);
        }
    }

    private void validaDatasHorasPrazo(PrazoAndamento prazoAndamento) {
		if (!prazoAndamento.getHorarioIndefinido()
				&& datasIguais(prazoAndamento)
				&& horasPreenchidas(prazoAndamento)
				&& horaInicialMaiorQueFinal(prazoAndamento)) {
			this.datasHorasValidas = "";
			return;
		}
		this.datasHorasValidas = VALIDO;
	}

	private boolean horasPreenchidas(PrazoAndamento prazoAndamento) {
		return !prazoAndamento.getHoraInicio().isEmpty() && !prazoAndamento.getHoraFim().isEmpty();
	}

	private boolean horaInicialMaiorQueFinal(PrazoAndamento prazoAndamento) {
		SimpleDateFormat formatador = new SimpleDateFormat("HH:mm");  
		try {
			Date horaInicial = formatador.parse(prazoAndamento.getHoraInicio());
			Date horaFinal = formatador.parse(prazoAndamento.getHoraFim());
			
			if(horaInicial.getTime() > horaFinal.getTime()) {
				return Boolean.TRUE;
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return false;
	}

	private boolean datasIguais(PrazoAndamento prazoAndamento) {
		return datasPreenchidas(prazoAndamento) && prazoAndamento.getDataInicioPrazo().equals(prazoAndamento.getDataFinalPrazo());
	}

	private void validaDatasPrazo(PrazoAndamento prazoAndamento) {
        if(datasPreenchidas(prazoAndamento) && prazoAndamento.getDataInicioPrazo().after(prazoAndamento.getDataFinalPrazo())) {
        	this.datasValidas = "";
        	return;
        }
        this.datasValidas = VALIDO;
	}

	private boolean datasPreenchidas(PrazoAndamento prazoAndamento) {
		return prazoAndamento.getDataInicioPrazo() != null && prazoAndamento.getDataFinalPrazo() != null;
	}

	private void validaDiasAviso(PrazoAndamento prazoAndamento) {
        if(prazoAndamento.getEnviaEmailAviso() != null && prazoAndamento.getEnviaEmailAviso()) {
            this.diasAviso = validate(prazoAndamento.getQuantidadeDiasEmailAvisoAntecedencia() == null);
            return;
        }
        this.diasAviso = VALIDO;
    }
}
