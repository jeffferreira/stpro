package br.com.juridico.validation.cadastros;

import br.com.juridico.validation.ValidationWrapper;

import javax.validation.constraints.Size;

public class ProjudiprLoginValidacaoWrapper implements ValidationWrapper {

    @Size(min = 1, message = "Já existe um registro cadastrado para empresa.")
    private String descricao;

    public ProjudiprLoginValidacaoWrapper(boolean loginJaEstaCadastrado) {
        this.descricao = validate(loginJaEstaCadastrado);
    }
}
