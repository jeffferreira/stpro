package br.com.juridico.validation.cadastros;

import javax.validation.constraints.Size;

import br.com.juridico.validation.ValidationWrapper;

/**
 * Created by Marcos Melo on 15/12/2016.
 */
public class RamoAtividadeValidacaoWrapper implements ValidationWrapper {

    @Size(min = 1, message = "Já existe um registro cadastrado com esta descrição.")
    private String descricao;

    public RamoAtividadeValidacaoWrapper(boolean ramoAtividadeJaEstaCadastrada) {
        this.descricao = validate(ramoAtividadeJaEstaCadastrada);
    }

}
