package br.com.juridico.validation.cadastros;

import br.com.juridico.entidades.Grupo;
import br.com.juridico.validation.ValidationWrapper;

import javax.validation.constraints.Size;

public class GrupoValidacaoWrapper implements ValidationWrapper {

    @Size(min = 1, message = "Já existe um registro cadastrado com esta descrição.")
    private String descricao;
    @Size(min = 1, message = "Informe ao menos um advogado para o grupo.")
    private String advogados;

    public GrupoValidacaoWrapper(Grupo grupo, boolean grupoJaEstaCadastrado) {
        this.descricao = validate(grupoJaEstaCadastrado);
        this.advogados = validate(grupo.getGruposAdvogados().isEmpty());
    }
}
