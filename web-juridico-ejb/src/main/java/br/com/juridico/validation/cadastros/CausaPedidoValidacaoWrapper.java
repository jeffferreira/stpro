package br.com.juridico.validation.cadastros;

import br.com.juridico.entidades.CausaPedido;
import br.com.juridico.validation.ValidationWrapper;
import com.google.common.base.Strings;

import javax.validation.constraints.Size;

public class CausaPedidoValidacaoWrapper implements ValidationWrapper {

    @Size(min = 1, message = "Descrição: preenchimento obrigatório")
    private String descricao;
    @Size(min = 1, message = "Já existe um registro cadastrado com esta descrição.")
    private String descricaoExistente;

    public CausaPedidoValidacaoWrapper(CausaPedido causaPedido, boolean causaPedidoJaEstaCadastrado) {
        this.descricao = validate(Strings.isNullOrEmpty(causaPedido.getDescricao()));
        this.descricaoExistente = validate(causaPedidoJaEstaCadastrado);
    }
}
