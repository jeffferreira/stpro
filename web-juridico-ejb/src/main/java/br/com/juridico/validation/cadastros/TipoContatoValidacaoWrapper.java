package br.com.juridico.validation.cadastros;

import br.com.juridico.validation.ValidationWrapper;

import javax.validation.constraints.Size;

public class TipoContatoValidacaoWrapper implements ValidationWrapper {

    @Size(min = 1, message = "Já existe um registro cadastrado com esta descrição.")
    private String descricao;

    public TipoContatoValidacaoWrapper(boolean tipoContatoJaEstaCadastrado) {
        this.descricao = validate(tipoContatoJaEstaCadastrado);
    }
}
