package br.com.juridico.validation.processo;

import br.com.juridico.dto.ParteDto;
import br.com.juridico.entidades.ProcessoPessoaJuridicaCentroCusto;
import br.com.juridico.validation.ValidationWrapper;
import com.google.common.base.Strings;

import javax.validation.constraints.Size;
import java.math.BigDecimal;

/**
 * Created by jeffersonl2009_00 on 08/07/2016.
 */
public class ParteValidationWrapper implements ValidationWrapper {

    private static final BigDecimal CEM = BigDecimal.valueOf(100.00).setScale(2);
    private static final String PESSOA_FISICA = "PF";

    @Size(min = 1, message = "Favor selecionar o Tipo.")
    private String tipo;
    @Size(min = 1, message = "Favor preencher o campo Nome.")
    private String nomeCliente;
    @Size(min = 1, message = "Favor selecionar a Raz\u00e3o Social.")
    private String razaoSocial;
    @Size(min = 1, message = "Favor selecionar a Posi\u00e7\u00e3o.")
    private String posicaoCliente;
    @Size(min = 1, message = "Favor selecionar Advogado/Grupo.")
    private String advogadoGrupo;
    @Size(min = 1, message = "A soma dos percentuais dos Centros de Custo deve ser de 100%. Por favor, informe corretamente.")
    private String porcentagem;
    @Size(min = 1, message = "Favor selecionar Advogado(s).")
    private String advogadoIndividual;
    @Size(min = 1, message = "Favor selecionar o Grupo de advogados")
    private String grupoAdvogado;

    /**
     *
     * @param parte
     */
    public ParteValidationWrapper(ParteDto parte) {
        this.tipo = validate(Strings.isNullOrEmpty(parte.getTipoPessoa()));
        this.posicaoCliente = validate(parte.getPosicao() == null);
        this.advogadoGrupo = validate(Strings.isNullOrEmpty(parte.getTipoAdvogado()));
        validaPessoaFisicaOuJuridica(parte);
        validaAdvogadosIndividuaisOuGrupo(parte);
    }

    private void validaAdvogadosIndividuaisOuGrupo(ParteDto parte) {
        if(!parte.getPessoa().isClienteEmpresa() && parte.isTipoAdvogadoIndividual()){
            this.advogadoIndividual = VALIDO;
            return;
        }

        if(Strings.isNullOrEmpty(parte.getTipoAdvogado())) {
            this.advogadoIndividual = "";
            return;
        }
        this.advogadoIndividual = validate(existeAdvogadoIndividualVazio(parte));
        this.grupoAdvogado = validate(existeGrupoVazio(parte));
    }

    private void validaPessoaFisicaOuJuridica(ParteDto parte) {
        if(parte.getTipoPessoa() == null || PESSOA_FISICA.equals(parte.getTipoPessoa())) {
            this.nomeCliente = validate(isPessoaVazioOuNulo(parte));
            this.razaoSocial = VALIDO;
            return;
        }
        this.razaoSocial = validate(isPessoaVazioOuNulo(parte));
        this.porcentagem = validate(percentualDiferenteDe100(parte));
        this.nomeCliente = VALIDO;
    }

    private boolean isPessoaVazioOuNulo(ParteDto parte) {
        return parte.getPessoa() == null || Strings.isNullOrEmpty(parte.getPessoa().getDescricao());
    }

    private boolean existeAdvogadoIndividualVazio(ParteDto parte) {
        if (parte.isTipoAdvogadoIndividual() && parte.getSelectedAdvogados().size() == 0) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    private boolean existeGrupoVazio(ParteDto parte) {
        if (!parte.isTipoAdvogadoIndividual() && parte.getGrupo() == null) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    private boolean percentualDiferenteDe100(ParteDto parteDto) {
        if(parteDto.getParte().getCentroCustos().isEmpty()){
            return Boolean.FALSE;
        }
        BigDecimal total = BigDecimal.ZERO.setScale(2);
        for (ProcessoPessoaJuridicaCentroCusto cc : parteDto.getParte().getCentroCustos()) {
            BigDecimal percentual = cc.getPercentual() == null ? BigDecimal.ZERO : cc.getPercentual();
            total = total.add(percentual);
        }
        return total.equals(CEM) ? Boolean.FALSE : Boolean.TRUE;
    }
}
