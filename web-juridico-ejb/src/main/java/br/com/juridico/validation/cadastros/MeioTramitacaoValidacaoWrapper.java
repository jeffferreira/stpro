package br.com.juridico.validation.cadastros;

import br.com.juridico.entidades.MeioTramitacao;
import br.com.juridico.validation.ValidationWrapper;
import com.google.common.base.Strings;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

public class MeioTramitacaoValidacaoWrapper implements ValidationWrapper {

    @Size(min = 1, message = "Descrição: preenchimento obrigatório")
    private String descricao;
    @Size(min = 1, message = "Já existe um registro cadastrado com esta descrição.")
    private String descricaoExistente;

    public MeioTramitacaoValidacaoWrapper(MeioTramitacao meioTramitacao, boolean meioTramitacaoJaEstaCadastrado) {
        this.descricao = validate(Strings.isNullOrEmpty(meioTramitacao.getDescricao()));
        this.descricaoExistente = validate(meioTramitacaoJaEstaCadastrado);
    }
}
