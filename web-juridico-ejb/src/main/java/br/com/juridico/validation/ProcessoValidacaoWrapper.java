package br.com.juridico.validation;

import br.com.juridico.dto.CentroCustoDto;
import br.com.juridico.dto.ParteDto;
import br.com.juridico.dto.PedidoDto;
import br.com.juridico.entidades.*;

import javax.validation.constraints.Size;

import java.math.BigDecimal;
import java.util.List;

/**
 * 
 * @author Jefferson
 *
 */
public class ProcessoValidacaoWrapper {

	private static final String VALIDO = "VALIDO";
	private static final BigDecimal CEM = BigDecimal.valueOf(100.00).setScale(2);

	@Size(min = 1, message = "Nome")
	private String nomeCliente;
    @Size(min = 1, message = "Razão Social")
    private String razaoSocial;
	@Size(min = 1, message = "Posição")
	private String posicaoCliente;
	@Size(min = 1, message = "A soma dos percentuais dos Centros de Custo deve ser de 100%. Por favor, informe corretamente.")
	private String porcentagem;
	@Size(min = 1, message = "Advogado(s)")
	private String advogadoIndividual;
	@Size(min = 1, message = "Grupo de advogados")
	private String grupoAdvogado;
	@Size(min = 1, message = "Favor preencher a data de abertura.")
	private String dataAbertura;
	@Size(min = 1, message = "Favor preencher a descrição do processo.")
	private String descricaoProcesso;
	@Size(min = 1, message = "Favor selecionar o meio de tramitação.")
	private String meioTramitacao;
	@Size(min = 1, message = "Favor selecionar a fase do processo.")
	private String faseProcesso;
	@Size(min = 1, message = "Favor selecionar a classe processual.")
	private String classeProcessual;
	@Size(min = 1, message = "Favor selecionar a natureza da ação.")
	private String naturezaAcao;
	@Size(min = 1, message = "Favor preencher o fórum.")
	private String forum;
	@Size(min = 1, message = "Favor preencher a comarca.")
	private String comarca;
	@Size(min = 1, message = "Favor preencher a vara.")
	private String vara;
	@Size(min = 1, message = "Favor selecionar o grau de risco.")
	private String grauDeRisco;
	@Size(min = 1, message = "Favor preencher o valor da causa.")
	private String valorDaCausa;
	@Size(min = 1, message = "Favor preencher o valor provável.")
	private String valorProvavel;
	@Size(min = 1, message = "Favor selecionar o pedido.")
	private String pedido;
	@Size(min = 1, message = "Favor selecionar a causa do pedido.")
	private String causaPedido;

	/**
	 *
	 * @param p
	 * @param parte
	 */
    public ProcessoValidacaoWrapper(Processo p, ParteDto parte) {
        if("PJ".equals(parte.getTipoPessoa())){
            Pessoa pessoaJuridica = parte.getPessoa() == null ? null : parte.getPessoa();
            this.razaoSocial = existePessoaVazia(pessoaJuridica) ? "" : VALIDO;
            this.porcentagem = percentualDiferenteDe100(parte) ? "" : VALIDO;
            this.nomeCliente = VALIDO;
        } else {
            Pessoa pessoaFisica = parte.getPessoa() == null ? null : parte.getPessoa();
            this.nomeCliente = existePessoaVazia(pessoaFisica) ? "" : VALIDO;
            this.razaoSocial = VALIDO;
        }

        this.posicaoCliente = existePosicaoVazia(parte.getPosicao()) ? "" : VALIDO;
        this.advogadoIndividual = existeAdvogadoIndividualVazio(parte) ? "" : VALIDO;
        this.grupoAdvogado = existeGrupoVazio(parte) ? "" : VALIDO;
    }

	public ProcessoValidacaoWrapper(Processo p, List<PedidoDto> pedidos) {
		this.dataAbertura = p.getDataAbertura() == null ? "" : VALIDO;
		this.descricaoProcesso = p.getDescricao().isEmpty() ? "" : VALIDO;
		this.meioTramitacao = p.getMeioTramitacao() == null ? "" : VALIDO;
		this.faseProcesso = p.getFase() == null ? "" : VALIDO;
		this.classeProcessual = p.getClasseProcessual() == null ? "" : VALIDO;
		this.naturezaAcao = p.getNaturezaAcao() == null ? "" : VALIDO;
		this.forum = p.getForum() == null ? "" : VALIDO;
		this.comarca = p.getComarca() == null ? "" : VALIDO;
		this.vara = p.getVara() == null ? "" : VALIDO;
		this.grauDeRisco = p.getGrauRisco() == null ? "" : VALIDO;
		this.valorDaCausa = p.getValorCausa() == null ? "" : VALIDO;
		this.valorProvavel = p.getValorProvavel() == null ? "" : VALIDO;
		this.pedido = existePedidoVazio(pedidos) ? "" : VALIDO;
		this.causaPedido = existeCausaPedidoVazio(pedidos) ? "" : VALIDO;

	}

	private static boolean existeCausaPedidoVazio(List<PedidoDto> pedidos) {
		boolean existeCausaPedidoVazio = Boolean.FALSE;

		for (PedidoDto pedidoDto : pedidos) {
			if (pedidoDto.getCausaPedido() == null) {
				existeCausaPedidoVazio = Boolean.TRUE;
			}
		}
		return existeCausaPedidoVazio;
	}

	private static boolean existePedidoVazio(List<PedidoDto> pedidos) {
		boolean existePedidoVazio = Boolean.FALSE;

		for (PedidoDto pedidoDto : pedidos) {
			if (pedidoDto.getPedido() == null) {
				existePedidoVazio = Boolean.TRUE;
			}
		}
		return existePedidoVazio;
	}

    private static boolean existeGrupoVazio(ParteDto parte) {
        boolean existePessoaVazia = Boolean.FALSE;

        if (!parte.isTipoAdvogadoIndividual() && parte.getGrupo() == null) {
            existePessoaVazia = Boolean.TRUE;
        }
        return existePessoaVazia;
    }

	private static boolean existeAdvogadoIndividualVazio(ParteDto parte) {
        if (!parte.isTipoAdvogadoIndividual() || parte.isTipoAdvogadoIndividual() && parte.getSelectedAdvogados().size() > 0) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
	}

	private static boolean existePessoaVazia(Pessoa pessoa) {
        if(pessoa == null) return Boolean.TRUE;

		return Boolean.FALSE;
	}

	private static boolean existePosicaoVazia(Posicao posicao) {
        if(posicao == null) return Boolean.TRUE;

		return Boolean.FALSE;
	}

	private boolean percentualDiferenteDe100(ParteDto parte) {
		BigDecimal total = BigDecimal.ZERO.setScale(2);
		for (CentroCustoDto centroCustoDto : parte.getCentroCustos()) {
			total = total.add(centroCustoDto.getPercentual());
		}

		return total.equals(CEM) ? Boolean.FALSE : Boolean.TRUE;
	}


}
