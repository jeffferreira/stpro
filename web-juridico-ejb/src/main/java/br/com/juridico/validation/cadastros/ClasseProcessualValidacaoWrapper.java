package br.com.juridico.validation.cadastros;

import br.com.juridico.entidades.ClasseProcessual;
import br.com.juridico.validation.ValidationWrapper;
import com.google.common.base.Strings;

import javax.validation.constraints.Size;

public class ClasseProcessualValidacaoWrapper implements ValidationWrapper {

    @Size(min = 1, message = "Descrição: preenchimento obrigatório")
    private String descricao;
    @Size(min = 1, message = "Já existe um registro cadastrado com esta descrição.")
    private String descricaoExistente;

    public ClasseProcessualValidacaoWrapper(ClasseProcessual classeProcessual, boolean classeProcessualJaEstaCadastrado) {
        this.descricao = validate(Strings.isNullOrEmpty(classeProcessual.getDescricao()));
        this.descricaoExistente = validate(classeProcessualJaEstaCadastrado);
    }
}
