package br.com.juridico.validation;

/**
 * Created by Jefferson on 07/07/2016.
 */
public interface ValidationWrapper {

    String VALIDO = "VALIDO";

    /**
     *
     * @param campoVazioNulo
     * @return
     */
    default String validate(boolean campoVazioNulo) {
        return campoVazioNulo ? "" : VALIDO;
    }
}
