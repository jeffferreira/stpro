package br.com.juridico.validation.processo;

import br.com.juridico.entidades.Processo;
import br.com.juridico.validation.ValidationWrapper;
import com.google.common.base.Strings;

import javax.validation.constraints.Size;

/**
 * Created by jeffersonl2009_00 on 08/07/2016.
 */
public class DadosProcessoValidacaoWrapper implements ValidationWrapper {

    @Size(min = 1, message = "Favor preencher o campo Data de Abertura.")
    private String dataAbertura;
    @Size(min = 1, message = "Favor preencher o campo Processo.")
    private String descricaoProcesso;
    @Size(min = 1, message = "Favor selecionar o Meio de Tramita\u00e7\u00e3o.")
    private String meioTramitacao;
    @Size(min = 1, message = "Favor selecionar a Fase do Processo.")
    private String faseProcesso;
    @Size(min = 1, message = "Favor selecionar a Classe Processual.")
    private String classeProcessual;
    @Size(min = 1, message = "Favor selecionar a Natureza da A\u00e7\u00e3o.")
    private String naturezaAcao;
    @Size(min = 1, message = "Favor selecionar o F\u00f3rum.")
    private String forum;
    @Size(min = 1, message = "Favor selecionar a Comarca.")
    private String comarca;
    @Size(min = 1, message = "Favor selecionar a Vara.")
    private String vara;

    public DadosProcessoValidacaoWrapper(Processo processo){
        this.dataAbertura = validate(processo.getDataAbertura() == null);
        this.descricaoProcesso = validate(Strings.isNullOrEmpty(processo.getDescricao()));
        this.comarca = validate(processo.getComarca() == null);
        this.meioTramitacao = validate(processo.getMeioTramitacao() == null);
        this.naturezaAcao = validate(processo.getNaturezaAcao() == null);

        if(processo.isPermiteCadastroComGeracaoAutomatica()){
            this.faseProcesso = VALIDO;
            this.classeProcessual = VALIDO;
            this.forum = VALIDO;
            this.vara = VALIDO;
            return;
        }

        this.faseProcesso = validate(processo.getFase() == null);
        this.classeProcessual = validate(processo.getClasseProcessual() == null);
        this.forum = validate(processo.getForum() == null);
        this.vara = validate(processo.getVara() == null);
    }
}
