package br.com.juridico.validation.pessoas;

import javax.validation.constraints.Size;

import br.com.juridico.entidades.PessoaFisica;
import br.com.juridico.validation.ValidationWrapper;

import com.google.common.base.Strings;

/**
 *
 * @author Jefferson
 *
 */
public class AdvogadoValidacaoWrapper implements ValidationWrapper {

	@Size(min = 1, message = "Favor preencher o campo Nome.")
	private String nomePessoaFisica;
	@Size(min = 1, message = "Favor selecionar o campo Sexo.")
	private String sexo;
	@Size(min = 1, message = "Favor selecionar o campo Advogado Interno.")
	private String advogadoInterno;
	@Size(min = 1, message = "Favor selecionar um Perfil.")
	private String perfil;
	@Size(min = 1, message = "Favor selecionar o campo Envia e-mail.")
	private String flagEnviaEmail;
	@Size(min = 1, message = "Favor preencher o campo N\u00famero OAB.")
	private String numeroOabAdvogado;
	@Size(min = 1, message = "Favor selecionar o campo UF OAB.")
	private String ufOab;
	@Size(min = 1, message = "O N\u00famero de OAB fornecido j\u00e1 est\u00e1 sendo utilizado por outro advogado. Por favor, forne\u00e7a outro N\u00famero OAB.")
	private String numeroOabCadastrado;
	@Size(min = 1, message = "Favor adicionar pelo menos um email.")
	private String emailObrigatorio;
	@Size(min = 1, message = "Favor adicionar pelo menos um contato.")
	private String contatoObrigatorio;
	@Size(min = 1, message = "Favor adicionar pelo menos um endere\u00e7o.")
	private String enderecoObrigatorio;
	@Size(min = 1, message = "Favor informar o CPF.")
	private String CPFObrigatorio;

	/**
	 *
	 * @param pessoaFisica
	 * @param existeNumeroOabCadastrado
	 */
	public AdvogadoValidacaoWrapper(PessoaFisica pessoaFisica, boolean existeNumeroOabCadastrado) {
		this.nomePessoaFisica = validate(existePessoaVazia(pessoaFisica));
		this.numeroOabAdvogado = validate(Strings.isNullOrEmpty(pessoaFisica.getNrOab()));
		this.sexo = validate(pessoaFisica.getSexo() == null);
		this.perfil = validate(isAdvogadoInternoEPerfilNulo(pessoaFisica));
		this.flagEnviaEmail = validate(isCampoEnvialEmailNulo(pessoaFisica));
		this.advogadoInterno = validate(pessoaFisica.getAdvogadoInterno() == null);
		this.numeroOabCadastrado = validate(existeNumeroOabCadastrado);
		this.ufOab = validate(pessoaFisica.getUfOab() == null);
		this.emailObrigatorio = validate(isAdvogadoInterno(pessoaFisica) && isEmailVazio(pessoaFisica));
		this.contatoObrigatorio = validate(isContatoVazio(pessoaFisica));
		this.enderecoObrigatorio = validate(isEnderecoVazio(pessoaFisica));
		this.CPFObrigatorio = validate(isAdvogadoInterno(pessoaFisica) && isObrigatorioCPF(pessoaFisica));
	}
	
	private boolean isObrigatorioCPF(PessoaFisica pessoaFisica) {
		return pessoaFisica.getCpf() == null || pessoaFisica.getCpf().isEmpty();
	}
	
	private boolean isAdvogadoInternoEPerfilNulo(PessoaFisica p) {
        return p.isAdvogadoInternoEscritorio() && p.getPessoa().getPerfil() == null;
	}

	private boolean existePessoaVazia(final PessoaFisica pessoaFisica) {
		if (pessoaFisica.getPessoa() == null
				|| Strings.isNullOrEmpty(pessoaFisica.getPessoa().getDescricao())) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	private boolean isAdvogadoInterno(PessoaFisica pessoaFisica) {
		if (pessoaFisica.getPessoa() != null
				&& pessoaFisica.isAdvogadoInternoEscritorio()) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	
	private boolean isEmailVazio(PessoaFisica pessoaFisica) {
		if (pessoaFisica.getPessoa().getPessoasEmails().isEmpty()) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	
	private boolean isContatoVazio(PessoaFisica pessoaFisica) {
		if (pessoaFisica.getPessoa().getPessoasContatos().isEmpty()) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	
	private boolean isEnderecoVazio(PessoaFisica pessoaFisica) {
		if (pessoaFisica.getPessoa().getPessoasEnderecos().isEmpty()) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	private boolean isCampoEnvialEmailNulo(final PessoaFisica pessoaFisica){
		if (pessoaFisica.getPessoa() != null
				&& pessoaFisica.isAdvogadoInternoEscritorio()
				&& pessoaFisica.getPessoa().getEnviaEmail() == null) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

}
