package br.com.juridico.validation.processo;

import br.com.juridico.entidades.Processo;
import br.com.juridico.validation.ValidationWrapper;

import javax.validation.constraints.Size;

/**
 * Created by jeffersonl2009_00 on 25/07/2016.
 */
public class DuasPartesValidacaoWrapper implements ValidationWrapper {

    @Size(min = 1, message = "Favor informar ao menos duas partes para o processo.")
    private String partes;

    public DuasPartesValidacaoWrapper(Processo processo) {
        this.partes = validate(processo.getPartes().size() < 2);
    }
}
