package br.com.juridico.validation.contratos;

import br.com.juridico.validation.ValidationWrapper;

import javax.validation.constraints.Size;

public class ConfiguracaoContratoValidacaoWrapper implements ValidationWrapper {

    @Size(min = 1, message = "Já existe um registro cadastrado com esta descrição.")
    private String descricao;

    /**
     *
     * @param contratoJaEstaCadastrado
     */
    public ConfiguracaoContratoValidacaoWrapper(boolean contratoJaEstaCadastrado) {
        this.descricao = validate(contratoJaEstaCadastrado);
    }
}
