package br.com.juridico.validation.cadastros;

import br.com.juridico.entidades.Vara;
import br.com.juridico.validation.ValidationWrapper;
import com.google.common.base.Strings;

import javax.validation.constraints.Size;

public class VaraValidacaoWrapper implements ValidationWrapper {

    @Size(min = 1, message = "Descrição: preenchimento obrigatório")
    private String descricao;
    @Size(min = 1, message = "Já existe um registro cadastrado com esta descrição.")
    private String descricaoExistente;

    public VaraValidacaoWrapper(Vara vara, boolean varaJaEstaCadastrado) {
        this.descricao = validate(Strings.isNullOrEmpty(vara.getDescricao()));
        this.descricaoExistente = validate(varaJaEstaCadastrado);
    }
}
