package br.com.juridico.validation.cadastros;

import br.com.juridico.entidades.NaturezaAcao;
import br.com.juridico.validation.ValidationWrapper;
import com.google.common.base.Strings;

import javax.validation.constraints.Size;

public class NaturezaAcaoValidacaoWrapper implements ValidationWrapper {

    @Size(min = 1, message = "Descrição: preenchimento obrigatório")
    private String descricao;
    @Size(min = 1, message = "Já existe um registro cadastrado com esta descrição.")
    private String descricaoExistente;

    public NaturezaAcaoValidacaoWrapper(NaturezaAcao naturezaAcao, boolean naturezaAcaoJaEstaCadastrado) {
        this.descricao = validate(Strings.isNullOrEmpty(naturezaAcao.getDescricao()));
        this.descricaoExistente = validate(naturezaAcaoJaEstaCadastrado);
    }
}
