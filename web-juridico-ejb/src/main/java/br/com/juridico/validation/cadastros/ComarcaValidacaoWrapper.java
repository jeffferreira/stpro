package br.com.juridico.validation.cadastros;

import br.com.juridico.entidades.Comarca;
import br.com.juridico.validation.ValidationWrapper;
import com.google.common.base.Strings;

import javax.validation.constraints.Size;

public class ComarcaValidacaoWrapper implements ValidationWrapper {

    @Size(min = 1, message = "Descrição: preenchimento obrigatório")
    private String descricao;
    @Size(min = 1, message = "Já existe um registro cadastrado com esta descrição.")
    private String descricaoExistente;

    public ComarcaValidacaoWrapper(Comarca comarca, boolean comarcaJaEstaCadastrado) {
        this.descricao = validate(Strings.isNullOrEmpty(comarca.getDescricao()));
        this.descricaoExistente = validate(comarcaJaEstaCadastrado);
    }
}
