package br.com.juridico.state.controladoria;

import br.com.juridico.entidades.StatusAgenda;
import br.com.juridico.enums.ControladoriaIndicador;
import br.com.juridico.state.ControladoriaState;

/**
 * Created by jefferson.ferreira on 03/02/2017.
 */
public class ControladoriaContext implements ControladoriaState {

    private ControladoriaState state;

    @Override
    public ControladoriaIndicador controladoriaAction(boolean edicao, StatusAgenda statusAgenda, boolean possuiConfiguracao, ControladoriaIndicador statusAtual) {
        return this.state.controladoriaAction(edicao, statusAgenda, possuiConfiguracao, statusAtual);
    }

    public ControladoriaState getState() {
        return state;
    }

    public void setState(ControladoriaState state) {
        this.state = state;
    }

}
