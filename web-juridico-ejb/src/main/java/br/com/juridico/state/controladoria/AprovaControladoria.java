package br.com.juridico.state.controladoria;

import br.com.juridico.entidades.StatusAgenda;
import br.com.juridico.enums.ControladoriaIndicador;
import br.com.juridico.state.ControladoriaState;

/**
 * Created by jefferson.ferreira on 03/02/2017.
 */
public class AprovaControladoria implements ControladoriaState {

    @Override
    public ControladoriaIndicador controladoriaAction(boolean edicao, StatusAgenda statusAgenda, boolean possuiConfiguracao, ControladoriaIndicador statusAtual) {
        if(statusAgenda == null) {
            return null;
        }
        if(!possuiConfiguracao || (!statusAgenda.getStatusAcumprirPassaControladoria() && statusAgenda.isStatusAcumprir() && !edicao)){
            return ControladoriaIndicador.NAO_SE_APLICA;
        }
        if(ControladoriaIndicador.PENDENTE.equals(statusAtual) && !edicao){
            return ControladoriaIndicador.APROVADO;
        }
        return ControladoriaIndicador.PENDENTE;
    }
}
