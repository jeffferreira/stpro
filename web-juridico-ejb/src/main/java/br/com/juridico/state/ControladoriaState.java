package br.com.juridico.state;

import br.com.juridico.entidades.StatusAgenda;
import br.com.juridico.enums.ControladoriaIndicador;

/**
 * Created by jefferson.ferreira on 03/02/2017.
 */
public interface ControladoriaState {

    /**
     *
     * @param edicao
     * @param statusAgenda
     * @param possuiConfiguracao
     * @param statusAtual
     * @return
     */
    ControladoriaIndicador controladoriaAction(boolean edicao, StatusAgenda statusAgenda, boolean possuiConfiguracao, ControladoriaIndicador statusAtual);
}
