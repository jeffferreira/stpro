package br.com.juridico.filter;

import br.com.juridico.entidades.Empresa;
import br.com.juridico.entidades.Fornecedor;
import br.com.juridico.enums.ContasPagarType;
import br.com.juridico.enums.StatusContasPagarType;
import br.com.juridico.enums.TipoDataContasPagarType;

import java.io.Serializable;
import java.util.Date;

public class ContasPagarFilter implements Serializable {

    private static final long serialVersionUID = 1L;

    private Date dataInicial;

    private Date dataFinal;

    private Boolean ativo = Boolean.TRUE;

    private Empresa empresa;

    private Fornecedor fornecedor;

    private String condicaoGeral;

    private StatusContasPagarType statusContasPagar = StatusContasPagarType.CA;

    private TipoDataContasPagarType tipoDataContasPagarType = TipoDataContasPagarType.EMISSAO;

    private StatusContasPagarType[] statusContasPagarTypes;

    private String numeroNF;

    private boolean excetoRecusadas = false;

    private Boolean vencido;

    private ContasPagarType tipo;

    private ContasPagarType tipoPagamento;

    private ContasPagarType[] tiposContasPagar;


    public ContasPagarFilter() {
        super();
    }

	public ContasPagarType getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(ContasPagarType tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }

    public ContasPagarFilter(StatusContasPagarType tipo) {
        statusContasPagar = tipo;
    }

    public void setStatusContasPagarTypes(
            StatusContasPagarType[] statusContasPagarTypes) {
        this.statusContasPagarTypes = statusContasPagarTypes.clone();
    }

    public StatusContasPagarType[] getStatusContasPagarTypes() {
        return statusContasPagarTypes;
    }

    public void setNumeroNF(String numeroNF) {
        this.numeroNF = numeroNF;
    }

    public String getNumeroNF() {
        return numeroNF;
    }

    public String getCondicaoGeral() {
        return condicaoGeral;
    }

    public void setCondicaoGeral(String condicaoGeral) {
        this.condicaoGeral = condicaoGeral;
    }

    public Date getDataInicial() {
        return dataInicial;
    }

    public void setDataInicial(Date dataInicial) {
        this.dataInicial = dataInicial;
    }

    public Date getDataFinal() {
        return dataFinal;
    }

    public void setDataFinal(Date dataFinal) {
        this.dataFinal = dataFinal;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }


    public void setStatusContasPagar(StatusContasPagarType statusContasPagar) {
        this.statusContasPagar = statusContasPagar;
    }

    public StatusContasPagarType getStatusContasPagar() {
        return statusContasPagar;
    }

    public boolean isExcetoRecusadas() {
        return excetoRecusadas;
    }

    public void setExcetoRecusadas(boolean excetoRecusadas) {
        this.excetoRecusadas = excetoRecusadas;
    }

    public Boolean getVencido() {
        return vencido;
    }

    public void setVencido(Boolean vencido) {
        this.vencido = vencido;
    }

    public ContasPagarType getTipo() {
        return tipo;
    }

    public void setTipo(ContasPagarType tipo) {
        this.tipo = tipo;
    }

	public ContasPagarType[] getTiposContasPagar() {
		return tiposContasPagar;
	}

	public void setTiposContasPagar(ContasPagarType[] tiposContasPagar) {
		this.tiposContasPagar = tiposContasPagar;
	}

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Fornecedor getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(Fornecedor fornecedor) {
        this.fornecedor = fornecedor;
    }

    public TipoDataContasPagarType getTipoDataContasPagarType() {
        return tipoDataContasPagarType;
    }

    public void setTipoDataContasPagarType(TipoDataContasPagarType tipoDataContasPagarType) {
        this.tipoDataContasPagarType = tipoDataContasPagarType;
    }
}
