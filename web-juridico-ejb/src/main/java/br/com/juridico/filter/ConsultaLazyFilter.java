package br.com.juridico.filter;

import java.io.Serializable;

/**
 * Created by jeffersonl2009_00 on 12/07/2016.
 */
public class ConsultaLazyFilter implements Serializable {

    private static final long serialVersionUID = 1L;

    private String descricao;
    private int primeiroRegistro;
    private int quantidadeRegistros;
    private String propriedadeOrdenacao;
    private boolean ascendente;
    private Long codigoEmpresa;
    private boolean possuiExclusaoLogica = true;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getPrimeiroRegistro() {
        return primeiroRegistro;
    }

    public void setPrimeiroRegistro(int primeiroRegistro) {
        this.primeiroRegistro = primeiroRegistro;
    }

    public int getQuantidadeRegistros() {
        return quantidadeRegistros;
    }

    public void setQuantidadeRegistros(int quantidadeRegistros) {
        this.quantidadeRegistros = quantidadeRegistros;
    }

    public String getPropriedadeOrdenacao() {
        return propriedadeOrdenacao;
    }

    public void setPropriedadeOrdenacao(String propriedadeOrdenacao) {
        this.propriedadeOrdenacao = propriedadeOrdenacao;
    }

    public boolean isAscendente() {
        return ascendente;
    }

    public void setAscendente(boolean ascendente) {
        this.ascendente = ascendente;
    }

    public Long getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(Long codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }

    public boolean isPossuiExclusaoLogica() {
        return possuiExclusaoLogica;
    }

    public void setPossuiExclusaoLogica(boolean possuiExclusaoLogica) {
        this.possuiExclusaoLogica = possuiExclusaoLogica;
    }
}
