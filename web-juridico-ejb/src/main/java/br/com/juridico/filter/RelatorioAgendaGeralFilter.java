package br.com.juridico.filter;

import java.util.Date;

import br.com.juridico.dao.IFilter;
import br.com.juridico.entidades.Pessoa;
import br.com.juridico.util.DateUtils;

public class RelatorioAgendaGeralFilter implements IFilter{
	
	private static final String TODOS = "TODOS";

	private Long codigoEmpresa;
	
	private Pessoa pessoa;
	
	private Date dataInicial;
	
	private Date dataFinal;
	
	private String opcaoPessoa;

	private String status;
	

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public Date getDataInicial() {
		return DateUtils.lowerLimit(dataInicial);
	}

	public void setDataInicial(Date dataInicial) {
		this.dataInicial = dataInicial;
	}

	public Date getDataFinal() {
		return DateUtils.upperLimit(dataFinal);
	}

	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}

	public Long getCodigoEmpresa() {
		return codigoEmpresa;
	}

	public void setCodigoEmpresa(Long codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getOpcaoPessoa() {
		if(opcaoPessoa == null) {
			this.opcaoPessoa = TODOS;
		}
		return opcaoPessoa;
	}

	public void setOpcaoPessoa(String opcaoPessoa) {
		this.opcaoPessoa = opcaoPessoa;
	}
	
	public Object getDescricaoPeriodo() {
		return DateUtils.formatData(dataInicial, "dd/MM/yyyy") + " \u00E0 "
				+ DateUtils.formatData(dataFinal, "dd/MM/yyyy");
	}
	
}
