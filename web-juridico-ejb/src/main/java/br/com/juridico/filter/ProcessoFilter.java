package br.com.juridico.filter;

import br.com.juridico.entidades.Pessoa;
import com.google.common.base.Strings;

/**
 *
 * @author Jefferson
 *
 */
public class ProcessoFilter {

	private String status;

	private String processo;

	private String cliente;

	private String adverso;

	private String buscaRapida;

	private Pessoa pessoa;

	public ProcessoFilter(){
		super();
	}

	public ProcessoFilter(String cliente, String adverso, Pessoa pessoa) {
		this.cliente = cliente;
		this.adverso = adverso;
		this.pessoa = pessoa;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getProcesso() {
		return processo;
	}

	public void setProcesso(String processo) {
		this.processo = processo;
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public String getAdverso() {
		return adverso;
	}

	public void setAdverso(String adverso) {
		this.adverso = adverso;
	}

	public String getBuscaRapida() {
		return buscaRapida;
	}

	public void setBuscaRapida(String buscaRapida) {
		this.buscaRapida = buscaRapida;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public boolean isPossuiFiltroCarregado(){
		return !Strings.isNullOrEmpty(status)
				|| !Strings.isNullOrEmpty(processo)
				|| !Strings.isNullOrEmpty(cliente)
				|| !Strings.isNullOrEmpty(adverso)
				|| !Strings.isNullOrEmpty(buscaRapida)
				|| pessoa != null;
	}
}
