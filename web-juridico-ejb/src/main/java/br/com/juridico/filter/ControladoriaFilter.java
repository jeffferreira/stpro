package br.com.juridico.filter;

import br.com.juridico.entidades.PessoaFisica;
import br.com.juridico.entidades.PessoaJuridica;
import br.com.juridico.enums.ControladoriaIndicador;
import br.com.juridico.enums.TipoPessoaControladoriaType;
import br.com.juridico.enums.TipoPessoaParteControladoriaType;

import java.util.Date;

/**
 * Created by jefferson.ferreira on 10/01/2017.
 */
public class ControladoriaFilter {

    private static final String PESSOA_FISICA = "PF";
    private Date dataInicioAlteracao;
    private Date dataFimAlteracao;
    private Date dataInicioPrazo;
    private Date dataFimPrazo;
    private String status;
    private PessoaFisica pessoaFisica;
    private PessoaFisica advogadoColaborador;
    private PessoaJuridica pessoaJuridica;
    private String controladoriaStatus;
    private String processo;
    private TipoPessoaControladoriaType tipoPessoaControladoriaType;
    private String tipoPessoa;

    public ControladoriaFilter(){
        this.controladoriaStatus = ControladoriaIndicador.PENDENTE.getStatus();
        this.tipoPessoaControladoriaType = TipoPessoaControladoriaType.ADVOGADO;
        this.tipoPessoa = PESSOA_FISICA;
    }

    public boolean isPartePessoaFisica(){
        return PESSOA_FISICA.equals(this.tipoPessoa);
    }

    public Date getDataInicioAlteracao() {
        return dataInicioAlteracao;
    }

    public void setDataInicioAlteracao(Date dataInicioAlteracao) {
        this.dataInicioAlteracao = dataInicioAlteracao;
    }

    public Date getDataFimAlteracao() {
        return dataFimAlteracao;
    }

    public void setDataFimAlteracao(Date dataFimAlteracao) {
        this.dataFimAlteracao = dataFimAlteracao;
    }

    public Date getDataInicioPrazo() {
        return dataInicioPrazo;
    }

    public void setDataInicioPrazo(Date dataInicioPrazo) {
        this.dataInicioPrazo = dataInicioPrazo;
    }

    public Date getDataFimPrazo() {
        return dataFimPrazo;
    }

    public void setDataFimPrazo(Date dataFimPrazo) {
        this.dataFimPrazo = dataFimPrazo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public PessoaFisica getPessoaFisica() {
        return pessoaFisica;
    }

    public void setPessoaFisica(PessoaFisica pessoaFisica) {
        this.pessoaFisica = pessoaFisica;
    }

    public String getControladoriaStatus() {
        return controladoriaStatus;
    }

    public void setControladoriaStatus(String controladoriaStatus) {
        this.controladoriaStatus = controladoriaStatus;
    }

    public String getProcesso() {
        return processo;
    }

    public void setProcesso(String processo) {
        this.processo = processo;
    }

    public TipoPessoaControladoriaType getTipoPessoaControladoriaType() {
        return tipoPessoaControladoriaType;
    }

    public void setTipoPessoaControladoriaType(TipoPessoaControladoriaType tipoPessoaControladoriaType) {
        this.tipoPessoaControladoriaType = tipoPessoaControladoriaType;
    }

    public String getTipoPessoa() {
        return tipoPessoa;
    }

    public void setTipoPessoa(String tipoPessoa) {
        this.tipoPessoa = tipoPessoa;
    }

    public PessoaJuridica getPessoaJuridica() {
        return pessoaJuridica;
    }

    public void setPessoaJuridica(PessoaJuridica pessoaJuridica) {
        this.pessoaJuridica = pessoaJuridica;
    }

    public PessoaFisica getAdvogadoColaborador() {
        return advogadoColaborador;
    }

    public void setAdvogadoColaborador(PessoaFisica advogadoColaborador) {
        this.advogadoColaborador = advogadoColaborador;
    }
}
