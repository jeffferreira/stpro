package br.com.juridico.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.Session;

import br.com.juridico.util.OrderClause;

/**
 * 
 * @author Jefferson
 *
 */
public abstract class AbstractDao<T extends IEntity> implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final String CODIGO_EMPRESA = "codigoEmpresa";

	protected Class<T> entityClass;
	protected EntityManager entityManager;

	protected AbstractDao() {
	}

	protected AbstractDao(Class<T> entityClass, EntityManager entityManager) {
		this();
		this.entityClass = entityClass;
		this.entityManager = entityManager;
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(final EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public Session getHibernateSession() {
		return (Session) entityManager.getDelegate();
	}

	public void clearSession() {
		this.getHibernateSession().clear();
	}

	public Long getObjectCount() {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		cq.select(cb.count(cq.from(entityClass)));
		return getEntityManager().createQuery(cq).getSingleResult();
	}

	protected TypedQuery<T> buildQuery(Long codigoEmpresa, Map<String, Object> params, boolean isOr, OrderClause[] orderClauses) {
		CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(entityClass);
		Root<T> root = criteriaQuery.from(entityClass);

        if(codigoEmpresa != null)
		    criteriaQuery.where(criteriaBuilder.equal(root.get(CODIGO_EMPRESA), codigoEmpresa));

		Predicate[] predicates = null;
		if (params != null) {
			predicates = buildPredicates(params, root, criteriaBuilder);
		}

		if (orderClauses != null) {
			putOrder(root, criteriaBuilder, criteriaQuery, orderClauses);
		}

		if (params != null) {
			if (isOr) {
				criteriaQuery.where(criteriaBuilder.or(predicates));
			} else {
				criteriaQuery.where(criteriaBuilder.and(predicates));
			}
		}

		return getEntityManager().createQuery(criteriaQuery);
	}

	protected TypedQuery<T> buildQuery(Long codigoEmpresa, OrderClause... orderClauses) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<T> cq = cb.createQuery(entityClass);
		Root<T> root = cq.from(entityClass);
		cq.where(cb.equal(root.get(CODIGO_EMPRESA), codigoEmpresa));

		if (orderClauses != null) {
			putOrder(root, cb, cq, orderClauses);
		}

		return getEntityManager().createQuery(cq);
	}

	protected Predicate[] buildPredicates(Map<String, Object> params, Root<T> rootEntity, CriteriaBuilder criteriaBuilder) {
		Predicate[] predicates = new Predicate[params.size()];
		int index = 0;
		for (Map.Entry<String, Object> entry : params.entrySet()) {
			Object value = entry.getValue();
			StringTokenizer st;
			if (value instanceof String) {
				st = new StringTokenizer(entry.getKey(), ".");
				Path<String> path = rootEntity.get(st.nextToken());
				while (st.hasMoreTokens()) {
					path = path.get(st.nextToken());
				}
				predicates[index] = criteriaBuilder.like(criteriaBuilder.upper(path), "%" + value.toString().trim().toUpperCase() + "%");
			} else {
				st = new StringTokenizer(entry.getKey(), ".");
				Path<?> path = rootEntity.get(st.nextToken());
				while (st.hasMoreTokens()) {
					path = path.get(st.nextToken());
				}
				predicates[index] = criteriaBuilder.equal(path, entry.getValue());
			}
			index++;
		}
		return predicates;
	}

	protected void putOrder(Root<T> rootEntity, CriteriaBuilder criteriaBuilder, CriteriaQuery<T> criteriaQuery, OrderClause... orderClauses) {
		Path<T> orderPath;
		StringTokenizer st;
		for (OrderClause orderClause : orderClauses) {
			orderPath = rootEntity;
			st = new StringTokenizer(orderClause.getPropertyName(), ".");
			while (st.hasMoreTokens()) {
				orderPath = orderPath.get(st.nextToken());
			}
			criteriaQuery.orderBy(orderClause.getOrderType().orderBy(criteriaBuilder, orderPath));
		}
	}

	public Long getObjectCount(Map<String, Object> attributes, boolean isOr) {
		CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
		Root<T> rootEntity = criteriaQuery.from(entityClass);
		criteriaQuery.select(criteriaBuilder.count(rootEntity));

		if (attributes != null) {
			Predicate[] predicates = buildPredicates(attributes, rootEntity, criteriaBuilder);

			if (isOr) {
				criteriaQuery.where(criteriaBuilder.or(predicates));
			} else {
				criteriaQuery.where(criteriaBuilder.and(predicates));
			}
		}

		return getEntityManager().createQuery(criteriaQuery).getSingleResult();
	}

	public void create(T entity) {
		boolean isAManagedEntity = isAManagedEntity(entity);
		getEntityManager().joinTransaction();
		if (isAManagedEntity) {
			getEntityManager().merge(entity);
		} else {
			getEntityManager().persist(entity);
		}
		getEntityManager().flush();
	}

	public T retrieve(Long id) {
		return getEntityManager().find(entityClass, id);
	}

	public void refresh(T entity) {
		getEntityManager().refresh(entity);
	}

	public T retrieve(String query, Map<String, Object> attributes, Long codigoEmpresa, boolean... isNamedQuery) {
		TypedQuery<T> q = isNamedQuery != null && isNamedQuery.length == 1 && isNamedQuery[0] ? getEntityManager().createNamedQuery(query,
				entityClass) : getEntityManager().createQuery(query, entityClass);
		if (attributes != null && !attributes.isEmpty()) {
			for (Map.Entry<String, Object> entry : attributes.entrySet()) {
				q.setParameter(entry.getKey(), entry.getValue());
			}
		}
		q.setParameter(CODIGO_EMPRESA, codigoEmpresa);

		return q.getSingleResult();
	}

	public List<T> retrieveAll(OrderClause[] orderClauses, Long codigoEmpresa) {
		TypedQuery<T> query = buildQuery(codigoEmpresa, orderClauses);
		return query.getResultList();
	}

	public List<T> retrieveByQuery(String query, Map<String, Object> attributes, Long codigoEmpresa, boolean isNamedQuery) {
		TypedQuery<T> q = isNamedQuery ? getEntityManager().createNamedQuery(query, entityClass) : getEntityManager().createQuery(query, entityClass);
		if (attributes != null && !attributes.isEmpty()) {
			for (Map.Entry<String, Object> entry : attributes.entrySet()) {
				q.setParameter(entry.getKey(), entry.getValue());
			}
		}
		q.setParameter(CODIGO_EMPRESA, codigoEmpresa);

		return q.getResultList();
	}

	public <X> List<X> retrieveAnyQuery(String query, Map<String, Object> attributes, boolean isNamedQuery, Class<X> clazz, Long codigoEmpresa) {
		TypedQuery<X> q = isNamedQuery ? getEntityManager().createNamedQuery(query, clazz) : getEntityManager().createQuery(query, clazz);
		if (attributes != null && !attributes.isEmpty()) {
			for (Map.Entry<String, Object> entry : attributes.entrySet()) {
				q.setParameter(entry.getKey(), entry.getValue());
			}
		}
		q.setParameter(CODIGO_EMPRESA, codigoEmpresa);

		return q.getResultList();
	}

	public List<T> retrieveByQuery(String query, Long codigoEmpresa, boolean isNamedQuery) {
		TypedQuery<T> q = isNamedQuery ? getEntityManager().createNamedQuery(query, entityClass) : getEntityManager().createQuery(query, entityClass);
		q.setParameter(CODIGO_EMPRESA, codigoEmpresa);
		return q.getResultList();
	}

	public <X> List<X> retrieveAnyQuery(String query, boolean isNamedQuery, Long codigoEmpresa, Class<X> clazz) {
		TypedQuery<X> q = isNamedQuery ? getEntityManager().createNamedQuery(query, clazz) : getEntityManager().createQuery(query, clazz);
		q.setParameter(CODIGO_EMPRESA, codigoEmpresa);
		return q.getResultList();
	}

	public List<T> retrieveAll(Map<String, Object> attributes, boolean isOr, OrderClause[] orderClauses, Long codigoEmpresa) {
		TypedQuery<T> query = buildQuery(codigoEmpresa, attributes, isOr, orderClauses);
		return query.getResultList();
	}

	public List<T> retrieveRange(int[] range, OrderClause[] orderClauses, Long codigoEmpresa) {
		TypedQuery<T> query = buildQuery(codigoEmpresa, orderClauses);

		if (range != null) {
			query.setMaxResults(range[1] - range[0]);
			query.setFirstResult(range[0]);
		}

		return query.getResultList();
	}

	public List<T> retrieveRange(int[] range, Map<String, Object> attributes, boolean isOr, OrderClause[] orderClauses, Long codigoEmpresa) {
		TypedQuery<T> query = buildQuery(codigoEmpresa, attributes, isOr, orderClauses);

		if (range != null) {
			query.setMaxResults(range[1] - range[0]);
			query.setFirstResult(range[0]);
		}

		return query.getResultList();
	}

	public void update(T entity) {
		getEntityManager().joinTransaction();
		getEntityManager().merge(entity);
		getEntityManager().flush();
	}

	public void update(List<T> entityList) {
		getEntityManager().joinTransaction();
		for (T currentEntity : entityList) {
			getEntityManager().merge(currentEntity);
		}
		getEntityManager().flush();
	}

	public void delete(Long id) {
		getEntityManager().joinTransaction();
		getEntityManager().remove(getEntityManager().getReference(entityClass, id));
		getEntityManager().flush();
	}

	public void delete(T entity) {
		getEntityManager().joinTransaction();

		Long id = entity.getId();
		if (id != null) {
			getEntityManager().remove(getEntityManager().getReference(entityClass, id));
		} else {
			getEntityManager().remove(entity);
		}

		getEntityManager().flush();
	}

	private boolean isAManagedEntity(T entity) {
		return getEntityManager().contains(entity) || hasIdBeenSet(entity);
	}

	private boolean hasIdBeenSet(T entity) {
		return entity.getId() != null;
	}
}
