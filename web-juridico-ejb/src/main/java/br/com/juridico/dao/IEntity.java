package br.com.juridico.dao;

import br.com.juridico.entidades.Pessoa;
import br.com.juridico.entidades.PessoaContato;
import br.com.juridico.entidades.PessoaEmail;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.ValidationWrapper;
import com.google.common.collect.Lists;

import javax.swing.text.MaskFormatter;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.List;
import java.util.Set;

/**
 *
 */
public interface IEntity {

	/**
	 *
	 * @return
     */
	Long getId();

    /**
     *
     * @param validations
     * @throws ValidationException
     */
    default void runValidations(Set<ConstraintViolation<ValidationWrapper>> validations) throws ValidationException {
        if (!validations.isEmpty()) {
            List<String> messages = Lists.newArrayList();
            for (Object violation : validations.toArray()) {
                messages.add(((ConstraintViolation<IEntity>) violation).getMessage());
            }
            throw new ValidationException(validations.toArray(), messages);
        }
    }

    default String formatString(String value, String pattern) {
        MaskFormatter mf;
        try {
            mf = new MaskFormatter(pattern);
            mf.setValueContainsLiteralCharacters(false);
            return mf.valueToString(value);
        } catch (ParseException ex) {
            return value;
        }
    }

    default String getEmails(Pessoa pessoa) {
        if(pessoa == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (PessoaEmail email : pessoa.getPessoasEmails()) {
            sb.append(email.getEmail()).append("; ");
        }
        removeUltimoCaractereVazio(sb);
        return sb.toString();
    }

    default String getTelefones(Pessoa pessoa) {
        if(pessoa == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (PessoaContato contato : pessoa.getPessoasContatos()) {
            sb.append(contato.getContato().getDDDTelefoneFormatado()).append("; ");
        }
        removeUltimoCaractereVazio(sb);
        return sb.toString();
    }

    default void removeUltimoCaractereVazio(StringBuilder sb) {
        if (sb.length() > 0) {
            sb.deleteCharAt(sb.lastIndexOf(" "));
        }
    }

    default BigDecimal getValorNotNul(BigDecimal valor){
        if(valor == null){
            valor = BigDecimal.ZERO;
        }

        return valor;
    }

    /**
     *
     * @return
     */
    default Validator buildValidatorFactory(){
        return Validation.buildDefaultValidatorFactory().getValidator();
    }
}
