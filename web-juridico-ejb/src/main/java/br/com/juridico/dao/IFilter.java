package br.com.juridico.dao;

import br.com.juridico.entidades.Pessoa;
import br.com.juridico.entidades.PessoaContato;
import br.com.juridico.entidades.PessoaEmail;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.ValidationWrapper;
import com.google.common.collect.Lists;

import javax.swing.text.MaskFormatter;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.List;
import java.util.Set;

/**
 *
 */
public interface IFilter {

    Long getCodigoEmpresa();

    /**
     *
     * @param validations
     * @throws ValidationException
     */
    default void runValidations(Set<ConstraintViolation<ValidationWrapper>> validations) throws ValidationException {
        if (!validations.isEmpty()) {
            List<String> messages = Lists.newArrayList();
            for (Object violation : validations.toArray()) {
                messages.add(((ConstraintViolation<IFilter>) violation).getMessage());
            }
            throw new ValidationException(validations.toArray(), messages);
        }
    }

    /**
     *
     * @return
     */
    default Validator buildValidatorFactory(){
        return Validation.buildDefaultValidatorFactory().getValidator();
    }
}
