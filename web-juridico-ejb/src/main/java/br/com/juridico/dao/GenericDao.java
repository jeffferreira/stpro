package br.com.juridico.dao;

import java.io.Serializable;

import javax.persistence.EntityManager;

/**
 * 
 * @author Jefferson
 *
 */
public class GenericDao<T extends IEntity> extends AbstractDao<T> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2423414195799547035L;

	public GenericDao(Class<T> entityClass, EntityManager entityManager) {
		super(entityClass, entityManager);
	}

}
