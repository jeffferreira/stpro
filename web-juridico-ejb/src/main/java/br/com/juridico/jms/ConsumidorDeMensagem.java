package br.com.juridico.jms;

import br.com.juridico.entidades.EmailCaixaSaida;
import br.com.juridico.entidades.Empresa;
import br.com.juridico.enums.StatusEmailCaixaSaidaIndicador;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.impl.EmailCaixaSaidaSessionBean;
import br.com.juridico.impl.EmpresaSessionBean;
import br.com.juridico.mail.MailBusiness;
import br.com.juridico.util.DateUtils;
import org.apache.log4j.Logger;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.*;
import javax.mail.MessagingException;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by jeffersonl2009_00 on 06/09/2016.
 */
@MessageDriven(activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
        @ActivationConfigProperty(propertyName = "destination", propertyValue = "java:/jms/queue/mailMessage"),
        @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge") })
public class ConsumidorDeMensagem implements MessageListener {

    private static final Logger LOGGER = Logger.getLogger(ConsumidorDeMensagem.class);

    @Inject
    private EmailCaixaSaidaSessionBean emailCaixaSaidaSessionBean;
    @Inject
    private EmpresaSessionBean empresaSessionBean;

    @Override
    public void onMessage(Message message) {
        ObjectMessage objMsg = (ObjectMessage) message;
        try {
            EmailCaixaSaida mail = ((EmailCaixaSaida) objMsg.getObject());
            if(mail == null) {
                return;
            }

            String[] destinatarios = mail.getDestinatarios().split("; ");
            Empresa empresa = empresaSessionBean.retrieve(mail.getCodigoEmpresa());

            if(StatusEmailCaixaSaidaIndicador.APROVADO.getValue().charAt(0) == mail.getFlagStatus()) {
                LOGGER.info(">>>>>>>>>>>>>>>>>>> ENVIANDO EMAIL >>>>>>>>>>>>>>>>>>>>>>>>");
                MailBusiness.enviarEmail(empresa.getConfiguracaoEmail(), mail.getAssunto(), mail.getCorpoEmail(), destinatarios);
                LOGGER.info(">>>>>>>>>>>>>>>>>>> ALTERANDO CAIXA DE SAIDA PARA PROCESSADO >>>>>>>>>>>>>>>>>>>>>>>>");
                mail.setFlagProcessado(true);
                mail.setDataProcessamento(Calendar.getInstance().getTime());
                emailCaixaSaidaSessionBean.update(mail);
                LOGGER.info(">>>>>>>>>>>>>>>>>>> PROCESSADO: "+ DateUtils.formataDataBrasil(new Date()) + " >>>>>>>>>>>>>>>>>>>>>>>>");
            }

        } catch (JMSException e) {
            LOGGER.error(e, e.getCause());
        } catch (MessagingException e) {
            LOGGER.error(e, e.getCause());
        } catch (ValidationException e) {
            LOGGER.error(e, e.getCause());
        }
    }
}
