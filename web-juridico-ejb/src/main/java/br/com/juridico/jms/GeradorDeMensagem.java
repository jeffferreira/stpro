package br.com.juridico.jms;

import br.com.juridico.entidades.EmailCaixaSaida;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.*;

/**
 * Created by jeffersonl2009_00 on 06/09/2016.
 */
@Stateless
public class GeradorDeMensagem {

    private static final Logger LOGGER = Logger.getLogger(GeradorDeMensagem.class);

    @Resource(mappedName = "java:/JmsXA")
    private ConnectionFactory connectionFactory;

    @Resource(mappedName = "java:/jms/queue/mailMessage")
    private Destination destination;

    private Connection connection;
    private Session session;
    private MessageProducer messageProducer;

    @PostConstruct
    public void init() {
        try {
            connection = connectionFactory.createConnection();
            session = connection.createSession(true, Session.AUTO_ACKNOWLEDGE);
            messageProducer = session.createProducer(destination);

        } catch (JMSException e) {
            LOGGER.error(e, e.getCause());
            throw new RuntimeException(e);
        }
    }

    @PreDestroy
    public void destroy() {
        if (connection != null) {
            try {
                connection.close();
            } catch (JMSException e) {
                LOGGER.error(e, e.getCause());
            }
        }
    }

    public void enviarMensagem(EmailCaixaSaida emailCaixaSaida) {
        ObjectMessage message;
        try {
            message = session.createObjectMessage(emailCaixaSaida);
            messageProducer.send(message);
        } catch (JMSException e) {
            LOGGER.error("<h2>Ocorreu um problema durante a entrega da mensagem</h2>");
            LOGGER.error(e, e.getCause());
        }
    }
}
