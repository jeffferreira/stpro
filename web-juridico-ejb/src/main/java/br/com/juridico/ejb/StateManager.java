package br.com.juridico.ejb;

import br.com.juridico.entidades.Empresa;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import java.io.Serializable;

/**
 * Created by jefferson.ferreira on 30/05/2017.
 */
@Stateful
@LocalBean
public class StateManager implements Serializable {

    private Empresa empresaSelecionada;

    public Empresa getEmpresaSelecionada() {
        return empresaSelecionada;
    }

    public void setEmpresaSelecionada(Empresa empresaSelecionada) {
        this.empresaSelecionada = empresaSelecionada;
    }


    public boolean isPossuiFiliais(){
        return this.empresaSelecionada != null
                && !this.empresaSelecionada.getFiliais().isEmpty()
                || this.empresaSelecionada.getMatriz() != null;
    }
}
