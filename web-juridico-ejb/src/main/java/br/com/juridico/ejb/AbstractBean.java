package br.com.juridico.ejb;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import br.com.juridico.filter.ConsultaLazyFilter;
import org.hibernate.exception.ConstraintViolationException;

import br.com.juridico.dao.AbstractDao;
import br.com.juridico.dao.IEntity;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;

/**
 * 
 * @author Jefferson
 *
 */
public abstract class AbstractBean<T extends IEntity> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6734277153192012983L;

	protected AbstractDao<T> dao;

	protected final transient Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

	public AbstractBean() {
	}

	public AbstractBean(AbstractDao<T> dao) {
		this.dao = dao;
	}

	protected void setDao(AbstractDao<T> dao) {
		this.dao = dao;
	}

	public AbstractDao<T> getDao() {
		return dao;
	}

	public void clearSession() {
		getDao().clearSession();
	}

	public Long getObjectCount() {
		return getDao().getObjectCount();
	}

	public Long getObjectCount(Map<String, Object> attributes, boolean isOr) {
		return getDao().getObjectCount(attributes, isOr);
	}

	public void create(T entity) throws ValidationException {
		validade(entity);
		try {
			getDao().create(entity);
		} catch (PersistenceException ex) {
			handlePersistenceException(ex);
		}
	}

	protected String translateConstraintError(String constraintName) {
		return "Restrição do banco de dados violada: ".concat(constraintName);
	}

	protected void handlePersistenceException(PersistenceException ex) {
		Throwable cause = ex.getCause();
		while (cause != null && cause != cause.getCause()) {
			if (cause instanceof ConstraintViolationException) {
				ConstraintViolationException cex = (ConstraintViolationException) cause;
				if (cex.getConstraintName() != null) {
					throw new IllegalArgumentException(translateConstraintError(cex.getConstraintName()), ex);
				}
			}
			cause = cause.getCause();
		}
		throw ex;
	}

	public void update(T entity) throws ValidationException {
		validade(entity);
		try {
			getDao().update(entity);
		} catch (PersistenceException ex) {
			handlePersistenceException(ex);
		}
	}

	public void update(List<T> entityList) throws ValidationException {
		validade(entityList);
		try {
			getDao().update(entityList);
		} catch (PersistenceException ex) {
			handlePersistenceException(ex);
		}
	}

	public T retrieve(Long id) {
		return getDao().retrieve(id);
	}

	public T retrieve(String query, Map<String, Object> attributes, Long codigoEmpresa, boolean... isNamedQuery) {
		return getDao().retrieve(query, attributes, codigoEmpresa, isNamedQuery);
	}

	public void refresh(T entity) {
		getDao().refresh(entity);
	}

	public OrderClause[] getDefaultOrder() {
		return new OrderClause[] { new OrderClause("id", OrderType.ASC) };
	}

	public List<T> retrieveAll(Long codigoEmpresa) {
		return getDao().retrieveAll(getDefaultOrder(), codigoEmpresa);
	}

	public List<T> retrieveAll(ConsultaLazyFilter filter, Long codigoEmpresa) {
		return getDao().retrieveAll(getDefaultOrder(), codigoEmpresa);
	}

	public List<T> retrieveAll(Long codigoEmpresa, OrderClause... orderClauseArray) {
		return getDao().retrieveAll(orderClauseArray, codigoEmpresa);
	}

	public List<T> retrieveAll(Map<String, Object> parameters, boolean isOr, Long codigoEmpresa) {
		return getDao().retrieveAll(parameters, isOr, getDefaultOrder(), codigoEmpresa);
	}

	public List<T> retrieveAll(Map<String, Object> parameters, boolean isOr, Long codigoEmpresa, OrderClause... orderClauseArray) {
		return getDao().retrieveAll(parameters, isOr, orderClauseArray, codigoEmpresa);
	}

	public List<T> retrieveRange(int[] range, Long codigoEmpresa) {
		return getDao().retrieveRange(range, getDefaultOrder(), codigoEmpresa);
	}

	public List<T> retrieveRange(int[] range, Map<String, Object> parameters, boolean isOr, Long codigoEmpresa) {
		return getDao().retrieveRange(range, parameters, isOr, getDefaultOrder(), codigoEmpresa);
	}

	public List<T> retrieveByQuery(String query, Map<String, Object> attributes, boolean isNamedQuery, Long codigoEmpresa) {
		return getDao().retrieveByQuery(query, attributes, codigoEmpresa, isNamedQuery);
	}

	public List<T> retrieveByQuery(String query, boolean isNamedQuery, Long codigoEmpresa) {
		return getDao().retrieveByQuery(query, codigoEmpresa, isNamedQuery);
	}

	@SuppressWarnings("unchecked")
	public List<T> retrieveByExclusaoLogica(String sql, Long codigoEmpresa) {
		Query query = getDao().getEntityManager().createQuery(sql);
		query.setParameter("codigoEmpresa", codigoEmpresa);

		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<T> retrieveByExclusaoLogica(String sql, ConsultaLazyFilter filter, Long codigoEmpresa) {
		Query query = getDao().getEntityManager().createQuery(sql);
		query.setParameter("codigoEmpresa", codigoEmpresa);
		query.setFirstResult(filter.getPrimeiroRegistro());
		query.setMaxResults(filter.getQuantidadeRegistros());

		return query.getResultList();
	}

	public <X> List<X> retrieveAnyQuery(String query, boolean isNamedQuery, Long codigoEmpresa, Class<X> clazz) {
		return getDao().retrieveAnyQuery(query, isNamedQuery, codigoEmpresa, clazz);
	}

	public <X> List<X> retrieveAnyQuery(String query, Map<String, Object> attributes, boolean isNamedQuery, Class<X> clazz, Long codigoEmpresa) {
		return getDao().retrieveAnyQuery(query, attributes, isNamedQuery, clazz, codigoEmpresa);
	}

	public void delete(Long id) {
		try {
			getDao().delete(id);
		} catch (PersistenceException ex) {
			handlePersistenceException(ex);
		}
	}

	public void delete(T entity) {
		try {
			getDao().delete(entity);
		} catch (PersistenceException ex) {
			handlePersistenceException(ex);
		}
	}

	protected void validade(T entity) throws ValidationException {
		Set<ConstraintViolation<T>> violations = validator.validate(entity);
		if (!(violations.isEmpty())) {
			throw new ValidationException(violations.toArray());
		}
	}

	protected void validade(List<T> entityList) throws ValidationException {
		for (T entity : entityList) {
			Set<ConstraintViolation<T>> violations = validator.validate(entity);
			if (!(violations.isEmpty())) {
				throw new ValidationException(violations.toArray());
			}
		}
	}

}
