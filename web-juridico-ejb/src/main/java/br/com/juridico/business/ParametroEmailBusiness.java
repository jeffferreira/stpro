package br.com.juridico.business;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by jeffersonl2009_00 on 27/09/2016.
 */
public class ParametroEmailBusiness {

    private static final String PATTERN_PARAMETROS = "(\\$\\{([^\\}]+)\\})+";

    private ParametroEmailBusiness(){}

    /**
     *
     * @param texto
     * @return
     */
    public static String[] retornaParametrosDoTexto(String texto){
        if(Strings.isNullOrEmpty(texto)) {
            return null;
        }
        Pattern pattern = Pattern.compile(PATTERN_PARAMETROS);
        Matcher matcher = pattern.matcher(texto);
        List<String> results = Lists.newArrayList();
        int i = 0;

        while(matcher.find()) {
            results.add(matcher.group(0));
            i++;
        }

        if(i > 0) {
            return results.toArray(new String[results.size()]);
        }
        return null;
    }

    /**
     *
     * @param paramValueMap
     * @param textoOriginal
     * @return
     */
    public static String formataEmailComParametros(Map<String, String> paramValueMap, String textoOriginal){
        String textoFormatado = "";
        Pattern p = Pattern.compile("\\$\\{[^\\}]+\\}");
        Matcher m = p.matcher(textoOriginal);

        while(m.find()) {
            for(Map.Entry entry : paramValueMap.entrySet()) {
                if(entry.getKey().equals(m.group())) {
                    textoFormatado = textoOriginal.replace(m.group(), entry.getValue().toString());
                    textoOriginal = textoFormatado;
                }
            }
        }
        return textoFormatado;

    }

}
