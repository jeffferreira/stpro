package br.com.juridico.business;

import br.com.juridico.entidades.Andamento;
import br.com.juridico.entidades.ParametroEmail;
import br.com.juridico.entidades.User;
import br.com.juridico.util.DateUtils;
import com.google.common.base.Strings;

/**
 * Created by marcos on 04/02/2017.
 */
public class PrazoVencidoEmailBusiness {

    private static final String PROCESSO = "PROC_";
    private static Long PROC_ANDAMENTO_PRAZO_VENCIDO = 17L;

    private static String result;

    public static String execute(ParametroEmail parametroEmail, Andamento andamento, User user){
        result = "";

        if(!Strings.isNullOrEmpty(parametroEmail.getChaveSessao()) &&
                parametroEmail.getChaveSessao().startsWith(PROCESSO)) {
            prazoVencidoAndamento(parametroEmail, andamento);
        }

        return result.isEmpty() ? GeralEmailBusiness.execute(parametroEmail, user) : result;
    }

    private static void prazoVencidoAndamento(ParametroEmail parametroEmail, Andamento andamento){
        if(parametroEmail != null && PROC_ANDAMENTO_PRAZO_VENCIDO.equals(parametroEmail.getId())) {
            result = DateUtils.formataDataBrasil(andamento.getDataFinalPrazoFatal());
        }
    }

}
