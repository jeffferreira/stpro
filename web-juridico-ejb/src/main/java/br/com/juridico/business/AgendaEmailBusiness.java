package br.com.juridico.business;

import br.com.juridico.entidades.Evento;
import br.com.juridico.entidades.ParametroEmail;
import br.com.juridico.entidades.User;
import br.com.juridico.util.DateUtils;
import com.google.common.base.Strings;

/**
 * Created by jefferson on 25/10/2016.
 */
public class AgendaEmailBusiness {

    private AgendaEmailBusiness(){}

    private static Long AGENDA_TITULO = 3L;
    private static Long AGENDA_DATAHORA_INICIOFIM_DE = 4L;
    private static Long AGENDA_DATAHORA_INICIO_FIM_PARA = 5L;
    private static Long AGENDA_STATUS_DE = 6L;
    private static Long AGENDA_STATUS_PARA = 7L;
    private static Long AGENDA_RESPONSAVEL_DE = 8L;
    private static Long AGENDA_RESPONSAVEL_PARA = 9L;
    private static Long AGENDA_DADOS_EVENTO_AGENDA_DE = 15L;
    private static Long AGENDA_DADOS_EVENTO_AGENDA_PARA = 16L;

    private static String result;

    public static String execute(ParametroEmail parametroEmail, Evento eventoDe, Evento eventoPara, User user){
        result = "";
        if(!Strings.isNullOrEmpty(parametroEmail.getChaveSessao()) &&
                parametroEmail.getChaveSessao().startsWith("AGENDA_")) {

            tituloAgenda(parametroEmail, eventoDe);
            dataHoraInicioFimEventoDe(parametroEmail, eventoDe);
            dataHoraInicioFimEventoPara(parametroEmail, eventoPara);
            statusEventoDe(parametroEmail, eventoDe);
            statusEventoPara(parametroEmail, eventoPara);
            responsavelEventoDe(parametroEmail, eventoDe);
            responsavelEventoPara(parametroEmail, eventoPara);
            dadosEventoAgendaDe(parametroEmail, eventoDe);
            dadosEventoAgendaPara(parametroEmail, eventoPara);
        }
        return result.isEmpty() ? GeralEmailBusiness.execute(parametroEmail, user) : result;
    }

    private static void tituloAgenda(ParametroEmail parametroEmail, Evento evento){
        if(parametroEmail != null && AGENDA_TITULO.equals(parametroEmail.getId())) {
            result = evento.getDescricao();
        }
    }

    private static void dadosEventoAgendaDe(ParametroEmail parametroEmail, Evento evento){
        if(parametroEmail != null && AGENDA_DADOS_EVENTO_AGENDA_DE.equals(parametroEmail.getId()) && evento != null) {
            String dataHoraInicio = DateUtils.formataDataHoraBrasil(evento.getDataInicio());
            String dataHoraFim = DateUtils.formataDataHoraBrasil(evento.getDataFim());

            result = "<b>PROCESSO: </b>".concat(evento.getDescricaoProcesso()).
                    concat("<br><b> TÍTULO: </b>").concat(evento.getDescricao()).
                    concat("<br><b> LOCAL: </b>").concat(evento.getLocalizacao() == null ? "" : evento.getLocalizacao()).
                    concat("<br><b> DE: </b>").concat(dataHoraInicio).
                    concat("<b> Até: </b>").concat(dataHoraFim).
                    concat("<br><b> STATUS: </b>").concat(evento.getStatusAgenda().getDescricao());
        }
    }

    private static void dadosEventoAgendaPara(ParametroEmail parametroEmail, Evento evento){
        if(parametroEmail != null && AGENDA_DADOS_EVENTO_AGENDA_PARA.equals(parametroEmail.getId()) && evento != null && evento.getId() != null) {
            String dataHoraInicio = DateUtils.formataDataHoraBrasil(evento.getDataInicio());
            String dataHoraFim = DateUtils.formataDataHoraBrasil(evento.getDataFim());

            result = "<b>NOVO EVENTO/PRAZO </b><br>".
                    concat("<br><b>PROCESSO: </b>").concat(evento.getDescricaoProcesso()).
                    concat("<br><b> TÍTULO: </b>").concat(evento.getDescricao()).
                    concat("<br><b> LOCAL: </b>").concat(evento.getLocalizacao() == null ? "" : evento.getLocalizacao()).
                    concat("<br><b> DE: </b>").concat(dataHoraInicio).
                    concat("<b> Até: </b>").concat(dataHoraFim).
                    concat("<br><b> STATUS: </b>").concat(evento.getStatusAgenda().getDescricao());
        }
    }

    private static void dataHoraInicioFimEventoDe(ParametroEmail parametroEmail, Evento evento){
        if(parametroEmail != null && AGENDA_DATAHORA_INICIOFIM_DE.equals(parametroEmail.getId())) {
            String dataHoraInicio = DateUtils.formataDataHoraBrasil(evento.getDataInicio());
            String dataHoraFim = DateUtils.formataDataHoraBrasil(evento.getDataFim());
            result = dataHoraInicio.concat(" até ").concat(dataHoraFim);
        }
    }

    private static void dataHoraInicioFimEventoPara(ParametroEmail parametroEmail, Evento evento){
        if(parametroEmail != null && AGENDA_DATAHORA_INICIO_FIM_PARA.equals(parametroEmail.getId())) {
            String dataHoraInicio = DateUtils.formataDataHoraBrasil(evento.getDataInicio());
            String dataHoraFim = DateUtils.formataDataHoraBrasil(evento.getDataFim());
            result = dataHoraInicio.concat(" até ").concat(dataHoraFim);
        }
    }

    private static void statusEventoDe(ParametroEmail parametroEmail, Evento evento){
        if(parametroEmail != null && AGENDA_STATUS_DE.equals(parametroEmail.getId())) {
            result = evento.getStatusAgenda().getDescricao();
        }
    }

    private static void statusEventoPara(ParametroEmail parametroEmail, Evento evento){
        if(parametroEmail != null && AGENDA_STATUS_PARA.equals(parametroEmail.getId())) {
            result = evento.getStatusAgenda().getDescricao();
        }
    }

    private static void responsavelEventoDe(ParametroEmail parametroEmail, Evento evento){
        if(parametroEmail != null && AGENDA_RESPONSAVEL_DE.equals(parametroEmail.getId())) {
            if(evento.getPessoa() != null) {
                result = evento.getPessoa().getDescricao();
                return;
            }
        }
    }

    private static void responsavelEventoPara(ParametroEmail parametroEmail, Evento evento){
        if(parametroEmail != null && AGENDA_RESPONSAVEL_PARA.equals(parametroEmail.getId())) {
            if(evento.getPessoa() != null) {
                result = evento.getPessoa().getDescricao();
                return;
            }
        }
    }
}
