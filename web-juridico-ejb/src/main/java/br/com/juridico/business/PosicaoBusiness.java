package br.com.juridico.business;

import br.com.juridico.entidades.Posicao;

public class PosicaoBusiness {

	private static final Long AUTOR = 1L;
	private static final Long REU = 2L;
	private static final Long TERCEIRO = 3L;

	private PosicaoBusiness() {
	}

	public static boolean isAutor(Posicao posicao) {
		return posicao != null && AUTOR.equals(posicao.getId());
	}

	public static boolean isReu(Posicao posicao) {
		return posicao != null && REU.equals(posicao.getId());
	}

	public static boolean isTerceiro(Posicao posicao) {
		return posicao != null && TERCEIRO.equals(posicao.getId());
	}
}
