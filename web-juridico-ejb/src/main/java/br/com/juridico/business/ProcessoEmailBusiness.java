package br.com.juridico.business;

import br.com.juridico.entidades.ParametroEmail;
import br.com.juridico.entidades.Parte;
import br.com.juridico.entidades.Processo;
import br.com.juridico.entidades.User;
import br.com.juridico.util.DateUtils;
import com.google.common.base.Strings;

/**
 * Created by Marcos on 03/06/2018.
 */
public class ProcessoEmailBusiness {

    private static final String PROCESSO = "PROC_";
    private static Long PROC_NUMERO_PROCESSO = 18L;
    private static Long PROC_CLASSE_PROCESSUAL = 19L;
    private static Long PROC_FORUM = 20L;
    private static Long PROC_VARA = 21L;
    private static Long PROC_DATA_ABERTURA = 22L;
    private static Long PROC_NATUREZA_ACAO = 23L;
    private static Long PROC_COMARCA = 24L;
    private static Long PROC_NUMERACAO_SECUNDARIA = 25L;
    private static Long PROC_STATUS_PROCESSO = 26L;
    private static Long PROC_MEIO_TRAMITACAO = 27L;
    private static Long PROC_FASE_PROCESSO = 28L;
    private static Long PROC_DATA_CITACAO = 29L;
    private static Long PROC_RESUMO_ACAO = 30L;


    private static String result;

    public static String execute(ParametroEmail parametroEmail, Processo processo, User user){
        result = "";

        if(!Strings.isNullOrEmpty(parametroEmail.getChaveSessao()) &&
                parametroEmail.getChaveSessao().startsWith(PROCESSO)) {
            processoNumeroProcesso(parametroEmail, processo);
            processoClasseProcessual(parametroEmail, processo);
            processoForum(parametroEmail, processo);
            processoVara(parametroEmail, processo);
            processoDataAbertura(parametroEmail, processo);
            processoNaturezaAcao(parametroEmail, processo);
            processoComarca(parametroEmail, processo);
            processoNumeracaoSecundaria(parametroEmail, processo);
            processoStatusProcesso(parametroEmail, processo);
            processoMeioTramitacao(parametroEmail, processo);
            processoFaseProcesso(parametroEmail, processo);
            processoDataCitacao(parametroEmail, processo);
            processoResumoAcao(parametroEmail, processo);
        }

        return result.isEmpty() ? GeralEmailBusiness.execute(parametroEmail, user) : result;
    }

    private static void processoNumeroProcesso(ParametroEmail parametroEmail, Processo processo){
        if(parametroEmail != null && PROC_NUMERO_PROCESSO.equals(parametroEmail.getId())) {
            result = processo.getDescricao();
        }
    }

    private static void processoClasseProcessual(ParametroEmail parametroEmail, Processo processo){
        if(parametroEmail != null && PROC_CLASSE_PROCESSUAL.equals(parametroEmail.getId())) {
            result = processo.getClasseProcessual().getDescricao();
        }
    }

    private static void processoForum(ParametroEmail parametroEmail, Processo processo){
        if(parametroEmail != null && PROC_FORUM.equals(parametroEmail.getId())) {
            result = processo.getForum().getDescricao();
        }
    }

    private static void processoVara(ParametroEmail parametroEmail, Processo processo){
        if(parametroEmail != null && PROC_VARA.equals(parametroEmail.getId())) {
            result = processo.getVara().getDescricao();
        }
    }

    private static void processoDataAbertura(ParametroEmail parametroEmail, Processo processo){
        if(parametroEmail != null && PROC_DATA_ABERTURA.equals(parametroEmail.getId())) {
            result = DateUtils.formataDataBrasil(processo.getDataAbertura());
        }
    }

    private static void processoNaturezaAcao(ParametroEmail parametroEmail, Processo processo){
        if(parametroEmail != null && PROC_NATUREZA_ACAO.equals(parametroEmail.getId())) {
            result = processo.getNaturezaAcao().getDescricao();
        }
    }

    private static void processoComarca(ParametroEmail parametroEmail, Processo processo){
        if(parametroEmail != null && PROC_COMARCA.equals(parametroEmail.getId())) {
            result = processo.getComarca().getDescricao();
        }
    }

    private static void processoNumeracaoSecundaria(ParametroEmail parametroEmail, Processo processo){
        if(parametroEmail != null && PROC_NUMERACAO_SECUNDARIA.equals(parametroEmail.getId())) {
            result = processo.getNumeracaoSecundaria();
        }
    }

    private static void processoStatusProcesso(ParametroEmail parametroEmail, Processo processo){
        if(parametroEmail != null && PROC_STATUS_PROCESSO.equals(parametroEmail.getId())) {
            result = processo.getDescricaoStatus();
        }
    }

    private static void processoMeioTramitacao(ParametroEmail parametroEmail, Processo processo){
        if(parametroEmail != null && PROC_MEIO_TRAMITACAO.equals(parametroEmail.getId())) {
            result = processo.getMeioTramitacao().getDescricao();
        }
    }

    private static void processoFaseProcesso(ParametroEmail parametroEmail, Processo processo){
        if(parametroEmail != null && PROC_FASE_PROCESSO.equals(parametroEmail.getId())) {
            result = processo.getFase().getDescricao();
        }
    }

    private static void processoDataCitacao(ParametroEmail parametroEmail, Processo processo){
        if(parametroEmail != null && PROC_DATA_CITACAO.equals(parametroEmail.getId())) {
            result = DateUtils.formataDataBrasil(processo.getDataCitacao());
        }
    }

    private static void processoResumoAcao(ParametroEmail parametroEmail, Processo processo){
        if(parametroEmail != null && PROC_RESUMO_ACAO.equals(parametroEmail.getId())) {
            result = processo.getResumoAcaoSemHtml();
        }
    }

}
