package br.com.juridico.business;

import br.com.juridico.entidades.Andamento;
import br.com.juridico.entidades.ParametroEmail;
import br.com.juridico.entidades.User;
import br.com.juridico.util.DateUtils;
import com.google.common.base.Strings;

/**
 * Created by jefferson on 11/12/2016.
 */
public class PrazoFinalEmailBusiness {

    private static final String PROCESSO = "PROC_";
    private static Long PROC_ANDAMENTO_PRAZO_FINAL = 13L;
    private static Long PROC_ANDAMENTO_TIPO = 14L;


    private static String result;

    public static String execute(ParametroEmail parametroEmail, Andamento andamento, User user){
        result = "";

        if(!Strings.isNullOrEmpty(parametroEmail.getChaveSessao()) &&
                parametroEmail.getChaveSessao().startsWith(PROCESSO)) {
            prazoFinalAndamento(parametroEmail, andamento);
            tipoAndamento(parametroEmail, andamento);
        }

        return result.isEmpty() ? GeralEmailBusiness.execute(parametroEmail, user) : result;
    }

    private static void prazoFinalAndamento(ParametroEmail parametroEmail, Andamento andamento){
        if(parametroEmail != null && PROC_ANDAMENTO_PRAZO_FINAL.equals(parametroEmail.getId())) {
            result = DateUtils.formataDataBrasil(andamento.getDataFinalPrazoFatal());
        }
    }

    private static void tipoAndamento(ParametroEmail parametroEmail, Andamento andamento){
        if(parametroEmail != null && PROC_ANDAMENTO_TIPO.equals(parametroEmail.getId())) {
            result = andamento.getTipoAndamento().getDescricao();
        }
    }
}
