package br.com.juridico.business;

/**
 * 
 * @author Jefferson
 *
 */
public class CalculaRegraTresBusiness {

    private CalculaRegraTresBusiness() {
    }

    /**
     * 
     * @param especificos
     * @param total
     * @return
     */
    public static Double calcula(int especificos, int total) {
        return 100 * especificos / (double) total;
    }
}
