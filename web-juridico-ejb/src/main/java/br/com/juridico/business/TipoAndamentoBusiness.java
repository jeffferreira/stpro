package br.com.juridico.business;

import br.com.juridico.entidades.TipoAndamento;

public class TipoAndamentoBusiness {

	private static final Long AUDIENCIA_CONCILIACAO = 1L;
	private static final Long AUDIENCIA_OITIVA = 2L;
	private static final Long AUDIENCIA_INSTRUCAO = 3L;

	private TipoAndamentoBusiness() {
	}

	public static boolean isAudienciaConciliacao(TipoAndamento tipoAndamento) {
		return tipoAndamento != null && AUDIENCIA_CONCILIACAO.equals(tipoAndamento.getId());
	}

	public static boolean isAudienciaOitiva(TipoAndamento tipoAndamento) {
		return tipoAndamento != null && AUDIENCIA_OITIVA.equals(tipoAndamento.getId());
	}

	public static boolean isAudienciaInstrucao(TipoAndamento tipoAndamento) {
		return tipoAndamento != null && AUDIENCIA_INSTRUCAO.equals(tipoAndamento.getId());
	}
}
