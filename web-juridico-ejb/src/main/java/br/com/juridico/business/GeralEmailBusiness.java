package br.com.juridico.business;

import br.com.juridico.entidades.ParametroEmail;
import br.com.juridico.entidades.User;
import br.com.juridico.util.DateUtils;
import com.google.common.base.Strings;

import java.util.Calendar;

/**
 * Created by jefferson on 25/10/2016.
 */
public class GeralEmailBusiness {

    protected static Long GERAL_NOME_RESPONSAVEL = 1L;
    protected static Long GERAL_DATAHORA_ALTERACAO = 2L;

    private static String result;

    public static String execute(ParametroEmail parametroEmail, User user){
        result = "";
        if(!Strings.isNullOrEmpty(parametroEmail.getChaveSessao()) &&
                parametroEmail.getChaveSessao().startsWith("GERAL_")) {
            nomeResponsavel(parametroEmail, user);
            dtahoraAlteracao(parametroEmail);
        }
        return result;
    }

    private static void nomeResponsavel(ParametroEmail parametroEmail, User user){
        if(parametroEmail != null && GERAL_NOME_RESPONSAVEL.equals(parametroEmail.getId())) {
            result = user.getName();
        }
    }

    private static void dtahoraAlteracao(ParametroEmail parametroEmail){
        if(parametroEmail != null && GERAL_DATAHORA_ALTERACAO.equals(parametroEmail.getId())) {
            result = DateUtils.formataDataHoraBrasil(Calendar.getInstance().getTime());
        }
    }
}
