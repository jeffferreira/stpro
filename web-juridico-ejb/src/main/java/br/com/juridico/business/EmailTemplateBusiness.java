package br.com.juridico.business;

import com.google.common.collect.Maps;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import java.io.StringWriter;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by jefferson on 17/04/2018.
 */
public class EmailTemplateBusiness {

    private static final String UTF_8 = "UTF-8";

    private EmailTemplateBusiness(){}

    public static String criarEmailTemplate(String titulo, String texto, Map<String, String> parametros){
        VelocityEngine ve = new VelocityEngine();
        Properties properties = new Properties();
        properties.setProperty("resource.loader", "class");
        properties.setProperty("input.encoding", UTF_8);
        properties.setProperty("output.encoding", UTF_8);
        properties.setProperty("response.encoding", UTF_8);
        properties.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        ve.init(properties);

        VelocityContext context = new VelocityContext();
        for(Map.Entry param : parametros.entrySet()){
            context.put((String) param.getKey(), param.getValue());
        }
        Template t = ve.getTemplate("templates/mail.vm", UTF_8);
        StringWriter writer = new StringWriter();
        t.merge(context, writer);

        StringBuilder template = new StringBuilder(writer.toString());
        template.replace(template.indexOf("TITULO_EMAIL<"), template.indexOf(">TITULO_EMAIL")+13, titulo);
        template.replace(template.indexOf("CORPO_EMAIL<"), template.indexOf(">CORPO_EMAIL")+12, ParametroEmailBusiness.formataEmailComParametros(parametros, texto));

        return template.toString();
    }

}
