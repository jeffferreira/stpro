package br.com.juridico.business;

import br.com.juridico.entidades.User;
import br.com.juridico.enums.MenuIndicador;
import com.google.common.base.Strings;

/**
 *
 * @author Jefferson
 *
 */
public class MenuAcessoBusiness {

	private MenuAcessoBusiness() {
		super();
	}

	public static boolean isRoleMenuAgenda(final User user) {
		return containsAcesso(user, MenuIndicador.AGENDA);
	}

	public static boolean isRoleMenuAdvogados(final User user) {
		return containsAcesso(user, MenuIndicador.ADVOGADOS);
	}

	public static boolean isRoleMenuAtendimento(final User user) {
		return containsAcesso(user, MenuIndicador.ATENDIMENTO);
	}

	public static boolean isRoleMenuCadastros(final User user) {
		return containsAcesso(user, MenuIndicador.CADASTROS);
	}

	public static boolean isRoleMenuControladoria(final User user) {
		return containsAcesso(user, MenuIndicador.CONTROLADORIA);
	}

	public static boolean isRoleMenuFinanceiro(final User user) {
		return containsAcesso(user, MenuIndicador.FINANCEIRO);
	}

	public static boolean isRoleMenuContratos(final User user) {
		return containsAcesso(user, MenuIndicador.CONTRATOS);
	}

	public static boolean isRoleMenuPessoas(final User user) {
		return containsAcesso(user, MenuIndicador.PESSOAS);
	}

	public static boolean isRoleMenuProcessos(final User user) {
		return containsAcesso(user, MenuIndicador.PROCESSOS);
	}

	public static boolean isRoleMenuRelatorios(final User user) {
		return containsAcesso(user, MenuIndicador.RELATORIOS);
	}

	public static boolean isRoleMenuAcessos(final User user) {
		return containsAcesso(user, MenuIndicador.ACESSOS);
	}

	private static boolean containsAcesso(final User user, MenuIndicador menuIndicador) {
		return !Strings.isNullOrEmpty(user.getAcessosMap().get(menuIndicador.getDescricao()));
	}

}
