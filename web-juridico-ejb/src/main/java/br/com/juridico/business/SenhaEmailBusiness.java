package br.com.juridico.business;

import br.com.juridico.entidades.ParametroEmail;
import br.com.juridico.entidades.Pessoa;
import br.com.juridico.entidades.User;
import br.com.juridico.util.DecryptUtil;
import com.google.common.base.Strings;
import org.apache.log4j.Logger;

/**
 * Created by jefferson on 25/10/2016.
 */
public class SenhaEmailBusiness {

    private static final Logger LOGGER = Logger.getLogger(SenhaEmailBusiness.class);

    protected static Long SENHA_DE = 10L;
    protected static Long SENHA_PARA = 11L;
    protected static Long SENHA_NOME_USUARIO = 12L;

    private static String result;

    public static String execute(ParametroEmail parametroEmail, Pessoa pessoaDe, Pessoa pessoaPara, User user){
        result = "";
        if(!Strings.isNullOrEmpty(parametroEmail.getChaveSessao()) &&
                parametroEmail.getChaveSessao().startsWith("SENHA_")) {
            senhaUsuario(parametroEmail, pessoaDe);
            novaSenhaUsuario(parametroEmail, pessoaPara);
            nomeUsuario(parametroEmail, pessoaDe);
        }
        return result.isEmpty() ? GeralEmailBusiness.execute(parametroEmail, user) : result;
    }

    private static void senhaUsuario(ParametroEmail parametroEmail, Pessoa pessoaDe){
        if(parametroEmail != null && SENHA_DE.equals(parametroEmail.getId())) {
            try {
                String senha = DecryptUtil.execute(pessoaDe.getLogin().getSenha());
                result = senha;
            } catch (Exception e) {
                LOGGER.error(e, e.getCause());
            }
        }
    }

    private static void novaSenhaUsuario(ParametroEmail parametroEmail, Pessoa pessoaPara){
        if(parametroEmail != null && SENHA_PARA.equals(parametroEmail.getId())) {
            try {
                String senha = DecryptUtil.execute(pessoaPara.getLogin().getSenha());
                result = senha;
            } catch (Exception e) {
                LOGGER.error(e, e.getCause());
            }
        }
    }

    private static void nomeUsuario(ParametroEmail parametroEmail, Pessoa pessoaDe){
        if(parametroEmail != null && SENHA_NOME_USUARIO.equals(parametroEmail.getId())) {
            result = pessoaDe.getDescricao();
        }
    }
}
