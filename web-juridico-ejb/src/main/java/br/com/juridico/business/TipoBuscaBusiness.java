package br.com.juridico.business;

/**
 * Created by Jefferson on 09/04/2016.
 */
public class TipoBuscaBusiness {

    private TipoBuscaBusiness(){}

    public static final int BUSCA_PROCESSO = 1;
    public static final int BUSCA_STATUS = 2;
    public static final int BUSCA_DATA_ABERTURA = 3;
    public static final int BUSCA_FASE = 4;
    public static final int BUSCA_CLASSE_PROCESSUAL = 5;
    public static final int BUSCA_NATUREZA_ACAO = 6;
    public static final int BUSCA_MEIO_TRAMITACAO = 7;
    public static final int BUSCA_VARA = 8;
    public static final int BUSCA_FORUM = 9;
    public static final int BUSCA_COMARCA = 10;
    public static final int BUSCA_GRAU_RISCO = 11;


}
