package br.com.juridico.comparator;

import br.com.juridico.entidades.HistoricoEvento;

import java.util.Comparator;

/**
 * Created by jeffersonl2009_00 on 08/12/2016.
 */
public class HistoricoEventoComparator implements Comparator<HistoricoEvento> {

    @Override
    public int compare(HistoricoEvento o1, HistoricoEvento o2) {
        return o2.getDataHoraAlteracao().compareTo(o1.getDataHoraAlteracao());
    }
}
