package br.com.juridico.comparator;

import br.com.juridico.entidades.Andamento;
import br.com.juridico.entidades.Evento;

import java.util.Comparator;

/**
 * Created by jeffersonl2009_00 on 08/12/2016.
 */
public class AndamentoComparator implements Comparator<Andamento> {

    @Override
    public int compare(Andamento o1, Andamento o2) {
        return o2.getDataAndamento().compareTo(o1.getDataAndamento());
    }
}
