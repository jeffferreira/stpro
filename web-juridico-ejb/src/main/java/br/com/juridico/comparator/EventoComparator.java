package br.com.juridico.comparator;

import br.com.juridico.entidades.Evento;

import java.util.Comparator;

/**
 * Created by jeffersonl2009_00 on 08/12/2016.
 */
public class EventoComparator implements Comparator<Evento> {

    @Override
    public int compare(Evento o1, Evento o2) {
        return o2.getDataHoraAlteracao().compareTo(o1.getDataHoraAlteracao());
    }
}
