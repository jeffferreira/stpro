package br.com.juridico.comparator;

import br.com.juridico.entidades.ProjudiprMovimentacao;

import java.util.Comparator;

/**
 * Created by jefferson on 25/03/2017.
 */
public class MovimentacaoTramiteComparator implements Comparator<ProjudiprMovimentacao> {

    public int compare(ProjudiprMovimentacao p1, ProjudiprMovimentacao p2) {
        int sequencia1 = Integer.parseInt(p1.getSequencia());
        int sequencia2 = Integer.parseInt(p2.getSequencia());
        return sequencia2 - sequencia1;
    }
}
