package br.com.juridico.exception;

/**
 * 
 * @author Jefferson
 *
 */
public class InvalidHeaderException extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * 
	 * @param string
	 */
	public InvalidHeaderException(String string) {
		super(string);
	}
}
