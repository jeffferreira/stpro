package br.com.juridico.exception;

/**
 * 
 * @author Jefferson
 *
 */
public class InvalidHashException extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * 
	 * @param string
	 */
	public InvalidHashException(String string) {
		super(string);
	}
}
