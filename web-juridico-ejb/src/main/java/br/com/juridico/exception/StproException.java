package br.com.juridico.exception;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

/**
 * 
 * @author Jefferson
 *
 */
public class StproException extends Exception {

	private static final long serialVersionUID = 1L;

	private final List<String> mensagemList = new ArrayList<>();

	public StproException() {
		super();
	}

	/**
	 * 
	 * @param errorMessage
	 */
	public StproException(String errorMessage) {
		super(errorMessage);
		adicionarMensagemDetalhe(errorMessage);
	}

	/**
	 * 
	 * @param erros
	 * @param errorMessage
	 */
	public StproException(List<String> erros, String errorMessage) {
		super(errorMessage);
		mensagemList.addAll(erros);
	}

	/**
	 * 
	 * @param mensagem
	 */
	public void adicionarMensagemDetalhe(String mensagem) {
		mensagemList.add(mensagem);
	}

	/**
	 * 
	 * @param mensagemList
	 */
	public void adicionarMensagemDetalhe(List<String> mensagemList) {
		this.mensagemList.addAll(mensagemList);
	}

	/**
	 * 
	 * @return
	 */
	public List<String> getMensagemListDetalhe() {
		return new ArrayList<>(mensagemList);
	}

	/**
	 * 
	 * @return
	 */
	public boolean isError() {
		return !mensagemList.isEmpty()
				|| (this.getLocalizedMessage() != null && !StringUtils.isWhitespace(this.getLocalizedMessage()));
	}

}
