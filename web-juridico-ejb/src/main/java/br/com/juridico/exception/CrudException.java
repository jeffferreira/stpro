package br.com.juridico.exception;

/**
 * 
 * @author Jefferson
 *
 */
public class CrudException extends StproException {

	private static final long serialVersionUID = 1L;

	public CrudException() {
		// CONSTRUTOR
	}

	/**
	 * 
	 * @param message
	 */
	public CrudException(String message) {
		super(message);
	}

}
