package br.com.juridico.exception;

import java.util.List;

/**
 * 
 * @author Jefferson
 *
 */
public class ValidationException extends Exception {

	private static final long serialVersionUID = 1L;

	private transient Object[] violations;

	private List<String> messages;

	private String violation;

	/**
	 * 
	 * @param violation
	 */
	public ValidationException(final String violation) {
		this.violation = violation;
	}

	/**
	 * 
	 * @param violations
	 */
	public ValidationException(final Object[] violations) {
		this.violations = violations;
	}

	/**
	 * 
	 * @param violations
	 * @param messages
	 */
	public ValidationException(final Object[] violations, final List<String> messages) {
		this.violations = violations;
		this.messages = messages;
	}

	public Object[] getViolations() {
		return violations;
	}

	public List<String> getMessages() {
		return messages;
	}

	public String getViolation() {
		return violation;
	}
}
