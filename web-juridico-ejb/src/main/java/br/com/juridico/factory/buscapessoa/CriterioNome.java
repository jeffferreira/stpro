package br.com.juridico.factory.buscapessoa;

import br.com.juridico.dto.CriterioBuscaDto;
import br.com.juridico.factory.Criterio;

/**
 * Created by Jefferson on 13/04/2016.
 */
public class CriterioNome implements Criterio {

    @Override
    public CriterioBuscaDto criterioBusca(String textFilter) {
        return new CriterioBuscaDto(BuscaPessoaJuridicaFactory.BUSCA_NOME, textFilter);
    }
}
