package br.com.juridico.factory.buscaprocesso;

import br.com.juridico.business.TipoBuscaBusiness;
import br.com.juridico.dto.CriterioBuscaDto;
import br.com.juridico.factory.Criterio;

/**
 * Created by Jefferson on 10/04/2016.
 */
public class CriterioMeioTramitacao implements Criterio {

    @Override
    public CriterioBuscaDto criterioBusca(String textFilter) {
        return new CriterioBuscaDto(TipoBuscaBusiness.BUSCA_MEIO_TRAMITACAO, textFilter);
    }
}
