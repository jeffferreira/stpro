package br.com.juridico.factory;

import br.com.juridico.business.TipoBuscaBusiness;
import br.com.juridico.dto.CriterioBuscaDto;
import br.com.juridico.entidades.ViewProcesso;
import br.com.juridico.factory.buscaprocesso.BuscaProcessoFactory;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * Created by Jefferson on 09/04/2016.
 */
public class ContextProcessoSearchFactory {
	
	private ContextProcessoSearchFactory(){}

	/**
	 * 
	 * @param textFilter
	 * @param codigoEmpresa
	 * @param cb
	 * @param root
	 * @return
	 */
    public static Predicate criterios(String textFilter, Long codigoEmpresa, CriteriaBuilder cb, Root<ViewProcesso> root){
        BuscaProcessoFactory factory = new BuscaProcessoFactory();
        Predicate condition = cb.conjunction();

        condition = cb.and(condition, cb.equal(root.get("numeroEmpresa"), codigoEmpresa));
        condition = adicionaCriteriosProcesso(textFilter, cb, root, factory, condition);
        condition = adicionaCriteriosStatus(textFilter, cb, root, factory, condition);
        condition = adicionaCriteriosDataAbertura(textFilter, cb, root, factory, condition);
        condition = adicionaCriteriosFase(textFilter, cb, root, factory, condition);
        condition = adicionaCriteriosClasseProcessual(textFilter, cb, root, factory, condition);
        condition = adicionaCriteriosNaturezaAcao(textFilter, cb, root, factory, condition);
        condition = adicionaCriteriosMeioTramitacao(textFilter, cb, root, factory, condition);
        condition = adicionaCriteriosVara(textFilter, cb, root, factory, condition);
        condition = adicionaCriteriosForum(textFilter, cb, root, factory, condition);
        condition = adicionaCriteriosComarca(textFilter, cb, root, factory, condition);
        condition = adicionaCriteriosGrauRisco(textFilter, cb, root, factory, condition);

        return condition;
    }


    private static Predicate adicionaCriteriosProcesso(String textFilter, CriteriaBuilder cb, Root<ViewProcesso> root, BuscaProcessoFactory factory, Predicate condition) {
        CriterioBuscaDto dto = factory.getCriterio(TipoBuscaBusiness.BUSCA_PROCESSO).criterioBusca(textFilter);
        condition = cb.and(condition, cb.like(root.get("processo"), ("%").concat(dto.getValorBusca().toString()).concat("%")));
        return condition;
    }

    private static Predicate adicionaCriteriosDataAbertura(String textFilter, CriteriaBuilder cb, Root<ViewProcesso> root, BuscaProcessoFactory factory, Predicate condition) {
        CriterioBuscaDto dto = factory.getCriterio(TipoBuscaBusiness.BUSCA_DATA_ABERTURA).criterioBusca(textFilter);
        condition = cb.or(condition, cb.like(root.get("dataAberturaTexto"), ("%").concat(dto.getValorBusca().toString()).concat("%")));
        return condition;
    }

    private static Predicate adicionaCriteriosStatus(String textFilter, CriteriaBuilder cb, Root<ViewProcesso> root, BuscaProcessoFactory factory, Predicate condition) {
        CriterioBuscaDto dto = factory.getCriterio(TipoBuscaBusiness.BUSCA_STATUS).criterioBusca(textFilter);
        condition = cb.or(condition, cb.like(root.get("status"), dto.getValorBusca().toString().concat("%")));
        return condition;
    }

    private static Predicate adicionaCriteriosFase(String textFilter, CriteriaBuilder cb, Root<ViewProcesso> root, BuscaProcessoFactory factory, Predicate condition) {
        CriterioBuscaDto dto = factory.getCriterio(TipoBuscaBusiness.BUSCA_FASE).criterioBusca(textFilter);
        condition = cb.or(condition, cb.like(root.get("fase"), dto.getValorBusca().toString().concat("%")));
        return condition;
    }

    private static Predicate adicionaCriteriosClasseProcessual(String textFilter, CriteriaBuilder cb, Root<ViewProcesso> root, BuscaProcessoFactory factory, Predicate condition) {
        CriterioBuscaDto dto = factory.getCriterio(TipoBuscaBusiness.BUSCA_CLASSE_PROCESSUAL).criterioBusca(textFilter);
        condition = cb.or(condition, cb.like(root.get("classeProcessual"), dto.getValorBusca().toString().concat("%")));
        return condition;
    }

    private static Predicate adicionaCriteriosNaturezaAcao(String textFilter, CriteriaBuilder cb, Root<ViewProcesso> root, BuscaProcessoFactory factory, Predicate condition) {
        CriterioBuscaDto dto = factory.getCriterio(TipoBuscaBusiness.BUSCA_NATUREZA_ACAO).criterioBusca(textFilter);
        condition = cb.or(condition, cb.like(root.get("naturezaAcao"), dto.getValorBusca().toString().concat("%")));
        return condition;
    }

    private static Predicate adicionaCriteriosMeioTramitacao(String textFilter, CriteriaBuilder cb, Root<ViewProcesso> root, BuscaProcessoFactory factory, Predicate condition) {
        CriterioBuscaDto dto = factory.getCriterio(TipoBuscaBusiness.BUSCA_MEIO_TRAMITACAO).criterioBusca(textFilter);
        condition = cb.or(condition, cb.like(root.get("meioTramitacao"), dto.getValorBusca().toString().concat("%")));
        return condition;
    }

    private static Predicate adicionaCriteriosVara(String textFilter, CriteriaBuilder cb, Root<ViewProcesso> root, BuscaProcessoFactory factory, Predicate condition) {
        CriterioBuscaDto dto = factory.getCriterio(TipoBuscaBusiness.BUSCA_VARA).criterioBusca(textFilter);
        condition = cb.or(condition, cb.like(root.get("vara"), dto.getValorBusca().toString().concat("%")));
        return condition;
    }

    private static Predicate adicionaCriteriosForum(String textFilter, CriteriaBuilder cb, Root<ViewProcesso> root, BuscaProcessoFactory factory, Predicate condition) {
        CriterioBuscaDto dto = factory.getCriterio(TipoBuscaBusiness.BUSCA_FORUM).criterioBusca(textFilter);
        condition = cb.or(condition, cb.like(root.get("forum"), dto.getValorBusca().toString().concat("%")));
        return condition;
    }

    private static Predicate adicionaCriteriosComarca(String textFilter, CriteriaBuilder cb, Root<ViewProcesso> root, BuscaProcessoFactory factory, Predicate condition) {
        CriterioBuscaDto dto = factory.getCriterio(TipoBuscaBusiness.BUSCA_COMARCA).criterioBusca(textFilter);
        condition = cb.or(condition, cb.like(root.get("comarca"), dto.getValorBusca().toString().concat("%")));
        return condition;
    }

    private static Predicate adicionaCriteriosGrauRisco(String textFilter, CriteriaBuilder cb, Root<ViewProcesso> root, BuscaProcessoFactory factory, Predicate condition) {
        CriterioBuscaDto dto = factory.getCriterio(TipoBuscaBusiness.BUSCA_GRAU_RISCO).criterioBusca(textFilter);
        condition = cb.or(condition, cb.like(root.get("grauRisco"), dto.getValorBusca().toString().concat("%")));
        return condition;
    }
}
