package br.com.juridico.factory;

import br.com.juridico.dto.CriterioBuscaDto;

/**
 * Created by Jefferson on 09/04/2016.
 */
public interface Criterio {

    /**
     *
     * @param textFilter
     * @return
     */
    CriterioBuscaDto criterioBusca(String textFilter);
}
