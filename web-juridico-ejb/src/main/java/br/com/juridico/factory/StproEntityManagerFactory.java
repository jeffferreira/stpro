/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.juridico.factory;

import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import br.com.juridico.annotation.JuridicoEntityManager;

/**
 *
 * @author Jefferson
 */
public class StproEntityManagerFactory {

   @PersistenceUnit(unitName = "webjuridicoPU")
   private EntityManagerFactory emf;

   /**
    * 
    * @return
    */
   @Produces
   @JuridicoEntityManager
   public EntityManager createEntityManager() {
      return emf.createEntityManager();
   }

   /**
    * 
    * @param entityManager
    */
   public void close(@Disposes @JuridicoEntityManager EntityManager entityManager) {
      if (entityManager != null && entityManager.isOpen()) {
         entityManager.close();
      }
   }
}
