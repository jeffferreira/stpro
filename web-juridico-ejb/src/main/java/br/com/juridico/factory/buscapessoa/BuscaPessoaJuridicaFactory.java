package br.com.juridico.factory.buscapessoa;

import br.com.juridico.factory.Criterio;

/**
 * Created by Jefferson on 13/04/2016.
 */
public class BuscaPessoaJuridicaFactory {

	public static final int BUSCA_NOME = 1;
	public static final int BUSCA_RESPONSAVEL = 2;
	public static final int BUSCA_CNPJ = 3;
	public static final int BUSCA_LOGIN = 4;

	/**
	 * 
	 * @param criterio
	 * @return
	 */
	public Criterio getCriterio(int criterio) {
		switch (criterio) {
		case BUSCA_NOME:
			return new CriterioNome();
		case BUSCA_RESPONSAVEL:
			return new CriterioResponsavel();
		case BUSCA_CNPJ:
			return new CriterioCnpj();
		case BUSCA_LOGIN:
			return new CriterioLogin();
		default:
			return null;
		}
	}
}
