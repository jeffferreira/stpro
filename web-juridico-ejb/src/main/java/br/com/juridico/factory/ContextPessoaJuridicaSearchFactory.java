package br.com.juridico.factory;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import br.com.juridico.dto.CriterioBuscaDto;
import br.com.juridico.entidades.PessoaJuridica;
import br.com.juridico.factory.buscapessoa.BuscaPessoaJuridicaFactory;

/**
 * Created by Jefferson on 13/04/2016.
 */
public class ContextPessoaJuridicaSearchFactory {

	private ContextPessoaJuridicaSearchFactory() {
	}

	public static Predicate criterios(String textFilter, Long codigoEmpresa, CriteriaBuilder cb,
			Root<PessoaJuridica> root) {
		BuscaPessoaJuridicaFactory factory = new BuscaPessoaJuridicaFactory();
		Predicate condition = cb.conjunction();

		condition = cb.and(condition, cb.equal(root.get("codigoEmpresa"), codigoEmpresa));
		condition = adicionaCriteriosNome(textFilter, cb, root, factory, condition);
		condition = adicionaCriteriosResponsavel(textFilter, cb, root, factory, condition);
		condition = adicionaCriteriosCnpj(textFilter, cb, root, factory, condition);

		return condition;
	}

	private static Predicate adicionaCriteriosNome(String textFilter, CriteriaBuilder cb, Root<PessoaJuridica> root,
			BuscaPessoaJuridicaFactory factory, Predicate condition) {
		CriterioBuscaDto dto = factory.getCriterio(factory.BUSCA_NOME).criterioBusca(textFilter);
		return cb.and(condition, cb.like(root.get("pessoa").get("descricao"),
				("%").concat(dto.getValorBusca().toString()).concat("%")));
	}

	private static Predicate adicionaCriteriosResponsavel(String textFilter, CriteriaBuilder cb,
			Root<PessoaJuridica> root, BuscaPessoaJuridicaFactory factory, Predicate condition) {
		CriterioBuscaDto dto = factory.getCriterio(factory.BUSCA_RESPONSAVEL).criterioBusca(textFilter);
		return cb.or(condition,
				cb.like(root.get("responsavel"), ("%").concat(dto.getValorBusca().toString()).concat("%")));
	}

	private static Predicate adicionaCriteriosCnpj(String textFilter, CriteriaBuilder cb, Root<PessoaJuridica> root,
			BuscaPessoaJuridicaFactory factory, Predicate condition) {
		CriterioBuscaDto dto = factory.getCriterio(factory.BUSCA_CNPJ).criterioBusca(textFilter);
		return cb.or(condition,
				cb.like(root.get("cnpj"), ("%").concat(dto.getValorBusca().toString()).concat("%")));
	}
}
