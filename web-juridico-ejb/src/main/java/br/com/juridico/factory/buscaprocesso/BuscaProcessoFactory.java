package br.com.juridico.factory.buscaprocesso;

import br.com.juridico.business.TipoBuscaBusiness;
import br.com.juridico.factory.Criterio;

/**
 * Created by Jefferson on 09/04/2016.
 */
public class BuscaProcessoFactory {

	/**
	 * 
	 * @param criterio
	 * @return
	 */
	public Criterio getCriterio(int criterio) {
		switch (criterio) {
		case TipoBuscaBusiness.BUSCA_PROCESSO:
			return new CriterioProcesso();
		case TipoBuscaBusiness.BUSCA_STATUS:
			return new CriterioStatus();
		case TipoBuscaBusiness.BUSCA_DATA_ABERTURA:
			return new CriterioDataAbertura();
		case TipoBuscaBusiness.BUSCA_FASE:
			return new CriterioFase();
		case TipoBuscaBusiness.BUSCA_CLASSE_PROCESSUAL:
			return new CriterioClasseProcessual();
		case TipoBuscaBusiness.BUSCA_NATUREZA_ACAO:
			return new CriterioNaturezaAcao();
		case TipoBuscaBusiness.BUSCA_MEIO_TRAMITACAO:
			return new CriterioMeioTramitacao();
		case TipoBuscaBusiness.BUSCA_VARA:
			return new CriterioVara();
		case TipoBuscaBusiness.BUSCA_FORUM:
			return new CriterioForum();
		case TipoBuscaBusiness.BUSCA_COMARCA:
			return new CriterioComarca();
		case TipoBuscaBusiness.BUSCA_GRAU_RISCO:
			return new CriterioGrauRisco();
		default:
			return null;
		}
	}
}
