package br.com.juridico.factory;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import br.com.juridico.dto.CriterioBuscaDto;
import br.com.juridico.entidades.Login;
import br.com.juridico.entidades.Pessoa;
import br.com.juridico.entidades.PessoaFisica;
import br.com.juridico.factory.buscapessoa.BuscaPessoaJuridicaFactory;
import br.com.juridico.filter.ConsultaLazyFilter;

/**
 * Created by Jefferson on 13/04/2016.
 */
public class ContextPessoaFisicaSearchFactory {

	private ContextPessoaFisicaSearchFactory() {
	}

	/**
	 *
	 * @param filter
	 * @param telaAdvogados
	 * @param telaColaborador
	 * @param cb
	 * @param root
     * @return
     */
	public static Predicate criterios(ConsultaLazyFilter filter, boolean telaAdvogados, boolean telaColaborador, CriteriaBuilder cb,
			Root<PessoaFisica> root) {
		BuscaPessoaJuridicaFactory factory = new BuscaPessoaJuridicaFactory();
		Predicate condition = cb.conjunction();

		condition = adicionaCriteriosNome(filter.getDescricao(), cb, root, factory, condition);
		condition = adicionaCriteriosLogin(filter.getDescricao(), cb, root, factory, condition);
		condition = cb.and(condition, cb.equal(root.get("codigoEmpresa"), filter.getCodigoEmpresa()));
		condition = cb.and(condition, cb.equal(root.get("advogado"), telaAdvogados));
		condition = cb.and(condition, cb.equal(root.get("colaborador"), telaColaborador));
		condition = cb.and(condition, cb.equal(root.get("pessoa").get("ativo"), true));
		condition = cb.and(condition, cb.isNull(root.get("dataExclusao")));
		
		return condition;
	}

	@SuppressWarnings("static-access")
	private static Predicate adicionaCriteriosNome(String textFilter, CriteriaBuilder cb, Root<PessoaFisica> root,
			BuscaPessoaJuridicaFactory factory, Predicate condition) {
		CriterioBuscaDto dto = factory.getCriterio(factory.BUSCA_NOME).criterioBusca(textFilter);
		return cb.and(condition, cb.like(root.get("pessoa").get("descricao"),
				("%").concat(dto.getValorBusca().toString()).concat("%")));
	}

	@SuppressWarnings("static-access")
	private static Predicate adicionaCriteriosLogin(String textFilter, CriteriaBuilder cb, Root<PessoaFisica> root,
			BuscaPessoaJuridicaFactory factory, Predicate condition) {
		CriterioBuscaDto dto = factory.getCriterio(factory.BUSCA_LOGIN).criterioBusca(textFilter);
		Join<PessoaFisica, Pessoa> pessoaFisicaJoinPessoa;
		pessoaFisicaJoinPessoa = root.join("pessoa");
		Join<Pessoa, Login> pessoaJoinLogin = pessoaFisicaJoinPessoa.join("login", JoinType.LEFT);

		return cb.or(condition, cb.like(pessoaJoinLogin.get("login"),
				("%").concat(dto.getValorBusca().toString()).concat("%")));
	}

}
