package br.com.juridico.proxy;

import br.com.juridico.entidades.Pessoa;
import br.com.juridico.util.DecryptUtil;
import org.apache.log4j.Logger;

import javax.faces.application.FacesMessage;

/**
 * Created by jeffersonl2009_00 on 24/10/2016.
 */
public class ProxyLogin implements ILogin {

    private static final Logger LOGGER = Logger.getLogger(ProxyLogin.class);
    private static final String USUARIO_NAO_CADASTRADO = "Login inválido: Usuário não está cadastrado no sistema.";
    private static final String SENHA_INVALIDA = "Senha Inválida.";
    private static final String PRIMEIRO_ACESSO = "Primeiro Acesso";
    private static final String USUARIO_SEM_PERFIL = "Usuário sem perfil cadastrado.";

    private Pessoa pessoa;
    private String senha;
    private String message;

    private boolean limpaLogin;
    private boolean limpaSenha;

    public ProxyLogin(Pessoa pessoa, String senha){
        this.pessoa = pessoa;
        this.senha = senha;
        setMessage("");
    }

    @Override
    public String executeLogin() {
        if (!validaPessaCadastrada() || !validaSenhaConfere() || !validaPerfilVinculado()) return "";

        else if (this.pessoa.getLogin().isPrimeiroAcesso()){
            setMessage(PRIMEIRO_ACESSO);
            return new PrimeiroAcesso().executeLogin();

        } else {
            setMessage("");
            return new PaginaInicial().executeLogin();
        }
    }

    public boolean validaPerfilVinculado(){
        if(this.pessoa.getPerfil() == null) {
            setMessage(USUARIO_SEM_PERFIL);
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    public boolean validaSenhaConfere(){
        if(this.pessoa.getLogin() != null && this.pessoa.getLogin().getSenha() != null){
            try {
                String password = DecryptUtil.execute(this.pessoa.getLogin().getSenha());
                if(password.equals(this.senha)){
                    return Boolean.TRUE;
                }
                this.limpaLogin = false;
                this.limpaSenha = true;
                setMessage(SENHA_INVALIDA);
                return Boolean.FALSE;

            } catch (Exception e) {
                LOGGER.error(e, e.getCause());
            }
        }
        return Boolean.FALSE;
    }

    public boolean validaPessaCadastrada() {
        if(this.pessoa == null) {
            this.limpaLogin = true;
            this.limpaSenha = true;
            setMessage(USUARIO_NAO_CADASTRADO);
            return false;
        }
        return true;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isLimpaLogin() {
        return limpaLogin;
    }

    public boolean isLimpaSenha() {
        return limpaSenha;
    }

}
