package br.com.juridico.proxy;

/**
 * Created by jeffersonl2009_00 on 24/10/2016.
 */
public interface ILogin {

    /**
     *
     * @return
     */
    String executeLogin();
}
