package br.com.juridico.proxy;

/**
 * Created by jeffersonl2009_00 on 24/10/2016.
 */
public class PaginaInicial implements ILogin {

    private static final String PAGINA_HOME = "/pages/index.xhtml?faces-redirect=true";

    @Override
    public String executeLogin() {
        return PAGINA_HOME;
    }
}
