package br.com.juridico.util;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;

/**
 * 
 * @author Jefferson
 *
 */
public enum OrderType {

	ASC() {
		@Override
		public <T> Order orderBy(CriteriaBuilder criteriaBuilder, Path<T> entityPath) {
			return criteriaBuilder.asc(entityPath);
		}
	},
	DESC() {
		@Override
		public <T> Order orderBy(CriteriaBuilder criteriaBuilder, Path<T> entityPath) {
			return criteriaBuilder.desc(entityPath);
		}
	};

	/**
	 * 
	 * @param criteriaBuilder
	 * @param entityPath
	 * @return
	 */
	public abstract <T> Order orderBy(CriteriaBuilder criteriaBuilder, Path<T> entityPath);
}
