package br.com.juridico.util;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 * 
 * @author Jefferson
 *
 */
public class FacesMessageHandler implements Serializable {

	private static final long serialVersionUID = 1L;

	public FacesMessageHandler() {
		super();
	}

	/**
	 * 
	 * @param clientId
	 * @param summary
	 * @param detail
	 */
	public void info(String clientId, String summary, String detail) {
		FacesContext fc = FacesContext.getCurrentInstance();
		fc.addMessage(clientId, new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail));
	}

	/**
	 * 
	 * @param clientId
	 * @param summary
	 * @param detail
	 */
	public void warn(String clientId, String summary, String detail) {
		FacesContext fc = FacesContext.getCurrentInstance();
		fc.addMessage(clientId, new FacesMessage(FacesMessage.SEVERITY_WARN, summary, detail));
	}

	/**
	 * 
	 * @param clientId
	 * @param summary
	 * @param detail
	 */
	public void error(String clientId, String summary, String detail) {
		FacesContext fc = FacesContext.getCurrentInstance();
		fc.addMessage(clientId, new FacesMessage(FacesMessage.SEVERITY_ERROR, summary, detail));
	}

	/**
	 * 
	 * @param clientId
	 * @param summary
	 * @param detail
	 */
	public void fatal(String clientId, String summary, String detail) {
		FacesContext fc = FacesContext.getCurrentInstance();
		fc.addMessage(clientId, new FacesMessage(FacesMessage.SEVERITY_FATAL, summary, detail));
	}
}
