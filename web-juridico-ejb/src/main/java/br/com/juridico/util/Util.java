package br.com.juridico.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import static java.lang.Math.*;
import static org.apache.commons.lang.StringUtils.leftPad;

/**
 * 
 * @author Jefferson
 *
 */
public class Util {

	private Util() {}

	/**
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	public static <T> T nvl(T a, T b) {
		return (a == null) ? b : a;
	}

	/**
	 * 
	 * @param a
	 * @return
	 */
	public static boolean isNull(Object a) {
		return a == null;
	}

	/**
	 * 
	 * @param a
	 * @return
	 */
	public static boolean isNotNull(Object a) {
		return a != null;
	}

	/**
	 * Verifica se todos os objetos são nulos
	 * 
	 * @param a
	 * @return
	 */
	public static boolean isNull(Object... a) {
		for (Object object : a) {
			if (object != null) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Verifica se todos os objetos são DIFERENTES de null
	 * 
	 * @param a
	 * @return
	 */
	public static boolean isNotNull(Object... a) {
		for (Object object : a) {
			if (object == null) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Verifica se todos os objetos são iguais a obj
	 * 
	 * @param obj
	 * @param comparacoes
	 * @return
	 */
	public static boolean eqAnd(Object obj, Object... comparacoes) {
		for (Object comparacao : comparacoes) {
			if (!obj.equals(comparacao)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Verifica se pelo menos um é igual a obj
	 * 
	 * @param obj
	 * @param comparacoes
	 * @return
	 */
	public static boolean eqOr(Object obj, Object... comparacoes) {
		for (Object comparacao : comparacoes) {
			if (obj.equals(comparacao)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 * @param s
	 * @param n
	 * @param filler
	 * @return
	 */
	public static <T, M> String rPad(M s, int n, T filler) {
		return toStr(s) + pad(toStr(s), toStr(filler), n);
	}

	/**
	 * 
	 * @param s
	 * @param n
	 * @param filler
	 * @return
	 */
	public static <T, M> String lPad(M s, int n, T filler) {
		return pad(toStr(s), toStr(filler), n) + toStr(s);
	}

	private static String pad(String s, String filler, int n) {
		StringBuilder pads = new StringBuilder();
		if (s.length() < n) {
			while ((s.length() + pads.length()) < n) {
				for (int i = 0; i < filler.length() && (s.length() + pads.length()) < n; i++) {
					pads.append(filler.charAt(i));
				}
			}
		}
		return pads.toString();
	}

	/**
	 * 
	 * @param s
	 * @param n
	 * @return
	 */
	public static <T> String rPad(T s, int n) {
		return String.format("%1$-" + n + "s", toStr(s));
	}

	/**
	 * 
	 * @param s
	 * @param n
	 * @return
	 */
	public static <T> String lPad(T s, int n) {
		return String.format("%1$#" + n + "s", toStr(s));
	}

	/**
	 * 
	 * @param s
	 * @return
	 */
	public static <T> String toStr(T s) {
		if (s instanceof Date)
			return new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(s);
		else
			return String.valueOf(s);
	}

	/**
	 * Gerador de string alphanumeric random
	 * @param length
	 * @return
     */
	public static String randomGenerateValue(int length) {
		StringBuffer sb = new StringBuffer();
		for (int i = length; i > 0; i -= 12) {
			int n = min(12, abs(i));
			sb.append(leftPad(Long.toString(round(random() * pow(36, n)), 36), n, '0'));
		}
		return sb.toString();
	}

	/**
	 * Gerador de string numeric random
	 * @param length
	 * @return
	 */
	public static String randomGenerateNumericValue(int length) {
		StringBuilder result = new StringBuilder();
		for (int i = 0; i< length; i++){
			Random r = new Random();
			Integer value = r.nextInt(10);
			result.append(value.toString());
		}
		return result.toString();
	}

}
