package br.com.juridico.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * 
 * @author Jefferson Ferreira
 * @since 21/10/2015
 *
 */
public class EncryptUtil {

	private static byte[] r = new byte[] { 0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xA, 0xB, 0xC, 0xD, 0xE,
			0xF };
	private static byte[] iv = new byte[] { 0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xA, 0xB, 0xC, 0xD, 0xE,
			0xF };
	private static byte[] header = new byte[] { 0x1, 0x2, 0x2 };

	private static SecretKeySpec secretKeySpec = new SecretKeySpec(r, "AES");
	private static IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);

	private EncryptUtil() {
		super();
	}

	/**
	 *
	 * @param plaintext
	 * @return
	 * @throws Exception
     */
	private static byte[] encrypt(String plaintext) throws Exception {

		byte[] text = plaintext.getBytes();

		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		stream.write(header);

		// Encrypt text
		Cipher cipher = Cipher.getInstance("AES/CFB/NoPadding");
		cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);
		stream.write(cipher.doFinal(text));

		// Hash text
		MessageDigest digest = MessageDigest.getInstance("SHA-256");
		digest.update(text);
		stream.write(digest.digest());

		byte[] bytes = stream.toByteArray();
		stream.close();

		return bytes;
	}

	public static byte[] execute(String texto) throws Exception {
		return encrypt(texto);
	}

}
