package br.com.juridico.util;

import br.com.juridico.rules.FeriadoRules;
import com.google.common.collect.Range;
import org.apache.log4j.Logger;

import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * 
 * @author Jefferson
 *
 */
public class DateUtils {

	private static final Logger LOGGER = Logger.getLogger(DateUtils.class);
	private static final FeriadoRules FERIADO_RULES = new FeriadoRules();
	private static final int ZERO = BigInteger.ZERO.intValue();
	private static final int UM = BigInteger.ONE.intValue();
	private static final String[] MESES = { "JANEIRO", "FEVEREIRO", "MARÇO", "ABRIL", "MAIO", "JUNHO", "JULHO",
			"AGOSTO", "SETEMBRO", "OUTUBRO", "NOVEMBRO", "DEZEMBRO" };

	private static final int MILLIS_IN_DAY = 86400000;
	private static final double MILLIS_IN_HOUR = 3600000;

	private static final DateFormat FORMATADOR_DATA_BRASIL = new SimpleDateFormat("dd/MM/yyyy");
	private static final DateFormat FORMATADOR_DATA_HORA_BRASIL = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	private static final DateFormat FORMATADOR_HORA_BRASIL = new SimpleDateFormat("HH:mm");

	private DateUtils() {
	}

	/**
	 * 
	 * @param date
	 * @return
	 */
	public static synchronized String formataDataBrasil(final Date date) {
		if (date == null)
			return "";
		return FORMATADOR_DATA_BRASIL.format(date);
	}

	/**
	 *
	 * @param dataLancamento
	 * @param quantidadeDias
	 * @return
	 */
	public static Date diaMenosUmValor(Date dataLancamento, int quantidadeDias) {
		for (int i = 0; i < quantidadeDias; i++) {
			dataLancamento = diaMenosUm(dataLancamento);
		}
		return dataLancamento;
	}

	/**
	 *
	 * @param dataLancamento
	 * @return
	 */
	public static Date diaMenosUm(Date dataLancamento) {
		boolean dataValida = false;
		while (!dataValida) {
			dataLancamento = DateUtils.subtract(dataLancamento, -1);
			if (DateUtils.isDiaUtil(dataLancamento) && !isFeriadoBancario(dataLancamento)) {
				dataValida = true;
			}
		}
		return dataLancamento;
	}

	protected static boolean isFeriadoBancario(Date data) {
		Date ultimoDia = DateUtils.upperDayOfMonthLimit(data);
		while (!DateUtils.isDiaUtil(ultimoDia)){
			ultimoDia = DateUtils.subtract(ultimoDia, -1);
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(data);
		return cal.get(Calendar.MONTH) == Calendar.DECEMBER && cal.get(Calendar.DAY_OF_MONTH) == ultimoDia.getDate();
	}

	/**
	 * 
	 * @param date
	 * @return
	 */
	public static Date lowerLimit(final Date date) {
		if (date == null) {
			return null;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);

		return calendar.getTime();
	}

	/**
	 * 
	 * @param date
	 * @return
	 */
	public static Date upperLimit(final Date date) {
		if (date == null) {
			return null;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 997);

		return calendar.getTime();
	}

	/**
	 * 
	 * @param date
	 * @return
	 */
	public static Date lowerDayOfMonthLimit(Date date) {
		if (date == null) {
			return null;
		}
		date = lowerLimit(date);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.DAY_OF_MONTH, 1);

		return calendar.getTime();
	}

	/**
	 * 
	 * @param date
	 * @return
	 */
	public static Date upperDayOfMonthLimit(Date date) {
		if (date == null) {
			return null;
		}
		date = lowerDayOfMonthLimit(date);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, 1);
		calendar.add(Calendar.DATE, -1);

		return upperLimit(calendar.getTime());
	}

	/**
	 * 
	 * @param date
	 * @param mask
	 * @return
	 */
	public static String formatData(final Date date, String mask) {
		if (date == null) {
			return "";
		}

		if (mask == null) {
			mask = "dd/MM/yyyy";
		}
		SimpleDateFormat sdf = new SimpleDateFormat(mask);

		return sdf.format(date);
	}

	/**
	 * 
	 * @param s
	 * @param mask
	 * @return
	 */
	public static Date parseDate(final String s, final String mask) {
		SimpleDateFormat sdf = new SimpleDateFormat(mask);
		try {
			return sdf.parse(s);
		} catch (ParseException e) {
			LOGGER.info(e.getCause(), e);
			return null;
		}
	}

	/**
	 * 
	 * @param qtDiasUteis
	 * @param date
	 * @return
	 */
	public static Date addUtilsDays(Integer qtDiasUteis, Date date) {

		Integer qtDiasPercorrer = 0;
		Calendar calendar = Calendar.getInstance();
		if (date != null) {
			calendar.setTime(date);
		}

		while (qtDiasUteis > qtDiasPercorrer) {
			calendar.add(Calendar.DATE, 1);

			if (FERIADO_RULES.isDiaUtil(calendar.getTime())) {
				qtDiasPercorrer++;
			}
		}
		return calendar.getTime();
	}

	/**
	 * Testa a data, verificando se e sabado ou domingo
	 * 
	 * @param date
	 * @return
	 */
	public static boolean isWeekEnd(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		return calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY
				|| calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY;
	}

	/**
	 * 
	 * @param data
	 * @param formato
	 * @return
	 */
	public static Boolean dataValida(final String data, final String formato) {
		SimpleDateFormat format = new SimpleDateFormat(formato);
		format.setLenient(false);
		try {
			format.parse(data);
		} catch (Exception e) {
			LOGGER.info(e.getCause(), e);
			return false;
		}

		return true;
	}

	/**
	 * 
	 * @param data
	 * @param dataValidacao
	 * @param formato
	 * @return
	 */
	public static Boolean dataFutura(final String data, final String dataValidacao, final String formato) {
		SimpleDateFormat format = new SimpleDateFormat(formato);
		format.setLenient(false);

		try {
			return (format.parse(data)).compareTo(format.parse(dataValidacao)) >= 0;
		} catch (ParseException e) {
			return false;
		}
	}

	/**
	 * 
	 * @param data
	 * @return
	 */
	public static boolean ehSabado(Date data) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);
		return calendar.get(Calendar.DAY_OF_WEEK) == 7;
	}

	/**
	 * 
	 * @param data
	 * @return
	 */
	public static boolean ehDomingo(Date data) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);
		return calendar.get(Calendar.DAY_OF_WEEK) == 1;
	}

	/**
	 * 
	 * @param data
	 * @return
	 */
	public static boolean isDiaUtil(Date data) {
		return FERIADO_RULES.isDiaUtil(data);
	}

	/**
	 * 
	 * @param data
	 * @return
	 */
	public static boolean isFeriadoNacional(Date data) {
		return FERIADO_RULES.isFeriadoNacional(data);
	}

	/**
	 * 
	 * @param dataInicio
	 * @param dataFim
	 * @return
	 */
	public static boolean dataFinalMenorQueInicial(Date dataInicio, Date dataFim) {
		if (dataFim != null)
			return dataInicio.after(dataFim);
		return Boolean.FALSE;
	}

	/**
	 * 
	 * @param date
	 * @return
	 */
	public static synchronized String formataDataHoraBrasil(final Date date) {
		return FORMATADOR_DATA_HORA_BRASIL.format(date);
	}

	/**
	 * 
	 * @param date
	 * @return
	 */
	public static synchronized String formataHoraBrasil(final Date date) {
		return FORMATADOR_HORA_BRASIL.format(date);
	}

	/**
	 * 
	 * @param data
	 * @return
	 */
	public static boolean isMesmoDiaMesAno(Date data) {
		int dia = getDia(data);
		int mes = getMes(data);
		int ano = getAno(data);
		int diaComparado = getDia(data);
		int mesComparado = getMes(data);
		int anoComparado = getAno(data);

		return dia == diaComparado && mes == mesComparado && ano == anoComparado;
	}

	/**
	 * 
	 * @param date
	 * @return
	 */
	public static Date calculaUltimoDiaDoMes(final Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
		return cal.getTime();
	}

	/**
	 * 
	 * @param date
	 * @return
	 */
	public static Date calculaPrimeiroDiaDoMes(final Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.DAY_OF_MONTH, UM);
		return cal.getTime();
	}

	/**
	 * 
	 * @param date
	 * @return
	 */
	public static int getDia(final Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal.get(Calendar.DAY_OF_MONTH);
	}

	/**
	 * 
	 * @param date
	 * @return
	 */
	public static int getMes(final Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal.get(Calendar.MONTH);
	}

	/**
	 * 
	 * @param date
	 * @return
	 */
	public static int getHora(final Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal.get(Calendar.HOUR_OF_DAY);
	}

	/**
	 * 
	 * @param date
	 * @return
	 */
	public static int getMinutos(final Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal.get(Calendar.MINUTE);
	}

	/**
	 * 
	 * @param date
	 * @return
	 */
	public static int getSegundos(final Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal.get(Calendar.SECOND);
	}

	/**
	 * 
	 * @param date
	 * @return
	 */
	public static String getNomeMes(final Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return MESES[cal.get(Calendar.MONTH)];
	}

	/**
	 * 
	 * @param date
	 * @return
	 */
	public static int getAno(final Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal.get(Calendar.YEAR);
	}

	/**
	 * 
	 * @param mes
	 * @param ano
	 * @return
	 */
	public static Date geraPrimeiroDiaDoMes(final int mes, int ano) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, UM);
		cal.set(Calendar.YEAR, ano);
		cal.set(Calendar.MONTH, mes);
		cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
		cal.set(Calendar.MILLISECOND, ZERO);
		cal.set(Calendar.SECOND, ZERO);
		cal.set(Calendar.MINUTE, ZERO);
		cal.set(Calendar.HOUR_OF_DAY, ZERO);
		return cal.getTime();
	}

	/**
	 * 
	 * @param mes
	 * @param ano
	 * @return
	 */
	public static Date geraDataUltimoDiaMes(int mes, int ano) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, UM);
		cal.set(Calendar.YEAR, ano);
		cal.set(Calendar.MONTH, mes);
		cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
		cal.set(Calendar.MILLISECOND, ZERO);
		cal.set(Calendar.SECOND, ZERO);
		cal.set(Calendar.MINUTE, ZERO);
		cal.set(Calendar.HOUR_OF_DAY, ZERO);
		return cal.getTime();
	}

	public static Date subtract(Date date, int amount) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(5, amount);
		return calendar.getTime();
	}

	/**
	 * 
	 * @param date
	 * @param dias
	 * @return
	 */
	public static Date somaDias(final Date date, final int dias) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, dias);
		return cal.getTime();
	}

	/**
	 * 
	 * @param date
	 * @param dias
	 * @return
	 */
	public static Date somaDiasUteis(final Date date, int dias) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int d = dias;
		while (d > 0) {
			cal.add(Calendar.DATE, UM);
			d = isFimDeSemana(cal.get(Calendar.DAY_OF_WEEK)) ? d : d - UM;
		}
		return cal.getTime();
	}

	private static boolean isFimDeSemana(final int diaDaSemana) {
		return diaDaSemana == Calendar.SATURDAY || diaDaSemana == Calendar.SUNDAY;
	}

	/**
	 * 
	 * @param data
	 * @return
	 */
	public static boolean isSabado(final Date data) {
		Calendar c = Calendar.getInstance();
		c.setTime(data);
		return c.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY;
	}

	/**
	 * 
	 * @param data
	 * @return
	 */
	public static boolean isDomingo(final Date data) {
		Calendar c = Calendar.getInstance();
		c.setTime(data);
		return c.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY;
	}

	/**
	 * 
	 * @param date
	 * @param meses
	 * @return
	 */
	public static Date somaMeses(final Date date, final int meses) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, meses);
		return cal.getTime();
	}

	public static Date somaHoras(final Date date, final int horas) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.HOUR_OF_DAY, horas);
		return cal.getTime();
	}

	/**
	 * 
	 * @param ano
	 * @return
	 */
	public static boolean isBissexto(final int ano) {
		return new GregorianCalendar().isLeapYear(ano);
	}

	/**
	 * 
	 * @param d1
	 * @param d2
	 * @return
	 */
	public static int diferencaEmDias(final Date d1, final Date d2) {
		Calendar c1 = Calendar.getInstance();
		c1.setTime(d1);
		c1.set(Calendar.MILLISECOND, ZERO);
		c1.set(Calendar.SECOND, ZERO);
		c1.set(Calendar.MINUTE, ZERO);
		c1.set(Calendar.HOUR_OF_DAY, ZERO);

		Calendar c2 = Calendar.getInstance();
		c2.setTime(d2);
		c2.set(Calendar.MILLISECOND, ZERO);
		c2.set(Calendar.SECOND, ZERO);
		c2.set(Calendar.MINUTE, ZERO);
		c2.set(Calendar.HOUR_OF_DAY, ZERO);
		return (int) ((c1.getTimeInMillis() - c2.getTimeInMillis()) / MILLIS_IN_DAY);
	}

	/**
	 * 
	 * @param d1
	 * @param d2
	 * @return
	 */
	public static double diferencaEmHoras(final Date d1, final Date d2) {
		Calendar c1 = Calendar.getInstance();
		c1.setTime(d1);

		Calendar c2 = Calendar.getInstance();
		c2.setTime(d2);
		return (c1.getTimeInMillis() - c2.getTimeInMillis()) / MILLIS_IN_HOUR;
	}

	/**
	 * 
	 * @param date
	 * @return
	 */
	public static Date limpaHora(final Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.set(Calendar.MILLISECOND, ZERO);
		c.set(Calendar.SECOND, ZERO);
		c.set(Calendar.MINUTE, ZERO);
		c.set(Calendar.HOUR_OF_DAY, ZERO);

		return c.getTime();
	}

	/**
	 * 
	 * @return
	 */
	public static Date getDataAtualHoraLimpa() {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.MILLISECOND, ZERO);
		c.set(Calendar.SECOND, ZERO);
		c.set(Calendar.MINUTE, ZERO);
		c.set(Calendar.HOUR_OF_DAY, ZERO);

		return c.getTime();
	}

	/**
	 * 
	 * @param hora
	 * @param minuto
	 * @param segundo
	 * @return
	 */
	public static Date getDataAtualComHoras(int hora, int minuto, int segundo) {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.SECOND, segundo);
		c.set(Calendar.MINUTE, minuto);
		c.set(Calendar.HOUR_OF_DAY, hora);

		return c.getTime();
	}

	/**
	 * 
	 * @param date
	 * @return
	 */
	public static boolean isPrimeiroDiaDoMes(final Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return c.get(Calendar.DAY_OF_MONTH) == 1;
	}

	/**
	 * 
	 * @param ano
	 * @return
	 */
	public static Range<Date> getPrimeiroSemestre(final int ano) {
		return Range.closed(geraPrimeiroDiaDoMes(1, ano), geraDataUltimoDiaMes(6, ano));
	}

	/**
	 * 
	 * @param ano
	 * @return
	 */
	public static Range<Date> getSegundoSemestre(final int ano) {
		return Range.closed(geraPrimeiroDiaDoMes(7, ano), geraDataUltimoDiaMes(12, ano));
	}

}
