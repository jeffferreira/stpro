/**
 * 
 */
package br.com.juridico.util;

import java.io.ByteArrayInputStream;
import java.io.OutputStream;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

/**
 * @author jefferson.ferreira
 *
 */
public class DownloadUtil {

    private static final int BUFFER_FILE_DOWNLOAD = 204800;

    private static final Logger LOGGER = Logger.getLogger(DownloadUtil.class);

    private DownloadUtil() {

    }

    private static FacesContext getFacesContext() {
        return FacesContext.getCurrentInstance();
    }

    public static void downloadFile(byte[] arquivo, String nomeComExtensao) {
        try {

            HttpServletResponse response = (HttpServletResponse) getFacesContext().getExternalContext().getResponse();
            String extensao = nomeComExtensao.substring(nomeComExtensao.lastIndexOf(".")+1, nomeComExtensao.length());

            response.setHeader("Content-disposition", "attachment;filename=\"" + nomeComExtensao.concat("\""));
            response.setContentType("application/" + extensao);

            OutputStream outputStream = response.getOutputStream();
            ByteArrayInputStream inputStream = new ByteArrayInputStream(arquivo);

            int len = BUFFER_FILE_DOWNLOAD;
            byte[] buffer = new byte[len];
            int offSet = 0;

            while ((len = inputStream.read(buffer, offSet, len)) >= 0) {
                outputStream.write(buffer, offSet, len);
            }
            inputStream.close();
            outputStream.flush();
            outputStream.close();

        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        } finally {
            getFacesContext().responseComplete();
        }
    }

}
