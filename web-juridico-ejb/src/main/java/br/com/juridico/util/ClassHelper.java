/**
 * $Id: ClassHelper.java 18 2009-06-02 23:32:16Z $
 */
package br.com.juridico.util;

import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Prov� m�todos utilit�rios para classe. Principalmente uso interno.
 * 
 * 
 */
public final class ClassHelper {

	private ClassHelper(){}
	
	/**
	 * Este m�todo implementa um algoritmo para descobrir qual a �ltima classe
	 * implementa todas as classes informadas.
	 * 
	 * @param rootClass a classe origem 
	 * @param parents o array contendo os parents obrigatorios para a instancia retornada.
	 * @param allowAbstract verdadeiro (true) para permitir ultima classe sendo abstrata.
	 * @return a �ltima classe que implementa as interfaces e extende a classe ou nulo (null) 
	 * caso n�o exista classe que tenha todos os parents. 
	 */
	public static Class<?> getTopClassForParents( Class<?> rootClass, 
			Class<?>[] parents, boolean allowAbstract ){
		
		final List<Class<?>> parentList = Arrays.asList( parents );
		
		return getTopClassForParents(rootClass, parentList, allowAbstract );
	}

	/**
	 * Este m�todo implementa um algoritmo para descobrir qual a �ltima classe
	 * implementa todas as classes informadas.
	 * 
	 * @param rootClass a classe origem 
	 * @param parentList a lista contendo os parents obrigatorios para a instancia retornada.
	 * @param allowAbstract verdadeiro (true) para permitir ultima classe sendo abstrata.
	 * @return a �ltima classe que implementa as interfaces e extende a classe ou nulo (null) 
	 * caso n�o exista classe que tenha todos os parents.
	 */
	@SuppressWarnings("rawtypes")
	public static Class<?> getTopClassForParents( Class rootClass,
			List<Class<?>> parentList, boolean allowAbstract) {
		
		final Class<?> result;
		final List<Class<?>> declaredParents = Arrays.asList( getClasses(rootClass) );
		final Class<?> superClass = rootClass.getSuperclass();
		
		if ( matchClassForParents( rootClass, parentList, declaredParents, allowAbstract)) {
			
			if (superClass != null && 
					matchClassForParents( superClass, parentList, 
							Arrays.asList( getClasses(superClass) ), allowAbstract)) {
				
				result = getTopClassForParents( superClass, parentList, allowAbstract );
				
			} else {
				result = rootClass;
			}
		} else {
			result = null;
		}
		
		return result;
	}
	
	private static boolean matchClassForParents(Class<?> clazz,
			List<Class<?>> parentList, final List<Class<?>> declaredParents, 
			boolean allowAbstract) {
		
		return ( ( allowAbstract || !Modifier.isAbstract( clazz.getModifiers() ) ) && 
				declaredParents.containsAll(parentList));
	}
	
	private static Class<?>[] getClasses(final Class<?> root) {
		
		List<Class<?>> result = new ArrayList<Class<?>>();
		Class<?> currentClass = root;
		Class<?>[] classes;
		
		while(currentClass != null) {
			
			classes = currentClass.getInterfaces();
			
			for(int i = 0; i < classes.length;i++) {
				if (Modifier.isPublic(classes[i].getModifiers())) {
					result.add(classes[i]);
				}
			}
			
			currentClass = currentClass.getSuperclass();
			
			if (currentClass != null && Modifier.isPublic(currentClass.getModifiers())) {
				result.add(currentClass);
			}
		}
		
		return result.toArray(new Class<?>[result.size()]);
	}
	
	/**
	 * Retorna a classe do parametro gen�rico, buscando na hierarquia � partir 
	 * da classe informada em clazz, at� chegar na classe Object.
	 * 
	 * @param clazz classe de onde 
	 * @param parameterIndex �ndice do parametro gen�rico da classe informada.
	 * @return uma instancia de Class do tipo do parametro encontrado no gen�rico.
	 * 
	 * @throws TypeNotPresentException - se qualquer dos tipos de argumentos referirem-se para 
	 * um tipo de declara��o n�o existente.
	 * @throws java.lang.reflect.MalformedParameterizedTypeException - se qualquer um dos tipos de parametros 
	 * atuais referirem-se para um tipo parametrizado que n�o possa ser instanciado por qualquer raz�o.
	 * @throws ArrayIndexOutOfBoundsException - se o �ndice do parametro informado n�o existir.
	 * @see ClassHelper#findGenericClass(Class, Class, int);
	 */
	@Deprecated
	public static Class<?> findGenericClass( Class<?> clazz,
			int parameterIndex ){
		
		return findGenericClass( clazz, Object.class, parameterIndex );
	}
	
	/**
	 * Retorna a classe do parametro gen�rico, buscando na hierarquia � partir 
	 * da classe informada em clazz, at� chegar na classe motherClass.
	 * 
	 * <pre>
	 * public abstract class AbstractDomainBean&lt;P extends Serializable & Comparable&lt;P&gt;&gt;
	 *	implements DomainBean&lt;P&gt;{...}
	 * 
	 * public class Cidade extends AbstractDomainBean&lt;Long&gt;{...}
	 * 
	 * Class<?> genericClass = findGenericClass( Cidade.class, AbstractDomainBean, 0 );
	 * 
	 * Long.class.isInstance( genericClass ); // true
	 * <pre>
	 * 
	 * @param clazz classe de onde 
	 * @param topParentClass classe limite para a busca do parametro gen�rico.
	 * @param parameterIndex �ndice do parametro gen�rico da classe informada.
	 * @return uma instancia de Class do tipo do parametro encontrado no gen�rico.
	 * 
	 * @throws TypeNotPresentException - se qualquer dos tipos de argumentos referirem-se para 
	 * um tipo de declara��o n�o existente.
	 * @throws java.lang.reflect.MalformedParameterizedTypeException - se qualquer um dos tipos de parametros 
	 * atuais referirem-se para um tipo parametrizado que n�o possa ser instanciado por qualquer raz�o.
	 * @throws ArrayIndexOutOfBoundsException - se o �ndice do parametro informado n�o existir.
	 */
	public static Class<?> findGenericClass(Class<?> clazz,
			Class<?> topParentClass, int parameterIndex) {
		
		Map<Type, Type> resolvedTypes = new HashMap<Type, Type>();
		Type type = clazz;
		ParameterizedType parameterizedType;
		Class<?> rawType;
		Type[] actualTypeArguments;
		TypeVariable<?>[] typeParameters;
		List<Class<?>> typeArgumentsAsClasses;
		
		while (!getClass(type).equals(topParentClass)) {
			
			if (type instanceof Class) {
				// nao faz nada com isso
				type = ((Class<?>) type).getGenericSuperclass();
			} else {
				parameterizedType = (ParameterizedType) type;
				rawType = (Class<?>) parameterizedType.getRawType();

				actualTypeArguments = 
						parameterizedType.getActualTypeArguments();
				typeParameters = rawType.getTypeParameters();
				
				for (int i = 0; i < actualTypeArguments.length; i++) {
					resolvedTypes.put(
							typeParameters[i], actualTypeArguments[i]);
				}

				if (!rawType.equals(topParentClass)) {
					type = rawType.getGenericSuperclass();
				}
			}
		}

		if (type instanceof Class) {
			actualTypeArguments = ((Class<?>) type).getTypeParameters();
		} else {
			actualTypeArguments = ((ParameterizedType) type)
					.getActualTypeArguments();
		}
		typeArgumentsAsClasses = new ArrayList<Class<?>>();
		
		for (Type baseType : actualTypeArguments) {
			while (resolvedTypes.containsKey(baseType)) {
				baseType = resolvedTypes.get(baseType);
			}
			typeArgumentsAsClasses.add(getClass(baseType));
		}
		return typeArgumentsAsClasses.get(parameterIndex);
	}
	
	/**
	 * Obtem a classe representada por type ou nulo (null) caso o tipo seja um tipo variavel.
	 * @param type o tipo que representa a classe esperada
	 * @return a instancia de Class para a classe representada por type.
	 */
	public static Class<?> getClass(Type type) {
		
		Type componentType;
		Class<?> componentClass;
		
		if (type instanceof Class) {
			
			return (Class<?>) type;
			
		} else if (type instanceof ParameterizedType) {
			
			return getClass(((ParameterizedType) type).getRawType());
			
		} else if (type instanceof GenericArrayType) {
			
			componentType = ((GenericArrayType) type)
					.getGenericComponentType();
			componentClass = getClass(componentType);
			
			if (componentClass != null) {
				return Array.newInstance(componentClass, 0).getClass();
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	
}
