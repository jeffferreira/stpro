package br.com.juridico.util;

import java.io.Serializable;

/**
 * 
 * @author Jefferson
 *
 */
public class OrderClause implements Serializable {

	private static final long serialVersionUID = 1L;

	private String propertyName;
	private OrderType orderType;

	/**
	 * 
	 * @param propertyName
	 * @param orderType
	 */
	public OrderClause(String propertyName, OrderType orderType) {
		this.propertyName = propertyName;
		this.orderType = orderType;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public OrderType getOrderType() {
		return orderType;
	}

	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}
}
