/**
 * 
 */
package br.com.juridico.util;

import java.nio.ByteBuffer;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import br.com.juridico.exception.InvalidHashException;
import br.com.juridico.exception.InvalidHeaderException;

/**
 * @author Jefferson Ferreira
 * 
 */
public class DecryptUtil {

	private static byte[] r = new byte[] { 0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xA, 0xB, 0xC, 0xD, 0xE,
			0xF };
	private static byte[] iv = new byte[] { 0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xA, 0xB, 0xC, 0xD, 0xE,
			0xF };
	private static byte[] header = new byte[] { 0x1, 0x2, 0x2 };

	private static int headerlen = 3;
	private static int shalen = 32;

	private SecretKeySpec secretKeySpec = new SecretKeySpec(r, "AES");
	private IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);

	/**
	 * 
	 */
	public DecryptUtil() {
		super();
	}

	/**
	 *
	 * @param bytes
	 * @return
	 * @throws Exception
     */
	private String decrypt(byte[] bytes) throws Exception {

		ByteBuffer buf = ByteBuffer.wrap(bytes);

		byte[] header = new byte[headerlen];
		buf.get(header);
		if (!Arrays.equals(header, DecryptUtil.header))
			throw new InvalidHeaderException("Header is not valid. Decryption aborted.");

		int aeslen = bytes.length - shalen - headerlen;
		byte[] aes = new byte[aeslen];
		buf.get(aes);

		// Decrypt text
		Cipher cipher = Cipher.getInstance("AES/CFB/NoPadding");
		cipher.init(Cipher.DECRYPT_MODE, this.secretKeySpec, this.ivParameterSpec);
		byte[] text = cipher.doFinal(aes);

		// Compute hash
		MessageDigest digest = MessageDigest.getInstance("SHA-256");
		digest.update(text);
		byte[] hash = digest.digest();

		byte[] hash2 = new byte[shalen];
		buf.get(hash2);

		if (!Arrays.equals(hash, hash2))
			throw new InvalidHashException("Verification failed. Decryption aborted.");

		return new String(text);
	}

	public static String execute(byte[] valorCriptografado) throws Exception {
		DecryptUtil util = new DecryptUtil();
		return util.decrypt(valorCriptografado);
	}

}
