package br.com.juridico.util;

import com.lowagie.text.DocumentException;
import org.w3c.dom.Document;
import org.w3c.tidy.Tidy;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.*;

public class HtmlToPdfUtil {

    private static final String UTF_8 = "UTF-8";

    private HtmlToPdfUtil(){}

    public static void convert(String input, OutputStream out) throws DocumentException{
        convert(new ByteArrayInputStream(input.getBytes()), out);
    }
    public static void convert(InputStream input, OutputStream out) throws DocumentException{
        Tidy tidy = new Tidy();
        tidy.setOutputEncoding(UTF_8);
        tidy.setXHTML(true);
        Document doc = tidy.parseDOM(input, null);
        ITextRenderer renderer = new ITextRenderer();
        renderer.setDocument(doc, null);
        renderer.layout();
        renderer.createPDF(out);
    }

    //teste

    public static String getHTML() {
        try {
            String text = "";
            FileReader in = new FileReader("C:/Users/jefferson.ferreira/testeHtmlToPdf/Teste.html");
            BufferedReader buffer = new BufferedReader(in);
            while(buffer.ready()) {
                text += buffer.readLine();
            }
            return text;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) {
        try {
            OutputStream os = new FileOutputStream("C:/Users/jefferson.ferreira/testeHtmlToPdf/relatorio.pdf");
            convert(getHTML(), os);
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}