package br.com.juridico.util;

/**
 * Created by jefferson.ferreira on 07/03/2017.
 */
public class PosicaoArvoreUtil implements Comparable {

    private String posicao;

    public PosicaoArvoreUtil(String posicao){
        this.posicao = posicao;
    }

    public String getPosicao() {
        return posicao;
    }

    public void setPosicao(String posicao) {
        this.posicao = posicao;
    }

    @Override
    public int compareTo(Object o) {
        PosicaoArvoreUtil o1 = (PosicaoArvoreUtil) o;
        return (this.posicao).compareTo(o1.getPosicao());
    }
}
