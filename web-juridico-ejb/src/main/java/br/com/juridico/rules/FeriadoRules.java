package br.com.juridico.rules;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * 
 * @author Jefferson
 *
 */
public class FeriadoRules {

	private static final Set<FeriadoEstatico> FERIADO_LIST;

	static {
		FERIADO_LIST = new HashSet<>();
		FERIADO_LIST.add(new FeriadoEstatico(0, 1));
		FERIADO_LIST.add(new FeriadoEstatico(3, 21));
		FERIADO_LIST.add(new FeriadoEstatico(4, 1));
		FERIADO_LIST.add(new FeriadoEstatico(8, 7));
		FERIADO_LIST.add(new FeriadoEstatico(9, 12));
		FERIADO_LIST.add(new FeriadoEstatico(10, 2));
		FERIADO_LIST.add(new FeriadoEstatico(10, 15));
		FERIADO_LIST.add(new FeriadoEstatico(11, 25));
	}

	public FeriadoRules() {
		super();
	}

	/**
	 * 
	 * @param data
	 * @return
	 */
	public boolean isDiaUtil(final Date data) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);
		int dayOfWeek = calendar.get(7);
		switch (dayOfWeek) {
		case 1: // '\001'
		case 7: // '\007'
			return false;
		default:
			return !isFeriadoNacional(data);
		}
	}

	/**
	 * 
	 * @param data
	 * @return
	 */
	public boolean isFeriadoNacional(final Date data) {
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(data);
		for (Iterator<FeriadoEstatico> iterator = FERIADO_LIST.iterator(); iterator.hasNext();) {
			FeriadoEstatico feriadoEstatico = iterator.next();
			if (feriadoEstatico.getMes() == calendar.get(2) && feriadoEstatico.getDia() == calendar.get(5)) {
				return true;
			}
		}

		Calendar pascoa = getPascoa(calendar.get(1));
		Calendar sextaFeiraSanta = (Calendar) pascoa.clone();
		sextaFeiraSanta.add(6, -2);
		if (sextaFeiraSanta.get(2) == calendar.get(2) && sextaFeiraSanta.get(5) == calendar.get(5)) {
			return true;
		}
		Calendar corpusChristi = (Calendar) pascoa.clone();
		corpusChristi.add(6, 60);
		if (corpusChristi.get(2) == calendar.get(2) && corpusChristi.get(5) == calendar.get(5)) {
			return true;
		}
		Calendar carnaval = (Calendar) pascoa.clone();
		carnaval.add(6, -47);
		return carnaval.get(2) == calendar.get(2) && carnaval.get(5) == calendar.get(5);
	}

	protected Calendar getPascoa(int year) {
		int a = year % 19;
		int b = year / 100;
		int c = year % 100;
		int d = b / 4;
		int e = b % 4;
		int f = (b + 8) / 25;
		int g = ((b - f) + 1) / 3;
		int h = (((19 * a + b) - d - g) + 15) % 30;
		int i = c / 4;
		int k = c % 4;
		int l = ((32 + 2 * e + 2 * i) - h - k) % 7;
		int m = (a + 11 * h + 22 * l) / 451;
		int p = (((h + l) - 7 * m) + 114) % 31;
		int month = (((h + l) - 7 * m) + 114) / 31;
		int day = p + 1;
		return new GregorianCalendar(year, month - 1, day);

	}

	@SuppressWarnings("unused")
	private static class FeriadoEstatico {

		private int mes;
		private int dia;

		public FeriadoEstatico(int mes, int dia) {
			this.mes = mes;
			this.dia = dia;
		}

		public int getDia() {
			return dia;
		}

		public void setDia(int dia) {
			this.dia = dia;
		}

		public int getMes() {
			return mes;
		}

		public void setMes(int mes) {
			this.mes = mes;
		}

		@Override
		public boolean equals(Object obj) {
			if (obj instanceof FeriadoEstatico) {
				FeriadoEstatico other = (FeriadoEstatico) obj;
				return mes == other.mes && dia == other.dia;
			} else {
				return false;
			}
		}

		@Override
		public int hashCode() {
			return Integer.toString(mes).hashCode() + Integer.toString(dia).hashCode();
		}

	}
}