package br.com.juridico.audit;

import br.com.juridico.entidades.User;
import org.hibernate.envers.RevisionListener;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;

/**
 * Created by jeffersonl2009_00 on 11/07/2016.
 */
public class UsernameRevisionNameListener implements RevisionListener {

    @Override
    public void newRevision(Object revisionEntity) {
        Revinfo revision = (Revinfo) revisionEntity;
        revision.setUsuarioRevisao(getUser() != null ? getUser().getLogin() : "MANUTENCAO");
        revision.setDataRevisao(new Date());
    }

    private User getUser() {
        return (User) getSessionAttribute("userLogged");
    }

    /**
     *
     * @param key
     * @return
     */
    private Object getSessionAttribute(String key) {
        return ((HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false))
                .getAttribute(key);
    }

}
