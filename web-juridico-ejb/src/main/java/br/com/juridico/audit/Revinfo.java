package br.com.juridico.audit;

import org.hibernate.envers.RevisionEntity;
import org.hibernate.envers.RevisionNumber;
import org.hibernate.envers.RevisionTimestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by jeffersonl2009_00 on 11/07/2016.
 */
@Entity
@RevisionEntity(UsernameRevisionNameListener.class)
public class Revinfo implements Serializable {

    private static final long serialVersionUID = -1255842407304508513L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @RevisionNumber
    private int rev;

    @RevisionTimestamp
    private long revtstmp;

    private Date dataRevisao;

    private String usuarioRevisao;


    public int getRev() {
        return rev;
    }

    public void setRev(int rev) {
        this.rev = rev;
    }

    public Date getDataRevisao() {
        return dataRevisao;
    }

    public void setDataRevisao(Date dataRevisao) {
        this.dataRevisao = dataRevisao;
    }

    public String getUsuarioRevisao() {
        return usuarioRevisao;
    }

    public void setUsuarioRevisao(String usuarioRevisao) {
        this.usuarioRevisao = usuarioRevisao;
    }

    public long getRevtstmp() {
        return revtstmp;
    }

    public void setRevtstmp(long revtstmp) {
        this.revtstmp = revtstmp;
    }
}
