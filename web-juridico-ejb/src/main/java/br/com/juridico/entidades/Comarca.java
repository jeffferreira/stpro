/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.cadastros.ComarcaValidacaoWrapper;
import com.google.common.base.Strings;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Jefferson
 */
@Entity
@Audited
@Table(name = "TB_COMARCA")
@NamedQueries({
		@NamedQuery(name = "Comarca.findByid", query = "SELECT t FROM Comarca t WHERE t.codigoEmpresa=:codigoEmpresa and t.id = :id"),
		@NamedQuery(name = "Comarca.findByDescricao", query = "SELECT t FROM Comarca t WHERE t.codigoEmpresa=:codigoEmpresa and t.descricao = :descricao") })
public class Comarca implements IEntity, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_COMARCA")
	private Long id;

	@Column(name = "DS_COMARCA", length = 200)
	private String descricao;

	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne
	@JoinColumn(name = "FK_UF", referencedColumnName = "ID_UF")
	private Uf uf;

	@Column(name = "DT_EXCLUSAO_LOGICA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataExclusao;

	@Column(name = "NR_EMPRESA", nullable = false)
	private Long codigoEmpresa;

	@NotAudited
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "comarca")
	private List<Processo> processos;

	public Comarca() {
		super();
	}

	public Comarca(Long id) {
		this.id = id;
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Uf getUf() {
		return uf;
	}

	public void setUf(Uf uf) {
		this.uf = uf;
	}

	public List<Processo> getProcessos() {
		return processos;
	}

	public void setProcessos(List<Processo> processos) {
		this.processos = processos;
	}

	public Date getDataExclusao() {
		return dataExclusao;
	}

	public void setDataExclusao(Date dataExclusao) {
		this.dataExclusao = dataExclusao;
	}

	public Long getCodigoEmpresa() {
		return codigoEmpresa;
	}

	public void setCodigoEmpresa(Long codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof Comarca)) {
			return false;
		}
		Comarca other = (Comarca) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		String templateFormat = "%1$s - %2$s";
		String descricaoComarca = Strings.isNullOrEmpty(descricao) ? "" : this.descricao;
		String ufComarca = this.uf == null ? "" : this.uf.getSigla();
		return String.format(templateFormat, descricaoComarca, ufComarca);
	}

	public void validar(boolean existeComarcaCadastrado) throws ValidationException {
		runValidations(buildValidatorFactory().validate(new ComarcaValidacaoWrapper(this, existeComarcaCadastrado)));
	}

}
