package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "TB_LOGIN")
public class Login implements IEntity, Serializable {

    /**
	 * 
	 */
    private static final long serialVersionUID = -6732903233144666231L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_LOGIN", nullable = false)
    private Long id;

    @Size(max = 20, message = "Login: tamanho máximo permitido 20")
    @Column(name = "DS_LOGIN", length = 20)
    private String login;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "BB_SENHA", columnDefinition = "BLOB")
    private byte[] senha;

    @Column(name = "DT_ULTIMO_ACESSO", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataUltimoAcesso;

    @Type(type="true_false")
    @Column(name = "FL_PRIMEIRO_ACESSO", length = 1)
    private Boolean primeiroAcesso;

    @NotNull
    @Column(name = "NR_EMPRESA", nullable = false)
    private Long codigoEmpresa;

    public Login() {
        super();
    }

    public Login(String login, byte[] senha, Long codigoEmpresa) {
        this.login = login;
        this.senha = senha;
        this.codigoEmpresa = codigoEmpresa;
        this.primeiroAcesso = true;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public byte[] getSenha() {
        return senha;
    }

    public void setSenha(byte[] senha) {
        this.senha = senha;
    }

    public Date getDataUltimoAcesso() {
        return dataUltimoAcesso;
    }

    public void setDataUltimoAcesso(Date dataUltimoAcesso) {
        this.dataUltimoAcesso = dataUltimoAcesso;
    }

    public Boolean getPrimeiroAcesso() {
        return primeiroAcesso;
    }

    public void setPrimeiroAcesso(Boolean primeiroAcesso) {
        this.primeiroAcesso = primeiroAcesso;
    }

    public Long getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(Long codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }

    public boolean isPrimeiroAcesso(){
        return primeiroAcesso != null && primeiroAcesso;
    }
}
