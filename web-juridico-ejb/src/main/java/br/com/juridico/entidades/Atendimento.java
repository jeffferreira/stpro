package br.com.juridico.entidades;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.google.common.collect.Lists;

import br.com.juridico.dao.IEntity;

/**
 *
 * @author Jefferson
 */
@Entity
@Table(name = "TB_ATENDIMENTO")
public class Atendimento implements IEntity, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_ATENDIMENTO", nullable = false)
	private Long id;

	@JoinColumn(name = "FK_TIPO_ATENDIMENTO", referencedColumnName = "ID_TIPO_ATENDIMENTO", nullable = false)
	@ManyToOne(optional = false)
	private TipoAtendimento tipoAtendimento;

	@Column(name = "NR_EMPRESA", nullable = false)
	private Long codigoEmpresa;

	private String status;

	private String dataAbertura;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "atendimento")
	private List<AndamentoAtendimento> andamentos = Lists.newArrayList();

	public Atendimento() {
		super();
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<AndamentoAtendimento> getAndamentos() {
		return andamentos;
	}

	public void setAndamentos(List<AndamentoAtendimento> andamentos) {
		this.andamentos = andamentos;
	}

	public TipoAtendimento getTipoAtendimento() {
		return tipoAtendimento;
	}

	public void setTipoAtendimento(TipoAtendimento tipoAtendimento) {
		this.tipoAtendimento = tipoAtendimento;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDataAbertura() {
		return dataAbertura;
	}

	public void setDataAbertura(String dataAbertura) {
		this.dataAbertura = dataAbertura;
	}

	public Long getCodigoEmpresa() {
		return codigoEmpresa;
	}

	public void setCodigoEmpresa(Long codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof Atendimento)) {
			return false;
		}
		Atendimento other = (Atendimento) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Atendimento[ id=" + id + " ]";
	}


}
