package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author Jefferson
 */
@Entity
@Table(name = "projudipr_processo_controladoria")
public class ProjudiprProcessoControladoria implements IEntity, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_PROCESSO_CONTROLADORIA", nullable = false)
	private Long id;

	@Type(type="true_false")
	@Column(name = "FL_NOVA_MOVIMENTACAO")
	private Boolean novaMovimentacao;

	@Type(type="true_false")
	@Column(name = "FL_VISTO_CONTROLADORIA")
	private Boolean vistoPelaControladoria;

	@ManyToOne(optional = false)
	@JoinColumn(name = "FK_PROJUDI_MOVIMENTACAO", referencedColumnName = "ID_MOVIMENTACAO", nullable = false)
	private ProjudiprMovimentacao movimentacao;

	@Column(name = "NR_EMPRESA", nullable = false)
	private Long codigoEmpresa;

	public ProjudiprProcessoControladoria(){
		super();
	}

	public ProjudiprProcessoControladoria(Boolean novaMovimentacao, ProjudiprMovimentacao movimentacao) {
		this.novaMovimentacao = novaMovimentacao;
		this.movimentacao = movimentacao;
		this.vistoPelaControladoria = Boolean.FALSE;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getNovaMovimentacao() {
		return novaMovimentacao;
	}

	public void setNovaMovimentacao(Boolean novaMovimentacao) {
		this.novaMovimentacao = novaMovimentacao;
	}

	public Boolean getVistoPelaControladoria() {
		return vistoPelaControladoria;
	}

	public void setVistoPelaControladoria(Boolean vistoPelaControladoria) {
		this.vistoPelaControladoria = vistoPelaControladoria;
	}

	public ProjudiprMovimentacao getMovimentacao() {
		return movimentacao;
	}

	public void setMovimentacao(ProjudiprMovimentacao movimentacao) {
		this.movimentacao = movimentacao;
	}

	public Long getCodigoEmpresa() {
		return codigoEmpresa;
	}

	public void setCodigoEmpresa(Long codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}
}
