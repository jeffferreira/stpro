package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.cadastros.MeioTramitacaoValidacaoWrapper;
import com.google.common.collect.Lists;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Jefferson
 */
@Entity
@Audited
@Table(name = "TB_MEIO_TRAMITACAO")
@NamedQueries({
		@NamedQuery(name = "MeioTramitacao.findByDescricao", query = "SELECT t FROM MeioTramitacao t WHERE t.codigoEmpresa=:codigoEmpresa and t.descricao = :descricao") })
public class MeioTramitacao implements IEntity, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_MEIO_TRAMITACAO", nullable = false)
	private Long id;

	@Column(name = "DS_MEIO_TRAMITACAO", length = 200)
	private String descricao;

	@Column(name = "DS_MASCARA", length = 45)
	private String mascara;

	@Column(name = "NR_EMPRESA", nullable = false)
	private Long codigoEmpresa;

	@Column(name = "DT_EXCLUSAO_LOGICA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataExclusao;

	@NotAudited
	@ManyToOne
	@JoinColumn(name = "FK_ROBO_TRAMITACAO", referencedColumnName = "ID_ROBO_TRAMITACAO")
	private RoboTramitacao roboTramitacao;

	@Type(type="true_false")
	@Column(name = "FL_GERADOR_AUTOMATICO", nullable = false)
	private Boolean geradorAutomatico = false;

	@NotAudited
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "fase")
	private List<Processo> processos;

	@NotAudited
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "meioTramitacao", orphanRemoval = true)
	private List<MeioTramitacaoNaturezaAcao> meioTramitacaoNaturezas = Lists.newArrayList();

	public MeioTramitacao() {
		super();
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof MeioTramitacao)) {
			return false;
		}
		MeioTramitacao other = (MeioTramitacao) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getMascara() {
		return mascara;
	}

	public void setMascara(String mascara) {
		this.mascara = mascara;
	}

	public Date getDataExclusao() {
		return dataExclusao;
	}

	public void setDataExclusao(Date dataExclusao) {
		this.dataExclusao = dataExclusao;
	}

	public List<Processo> getProcessos() {
		return processos;
	}

	public void setProcessos(List<Processo> processos) {
		this.processos = processos;
	}

	@Override
	public String toString() {
		return "MeioTramitacao [id=" + id + ", descricao=" + descricao + "]";
	}

	public Long getCodigoEmpresa() {
		return codigoEmpresa;
	}

	public void setCodigoEmpresa(Long codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}

	public RoboTramitacao getRoboTramitacao() {
		return roboTramitacao;
	}

	public void setRoboTramitacao(RoboTramitacao roboTramitacao) {
		this.roboTramitacao = roboTramitacao;
	}

	public boolean isPossuiRobo(){
		return roboTramitacao != null;
	}

	public Boolean getGeradorAutomatico() {
		return geradorAutomatico;
	}

	public void setGeradorAutomatico(Boolean geradorAutomatico) {
		this.geradorAutomatico = geradorAutomatico;
	}

	public List<MeioTramitacaoNaturezaAcao> getMeioTramitacaoNaturezas() {
		return meioTramitacaoNaturezas;
	}

	public void setMeioTramitacaoNaturezas(List<MeioTramitacaoNaturezaAcao> meioTramitacaoNaturezas) {
		this.meioTramitacaoNaturezas = meioTramitacaoNaturezas;
	}

	public void addNaturezas(MeioTramitacaoNaturezaAcao meioTramitacaoNaturezaAcao){
		if(meioTramitacaoNaturezaAcao != null) {
			meioTramitacaoNaturezas.add(meioTramitacaoNaturezaAcao);
		}
	}

	public void validar(boolean existeMeioTramitacaoCadastrado) throws ValidationException {
		runValidations(buildValidatorFactory().validate(new MeioTramitacaoValidacaoWrapper(this, existeMeioTramitacaoCadastrado)));
	}
}
