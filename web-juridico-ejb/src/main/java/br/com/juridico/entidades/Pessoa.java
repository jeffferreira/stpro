package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import br.com.juridico.util.DateUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Jefferson
 */
@Entity
@Audited
@Table(name = "TB_PESSOA")
@NamedQueries({
        @NamedQuery(name = "Pessoa.findBydescricao", query = "SELECT t FROM Pessoa t WHERE t.codigoEmpresa=:codigoEmpresa and t.descricao = :descricao"),
        @NamedQuery(name = "Pessoa.findBytipo", query = "SELECT t FROM Pessoa t WHERE t.codigoEmpresa=:codigoEmpresa and t.tipo = :tipo"),
        @NamedQuery(name = "Pessoa.findByenviaEmail", query = "SELECT t FROM Pessoa t WHERE t.codigoEmpresa=:codigoEmpresa and t.enviaEmail = :enviaEmail") })
public class Pessoa implements IEntity, Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_PESSOA", nullable = false)
    private Long id;

    @Column(name = "DS_PESSOA", nullable = false, length = 200)
    private String descricao;

    @Column(name = "DS_NOME_INICIAIS", nullable = true, length = 3)
    private String nomeIniciais;

    @NotNull
    @Column(name = "FL_TIPO_PESSOA", nullable = false)
    private Character tipo;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_LOGIN", referencedColumnName = "ID_LOGIN")
    private Login login;

    @Type(type="true_false")
    @Column(name = "FL_POSSUI_EMAIL", nullable = false)
    private Boolean possuiEmail = true;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @ManyToOne
    @JoinColumn(name = "FK_IMAGE", referencedColumnName = "ID_IMAGE")
    private Imagem imagem;

    @Column(name = "DT_CADASTRO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataCadastro;

    @Type(type="true_false")
    @Column(name = "FL_ENVIAR_EMAIL", nullable = false)
    private Boolean enviaEmail = true;

    @Type(type="true_false")
    @Column(name = "FL_ATIVO", nullable = false)
    private Boolean ativo;

    @NotNull
    @Column(name = "NR_EMPRESA", nullable = false)
    private Long codigoEmpresa;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @ManyToOne
    @JoinColumn(name = "FK_PERFIL", referencedColumnName = "ID_PERFIL")
    private Perfil perfil;

    @Type(type="true_false")
    @Column(name = "FL_CLIENTE", nullable = false)
    private Boolean cliente;

    @Column(name = "DT_EXCLUSAO_LOGICA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataExclusao;

    @NotAudited
    @OneToMany(mappedBy = "pessoa")
    private List<PessoaFisica> pessoasFisicas = Lists.newArrayList();

    @NotAudited
    @OneToMany(mappedBy = "pessoa")
    private List<PessoaJuridica> pessoasJuridicas = Lists.newArrayList();

    @NotAudited
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pessoa", orphanRemoval = true)
    private List<PessoaContato> pessoasContatos = Lists.newArrayList();

    @NotAudited
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pessoa", orphanRemoval = true)
    private List<PessoaEndereco> pessoasEnderecos = Lists.newArrayList();

    @NotAudited
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pessoa", orphanRemoval = true)
    private List<PessoaEmail> pessoasEmails = Lists.newArrayList();

    @NotAudited
    @OneToMany(mappedBy = "pessoa")
    private List<PessoaConfiguracao> configuracoes = Lists.newArrayList();

    @NotAudited
    @OneToMany(mappedBy = "pessoa")
    private List<ContratoPessoa> contratos = Lists.newArrayList();

    @NotAudited
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "pessoa")
    @Fetch(FetchMode.SUBSELECT)
    private List<HistoricoPessoaCliente> historicoClienteList = Lists.newArrayList();

    public Pessoa() {
        super();
        this.historicoClienteList = Lists.newArrayList();
    }

    public Pessoa(Long id) {
        this.id = id;
    }

    public Pessoa(Long id, String descricao, Character tipo, Boolean enviaEmail) {
        this.id = id;
        this.descricao = descricao;
        this.tipo = tipo;
        this.enviaEmail = enviaEmail;
    }

    public String getClienteSimNao() {
        return isClienteEmpresa() ? "SIM" : "NÃO";
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(id).toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Pessoa) {
            final Pessoa other = (Pessoa) obj;
            return new EqualsBuilder().append(id, other.id).isEquals();
        } else {
            return false;
        }
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getNomeIniciais() {
        return nomeIniciais;
    }

    public void setNomeIniciais(String nomeIniciais) {
        if(Strings.isNullOrEmpty(nomeIniciais)){
            this.nomeIniciais = null;
            return;
        }
        this.nomeIniciais = nomeIniciais.toUpperCase();
    }

    public Character getTipo() {
        return tipo;
    }

    public void setTipo(Character tipo) {
        this.tipo = tipo;
    }

    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public Imagem getImagem() {
        return imagem;
    }

    public void setImagem(Imagem imagem) {
        this.imagem = imagem;
    }

    public Boolean getEnviaEmail() {
        return enviaEmail;
    }

    public void setEnviaEmail(Boolean enviaEmail) {
        this.enviaEmail = enviaEmail;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public void setCliente(Boolean cliente) {
        this.cliente = cliente;
    }

    public Boolean getCliente() {
        return cliente;
    }

    public List<PessoaFisica> getPessoasFisicas() {
        return pessoasFisicas;
    }

    public void setPessoasFisicas(List<PessoaFisica> pessoasFisicas) {
        this.pessoasFisicas = pessoasFisicas;
    }

    public List<PessoaJuridica> getPessoasJuridicas() {
        return pessoasJuridicas;
    }

    public void setPessoasJuridicas(List<PessoaJuridica> pessoasJuridicas) {
        this.pessoasJuridicas = pessoasJuridicas;
    }

    public List<PessoaContato> getPessoasContatos() {
        return pessoasContatos;
    }

    public void setPessoasContatos(List<PessoaContato> pessoasContatos) {
        this.pessoasContatos = pessoasContatos;
    }

    public List<PessoaEndereco> getPessoasEnderecos() {
        return pessoasEnderecos;
    }

    public void setPessoasEnderecos(List<PessoaEndereco> pessoasEnderecos) {
        this.pessoasEnderecos = pessoasEnderecos;
    }

    public List<PessoaEmail> getPessoasEmails() {
        return pessoasEmails;
    }

    public void setPessoasEmails(List<PessoaEmail> pessoasEmails) {
        this.pessoasEmails = pessoasEmails;
    }

    public Long getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(Long codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }

    public Perfil getPerfil() {
        return perfil;
    }

    public void setPerfil(Perfil perfil) {
        this.perfil = perfil;
    }

    public boolean isPessoaFisica() {
        return Character.valueOf('F').equals(tipo);
    }

    public Login getLogin() {
        return login;
    }

    public void setLogin(Login login) {
        this.login = login;
    }

    public Boolean getPossuiEmail() { return possuiEmail; }

    public void setPossuiEmail(Boolean possuiEmail) { this.possuiEmail = possuiEmail; }

    public Date getDataExclusao() {
        return dataExclusao;
    }

    public void setDataExclusao(Date dataExclusao) {
        this.dataExclusao = dataExclusao;
    }

    public List<PessoaConfiguracao> getConfiguracoes() {
        return configuracoes;
    }

    public void setConfiguracoes(List<PessoaConfiguracao> configuracoes) {
        this.configuracoes = configuracoes;
    }

    /**
     *
     * @return
     */
    public boolean isClienteEmpresa() {
        if(historicoClienteList == null || historicoClienteList.isEmpty()){
            return cliente == null ? false : cliente;
        }
        int tamanhoHistorico = historicoClienteList.size() -1;
        HistoricoPessoaCliente historicoPessoaCliente = historicoClienteList.get(tamanhoHistorico);
        return tamanhoHistorico >= 0 && historicoPessoaCliente.getCliente() != null && historicoPessoaCliente.getCliente();
    }

    public void addPessoasEmails(PessoaEmail pessoaEmail){
        if(pessoaEmail != null)
            pessoasEmails.add(pessoaEmail);
    }

    public void addPessoasContatos(PessoaContato pessoaContato){
        if(pessoaContato != null)
            pessoasContatos.add(pessoaContato);
    }

    public void addPessoasEnderecos(PessoaEndereco pessoaEndereco) {
        if(pessoaEndereco != null) {
            pessoasEnderecos.add(pessoaEndereco);
        }
    }

    public List<HistoricoPessoaCliente> getHistoricoClienteList() {
        return historicoClienteList;
    }

    public void setHistoricoClienteList(List<HistoricoPessoaCliente> historicoClienteList) {
        this.historicoClienteList = historicoClienteList;
    }

    public void addHistorico(HistoricoPessoaCliente historicoPessoaCliente){
        if(historicoPessoaCliente != null) {
            for (HistoricoPessoaCliente historico : historicoClienteList) {
                historico.setAtivo(Boolean.FALSE);
            }
            historicoPessoaCliente.setAtivo(Boolean.TRUE);
            this.historicoClienteList.add(historicoPessoaCliente);
        }
    }

    public String getDataCadastroFormatada() {
        return DateUtils.formataDataBrasil(this.dataCadastro);
    }

    @Override
    public String toString() {
        return "Pessoa [id=" + id + ", descricao=" + descricao + "]";
    }
}
