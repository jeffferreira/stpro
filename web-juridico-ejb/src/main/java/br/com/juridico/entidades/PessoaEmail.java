/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 *
 * @author Jefferson
 */
@Entity
@Table(name = "TB_PESSOA_EMAIL")
@NamedQueries({
		@NamedQuery(name = "PessoaEmail.findByid", query = "SELECT t FROM PessoaEmail t WHERE t.codigoEmpresa=:codigoEmpresa and t.id = :id"),
		@NamedQuery(name = "PessoaEmail.findByemail", query = "SELECT t FROM PessoaEmail t WHERE t.codigoEmpresa=:codigoEmpresa and t.email = :email") })
public class PessoaEmail implements IEntity, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_PESSOA_EMAIL", nullable = false)
	private Long id;

	@NotNull
	@Column(name = "DS_EMAIL", nullable = false, length = 75)
	private String email;

	@ManyToOne
	@JoinColumn(name = "FK_PESSOA", referencedColumnName = "ID_PESSOA", nullable = false)
	private Pessoa pessoa;

	@Column(name = "NR_EMPRESA", nullable = false)
	private Long codigoEmpresa;

	@Transient
	private transient int rowNumber;

	public PessoaEmail() {
		super();
	}

	public PessoaEmail(Long codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}

	public PessoaEmail(Pessoa pessoa, String email, Long codigoEmpresa) {
		this.pessoa = pessoa;
		this.email = email;
		this.codigoEmpresa = codigoEmpresa;
	}

	public boolean equals(Object obj) {
		if(!(obj instanceof PessoaEmail))
			return false;

		PessoaEmail pessoaEmail = (PessoaEmail) obj;

		return (pessoaEmail.getEmail() != null && pessoaEmail.getEmail().equals(email));
	}

	public int hashCode() {
		int hash = 1;
		if(email != null)
			hash = hash * 31 + email.hashCode();
		return hash;
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}


	public int getRowNumber() {
		return rowNumber;
	}

	public void setRowNumber(int rowNumber) {
		this.rowNumber = rowNumber;
	}

	public Long getCodigoEmpresa() {
		return codigoEmpresa;
	}

	public void setCodigoEmpresa(Long codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}

	@Override
	public String toString() {
		return "PessoaEmail[ id=" + id + " ]";
	}

}
