/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.juridico.entidades;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import br.com.juridico.dao.IEntity;

/**
 *
 * @author Jefferson
 */
@Entity
@Table(name = "TB_CIDADE", uniqueConstraints = { @UniqueConstraint(columnNames = { "DS_CIDADE" }) })
@NamedQueries({ @NamedQuery(name = "Cidade.findByid", query = "SELECT t FROM Cidade t WHERE t.id = :id"),
        @NamedQuery(name = "Cidade.findByUfDescricao", query = "SELECT t FROM Cidade t WHERE t.uf = :uf AND t.descricao = :descricao"),
        @NamedQuery(name = "Cidade.findBydescricao", query = "SELECT t FROM Cidade t WHERE t.descricao = :descricao") })
public class Cidade implements IEntity, Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_CIDADE", nullable = false)
    private Long id;

    @NotNull
    @Column(name = "DS_CIDADE", nullable = false, length = 255)
    private String descricao;

    @ManyToOne
    @JoinColumn(name = "FK_UF", referencedColumnName = "ID_UF")
    private Uf uf;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cidade")
    private List<Endereco> enderecos;

    public Cidade() {
        super();
    }

    public Cidade(Long id) {
        this.id = id;
    }

    public Cidade(Long id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Cidade cidade = (Cidade) o;

        if (id != null ? !id.equals(cidade.id) : cidade.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public List<Endereco> getEnderecos() {
        return enderecos;
    }

    public void setEnderecos(List<Endereco> enderecos) {
        this.enderecos = enderecos;
    }

    public Uf getUf() {
        return uf;
    }

    public void setUf(Uf uf) {
        this.uf = uf;
    }

    @Override
    public String toString() {
        return "Cidade[ id=" + id + " ]";
    }

}
