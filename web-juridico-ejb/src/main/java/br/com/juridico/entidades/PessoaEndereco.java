/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.juridico.entidades;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.com.juridico.dao.IEntity;

/**
 *
 * @author Jefferson
 */
@Entity
@Table(name = "TB_PESSOA_ENDERECO")
@NamedQueries({ @NamedQuery(name = "PessoaEndereco.findByid", query = "SELECT t FROM PessoaEndereco t WHERE t.codigoEmpresa=:codigoEmpresa and t.id = :id") })
public class PessoaEndereco implements IEntity, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_PESSOA_ENDERECO", nullable = false)
	private Long id;

	@JoinColumn(name = "FK_ENDERECO", referencedColumnName = "ID_ENDERECO", nullable = false)
	@ManyToOne(cascade = CascadeType.ALL, optional = false)
	private Endereco endereco;

    @ManyToOne
	@JoinColumn(name = "FK_PESSOA", referencedColumnName = "ID_PESSOA", nullable = false)
	private Pessoa pessoa;

	@Column(name = "NR_EMPRESA", nullable = false)
	private Long codigoEmpresa;

	@Transient
	private transient int rowNumber;

	public PessoaEndereco() {
		super();
		this.endereco = new Endereco(codigoEmpresa);
	}

	public PessoaEndereco(Long codigoEmpresa) {
        this.endereco = new Endereco(codigoEmpresa);
		this.codigoEmpresa = codigoEmpresa;
	}
	
	public PessoaEndereco(Endereco endereco, Pessoa pessoa, Long codigoEmpresa) {
		this.endereco = new Endereco(codigoEmpresa);
		this.endereco = endereco;
		this.pessoa = pessoa;
		this.codigoEmpresa = codigoEmpresa;
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public int getRowNumber() {
		return rowNumber;
	}

	public void setRowNumber(int rowNumber) {
		this.rowNumber = rowNumber;
	}

    public Long getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(Long codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }

	@Override
	public String toString() {
		return "PessoaEndereco [id=" + id + ", endereco=" + endereco + "]";
	}


}
