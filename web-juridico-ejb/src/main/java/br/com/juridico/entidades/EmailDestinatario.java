package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Jefferson on 06/08/2016.
 */
@Entity
@Table(name = "TB_EMAIL_DESTINATARIO")
public class EmailDestinatario implements IEntity, Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -8842508541112372866L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_EMAIL_DESTINATARIO", nullable = false)
    private Long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "FK_EMAIL_CAIXA_SAIDA", referencedColumnName = "ID_EMAIL_CAIXA_SAIDA", nullable = false)
    private EmailCaixaSaida emailCaixaSaida;

    @Column(name = "DS_EMAIL_DESTINATARIO", nullable = false)
    private String destinatario;

    public EmailDestinatario(){super();}

    public EmailDestinatario(EmailCaixaSaida emailCaixaSaida, String destinatario){
        this.emailCaixaSaida = emailCaixaSaida;
        this.destinatario = destinatario;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EmailCaixaSaida getEmailCaixaSaida() {
        return emailCaixaSaida;
    }

    public void setEmailCaixaSaida(EmailCaixaSaida emailCaixaSaida) {
        this.emailCaixaSaida = emailCaixaSaida;
    }

    public String getDestinatario() {
        return destinatario;
    }

    public void setDestinatario(String destinatario) {
        this.destinatario = destinatario;
    }

}
