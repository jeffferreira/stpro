/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Marcos
 */
@Entity
@Audited
@Table(name = "TB_ANDAMENTO_ADVOGADO")
@NamedQuery(name = "AndamentoAdvogado.findByid", query = "SELECT t FROM AndamentoAdvogado t WHERE t.codigoEmpresa=:codigoEmpresa and t.id = :id")
public class AndamentoAdvogado implements IEntity, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_ANDAMENTO_ADVOGADO", nullable = false)
	private Long id;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne
	@JoinColumn(name = "FK_ANDAMENTO", referencedColumnName = "ID_ANDAMENTO", nullable = false)
	private Andamento andamento;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@JoinColumn(name = "FK_PESSOA_FISICA", referencedColumnName = "ID_PESSOA_FISICA", nullable = false)
	@ManyToOne(optional = false)
	private PessoaFisica advogadoPessoa;

	@Column(name = "NR_EMPRESA", nullable = false)
	private Long codigoEmpresa;

	@Column(name = "DT_EXCLUSAO_LOGICA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataExclusao;

	public AndamentoAdvogado() {
		super();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder()
				.append(id).toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof AndamentoAdvogado) {
			final AndamentoAdvogado other = (AndamentoAdvogado) obj;
			return new EqualsBuilder().append(id, other.id).isEquals();
		} else {
			return false;
		}
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Andamento getAndamento() {
		return andamento;
	}

	public void setAndamento(Andamento andamento) {
		this.andamento = andamento;
	}

	public PessoaFisica getAdvogadoPessoa() {
		return advogadoPessoa;
	}

	public void setAdvogadoPessoa(PessoaFisica advogadoPessoa) {
		this.advogadoPessoa = advogadoPessoa;
	}

	public Long getCodigoEmpresa() {
		return codigoEmpresa;
	}

	public void setCodigoEmpresa(Long codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}

	public Date getDataExclusao() {
		return dataExclusao;
	}

	public void setDataExclusao(Date dataExclusao) {
		this.dataExclusao = dataExclusao;
	}
}
