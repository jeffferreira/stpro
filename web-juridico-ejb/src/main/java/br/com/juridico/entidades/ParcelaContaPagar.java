package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import br.com.juridico.enums.StatusParcelaContasType;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "TB_PARCELA_CONTA_PAGAR")
public class ParcelaContaPagar implements IEntity, Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_PARCELA_CONTA_PAGAR", nullable = false)
    private Long id;

    @Column(name = "VL_CONTA_PAGAR", nullable = false, scale = 2)
    private BigDecimal valor;

    @Column(name = "DT_VENCIMENTO")
    @Temporal(TemporalType.DATE)
    private Date dataVencimento;

    @Column(name = "DT_PAGAMENTO")
    @Temporal(TemporalType.DATE)
    private Date dataPagamento;

    @Column(name = "NR_PARCELA")
    private Integer numeroParcela;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "FK_CONTA_PAGAR", updatable = true, nullable = false)
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    private ContaPagar contaPagar;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_PAGAMENTO_CONTA_PAGAR")
    private PagamentoContaPagar pagamentoContaPagar;

    @Enumerated(EnumType.STRING)
    @Column(length = 2, name = "FL_STATUS_PARCELA", columnDefinition = "char")
    private StatusParcelaContasType statusParcela;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public Date getDataVencimento() {
        return dataVencimento;
    }

    public void setDataVencimento(Date dataVencimento) {
        this.dataVencimento = dataVencimento;
    }

    public Date getDataPagamento() {
        return dataPagamento;
    }

    public void setDataPagamento(Date dataPagamento) {
        this.dataPagamento = dataPagamento;
    }

    public Integer getNumeroParcela() {
        return numeroParcela;
    }

    public void setNumeroParcela(Integer numeroParcela) {
        this.numeroParcela = numeroParcela;
    }

    public ContaPagar getContaPagar() {
        return contaPagar;
    }

    public void setContaPagar(ContaPagar contaPagar) {
        this.contaPagar = contaPagar;
    }

    public PagamentoContaPagar getPagamentoContaPagar() {
        return pagamentoContaPagar;
    }

    public void setPagamentoContaPagar(PagamentoContaPagar pagamentoContaPagar) {
        this.pagamentoContaPagar = pagamentoContaPagar;
    }

    public StatusParcelaContasType getStatusParcela() {
        return statusParcela;
    }

    public void setStatusParcela(StatusParcelaContasType statusParcela) {
        this.statusParcela = statusParcela;
    }
}
