package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import br.com.juridico.enums.TipoPessoaType;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.cadastros.FornecedorValidacaoWrapper;
import br.com.juridico.validation.cadastros.VaraValidacaoWrapper;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by jefferson.ferreira on 17/05/2017.
 */
@Entity
@Table(name = "tb_fornecedor")
public class Fornecedor implements IEntity, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_FORNECEDOR", nullable = false)
    private Long id;

    @Column(name = "DS_NOME ", nullable = false)
    private String nome;

    @Enumerated(EnumType.STRING)
    @Column(name = "FL_TIPO_PESSOA")
    private TipoPessoaType tipoPessoa = TipoPessoaType.F;

    @Column(name = "DS_CNPJ_CPF")
    private String cpfCnpj;

    @Column(name = "DS_APELIDO")
    private String apelido;

    @Enumerated(EnumType.STRING)
    @Column(name = "FL_TIPO_FAVORECIDO ")
    private TipoPessoaType tipoFavorecido = TipoPessoaType.F;

    @Column(name = "DS_NOME_FAVORECIDO ", nullable = false)
    private String nomeFavorecido;

    @Column(name = "DS_CPF_CNPJ_FAVORECIDO")
    private String cpfCnpjFavorecido;

    @Column(name = "DS_BANCO_FAVORECIDO")
    private String bancoFavorecido;

    @Column(name = "DS_AGENCIA_FAVORECIDO")
    private String agenciaFavorecido;

    @Column(name = "DS_CONTA_CORRENTE_FAVORECIDO")
    private String contaFavorecido;

    @Column(name = "NR_EMPRESA", nullable = false)
    private Long codigoEmpresa;

    @Column(name = "DT_EXCLUSAO_LOGICA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataExclusao;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpfCnpj() {
        return cpfCnpj;
    }

    public void setCpfCnpj(String cpfCnpj) {
        this.cpfCnpj = cpfCnpj;
    }

    public String getApelido() {
        return apelido;
    }

    public void setApelido(String apelido) {
        this.apelido = apelido;
    }

    public String getNomeFavorecido() {
        return nomeFavorecido;
    }

    public void setNomeFavorecido(String nomeFavorecido) {
        this.nomeFavorecido = nomeFavorecido;
    }

    public String getCpfCnpjFavorecido() {
        return cpfCnpjFavorecido;
    }

    public void setCpfCnpjFavorecido(String cpfCnpjFavorecido) {
        this.cpfCnpjFavorecido = cpfCnpjFavorecido;
    }

    public String getBancoFavorecido() {
        return bancoFavorecido;
    }

    public void setBancoFavorecido(String bancoFavorecido) {
        this.bancoFavorecido = bancoFavorecido;
    }

    public String getAgenciaFavorecido() {
        return agenciaFavorecido;
    }

    public void setAgenciaFavorecido(String agenciaFavorecido) {
        this.agenciaFavorecido = agenciaFavorecido;
    }

    public String getContaFavorecido() {
        return contaFavorecido;
    }

    public void setContaFavorecido(String contaFavorecido) {
        this.contaFavorecido = contaFavorecido;
    }

    public TipoPessoaType getTipoPessoa() {
        return tipoPessoa;
    }

    public void setTipoPessoa(TipoPessoaType tipoPessoa) {
        this.tipoPessoa = tipoPessoa;
    }

    public TipoPessoaType getTipoFavorecido() {
        return tipoFavorecido;
    }

    public void setTipoFavorecido(TipoPessoaType tipoFavorecido) {
        this.tipoFavorecido = tipoFavorecido;
    }

    public Long getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(Long codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }

    public Date getDataExclusao() {
        return dataExclusao;
    }

    public void setDataExclusao(Date dataExclusao) {
        this.dataExclusao = dataExclusao;
    }

    public void validar(boolean existeFornecedorCadastrado, boolean cadastrarFavorecido) throws ValidationException {
        runValidations(buildValidatorFactory().validate(new FornecedorValidacaoWrapper(this, existeFornecedorCadastrado, cadastrarFavorecido)));
    }
}
