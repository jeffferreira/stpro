package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.cadastros.GrupoValidacaoWrapper;
import com.google.common.collect.Lists;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Jefferson
 */
@Entity
@Audited
@Table(name = "TB_GRUPO")
@NamedQueries({
		@NamedQuery(name = "Grupo.findByid", query = "SELECT t FROM Grupo t WHERE t.codigoEmpresa=:codigoEmpresa and t.id = :id"),
		@NamedQuery(name = "Grupo.findByDescricao", query = "SELECT t FROM Grupo t WHERE t.codigoEmpresa=:codigoEmpresa and t.descricao = :descricao") })
public class Grupo implements IEntity, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_GRUPO", nullable = false)
	private Long id;

	@NotNull
	@NotEmpty(message = "Descrição: preenchimento obrigatório")
	@Column(name = "DS_GRUPO", nullable = false, length = 200)
	private String descricao;

	@Column(name = "NR_EMPRESA", nullable = false)
	private Long codigoEmpresa;

	@Column(name = "DT_EXCLUSAO_LOGICA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataExclusao;

	@NotAudited
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "grupo", orphanRemoval = true)
	private List<GrupoAdvogado> gruposAdvogados = Lists.newArrayList();

	public Grupo() {
		super();
	}

	public Grupo(Long id, String descricao) {
		this.id = id;
		this.descricao = descricao;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof Grupo)) {
			return false;
		}
		Grupo other = (Grupo) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Date getDataExclusao() {
		return dataExclusao;
	}

	public void setDataExclusao(Date dataExclusao) {
		this.dataExclusao = dataExclusao;
	}

	public List<GrupoAdvogado> getGruposAdvogados() {
		return gruposAdvogados;
	}

	public void setGruposAdvogados(List<GrupoAdvogado> gruposAdvogados) {
		this.gruposAdvogados = gruposAdvogados;
	}

	public Long getCodigoEmpresa() {
		return codigoEmpresa;
	}

	public void setCodigoEmpresa(Long codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}

	public void addGruposAdvogados(GrupoAdvogado grupoAdvogado){
		if(grupoAdvogado != null)
			this.gruposAdvogados.add(grupoAdvogado);
	}

	public void validar(boolean existeGrupoCadastrado) throws ValidationException {
		runValidations(buildValidatorFactory().validate(new GrupoValidacaoWrapper(this, existeGrupoCadastrado)));
	}

	@Override
	public String toString() {
		return "Grupo [id=" + id + ", descricao=" + descricao + "]";
	}
}
