package br.com.juridico.entidades;

import br.com.caelum.stella.format.CPFFormatter;
import br.com.caelum.stella.format.Formatter;
import br.com.juridico.dao.IEntity;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.pessoas.AdvogadoValidacaoWrapper;
import br.com.juridico.validation.pessoas.ColaboradorValidacaoWrapper;
import br.com.juridico.validation.pessoas.PessoaExistenteValidacaoWrapper;
import br.com.juridico.validation.pessoas.PessoaFisicaValidacaoWrapper;
import com.google.common.base.Strings;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Jefferson
 */
@Entity
@Audited
@Table(name = "TB_PESSOA_FISICA")
@NamedQueries({
        @NamedQuery(name = "PessoaFisica.findBycpf", query = "SELECT t FROM PessoaFisica t WHERE t.codigoEmpresa=:codigoEmpresa and t.cpf = :cpf"),
        @NamedQuery(name = "PessoaFisica.findBysexo", query = "SELECT t FROM PessoaFisica t WHERE t.codigoEmpresa=:codigoEmpresa and t.sexo = :sexo"),
        @NamedQuery(name = "PessoaFisica.findBydataNascimento", query = "SELECT t FROM PessoaFisica t WHERE t.codigoEmpresa=:codigoEmpresa and t.dataNascimento = :dataNascimento"),
        @NamedQuery(name = "PessoaFisica.findByrg", query = "SELECT t FROM PessoaFisica t WHERE t.codigoEmpresa=:codigoEmpresa and t.rg = :rg"),
        @NamedQuery(name = "PessoaFisica.findByprofissao", query = "SELECT t FROM PessoaFisica t WHERE t.codigoEmpresa=:codigoEmpresa and t.profissao = :profissao"),
        @NamedQuery(name = "PessoaFisica.findByobservacao", query = "SELECT t FROM PessoaFisica t WHERE t.codigoEmpresa=:codigoEmpresa and t.observacao = :observacao") })
public class PessoaFisica implements IEntity, Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_PESSOA_FISICA", nullable = false)
    private Long id;

    @Column(name = "DS_CPF", length = 11)
    private String cpf;

    @Column(name = "FL_SEXO", length = 1)
    private Character sexo;

    @Temporal(TemporalType.DATE)
    @Column(name = "DT_NASCIMENTO")
    private Date dataNascimento;

    @Column(name = "DS_RG", length = 12)
    private String rg;

    @Column(name = "DS_PROFISSAO", length = 100)
    private String profissao;

    @Column(name = "DS_OBSERVACOES", length = 4000)
    private String observacao;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @ManyToOne(cascade = CascadeType.ALL, optional = false)
    @JoinColumn(name = "FK_PESSOA", referencedColumnName = "ID_PESSOA", nullable = false)
    private Pessoa pessoa;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @ManyToOne
    @JoinColumn(name = "FK_ESTADO_CIVIL", referencedColumnName = "ID_ESTADO_CIVIL")
    private EstadoCivil estadoCivil;

    @Type(type="true_false")
    @NotNull(message = "A indicação de Advogado é de preenchimento obrigatório")
    @Column(name = "FL_ADVOGADO", nullable = false)
    private Boolean advogado;

    @Type(type="true_false")
    @Column(name = "FL_ADVOGADO_INTERNO", nullable = false)
    private Boolean advogadoInterno = false;

    @Type(type="true_false")
    @Column(name = "FL_ADVOGADO_TERCEIRIZADO", nullable = false)
    private Boolean advogadoTerceirizado = false;

    @Type(type="true_false")
    @Column(name = "FL_COLABORADOR", nullable = false)
    private Boolean colaborador = false;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @ManyToOne
    @JoinColumn(name = "FK_FUNCAO", referencedColumnName = "ID_FUNCAO")
    private Funcao funcao;

    @Column(name = "NR_OAB", length = 20)
    private String nrOab;

    @Type(type="true_false")
    @NotNull(message = "A indicação de Receber Publicações é de preenchimento obrigatório")
    @Column(name = "FL_RECEBER_PUBLICACOES", nullable = false)
    private Boolean receberPublicacoes = false;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @ManyToOne
    @JoinColumn(name = "FK_UF_OAB", referencedColumnName = "ID_UF", nullable = true)
    private Uf ufOab;

    @Column(name = "NR_EMPRESA", nullable = false)
    private Long codigoEmpresa;

    @Column(name = "DT_EXCLUSAO_LOGICA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataExclusao;

    @NotAudited
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "advogadoPessoa")
    private List<GrupoAdvogado> gruposAdvogados;

    @NotAudited
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "advogadoPessoa")
    private List<AndamentoAdvogado> andamentoAdvogados;

    @Transient
    private Integer ratingPrincipal;

    public PessoaFisica() {
        super();
        setPessoa(new Pessoa());
    }

    public PessoaFisica(Long id, String cpf, Character sexo, Date dataNascimento) {
        this.id = id;
        this.cpf = cpf;
        this.sexo = sexo;
        this.dataNascimento = dataNascimento;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PessoaFisica other = (PessoaFisica) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Character getSexo() {
        return sexo;
    }

    public void setSexo(Character sexo) {
        this.sexo = sexo;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String getProfissao() {
        return profissao;
    }

    public void setProfissao(String profissao) {
        this.profissao = profissao;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public EstadoCivil getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(EstadoCivil estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public String getNrOab() {
        return nrOab;
    }

    public void setNrOab(String nrOab) {
        this.nrOab = nrOab;
    }

    public Uf getUfOab() {
        return ufOab;
    }

    public void setUfOab(Uf ufOab) {
        this.ufOab = ufOab;
    }

    public List<GrupoAdvogado> getGruposAdvogados() {
        return gruposAdvogados;
    }

    public void setGruposAdvogados(List<GrupoAdvogado> gruposAdvogados) {
        this.gruposAdvogados = gruposAdvogados;
    }

    public Long getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(Long codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }

    public List<AndamentoAdvogado> getAndamentoAdvogados() {
        return andamentoAdvogados;
    }

    public void setAndamentoAdvogados(List<AndamentoAdvogado> andamentoAdvogados) {
        this.andamentoAdvogados = andamentoAdvogados;
    }

    public Boolean getAdvogado() {
        return advogado;
    }

    public void setAdvogado(Boolean advogado) {
        this.advogado = advogado;
    }

    public Boolean getAdvogadoInterno() {
        return advogadoInterno;
    }

    public void setAdvogadoInterno(Boolean advogadoInterno) {
        this.advogadoInterno = advogadoInterno;
    }

    public void setAdvogadoTerceirizado(Boolean advogadoTerceirizado) {
        this.advogadoTerceirizado = advogadoTerceirizado;
    }

    public void setColaborador(Boolean colaborador) {
        this.colaborador = colaborador;
    }

    public Boolean getReceberPublicacoes() {
        return receberPublicacoes;
    }

    public void setReceberPublicacoes(Boolean receberPublicacoes) {
        this.receberPublicacoes = receberPublicacoes;
    }

    public Date getDataExclusao() {
        return dataExclusao;
    }

    public void setDataExclusao(Date dataExclusao) {
        this.dataExclusao = dataExclusao;
    }


    public Funcao getFuncao() {
        return funcao;
    }

    public void setFuncao(Funcao funcao) {
        this.funcao = funcao;
    }

    public Boolean getAdvogadoTerceirizado() {
        return advogadoTerceirizado;
    }

    public Boolean getColaborador() {
        return colaborador;
    }

    public Integer getRatingPrincipal() {
        return ratingPrincipal;
    }

    public void setRatingPrincipal(Integer ratingPrincipal) {
        this.ratingPrincipal = ratingPrincipal;
    }

    /**
     *
     * @throws ValidationException
     */
    public void validar() throws ValidationException {
        runValidations(buildValidatorFactory().validate(new PessoaFisicaValidacaoWrapper(this)));
    }

    /**
     *
     * @param cpfJaExiste
     * @param nomeJaExiste
     * @throws ValidationException
     */
    public void validarPessoaJaCadastrada(boolean cpfJaExiste, boolean nomeJaExiste) throws ValidationException {
        runValidations(buildValidatorFactory().validate(new PessoaExistenteValidacaoWrapper(true, cpfJaExiste, nomeJaExiste)));
    }

    /**
     *
     * @param existeNumeroOabCadastrado
     * @throws ValidationException
     */
    public void validarAdvogado(boolean existeNumeroOabCadastrado) throws ValidationException {
        runValidations(buildValidatorFactory().validate(new AdvogadoValidacaoWrapper(this, existeNumeroOabCadastrado)));
    }

    public boolean isAdvogadoInternoEscritorio() {
        return advogadoInterno != null && advogadoInterno;
    }

    public boolean isAdvogadoTerceirizadoEscritorio() {
        return advogadoTerceirizado != null && advogadoTerceirizado;
    }

    /**
     *
     * @param existeNumeroOabCadastrado
     * @throws ValidationException
     */
    public void validarColaborador(boolean existeNumeroOabCadastrado) throws ValidationException {
        runValidations(buildValidatorFactory().validate(new ColaboradorValidacaoWrapper(this, existeNumeroOabCadastrado)));
    }

    public boolean isColaboradorEscritorio() {
        return colaborador != null && colaborador;
    }

    public String getEmails() {
        return getEmails(pessoa);
    }

    public String getTelefones() {
        return getTelefones(pessoa);
    }

    public String getNomePessoa(){
        if(pessoa != null && !Strings.isNullOrEmpty(pessoa.getDescricao())){
            return this.pessoa.getDescricao();
        }
        return "";
    }

    public String getCpfFormatado(){
        if(Strings.isNullOrEmpty(cpf)){
            return "";
        }
        Formatter formatter = new CPFFormatter();
        return formatter.format(cpf);
    }

    @Override
    public String toString() {
        if(pessoa == null) {
            return "";
        }
        return pessoa.getDescricao();
    }


}
