package br.com.juridico.entidades;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.juridico.dao.IEntity;

@Entity
@Table(name = "PFL_DIREITO")
public class Direito implements IEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_DIREITO", nullable = false)
	private Long id;

	@Column(name = "DS_SIGLA", nullable = false)
	private String sigla;

	@Column(name = "DS_DIREITO", nullable = false)
	private String descricao;

	public Direito(){}

	public Direito(String sigla, String descricao){
		this.sigla = sigla;
		this.descricao = descricao;
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
