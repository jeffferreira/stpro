package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import com.google.common.collect.Lists;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Jefferson
 */
@Entity
@Table(name = "projudipr_movimentacao")
public class ProjudiprMovimentacao implements IEntity, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_MOVIMENTACAO", nullable = false)
	private Long id;

	@Column(name = "DS_SEQUENCIA", length = 10)
	private String sequencia;

	@Column(name = "DS_DATA", length = 25)
	private String data;

	@Column(name = "DS_EVENTO")
	private String evento;

	@Column(name = "DS_MOVIMENTADO_POR")
	private String movimentadoPor;

	@ManyToOne
	@JoinColumn(name = "FK_PROJUDI_PROCESSO", referencedColumnName = "ID_PROCESSO", nullable = false)
	private ProjudiprProcesso processo;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "movimentacao", orphanRemoval = true)
	private List<ProjudiprProcessoControladoria> controladoriaProcessos = Lists.newArrayList();

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "movimentacao", orphanRemoval = true)
	private List<ProjudiprProcessoDocumento> documentoProcessos = Lists.newArrayList();

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DT_ULTIMA_ATUALIZACAO")
	private Date dataUltimaAtualizacao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSequencia() {
		return sequencia;
	}

	public void setSequencia(String sequencia) {
		this.sequencia = sequencia;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getEvento() {
		return evento;
	}

	public void setEvento(String evento) {
		this.evento = evento;
	}

	public String getMovimentadoPor() {
		return movimentadoPor;
	}

	public void setMovimentadoPor(String movimentadoPor) {
		this.movimentadoPor = movimentadoPor;
	}

	public ProjudiprProcesso getProcesso() {
		return processo;
	}

	public void setProcesso(ProjudiprProcesso processo) {
		this.processo = processo;
	}

	public List<ProjudiprProcessoControladoria> getControladoriaProcessos() {
		return controladoriaProcessos;
	}

	public void setControladoriaProcessos(List<ProjudiprProcessoControladoria> controladoriaProcessos) {
		this.controladoriaProcessos = controladoriaProcessos;
	}

    public List<ProjudiprProcessoDocumento> getDocumentoProcessos() {
        return documentoProcessos;
    }

    public void setDocumentoProcessos(List<ProjudiprProcessoDocumento> documentoProcessos) {
        this.documentoProcessos = documentoProcessos;
    }

    public Date getDataUltimaAtualizacao() {
		return dataUltimaAtualizacao;
	}

	public void setDataUltimaAtualizacao(Date dataUltimaAtualizacao) {
		this.dataUltimaAtualizacao = dataUltimaAtualizacao;
	}

	public int formatSequenciaToInt() {
	    return Integer.parseInt(sequencia);
    }
}
