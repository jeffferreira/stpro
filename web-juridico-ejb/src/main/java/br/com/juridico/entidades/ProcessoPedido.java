package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Audited
@Table(name = "TB_PROCESSO_PEDIDO")
public class ProcessoPedido implements IEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8522371972484003577L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_PROCESSO_PEDIDO", nullable = false)
	private Long id;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@JoinColumn(name = "FK_PROCESSO", referencedColumnName = "ID_PROCESSO", nullable = false)
	@ManyToOne(optional = false)
	private Processo processo;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@JoinColumn(name = "FK_PEDIDO", referencedColumnName = "ID_PEDIDO", nullable = false)
	@ManyToOne(optional = false)
	private Pedido pedido;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@JoinColumn(name = "FK_CAUSA_PEDIDO", referencedColumnName = "ID_CAUSA_PEDIDO", nullable = false)
	@ManyToOne(optional = false)
	private CausaPedido causaPedido;

    @Column(name = "VL_PEDIDO", nullable = false)
    private BigDecimal valorPedido;

    @Column(name = "VL_PROVAVEL", precision = 20, scale = 2)
    private BigDecimal valorProvavel;

	@Column(name = "NR_EMPRESA", nullable = false)
	private Long codigoEmpresa;

	public ProcessoPedido() {
		super();
	}

	public ProcessoPedido(Pedido pedido, Processo processo, CausaPedido causaPedido, BigDecimal valorPedido, Long codigoEmpresa) {
		this.pedido = pedido;
		this.processo = processo;
		this.causaPedido = causaPedido;
        this.valorPedido = valorPedido;
		this.codigoEmpresa = codigoEmpresa;
	}

    public ProcessoPedido(Long codigoEmpresa){
        this.codigoEmpresa = codigoEmpresa;
    }

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	public Processo getProcesso() {
		return processo;
	}

	public void setProcesso(Processo processo) {
		this.processo = processo;
	}

	public Long getCodigoEmpresa() {
		return codigoEmpresa;
	}

	public void setCodigoEmpresa(Long codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}

	public CausaPedido getCausaPedido() {
		return causaPedido;
	}

	public void setCausaPedido(CausaPedido causaPedido) {
		this.causaPedido = causaPedido;
	}

    public BigDecimal getValorPedido() {
        return valorPedido;
    }

    public void setValorPedido(BigDecimal valorPedido) {
        this.valorPedido = valorPedido;
    }

    public BigDecimal getValorProvavel() {
		return valorProvavel;
	}

	public void setValorProvavel(BigDecimal valorProvavel) {
		this.valorProvavel = valorProvavel;
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProcessoPedido that = (ProcessoPedido) o;

        if (causaPedido != null ? !causaPedido.equals(that.causaPedido) : that.causaPedido != null) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (pedido != null ? !pedido.equals(that.pedido) : that.pedido != null) return false;
        if (processo != null ? !processo.equals(that.processo) : that.processo != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (processo != null ? processo.hashCode() : 0);
        result = 31 * result + (pedido != null ? pedido.hashCode() : 0);
        result = 31 * result + (causaPedido != null ? causaPedido.hashCode() : 0);
        return result;
    }
}
