package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import br.com.juridico.enums.TipoRetornoRoboIndicador;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Jefferson
 */
@Entity
@Table(name = "projudipr_processo")
public class ProjudiprProcesso implements IEntity, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID_PROCESSO", nullable = false)
	private Long id;

	@Column(name = "DS_PROCESSO", length = 125)
	private String processo;

	@Column(name = "DT_CADASTRO", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCadastro;

	@Enumerated(EnumType.STRING)
	@Column(name = "DS_RETORNO_ROBO", length = 45)
	private TipoRetornoRoboIndicador tipoRetornoRobo;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "processo", orphanRemoval = true)
	private List<ProjudiprParte> partes = Lists.newArrayList();

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "processo", orphanRemoval = true)
	private List<ProjudiprMovimentacao> movimentacoes = Lists.newArrayList();

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "processo", orphanRemoval = true)
	private List<ProjudiprProcessoDetalhe> detalhes = Lists.newArrayList();

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "processo", orphanRemoval = true)
	private List<ProjudiprProcessoInformacao> informacoes = Lists.newArrayList();

	@Transient
	private Map<String, List<ProjudiprParte>> mapaPartes = Maps.newHashMap();

	@Transient
	private boolean edicao;

	public ProjudiprProcesso(){
		this.dataCadastro = new Date();
	}

	public ProjudiprProcesso(Long id, String processo, TipoRetornoRoboIndicador tipoRetornoRobo) {
		this.id = id;
		this.processo = processo;
		this.tipoRetornoRobo = tipoRetornoRobo;
		this.dataCadastro = new Date();
		this.edicao = Boolean.FALSE;
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProcesso() {
		return processo;
	}

	public void setProcesso(String processo) {
		this.processo = processo;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public TipoRetornoRoboIndicador getTipoRetornoRobo() {
		return tipoRetornoRobo;
	}

	public void setTipoRetornoRobo(TipoRetornoRoboIndicador tipoRetornoRobo) {
		this.tipoRetornoRobo = tipoRetornoRobo;
	}

	public List<ProjudiprParte> getPartes() {
		return partes;
	}

	public void setPartes(List<ProjudiprParte> partes) {
		this.partes = partes;
	}

	public List<ProjudiprMovimentacao> getMovimentacoes() {
		return movimentacoes;
	}

	public void setMovimentacoes(List<ProjudiprMovimentacao> movimentacoes) {
		this.movimentacoes = movimentacoes;
	}

	public List<ProjudiprProcessoDetalhe> getDetalhes() {
		return detalhes;
	}

	public void setDetalhes(List<ProjudiprProcessoDetalhe> detalhes) {
		this.detalhes = detalhes;
	}

	public List<ProjudiprProcessoInformacao> getInformacoes() {
		return informacoes;
	}

	public void setInformacoes(List<ProjudiprProcessoInformacao> informacoes) {
		this.informacoes = informacoes;
	}

	public boolean isEdicao() {
		return edicao;
	}

	public void setEdicao(boolean edicao) {
		this.edicao = edicao;
	}

	public Map<String, List<ProjudiprParte>> getMapaPartes() {
		mapaPartes.clear();

		for (ProjudiprParte parte : this.partes) {
			List<ProjudiprParte> result = mapaPartes.get(parte.getTipo());

			if(result == null) {
				List<ProjudiprParte> partes = Lists.newArrayList();
				partes.add(parte);
				this.mapaPartes.put(parte.getTipo(), partes);
				continue;
			}
			this.mapaPartes.get(parte.getTipo()).add(parte);
		}
		return mapaPartes;
	}

	public void setMapaPartes(Map<String, List<ProjudiprParte>> mapaPartes) {
		this.mapaPartes = mapaPartes;
	}
}
