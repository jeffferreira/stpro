package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import br.com.juridico.enums.TipoEndereco;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Jefferson
 */
@Entity
@Table(name = "TB_ENDERECO")
@NamedQueries({
        @NamedQuery(name = "Endereco.findBydescricao", query = "SELECT t FROM Endereco t WHERE t.codigoEmpresa=:codigoEmpresa and t.descricao = :descricao"),
        @NamedQuery(name = "Endereco.findBynumero", query = "SELECT t FROM Endereco t WHERE t.codigoEmpresa=:codigoEmpresa and t.numero = :numero"),
        @NamedQuery(name = "Endereco.findBycomplemento", query = "SELECT t FROM Endereco t WHERE t.codigoEmpresa=:codigoEmpresa and t.complemento = :complemento"),
        @NamedQuery(name = "Endereco.findBybairro", query = "SELECT t FROM Endereco t WHERE t.codigoEmpresa=:codigoEmpresa and t.bairro = :bairro"),
        @NamedQuery(name = "Endereco.findBycep", query = "SELECT t FROM Endereco t WHERE t.codigoEmpresa=:codigoEmpresa and t.cep = :cep") })
public class Endereco implements IEntity, Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_ENDERECO", nullable = false)
    private Long id;

    @NotNull
    @Column(name = "DS_ENDERECO", nullable = false, length = 200)
    private String descricao;

    @Column(name = "DS_NUMERO", length = 25)
    private String numero;

    @Column(name = "DS_COMPLEMENTO", length = 255)
    private String complemento;

    @Column(name = "DS_BAIRRO", length = 255)
    private String bairro;

    @Column(name = "DS_CEP")
    private String cep;

    @JoinColumn(name = "FK_CIDADE", referencedColumnName = "ID_CIDADE", nullable = false)
    @ManyToOne(optional = false)
    private Cidade cidade;

    @Column(name = "NR_EMPRESA", nullable = false)
    private Long codigoEmpresa;
    
    @Column(name = "FL_TIPO_ENDERECO", length = 1)
    private String tipoEndereco;

    @Type(type="true_false")
    @Column(name = "FL_SEM_NUMERO")
    private Boolean semNumero;

    @OneToMany(mappedBy = "endereco")
    private List<PessoaEndereco> pessoas;

    public Endereco() {
        super();
    }

    public Endereco(Long codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }

    public Endereco(Long id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    public Endereco(String descricao, String cep, Cidade cidade, String tipoEndereco){
        this.descricao = descricao;
        this.cep = cep;
        this.cidade = cidade;
        this.tipoEndereco = tipoEndereco;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    public List<PessoaEndereco> getPessoas() {
        return pessoas;
    }

    public void setPessoas(List<PessoaEndereco> pessoas) {
        this.pessoas = pessoas;
    }

    public String getTipoEnderecoDescricao(){
        return TipoEndereco.RESIDENCIAL.getValue().equals(tipoEndereco) ? TipoEndereco.RESIDENCIAL.getDescricao() : TipoEndereco.COMERCIAL.getDescricao();
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Endereco)) {
            return false;
        }
        Endereco other = (Endereco) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Endereco[ id=" + id + " ]";
    }

    public Long getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(Long codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }

	public String getTipoEndereco() {
		return tipoEndereco;
	}

	public void setTipoEndereco(String tipoEndereco) {
		this.tipoEndereco = tipoEndereco;
	}

    public Boolean getSemNumero() {
        return semNumero;
    }

    public void setSemNumero(Boolean semNumero) {
        this.semNumero = semNumero;
    }
}
