package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by jefferson.ferreira on 11/01/2017.
 */
@Entity
@Table(name = "ADMIN_EMPRESA_CONF")
public class EmpresaConfiguracao implements IEntity, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_EMPRESA_CONF", nullable = false)
    private Long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "FK_EMPRESA", referencedColumnName = "ID_EMPRESA")
    private Empresa empresa;

    @ManyToOne(optional = false)
    @JoinColumn(name = "FK_CONFIGURACAO", referencedColumnName = "ID_CONFIGURACAO")
    private Configuracao configuracao;

    public EmpresaConfiguracao() {
    }

    public EmpresaConfiguracao(Empresa empresa, Configuracao configuracao) {
        this.empresa = empresa;
        this.configuracao = configuracao;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(id).append(empresa).append(configuracao).toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof EmpresaConfiguracao) {
            final EmpresaConfiguracao other = (EmpresaConfiguracao) obj;
            return new EqualsBuilder().append(id, other.id)
                    .append(empresa, other.empresa)
                    .append(configuracao, other.configuracao).isEquals();
        } else {
            return false;
        }
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Configuracao getConfiguracao() {
        return configuracao;
    }

    public void setConfiguracao(Configuracao configuracao) {
        this.configuracao = configuracao;
    }
}
