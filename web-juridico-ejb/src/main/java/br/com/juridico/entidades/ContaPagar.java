
package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import br.com.juridico.enums.ContasPagarType;
import br.com.juridico.enums.StatusContasPagarType;
import br.com.juridico.enums.TipoPessoaType;
import br.com.juridico.exception.ValidationException;
import com.google.common.collect.Lists;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Jefferson
 */
@Entity
@Table(name = "tb_conta_pagar")
public class ContaPagar implements IEntity, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_CONTA_PAGAR", nullable = false)
	private Long id;

	@Temporal(TemporalType.DATE)
	@Column(name = "DT_EMISSAO", nullable = false)
	private Date dataEmissao;

	@Column(name = "NR_NOTA_FISCAL", nullable = false)
	private String notaFiscal;

	@ManyToOne
	@JoinColumn(name = "FK_FORNECEDOR", referencedColumnName = "ID_FORNECEDOR", nullable = false)
	private Fornecedor fornecedor;

	@Column(name = "VL_TOTAL", nullable = false)
	private BigDecimal valorTotal;

	@Enumerated(EnumType.STRING)
	@Column(name = "FL_STATUS")
	private StatusContasPagarType status;

	@Enumerated(EnumType.STRING)
	@Column(name = "FL_TIPO")
	private ContasPagarType tipo = ContasPagarType.B;

	@Column(name = "DS_BANCO")
	private String banco;

	@Column(name = "DS_AGENCIA")
	private String agencia;

	@Column(name = "DS_CONTA")
	private String contaCorrente;

	@Column(name = "VL_LIQUIDO")
	private BigDecimal valorLiquido;

	@Column(name = "DS_CONTA_PAGAR")
	private String descricao;

	@Column(name = "DS_COMPLEMENTO")
	private String complemento;

	@Column(name = "DT_CADASTRO")
	private Date dataCadastro;

	@Column(name = "NR_QTDE_PARCELA")
	private Integer qtdeParcelas;

	@Column(name = "VL_DESCONTO")
	private BigDecimal valorDesconto;

	@Column(name = "DS_USUARIO")
	private String usuario;

	@Column(name = "DT_APROVACAO")
	private Date dataAprovacao;

	@Column(name = "NR_EMPRESA", nullable = false)
	private Long codigoEmpresa;

	@OneToMany(mappedBy = "contaPagar", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<ParcelaContaPagar> parcelas = Lists.newArrayList();

	@OneToMany(mappedBy = "contaPagar", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<ContaPagarEmpresaItem> itens = Lists.newArrayList();

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataEmissao() {
		return dataEmissao;
	}

	public void setDataEmissao(Date dataEmissao) {
		this.dataEmissao = dataEmissao;
	}

	public String getNotaFiscal() {
		return notaFiscal;
	}

	public void setNotaFiscal(String notaFiscal) {
		this.notaFiscal = notaFiscal;
	}

	public BigDecimal getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}

	public String getBanco() {
		return banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public String getAgencia() {
		return agencia;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	public String getContaCorrente() {
		return contaCorrente;
	}

	public void setContaCorrente(String contaCorrente) {
		this.contaCorrente = contaCorrente;
	}

	public BigDecimal getValorLiquido() {
		return valorLiquido;
	}

	public void setValorLiquido(BigDecimal valorLiquido) {
		this.valorLiquido = valorLiquido;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Integer getQtdeParcelas() {
		return qtdeParcelas;
	}

	public void setQtdeParcelas(Integer qtdeParcelas) {
		this.qtdeParcelas = qtdeParcelas;
	}

	public BigDecimal getValorDesconto() {
		return valorDesconto;
	}

	public void setValorDesconto(BigDecimal valorDesconto) {
		this.valorDesconto = valorDesconto;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public Date getDataAprovacao() {
		return dataAprovacao;
	}

	public void setDataAprovacao(Date dataAprovacao) {
		this.dataAprovacao = dataAprovacao;
	}

	public Long getCodigoEmpresa() {
		return codigoEmpresa;
	}

	public void setCodigoEmpresa(Long codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public StatusContasPagarType getStatus() {
		return status;
	}

	public void setStatus(StatusContasPagarType status) {
		this.status = status;
	}

	public ContasPagarType getTipo() {
		return tipo;
	}

	public void setTipo(ContasPagarType tipo) {
		this.tipo = tipo;
	}

	public List<ParcelaContaPagar> getParcelas() {
		return parcelas;
	}

	public void setParcelas(List<ParcelaContaPagar> parcelas) {
		this.parcelas = parcelas;
	}

	public List<ContaPagarEmpresaItem> getItens() {
		return itens;
	}

	public void setItens(List<ContaPagarEmpresaItem> itens) {
		this.itens = itens;
	}

	public boolean validarValorTotalParaValorCalculado() {
		return BigDecimal.ZERO.compareTo(getDiferencaValorTotalCalculado()) == 0;
	}

	public BigDecimal getDiferencaValorTotalCalculado() {
		if (this.valorTotal != null) {
			return this.valorTotal.subtract(getValorTotalCalculado());
		}
		return BigDecimal.ZERO;
	}

	public BigDecimal getValorTotalCalculado() {
		BigDecimal valorTotalAux = BigDecimal.ZERO;

		for (ContaPagarEmpresaItem item : itens) {
			valorTotalAux = valorTotalAux.add(item.getValor());
		}
		return valorTotalAux;
	}

	public BigDecimal getValorTotalParcelas() {
		BigDecimal valorTotalAux = BigDecimal.ZERO;
		valorTotalAux = getValorNotNul(valorTotalAux);
		for (ParcelaContaPagar parcela : parcelas) {
			valorTotalAux = valorTotalAux.add(getValorNotNul(parcela.getValor()));
		}
		setValorLiquido(valorTotalAux);
		return valorTotalAux;
	}

	public void validar(boolean existeContaPagarCadastrada) throws ValidationException {
		//runValidations(buildValidatorFactory().validate(new FornecedorValidacaoWrapper(this, existeContaPagarCadastrada)));

	}

}
