package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import com.google.common.collect.Lists;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Audited
@Table(name = "TB_PARTE")
public class Parte implements IEntity, Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 5340012286546713886L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_PARTE", nullable = false)
	private Long id;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@JoinColumn(name = "FK_PROCESSO", referencedColumnName = "ID_PROCESSO", nullable = false)
	@ManyToOne(optional = false)
	private Processo processo;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@JoinColumn(name = "FK_PESSOA", referencedColumnName = "ID_PESSOA", nullable = false)
	@ManyToOne(optional = false)
	private Pessoa pessoa;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@JoinColumn(name = "FK_POSICAO", referencedColumnName = "ID_POSICAO", nullable = false)
	@ManyToOne(optional = false)
	private Posicao posicao;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@JoinColumn(name = "FK_GRUPO", referencedColumnName = "ID_GRUPO")
	@ManyToOne
	private Grupo grupo;

	@Column(name = "NR_EMPRESA", nullable = false)
	private Long codigoEmpresa;

	@Type(type="true_false")
	@Column(name = "FL_CLIENTE_ATO_PROCESSO", nullable = false)
	private Boolean clienteAtoProcesso;

	@NotAudited
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "parte", orphanRemoval = true)
	private List<ParteAdvogado> advogados = Lists.newArrayList();

	@NotAudited
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "parte", orphanRemoval = true)
	private List<ProcessoPessoaJuridicaCentroCusto> centroCustos = Lists.newArrayList();

	public Parte() {
		super();
		carregaUltimoHistoricoCliente();
	}

	public Parte(Processo processo, Pessoa pessoa, Posicao posicao, Grupo grupo, Long codigoEmpresa) {
		this.processo = processo;
		this.pessoa = pessoa;
		this.posicao = posicao;
		this.grupo = grupo;
		this.codigoEmpresa = codigoEmpresa;
		carregaUltimoHistoricoCliente();
	}

	private void carregaUltimoHistoricoCliente() {
		if(pessoa == null) {
			return;
		}
		int ultimoHistorico = pessoa.getHistoricoClienteList().size() - 1;

		if(ultimoHistorico >= 0){
			this.clienteAtoProcesso = pessoa.getHistoricoClienteList().get(ultimoHistorico)	.getCliente();
		}
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder()
				.append(id).append(processo).append(pessoa).toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Parte) {
			final Parte other = (Parte) obj;
			return new EqualsBuilder()
					.append(id, other.id)
					.append(processo, other.processo)
					.append(pessoa, other.pessoa)
					.isEquals();
		} else {
			return false;
		}
	}

	public String getDescricaoAdvogados() {
		StringBuilder sb = new StringBuilder();
		if(grupo != null){
			for (GrupoAdvogado advogado : grupo.getGruposAdvogados()) {
				sb.append(advogado.getAdvogadoPessoa().getPessoa().getDescricao()).append(" / ");
			}
		} else {
			for (ParteAdvogado advogado : advogados) {
				sb.append(advogado.getPessoa().getDescricao()).append(" / ");
			}
		}
		removeUltimoCaractere(sb);
		return sb.toString();
	}

	public boolean isExisteAdvogadoPrincipal(){
		for (ParteAdvogado advogado : advogados) {
			if(advogado.getRatingPrincipal() != null) {
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}

	public List<ParteAdvogado> getAdvogadosPrincipais(){
		List<ParteAdvogado> result = Lists.newArrayList();
		for (ParteAdvogado advogado : advogados) {
			if(advogado.getRatingPrincipal() != null) {
				result.add(advogado);
			}
		}
		return result;
	}

	public String getResponsaveisPrincipais(){
		List<ParteAdvogado> advogados = getAdvogados().stream().filter(p -> p.getRatingPrincipal() != null).collect(Collectors.toList());
		StringBuilder sb = new StringBuilder();
		for (ParteAdvogado parteAdvogado : advogados) {
			sb.append(parteAdvogado.getPessoa().getDescricao()).append(" / ");
		}
		removeUltimoCaractere(sb);
		return sb.toString();
	}

	private void removeUltimoCaractere(StringBuilder sb) {
		if (sb.length() > 0) {
			sb.deleteCharAt(sb.lastIndexOf("/"));
			sb.deleteCharAt(sb.lastIndexOf(" ")).deleteCharAt(sb.lastIndexOf(" "));
		}
	}

	public void addCentroCustos(ProcessoPessoaJuridicaCentroCusto centroCustos){
		if(centroCustos != null) {
			this.centroCustos.add(centroCustos);
		}
	}

	public void addParteAdvogados(ParteAdvogado parteAdvogado){
		if(parteAdvogado != null) {
			this.advogados.add(parteAdvogado);
		}
	}

	public String getClienteSimNaoAtoProcesso(){
		if(clienteAtoProcesso == null) {
			return "";
		}
		return clienteAtoProcesso ? "SIM" : "NÃO";
	}


	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Processo getProcesso() {
		return processo;
	}

	public void setProcesso(Processo processo) {
		this.processo = processo;
	}

	public Boolean getClienteAtoProcesso() {
		return clienteAtoProcesso;
	}

	public void setClienteAtoProcesso(Boolean clienteAtoProcesso) {
		this.clienteAtoProcesso = clienteAtoProcesso;
	}

	public Grupo getGrupo() {
		return grupo;
	}

	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}

	public Long getCodigoEmpresa() {
		return codigoEmpresa;
	}

	public void setCodigoEmpresa(Long codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}

	public List<ParteAdvogado> getAdvogados() {
		return advogados;
	}

	public void setAdvogados(List<ParteAdvogado> advogados) {
		this.advogados = advogados;
	}

	public Posicao getPosicao() {
		return posicao;
	}

	public void setPosicao(Posicao posicao) {
		this.posicao = posicao;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public List<ProcessoPessoaJuridicaCentroCusto> getCentroCustos() {
		return centroCustos;
	}

	public void setCentroCustos(List<ProcessoPessoaJuridicaCentroCusto> centroCustos) {
		this.centroCustos = centroCustos;
	}

	@Override
	public String toString() {
		return "Parte [id=" + id + ",  processo=" + processo + "]";
	}
}
