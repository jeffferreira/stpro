/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author Jefferson
 */
@Entity
@Table(name = "TB_PESSOA_CONFIGURACAO")
public class PessoaConfiguracao implements IEntity, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_PESSOA_CONFIGURACAO", nullable = false)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "PESSOA_FK", referencedColumnName = "ID_PESSOA")
	private Pessoa pessoa;

	@ManyToOne
	@JoinColumn(name = "DIREITO_PERFIL_FK", referencedColumnName = "ID_DIREITO_PERFIL")
	private DireitoPerfil direitoPerfil;

	@Type(type="true_false")
	@Column(name = "FL_RENDERIZA", nullable = false)
	private Boolean renderiza = Boolean.TRUE;

	@Column(name = "DS_OBSERVACAO")
	private String observacao;

	@Basic(optional = false)
	@Column(name = "NR_EMPRESA", nullable = false)
	private Long codigoEmpresa;
	
	public PessoaConfiguracao(){super();}

	public PessoaConfiguracao(Pessoa pessoa, DireitoPerfil direitoPerfil, String observacao, Long codigoEmpresa){
		this.pessoa = pessoa;
		this.direitoPerfil = direitoPerfil;
		this.renderiza = Boolean.TRUE;
		this.observacao = observacao;
		this.codigoEmpresa = codigoEmpresa;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof PessoaConfiguracao)) {
			return false;
		}
		PessoaConfiguracao other = (PessoaConfiguracao) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public DireitoPerfil getDireitoPerfil() {
		return direitoPerfil;
	}

	public void setDireitoPerfil(DireitoPerfil direitoPerfil) {
		this.direitoPerfil = direitoPerfil;
	}

	public Boolean getRenderiza() {
		return renderiza;
	}

	public void setRenderiza(Boolean renderiza) {
		this.renderiza = renderiza;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Long getCodigoEmpresa() {
		return codigoEmpresa;
	}

	public void setCodigoEmpresa(Long codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}
}
