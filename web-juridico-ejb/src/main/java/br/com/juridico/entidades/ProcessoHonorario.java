package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import com.google.common.collect.Lists;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Audited
@Table(name = "TB_PROCESSO_HONORARIO")
public class ProcessoHonorario implements IEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5114211391060476443L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_PROCESSO_HONORARIO", nullable = false)
	private Long id;

	@Type(type="true_false")
	@Column(name = "FL_CONTRATO", nullable = false)
	private Boolean flagContrato = false;

	@Type(type="true_false")
	@Column(name = "FL_VALOR", nullable = false)
	private Boolean flagValor = false;

	@Type(type="true_false")
	@Column(name = "FL_PERCENTUAL", nullable = false)
	private Boolean flagPercentual = false;

	@Column(name = "DS_PERCENTUAL")
	private String descricaoPercentual;

	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne
	@JoinColumn(name = "FK_CONTRATO", referencedColumnName = "ID_CONTRATO_PESSOA", nullable = true)
	private ContratoPessoa contratoPessoa;

	@Column(name = "VL_VALOR")
	private BigDecimal valor;

	@Column(name = "VL_PERCENTUAL")
	private Double percentual;
	
	@Column(name = "NR_EMPRESA", nullable = false)
	private Long codigoEmpresa;

	@NotAudited
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "processoHonorario", orphanRemoval = true)
    private List<ParcelaHonorario> parcelaHonorarios = Lists.newArrayList();


	public ProcessoHonorario(){
		super();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder()
				.append(id).append(contratoPessoa).toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ProcessoHonorario) {
			final ProcessoHonorario other = (ProcessoHonorario) obj;
			return new EqualsBuilder().append(id, other.id)
					.append(contratoPessoa, other.contratoPessoa).isEquals();
		} else {
			return false;
		}

	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getFlagContrato() {
		return flagContrato;
	}

	public void setFlagContrato(Boolean flagContrato) {
		this.flagContrato = flagContrato;
	}

	public Boolean getFlagValor() {
		return flagValor;
	}

	public void setFlagValor(Boolean flagValor) {
		this.flagValor = flagValor;
	}

	public Boolean getFlagPercentual() {
		return flagPercentual;
	}

	public void setFlagPercentual(Boolean flagPercentual) {
		this.flagPercentual = flagPercentual;
	}

	public String getDescricaoPercentual() {
		return descricaoPercentual;
	}

	public void setDescricaoPercentual(String descricaoPercentual) {
		this.descricaoPercentual = descricaoPercentual;
	}

	public ContratoPessoa getContratoPessoa() {
		return contratoPessoa;
	}

	public void setContratoPessoa(ContratoPessoa contratoPessoa) {
		this.contratoPessoa = contratoPessoa;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public Double getPercentual() {
		return percentual;
	}

	public void setPercentual(Double percentual) {
		this.percentual = percentual;
	}

	public Long getCodigoEmpresa() {
		return codigoEmpresa;
	}

	public void setCodigoEmpresa(Long codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}

	public List<ParcelaHonorario> getParcelaHonorarios() {
		return parcelaHonorarios;
	}

	public void setParcelaHonorarios(List<ParcelaHonorario> parcelaHonorarios) {
		this.parcelaHonorarios = parcelaHonorarios;
	}

	public void addParcelaValorHonorario(ParcelaHonorario parcelaHonorario){
		if(parcelaHonorario != null) {
			parcelaHonorarios.add(parcelaHonorario);
		}
	}

	public BigDecimal getValorTotalParcelas(){
        BigDecimal valorTotal = BigDecimal.ZERO;

        for (ParcelaHonorario parcela : parcelaHonorarios) {
            valorTotal = valorTotal.add(parcela.getValorParcela());
        }
        return valorTotal;
    }

	
}
