/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.juridico.entidades;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.com.juridico.dao.IEntity;
import com.google.common.collect.Lists;

/**
 *
 * @author Jefferson
 */
@Entity
@Table(name = "TB_ESTADO_CIVIL", uniqueConstraints = { @UniqueConstraint(columnNames = { "DS_ESTADO_CIVIL" }) })
@NamedQueries({
		@NamedQuery(name = "EstadoCivil.findByid", query = "SELECT t FROM EstadoCivil t WHERE t.id = :id"),
		@NamedQuery(name = "EstadoCivil.findBydescricao", query = "SELECT t FROM EstadoCivil t WHERE t.descricao = :descricao") })
public class EstadoCivil implements IEntity, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID_ESTADO_CIVIL", nullable = false)
	private Long id;

	@NotNull
	@Size(min = 1, max = 100)
	@Column(name = "DS_ESTADO_CIVIL", nullable = false, length = 100)
	private String descricao;

	@OneToMany(mappedBy = "estadoCivil")
	private List<PessoaFisica> pessoasFisicas = Lists.newArrayList();

	public EstadoCivil() {
		super();
	}

	public EstadoCivil(Long id, String descricao) {
		this.id = id;
		this.descricao = descricao;
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<PessoaFisica> getPessoasFisicas() {
		return pessoasFisicas;
	}

	public void setPessoasFisicas(List<PessoaFisica> pessoasFisicas) {
		this.pessoasFisicas = pessoasFisicas;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof EstadoCivil)) {
			return false;
		}
		EstadoCivil other = (EstadoCivil) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "EstadoCivil[ id=" + id + " ]";
	}

}
