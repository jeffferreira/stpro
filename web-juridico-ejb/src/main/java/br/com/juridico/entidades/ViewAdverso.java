package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Jefferson on 09/04/2016.
 */
@Entity
@Table(name = "VW_PROC_ADVERSO")
public class ViewAdverso implements IEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7335896786781313381L;

	@Id
	@Column(name = "ID")
	private Long id;

	@Column(name = "FK_PARTE")
	private Long idParte;

	@Column(name = "ADVERSO")
	private String adverso;

	@Column(name = "POSICAO")
	private String posicao;

	@Column(name = "NUMERO_EMPRESA")
	private Long codigoEmpresa;

	@ManyToOne
	@JoinColumn(name = "FK_PROCESSO", referencedColumnName = "ID")
	private ViewProcesso processo;

	@Override
	public Long getId() {
		return id;
	}

	public Long getIdParte() {
		return idParte;
	}

	public String getAdverso() {
		return adverso;
	}

	public String getPosicao() {
		return posicao;
	}

	public ViewProcesso getProcesso() {
		return processo;
	}

	public Long getCodigoEmpresa() {
		return codigoEmpresa;
	}
}
