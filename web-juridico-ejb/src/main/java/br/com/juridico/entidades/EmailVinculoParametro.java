package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by jeffersonl2009_00 on 25/10/2016.
 */
@Entity
@Table(name = "TB_EMAIL_VINCULO_PARAMETRO")
public class EmailVinculoParametro implements IEntity, Serializable {

    private static final long serialVersionUID = 9011292893833538553L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_EMAIL_VINCULO_PARAMETRO", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "FK_EMAIL", referencedColumnName = "ID_EMAIL", nullable = false)
    private Email email;

    @ManyToOne
    @JoinColumn(name = "FK_PARAMETRO_EMAIL", referencedColumnName = "ID_PARAMETRO_EMAIL")
    private ParametroEmail parametroEmail;

    public EmailVinculoParametro(){
        super();
    }

    public EmailVinculoParametro(Email email, ParametroEmail parametroEmail) {
        this.email = email;
        this.parametroEmail = parametroEmail;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Email getEmail() {
        return email;
    }

    public void setEmail(Email email) {
        this.email = email;
    }

    public ParametroEmail getParametroEmail() {
        return parametroEmail;
    }

    public void setParametroEmail(ParametroEmail parametroEmail) {
        this.parametroEmail = parametroEmail;
    }
}
