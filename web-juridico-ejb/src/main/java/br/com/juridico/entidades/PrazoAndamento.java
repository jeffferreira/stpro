package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import br.com.juridico.util.DateUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Jefferson on 28/08/2016.
 */
@Entity
@Audited
@Table(name = "TB_PRAZO_ANDAMENTO")
public class PrazoAndamento implements IEntity, Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 2300327456438936765L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_PRAZO_ANDAMENTO", nullable = false)
    private Long id;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @JoinColumn(name = "FK_ANDAMENTO", referencedColumnName = "ID_ANDAMENTO", nullable = false)
    @ManyToOne(optional = false)
    private Andamento andamento;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @ManyToOne(optional = false)
    @JoinColumn(name = "FK_SUB_ANDAMENTO", referencedColumnName = "ID_SUB_ANDAMENTO", nullable = false)
    private SubAndamento subAndamento;

    @Type(type="true_false")
    @Column(name="FL_HORARIO_INDEFINIDO")
    private Boolean horarioIndefinido;

    @Column(name = "DT_INICIO_PRAZO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataInicioPrazo;

    @Column(name = "DT_FIM_PRAZO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataFinalPrazo;

    @Column(name = "DT_INICIO_PRAZO_FATAL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataInicioPrazoFatal;

    @Column(name = "DT_FIM_PRAZO_FATAL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataFinalPrazoFatal;

    @Column(name = "DS_HORA_INICIO", length = 5)
    private String horaInicio;

    @Column(name = "DS_HORA_FIM", length = 5)
    private String horaFim;

    @Type(type="true_false")
    @Column(name = "FL_ENVIA_EMAIL_AVISO", nullable = false)
    private Boolean enviaEmailAviso;

    @Column(name = "NR_QTDE_DIAS_EMAIL_AVISO_ANTECEDENCIA")
    private Long quantidadeDiasEmailAvisoAntecedencia;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @ManyToOne
    @JoinColumn(name = "FK_STATUS_AGENDA", referencedColumnName = "ID_STATUS_AGENDA", nullable = true)
    private StatusAgenda statusAgenda;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @ManyToOne
    @JoinColumn(name = "FK_PESSOA_FISICA", referencedColumnName = "ID_PESSOA_FISICA")
    private PessoaFisica pessoaFisica;

    @Column(name = "DS_OBSERVACAO", length = 125)
    private String observacao;

    @Column(name = "DT_EXCLUSAO_LOGICA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataExclusao;

    public PrazoAndamento(){
        super();
        this.horarioIndefinido = true;
        this.enviaEmailAviso = true;
    }

    public PrazoAndamento(SubAndamento subAndamento){
        this.subAndamento = subAndamento;
        this.horarioIndefinido = true;
        this.enviaEmailAviso = true;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Andamento getAndamento() {
        return andamento;
    }

    public void setAndamento(Andamento andamento) {
        this.andamento = andamento;
    }

    public SubAndamento getSubAndamento() {
        return subAndamento;
    }

    public void setSubAndamento(SubAndamento subAndamento) {
        this.subAndamento = subAndamento;
    }

    public Boolean getHorarioIndefinido() {
        return horarioIndefinido;
    }

    public void setHorarioIndefinido(Boolean horarioIndefinido) {
        this.horarioIndefinido = horarioIndefinido;
    }

    public Date getDataInicioPrazo() {
        return dataInicioPrazo;
    }

    public void setDataInicioPrazo(Date dataInicioPrazo) {
        this.dataInicioPrazo = dataInicioPrazo;
    }

    public Date getDataFinalPrazo() {
        return dataFinalPrazo;
    }

    public void setDataFinalPrazo(Date dataFinalPrazo) {
        this.dataFinalPrazo = dataFinalPrazo;
    }

    public Date getDataInicioPrazoFatal() {
        return dataInicioPrazoFatal;
    }

    public void setDataInicioPrazoFatal(Date dataInicioPrazoFatal) {
        this.dataInicioPrazoFatal = dataInicioPrazoFatal;
    }

    public Date getDataFinalPrazoFatal() {
        return dataFinalPrazoFatal;
    }

    public void setDataFinalPrazoFatal(Date dataFinalPrazoFatal) {
        this.dataFinalPrazoFatal = dataFinalPrazoFatal;
    }

    public Boolean getEnviaEmailAviso() {
        return enviaEmailAviso;
    }

    public void setEnviaEmailAviso(Boolean enviaEmailAviso) {
        this.enviaEmailAviso = enviaEmailAviso;
    }

    public Long getQuantidadeDiasEmailAvisoAntecedencia() {
        return quantidadeDiasEmailAvisoAntecedencia;
    }

    public void setQuantidadeDiasEmailAvisoAntecedencia(Long quantidadeDiasEmailAvisoAntecedencia) {
        this.quantidadeDiasEmailAvisoAntecedencia = quantidadeDiasEmailAvisoAntecedencia;
    }

    public StatusAgenda getStatusAgenda() {
        return statusAgenda;
    }

    public void setStatusAgenda(StatusAgenda statusAgenda) {
        this.statusAgenda = statusAgenda;
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    public String getHoraFim() {
        return horaFim;
    }

    public void setHoraFim(String horaFim) {
        this.horaFim = horaFim;
    }

    public PessoaFisica getPessoaFisica() {
        return pessoaFisica;
    }

    public void setPessoaFisica(PessoaFisica pessoaFisica) {
        this.pessoaFisica = pessoaFisica;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Date getDataExclusao() {
        return dataExclusao;
    }

    public void setDataExclusao(Date dataExclusao) {
        this.dataExclusao = dataExclusao;
    }

    public boolean isPrazoAtrasado(){
        Calendar calendar = Calendar.getInstance();
        return dataFinalPrazo != null
                && dataFinalPrazo.before(calendar.getTime())
                && (statusAgenda != null && !"Cumprido".equals(statusAgenda.getDescricao()) && !"Cancelado".equals(statusAgenda.getDescricao()));
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(id)
                .append(andamento)
                .append(subAndamento).toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof PrazoAndamento) {
            final PrazoAndamento other = (PrazoAndamento) obj;
            return new EqualsBuilder().append(id, other.id)
                    .append(andamento, other.andamento)
                    .append(subAndamento, other.subAndamento).isEquals();
        } else {
            return false;
        }
    }

    public String getDataFinalPrazoFatalFormatado() {
        return String.format("Prazo Fatal: %s", dataFinalPrazoFatal != null ? DateUtils.formataDataHoraBrasil(dataFinalPrazoFatal) : "") ;
    }

    public String getDataHoraFinalPrazoFatalFormatado() {
        return String.format("Prazo Fatal: %s %s", dataFinalPrazoFatal != null ? DateUtils.formataDataBrasil(dataFinalPrazoFatal) : "", getHoraFim() != null ? getHoraFim() : "") ;
    }

    public String getDataHoraFinalPrazo() {
        return String.format("%s %s", dataFinalPrazo != null ? DateUtils.formataDataBrasil(dataFinalPrazo) : "", getHoraFim() != null ? getHoraFim() : "");
    }
}
