package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "TB_PAGAMENTO_CONTA_PAGAR")
public class PagamentoContaPagar implements IEntity, Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_PAGAMENTO_CONTA_PAGAR", nullable = false)
    private Long id;

    @Column(name = "DT_EFETIVACAO")
    @Temporal(TemporalType.DATE)
    private Date dataEfetivacao;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "FK_EMPRESA", updatable = false, nullable = false)
    private Empresa empresa;

    @Column(name = "FL_ATIVO")
    private Boolean ativo = true;

    @Column(name = "NR_EMPRESA", nullable = false)
    private Long codigoEmpresa;

    @OneToMany(mappedBy = "pagamentoContaPagar", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ComprovantePagamento> comprovantes = new ArrayList<>();

    @OneToMany(mappedBy = "pagamentoContaPagar", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ParcelaContaPagar> parcelas = new ArrayList<>();

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDataEfetivacao() {
        return dataEfetivacao;
    }

    public void setDataEfetivacao(Date dataEfetivacao) {
        this.dataEfetivacao = dataEfetivacao;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public Long getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(Long codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }

    public List<ComprovantePagamento> getComprovantes() {
        return comprovantes;
    }

    public void setComprovantes(List<ComprovantePagamento> comprovantes) {
        this.comprovantes = comprovantes;
    }

    public List<ParcelaContaPagar> getParcelas() {
        return parcelas;
    }

    public void setParcelas(List<ParcelaContaPagar> parcelas) {
        this.parcelas = parcelas;
    }
}
