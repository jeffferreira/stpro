/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import com.google.common.collect.Lists;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Marcos
 */
@Entity
@Table(name = "TB_PESSOA_JURIDICA_CENTRO_CUSTO")
public class PessoaJuridicaCentroCusto implements IEntity, Serializable {

    private static final long serialVersionUID = 2307583638913019769L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_PESSOA_JURIDICA_CENTRO_CUSTO", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "FK_PESSOA_JURIDICA", referencedColumnName = "ID_PESSOA_JURIDICA", nullable = false)
    private PessoaJuridica pessoaJuridica;

    @JoinColumn(name = "FK_CENTRO_CUSTO", referencedColumnName = "ID_CENTRO_CUSTO", nullable = false)
    @ManyToOne(optional = false)
    private CentroCusto centroCusto;

    @ManyToOne
    @JoinColumn(name = "FK_PESSOA_FISICA", referencedColumnName = "ID_PESSOA_FISICA")
    private PessoaFisica pessoaFisica;

    @Column(name = "NR_EMPRESA", nullable = false)
    private Long codigoEmpresa;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "centroCusto", orphanRemoval = true)
    private List<ProcessoPessoaJuridicaCentroCusto> processosCentroCusto = Lists.newArrayList();

    @Transient
    private transient int rowNumber;

    public PessoaJuridicaCentroCusto() {
        super();
    }

    public PessoaJuridicaCentroCusto(PessoaJuridica pessoaJuridica,
                                     CentroCusto centroCusto, PessoaFisica pessoaFisica,
                                     Long codigoEmpresa) {
        this.pessoaJuridica = pessoaJuridica;
        this.centroCusto = centroCusto;
        this.pessoaFisica = pessoaFisica;
        this.codigoEmpresa = codigoEmpresa;
    }

    public PessoaJuridicaCentroCusto(Long codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }

    @Override
    public String toString() {
        return "PessoaJuridicaCentroCusto[ id=" + id + " ]";
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PessoaJuridica getPessoaJuridica() {
        return pessoaJuridica;
    }

    public void setPessoaJuridica(PessoaJuridica pessoaJuridica) {
        this.pessoaJuridica = pessoaJuridica;
    }

    public CentroCusto getCentroCusto() {
        return centroCusto;
    }

    public void setCentroCusto(CentroCusto centroCusto) {
        this.centroCusto = centroCusto;
    }

    public PessoaFisica getPessoaFisica() {
        return pessoaFisica;
    }

    public void setPessoaFisica(PessoaFisica pessoaFisica) {
        this.pessoaFisica = pessoaFisica;
    }

    public int getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(int rowNumber) {
        this.rowNumber = rowNumber;
    }

    public Long getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(Long codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }

    public List<ProcessoPessoaJuridicaCentroCusto> getProcessosCentroCusto() {
        return processosCentroCusto;
    }

    public void setProcessosCentroCusto(List<ProcessoPessoaJuridicaCentroCusto> processosCentroCusto) {
        this.processosCentroCusto = processosCentroCusto;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(centroCusto).append(pessoaJuridica).toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof PessoaJuridicaCentroCusto) {
            final PessoaJuridicaCentroCusto other = (PessoaJuridicaCentroCusto) obj;
            return new EqualsBuilder().append(centroCusto, other.centroCusto).append(pessoaJuridica, other.pessoaJuridica).isEquals();
        } else {
            return false;
        }
    }

}
