/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import com.google.common.base.Strings;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Jefferson
 */
@Entity
@Table(name = "TB_CONTATO")
@NamedQueries({ @NamedQuery(name = "Contato.findByid", query = "SELECT t FROM Contato t WHERE t.codigoEmpresa=:codigoEmpresa and t.id = :id"),
		@NamedQuery(name = "Contato.findByddd", query = "SELECT t FROM Contato t WHERE t.codigoEmpresa=:codigoEmpresa and t.ddd = :ddd"),
		@NamedQuery(name = "Contato.findBynumero", query = "SELECT t FROM Contato t WHERE t.codigoEmpresa=:codigoEmpresa and t.numero = :numero"),
		@NamedQuery(name = "Contato.findByramal", query = "SELECT t FROM Contato t WHERE t.codigoEmpresa=:codigoEmpresa and t.ramal = :ramal"),
		@NamedQuery(name = "Contato.findBysetor", query = "SELECT t FROM Contato t WHERE t.codigoEmpresa=:codigoEmpresa and t.setor = :setor"),
		@NamedQuery(name = "Contato.findBycontato", query = "SELECT t FROM Contato t WHERE t.codigoEmpresa=:codigoEmpresa and t.descricao = :contato") })
public class Contato implements IEntity, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_CONTATO", nullable = false)
	private Long id;

	@Column(name = "NR_DDD")
	private Integer ddd;

	@Column(name = "NR_NUMERO", length = 9)
	private String numero;

	@Column(name = "NR_RAMAL", length = 5)
	private String ramal;

	@Column(name = "DS_SETOR", length = 50)
	private String setor;

	@Column(name = "DS_CONTATO", length = 50)
	private String descricao;

	@JoinColumn(name = "FK_TIPO_CONTATO", referencedColumnName = "ID_TIPO_CONTATO", nullable = false)
	@ManyToOne(optional = false)
	private TipoContato tipoContato;

	@Column(name = "NR_EMPRESA", nullable = false)
	private Long codigoEmpresa;

	@OneToMany(mappedBy = "contato")
	private List<PessoaContato> contatos;

	public Contato() {
		super();
	}

	public Contato(Long codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}

	public Contato(Integer ddd, String numero){
		this.ddd = ddd;
		this.numero = numero;
	}

	public Contato(TipoContato tipoContato, String descricao, String numero, Integer ddd) {
		this.tipoContato = tipoContato;
		this.descricao = descricao;
		this.numero = numero;
		this.ddd = ddd;
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getDdd() {
		return ddd;
	}

	public void setDdd(Integer ddd) {
		this.ddd = ddd;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getRamal() {
		return ramal;
	}

	public void setRamal(String ramal) {
		this.ramal = ramal;
	}

	public String getSetor() {
		return setor;
	}

	public void setSetor(String setor) {
		this.setor = setor;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public TipoContato getTipoContato() {
		return tipoContato;
	}

	public void setTipoContato(TipoContato tipoContato) {
		this.tipoContato = tipoContato;
	}

	public List<PessoaContato> getContatos() {
		return contatos;
	}

	public void setContatos(List<PessoaContato> contatos) {
		this.contatos = contatos;
	}

	public Long getCodigoEmpresa() {
		return codigoEmpresa;
	}

	public void setCodigoEmpresa(Long codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof Contato)) {
			return false;
		}
		Contato other = (Contato) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	public String getTelefoneFormatado(){
		if(Strings.isNullOrEmpty(numero)) {
			return "";
		}
		if(this.numero.length() == 8) {
			return formatString(this.numero, "####-####");
		} else if(this.numero.length() == 9) {
			return formatString(this.numero, "#####-####");
		}
		return "";
	}

	public String getDDDTelefoneFormatado(){
		if(ddd == null || Strings.isNullOrEmpty(numero)) {
			return "";
		}
		if(this.numero.length() == 8) {
			return formatString(this.ddd + this.numero, "(##) ####-####");
		} else if(this.numero.length() == 9) {
			return formatString(this.ddd + this.numero, "(##) #####-####");
		}
		return "";
	}

	@Override
	public String toString() {
		return "Contato[ id=" + id + " ]";
	}
}
