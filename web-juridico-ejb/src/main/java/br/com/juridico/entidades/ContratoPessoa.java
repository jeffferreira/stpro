package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.cadastros.ContratoValidacaoWrapper;
import com.google.common.collect.Lists;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Created by Jefferson on 21/05/2016.
 */
@Entity
@Audited
@Table(name = "TB_CONTRATO_PESSOA")
@NamedQueries({
        @NamedQuery(name = "ContratoPessoa.findByid", query = "SELECT t FROM ContratoPessoa t WHERE t.codigoEmpresa=:codigoEmpresa and t.id = :id"),
        @NamedQuery(name = "ContratoPessoa.findByDescricao", query = "SELECT t FROM ContratoPessoa t WHERE t.codigoEmpresa=:codigoEmpresa and t.descricao = :descricao") })
public class ContratoPessoa implements IEntity, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8803080300419709343L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_CONTRATO_PESSOA", nullable = false)
    private Long id;

    @Column(name = "DS_CONTRATO_PESSOA", length = 100)
    private String descricao;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @ManyToOne(optional = false)
    @JoinColumn(name = "FK_PESSOA", referencedColumnName = "ID_PESSOA", nullable = false)
    private Pessoa pessoa;

    @Column(name = "DT_INICIO", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataInicio;

    @Column(name = "DT_FIM")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataFim;

    @Type(type="true_false")
    @Column(name = "FL_ATOS", nullable = false)
    private Boolean atos;

    @Type(type="true_false")
    @Column(name = "FL_ACOMP_MENSAL", nullable = false)
    private Boolean acompanhamentoMensal;

    @Column(name = "DT_EXCLUSAO_LOGICA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataExclusao;

    @Column(name = "NR_EMPRESA", nullable = false)
    private Long codigoEmpresa;

    @NotAudited
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "contratoPessoa", orphanRemoval = true)
    private List<ContratoPessoaVigencia> vigencias = Lists.newArrayList();

    @NotAudited
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "contratoPessoa", orphanRemoval = true)
    private List<ContratoDocumento> documentos = Lists.newArrayList();

    @Transient
    private PessoaFisica pessoaFisica;
    @Transient
    private PessoaJuridica pessoaJuridica;

    public ContratoPessoa(){
        this.dataInicio = new Date();
        this.atos = false;
        this.acompanhamentoMensal = false;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(id).toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ContratoPessoa) {
            final ContratoPessoa other = (ContratoPessoa) obj;
            return new EqualsBuilder().append(id, other.id).isEquals();
        } else {
            return false;
        }
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public Date getDataExclusao() {
        return dataExclusao;
    }

    public void setDataExclusao(Date dataExclusao) {
        this.dataExclusao = dataExclusao;
    }

    public Long getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(Long codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }

    public void setAtos(Boolean atos) {
        this.atos = atos;
    }

    public Boolean getAcompanhamentoMensal() {
        return acompanhamentoMensal;
    }

    public void setAcompanhamentoMensal(Boolean acompanhamentoMensal) {
        this.acompanhamentoMensal = acompanhamentoMensal;
    }

    public Boolean getAtos() {
        return atos;
    }

    public void setVigencias(List<ContratoPessoaVigencia> vigencias) {
        this.vigencias = vigencias;
    }

    public void addVigencia(ContratoPessoaVigencia vigencia) {
        this.vigencias.add(vigencia);
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataFim() {
        return dataFim;
    }

    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }

    public List<ContratoDocumento> getDocumentos() {
        return documentos;
    }

    public void setDocumentos(List<ContratoDocumento> documentos) {
        this.documentos = documentos;
    }

    public void addDocumentos(ContratoDocumento documento){
        this.documentos.add(documento);
    }

    public void removeDocumentos(int rowNumber){
        this.documentos.remove(rowNumber);
    }

    public boolean isAcompMensal() {
        return this.acompanhamentoMensal != null && this.acompanhamentoMensal;
    }

    public PessoaFisica getPessoaFisica() {
        return pessoaFisica;
    }

    public void setPessoaFisica(PessoaFisica pessoaFisica) {
        this.pessoaFisica = pessoaFisica;
    }

    public PessoaJuridica getPessoaJuridica() {
        return pessoaJuridica;
    }

    public void setPessoaJuridica(PessoaJuridica pessoaJuridica) {
        this.pessoaJuridica = pessoaJuridica;
    }

    public void validar(ContratoPessoaVigencia vigencia, String tipoPessoa) throws ValidationException {
        runValidations(buildValidatorFactory().validate(new ContratoValidacaoWrapper(this, tipoPessoa, vigencia)));
    }

    public List<ContratoPessoaVigencia> getVigencias() {
        Collections.sort(vigencias, new Comparator<Object>() {
            public int compare(Object o1, Object o2) {
                ContratoPessoaVigencia p1 = (ContratoPessoaVigencia) o1;
                ContratoPessoaVigencia p2 = (ContratoPessoaVigencia) o2;
                return p2.isContratoVigente() ? +1 : (p1.isContratoVigente() ? -1 : 0);
            }
        });
        return vigencias;
    }

}
