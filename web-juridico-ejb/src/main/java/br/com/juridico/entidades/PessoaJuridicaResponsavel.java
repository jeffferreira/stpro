package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author Marcos
 */
@Entity
@Table(name = "TB_PESSOA_JURIDICA_RESPONSAVEL")
public class PessoaJuridicaResponsavel implements IEntity, Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_PESSOA_JURIDICA_RESPONSAVEL", nullable = false)
    private Long id;

    @JoinColumn(name = "FK_PESSOA_JURIDICA", referencedColumnName = "ID_PESSOA_JURIDICA", nullable = false)
    @ManyToOne(optional = false)
    private PessoaJuridica pessoaJuridica;

    @JoinColumn(name = "FK_PESSOA_FISICA", referencedColumnName = "ID_PESSOA_FISICA", nullable = false)
    @ManyToOne(optional = false)
    private PessoaFisica pessoaFisica;

    @Column(name = "NR_EMPRESA", nullable = false)
    private Long codigoEmpresa;

    public PessoaJuridicaResponsavel() {
        super();
    }

    public PessoaJuridicaResponsavel(Long codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }
    
    public PessoaJuridicaResponsavel(PessoaJuridica pessoaJuridica, PessoaFisica pessoaFisica, Long codigoEmpresa) {
		this.pessoaJuridica = pessoaJuridica;
		this.pessoaFisica = pessoaFisica;
		this.codigoEmpresa = codigoEmpresa;
	}

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(id).append(pessoaJuridica).toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof PessoaJuridicaResponsavel) {
            final PessoaJuridicaResponsavel other = (PessoaJuridicaResponsavel) obj;
            return new EqualsBuilder().append(id, other.id).append(pessoaJuridica, other.pessoaJuridica).isEquals();
        } else {
            return false;
        }
    }

	@Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PessoaJuridica getPessoaJuridica() {
        return pessoaJuridica;
    }

    public void setPessoaJuridica(PessoaJuridica pessoaJuridica) {
        this.pessoaJuridica = pessoaJuridica;
    }

    public PessoaFisica getPessoaFisica() {
        return pessoaFisica;
    }

    public void setPessoaFisica(PessoaFisica pessoaFisica) {
        this.pessoaFisica = pessoaFisica;
    }

    public Long getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(Long codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }

}
