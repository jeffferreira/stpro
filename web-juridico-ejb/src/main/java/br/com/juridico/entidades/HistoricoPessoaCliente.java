/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.juridico.entidades;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;

import br.com.juridico.dao.IEntity;
import br.com.juridico.util.DateUtils;

/**
 *
 * @author Marcos
 */
@Entity
@Table(name = "TB_HISTORICO_PESSOA_CLIENTE")
@NamedQueries({ @NamedQuery(name = "HistoricoPessoaCliente.findByid", query = "SELECT t FROM HistoricoPessoaCliente t WHERE t.codigoEmpresa=:codigoEmpresa and t.id = :id") })
public class HistoricoPessoaCliente implements IEntity, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_HISTORICO_PESSOA_CLIENTE", nullable = false)
	private Long id;

	@Type(type = "true_false")
	@Column(name = "FL_CLIENTE", nullable = false)
	private Boolean cliente = false;

	@ManyToOne
	@JoinColumn(name = "FK_PESSOA", referencedColumnName = "ID_PESSOA", nullable = false)
	private Pessoa pessoa;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DT_ALTERACAO")
	private Date dataAlteracao;

	@Type(type = "true_false")
	@Column(name = "FL_ATIVO", nullable = false)
	private Boolean ativo = false;

	@Column(name = "DS_JUSTIFICATIVA", length = 4000)
	private String justificativa;

	@Column(name = "NR_EMPRESA", nullable = false)
	private Long codigoEmpresa;

	@Transient
	private transient int rowNumber;

	public HistoricoPessoaCliente() {
		super();
	}

	public HistoricoPessoaCliente(Long codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}

	public HistoricoPessoaCliente(Boolean cliente, Pessoa pessoa, Date dataAlteracao, String justificativa, Long codigoEmpresa) {
		super();
		this.cliente = cliente;
		this.pessoa = pessoa;
		this.dataAlteracao = dataAlteracao;
		this.ativo = Boolean.TRUE;
		this.justificativa = justificativa;
		this.codigoEmpresa = codigoEmpresa;
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getRowNumber() {
		return rowNumber;
	}

	public void setRowNumber(int rowNumber) {
		this.rowNumber = rowNumber;
	}

	public Boolean getCliente() {
		return cliente;
	}

	public void setCliente(Boolean cliente) {
		this.cliente = cliente;
	}

	public Date getDataAlteracao() {
		return dataAlteracao;
	}

	public void setDataAlteracao(Date dataAlteracao) {
		this.dataAlteracao = dataAlteracao;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public Long getCodigoEmpresa() {
		return codigoEmpresa;
	}

	public void setCodigoEmpresa(Long codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}

	@Override
	public String toString() {
		return "HistoricoPessoaCliente[ id=" + id + " ]";
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public String getJustificativa() {
		return justificativa;
	}

	public void setJustificativa(String justificativa) {
		this.justificativa = justificativa;
	}
	
    public String getClienteSimNao() {
        return getCliente() ? "SIM" : "NÃO";
    }
    
    public String getDataAlteracaoFormatada() {
    	return DateUtils.formataDataHoraBrasil(dataAlteracao);
    }

}
