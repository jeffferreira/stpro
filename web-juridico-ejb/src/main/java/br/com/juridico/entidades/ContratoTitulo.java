package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import com.google.common.collect.Lists;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by jefferson.ferreira on 02/03/2017.
 */
@Entity
@Table(name = "CT_TITULO")
public class ContratoTitulo implements IEntity, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_TITULO", nullable = false)
    private Long id;

    @Column(name = "DS_TITULO", nullable = false)
    private String descricao;

    @Column(name = "NR_EMPRESA", nullable = false)
    private Long codigoEmpresa;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "contratoTitulo", orphanRemoval = true)
    private List<ContratoCampoFormulario> formularios = Lists.newArrayList();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "contratoTitulo", orphanRemoval = true)
    private List<ContratoCampoQuestionario> questionarios = Lists.newArrayList();

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Long getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(Long codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }

    public List<ContratoCampoFormulario> getFormularios() {
        return formularios;
    }

    public void setFormularios(List<ContratoCampoFormulario> formularios) {
        this.formularios = formularios;
    }

    public List<ContratoCampoQuestionario> getQuestionarios() {
        return questionarios;
    }

    public void setQuestionarios(List<ContratoCampoQuestionario> questionarios) {
        this.questionarios = questionarios;
    }
}
