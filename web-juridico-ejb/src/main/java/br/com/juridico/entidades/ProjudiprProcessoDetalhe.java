package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author Jefferson
 */
@Entity
@Table(name = "projudipr_processo_detalhe")
public class ProjudiprProcessoDetalhe implements IEntity, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_PROCESSO_DETALHE", nullable = false)
	private Long id;

	@Column(name = "DS_TIPO", length = 75)
	private String tipo;

	@Column(name = "DS_DESCRICAO")
	private String descricao;

	@ManyToOne(optional = false)
	@JoinColumn(name = "FK_PROJUDI_PROCESSO", referencedColumnName = "ID_PROCESSO", nullable = false)
	private ProjudiprProcesso processo;

	public ProjudiprProcessoDetalhe(){
		super();
	}

	public ProjudiprProcessoDetalhe(String tipo, String descricao, ProjudiprProcesso processo) {
		this.tipo = tipo;
		this.descricao = descricao;
		this.processo = processo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public ProjudiprProcesso getProcesso() {
		return processo;
	}

	public void setProcesso(ProjudiprProcesso processo) {
		this.processo = processo;
	}

}
