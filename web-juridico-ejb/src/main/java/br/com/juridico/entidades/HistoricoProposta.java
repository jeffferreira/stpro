package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.processo.PropostaValidacaoWrapper;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by jefferson.ferreira on 18/01/2017.
 */
@Entity
@Audited
@Table(name = "TB_HISTORICO_PROPOSTA")
public class HistoricoProposta implements IEntity, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_HISTORICO_PROPOSTA", nullable = false)
    private Long id;

    @Column(name = "DT_PROPOSTA", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataProposta;

    @Column(name = "DT_FIM_PRAZO", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataFimPrazo;

    @Column(name = "NR_VALOR_PROPOSTA", nullable = false)
    private BigDecimal valorProposta;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @ManyToOne
    @JoinColumn(name = "FK_PESSOA", referencedColumnName = "ID_PESSOA", nullable = false)
    private Pessoa pessoa;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @ManyToOne
    @JoinColumn(name = "FK_PROCESSO", referencedColumnName = "ID_PROCESSO", nullable = false)
    private Processo processo;

    @Column(name = "DS_OBSERVACAO", length = 4000)
    private String observacao;

    @Column(name = "DS_DEVOLUTIVA", length = 4000)
    private String devolutiva;

    @Column(name = "NR_EMPRESA", nullable = false)
    private Long codigoEmpresa;

    public HistoricoProposta(){}

    public HistoricoProposta(Processo processo, Long codigoEmpresa){
        this.processo = processo;
        this.codigoEmpresa = codigoEmpresa;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(id).append(processo).toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof HistoricoProposta) {
            final HistoricoProposta other = (HistoricoProposta) obj;
            return new EqualsBuilder()
                    .append(id, other.id)
                    .append(processo, other.processo).isEquals();
        } else {
            return false;
        }
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDataProposta() {
        return dataProposta;
    }

    public void setDataProposta(Date dataProposta) {
        this.dataProposta = dataProposta;
    }

    public BigDecimal getValorProposta() {
        return valorProposta;
    }

    public void setValorProposta(BigDecimal valorProposta) {
        this.valorProposta = valorProposta;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public Processo getProcesso() {
        return processo;
    }

    public void setProcesso(Processo processo) {
        this.processo = processo;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Long getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(Long codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }

    public Date getDataFimPrazo() {
        return dataFimPrazo;
    }

    public void setDataFimPrazo(Date dataFimPrazo) {
        this.dataFimPrazo = dataFimPrazo;
    }


    public String getDevolutiva() {
        return devolutiva;
    }

    public void setDevolutiva(String devolutiva) {
        this.devolutiva = devolutiva;
    }

    public void validar() throws ValidationException {
        runValidations(buildValidatorFactory().validate(new PropostaValidacaoWrapper(this)));
    }
}
