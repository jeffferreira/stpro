package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import br.com.juridico.enums.ControladoriaIndicador;
import br.com.juridico.enums.TipoOperacaoIndicador;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Jefferson on 23/08/2016.
 */
@Entity
@Table(name = "TB_HISTORICO_EVENTO")
public class HistoricoEvento implements IEntity, Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -6764738977589478657L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_HISTORICO_EVENTO", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "FK_EVENTO", referencedColumnName = "ID_EVENTO")
    private Evento evento;

    @Column(name = "DS_STATUS", nullable = false)
    private String status;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DH_ALTERACAO", nullable = false)
    private Date dataHoraAlteracao;

    @Column(name = "DS_RESPONSAVEL")
    private String responsavel;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DT_INICIO", nullable = false)
    private Date dataInicio;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DT_FIM", nullable = false)
    private Date dataFim;

    @Type(type="true_false")
    @Column(name = "FL_HOUVE_ACORDO")
    private Boolean houveAcordo;

    @Enumerated(EnumType.STRING)
    @Column(name = "DS_TIPO_OPERACAO")
    private TipoOperacaoIndicador tipoOperacao;

    @Column(name = "DS_QUEM_ALTEROU", nullable = false)
    private String quemAlterou;

    @Column(name = "FL_APROVADO_CONTROLADORIA")
    private String controladoriaStatus;

    @Column(name = "DS_QUEM_APROVOU")
    private String quemAprovou;


    public HistoricoEvento(){
        this.dataHoraAlteracao = Calendar.getInstance().getTime();
    }

    /**
     *
     * @param evento
     * @param quemAlterou
     */
    public HistoricoEvento(Evento evento, String quemAlterou) {
        if(evento != null) {
            this.evento = evento;
            this.responsavel = evento.getPessoa() == null ? null : evento.getPessoa().getDescricao();
            this.dataInicio = evento.getDataInicio();
            this.dataFim = evento.getDataFim();
            this.status = evento.getStatusAgenda().getDescricao();
            ControladoriaIndicador type = ControladoriaIndicador.fromValue(evento.getControladoriaStatus());
            this.controladoriaStatus = type == null ? "" : type.getStatus();
            this.houveAcordo = Boolean.FALSE;
            if (evento.getAndamento() != null) {
                this.houveAcordo = evento.getAndamento().getHouveAcordo();
            }
            this.quemAlterou = quemAlterou;
        }
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Evento getEvento() {
        return evento;
    }

    public void setEvento(Evento evento) {
        this.evento = evento;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getDataHoraAlteracao() {
        return dataHoraAlteracao;
    }

    public void setDataHoraAlteracao(Date dataHoraAlteracao) {
        this.dataHoraAlteracao = dataHoraAlteracao;
    }

    public String getResponsavel() {
        return responsavel;
    }

    public void setResponsavel(String responsavel) {
        this.responsavel = responsavel;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataFim() {
        return dataFim;
    }

    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }

    public String getQuemAlterou() {
        return quemAlterou;
    }

    public void setQuemAlterou(String quemAlterou) {
        this.quemAlterou = quemAlterou;
    }

    public Boolean getHouveAcordo() {
        return houveAcordo;
    }

    public void setHouveAcordo(Boolean houveAcordo) {
        this.houveAcordo = houveAcordo;
    }

    public String getControladoriaStatus() {
        return controladoriaStatus;
    }

    public void setControladoriaStatus(String controladoriaStatus) {
        this.controladoriaStatus = controladoriaStatus;
    }

    public String getQuemAprovou() {
        return quemAprovou;
    }

    public void setQuemAprovou(String quemAprovou) {
        this.quemAprovou = quemAprovou;
    }

    public TipoOperacaoIndicador getTipoOperacao() {
        return tipoOperacao;
    }

    public void setTipoOperacao(TipoOperacaoIndicador tipoOperacao) {
        this.tipoOperacao = tipoOperacao;
    }
}
