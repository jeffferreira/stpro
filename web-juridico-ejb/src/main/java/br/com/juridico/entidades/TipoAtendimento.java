/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.juridico.entidades;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.NotEmpty;

import br.com.juridico.dao.IEntity;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.cadastros.TipoAtendimentoValidacaoWrapper;

/**
 *
 * @author Jefferson
 */
@Entity
@Audited
@Table(name = "TB_TIPO_ATENDIMENTO")
@NamedQueries({
		@NamedQuery(name = "TipoAtendimento.findByid", query = "SELECT t FROM TipoAtendimento t WHERE t.codigoEmpresa=:codigoEmpresa and t.id = :id"),
		@NamedQuery(name = "TipoAtendimento.findByDescricao", query = "SELECT t FROM TipoAtendimento t WHERE t.codigoEmpresa=:codigoEmpresa and t.descricao = :descricao"),
		@NamedQuery(name = "TipoAtendimento.findBydataExclusao", query = "SELECT t FROM TipoAtendimento t WHERE t.codigoEmpresa=:codigoEmpresa and t.dataExclusao = :dataExclusao") })
public class TipoAtendimento implements IEntity, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_TIPO_ATENDIMENTO", nullable = false)
	private Long id;

	@NotNull
	@NotEmpty(message = "Descrição: preenchimento obrigatório")
	@Column(name = "DS_DESCRICAO", length = 255)
	private String descricao;

	@Column(name = "NR_EMPRESA", nullable = false)
	private Long codigoEmpresa;

	@Column(name = "DT_EXCLUSAO_LOGICA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataExclusao;

	public TipoAtendimento() {
		super();
	}

	public TipoAtendimento(Long id) {
		this.id = id;
	}

	public TipoAtendimento(Long id, String descricao) {
		this.id = id;
		this.descricao = descricao;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof TipoAtendimento)) {
			return false;
		}
		TipoAtendimento other = (TipoAtendimento) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Date getDataExclusao() {
		return dataExclusao;
	}

	public void setDataExclusao(Date dataExclusao) {
		this.dataExclusao = dataExclusao;
	}

	@Override
	public String toString() {
		return "TipoAtendimento [id=" + id + ", descricao=" + descricao + "]";
	}

	public Long getCodigoEmpresa() {
		return codigoEmpresa;
	}

	public void setCodigoEmpresa(Long codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}

	public void validar(boolean existeTipoAtendimentoCadastrado) throws ValidationException {
		runValidations(buildValidatorFactory().validate(new TipoAtendimentoValidacaoWrapper(existeTipoAtendimentoCadastrado)));
	}
}
