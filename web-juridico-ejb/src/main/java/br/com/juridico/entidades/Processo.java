package br.com.juridico.entidades;

import br.com.juridico.comparator.AndamentoComparator;
import br.com.juridico.dao.IEntity;
import br.com.juridico.dto.ParteDto;
import br.com.juridico.enums.StatusProcessoIndicador;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.processo.*;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author Jefferson
 */
@Entity
@Audited
@Table(name = "TB_PROCESSO")
@NamedQuery(name = "Processo.findByid", query = "SELECT t FROM Processo t WHERE t.codigoEmpresa=:codigoEmpresa and t.id = :idProcesso")
public class Processo implements IEntity, Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_PROCESSO", nullable = false)
    private Long id;

    @Type(type="true_false")
    @Column(name = "FL_PRIVADO", nullable = false)
    private Boolean privado = false;

    @Column(name = "FL_STATUS", nullable = false, length = 1)
    private String status;

    @Column(name = "DS_PROCESSO", nullable = false, length = 200)
    private String descricao;

    @Column(name = "DS_NUMERACAO_SECUNDARIA", nullable = false, length = 200)
    private String numeracaoSecundaria;

    @Column(name = "DT_ABERTURA", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataAbertura;

    @Column(name = "VL_CAUSA", precision = 20, scale = 2)
    private BigDecimal valorCausa;

    @Column(name = "VL_PEDIDO", precision = 20, scale = 2)
    private BigDecimal valorPedido;

    @Column(name = "VL_PROVAVEL", precision = 20, scale = 2)
    private BigDecimal valorProvavel;

    @Column(name = "DS_OBSERVACAO")
    private String observacao;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @ManyToOne
    @JoinColumn(name = "FK_NR_VARA", referencedColumnName = "ID_NUMERO_VARA")
    private NumeroVara numeroVara;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @ManyToOne(optional = false)
    @JoinColumn(name = "FK_VARA", referencedColumnName = "ID_VARA", nullable = false)
    private Vara vara;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @ManyToOne(optional = false)
    @JoinColumn(name = "FK_MEIO_TRAMITACAO", referencedColumnName = "ID_MEIO_TRAMITACAO", nullable = false)
    private MeioTramitacao meioTramitacao;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @JoinColumn(name = "FK_NATUREZA_ACAO", referencedColumnName = "ID_NATUREZA_ACAO", nullable = false)
    @ManyToOne(optional = false)
    private NaturezaAcao naturezaAcao;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @ManyToOne(optional = false)
    @JoinColumn(name = "FK_FORUM", referencedColumnName = "ID_FORUM", nullable = false)
    private Forum forum;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @ManyToOne(optional = false)
    @JoinColumn(name = "FK_FASE", referencedColumnName = "ID_FASE", nullable = false)
    private Fase fase;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @ManyToOne(optional = false)
    @JoinColumn(name = "FK_COMARCA", referencedColumnName = "ID_COMARCA", nullable = false)
    private Comarca comarca;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @ManyToOne(optional = false)
    @JoinColumn(name = "FK_CLASSE_PROCESSUAL", referencedColumnName = "ID_CLASSE_PROCESSUAL", nullable = false)
    private ClasseProcessual classeProcessual;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @ManyToOne(optional = false)
    @JoinColumn(name = "FK_GRAU_RISCO", referencedColumnName = "ID_GRAU_RISCO", nullable = false)
    private GrauRisco grauRisco;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_PROCESSO_HONORARIO", referencedColumnName = "ID_PROCESSO_HONORARIO")
    private ProcessoHonorario processoHonorario = new ProcessoHonorario();

    @Column(name = "NR_EMPRESA", nullable = false)
    private Long codigoEmpresa;

    @Column(name = "DT_CADASTRO", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataCadastro;

    @Column(name = "DT_CITACAO")
    @Temporal(TemporalType.DATE)
    private Date dataCitacao;

    @Column(name = "DS_USUARIO_CADASTRO", nullable = false, length = 15)
    private String usuarioCadastro;

    @NotAudited
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "processo", orphanRemoval = true)
    private List<Parte> partes = Lists.newArrayList();

    @NotAudited
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "processo")
    private List<Andamento> andamentos = Lists.newArrayList();

    @NotAudited
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "processo", fetch = FetchType.EAGER)
    private List<ProcessoPedido> pedidos = Lists.newArrayList();

    @NotAudited
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "processo", orphanRemoval = true)
    private List<Custa> custas = Lists.newArrayList();

    @NotAudited
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "processo", orphanRemoval = true)
    private List<HistoricoProposta> propostas = Lists.newArrayList();

    public Processo() {
        super();
    }

    public Processo(Long id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(id).toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Processo) {
            final Processo other = (Processo) obj;
            return new EqualsBuilder().append(id, other.id).isEquals();
        } else {
            return false;
        }
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getNumeracaoSecundaria() {
        return numeracaoSecundaria;
    }

    public void setNumeracaoSecundaria(String numeracaoSecundaria) {
        this.numeracaoSecundaria = numeracaoSecundaria;
    }

    public Date getDataAbertura() {
        return dataAbertura;
    }

    public void setDataAbertura(Date dataAbertura) {
        this.dataAbertura = dataAbertura;
    }

    public BigDecimal getValorCausa() {
        return valorCausa;
    }

    public void setValorCausa(BigDecimal valorCausa) {
        this.valorCausa = valorCausa;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Vara getVara() {
        return vara;
    }

    public void setVara(Vara vara) {
        this.vara = vara;
    }

    public MeioTramitacao getMeioTramitacao() {
        return meioTramitacao;
    }

    public void setMeioTramitacao(MeioTramitacao meioTramitacao) {
        this.meioTramitacao = meioTramitacao;
    }

    public NaturezaAcao getNaturezaAcao() {
        return naturezaAcao;
    }

    public void setNaturezaAcao(NaturezaAcao naturezaAcao) {
        this.naturezaAcao = naturezaAcao;
    }

    public Forum getForum() {
        return forum;
    }

    public void setForum(Forum forum) {
        this.forum = forum;
    }

    public Fase getFase() {
        return fase;
    }

    public void setFase(Fase fase) {
        this.fase = fase;
    }

    public Comarca getComarca() {
        return comarca;
    }

    public void setComarca(Comarca comarca) {
        this.comarca = comarca;
    }

    public ClasseProcessual getClasseProcessual() {
        return classeProcessual;
    }

    public void setClasseProcessual(ClasseProcessual classeProcessual) {
        this.classeProcessual = classeProcessual;
    }

    public Boolean getPrivado() {
        return privado;
    }

    public void setPrivado(Boolean permiteAcessoInformacoes) {
        this.privado = permiteAcessoInformacoes != null && !permiteAcessoInformacoes;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public GrauRisco getGrauRisco() {
        return grauRisco;
    }

    public void setGrauRisco(GrauRisco grauRisco) {
        this.grauRisco = grauRisco;
    }

    public ProcessoHonorario getProcessoHonorario() {
        return processoHonorario;
    }

    public void setProcessoHonorario(ProcessoHonorario processoHonorario) {
        this.processoHonorario = processoHonorario;
    }

    public Date getDataCitacao() {
        return dataCitacao;
    }

    public void setDataCitacao(Date dataCitacao) {
        this.dataCitacao = dataCitacao;
    }

    public BigDecimal getValorPedido() {
        return valorPedido;
    }

    public void setValorPedido(BigDecimal valorPedido) {
        this.valorPedido = valorPedido;
    }

    public BigDecimal getValorProvavel() {
        return valorProvavel;
    }

    public void setValorProvavel(BigDecimal valorProvavel) {
        this.valorProvavel = valorProvavel;
    }

    public List<Parte> getPartes() {
        return partes;
    }

    public void setPartes(List<Parte> partes) {
        this.partes = partes;
    }

    public Long getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(Long codigoEmpresa) {
        this.getProcessoHonorario().setCodigoEmpresa(codigoEmpresa);
        this.codigoEmpresa = codigoEmpresa;
    }

    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public String getUsuarioCadastro() {
        return usuarioCadastro;
    }

    public void setUsuarioCadastro(String usuarioCadastro) {
        this.usuarioCadastro = usuarioCadastro;
    }

    public List<Andamento> getAndamentos() {
        if(!andamentos.isEmpty()) {
            Collections.sort(this.andamentos, new AndamentoComparator());
            return andamentos.stream().filter(a -> a.getDataExclusao() == null).collect(Collectors.toList());
        }
        return andamentos;
    }

    public void addAndamento(Andamento andamento){
        if(andamento == null) {
            return;
        }
        this.andamentos.add(andamento);
    }

    public void setAndamentos(List<Andamento> andamentos) {
        this.andamentos = andamentos;
    }

    public List<ProcessoPedido> getPedidos() {
        return pedidos;
    }

    public void setPedidos(List<ProcessoPedido> pedidos) {
        this.pedidos = pedidos;
    }

    public NumeroVara getNumeroVara() {
        return numeroVara;
    }

    public void setNumeroVara(NumeroVara numeroVara) {
        this.numeroVara = numeroVara;
    }

    public List<HistoricoProposta> getPropostas() {
        return propostas;
    }

    public void setPropostas(List<HistoricoProposta> propostas) {
        this.propostas = propostas;
    }

    public void addParte(Parte parte){
        if(parte != null)
            this.partes.add(parte);
    }

    public void addProposta(HistoricoProposta proposta){
        if(proposta != null) {
            this.propostas.add(proposta);
        }
    }

    public void addPedido(ProcessoPedido processoPedido){
        if(processoPedido != null) {
            processoPedido.setProcesso(this);
            this.pedidos.add(processoPedido);
        }
    }

    public List<Custa> getCustas() {
        return custas;
    }

    public void setCustas(List<Custa> custas) {
        this.custas = custas;
    }

    public String getCliente() {
        StringBuilder sb = new StringBuilder();
        partes.stream().filter(p -> p.getPessoa().isClienteEmpresa()).forEach(pp ->
                sb.append(pp.getPessoa().getDescricao()).append(" / ")
        );
        removeUltimoCaractere(sb);

        return sb.toString();
    }

    private void removeUltimoCaractere(StringBuilder sb) {
        if (sb.length() > 0) {
            sb.deleteCharAt(sb.lastIndexOf(" ")).deleteCharAt(sb.lastIndexOf("/")).deleteCharAt(sb.lastIndexOf(" "));
        }
    }

    /**
     *
     * @return
     */
    public BigDecimal getTotalValorProvavel(){
        BigDecimal total = BigDecimal.ZERO;
        for (ProcessoPedido pedido : pedidos) {
            total = total.add(pedido.getValorProvavel());
        }
        return total;
    }

    /**
     *
     * @return
     */
    public BigDecimal getTotalValorPedido(){
        BigDecimal total = BigDecimal.ZERO;
        for (ProcessoPedido pedido : pedidos) {
            total = total.add(pedido.getValorPedido());
        }
        return total;
    }

    public Pessoa getClienteProcesso(){
        for (Parte p : this.partes) {
            if(p.getPessoa().isClienteEmpresa()) {
                return p.getPessoa();
            }
        }
        return null;
    }

    public boolean isHonorarioContrato(){
        if(processoHonorario != null) {
            return processoHonorario.getFlagContrato() != null ? processoHonorario.getFlagContrato() : false;
        }
        return Boolean.FALSE;
    }

    public boolean isHonorarioValor(){
        if(processoHonorario != null) {
            return processoHonorario.getFlagValor() != null ? processoHonorario.getFlagValor() : false;
        }
        return Boolean.FALSE;
    }

    public boolean isHonorarioPercentual(){
        if(processoHonorario != null) {
            return processoHonorario.getFlagPercentual() != null ? processoHonorario.getFlagPercentual() : false;
        }
        return Boolean.FALSE;
    }

    public boolean isProvavelNecessidadeImpugnacao(){
        boolean provavelNecessidadeImpugnacao = Boolean.FALSE;
        if (valorCausa == null || valorCausa.compareTo(BigDecimal.ZERO) == 0) {
            this.valorCausa = BigDecimal.ZERO;
            return Boolean.FALSE;
        }

        for (Parte parte : this.partes) {
            if (parte.getClienteAtoProcesso() && parte.getPosicao().isReuOuTerceiro()) {
                provavelNecessidadeImpugnacao = Boolean.TRUE;
                break;
            }
        }

        if (!provavelNecessidadeImpugnacao){
            return provavelNecessidadeImpugnacao;
        }

        return !valorCausa.equals(getTotalValorPedido());
    }

    public boolean isExistePartesConcorrentes(){
        int parteCliente = 0;
        for (Parte parte : this.partes) {
            if(parte.getPessoa().isClienteEmpresa()){
                parteCliente++;
            }
        }
        return parteCliente > 1;
    }

    public boolean isProcessoFinalizado(){
        return "F".equals(status);
    }

    /**
     *
     * @param parte
     * @throws ValidationException
     */
    public void validarParte(ParteDto parte) throws ValidationException {
        runValidations(buildValidatorFactory().validate(new ParteValidationWrapper(parte)));
    }

    /**
     *
     * @throws ValidationException
     */
    public void validarDuasPartes() throws ValidationException {
        runValidations(buildValidatorFactory().validate(new DuasPartesValidacaoWrapper(this)));
    }

    /**
     *
     * @throws ValidationException
     */
    public void validarDadosProcesso() throws ValidationException {
        runValidations(buildValidatorFactory().validate(new DadosProcessoValidacaoWrapper(this)));
    }

    /**
     *
     * @throws ValidationException
     */
    public void validarResumoAcao() throws ValidationException {
        runValidations(buildValidatorFactory().validate(new ResumoValidacaoWrapper(this)));
    }

    /**
     *
     * @throws ValidationException
     */
    public void validarPartesConcorrentes(User user) throws ValidationException {
        runValidations(buildValidatorFactory().validate(new PartesConcorrentesWrapper(this, user)));
    }

    /**
     *
     * @param existeProcessoCadastrado
     * @throws ValidationException
     */
    public void validarProcessoExistente(boolean existeProcessoCadastrado)throws ValidationException {
        runValidations(buildValidatorFactory().validate(new ProcessoExistenteWrapper(existeProcessoCadastrado)));
    }

    /**
     *
     * @throws ValidationException
     */
    public void validarValoresProcesso() throws ValidationException {
        runValidations(buildValidatorFactory().validate(new ValoresValidacaoWrapper(this)));
    }

    public void validarHonorarios() throws ValidationException {
        runValidations(buildValidatorFactory().validate(new HonorariosValidacaoWrapper(this)));
    }

    public String getDescricaoStatus(){
        return StatusProcessoIndicador.fromString(this.status).getDescricao();
    }

    @Override
    public String toString() {
        String templateFormat = "%1$s | %2$s | %3$s | %4$s | %5$s | %6$s";
        String descricaoNaturezaAcao = naturezaAcao == null ? "" : naturezaAcao.getDescricao();
        String descricaoMeioTramitacao = meioTramitacao == null ? "" : meioTramitacao.getDescricao();
        String descricaoVara = vara == null ? "" : vara.getDescricao();
        String descricaoComarca = comarca == null ? "" : comarca.toString();
        String descricaoClasseProcessual = classeProcessual == null ? "" : classeProcessual.toString();
        return String.format(templateFormat, descricao, descricaoNaturezaAcao, descricaoMeioTramitacao, descricaoVara, descricaoComarca, descricaoClasseProcessual);
    }

    public String getDescricaoCompleta(){
        return toString();
    }

    public String getResumoAcaoSemHtml(){
        if(Strings.isNullOrEmpty(observacao)) {
            return "";
        }
        return observacao.toString().replaceAll("<[^>]*>", "").replaceAll(System.getProperty("line.separator"), "");
    }

    public boolean isPermiteCadastroComGeracaoAutomatica(){
        if(this.naturezaAcao == null || this.meioTramitacao == null || !meioTramitacao.getGeradorAutomatico()) {
            return Boolean.FALSE;
        }
        MeioTramitacao meioTramitacao = this.getMeioTramitacao();
        return meioTramitacao.getMeioTramitacaoNaturezas().stream().filter(m -> m.getNaturezaAcao().equals(this.getNaturezaAcao())).findFirst().isPresent();
    }
}
