package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Comparator;
import java.util.List;

/**
 * Created by jefferson.ferreira on 02/03/2017.
 */
@Entity
@Table(name = "CT_SUB_CONTRATO")
public class ContratoSubContrato implements Comparator<ContratoSubContrato>, IEntity, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_SUB_CONTRATO", nullable = false)
    private Long id;

    @Column(name = "DS_POSICAO", nullable = false)
    private String posicao;

    @Column(name = "DS_SUB_CONTRATO", nullable = false)
    private String descricao;

    @ManyToOne
    @JoinColumn(name = "FK_CONTRATO", referencedColumnName = "ID_CONTRATO", nullable = false)
    private Contrato contrato;

    @Column(name = "FL_QUESTIONARIO_FORMULARIO", length = 1)
    private String opcaoQuestionarioFormulario;

    @ManyToOne
    @JoinColumn(name = "FK_TITULO", referencedColumnName = "ID_TITULO")
    private ContratoTitulo contratoTitulo;

    @ManyToOne
    @JoinColumn(name = "FK_SUB_CONTRATO", referencedColumnName = "ID_SUB_CONTRATO")
    private ContratoSubContrato contratoSubContrato;

    @OneToMany(mappedBy = "contratoSubContrato")
    private List<ContratoSubContrato> filhos = Lists.newArrayList();

    @Transient
    private Boolean possuiFilhos;

    public boolean isNoAbaixoRaiz(){
        return Strings.isNullOrEmpty(opcaoQuestionarioFormulario) && contratoTitulo == null && contratoSubContrato == null;
    }

    public Boolean getPossuiFilhos() {
        return possuiFilhos;
    }

    public void setPossuiFilhos(Boolean possuiFilhos) {
        this.possuiFilhos = possuiFilhos;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPosicao() {
        return posicao;
    }

    public void setPosicao(String posicao) {
        this.posicao = posicao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getOpcaoQuestionarioFormulario() {
        return opcaoQuestionarioFormulario;
    }

    public void setOpcaoQuestionarioFormulario(String opcaoQuestionarioFormulario) {
        this.opcaoQuestionarioFormulario = opcaoQuestionarioFormulario;
    }

    public ContratoTitulo getContratoTitulo() {
        return contratoTitulo;
    }

    public void setContratoTitulo(ContratoTitulo contratoTitulo) {
        this.contratoTitulo = contratoTitulo;
    }

    public Contrato getContrato() {
        return contrato;
    }

    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }

    public ContratoSubContrato getContratoSubContrato() {
        return contratoSubContrato;
    }

    public void setContratoSubContrato(ContratoSubContrato contratoSubContrato) {
        this.contratoSubContrato = contratoSubContrato;
    }

    public List<ContratoSubContrato> getFilhos() {
        return filhos;
    }

    public void setFilhos(List<ContratoSubContrato> filhos) {
        this.filhos = filhos;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(id).append(descricao).toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ContratoSubContrato) {
            final ContratoSubContrato other = (ContratoSubContrato) obj;
            return new EqualsBuilder().append(id, other.id).append(descricao, other.descricao).isEquals();
        } else {
            return false;
        }
    }

    public int compare(ContratoSubContrato contratoSubContrato, ContratoSubContrato outrocontratoSubContrato) {
        return contratoSubContrato.getPosicao().compareTo(outrocontratoSubContrato.getPosicao());
    }

    @Override
    public String toString() {
        String templateFormat = "%1$s. %2$s";
        return String.format(templateFormat, posicao, descricao);
    }
}
