package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.cadastros.PedidoValidacaoWrapper;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Marcos
 */
@Entity
@Audited
@Table(name = "TB_PEDIDO")
@NamedQueries({
		@NamedQuery(name = "Pedido.findByid", query = "SELECT t FROM Pedido t WHERE t.codigoEmpresa=:codigoEmpresa and t.id = :id"),
		@NamedQuery(name = "Pedido.findByDescricao", query = "SELECT t FROM Pedido t WHERE t.codigoEmpresa=:codigoEmpresa and t.descricao = :descricao") })
public class Pedido implements IEntity, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_PEDIDO", nullable = false)
	private Long id;

	@Column(name = "DS_PEDIDO", length = 200)
	private String descricao;

	@Column(name = "NR_EMPRESA", nullable = false)
	private Long codigoEmpresa;

	@Column(name = "DT_EXCLUSAO_LOGICA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataExclusao;

	public Pedido() {
		super();
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Date getDataExclusao() {
		return dataExclusao;
	}

	public void setDataExclusao(Date dataExclusao) {
		this.dataExclusao = dataExclusao;
	}

	public Long getCodigoEmpresa() {
		return codigoEmpresa;
	}

	public void setCodigoEmpresa(Long codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}

	public void validar(boolean existePedidoCadastrado) throws ValidationException {
		runValidations(buildValidatorFactory().validate(new PedidoValidacaoWrapper(this, existePedidoCadastrado)));
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Pedido pedido = (Pedido) o;

		if (id != null ? !id.equals(pedido.id) : pedido.id != null) return false;

		return true;
	}

	@Override
	public int hashCode() {
		return id != null ? id.hashCode() : 0;
	}

	@Override
	public String toString() {
		return "Pedido[ id=" + id + " ]";
	}

}
