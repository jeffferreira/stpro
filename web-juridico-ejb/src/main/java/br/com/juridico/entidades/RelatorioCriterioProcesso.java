package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import br.com.juridico.enums.RelatorioCriterioType;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by jefferson.ferreira on 31/08/2017.
 */
@Entity
@Table(name = "tb_relatorio_criterio_processo")
public class RelatorioCriterioProcesso implements IEntity, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_RELATORIO_CRITERIO_PROCESSO", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "FK_RELATORIO_FILTRO_PROCESSO", referencedColumnName = "ID_RELATORIO_FILTRO_PROCESSO")
    private RelatorioFiltroProcesso relatorioFiltroProcesso;

    @Column(name = "DS_NOME_CRITERIO", nullable = false)
    private String descricao;

    @Column(name = "DS_CAMPO", nullable = false)
    private String campo;

    @Enumerated(EnumType.STRING)
    @Column(length = 3, name = "DS_CRITERIO", columnDefinition = "char")
    private RelatorioCriterioType criterioType = RelatorioCriterioType.E;

    @Type(type="true_false")
    @Column(name = "FL_FILTRO_ATIVO")
    private Boolean filtroAtivo;

    @Type(type="true_false")
    @Column(name = "FL_FILTRO_OBRIGATORIO")
    private Boolean filtroObrigatorio;

    @Type(type="true_false")
    @Column(name = "FL_MOSTRAR_RELATORIO")
    private Boolean mostrarNoRelatorio;

    @Column(name = "TIPO_CAMPO")
    private String tipoCampo;

    @Column(name = "DS_NOME_CAMPO_VIEW")
    private String nomeCampoView;

    @Transient
    private String valorString;
    @Transient
    private Date valorDate;
    @Transient
    private Date valorPeriodoUm;
    @Transient
    private Date valorPeriodoDois;
    @Transient
    private BigDecimal valorPeriodoNumberUm;
    @Transient
    private BigDecimal valorPeriodoNumberDois;


    public RelatorioCriterioProcesso() {
        super();
    }

    public RelatorioCriterioProcesso(RelatorioFiltroProcesso relatorioFiltroProcesso,
                                     String descricao, String campo, String tipoCampo, String nomeCampoView) {
        this.relatorioFiltroProcesso = relatorioFiltroProcesso;
        this.descricao = descricao;
        this.campo = campo;
        this.tipoCampo = tipoCampo;
        this.nomeCampoView = nomeCampoView;
        this.filtroObrigatorio = Boolean.FALSE;
        this.criterioType = RelatorioCriterioType.E;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public RelatorioFiltroProcesso getRelatorioFiltroProcesso() {
        return relatorioFiltroProcesso;
    }

    public void setRelatorioFiltroProcesso(RelatorioFiltroProcesso relatorioFiltroProcesso) {
        this.relatorioFiltroProcesso = relatorioFiltroProcesso;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getCampo() {
        return campo;
    }

    public void setCampo(String campo) {
        this.campo = campo;
    }

    public RelatorioCriterioType getCriterioType() {
        return criterioType;
    }

    public void setCriterioType(RelatorioCriterioType criterioType) {
        this.criterioType = criterioType;
    }

    public Boolean getFiltroAtivo() {
        return filtroAtivo;
    }

    public void setFiltroAtivo(Boolean filtroAtivo) {
        this.filtroAtivo = filtroAtivo;
    }

    public Boolean getFiltroObrigatorio() {
        return filtroObrigatorio;
    }

    public void setFiltroObrigatorio(Boolean filtroObrigatorio) {
        this.filtroObrigatorio = filtroObrigatorio;
    }

    public Boolean getMostrarNoRelatorio() {
        return mostrarNoRelatorio;
    }

    public void setMostrarNoRelatorio(Boolean mostrarNoRelatorio) {
        this.mostrarNoRelatorio = mostrarNoRelatorio;
    }

    public String getTipoCampo() {
        return tipoCampo;
    }

    public void setTipoCampo(String tipoCampo) {
        this.tipoCampo = tipoCampo;
    }

    public String getValorString() {
        return valorString;
    }

    public void setValorString(String valorString) {
        this.valorString = valorString;
    }

    public Date getValorDate() {
        return valorDate;
    }

    public void setValorDate(Date valorDate) {
        this.valorDate = valorDate;
    }

    public BigDecimal getValorPeriodoNumberUm() {
        return valorPeriodoNumberUm;
    }

    public void setValorPeriodoNumberUm(BigDecimal valorPeriodoNumberUm) {
        this.valorPeriodoNumberUm = valorPeriodoNumberUm;
    }

    public BigDecimal getValorPeriodoNumberDois() {
        return valorPeriodoNumberDois;
    }

    public void setValorPeriodoNumberDois(BigDecimal valorPeriodoNumberDois) {
        this.valorPeriodoNumberDois = valorPeriodoNumberDois;
    }

    public Date getValorPeriodoUm() {
        return valorPeriodoUm;
    }

    public void setValorPeriodoUm(Date valorPeriodoUm) {
        this.valorPeriodoUm = valorPeriodoUm;
    }

    public Date getValorPeriodoDois() {
        return valorPeriodoDois;
    }

    public void setValorPeriodoDois(Date valorPeriodoDois) {
        this.valorPeriodoDois = valorPeriodoDois;
    }

    public String getNomeCampoView() {
        return nomeCampoView;
    }

    public void setNomeCampoView(String nomeCampoView) {
        this.nomeCampoView = nomeCampoView;
    }
}
