package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import com.google.common.base.Strings;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author Marcos
 */
@Entity
@Table(name = "projudipr_processo_documento")
public class ProjudiprProcessoDocumento implements IEntity, Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_PROCESSO_DOCUMENTO", nullable = false)
    private Long id;

    @Column(name = "DS_TIPO", length = 75, nullable = false)
    private String tipo;

    @Column(name = "DS_ASSINANTE", length = 225)
    private String assinante;

    @Column(name = "DS_ARQUIVO", length = 2000)
    private String arquivo;

    @Column(name = "DS_NOME_ARQUIVO", length = 225)
    private String nomeArquivo;

    @Column(name = "DS_NIVEL_SIGILO", length = 225)
    private String nivelSigilo;

    @ManyToOne(optional = false)
    @JoinColumn(name = "FK_PROJUDI_MOVIMENTACAO", referencedColumnName = "ID_MOVIMENTACAO", nullable = false)
    private ProjudiprMovimentacao movimentacao;

    public ProjudiprProcessoDocumento(){
        super();
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getAssinante() {
        return assinante;
    }

    public void setAssinante(String assinante) {
        this.assinante = assinante;
    }

    public String getArquivo() {
        return arquivo;
    }

    public void setArquivo(String arquivo) {
        this.arquivo = arquivo;
    }

    public String getNomeArquivo() {
        return nomeArquivo;
    }

    public void setNomeArquivo(String nomeArquivo) {
        this.nomeArquivo = nomeArquivo;
    }

    public String getNivelSigilo() {
        return nivelSigilo;
    }

    public void setNivelSigilo(String nivelSigilo) {
        this.nivelSigilo = nivelSigilo;
    }

    public ProjudiprMovimentacao getMovimentacao() {
        return movimentacao;
    }

    public void setMovimentacao(ProjudiprMovimentacao movimentacao) {
        this.movimentacao = movimentacao;
    }

    public String getNome(){
        if(Strings.isNullOrEmpty(nomeArquivo)){
            return "";
        }
        String formatado = nomeArquivo.replaceAll("/", "#");
        String parts[] = formatado.replaceAll("##", "#").split("#");
        return parts[parts.length-1].trim();
    }
}
