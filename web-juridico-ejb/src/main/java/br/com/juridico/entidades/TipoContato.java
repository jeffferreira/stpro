/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.juridico.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.validator.constraints.NotEmpty;

import br.com.juridico.dao.IEntity;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.cadastros.TipoContatoValidacaoWrapper;

/**
 *
 * @author Jefferson
 */
@Entity
@Audited
@Table(name = "TB_TIPO_CONTATO")
@NamedQueries({
		@NamedQuery(name = "TipoContato.findByid", query = "SELECT t FROM TipoContato t WHERE t.codigoEmpresa=:codigoEmpresa and t.id = :id"),
		@NamedQuery(name = "TipoContato.findByDescricao", query = "SELECT t FROM TipoContato t WHERE t.codigoEmpresa=:codigoEmpresa and t.descricao = :descricao") })
public class TipoContato implements IEntity, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_TIPO_CONTATO", nullable = false)
	private Long id;

	@NotNull
    @NotEmpty(message = "Descrição: preenchimento obrigatório")
	@Column(name = "DS_TIPO_CONTATO", length = 200)
	private String descricao;

	@Column(name = "NR_EMPRESA", nullable = false)
	private Long codigoEmpresa;

	@Column(name = "DT_EXCLUSAO_LOGICA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataExclusao;

	@NotAudited
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "tipoContato")
	private List<Contato> contatos;

	public TipoContato() {
		super();
	}

	public TipoContato(Long id) {
		this.id = id;
	}

	public TipoContato(Long id, String descricao) {
		this.id = id;
		this.descricao = descricao;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof TipoContato)) {
			return false;
		}
		TipoContato other = (TipoContato) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Date getDataExclusao() {
		return dataExclusao;
	}

	public void setDataExclusao(Date dataExclusao) {
		this.dataExclusao = dataExclusao;
	}

	public List<Contato> getContatos() {
		return contatos;
	}

	public void setContatos(List<Contato> contatos) {
		this.contatos = contatos;
	}

	@Override
	public String toString() {
		return "TipoContato [id=" + id + ", descricao=" + descricao + "]";
	}

	public Long getCodigoEmpresa() {
		return codigoEmpresa;
	}

	public void setCodigoEmpresa(Long codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}
	
	public void validar(boolean existeTipoContatoCadastrado) throws ValidationException {
		runValidations(buildValidatorFactory().validate(new TipoContatoValidacaoWrapper(existeTipoContatoCadastrado)));
	}

}
