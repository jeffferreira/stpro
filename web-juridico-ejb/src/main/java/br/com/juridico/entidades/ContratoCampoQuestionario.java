package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by jefferson.ferreira on 02/03/2017.
 */
@Entity
@Table(name = "CT_CAMPO_QUESTIONARIO")
public class ContratoCampoQuestionario implements IEntity, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_CAMPO_QUESTIONARIO", nullable = false)
    private Long id;

    @Column(name = "DS_LABEL", nullable = false)
    private String label;

    @Column(name = "DS_TIPO_CAMPO", nullable = false)
    private String tipoCampo;

    @Column(name = "FL_ACAO_RESPOSTA", nullable = false, length = 1)
    private String acaoResposta;

    @ManyToOne(optional = false)
    @JoinColumn(name = "FK_TITULO", referencedColumnName = "ID_TITULO", nullable = false)
    private ContratoTitulo contratoTitulo;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getTipoCampo() {
        return tipoCampo;
    }

    public void setTipoCampo(String tipoCampo) {
        this.tipoCampo = tipoCampo;
    }

    public String getAcaoResposta() {
        return acaoResposta;
    }

    public void setAcaoResposta(String acaoResposta) {
        this.acaoResposta = acaoResposta;
    }

    public ContratoTitulo getContratoTitulo() {
        return contratoTitulo;
    }

    public void setContratoTitulo(ContratoTitulo contratoTitulo) {
        this.contratoTitulo = contratoTitulo;
    }
}
