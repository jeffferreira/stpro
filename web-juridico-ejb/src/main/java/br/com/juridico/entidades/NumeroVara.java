package br.com.juridico.entidades;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.juridico.dao.IEntity;

@Entity
@Table(name = "TB_NUMERO_VARA")
public class NumeroVara implements IEntity, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2971849339207536860L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_NUMERO_VARA", nullable = false)
    private Long id;

    @Column(name = "DS_NUMERO_VARA", nullable = false)
    private String descricao;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

}
