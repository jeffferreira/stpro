package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.relatorios.RelatorioFiltroProcessoValidacaoWrapper;
import com.google.common.collect.Lists;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by jefferson.ferreira on 31/08/2017.
 */
@Entity
@Table(name = "tb_relatorio_filtro_processo")
public class RelatorioFiltroProcesso implements IEntity, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_RELATORIO_FILTRO_PROCESSO", nullable = false)
    private Long id;

    @Column(name = "DS_RELATORIO_FILTRO", nullable = false)
    private String descricao;

    @Column(name = "NR_EMPRESA", nullable = false)
    private Long codigoEmpresa;

    @Column(name = "DT_EXCLUSAO_LOGICA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataExclusao;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "relatorioFiltroProcesso", orphanRemoval = true)
    private List<RelatorioCriterioProcesso> criterios = Lists.newArrayList();

    public RelatorioFiltroProcesso(){
        super();
    }

    public RelatorioFiltroProcesso(String descricao, Long codigoEmpresa) {
        this.descricao = descricao;
        this.codigoEmpresa = codigoEmpresa;
    }

    /**
     *
     * @throws ValidationException
     */
    public void validar() throws ValidationException {
        runValidations(buildValidatorFactory().validate(new RelatorioFiltroProcessoValidacaoWrapper(this)));
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Long getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(Long codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }

    public Date getDataExclusao() {
        return dataExclusao;
    }

    public void setDataExclusao(Date dataExclusao) {
        this.dataExclusao = dataExclusao;
    }

    public List<RelatorioCriterioProcesso> getCriterios() {
        return criterios;
    }

    public void setCriterios(List<RelatorioCriterioProcesso> criterios) {
        this.criterios = criterios;
    }
}
