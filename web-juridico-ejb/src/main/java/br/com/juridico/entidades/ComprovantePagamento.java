package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import br.com.juridico.enums.TipoComprovanteType;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "TB_COMPROVANTE_PAGAMENTO")
public class ComprovantePagamento implements IEntity, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_PAGAMENTO_CONTA_PAGAR", nullable = false)
	private Long id;

	@Enumerated(EnumType.STRING)
	@Column(name = "TIPO_COMPROVANTE", nullable = false)
	private TipoComprovanteType tipoComprovante;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_PAGAMENTO_CONTA_PAGAR", updatable = false, nullable = false)
	private PagamentoContaPagar pagamentoContaPagar;

//	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
//	@JoinColumn(name = "arquivo_fk", updatable = true, nullable = false)
//	private Arquivo arquivo;

    @Column(name = "NR_EMPRESA", nullable = false)
    private Long codigoEmpresa;

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TipoComprovanteType getTipoComprovante() {
		return tipoComprovante;
	}

	public void setTipoComprovante(TipoComprovanteType tipoComprovante) {
		this.tipoComprovante = tipoComprovante;
	}

	public PagamentoContaPagar getPagamentoContaPagar() {
		return pagamentoContaPagar;
	}

	public void setPagamentoContaPagar(PagamentoContaPagar pagamentoContaPagar) {
		this.pagamentoContaPagar = pagamentoContaPagar;
	}

    public Long getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(Long codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }
}
