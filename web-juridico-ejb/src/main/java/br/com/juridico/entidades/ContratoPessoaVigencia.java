package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import com.google.common.collect.Lists;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by Jefferson on 21/05/2016.
 */
@Entity
@Audited
@AuditTable(value="TB_CONTRATO_PESSOA_VIGENCIA_AUD")
@Table(name = "TB_CONTRATO_PESSOA_VIGENCIA")
public class ContratoPessoaVigencia implements IEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5284217107267424427L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_CONTRATO_PESSOA_VIGENCIA", nullable = false)
	private Long id;

	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@JoinColumn(name = "FK_CONTRATO_PESSOA", referencedColumnName = "ID_CONTRATO_PESSOA", nullable = false)
	@ManyToOne(optional = false)
	private ContratoPessoa contratoPessoa;

	@Column(name = "VL_MENSAL", nullable = false)
    private BigDecimal valorMensal;

    @Column(name = "VL_ACOMP_MENSAL", nullable = false)
    private BigDecimal valorAcompanhamentoMensal;

	@Column(name = "DT_INICIAL", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataInicio;

	@Column(name = "DT_FINAL")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataFim;

	@NotAudited
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "contratoPessoaVigencia", orphanRemoval = true)
    private List<ContratoPessoaTipoAndamento> andamentoAtos = Lists.newArrayList();

    public ContratoPessoaVigencia(){
        this.valorMensal = BigDecimal.ZERO;
        this.dataInicio = new Date();
    }

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ContratoPessoa getContratoPessoa() {
		return contratoPessoa;
	}

	public void setContratoPessoa(ContratoPessoa contratoPessoa) {
		this.contratoPessoa = contratoPessoa;
	}

	public BigDecimal getValorMensal() {
		return valorMensal;
	}

	public void setValorMensal(BigDecimal valorMensal) {
		this.valorMensal = valorMensal;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

    public List<ContratoPessoaTipoAndamento> getAndamentoAtos() {
        return andamentoAtos;
    }

    public void setAndamentoAtos(List<ContratoPessoaTipoAndamento> andamentoAtos) {
        this.andamentoAtos = andamentoAtos;
    }

    public BigDecimal getValorAcompanhamentoMensal() {
        return valorAcompanhamentoMensal;
    }

    public void setValorAcompanhamentoMensal(BigDecimal valorAcompanhamentoMensal) {
        this.valorAcompanhamentoMensal = valorAcompanhamentoMensal;
    }

	public void addAndamentosAtos(ContratoPessoaTipoAndamento andamento){
		this.andamentoAtos.add(andamento);
	}

    public boolean isContratoVigente() {
		if ((dataInicio != null && dataFim == null) || (dataFim != null && dataFim.after(new Date())))
			return Boolean.TRUE;
		return Boolean.FALSE;
	}

    public void addRowNumberAndamentos(){
        int i = 1;
        for (ContratoPessoaTipoAndamento item : andamentoAtos) {
            item.setRowNumber(i);
            i++;
        }
    }
}
