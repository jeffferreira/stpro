package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import br.com.juridico.enums.ControladoriaIndicador;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.util.DateUtils;
import br.com.juridico.validation.processo.andamento.AndamentoValidacaoWrapper;
import br.com.juridico.validation.processo.andamento.PrazoValidacaoWrapper;
import com.google.common.collect.Lists;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author Marcos.Melo
 */
@Entity
@Audited
@Table(name = "TB_ANDAMENTO")
public class Andamento implements IEntity, Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_ANDAMENTO", nullable = false)
	private Long id;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@JoinColumn(name = "FK_PROCESSO", referencedColumnName = "ID_PROCESSO", nullable = false)
	@ManyToOne(optional = false)
	private Processo processo;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@JoinColumn(name = "FK_TIPO_ANDAMENTO", referencedColumnName = "ID_TIPO_ANDAMENTO", nullable = false)
	@ManyToOne(optional = false)
	private TipoAndamento tipoAndamento;

	@Type(type="true_false")
	@Column(name = "FL_DEFINE_PRAZO_FINAL", nullable = false)
	private Boolean definePrazoFinal;

	@Type(type="true_false")
	@Column(name = "FL_HORARIO_INDEFINIDO", nullable = false)
	private Boolean horarioIndefinido;

	@Column(name = "DT_ANDAMENTO", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataAndamento;

	@Type(type="true_false")
	@Column(name = "FL_CONFIDENCIAL", nullable = false)
	private Boolean confidencial;

	@Column(name = "DS_INFORMACAO_CLIENTE", length = 4000)
	private String informacaoCliente;

	@Column(name = "DS_COMPLEMENTO", length = 4000)
	private String complemento;

	@Type(type="true_false")
	@Column(name = "FL_ENVIA_RESUMO_ADVOGADO", nullable = false)
	private Boolean enviaResumoAdvogado;

	@Type(type="true_false")
	@Column(name = "FL_ENVIA_RESUMO_ADVOGADOS", nullable = false)
	private Boolean enviaResumoAdvogados;

	@Type(type="true_false")
	@Column(name = "FL_ENVIA_RESUMO_CLIENTE", nullable = false)
	private Boolean enviaResumoCliente;

	@Type(type="true_false")
	@Column(name = "FL_ENVIA_EMAIL_AVISO", nullable = false)
	private Boolean enviaEmailAviso;

	@Column(name = "DT_FINAL_PRAZO_FATAL")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataFinalPrazoFatal;

	@Column(name = "DT_FINAL_PRAZO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataFinalPrazo;

	@Column(name = "DS_HORA_INICIO", length = 5)
	private String horaInicio;

	@Column(name = "DS_HORA_FIM", length = 5)
	private String horaFim;

	@Column(name = "DS_DESCRICAO_PRAZO", length = 200)
	private String descricaoPrazo;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne
	@JoinColumn(name = "FK_STATUS_AGENDA", referencedColumnName = "ID_STATUS_AGENDA", nullable = true)
	private StatusAgenda statusAgenda;

	@Type(type="true_false")
	@Column(name = "FL_HOUVE_ACORDO", nullable = false)
	private Boolean houveAcordo;

	@Column(name = "NR_QTDE_DIAS_EMAIL_AVISO_ANTECEDENCIA")
	private Long quantidadeDiasEmailAvisoAntecedencia;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne
	@JoinColumn(name = "FK_PESSOA_FISICA", referencedColumnName = "ID_PESSOA_FISICA")
	private PessoaFisica pessoaFisica;

	@Column(name = "NR_EMPRESA", nullable = false)
	private Long codigoEmpresa;

	@NotAudited
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "andamento")
	private List<AndamentoAdvogado> andamentoAdvogados = Lists.newArrayList();

    @NotAudited
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "andamento")
	private List<PrazoAndamento> prazos = Lists.newArrayList();

    @NotAudited
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "andamento")
	private List<Evento> eventos = Lists.newArrayList();

	@Column(name = "DS_OPCAO_LOCAL", nullable = false)
	private String opcaoLocal;

	@Column(name = "DS_LOCALIZACAO", nullable = false)
	private String localizacao;

	@Column(name = "DT_EXCLUSAO_LOGICA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataExclusao;

	public Andamento() {
		this.dataAndamento = new Date();
		this.definePrazoFinal = false;
		this.horarioIndefinido = false;
		this.confidencial = false;
		this.enviaResumoAdvogado = false;
		this.enviaResumoAdvogados = false;
		this.enviaResumoCliente = false;
		this.houveAcordo = false;
		this.enviaEmailAviso = true;
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Processo getProcesso() {
		return processo;
	}

	public void setProcesso(Processo processo) {
		this.processo = processo;
	}

	public TipoAndamento getTipoAndamento() {
		return tipoAndamento;
	}

	public void setTipoAndamento(TipoAndamento tipoAndamento) {
		this.tipoAndamento = tipoAndamento;
	}

	public Boolean getDefinePrazoFinal() {
		return definePrazoFinal;
	}

	public void setDefinePrazoFinal(Boolean definePrazoFinal) {
		this.definePrazoFinal = definePrazoFinal;
	}

	public Boolean getHorarioIndefinido() {
		return horarioIndefinido;
	}

	public void setHorarioIndefinido(Boolean horarioIndefinido) {
		this.horarioIndefinido = horarioIndefinido;
	}

	public Date getDataAndamento() {
		return dataAndamento;
	}

	public void setDataAndamento(Date dataAndamento) {
		this.dataAndamento = dataAndamento;
	}

	public Boolean getConfidencial() {
		return confidencial;
	}

	public void setConfidencial(Boolean confidencial) {
		this.confidencial = confidencial;
	}

	public String getInformacaoCliente() {
		return informacaoCliente;
	}

	public void setInformacaoCliente(String informacaoCliente) {
		this.informacaoCliente = informacaoCliente;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public Boolean getEnviaResumoAdvogado() {
		return enviaResumoAdvogado;
	}

	public void setEnviaResumoAdvogado(Boolean enviaResumoAdvogado) {
		this.enviaResumoAdvogado = enviaResumoAdvogado;
	}

	public Boolean getEnviaResumoAdvogados() {
		return enviaResumoAdvogados;
	}

	public void setEnviaResumoAdvogados(Boolean enviaResumoAdvogados) {
		this.enviaResumoAdvogados = enviaResumoAdvogados;
	}

	public Boolean getEnviaResumoCliente() {
		return enviaResumoCliente;
	}

	public void setEnviaResumoCliente(Boolean enviaResumoCliente) {
		this.enviaResumoCliente = enviaResumoCliente;
	}

	public Date getDataFinalPrazoFatal() {
		return dataFinalPrazoFatal;
	}

	public void setDataFinalPrazoFatal(Date dataFinalPrazoFatal) {
		this.dataFinalPrazoFatal = dataFinalPrazoFatal;
	}

    public Date getDataFinalPrazo() {
        return dataFinalPrazo;
    }

    public void setDataFinalPrazo(Date dataFinalPrazo) {
        this.dataFinalPrazo = dataFinalPrazo;
    }

    public String getDescricaoPrazo() {
		return descricaoPrazo;
	}

	public void setDescricaoPrazo(String descricaoPrazo) {
		this.descricaoPrazo = descricaoPrazo;
	}

	public StatusAgenda getStatusAgenda() {
		return statusAgenda;
	}

	public void setStatusAgenda(StatusAgenda statusAgenda) {
		this.statusAgenda = statusAgenda;
	}

	public Long getCodigoEmpresa() {
		return codigoEmpresa;
	}

	public void setCodigoEmpresa(Long codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}

	public List<AndamentoAdvogado> getAndamentoAdvogados() {
		return andamentoAdvogados;
	}

	public void setAndamentoAdvogados(List<AndamentoAdvogado> andamentoAdvogados) {
		this.andamentoAdvogados = andamentoAdvogados;
	}

	public List<PrazoAndamento> getPrazos() {
		return prazos.stream().filter(p -> p.getDataExclusao() == null).collect(Collectors.toList());
	}

	public void setPrazos(List<PrazoAndamento> prazos) {
		this.prazos = prazos;
	}

	public String getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(String horaInicio) {
		this.horaInicio = horaInicio;
	}

	public String getHoraFim() {
		return horaFim;
	}

	public void setHoraFim(String horaFim) {
		this.horaFim = horaFim;
	}

	public PessoaFisica getPessoaFisica() {
		return pessoaFisica;
	}

	public void setPessoaFisica(PessoaFisica pessoaFisica) {
		this.pessoaFisica = pessoaFisica;
	}

	public Boolean getEnviaEmailAviso() {
		return enviaEmailAviso;
	}

	public void setEnviaEmailAviso(Boolean enviaEmailAviso) {
		this.enviaEmailAviso = enviaEmailAviso;
	}

	public Long getQuantidadeDiasEmailAvisoAntecedencia() {
		return quantidadeDiasEmailAvisoAntecedencia;
	}

	public void setQuantidadeDiasEmailAvisoAntecedencia(Long quantidadeDiasEmailAvisoAntecedencia) {
		this.quantidadeDiasEmailAvisoAntecedencia = quantidadeDiasEmailAvisoAntecedencia;
	}

	public List<Evento> getEventos() {
		return eventos;
	}

	public void setEventos(List<Evento> eventos) {
		this.eventos = eventos;
	}

	public Boolean getHouveAcordo() {
		return houveAcordo;
	}

	public void setHouveAcordo(Boolean houveAcordo) {
		this.houveAcordo = houveAcordo;
	}

	public String getLocalizacao() {
		return localizacao;
	}

	public void setLocalizacao(String localizacao) {
		this.localizacao = localizacao;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder()
				.append(id).append(statusAgenda).toHashCode();
	}

	public String getOpcaoLocal() {
		return opcaoLocal;
	}

	public void setOpcaoLocal(String opcaoLocal) {
		this.opcaoLocal = opcaoLocal;
	}

	public Date getDataExclusao() {
		return dataExclusao;
	}

	public void setDataExclusao(Date dataExclusao) {
		this.dataExclusao = dataExclusao;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Andamento) {
			final Andamento other = (Andamento) obj;
			return new EqualsBuilder().append(id, other.id).append(statusAgenda, other.statusAgenda).isEquals();
		} else {
			return false;
		}
	}

	/**
	 *
	 * @throws ValidationException
	 */
	public void validarAndamento() throws ValidationException {
		runValidations(buildValidatorFactory().validate(new AndamentoValidacaoWrapper(this)));
	}

	public void validarPrazos() throws ValidationException {
		runValidations(buildValidatorFactory().validate(new PrazoValidacaoWrapper(this)));
	}

	public boolean isPrazoAtendido(){
		return statusAgenda != null && "Cumprido".equals(statusAgenda.getDescricao());
	}

	public boolean isTodosEventosEstaoPendentes(){
		if(this.eventos.isEmpty()){
			return Boolean.FALSE;
		}

		boolean todosEventosPendentes = Boolean.TRUE;
		for (Evento evento : this.eventos) {
			if(!ControladoriaIndicador.PENDENTE.getStatus().equals(evento.getControladoriaStatus())){
				todosEventosPendentes = Boolean.FALSE;
				break;
			}
		}
		return todosEventosPendentes;
	}

	public boolean isPrazoAtrasado(){
		Calendar calendar = Calendar.getInstance();
		return dataFinalPrazoFatal != null
				&& dataFinalPrazoFatal.before(calendar.getTime())
				&& (statusAgenda != null && !"Cumprido".equals(statusAgenda.getDescricao()) && !"Cancelado".equals(statusAgenda.getDescricao()));
	}

	public boolean isPossuiEventoPendente(){
		boolean possuiEventoPendente = Boolean.FALSE;
		for (Evento evento : this.eventos) {
			if(ControladoriaIndicador.PENDENTE.getStatus().equals(evento.getControladoriaStatus())){
				possuiEventoPendente = Boolean.TRUE;
				break;
			}
		}
		return possuiEventoPendente;
	}

	public boolean isEventoPrincipalPendente(){
		for (Evento evento : this.eventos) {
			if (evento.getPrazoAndamento() == null) {
				return ControladoriaIndicador.PENDENTE.getStatus().equals(evento.getControladoriaStatus());
			}
		}
		return Boolean.FALSE;
	}

	public String getDataFinalPrazoFatalFormatado() {
		return String.format("Prazo Fatal: %s", dataFinalPrazoFatal != null ? DateUtils.formataDataHoraBrasil(dataFinalPrazoFatal) : "") ;
	}

    public String getDataHoraFinalPrazoFatalFormatado() {
        return String.format("Prazo Fatal: %s %s", dataFinalPrazoFatal != null ? DateUtils.formataDataBrasil(dataFinalPrazoFatal) : "", getHoraInicio()) ;
    }

	public String getDataHoraFinalPrazo() {
	    return String.format("%s %s", DateUtils.formataDataBrasil(dataFinalPrazo), getHoraInicio());
    }

	public void addPrazo(PrazoAndamento prazoAndamento) {
		if(prazoAndamento == null) {
			return;
		}
		this.prazos.add(prazoAndamento);
	}

	public void removePrazosNovosExcluidosOuAdicionaAndamento(){
		Iterator<PrazoAndamento> iterator = prazos.iterator();
		while (iterator.hasNext()) {
			PrazoAndamento prazoAndamento = iterator.next();
			if(prazoAndamento.getId() == null && prazoAndamento.getDataExclusao() != null) {
				iterator.remove();
			} else {
				prazoAndamento.setAndamento(this);
			}
		}
	}

	public void removeSubAndamentoPorDescricao(String descricaoSubAndamento){
		Iterator<PrazoAndamento> iterator = prazos.iterator();
		while (iterator.hasNext()) {
			PrazoAndamento prazoAndamento = iterator.next();
			if(prazoAndamento.getSubAndamento().getDescricao().equals(descricaoSubAndamento)){
				iterator.remove();
			}
		}
	}

	public void limparSubAndamentos(){
		this.prazos.clear();
	}
}
