/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.cadastros.PosicaoValidacaoWrapper;
import com.google.common.base.Strings;
import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Jefferson
 */
@Entity
@Audited
@Table(name = "TB_POSICAO")
@NamedQueries({
		@NamedQuery(name = "Posicao.findByid", query = "SELECT t FROM Posicao t WHERE t.codigoEmpresa=:codigoEmpresa and t.id = :id"),
		@NamedQuery(name = "Posicao.findByDescricao", query = "SELECT t FROM Posicao t WHERE t.codigoEmpresa=:codigoEmpresa and t.descricao = :descricao") })
public class Posicao implements IEntity, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_POSICAO", nullable = false)
	private Long id;

	@NotNull
	@NotEmpty(message = "Descrição: preenchimento obrigatório")
	@Column(name = "DS_POSICAO", length = 200)
	private String descricao;

	@Column(name = "NR_EMPRESA", nullable = false)
	private Long codigoEmpresa;

	@Column(name = "DT_EXCLUSAO_LOGICA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataExclusao;

	public Posicao() {
		super();
	}

	public Posicao(Long id, String descricao) {
		this.id = id;
		this.descricao = descricao;
	}

	public Posicao(String descricao, Long codigoEmpresa) {
		this.descricao = descricao;
		this.codigoEmpresa = codigoEmpresa;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof Posicao)) {
			return false;
		}
		Posicao other = (Posicao) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Date getDataExclusao() {
		return dataExclusao;
	}

	public void setDataExclusao(Date dataExclusao) {
		this.dataExclusao = dataExclusao;
	}

	@Override
	public String toString() {
		return "Posicao [id=" + id + ", descricao=" + descricao + "]";
	}

	public Long getCodigoEmpresa() {
		return codigoEmpresa;
	}

	public void setCodigoEmpresa(Long codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}

	public void validar(boolean existePosicaoCadastrado) throws ValidationException {
		runValidations(buildValidatorFactory().validate(new PosicaoValidacaoWrapper(existePosicaoCadastrado)));
	}

	public boolean isPosicaoUtilizadaSistema(){
		return PosicaoObrigatoria.fromPosicoesObrigatoria(descricao) != null;
	}

	public boolean isReuOuTerceiro(){
		if(Strings.isNullOrEmpty(this.descricao)){
			return Boolean.FALSE;
		}

		return this.descricao.equals(PosicaoObrigatoria.REU.getDescricao())
				|| this.descricao.equals(PosicaoObrigatoria.TERCEIRO.getDescricao());
	}

	/**
	 *
	 */
	enum PosicaoObrigatoria {
		AUTOR("Autor"), REU("Réu"), TERCEIRO("Terceiro");

		private String descricao;

		PosicaoObrigatoria(String descricao){
			this.descricao = descricao;
		}

		public static PosicaoObrigatoria fromPosicoesObrigatoria(String descricao) {
			for (PosicaoObrigatoria posicaoObrigatoria : values()) {
				if(posicaoObrigatoria.getDescricao().equals(descricao)){
					return posicaoObrigatoria;
				}
			}
			return null;
		}

		public String getDescricao() {
			return descricao;
		}
	}

}
