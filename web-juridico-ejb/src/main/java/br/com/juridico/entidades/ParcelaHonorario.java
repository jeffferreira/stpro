package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "TB_PARCELA_HONORARIO")
public class ParcelaHonorario implements IEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 295653569933262034L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_PARCELA_HONORARIO", nullable = false)
	private Long id;

	@JoinColumn(name = "FK_PROCESSO_HONORARIO", referencedColumnName = "ID_PROCESSO_HONORARIO", nullable = false)
	@ManyToOne(optional = false)
	private ProcessoHonorario processoHonorario;

    @Column(name = "VL_PARCELA", nullable = true)
    private BigDecimal valorParcela;
    
	@Column(name = "DS_PARCELA", nullable = true, length = 100)
	private String descricaoParcela;

	@Type(type="true_false")
	@Column(name = "FL_DT_FIM_ACAO", nullable = false)
	private Boolean flagDataFimAcao = true;

	@Column(name = "DT_VENCIMENTO", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataVencimento;

	@Column(name = "DS_VENCIMENTO", nullable = true)
	private String descricaoVencimento;

	@Type(type="true_false")
	@Column(name = "FL_PAGO", nullable = false, length = 1)
	private Boolean pago = false;
    
	@Column(name = "NR_EMPRESA", nullable = false)
	private Long codigoEmpresa;


    public ParcelaHonorario() {
        super();
	}

	public ParcelaHonorario(Long codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

    public ProcessoHonorario getProcessoHonorario() {
		return processoHonorario;
	}

	public void setProcessoHonorario(ProcessoHonorario processoHonorario) {
		this.processoHonorario = processoHonorario;
	}

	public BigDecimal getValorParcela() {
		return valorParcela;
	}

	public void setValorParcela(BigDecimal valorParcela) {
		this.valorParcela = valorParcela;
	}

	public String getDescricaoParcela() {
		return descricaoParcela;
	}

	public void setDescricaoParcela(String descricaoParcela) {
		this.descricaoParcela = descricaoParcela;
	}

	public Boolean getFlagDataFimAcao() {
		return flagDataFimAcao;
	}

	public void setFlagDataFimAcao(Boolean flagDataFimAcao) {
		this.flagDataFimAcao = flagDataFimAcao;
	}

	public Date getDataVencimento() {
		return dataVencimento;
	}

	public void setDataVencimento(Date dataVencimento) {
		this.dataVencimento = dataVencimento;
	}

	public String getDescricaoVencimento() {
		return descricaoVencimento;
	}

	public void setDescricaoVencimento(String descricaoVencimento) {
		this.descricaoVencimento = descricaoVencimento;
	}

	public Boolean getPago() {
		return pago;
	}

	public void setPago(Boolean pago) {
		this.pago = pago;
	}

	public Long getCodigoEmpresa() {
		return codigoEmpresa;
	}

	public void setCodigoEmpresa(Long codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}


}
