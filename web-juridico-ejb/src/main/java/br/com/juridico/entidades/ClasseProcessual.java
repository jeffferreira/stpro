/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.cadastros.ClasseProcessualValidacaoWrapper;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Jefferson
 */
@Entity
@Audited
@Table(name = "TB_CLASSE_PROCESSUAL")
@NamedQueries({
		@NamedQuery(name = "ClasseProcessual.findByid", query = "SELECT t FROM ClasseProcessual t WHERE t.codigoEmpresa=:codigoEmpresa and t.id = :id"),
		@NamedQuery(name = "ClasseProcessual.findByDescricao", query = "SELECT t FROM ClasseProcessual t WHERE t.codigoEmpresa=:codigoEmpresa and t.descricao = :descricao") })
public class ClasseProcessual implements IEntity, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_CLASSE_PROCESSUAL", nullable = false)
	private Long id;

	@Column(name = "DS_CLASSE_PROCESSUAL", length = 200)
	private String descricao;

	@Column(name = "DT_EXCLUSAO_LOGICA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataExclusao;

	@Column(name = "NR_EMPRESA", nullable = false)
	private Long codigoEmpresa;

	@NotAudited
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "classeProcessual")
	private List<Processo> processos;

	public ClasseProcessual() {
		super();
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<Processo> getProcessos() {
		return processos;
	}

	public void setProcessos(List<Processo> processos) {
		this.processos = processos;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof ClasseProcessual)) {
			return false;
		}
		ClasseProcessual other = (ClasseProcessual) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return descricao;
	}

	public Date getDataExclusao() {
		return dataExclusao;
	}

	public void setDataExclusao(Date dataExclusao) {
		this.dataExclusao = dataExclusao;
	}

	public Long getCodigoEmpresa() {
		return codigoEmpresa;
	}

	public void setCodigoEmpresa(Long codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}

	public void validar(boolean existeClasseProcessualCadastrado) throws ValidationException {
		runValidations(buildValidatorFactory().validate(new ClasseProcessualValidacaoWrapper(this, existeClasseProcessualCadastrado)));
	}

}
