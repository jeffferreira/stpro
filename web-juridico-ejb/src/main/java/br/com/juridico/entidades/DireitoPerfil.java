package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import com.google.common.base.Strings;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Audited
@Table(name = "PFL_DIREITO_PERFIL")
public class DireitoPerfil implements IEntity, Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 3040574877620152837L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_DIREITO_PERFIL", nullable = false)
	private Long id;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@JoinColumn(name = "FK_DIREITO", referencedColumnName = "ID_DIREITO", nullable = false)
	@ManyToOne(optional = false)
	private Direito direito;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@JoinColumn(name = "FK_PERFIL_ACESSO", referencedColumnName = "ID_PERFIL_ACESSO", nullable = false)
	@ManyToOne(optional = false)
	private PerfilAcesso perfilAcesso;

	@Column(name = "NR_EMPRESA", nullable = false)
	private Long codigoEmpresa;

	@Column(name = "DT_EXCLUSAO_LOGICA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataExclusao;

	public DireitoPerfil(){super();}

	public DireitoPerfil(Direito direito, PerfilAcesso perfilAcesso, Long codigoEmpresa){
		this.direito = direito;
		this.perfilAcesso = perfilAcesso;
		this.codigoEmpresa = codigoEmpresa;
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Direito getDireito() {
		return direito;
	}

	public void setDireito(Direito direito) {
		this.direito = direito;
	}

	public Long getCodigoEmpresa() {
		return codigoEmpresa;
	}

	public void setCodigoEmpresa(Long codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}

	public Date getDataExclusao() {
		return dataExclusao;
	}

	public void setDataExclusao(Date dataExclusao) {
		this.dataExclusao = dataExclusao;
	}

	public PerfilAcesso getPerfilAcesso() {
		return perfilAcesso;
	}

	public void setPerfilAcesso(PerfilAcesso perfilAcesso) {
		this.perfilAcesso = perfilAcesso;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof Processo)) {
			return false;
		}
		DireitoPerfil other = (DireitoPerfil) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	public boolean containsPerfilDireito(String sigla, Long perfilAcessoId, Long direitoId){
		if(!Strings.isNullOrEmpty(sigla) && perfilAcessoId != null && direitoId != null) {
			return (direito != null && sigla.equals(direito.getSigla()) && direitoId.equals(direito.getId())) &&
					(perfilAcesso != null && perfilAcessoId.equals(perfilAcesso.getId()));
		}
		return Boolean.FALSE;

	}
}
