package br.com.juridico.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.validator.constraints.NotEmpty;

import br.com.juridico.dao.IEntity;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.cadastros.CentroCustoValidacaoWrapper;

/**
 *
 * @author Marcos
 */
@Entity
@Audited
@Table(name = "TB_CENTRO_CUSTO")
@NamedQueries({
		@NamedQuery(name = "CentroCusto.findByid", query = "SELECT t FROM CentroCusto t WHERE t.codigoEmpresa=:codigoEmpresa and t.id = :id"),
		@NamedQuery(name = "CentroCusto.findByDescricao", query = "SELECT t FROM CentroCusto t WHERE t.codigoEmpresa=:codigoEmpresa and t.descricao = :descricao") })
public class CentroCusto implements IEntity, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_CENTRO_CUSTO", nullable = false)
	private Long id;

	@NotNull
	@NotEmpty(message = "Descrição: preenchimento obrigatório")
	@Column(name = "DS_CENTRO_CUSTO", length = 200)
	private String descricao;

	@Column(name = "DT_EXCLUSAO_LOGICA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataExclusao;

	@Column(name = "NR_EMPRESA", nullable = false)
	private Long codigoEmpresa;

	@NotAudited
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "centroCusto")
	private List<PessoaJuridicaCentroCusto> pessoasJuridicasCentrosCustos;

	public CentroCusto() {
		super();
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<PessoaJuridicaCentroCusto> getPessoasJuridicasCentrosCustos() {
		return pessoasJuridicasCentrosCustos;
	}

	public void setPessoasJuridicasCentrosCustos(List<PessoaJuridicaCentroCusto> pessoasJuridicasCentrosCustos) {
		this.pessoasJuridicasCentrosCustos = pessoasJuridicasCentrosCustos;
	}

	public Date getDataExclusao() {
		return dataExclusao;
	}

	public void setDataExclusao(Date dataExclusao) {
		this.dataExclusao = dataExclusao;
	}

	public Long getCodigoEmpresa() {
		return codigoEmpresa;
	}

	public void setCodigoEmpresa(Long codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof CentroCusto)) {
			return false;
		}
		CentroCusto other = (CentroCusto) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	public void validar(boolean existeCentroCustoCadastrado) throws ValidationException {
		runValidations(buildValidatorFactory().validate(new CentroCustoValidacaoWrapper(existeCentroCustoCadastrado)));
	}

	@Override
	public String toString() {
		return "CentroCusto[ id=" + id + " ]";
	}



}
