package br.com.juridico.entidades;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.juridico.dao.IEntity;

/**
 * Created by jeffersonl2009_00 on 22/06/2016.
 */
@Entity
@Table(name = "TB_ATENDIMENTO_DOCUMENTO")
public class AtendimentoDocumento implements IEntity, Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_ATENDIMENTO_DOCUMENTO", nullable = false)
    private Long id;

    @JoinColumn(name = "FK_ATENDIMENTO_ANDAMENTO", referencedColumnName = "ID_ATENDIMENTO_ANDAMENTO", nullable = false)
    @ManyToOne(optional = false)
    private AndamentoAtendimento andamentoAtendimento;

    @Column(name = "DS_DOCUMENTO", nullable = false)
    private String nome;

    @Column(name = "DS_EXTENSAO", nullable = false)
    private String extensao;

    @Column(name = "DT_DATA_CADASTRO", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataCadastro;

    @Lob
    @Column(name = "BB_ARQUIVO", nullable = false)
    private byte[] arquivo;

    @Column(name = "NR_EMPRESA", nullable = false)
    private Long codigoEmpresa;

    public AtendimentoDocumento() {
		super();
	}

	public AtendimentoDocumento(AndamentoAtendimento andamentoAtendimento, String nome, String extensao, byte[] arquivo, Long codigoEmpresa){
        this.andamentoAtendimento = andamentoAtendimento;
        this.nome = nome;
        this.extensao = extensao;
        this.arquivo = arquivo;
        this.codigoEmpresa = codigoEmpresa;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AndamentoAtendimento getAndamentoAtendimento() {
        return andamentoAtendimento;
    }

    public void setAndamentoAtendimento(AndamentoAtendimento andamentoAtendimento) {
        this.andamentoAtendimento = andamentoAtendimento;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getExtensao() {
        return extensao;
    }

    public void setExtensao(String extensao) {
        this.extensao = extensao;
    }

    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public byte[] getArquivo() {
        return arquivo;
    }

    public void setArquivo(byte[] arquivo) {
        this.arquivo = arquivo;
    }

    public Long getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(Long codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }
}
