package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Jefferson
 */
@Entity
@Table(name = "TB_GRUPO_ADVOGADO")
@NamedQuery(name = "GrupoAdvogado.findByid", query = "SELECT t FROM GrupoAdvogado t WHERE t.codigoEmpresa=:codigoEmpresa and t.id = :id")
public class GrupoAdvogado implements IEntity, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_GRUPO_ADVOGADO", nullable = false)
	private Long id;

	@ManyToOne(optional = false)
	@JoinColumn(name = "FK_GRUPO", referencedColumnName = "ID_GRUPO", nullable = false)
	private Grupo grupo;

	@ManyToOne(optional = false)
	@JoinColumn(name = "FK_ADVOGADO", referencedColumnName = "ID_PESSOA_FISICA", nullable = false)
	private PessoaFisica advogadoPessoa;

	@Column(name = "NR_EMPRESA", nullable = false)
	private Long codigoEmpresa;

	@Column(name = "DT_EXCLUSAO_LOGICA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataExclusao;

	public GrupoAdvogado() {
		super();
	}

	public GrupoAdvogado(Grupo grupo, PessoaFisica advogadoPessoa, Long codigoEmpresa) {
		this.grupo = grupo;
		this.advogadoPessoa = advogadoPessoa;
		this.codigoEmpresa = codigoEmpresa;
	}

	public GrupoAdvogado(Long id) {
		this.id = id;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;

		if (o == null || getClass() != o.getClass()) return false;

		GrupoAdvogado that = (GrupoAdvogado) o;

		return new EqualsBuilder()
				.append(id, that.id)
				.append(grupo, that.grupo)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(id)
				.append(grupo)
				.toHashCode();
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Grupo getGrupo() {
		return grupo;
	}

	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}

	@Override
	public String toString() {
		return "GrupoAdvogado[ id=" + id + " ]";
	}

	public PessoaFisica getAdvogadoPessoa() {
		return advogadoPessoa;
	}

	public void setAdvogadoPessoa(PessoaFisica advogadoPessoa) {
		this.advogadoPessoa = advogadoPessoa;
	}

	public Date getDataExclusao() {
		return dataExclusao;
	}

	public void setDataExclusao(Date dataExclusao) {
		this.dataExclusao = dataExclusao;
	}

	public Long getCodigoEmpresa() {
		return codigoEmpresa;
	}

	public void setCodigoEmpresa(Long codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}

}
