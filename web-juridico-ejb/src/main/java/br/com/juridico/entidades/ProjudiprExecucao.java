package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Jefferson
 */
@Entity
@Table(name = "projudipr_execucao")
public class ProjudiprExecucao implements IEntity, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_EXECUCAO", nullable = false)
	private Long id;

	@Column(name = "ID_PROCESSO", nullable = false)
	private Long idProcesso;

	@Column(name = "DS_PROCESSO", length = 125)
	private String processo;

	@Column(name = "DT_CADASTRO", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCadastro;

	@Column(name = "FL_EXECUTADO", length = 3)
	private String executado = "NAO";

	@Column(name = "NR_EMPRESA", nullable = false)
	private Long codigoEmpresa;

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdProcesso() {
		return idProcesso;
	}

	public void setIdProcesso(Long idProcesso) {
		this.idProcesso = idProcesso;
	}

	public String getProcesso() {
		return processo;
	}

	public void setProcesso(String processo) {
		this.processo = processo;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public String getExecutado() {
		return executado;
	}

	public void setExecutado(String executado) {
		this.executado = executado;
	}

	public Long getCodigoEmpresa() {
		return codigoEmpresa;
	}

	public void setCodigoEmpresa(Long codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}
}
