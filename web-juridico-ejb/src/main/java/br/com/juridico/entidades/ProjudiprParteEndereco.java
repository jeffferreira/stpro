package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author Jefferson
 */
@Entity
@Table(name = "projudipr_parte_endereco")
public class ProjudiprParteEndereco implements IEntity, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_PARTE_ENDERECO", nullable = false)
	private Long id;

	@Column(name = "DS_TIPO", length = 75)
	private String tipo;

	@Column(name = "DS_DESCRICAO")
	private String descricao;

	@ManyToOne(optional = false)
	@JoinColumn(name = "FK_PROJUDI_PARTE", referencedColumnName = "ID_PARTE", nullable = false)
	private ProjudiprParte parte;

	public ProjudiprParteEndereco(){super();}

	public ProjudiprParteEndereco(String tipo, String descricao, ProjudiprParte parte) {
		this.tipo = tipo;
		this.descricao = descricao;
		this.parte = parte;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public ProjudiprParte getParte() {
		return parte;
	}

	public void setParte(ProjudiprParte parte) {
		this.parte = parte;
	}
}
