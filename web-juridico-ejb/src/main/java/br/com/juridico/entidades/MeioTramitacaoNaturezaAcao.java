package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author Jefferson
 */
@Entity
@Table(name = "tb_meio_tramitacao_natureza_acao")
public class MeioTramitacaoNaturezaAcao implements IEntity, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_MEIO_TRAMITACAO_NATUREZA_ACAO", nullable = false)
	private Long id;

	@NotAudited
	@ManyToOne
	@JoinColumn(name = "FK_MEIO_TRAMITACAO", referencedColumnName = "ID_MEIO_TRAMITACAO")
	private MeioTramitacao meioTramitacao;

	@NotAudited
	@ManyToOne
	@JoinColumn(name = "FK_NATUREZA_ACAO", referencedColumnName = "ID_NATUREZA_ACAO")
	private NaturezaAcao naturezaAcao;

	@Column(name = "NR_EMPRESA", nullable = false)
	private Long codigoEmpresa;

	public MeioTramitacaoNaturezaAcao() {
		super();
	}

	public MeioTramitacaoNaturezaAcao(MeioTramitacao meioTramitacao, NaturezaAcao naturezaAcao, Long codigoEmpresa) {
		this.meioTramitacao = meioTramitacao;
		this.naturezaAcao = naturezaAcao;
		this.codigoEmpresa = codigoEmpresa;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder()
				.append(naturezaAcao)
				.toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof MeioTramitacaoNaturezaAcao) {
			final MeioTramitacaoNaturezaAcao other = (MeioTramitacaoNaturezaAcao) obj;
			return new EqualsBuilder()
					.append(naturezaAcao, other.naturezaAcao)
					.isEquals();
		} else {
			return false;
		}
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public MeioTramitacao getMeioTramitacao() {
		return meioTramitacao;
	}

	public void setMeioTramitacao(MeioTramitacao meioTramitacao) {
		this.meioTramitacao = meioTramitacao;
	}

	public NaturezaAcao getNaturezaAcao() {
		return naturezaAcao;
	}

	public void setNaturezaAcao(NaturezaAcao naturezaAcao) {
		this.naturezaAcao = naturezaAcao;
	}

	public Long getCodigoEmpresa() {
		return codigoEmpresa;
	}

	public void setCodigoEmpresa(Long codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}
}
