package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import com.google.common.collect.Lists;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by jeffersonl2009_00 on 27/06/2016.
 */
@Entity
@Audited
@Table(name = "TB_EMAIL")
public class Email implements IEntity, Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_EMAIL", nullable = false)
    private Long id;

    @NotEmpty(message = "Sigla: preenchimento obrigatório")
    @Column(name = "DS_SIGLA")
    private String sigla;

    @Column(name = "DS_NOME")
    private String nome;

    @NotEmpty(message = "Assunto: preenchimento obrigatório")
    @Column(name = "DS_ASSUNTO")
    private String assunto;

    @NotEmpty(message = "Descrição: preenchimento obrigatório")
    @Column(name = "DS_EMAIL")
    private String descricao;

    @Column(name = "NR_EMPRESA", nullable = false)
    private Long codigoEmpresa;

    @Type(type="true_false")
    @Column(name = "FL_ATIVO", length = 1)
    private Boolean ativo;

    @Type(type="true_false")
    @Column(name = "FL_ENVIA_RESP_GERAL", length = 1)
    private Boolean enviaRespGeral;

    @Type(type="true_false")
    @Column(name = "FL_ENVIA_RESP_TODOS_DEPART", length = 1)
    private Boolean enviaRespTodosDepart;

    @Type(type="true_false")
    @Column(name = "FL_NECESSARIO_APROVACAO", length = 1)
    private Boolean necessarioAprovacao;

    @NotAudited
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "email", orphanRemoval = true)
    private List<EmailVinculoParametro> parametroEmails = Lists.newArrayList();

    public Email(){
        super();
    }

    public Email(String sigla, String assunto, String descricao, Long codigoEmpresa, Boolean ativo, Boolean necessarioAprovacao) {
        this.sigla = sigla;
        this.assunto = assunto;
        this.descricao = descricao;
        this.codigoEmpresa = codigoEmpresa;
        this.ativo = ativo;
        this.necessarioAprovacao = necessarioAprovacao;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Long getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(Long codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public Boolean getEnviaRespGeral() {
        return enviaRespGeral;
    }

    public void setEnviaRespGeral(Boolean enviaRespGeral) {
        this.enviaRespGeral = enviaRespGeral;
    }

    public Boolean getEnviaRespTodosDepart() {
        return enviaRespTodosDepart;
    }

    public void setEnviaRespTodosDepart(Boolean enviaRespTodosDepart) {
        this.enviaRespTodosDepart = enviaRespTodosDepart;
    }

    public String getAssunto() {
        return assunto;
    }

    public void setAssunto(String assunto) {
        this.assunto = assunto;
    }

    public List<EmailVinculoParametro> getParametroEmails() {
        return parametroEmails;
    }

    public void setParametroEmails(List<EmailVinculoParametro> parametroEmails) {
        this.parametroEmails = parametroEmails;
    }

    public Boolean getNecessarioAprovacao() {
        return necessarioAprovacao != null && necessarioAprovacao;
    }

    public void setNecessarioAprovacao(Boolean necessarioAprovacao) {
        this.necessarioAprovacao = necessarioAprovacao;
    }
}
