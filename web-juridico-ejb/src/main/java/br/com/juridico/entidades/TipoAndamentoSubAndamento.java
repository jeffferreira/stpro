package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by jeffersonl2009_00 on 26/08/2016.
 */
@Entity
@Table(name = "TB_TIPO_ANDAMENTO_SUB_ANDAMENTO")
public class TipoAndamentoSubAndamento implements IEntity, Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -8615615163104008049L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_ANDAMENTO_SUB_ANDAMENTO", nullable = false)
    private Long id;

    @Column(name = "NR_POSICAO", nullable = false)
    private Integer posicao;

    @JoinColumn(name = "FK_TIPO_ANDAMENTO", referencedColumnName = "ID_TIPO_ANDAMENTO", nullable = false)
    @ManyToOne(optional = false)
    private TipoAndamento tipoAndamento;

    @JoinColumn(name = "FK_SUB_ANDAMENTO", referencedColumnName = "ID_SUB_ANDAMENTO", nullable = false)
    @ManyToOne(optional = false)
    private SubAndamento subAndamento;

    public TipoAndamentoSubAndamento(){super();}

    public TipoAndamentoSubAndamento(TipoAndamento tipoAndamento, SubAndamento subAndamento, Integer posicao){
        this.tipoAndamento = tipoAndamento;
        this.subAndamento = subAndamento;
        this.posicao = posicao;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPosicao() {
        return posicao;
    }

    public void setPosicao(Integer posicao) {
        this.posicao = posicao;
    }

    public TipoAndamento getTipoAndamento() {
        return tipoAndamento;
    }

    public void setTipoAndamento(TipoAndamento tipoAndamento) {
        this.tipoAndamento = tipoAndamento;
    }

    public SubAndamento getSubAndamento() {
        return subAndamento;
    }

    public void setSubAndamento(SubAndamento subAndamento) {
        this.subAndamento = subAndamento;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(id)
                .append(posicao)
                .append(tipoAndamento)
                .append(subAndamento).toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof TipoAndamentoSubAndamento) {
            final TipoAndamentoSubAndamento other = (TipoAndamentoSubAndamento) obj;
            return new EqualsBuilder()
                    .append(id, other.id)
                    .append(posicao, other.posicao)
                    .append(tipoAndamento, other.tipoAndamento)
                    .append(subAndamento, other.subAndamento)
                    .isEquals();
        } else {
            return false;
        }
    }

}
