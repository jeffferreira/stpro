package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.cadastros.SubAndamentoValidacaoWrapper;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by jeffersonl2009_00 on 26/08/2016.
 */
@Entity
@Audited
@Table(name = "TB_SUB_ANDAMENTO")
@NamedQueries({
        @NamedQuery(name = "SubAndamento.findByid", query = "SELECT t FROM SubAndamento t WHERE t.codigoEmpresa=:codigoEmpresa and t.id = :id"),
        @NamedQuery(name = "SubAndamento.findByDescricao", query = "SELECT t FROM SubAndamento t WHERE t.codigoEmpresa=:codigoEmpresa and t.descricao = :descricao"),
        @NamedQuery(name = "SubAndamento.findLikeByDescricao", query = "SELECT t FROM SubAndamento t WHERE t.codigoEmpresa=:codigoEmpresa and t.descricao like :descricao")})
public class SubAndamento implements IEntity, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7402990284342742034L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_SUB_ANDAMENTO", nullable = false)
    private Long id;

    @NotNull
    @NotEmpty(message = "Descrição: preenchimento obrigatório")
    @Column(name = "DS_SUB_ANDAMENTO", length = 200)
    private String descricao;

    @Type(type="true_false")
    @Column(name = "FL_PRAZO_POSTERIOR_ANDAMENTO", nullable = false)
    private Boolean flagPrazoPosteriorAndamento;

    @Type(type="true_false")
    @Column(name = "FL_CRIA_EVENTO", nullable = false)
    private Boolean criaEventoFlag;

    @Column(name = "NR_DIAS_PRAZO_SEGURANCA", nullable = false)
    private Long diasPrazoSeguranca;

    @Column(name = "NR_EMPRESA", nullable = false)
    private Long codigoEmpresa;

    @Column(name = "DT_EXCLUSAO_LOGICA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataExclusao;

    public SubAndamento(){super();}

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Boolean getFlagPrazoPosteriorAndamento() {
        return flagPrazoPosteriorAndamento;
    }

    public void setFlagPrazoPosteriorAndamento(Boolean flagPrazoPosteriorAndamento) {
        this.flagPrazoPosteriorAndamento = flagPrazoPosteriorAndamento;
    }

    public Boolean getCriaEventoFlag() {
        return criaEventoFlag;
    }

    public void setCriaEventoFlag(Boolean criaEventoFlag) {
        this.criaEventoFlag = criaEventoFlag;
    }

    public Long getDiasPrazoSeguranca() {
        return diasPrazoSeguranca;
    }

    public void setDiasPrazoSeguranca(Long diasPrazoSeguranca) {
        this.diasPrazoSeguranca = diasPrazoSeguranca;
    }

    public Long getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(Long codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }

    public Date getDataExclusao() {
        return dataExclusao;
    }

    public void setDataExclusao(Date dataExclusao) {
        this.dataExclusao = dataExclusao;
    }

    public void validar(boolean existeVaraCadastrado) throws ValidationException {
        runValidations(buildValidatorFactory().validate(new SubAndamentoValidacaoWrapper(existeVaraCadastrado)));
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(id).append(descricao).toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof SubAndamento) {
            final SubAndamento other = (SubAndamento) obj;
            return new EqualsBuilder().append(id, other.id).append(descricao, other.descricao).isEquals();
        } else {
            return false;
        }
    }
}
