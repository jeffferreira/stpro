/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import com.google.common.collect.Lists;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Jefferson
 */
@Entity
@Table(name = "TB_ATENDIMENTO_ANDAMENTO")
public class AndamentoAtendimento implements IEntity, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_ATENDIMENTO_ANDAMENTO", nullable = false)
	private Long id;

	@JoinColumn(name = "FK_PESSOA", referencedColumnName = "ID_PESSOA", nullable = false)
	@ManyToOne(optional = false)
	private Pessoa pessoa;

	@JoinColumn(name = "FK_ATENDIMENTO", referencedColumnName = "ID_ATENDIMENTO", nullable = false)
	@ManyToOne(optional = false)
	private Atendimento atendimento;

	@Column(name = "DS_DESCRICAO", nullable = false, length = 255)
	private String descricao;

	@Column(name = "FL_STATUS", nullable = false, length = 1)
	private String status;

	@Column(name = "DT_DATA_CADASTRO", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCadastro;

	@Column(name = "NR_EMPRESA", nullable = false)
	private Long codigoEmpresa;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "andamentoAtendimento", orphanRemoval = true)
	private List<AtendimentoDocumento> documentos = Lists.newArrayList();

	public AndamentoAtendimento() {
		super();
	}

	public AndamentoAtendimento(String status, Date dataCadastro) {
		this.status = status;
		this.dataCadastro = dataCadastro;
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public Atendimento getAtendimento() {
		return atendimento;
	}

	public void setAtendimento(Atendimento atendimento) {
		this.atendimento = atendimento;
	}

	public Long getCodigoEmpresa() {
		return codigoEmpresa;
	}

	public void setCodigoEmpresa(Long codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}

	public List<AtendimentoDocumento> getDocumentos() {
		return documentos;
	}

	public void setDocumentos(List<AtendimentoDocumento> documentos) {
		this.documentos = documentos;
	}

	public void addDocumento(AtendimentoDocumento documento){
		this.documentos.add(documento);
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof AndamentoAtendimento)) {
			return false;
		}
		AndamentoAtendimento other = (AndamentoAtendimento) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}


	@Override
	public String toString() {
		return "AndamentoAtendimento[ id=" + id + " ]";
	}

}
