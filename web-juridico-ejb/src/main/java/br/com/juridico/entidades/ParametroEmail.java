package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by jeffersonl2009_00 on 18/08/2016.
 */
@Entity
@Table(name = "TB_PARAMETRO_EMAIL")
public class ParametroEmail implements IEntity, Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -8374095021522589008L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_PARAMETRO_EMAIL", nullable = false)
    private Long id;

    @Column(name = "DS_SIGLA", nullable = false)
    private String sigla;

    @Column(name = "DS_CHAVE_SESSAO", nullable = false)
    private String chaveSessao;

    @Column(name = "DS_OBSERVACAO ", length = 200)
    private String observacao;


    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getChaveSessao() {
        return chaveSessao;
    }

    public void setChaveSessao(String chaveSessao) {
        this.chaveSessao = chaveSessao;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof ParametroEmail)) {
            return false;
        }
        ParametroEmail other = (ParametroEmail) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
}


