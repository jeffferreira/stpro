package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Jefferson on 19/06/2016.
 */
@Entity
@Audited
@AuditTable(value="TB_CONTRATO_DOCUMENTO_AUD")
@Table(name = "TB_CONTRATO_DOCUMENTO")
public class ContratoDocumento implements IEntity, Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -7319223981065079923L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_CONTRATO_DOCUMENTO", nullable = false)
    private Long id;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @JoinColumn(name = "FK_CONTRATO_PESSOA", referencedColumnName = "ID_CONTRATO_PESSOA", nullable = false)
    @ManyToOne(optional = false)
    private ContratoPessoa contratoPessoa;

    @Column(name = "DS_DESCRICAO_ARQUIVO", nullable = false)
    private String descricao;

    @Column(name = "DS_TIPO_ARQUIVO", nullable = false)
    private String tipo;

    @Lob
    @NotAudited
    @Column(name = "BB_ARQUIVO", nullable = false)
    private byte[] arquivo;

    @Column(name = "DT_DATA_UPLOAD", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataUpload;

    @Column(name = "NR_EMPRESA", nullable = false)
    private Long codigoEmpresa;

    public ContratoDocumento(){
        super();
    }

    public ContratoDocumento(ContratoPessoa contratoPessoa, String descricao, String tipo, byte[] arquivo, Long codigoEmpresa){
        this.contratoPessoa = contratoPessoa;
        this.descricao = descricao;
        this.tipo = tipo;
        this.arquivo = arquivo;
        this.codigoEmpresa = codigoEmpresa;
        this.dataUpload = new Date();
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ContratoPessoa getContratoPessoa() {
        return contratoPessoa;
    }

    public void setContratoPessoa(ContratoPessoa contratoPessoa) {
        this.contratoPessoa = contratoPessoa;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public byte[] getArquivo() {
        return arquivo;
    }

    public void setArquivo(byte[] arquivo) {
        this.arquivo = arquivo;
    }

    public Date getDataUpload() {
        return dataUpload;
    }

    public void setDataUpload(Date dataUpload) {
        this.dataUpload = dataUpload;
    }

    public Long getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(Long codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }
}
