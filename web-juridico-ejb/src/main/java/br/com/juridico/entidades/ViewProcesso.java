package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import com.google.common.collect.Lists;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by Jefferson on 09/04/2016.
 */
@Entity
@Table(name = "VW_PROCESSO")
public class ViewProcesso implements IEntity, Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 4613046843312023113L;

	@Id
	@Column(name = "ID")
	private Long id;

	@Column(name = "PROCESSO")
	private String processo;

	@Column(name = "CLIENTES")
	private String clientesText;

	@Column(name = "ADVERSOS")
	private String adversosText;

	@Column(name = "ADVOGADOS")
	private String advogadosText;

	@Column(name = "FL_STATUS")
	private String status;

	@Column(name = "DATA_ABERTURA")
	private Date dataAbertura;

	@Column(name = "DATA_ABERTURA_TEXTO")
	private String dataAberturaTexto;

	@Column(name = "DATA_CADASTRO")
	private Date dataCadastro;

	@Column(name = "DATA_CADASTRO_TEXTO")
	private String dataCadastroTexto;

	@Column(name = "FASE")
	private String fase;

	@Column(name = "CLASSE_PROCESSUAL")
	private String classeProcessual;

	@Column(name = "NATUREZA_ACAO")
	private String naturezaAcao;

	@Column(name = "MEIO_TRAMITACAO")
	private String meioTramitacao;

	@Column(name = "CENTRO_CUSTO")
	private String centroCusto;

	@Column(name = "PEDIDOS")
	private String pedidosText;

	@Column(name = "CAUSAS_PEDIDO")
	private String causasPedidoText;

	@Column(name = "VARA")
	private String vara;

	@Column(name = "FORUM")
	private String forum;

	@Column(name = "COMARCA")
	private String comarca;

	@Column(name = "GRAU_RISCO")
	private String grauRisco;

	@Column(name = "VALOR_CAUSA")
	private BigDecimal valorCausa;

	@Column(name = "VALOR_PEDIDO")
	private BigDecimal valorPedido;

	@Column(name = "VALOR_PROVAVEL")
	private BigDecimal valorProvavel;

	@Column(name = "VALOR_TOTAL_PEDIDO")
	private BigDecimal valorTotalPedido;

	@Column(name = "VALOR_TOTAL_PROVAVEL")
	private BigDecimal valorTotalProvavel;

	@Column(name = "RESUMO")
	private  String resumo;

	@Column(name = "NUMERO_EMPRESA")
	private Long numeroEmpresa;

	@OneToMany(mappedBy = "processo")
	private List<ViewAdverso> adversos = Lists.newArrayList();

	@OneToMany(mappedBy = "processo")
	private List<ViewCliente> clientes = Lists.newArrayList();

	public ViewProcesso() {
		super();
	}

	public ViewProcesso(Long id, String processo) {
		this.id = id;
		this.processo = processo;
	}

	@Override
	public Long getId() {
		return id;
	}

	public String getProcesso() {
		return processo;
	}

	public String getClientesText() {
		return clientesText;
	}

	public String getAdversosText() {
		return adversosText;
	}

	public String getAdvogadosText() {
		return advogadosText;
	}

	public String getCentroCusto() {
		return centroCusto;
	}

	public String getPedidosText() {
		return pedidosText;
	}

	public String getCausasPedidoText() {
		return causasPedidoText;
	}

	public String getStatus() {
		return status;
	}

	public Date getDataAbertura() {
		return dataAbertura;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public String getDataCadastroTexto() {
		return dataCadastroTexto;
	}

	public String getFase() {
		return fase;
	}

	public String getClasseProcessual() {
		return classeProcessual;
	}

	public String getNaturezaAcao() {
		return naturezaAcao;
	}

	public String getMeioTramitacao() {
		return meioTramitacao;
	}

	public String getVara() {
		return vara;
	}

	public String getDataAberturaTexto() {
		return dataAberturaTexto;
	}

	public String getForum() {
		return forum;
	}

	public String getComarca() {
		return comarca;
	}

	public String getGrauRisco() {
		return grauRisco;
	}

	public BigDecimal getValorCausa() {
		return valorCausa;
	}

	public BigDecimal getValorPedido() {
		return valorPedido;
	}

	public BigDecimal getValorProvavel() {
		return valorProvavel;
	}

	public String getResumo() {
		return resumo;
	}

	public void setResumo(String resumo) {
		this.resumo = resumo;
	}

	public Long getNumeroEmpresa() {
		return numeroEmpresa;
	}

	public List<ViewAdverso> getAdversos() {
		return adversos;
	}

	public List<ViewCliente> getClientes() {
		return clientes;
	}

	public BigDecimal getValorTotalPedido() {
		return valorTotalPedido;
	}

	public BigDecimal getValorTotalProvavel() {
		return valorTotalProvavel;
	}

	public boolean isAtivo() {
		return "ATIVO".equals(status);
	}

	public boolean isSuspenso() {
		return "SUSPENSO".equals(status);
	}

	public boolean isFinalizado() {
		return "FINALIZADO".equals(status);
	}

	public String getDescricaoCliente() {
		return getNomePessoa(clientesText);
	}

	public String getDescricaoAdverso() {
		return getNomePessoa(adversosText);
	}

	public String getNomePessoa(String nomePessoa) {
		StringBuilder sb = new StringBuilder();
		if(nomePessoa == null){
			return "";
		}
		String arr[] = nomePessoa.trim().split("\\|");

		for (String pessoa : arr) {
			int index = pessoa.indexOf(":");
			sb.append(pessoa.substring(index + 1).trim()).append("; ");
		}

		if (arr.length == 1) {
			sb.deleteCharAt(sb.lastIndexOf(";"));
		}
		return sb.toString();
	}

}
