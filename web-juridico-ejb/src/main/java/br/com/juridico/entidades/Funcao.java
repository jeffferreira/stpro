package br.com.juridico.entidades;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.NotEmpty;

import br.com.juridico.dao.IEntity;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.cadastros.FuncaoValidacaoWrapper;

/**
 * Created by jeffersonl2009_00 on 17/08/2016.
 */
@Entity
@Audited
@Table(name = "TB_FUNCAO")
@NamedQueries({
        @NamedQuery(name = "Funcao.findByid", query = "SELECT t FROM Funcao t WHERE t.codigoEmpresa=:codigoEmpresa and t.id = :id"),
        @NamedQuery(name = "Funcao.findByDescricao", query = "SELECT t FROM Funcao t WHERE t.codigoEmpresa=:codigoEmpresa and t.descricao = :descricao") })
public class Funcao implements IEntity, Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_FUNCAO", nullable = false)
    private Long id;

    @NotNull
    @NotEmpty(message = "Descrição: preenchimento obrigatório")
    @Column(name = "DS_FUNCAO", length = 200)
    private String descricao;

    @Column(name = "NR_EMPRESA", nullable = false)
    private Long codigoEmpresa;

    @Column(name = "DT_EXCLUSAO_LOGICA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataExclusao;

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Funcao other = (Funcao) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Long getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(Long codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }

    public Date getDataExclusao() {
        return dataExclusao;
    }

    public void setDataExclusao(Date dataExclusao) {
        this.dataExclusao = dataExclusao;
    }

    public void validar(boolean existeFuncaoCadastrada) throws ValidationException {
        runValidations(buildValidatorFactory().validate(new FuncaoValidacaoWrapper(existeFuncaoCadastrada)));
    }
}
