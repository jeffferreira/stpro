package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import com.google.common.collect.Lists;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Jefferson
 */
@Entity
@Table(name = "projudipr_parte")
public class ProjudiprParte implements IEntity, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_PARTE", nullable = false)
	private Long id;

	@Column(name = "DS_TIPO", length = 75)
	private String tipo;

	@Column(name = "DS_NOME")
	private String nome;

	@Column(name = "DS_RG")
	private String rg;

	@Column(name = "DS_CPFCNPJ")
	private String cpfCnpj;

	@Column(name = "DS_OBSERVACAO")
	private String observacao;

	@ManyToOne(optional = false)
	@JoinColumn(name = "FK_PROJUDI_PROCESSO", referencedColumnName = "ID_PROCESSO", nullable = false)
	private ProjudiprProcesso processo;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "parte", orphanRemoval = true)
	private List<ProjudiprParteAdvogado> advogados = Lists.newArrayList();

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "parte", orphanRemoval = true)
	private List<ProjudiprParteEndereco> enderecos = Lists.newArrayList();

	public ProjudiprParte(){
		super();
	}

	public ProjudiprParte(String tipo, String nome, String observacao, ProjudiprProcesso processo) {
		this.tipo = tipo;
		this.nome = nome;
		this.observacao = observacao;
		this.processo = processo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public ProjudiprProcesso getProcesso() {
		return processo;
	}

	public void setProcesso(ProjudiprProcesso processo) {
		this.processo = processo;
	}

	public List<ProjudiprParteAdvogado> getAdvogados() {
		return advogados;
	}

	public void setAdvogados(List<ProjudiprParteAdvogado> advogados) {
		this.advogados = advogados;
	}

	public List<ProjudiprParteEndereco> getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(List<ProjudiprParteEndereco> enderecos) {
		this.enderecos = enderecos;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}
}
