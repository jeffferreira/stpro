package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author Jefferson
 */
@Entity
@Audited
@Table(name = "TB_PARTE_ADVOGADO")
public class ParteAdvogado implements IEntity, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_PARTE_ADVOGADO", nullable = false)
	private Long id;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@JoinColumn(name = "FK_PESSOA", referencedColumnName = "ID_PESSOA", nullable = false)
	@ManyToOne(optional = false)
	private Pessoa pessoa;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @ManyToOne(optional = false, cascade = CascadeType.ALL)
	@JoinColumn(name = "FK_PARTE", referencedColumnName = "ID_PARTE", nullable = false)
	private Parte parte;

	@Column(name = "NR_EMPRESA", nullable = false)
	private Long codigoEmpresa;

	@Column(name = "NR_RATING_PRINCIPAL")
	private Integer ratingPrincipal;

	public ParteAdvogado() {
		super();
	}

	public ParteAdvogado(Pessoa pessoa, Parte parte, Long codigoEmpresa) {
		this.pessoa = pessoa;
		this.parte = parte;
		this.codigoEmpresa = codigoEmpresa;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder()
				.append(id).toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ParteAdvogado) {
			final ParteAdvogado other = (ParteAdvogado) obj;
			return new EqualsBuilder().append(id, other.id).isEquals();
		} else {
			return false;
		}
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public Parte getParte() {
		return parte;
	}

	public void setParte(Parte parte) {
		this.parte = parte;
	}

	public Long getCodigoEmpresa() {
		return codigoEmpresa;
	}

	public void setCodigoEmpresa(Long codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}

	public Integer getRatingPrincipal() {
		return ratingPrincipal;
	}

	public void setRatingPrincipal(Integer ratingPrincipal) {
		this.ratingPrincipal = ratingPrincipal;
	}
}
