package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Jefferson on 23/08/2016.
 */
@Entity
@Table(name = "tb_evento_rejeito")
public class EventoRejeito implements IEntity, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_EVENTO_REJEITO", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "FK_EVENTO", referencedColumnName = "ID_EVENTO")
    private Evento evento;

    @Column(name = "DS_QUEM_REJEITOU")
    private String quemRejeitou;

    @Column(name = "DS_QUEM_CADASTROU")
    private String quemCadastrou;

    @Column(name = "DS_MOTIVO")
    private String motivo;

    @Column(name = "DT_REJEITO", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataRejeito;

    @Column(name = "NR_EMPRESA", nullable = false)
    private Long codigoEmpresa;

    public EventoRejeito(){
        super();
    }

    public EventoRejeito(Evento evento, String quemRejeitou, String quemCadastrou, String motivo, Long codigoEmpresa) {
        this.evento = evento;
        this.quemRejeitou = quemRejeitou;
        this.quemCadastrou = quemCadastrou;
        this.motivo = motivo;
        this.dataRejeito = Calendar.getInstance().getTime();
        this.codigoEmpresa = codigoEmpresa;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(evento).append(dataRejeito).toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof EventoRejeito) {
            final EventoRejeito other = (EventoRejeito) obj;
            return new EqualsBuilder().append(evento, other.evento).append(dataRejeito, other.dataRejeito).isEquals();
        } else {
            return false;
        }
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Evento getEvento() {
        return evento;
    }

    public void setEvento(Evento evento) {
        this.evento = evento;
    }

    public String getQuemRejeitou() {
        return quemRejeitou;
    }

    public void setQuemRejeitou(String quemRejeitou) {
        this.quemRejeitou = quemRejeitou;
    }

    public String getQuemCadastrou() {
        return quemCadastrou;
    }

    public void setQuemCadastrou(String quemCadastrou) {
        this.quemCadastrou = quemCadastrou;
    }

    public Date getDataRejeito() {
        return dataRejeito;
    }

    public void setDataRejeito(Date dataRejeito) {
        this.dataRejeito = dataRejeito;
    }

    public Long getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(Long codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }
}
