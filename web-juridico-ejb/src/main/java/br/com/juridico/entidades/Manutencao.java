package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table(name = "ADMIN_MANUTENCAO")
public class Manutencao implements IEntity, Serializable {

    /**
	 *
	 */
    private static final long serialVersionUID = -6732903233144666231L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_MANUTENCAO", nullable = false)
    private Long id;

    @Size(max = 20, message = "Login: tamanho máximo permitido 20")
    @Column(name = "DS_LOGIN", length = 20)
    private String login;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "BB_SENHA", columnDefinition = "BLOB")
    private byte[] senha;

    public Manutencao() {
        super();
    }

    public Manutencao(String login, byte[] senha) {
        this.login = login;
        this.senha = senha;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public byte[] getSenha() {
        return senha;
    }

    public void setSenha(byte[] senha) {
        this.senha = senha;
    }

}
