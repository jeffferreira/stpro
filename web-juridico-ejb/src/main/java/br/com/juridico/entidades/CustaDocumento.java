package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Audited
@Table(name = "TB_CUSTA_DOCUMENTO")
public class CustaDocumento implements IEntity, Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1751778107175274390L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_CUSTA_DOCUMENTO", nullable = false)
    private Long id;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @JoinColumn(name = "FK_CUSTA", referencedColumnName = "ID_CUSTA", nullable = false)
    @ManyToOne(optional = false)
    private Custa custa;

    @Column(name = "DS_DESCRICAO_ARQUIVO", nullable = false)
    private String descricao;

    @Column(name = "DS_TIPO_ARQUIVO", nullable = false)
    private String tipo;

    @NotAudited
    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "BB_ARQUIVO", nullable = false)
    private byte[] arquivo;

    @Column(name = "DT_DATA_UPLOAD", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataUpload;

    @Column(name = "NR_EMPRESA", nullable = false)
    private Long codigoEmpresa;

    public CustaDocumento(){
        super();
    }

    public CustaDocumento(Custa custa, String descricao, String tipo, byte[] arquivo, Long codigoEmpresa){
        this.custa = custa;
        this.descricao = descricao;
        this.tipo = tipo;
        this.arquivo = arquivo;
        this.codigoEmpresa = codigoEmpresa;
        this.dataUpload = new Date();
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Custa getCusta() {
		return custa;
	}

	public void setCusta(Custa custa) {
		this.custa = custa;
	}

	public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public byte[] getArquivo() {
        return arquivo;
    }

    public void setArquivo(byte[] arquivo) {
        this.arquivo = arquivo;
    }

    public Date getDataUpload() {
        return dataUpload;
    }

    public void setDataUpload(Date dataUpload) {
        this.dataUpload = dataUpload;
    }

    public Long getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(Long codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }
}
