package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author Jefferson
 */
@Entity
@Table(name = "TB_ROBO_TRAMITACAO")
public class RoboTramitacao implements IEntity, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_ROBO_TRAMITACAO", nullable = false)
	private Long id;

	@Column(name = "DS_MEIO_TRAMITACAO", length = 200)
	private String descricao;


	@Column(name = "DS_HOME_PAGE", length = 200)
	private String homePage;

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getHomePage() {
		return homePage;
	}

	public void setHomePage(String homePage) {
		this.homePage = homePage;
	}
}
