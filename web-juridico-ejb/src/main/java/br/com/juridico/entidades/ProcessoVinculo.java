package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.processo.ProcessoVinculoValidacaoWrapper;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author Marcos.Melo
 */
@Entity
@Audited
@Table(name = "TB_PROCESSO_VINCULO")
public class ProcessoVinculo implements IEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1067895706985091577L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_PROCESSO_VINCULO", nullable = false)
	private Long id;

	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@JoinColumn(name = "FK_PROCESSO_PAI", referencedColumnName = "ID_PROCESSO", nullable = false)
	@ManyToOne(optional = false)
	private Processo processoPai;

	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@JoinColumn(name = "FK_PROCESSO_FILHO", referencedColumnName = "ID_PROCESSO", nullable = false)
	@ManyToOne(optional = false)
	private Processo processoFilho;

	@Column(name = "NR_EMPRESA", nullable = false)
	private Long codigoEmpresa;

	@Transient
	private Processo processo;


	public ProcessoVinculo() {
		super();
	}

	public ProcessoVinculo(Processo processoPai, Processo processoFilho, Long codigoEmpresa) {
		this.processoPai = processoPai;
		this.processoFilho = processoFilho;
		this.codigoEmpresa = codigoEmpresa;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder()
				.append(id).toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ProcessoVinculo) {
			final ProcessoVinculo other = (ProcessoVinculo) obj;
			return new EqualsBuilder().append(id, other.id).isEquals();
		} else {
			return false;
		}
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Processo getProcessoPai() {
		return processoPai;
	}

	public void setProcessoPai(Processo processoPai) {
		this.processoPai = processoPai;
	}

	public Processo getProcessoFilho() {
		return processoFilho;
	}

	public void setProcessoFilho(Processo processoFilho) {
		this.processoFilho = processoFilho;
	}

	public Long getCodigoEmpresa() {
		return codigoEmpresa;
	}

	public void setCodigoEmpresa(Long codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}

	public Processo getProcesso() {
		return processo;
	}

	public void setProcesso(Long idProcesso) {
		if(idProcesso != null && !getProcessoPai().getId().equals(idProcesso)) {
			this.processo = getProcessoPai();
		} else if(idProcesso != null && !getProcessoFilho().getId().equals(idProcesso)) {
			this.processo = getProcessoFilho();
		}
	}
	
	public void validar() throws ValidationException {
		runValidations(buildValidatorFactory().validate(new ProcessoVinculoValidacaoWrapper(this)));
	}
}
