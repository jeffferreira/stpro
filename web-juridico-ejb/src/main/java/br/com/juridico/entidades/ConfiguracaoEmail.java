package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by jefferson.ferreira on 07/02/2017.
 */
@Entity
@Table(name = "TB_CONFIGURACAO_EMAIL")
public class ConfiguracaoEmail implements IEntity, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_CONFIGURACAO_EMAIL", nullable = false)
    private Long id;

    @Column(name = "DS_HOST", nullable = false)
    private String host;

    @Column(name = "NR_PORT", nullable = false)
    private Integer port;

    @Column(name = "DS_USERNAME", nullable = false)
    private String username;

    @Column(name = "DS_PASSWORD", nullable = false)
    private String password;

    @Column(name = "DS_PROTOCOL", nullable = false)
    private String protocol;

    @Column(name = "DS_FROM", nullable = false)
    private String from;

    @Column(name = "NR_EMPRESA", nullable = false)
    private Long codigoEmpresa;

    public ConfiguracaoEmail() {
    }

    public ConfiguracaoEmail(String host, Integer port, String username, String password, String protocol, String from, Long codigoEmpresa) {
        this.host = host;
        this.port = port;
        this.username = username;
        this.password = password;
        this.protocol = protocol;
        this.from = from;
        this.codigoEmpresa = codigoEmpresa;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public Long getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(Long codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }
}
