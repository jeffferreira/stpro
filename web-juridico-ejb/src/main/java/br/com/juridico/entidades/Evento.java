package br.com.juridico.entidades;

import br.com.juridico.comparator.HistoricoEventoComparator;
import br.com.juridico.dao.IEntity;
import br.com.juridico.state.ControladoriaState;
import br.com.juridico.state.controladoria.AprovaControladoria;
import br.com.juridico.state.controladoria.ControladoriaContext;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.*;

/**
 * Created by Jefferson on 23/08/2016.
 */
@Entity
@Audited
@Table(name = "TB_EVENTO")
public class Evento implements IEntity, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -6764738977589478657L;

    private static final String CUMPRIDO = "Cumprido";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_EVENTO", nullable = false)
    private Long id;

    @Column(name = "DS_NOME_INICIAIS", length = 2)
    private String nomeIniciais;

    @Column(name = "CHAVE", nullable = false)
    private String chaveId;

    @Column(name = "DS_DESCRICAO", nullable = false)
    private String descricao;

    @Column(name = "DS_OBSERVACAO")
    private String observacao;

    @Column(name = "DS_LOCALIZACAO")
    private String localizacao;

    @Column(name = "DT_INICIO", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataInicio;

    @Column(name = "DT_FIM", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataFim;

    @Type(type="true_false")
    @Column(name = "FL_HORARIO_INDEFINIDO")
    private Boolean horarioIndefinido;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @ManyToOne
    @JoinColumn(name = "FK_STATUS_AGENDA", referencedColumnName = "ID_STATUS_AGENDA")
    private StatusAgenda statusAgenda;

    @Column(name = "DS_ESTILO")
    private String style;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @ManyToOne
    @JoinColumn(name = "FK_PESSOA", referencedColumnName = "ID_PESSOA")
    private Pessoa pessoa;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @ManyToOne
    @JoinColumn(name = "FK_ANDAMENTO", referencedColumnName = "ID_ANDAMENTO")
    private Andamento andamento;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @ManyToOne
    @JoinColumn(name = "FK_PRAZO_ANDAMENTO", referencedColumnName = "ID_PRAZO_ANDAMENTO")
    private PrazoAndamento prazoAndamento;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "FK_PROCESSO", referencedColumnName = "ID_PROCESSO")
    private Processo processo;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DH_ALTERACAO", nullable = false)
    private Date dataHoraAlteracao;

    @Column(name = "FL_APROVADO_CONTROLADORIA", length = 1)
    private String controladoriaStatus;

    @NotNull
    @Column(name = "NR_EMPRESA", nullable = false)
    private Long codigoEmpresa;

    @Column(name = "DT_EXCLUSAO_LOGICA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataExclusao;

    @NotAudited
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "evento", orphanRemoval = true)
    private List<HistoricoEvento> historicoEventos = Lists.newArrayList();

    @NotAudited
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "evento")
    private Set<EventoRejeito> rejeitos = Sets.newHashSet();

    @Transient
    private boolean transferir;
    @Transient
    private String ultimoTipoOperacao;

    public Evento(){
        super();
    }

    /**
     *
     * @param chaveId
     * @param descricao
     * @param dataInicio
     * @param dataFim
     * @param style
     * @param pessoa
     * @param empresa
     */
    public Evento(String chaveId, String descricao, StatusAgenda statusAgenda, Date dataInicio, Date dataFim, String style, Pessoa pessoa, Empresa empresa){
        this.chaveId = chaveId;
        this.descricao = descricao;
        this.statusAgenda = statusAgenda;
        this.dataInicio = dataInicio;
        this.dataFim = dataFim;
        this.style = style;
        this.pessoa = pessoa;
        this.dataHoraAlteracao = Calendar.getInstance().getTime();
        this.codigoEmpresa = empresa == null ? null : empresa.getId();
        adicionaControladoria(empresa);
    }

    private void adicionaControladoria(Empresa empresa) {
        boolean configuracaoControadoria = empresa == null ? Boolean.FALSE : empresa.isConfiguracaoControladoria();
        ControladoriaContext context = new ControladoriaContext();
        ControladoriaState aprovacaoState = new AprovaControladoria();
        context.setState(aprovacaoState);
        controladoriaStatus = context.controladoriaAction(false, this.statusAgenda, configuracaoControadoria, null).getStatus();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(id).append(pessoa).append(dataHoraAlteracao).toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Evento) {
            final Evento other = (Evento) obj;
            return new EqualsBuilder().append(id, other.id).append(pessoa, other.pessoa).append(dataHoraAlteracao, other.dataHoraAlteracao).isEquals();
        } else {
            return false;
        }
    }

    public void addHistorico(HistoricoEvento historicoEvento){
        if(historicoEvento != null) {
            if(this.id != null) {
                this.dataHoraAlteracao = Calendar.getInstance().getTime();
                historicoEvento.setDataHoraAlteracao(this.dataHoraAlteracao);
                this.historicoEventos.add(historicoEvento);
                return;
            }
            historicoEvento.setQuemAprovou("STATUS_INICIAL");
            historicoEvento.setDataHoraAlteracao(this.dataHoraAlteracao);
            this.historicoEventos.add(historicoEvento);
            return;
        }
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomeIniciais() {
        return nomeIniciais;
    }

    public void setNomeIniciais(String nomeIniciais) {
        this.nomeIniciais = nomeIniciais;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public String getLocalizacao() {
        return localizacao;
    }

    public void setLocalizacao(String localizacao) {
        this.localizacao = localizacao;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataFim() {
        return dataFim;
    }

    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public Long getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(Long codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getChaveId() {
        return chaveId;
    }

    public void setChaveId(String chaveId) {
        this.chaveId = chaveId;
    }

    public Boolean getHorarioIndefinido() {
        return horarioIndefinido;
    }

    public void setHorarioIndefinido(Boolean horarioIndefinido) {
        this.horarioIndefinido = horarioIndefinido;
    }

    public boolean isTransferir() {
        return transferir;
    }

    public void setTransferir(boolean transferir) {
        this.transferir = transferir;
    }

    public StatusAgenda getStatusAgenda() {
        return statusAgenda;
    }

    public void setStatusAgenda(StatusAgenda statusAgenda) {
        this.statusAgenda = statusAgenda;
    }

    public boolean isTarefaCumprida(){
        return statusAgenda != null && CUMPRIDO.equals(statusAgenda.getDescricao());
    }

    public Andamento getAndamento() {
        return andamento;
    }

    public void setAndamento(Andamento andamento) {
        this.andamento = andamento;
    }

    public Processo getProcesso() {
        return processo;
    }

    public void setProcesso(Processo processo) {
        this.processo = processo;
    }

    public PrazoAndamento getPrazoAndamento() {
        return prazoAndamento;
    }

    public void setPrazoAndamento(PrazoAndamento prazoAndamento) {
        this.prazoAndamento = prazoAndamento;
    }

    public Date getDataHoraAlteracao() {
        return dataHoraAlteracao;
    }

    public void setDataHoraAlteracao(Date dataHoraAlteracao) {
        this.dataHoraAlteracao = dataHoraAlteracao;
    }

    public String getControladoriaStatus() {
        return controladoriaStatus;
    }

    public void setControladoriaStatus(String controladoriaStatus) {
        this.controladoriaStatus = controladoriaStatus;
    }

    public Set<EventoRejeito> getRejeitos() {
        return rejeitos;
    }

    public void setRejeitos(Set<EventoRejeito> rejeitos) {
        this.rejeitos = rejeitos;
    }

    public Date getDataExclusao() {
        return dataExclusao;
    }

    public void setDataExclusao(Date dataExclusao) {
        this.dataExclusao = dataExclusao;
    }

    public List<HistoricoEvento> getHistoricoEventos() {
        Collections.sort(historicoEventos, new HistoricoEventoComparator());
        return historicoEventos;
    }

    public void setHistoricoEventos(List<HistoricoEvento> historicoEventos) {
        this.historicoEventos = historicoEventos;
    }

    public String getUltimoTipoOperacao() {
        return ultimoTipoOperacao;
    }

    public void setUltimoTipoOperacao(String ultimoTipoOperacao) {
        this.ultimoTipoOperacao = ultimoTipoOperacao;
    }

    public String getDescricaoProcesso() {
        return this.processo != null ? this.processo.toString() : "Não vinculado.";
    }

    public String getDescricaoTipoAndamento(){
        if(this.andamento == null) {
            return "";
        }
        if(this.prazoAndamento != null) {
            return this.prazoAndamento.getSubAndamento().getDescricao();
        }
        return this.andamento.getTipoAndamento().getDescricao();
    }

    public String getDataFinalPrazoFatalEventoFormatado() {
        if (prazoAndamento != null) {
            return prazoAndamento.getDataFinalPrazoFatalFormatado();
        }
        return andamento.getDataFinalPrazoFatalFormatado();
    }
}
