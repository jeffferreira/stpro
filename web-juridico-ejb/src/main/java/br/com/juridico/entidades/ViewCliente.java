package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Jefferson on 09/04/2016.
 */
@Entity
@Table(name = "VW_PROC_CLIENTE")
public class ViewCliente implements IEntity, Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -4695284473871040352L;

	@Id
	@Column(name = "ID")
	private Long id;

	@Column(name = "CLIENTE")
	private String cliente;

	@Column(name = "POSICAO")
	private String posicao;

	@ManyToOne
	@JoinColumn(name = "FK_PROCESSO", referencedColumnName = "ID")
	private ViewProcesso processo;

	@Column(name = "NUMERO_EMPRESA")
	private Long numeroEmpresa;

	@Override
	public Long getId() {
		return id;
	}

	public String getCliente() {
		return cliente;
	}

	public String getPosicao() {
		return posicao;
	}

	public ViewProcesso getProcesso() {
		return processo;
	}

	public Long getNumeroEmpresa() {
		return numeroEmpresa;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder()
				.append(id).append(cliente).toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ViewCliente) {
			final ViewCliente other = (ViewCliente) obj;
			return new EqualsBuilder().append(id, other.id).append(cliente, other.cliente).isEquals();
		} else {
			return false;
		}
	}
}
