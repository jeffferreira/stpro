/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.juridico.entidades;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.NotEmpty;

import br.com.juridico.dao.IEntity;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.cadastros.CausaPedidoValidacaoWrapper;

/**
 *
 * @author Marcos
 */


@Entity
@Audited
@Table(name = "TB_CAUSA_PEDIDO")
@NamedQueries({
		@NamedQuery(name = "CausaPedido.findByid", query = "SELECT t FROM CausaPedido t WHERE t.codigoEmpresa=:codigoEmpresa and t.id = :id"),
		@NamedQuery(name = "CausaPedido.findByDescricao", query = "SELECT t FROM CausaPedido t WHERE t.codigoEmpresa=:codigoEmpresa and t.descricao = :descricao") })
public class CausaPedido implements IEntity, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_CAUSA_PEDIDO", nullable = false)
	private Long id;

	@Column(name = "DS_CAUSA_PEDIDO", length = 200)
	private String descricao;

	@Column(name = "DT_EXCLUSAO_LOGICA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataExclusao;

	@Column(name = "NR_EMPRESA", nullable = false)
	private Long codigoEmpresa;

	public CausaPedido() {
		super();
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Date getDataExclusao() {
		return dataExclusao;
	}

	public void setDataExclusao(Date dataExclusao) {
		this.dataExclusao = dataExclusao;
	}

	public Long getCodigoEmpresa() {
		return codigoEmpresa;
	}

	public void setCodigoEmpresa(Long codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CausaPedido that = (CausaPedido) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
    
	public void validar(boolean existeCausaPedidoCadastrado) throws ValidationException {
        runValidations(buildValidatorFactory().validate(new CausaPedidoValidacaoWrapper(this, existeCausaPedidoCadastrado)));
	}
}
