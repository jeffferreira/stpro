/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.juridico.entidades;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.com.juridico.dao.IEntity;

/**
 *
 * @author Jefferson
 */
@Entity
@Table(name = "TB_UF", uniqueConstraints = { @UniqueConstraint(columnNames = { "SG_UF" }) })
@NamedQueries({ @NamedQuery(name = "Uf.findAll", query = "SELECT t FROM Uf t"),
        @NamedQuery(name = "Uf.findByid", query = "SELECT t FROM Uf t WHERE t.id = :id"),
        @NamedQuery(name = "Uf.findBysigla", query = "SELECT t FROM Uf t WHERE t.sigla = :sigla"),
        @NamedQuery(name = "Uf.findBynome", query = "SELECT t FROM Uf t WHERE t.nome = :nome") })
public class Uf implements IEntity, Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_UF", nullable = false)
    private Long id;

    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "SG_UF", nullable = false, length = 2)
    private String sigla;

    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "NO_UF", nullable = false, length = 45)
    private String nome;

    public Uf() {
    	super();
    }

    public Uf(Long id, String sigla, String nome) {
        this.id = id;
        this.sigla = sigla;
        this.nome = nome;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Uf)) {
            return false;
        }
        Uf other = (Uf) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public String toString() {
        return "module.Uf[ id=" + id + " ]";
    }

}
