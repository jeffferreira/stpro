package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by Jefferson on 20/03/2016.
 */
@Entity
@Audited
@Table(name = "TB_PROCESSO_PESSOA_JURIDICA_CENTRO_CUSTO")
public class ProcessoPessoaJuridicaCentroCusto implements IEntity, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6675997614190659982L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_PROCESSO_PESSOA_JURIDICA_CENTRO_CUSTO", nullable = false)
    private Long id;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @JoinColumn(name = "FK_PARTE", referencedColumnName = "ID_PARTE", nullable = false)
    @ManyToOne(optional = false)
    private Parte parte;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @JoinColumn(name = "FK_PESSOA_JURIDICA_CENTRO_CUSTO", referencedColumnName = "ID_PESSOA_JURIDICA_CENTRO_CUSTO", nullable = false)
    @ManyToOne(optional = false)
    private PessoaJuridicaCentroCusto centroCusto;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @ManyToOne
    @JoinColumn(name = "FK_PROCESSO", referencedColumnName = "ID_PROCESSO", nullable = false)
    private Processo processo;

    @Column(name = "NR_PERCENTUAL_PROCESSO", nullable = false)
    private BigDecimal percentual;

    @Column(name = "NR_EMPRESA", nullable = false)
    private Long codigoEmpresa;

    public ProcessoPessoaJuridicaCentroCusto() {
        super();
    }

    public ProcessoPessoaJuridicaCentroCusto(Long codigoEmpresa){
        this.codigoEmpresa = codigoEmpresa;
        this.percentual = BigDecimal.ZERO;
    }

    public ProcessoPessoaJuridicaCentroCusto(Parte parte, Processo processo, PessoaJuridicaCentroCusto centroCusto, BigDecimal percentual, Long codigoEmpresa) {
        this.parte = parte;
        this.processo = processo;
        this.centroCusto = centroCusto;
        this.percentual = percentual;
        this.codigoEmpresa = codigoEmpresa;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(id).append(centroCusto).toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ProcessoPessoaJuridicaCentroCusto) {
            final ProcessoPessoaJuridicaCentroCusto other = (ProcessoPessoaJuridicaCentroCusto) obj;
            return new EqualsBuilder().append(id, other.id).append(centroCusto, other.centroCusto).isEquals();
        } else {
            return false;
        }
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Parte getParte() {
        return parte;
    }

    public void setParte(Parte parte) {
        this.parte = parte;
    }

    public PessoaJuridicaCentroCusto getCentroCusto() {
        return centroCusto;
    }

    public void setCentroCusto(PessoaJuridicaCentroCusto centroCusto) {
        this.centroCusto = centroCusto;
    }

    public BigDecimal getPercentual() {
        return percentual;
    }

    public void setPercentual(BigDecimal percentual) {
        this.percentual = percentual;
    }

    public Long getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(Long codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }

    public Processo getProcesso() {
        return processo;
    }

    public void setProcesso(Processo processo) {
        this.processo = processo;
    }
}
