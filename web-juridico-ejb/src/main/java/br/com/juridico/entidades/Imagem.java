package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by jefferson on 23/10/2016.
 */
@Entity
@Table(name = "TB_IMAGE")
public class Imagem implements IEntity, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_IMAGE", nullable = false)
    private Long id;

    @Lob
    @Column(name = "BB_IMAGE", columnDefinition = "BLOB")
    private byte[] logo;

    @Column(name = "DS_NOME", length = 255, nullable = true)
    private String nomeArquivo;

    @Column(name = "DS_EXTENSAO", length = 5, nullable = true)
    private String extensaoArquivo;

    public Imagem(){super();}

    public Imagem(byte[] logo, String nomeArquivo, String extensaoArquivo) {
        this.logo = logo;
        this.nomeArquivo = nomeArquivo;
        this.extensaoArquivo = extensaoArquivo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Imagem imagem = (Imagem) o;

        return new EqualsBuilder()
                .append(id, imagem.id)
                .append(nomeArquivo, imagem.nomeArquivo)
                .append(extensaoArquivo, imagem.extensaoArquivo)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(nomeArquivo)
                .append(extensaoArquivo)
                .toHashCode();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getLogo() {
        return logo;
    }

    public void setLogo(byte[] logo) {
        this.logo = logo;
    }

    public String getNomeArquivo() {
        return nomeArquivo;
    }

    public void setNomeArquivo(String nomeArquivo) {
        this.nomeArquivo = nomeArquivo;
    }

    public String getExtensaoArquivo() {
        return extensaoArquivo;
    }

    public void setExtensaoArquivo(String extensaoArquivo) {
        this.extensaoArquivo = extensaoArquivo;
    }
}
