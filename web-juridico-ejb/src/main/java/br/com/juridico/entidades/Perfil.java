package br.com.juridico.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.cadastros.PerfilValidacaoWrapper;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.validator.constraints.NotEmpty;

import com.google.common.collect.Lists;

import br.com.juridico.dao.IEntity;

@Entity
@Audited
@Table(name = "PFL_PERFIL")
@NamedQueries({
		@NamedQuery(name = "Perfil.findByid", query = "SELECT t FROM Perfil t WHERE t.codigoEmpresa=:codigoEmpresa and t.id = :id"),
		@NamedQuery(name = "Perfil.findBySigla", query = "SELECT t FROM Perfil t WHERE t.codigoEmpresa=:codigoEmpresa and t.sigla = :sigla") })
public class Perfil implements IEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 760449817363769978L;
	private static final long ID_ADMIN = 1;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_PERFIL", nullable = false)
	private Long id;

	@NotNull
	@NotEmpty(message = "Sigla: preenchimento obrigatório")
	@Column(name = "DS_SIGLA")
	private String sigla;
	
	@NotNull
	@NotEmpty(message = "Descrição: preenchimento obrigatório")
	@Column(name = "DS_PERFIL")
	private String descricao;

	@NotAudited
	@OneToMany(mappedBy = "perfil")
	private List<PerfilAcesso> acessos = Lists.newArrayList();

	@Column(name = "NR_EMPRESA", nullable = false)
	private Long codigoEmpresa;
	
	@Column(name = "DT_EXCLUSAO_LOGICA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataExclusao;
	
	@Type(type="true_false")
	@Column(name = "FL_VER_TODOS_PROCESSOS", nullable = false)
    private Boolean verTodosProcessos;

	public Perfil(){
		super();
	}

	public Perfil(String sigla, String descricao, Long codigoEmpresa, Boolean verTodosProcessos){
		this.sigla = sigla;
		this.descricao = descricao;
		this.codigoEmpresa = codigoEmpresa;
		this.verTodosProcessos = verTodosProcessos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Perfil other = (Perfil) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Long getCodigoEmpresa() {
		return codigoEmpresa;
	}

	public void setCodigoEmpresa(Long codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}

	public List<PerfilAcesso> getAcessos() {
		return acessos;
	}

	public void setAcessos(List<PerfilAcesso> acessos) {
		this.acessos = acessos;
	}

	public Date getDataExclusao() {
		return dataExclusao;
	}

	public void setDataExclusao(Date dataExclusao) {
		this.dataExclusao = dataExclusao;
	}

	public Boolean getVerTodosProcessos() {
		return verTodosProcessos;
	}

	public void setVerTodosProcessos(Boolean verTodosProcessos) {
		this.verTodosProcessos = verTodosProcessos;
	}

	public boolean isAdmin() {
		return this.id == ID_ADMIN;
	}

	public void validar(boolean existeDescricaoPerfilCadastrado, boolean existeSiglaPerfilCadastrado) throws ValidationException {
		runValidations(buildValidatorFactory().validate(new PerfilValidacaoWrapper(existeDescricaoPerfilCadastrado, existeSiglaPerfilCadastrado)));
	}
}
