package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by Jefferson on 15/06/2016.
 */
@Entity
@Audited
@AuditTable(value="TB_CONTRATO_PESSOA_TIPO_ANDAMENTO_AUD")
@Table(name = "TB_CONTRATO_PESSOA_TIPO_ANDAMENTO")
public class ContratoPessoaTipoAndamento implements IEntity, Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_CONTRATO_PESSOA_TIPO_ANDAMENTO", nullable = false)
    private Long id;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @JoinColumn(name = "FK_CONTRATO_PESSOA_VIGENCIA", referencedColumnName = "ID_CONTRATO_PESSOA_VIGENCIA", nullable = false)
    @ManyToOne(optional = false)
    private ContratoPessoaVigencia contratoPessoaVigencia;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @JoinColumn(name = "FK_TIPO_ANDAMENTO", referencedColumnName = "ID_TIPO_ANDAMENTO", nullable = false)
    @ManyToOne(optional = false)
    private TipoAndamento tipoAndamento;

    @Column(name = "VL_VALOR", nullable = false)
    private BigDecimal valor;

    @Column(name = "NR_EMPRESA", nullable = false)
    private Long codigoEmpresa;

    @Transient
    private transient int rowNumber;

    public ContratoPessoaTipoAndamento(){
        this.valor = BigDecimal.ZERO;
    }

    public ContratoPessoaTipoAndamento(Long codigoEmpresa){
        this.valor = BigDecimal.ZERO;
        this.codigoEmpresa = codigoEmpresa;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ContratoPessoaVigencia getContratoPessoaVigencia() {
        return contratoPessoaVigencia;
    }

    public void setContratoPessoaVigencia(ContratoPessoaVigencia contratoPessoaVigencia) {
        this.contratoPessoaVigencia = contratoPessoaVigencia;
    }

    public TipoAndamento getTipoAndamento() {
        return tipoAndamento;
    }

    public void setTipoAndamento(TipoAndamento tipoAndamento) {
        this.tipoAndamento = tipoAndamento;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public Long getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(Long codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }

    public int getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(int rowNumber) {
        this.rowNumber = rowNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ContratoPessoaTipoAndamento that = (ContratoPessoaTipoAndamento) o;

        if (contratoPessoaVigencia != null ? !contratoPessoaVigencia.equals(that.contratoPessoaVigencia) : that.contratoPessoaVigencia != null) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (tipoAndamento != null ? !tipoAndamento.equals(that.tipoAndamento) : that.tipoAndamento != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (contratoPessoaVigencia != null ? contratoPessoaVigencia.hashCode() : 0);
        result = 31 * result + (tipoAndamento != null ? tipoAndamento.hashCode() : 0);
        return result;
    }
}
