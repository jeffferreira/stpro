package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.cadastros.GrauRiscoValidacaoWrapper;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Marcos
 */
@Entity
@Audited
@Table(name = "TB_GRAU_RISCO")
@NamedQueries({
		@NamedQuery(name = "GrauRisco.findByid", query = "SELECT t FROM GrauRisco t WHERE t.codigoEmpresa=:codigoEmpresa and t.id = :id"),
		@NamedQuery(name = "GrauRisco.findByDescricao", query = "SELECT t FROM GrauRisco t WHERE t.codigoEmpresa=:codigoEmpresa and t.descricao = :descricao") })
public class GrauRisco implements IEntity, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_GRAU_RISCO", nullable = false)
	private Long id;

	@Column(name = "DS_GRAU_RISCO", nullable = false, length = 200)
	private String descricao;

	@Column(name = "NR_EMPRESA", nullable = false)
	private Long codigoEmpresa;

	@Column(name = "DT_EXCLUSAO_LOGICA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataExclusao;

	@NotAudited
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "grauRisco")
	private List<Processo> processos;

	public GrauRisco() {
		super();
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<Processo> getProcessos() {
		return processos;
	}

	public void setProcessos(List<Processo> processos) {
		this.processos = processos;
	}

	public Date getDataExclusao() {
		return dataExclusao;
	}

	public void setDataExclusao(Date dataExclusao) {
		this.dataExclusao = dataExclusao;
	}

	@Override
	public String toString() {
		return "GrauRisco[ id=" + id + " ]";
	}

	public Long getCodigoEmpresa() {
		return codigoEmpresa;
	}

	public void setCodigoEmpresa(Long codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof GrauRisco)) {
			return false;
		}
		GrauRisco other = (GrauRisco) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	public void validar(boolean existeGrauRiscoCadastrado) throws ValidationException {
		runValidations(buildValidatorFactory().validate(new GrauRiscoValidacaoWrapper(this, existeGrauRiscoCadastrado)));
	}
}
