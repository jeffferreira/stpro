package br.com.juridico.entidades;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import br.com.juridico.dao.IEntity;

/**
 * Created by jeffersonl2009_00 on 27/06/2016.
 */
@Entity
@Table(name = "TB_PARAMETRO", uniqueConstraints = { @UniqueConstraint(columnNames = { "DS_SIGLA" }) })
public class Parametro implements IEntity, Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -3877949698897370890L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_PARAMETRO", nullable = false)
    private Long id;

    @Column(name = "DS_SIGLA", nullable = false)
    private String sigla;

    @Column(name = "DS_DESCRICAO", nullable = false)
    private String descricao;

    @Column(name = "NR_EMPRESA", nullable = false)
    private Long codigoEmpresa;

    public Parametro(){
        super();
    }

    public Parametro(String sigla, String descricao, Long codigoEmpresa) {
        this.sigla = sigla;
        this.descricao = descricao;
        this.codigoEmpresa = codigoEmpresa;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Long getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(Long codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Vara)) {
            return false;
        }
        Parametro other = (Parametro) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

}
