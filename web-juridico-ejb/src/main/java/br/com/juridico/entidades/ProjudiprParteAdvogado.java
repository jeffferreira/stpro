package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author Jefferson
 */
@Entity
@Table(name = "projudipr_parte_advogado")
public class ProjudiprParteAdvogado implements IEntity, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_PARTE_ADVOGADO", nullable = false)
	private Long id;

	@Column(name = "DS_OAB_NOME", nullable = false)
	private String oabNome;

	@ManyToOne(optional = false)
	@JoinColumn(name = "FK_PROJUDI_PARTE", referencedColumnName = "ID_PARTE", nullable = false)
	private ProjudiprParte parte;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOabNome() {
		return oabNome;
	}

	public void setOabNome(String oabNome) {
		this.oabNome = oabNome;
	}

	public ProjudiprParte getParte() {
		return parte;
	}

	public void setParte(ProjudiprParte parte) {
		this.parte = parte;
	}
}
