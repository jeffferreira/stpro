package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Jefferson on 30/07/2016.
 */
@Entity
@Table(name = "PFL_MENU")
@NamedQuery(name = "Menu.findByDescricao", query = "SELECT t FROM Menu t WHERE t.descricao = :descricao")
public class Menu implements IEntity, Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 3295044772293782042L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_MENU", nullable = false)
    private Long id;

    @Column(name = "DS_MENU", nullable = false)
    private String descricao;

    public Menu(){
        super();
    }

    public Menu(Long id, String descricao){
        this.id = id;
        this.descricao = descricao;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
