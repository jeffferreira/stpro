package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.cadastros.ProjudiprLoginValidacaoWrapper;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table(name = "projudipr_login")
public class ProjudiprLogin implements IEntity, Serializable {

    /**
	 *
	 */
    private static final long serialVersionUID = -6732903233144666231L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_LOGIN", nullable = false)
    private Long id;

    @Size(max = 20, message = "Login: tamanho máximo permitido 20")
    @Column(name = "DS_LOGIN", length = 20)
    private String login;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "BB_SENHA", columnDefinition = "BLOB")
    private byte[] senha;

    @NotNull
    @Column(name = "NR_EMPRESA", nullable = false)
    private Long codigoEmpresa;

    public ProjudiprLogin() {
        super();
    }

    public ProjudiprLogin(Long codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }

    public ProjudiprLogin(String login, byte[] senha, Long codigoEmpresa) {
        this.login = login;
        this.senha = senha;
        this.codigoEmpresa = codigoEmpresa;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public byte[] getSenha() {
        return senha;
    }

    public void setSenha(byte[] senha) {
        this.senha = senha;
    }

    public Long getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(Long codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }

    public void validar(boolean existeLoginCadastrado) throws ValidationException {
        runValidations(buildValidatorFactory().validate(new ProjudiprLoginValidacaoWrapper(existeLoginCadastrado)));
    }
}
