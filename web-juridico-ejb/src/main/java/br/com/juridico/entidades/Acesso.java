package br.com.juridico.entidades;

import java.io.Serializable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import br.com.juridico.dao.IEntity;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "PFL_ACESSO")
public class Acesso implements IEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_ACESSO", nullable = false)
	private Long id;

	@JoinColumn(name = "FK_MENU", referencedColumnName = "ID_MENU", nullable = false)
	@ManyToOne(optional = false)
	private Menu menu;

	@Column(name = "DS_FUNCIONALIDADE", nullable = false)
	private String funcionalidade;

	@Column(name = "DS_ACESSO", nullable = false)
	private String descricao;

	@Column(name = "DS_SUB_TITULO", nullable = true)
	private String subTitulo;

	@Column(name = "FL_OPERACAO", nullable = false)
	private Integer flagOperacao = 0;

	@Column(name = "ACESSO_PAI", nullable = true)
	private Long acessoPai;

	public Acesso(){super();}

	public Acesso(Menu menu, String funcionalidade, String descricao) {
		this.menu = menu;
		this.funcionalidade = funcionalidade;
		this.descricao = descricao;
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getFuncionalidade() {
		return funcionalidade;
	}

	public void setFuncionalidade(String funcionalidade) {
		this.funcionalidade = funcionalidade;
	}

	public Menu getMenu() {
		return menu;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}

	public Integer getFlagOperacao() {
		return flagOperacao;
	}

	public void setFlagOperacao(Integer flagOperacao) {
		this.flagOperacao = flagOperacao;
	}

	public Long getAcessoPai() {
		return acessoPai;
	}

	public void setAcessoPai(Long acessoPai) {
		this.acessoPai = acessoPai;
	}

	public String getSubTitulo() {
		return subTitulo;
	}

	public void setSubTitulo(String subTitulo) {
		this.subTitulo = subTitulo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Acesso other = (Acesso) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
