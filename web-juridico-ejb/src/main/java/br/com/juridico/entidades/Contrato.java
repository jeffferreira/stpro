package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.contratos.ConfiguracaoContratoValidacaoWrapper;
import com.google.common.collect.Lists;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by jefferson.ferreira on 02/03/2017.
 */
@Entity
@Table(name = "CT_CONTRATO")
@NamedQueries({
        @NamedQuery(name = "Contrato.findByDescricao", query = "SELECT t FROM Contrato t WHERE t.codigoEmpresa=:codigoEmpresa and t.descricao = :descricao") })
public class Contrato implements IEntity, Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -8522371972484003577L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_CONTRATO", nullable = false)
    private Long id;

    @Column(name = "DS_CONTRATO", nullable = false)
    private String descricao;

    @Column(name = "NR_EMPRESA", nullable = false)
    private Long codigoEmpresa;

    @Column(name = "DT_EXCLUSAO_LOGICA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataExclusao;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "contrato", orphanRemoval = true)
    private List<ContratoSubContrato> subContratos = Lists.newArrayList();


    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Long getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(Long codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }

    public Date getDataExclusao() {
        return dataExclusao;
    }

    public void setDataExclusao(Date dataExclusao) {
        this.dataExclusao = dataExclusao;
    }

    public List<ContratoSubContrato> getSubContratos() {
        for(Iterator<ContratoSubContrato> i = subContratos.iterator(); i.hasNext();) {
            ContratoSubContrato sub = i.next();
            if(sub.getContratoSubContrato() != null) {
                i.remove();
            }
        }
        return subContratos;
    }

    public void setSubContratos(List<ContratoSubContrato> subContratos) {
        this.subContratos = subContratos;
    }

    public void addSubContrato(ContratoSubContrato subContrato){
        if(subContrato != null) {
            this.subContratos.add(subContrato);
        }
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(id).append(descricao).toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Contrato) {
            final Contrato other = (Contrato) obj;
            return new EqualsBuilder().append(id, other.id).append(descricao, other.descricao).isEquals();
        } else {
            return false;
        }
    }

    /**
     *
     * @param existeContratoCadastrado
     * @throws ValidationException
     */
    public void validar(boolean existeContratoCadastrado) throws ValidationException {
        runValidations(buildValidatorFactory().validate(new ConfiguracaoContratoValidacaoWrapper(existeContratoCadastrado)));
    }

    @Override
    public String toString() {
        return descricao;
    }
}
