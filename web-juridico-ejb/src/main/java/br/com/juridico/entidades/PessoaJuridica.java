package br.com.juridico.entidades;

import br.com.caelum.stella.format.CNPJFormatter;
import br.com.caelum.stella.format.Formatter;
import br.com.juridico.dao.IEntity;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.pessoas.PessoaExistenteValidacaoWrapper;
import br.com.juridico.validation.pessoas.PessoaJuridicaValidacaoWrapper;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Jefferson
 */
@Entity
@Audited
@Table(name = "TB_PESSOA_JURIDICA")
@NamedQueries({
        @NamedQuery(name = "PessoaJuridica.findBycnpj", query = "SELECT t FROM PessoaJuridica t WHERE t.codigoEmpresa=:codigoEmpresa and t.cnpj = :cnpj and t.id = :id"),
        @NamedQuery(name = "PessoaJuridica.findByresponsavel", query = "SELECT t FROM PessoaJuridica t WHERE t.codigoEmpresa=:codigoEmpresa and t.responsavel = :responsavel"),
        @NamedQuery(name = "PessoaJuridica.findBydataFundacao", query = "SELECT t FROM PessoaJuridica t WHERE t.codigoEmpresa=:codigoEmpresa and t.dataFundacao = :dataFundacao"),
        @NamedQuery(name = "PessoaJuridica.findBytipoEmpresa", query = "SELECT t FROM PessoaJuridica t WHERE t.codigoEmpresa=:codigoEmpresa and t.tipoEmpresa = :tipoEmpresa"),
        @NamedQuery(name = "PessoaJuridica.findByinscricaoMunicipal", query = "SELECT t FROM PessoaJuridica t WHERE t.codigoEmpresa=:codigoEmpresa and t.inscricaoMunicipal = :inscricaoMunicipal"),
        @NamedQuery(name = "PessoaJuridica.findByinscricaoEstadual", query = "SELECT t FROM PessoaJuridica t WHERE t.codigoEmpresa=:codigoEmpresa and t.inscricaoEstadual = :inscricaoEstadual"),
        @NamedQuery(name = "PessoaJuridica.findByobservacao", query = "SELECT t FROM PessoaJuridica t WHERE t.codigoEmpresa=:codigoEmpresa and t.observacao = :observacao") })
public class PessoaJuridica implements IEntity, Serializable {


    private static final long serialVersionUID = -8711839999315955418L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_PESSOA_JURIDICA", nullable = false)
    private Long id;

    @Column(name = "DS_CNPJ", length = 14)
    private String cnpj;

    @Column(name = "DS_RESPONSAVEL", length = 100)
    private String responsavel;

    @Column(name = "DT_FUNDACAO")
    @Temporal(TemporalType.DATE)
    private Date dataFundacao;

    @Column(name = "DS_TIPO_EMPRESA")
    private String tipoEmpresa;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @ManyToOne
    @JoinColumn(name = "FK_RAMO_ATIVIDADE", referencedColumnName = "ID_RAMO_ATIVIDADE")
    private RamoAtividade ramoAtividade;

    @Column(name = "DS_INSCRICAO_MUNICIPAL", length = 50)
    private String inscricaoMunicipal;

    @Column(name = "DS_INSCRICAO_ESTADUAL", length = 50)
    private String inscricaoEstadual;

    @Column(name = "DS_OBSERVACOES", length = 4000)
    private String observacao;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @JoinColumn(name = "FK_PESSOA", referencedColumnName = "ID_PESSOA", nullable = false)
    @ManyToOne(cascade = CascadeType.ALL, optional = false)
    private Pessoa pessoa;

    @Column(name = "NR_EMPRESA", nullable = false)
    private Long codigoEmpresa;

    @Column(name = "DT_EXCLUSAO_LOGICA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataExclusao;

    @NotAudited
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pessoaJuridica", orphanRemoval = true)
    private Set<PessoaJuridicaCentroCusto> pessoasJuridicasCentrosCustos = Sets.newHashSet();

    @NotAudited
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pessoaJuridica", orphanRemoval = true)
    private List<PessoaJuridicaResponsavel> pessoasJuridicasResponsaveis = Lists.newArrayList();

    public PessoaJuridica() {
        super();
    }

    public PessoaJuridica(Long id, String cnpj, String responsavel) {
        this.id = id;
        this.cnpj = cnpj;
        this.responsavel = responsavel;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(id).toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof PessoaJuridica) {
            final PessoaJuridica other = (PessoaJuridica) obj;
            return new EqualsBuilder().append(id, other.id).isEquals();
        } else {
            return false;
        }
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getResponsavel() {
        return responsavel;
    }

    public void setResponsavel(String responsavel) {
        this.responsavel = responsavel;
    }

    public Date getDataFundacao() {
        return dataFundacao;
    }

    public void setDataFundacao(Date dataFundacao) {
        this.dataFundacao = dataFundacao;
    }

    public String getTipoEmpresa() {
        return tipoEmpresa;
    }

    public void setTipoEmpresa(String tipoEmpresa) {
        this.tipoEmpresa = tipoEmpresa;
    }

    public String getInscricaoMunicipal() {
        return inscricaoMunicipal;
    }

    public void setInscricaoMunicipal(String inscricaoMunicipal) {
        this.inscricaoMunicipal = inscricaoMunicipal;
    }

    public String getInscricaoEstadual() {
        return inscricaoEstadual;
    }

    public void setInscricaoEstadual(String inscricaoEstadual) {
        this.inscricaoEstadual = inscricaoEstadual;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }



    public Set<PessoaJuridicaCentroCusto> getPessoasJuridicasCentrosCustos() {
        return pessoasJuridicasCentrosCustos;
    }

    public void setPessoasJuridicasCentrosCustos(Set<PessoaJuridicaCentroCusto> pessoasJuridicasCentrosCustos) {
        this.pessoasJuridicasCentrosCustos.clear();
        this.pessoasJuridicasCentrosCustos.addAll(pessoasJuridicasCentrosCustos);
    }

    public Long getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(Long codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }

    /**
     *
     * @throws ValidationException
     */
    public void validar() throws ValidationException {
        runValidations(buildValidatorFactory().validate(new PessoaJuridicaValidacaoWrapper(this)));
    }

    /**
     *
     * @param cnpjJaExiste
     * @param nomeJaExiste
     * @throws ValidationException
     */
    public void validarPessoaJaCadastrada(boolean cnpjJaExiste, boolean nomeJaExiste) throws ValidationException {
        runValidations(buildValidatorFactory().validate(new PessoaExistenteValidacaoWrapper(false, cnpjJaExiste, nomeJaExiste)));
    }

    @Override
    public String toString() {
        return "PessoaJuridica[ id=" + id + " ]";
    }

    public Date getDataExclusao() {
        return dataExclusao;
    }

    public void setDataExclusao(Date dataExclusao) {
        this.dataExclusao = dataExclusao;
    }

    public List<PessoaJuridicaResponsavel> getPessoasJuridicasResponsaveis() {
        return pessoasJuridicasResponsaveis;
    }

    public void setPessoasJuridicasResponsaveis(
            List<PessoaJuridicaResponsavel> pessoasJuridicasResponsaveis) {
        this.pessoasJuridicasResponsaveis = pessoasJuridicasResponsaveis;
    }

    public void addPessoasJuridicasResponsaveis(PessoaJuridicaResponsavel responsavel){
        if(responsavel != null)
            this.pessoasJuridicasResponsaveis.add(responsavel);
    }

    public String getEmails() {
        return getEmails(pessoa);
    }

    public String getTelefones() {
        return getTelefones(pessoa);
    }

    public String getResponsaveis() {
        StringBuilder sb = new StringBuilder();
        for (PessoaJuridicaResponsavel responsavel : this.pessoasJuridicasResponsaveis) {
            sb.append(responsavel.getPessoaFisica().getPessoa().getDescricao()).append("; ");
        }
        removeUltimoCaractereVazio(sb);
        return sb.toString();
    }

    public String getCnpjFormatado(){
        Formatter formatter = new CNPJFormatter();
        return formatter.format(cnpj);
    }

    public RamoAtividade getRamoAtividade() {
        return ramoAtividade;
    }

    public void setRamoAtividade(RamoAtividade ramoAtividade) {
        this.ramoAtividade = ramoAtividade;
    }

    public String getNomePessoa(){
        if(pessoa != null && !Strings.isNullOrEmpty(pessoa.getDescricao())){
            return this.pessoa.getDescricao();
        }
        return "";
    }

}
