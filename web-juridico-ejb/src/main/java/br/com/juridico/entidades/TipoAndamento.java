package br.com.juridico.entidades;

import br.com.juridico.business.TipoAndamentoBusiness;
import br.com.juridico.dao.IEntity;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.cadastros.TipoAndamentoValidacaoWrapper;
import com.google.common.collect.Lists;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Marcos
 */
@Entity
@Audited
@Table(name = "TB_TIPO_ANDAMENTO")
@NamedQueries({
		@NamedQuery(name = "TipoAndamento.findByid", query = "SELECT t FROM TipoAndamento t WHERE t.codigoEmpresa=:codigoEmpresa and t.id = :id"),
		@NamedQuery(name = "TipoAndamento.findByDescricao", query = "SELECT t FROM TipoAndamento t WHERE t.codigoEmpresa=:codigoEmpresa and t.descricao = :descricao") })
public class TipoAndamento implements IEntity, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_TIPO_ANDAMENTO", nullable = false)
	private Long id;

	@NotNull
	@NotEmpty(message = "Descrição: preenchimento obrigatório")
	@Column(name = "DS_TIPO_ANDAMENTO", length = 200)
	private String descricao;

	@Type(type="true_false")
	@Column(name = "FL_CRIA_EVENTO", nullable = false)
	private Boolean criaEventoFlag;

	@Column(name = "NR_DIAS_PRAZO_SEGURANCA", nullable = false)
	private Long diasPrazoSeguranca;

	@Column(name = "NR_EMPRESA", nullable = false)
	private Long codigoEmpresa;

	@Column(name = "DT_EXCLUSAO_LOGICA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataExclusao;

	@NotAudited
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "tipoAndamento", orphanRemoval = true)
	private List<TipoAndamentoSubAndamento> subAndamentos = Lists.newArrayList();

	public TipoAndamento(){
		super();
	}

	public TipoAndamento(Long id){
		this.id = id;
	}

    public TipoAndamento(String descricao, Boolean criaEventoFlag, Long diasPrazoSeguranca, Long codigoEmpresa) {
        this.descricao = descricao;
        this.criaEventoFlag = criaEventoFlag;
        this.diasPrazoSeguranca = diasPrazoSeguranca;
        this.codigoEmpresa = codigoEmpresa;
    }

    @Override
	public int hashCode() {
		return new HashCodeBuilder()
				.append(id).append(criaEventoFlag).toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof TipoAndamento) {
			final TipoAndamento other = (TipoAndamento) obj;
			return new EqualsBuilder().append(id, other.id).append(criaEventoFlag, other.criaEventoFlag).isEquals();
		} else {
			return false;
		}
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Boolean getCriaEventoFlag() {
		return criaEventoFlag;
	}

	public void setCriaEventoFlag(Boolean criaEventoFlag) {
		this.criaEventoFlag = criaEventoFlag;
	}

	public Date getDataExclusao() {
		return dataExclusao;
	}

	public void setDataExclusao(Date dataExclusao) {
		this.dataExclusao = dataExclusao;
	}

    public Long getDiasPrazoSeguranca() {
        return diasPrazoSeguranca;
    }

    public void setDiasPrazoSeguranca(Long diasPrazoSeguranca) {
        this.diasPrazoSeguranca = diasPrazoSeguranca;
    }

    public Long getCodigoEmpresa() {
		return codigoEmpresa;
	}

	public void setCodigoEmpresa(Long codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}

	public boolean isPodeAlterarExcluir() {
		return !TipoAndamentoBusiness.isAudienciaConciliacao(this) && !TipoAndamentoBusiness.isAudienciaOitiva(this) && !TipoAndamentoBusiness.isAudienciaInstrucao(this);
	}

	public List<TipoAndamentoSubAndamento> getSubAndamentos() {
		return subAndamentos;
	}

	public void setSubAndamentos(List<TipoAndamentoSubAndamento> subAndamentos) {
		this.subAndamentos = subAndamentos;
	}

	public void addSubAndamento(TipoAndamentoSubAndamento tipoAndamentoSubAndamento){
		subAndamentos.add(tipoAndamentoSubAndamento);
	}

	public void validar(boolean existeTipoAndamentoCadastrado) throws ValidationException {
		runValidations(buildValidatorFactory().validate(new TipoAndamentoValidacaoWrapper(existeTipoAndamentoCadastrado)));
	}

	@Override
	public String toString() {
		return "TipoAndamento[ descricao=" + descricao + " ]";
	}

    public boolean isTipoAndamentoUtilizadoSistema(){
        return TipoAndamentoObrigatorio.fromTiposAndamentoObrigatorio(descricao) != null;
    }

    /**
	 *
	 */
	enum TipoAndamentoObrigatorio {
        AUDIENCIA_CONCILIACAO("Audiência de Conciliação"), AUDIENCIA_OITIVA("Audiência Oitiva"), AUDIENCIA_INSTRUCAO("Audiência de Instrução");

        private String tipoAndamentoObg;

        TipoAndamentoObrigatorio(String tipoAndamentoObrigatorio){
			this.tipoAndamentoObg = tipoAndamentoObrigatorio;
		}

		public static TipoAndamentoObrigatorio fromTiposAndamentoObrigatorio(String descricao) {
			for (TipoAndamentoObrigatorio tipoAndamentoObrigatorio : values()) {
				if(tipoAndamentoObrigatorio.getTipoAndamentoObg().equals(descricao)){
					return tipoAndamentoObrigatorio;
				}
			}
			return null;
		}

		public String getTipoAndamentoObg() {
			return tipoAndamentoObg;
		}
	}
}
