/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author Jefferson
 */
@Entity
@Table(name = "TB_PESSOA_CONTATO")
@NamedQuery(name = "PessoaContato.findByid", query = "SELECT t FROM PessoaContato t WHERE t.codigoEmpresa=:codigoEmpresa and t.id = :id")
public class PessoaContato implements IEntity, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_PESSOA_CONTATO", nullable = false)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "FK_PESSOA", referencedColumnName = "ID_PESSOA", nullable = false)
	private Pessoa pessoa;

	@JoinColumn(name = "FK_CONTATO", referencedColumnName = "ID_CONTATO", nullable = false)
	@ManyToOne(cascade = CascadeType.ALL, optional = false)
	private Contato contato;

	@Column(name = "NR_EMPRESA", nullable = false)
	private Long codigoEmpresa;

	@Transient
	private transient int rowNumber;

	public PessoaContato() {
		super();
	}

	public PessoaContato(Long codigoEmpresa) {
        this.contato = new Contato(codigoEmpresa);
        this.codigoEmpresa = codigoEmpresa;
	}

	public PessoaContato(Contato contato, Pessoa pessoa, Long codigoEmpresa) {
		this.contato = contato;
		this.pessoa = pessoa;
		this.codigoEmpresa = codigoEmpresa;
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public Contato getContato() {
		return contato;
	}

	public void setContato(Contato contato) {
		this.contato = contato;
	}

	public int getRowNumber() {
		return rowNumber;
	}

	public void setRowNumber(int rowNumber) {
		this.rowNumber = rowNumber;
	}

	@Override
	public String toString() {
		return "PessoaContato[ id=" + id + " ]";
	}

	public Long getCodigoEmpresa() {
		return codigoEmpresa;
	}

	public void setCodigoEmpresa(Long codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}

}
