package br.com.juridico.entidades;

import java.io.Serializable;

/**
 * 
 * @author Jefferson Ferreira
 *
 */
public class UserPrimeiroAcesso implements Serializable {

    private static final long serialVersionUID = 9167015146220421477L;

    private Long id;

    private String name;

    private String login;

    private String ip;

    private Long codEmpresa;


    /**
     *
     */
    public UserPrimeiroAcesso() {
    	super();
    }

    /**
     *
     * @param id
     * @param name
     */
    public UserPrimeiroAcesso(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Long getCodEmpresa() {
        return codEmpresa;
    }

    public void setCodEmpresa(Long codEmpresa) {
        this.codEmpresa = codEmpresa;
    }

    @Override
    public String toString() {
        return "User [id=" + id + ", login=" + login + "]";
    }
}
