/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.processo.CustaValidacaoWrapper;
import com.google.common.collect.Lists;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Marcos.Melo
 */
@Entity
@Audited
@Table(name = "TB_CUSTA")
public class Custa implements IEntity, Serializable {

	private static final String FL_CLIENTE = "C";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_CUSTA", nullable = false)
	private Long id;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@JoinColumn(name = "FK_PROCESSO", referencedColumnName = "ID_PROCESSO", nullable = false)
	@ManyToOne(optional = false)
	private Processo processo;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@JoinColumn(name = "FK_TIPO_CUSTO", referencedColumnName = "ID_TIPO_CUSTO", nullable = false)
	@ManyToOne(optional = false)
	private TipoCusto tipoCusto;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@JoinColumn(name = "FK_FORMA_PAGAMENTO", referencedColumnName = "ID_FORMA_PAGAMENTO", nullable = false)
	@ManyToOne(optional = false)
	private FormaPagamento formaPagamento;
	
	@Column(name = "DS_CUSTA", nullable = true, length = 200)
	private String descricao;

    @Column(name = "VL_CUSTA", nullable = false)
    private BigDecimal valorCusta;
	
	@Column(name = "DT_PAGAMENTO", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataPagamento;

	@Column(name = "FL_RESPONSAVEL_PAGAMENTO", nullable = false, length = 1)
	private String responsavelPagamento;

	@Column(name = "NR_EMPRESA", nullable = false)
	private Long codigoEmpresa;

	@NotAudited
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "custa", orphanRemoval = true)
	private List<CustaDocumento> custaDocumentos = Lists.newArrayList();
	
	public Custa() {
		this.responsavelPagamento = FL_CLIENTE;
	}

	public Custa(Long id) {
		this.id = id;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;

		if (o == null || getClass() != o.getClass()) return false;

		Custa custa = (Custa) o;

		return new EqualsBuilder()
				.append(id, custa.id)
				.append(processo, custa.processo)
				.append(tipoCusto, custa.tipoCusto)
				.append(formaPagamento, custa.formaPagamento)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(id)
				.append(processo)
				.append(tipoCusto)
				.append(formaPagamento)
				.toHashCode();
	}

	@Override
	public String toString() {
		return "Custa[ id=" + id + " ]";
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Processo getProcesso() {
		return processo;
	}

	public void setProcesso(Processo processo) {
		this.processo = processo;
	}

	public TipoCusto getTipoCusto() {
		return tipoCusto;
	}

	public void setTipoCusto(TipoCusto tipoCusto) {
		this.tipoCusto = tipoCusto;
	}

	public FormaPagamento getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(FormaPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public BigDecimal getValorCusta() {
		return valorCusta;
	}

	public void setValorCusta(BigDecimal valorCusta) {
		this.valorCusta = valorCusta;
	}

	public Date getDataPagamento() {
		return dataPagamento;
	}

	public void setDataPagamento(Date dataPagamento) {
		this.dataPagamento = dataPagamento;
	}

	public String getResponsavelPagamento() {
		return responsavelPagamento;
	}

	public void setResponsavelPagamento(String responsavelPagamento) {
		this.responsavelPagamento = responsavelPagamento;
	}

	public Long getCodigoEmpresa() {
		return codigoEmpresa;
	}

	public void setCodigoEmpresa(Long codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}

	public List<CustaDocumento> getCustaDocumentos() {
		return custaDocumentos;
	}

	public void setCustaDocumentos(List<CustaDocumento> custaDocumentos) {
		this.custaDocumentos = custaDocumentos;
	}

    public void addCustaDocumento(CustaDocumento custaDocumento) {
    	this.custaDocumentos.add(custaDocumento);
    }
    
    public void removeCustaDocumentos(int rowNumber){
        this.custaDocumentos.remove(rowNumber);
    }

	public void validar() throws ValidationException {
		runValidations(buildValidatorFactory().validate(new CustaValidacaoWrapper(this)));
	}

}
