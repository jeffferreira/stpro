package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import br.com.juridico.enums.EventoColorTypes;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.cadastros.StatusAgendaValidacaoWrapper;
import com.google.common.base.Strings;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "TB_STATUS_AGENDA")
@NamedQueries({
	@NamedQuery(name = "StatusAgenda.findByid", query = "SELECT t FROM StatusAgenda t WHERE t.codigoEmpresa=:codigoEmpresa and t.id = :id"),
	@NamedQuery(name = "StatusAgenda.findByDescricao", query = "SELECT t FROM StatusAgenda t WHERE t.codigoEmpresa=:codigoEmpresa and t.descricao = :descricao") })
public class StatusAgenda implements IEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7827246463374344019L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_STATUS_AGENDA", nullable = false)
	private Long id;
	
	@NotNull
	@NotEmpty(message = "Descrição: preenchimento obrigatório")
	@Column(name = "DS_STATUS_AGENDA", length = 100)
	private String descricao;
	
	@Column(name = "DS_STYLE_COLOR", length = 45)
	private String styleColor;
	
	@Column(name = "NR_EMPRESA", nullable = false)
    private Long codigoEmpresa;

	@Type(type="true_false")
	@Column(name = "FL_STATUS_A_CUMPRIR_CONTROLADORIA")
	private Boolean statusAcumprirPassaControladoria;

    @Column(name = "DT_EXCLUSAO_LOGICA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataExclusao;
    
    public StatusAgenda() {
		super();
	}

	public StatusAgenda(String descricao, String styleColor, Boolean statusAcumprirPassaControladoria, Long codigoEmpresa) {
		this.descricao = descricao;
		this.styleColor = styleColor;
		this.statusAcumprirPassaControladoria = statusAcumprirPassaControladoria;
		this.codigoEmpresa = codigoEmpresa;
	}

	public StatusAgenda(Long id, String descricao, Long codigoEmpresa) {
		this.id = id;
		this.descricao = descricao;
		this.codigoEmpresa = codigoEmpresa;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder()
				.append(id).toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof StatusAgenda) {
			final StatusAgenda other = (StatusAgenda) obj;
			return new EqualsBuilder().append(id, other.id).isEquals();
		} else {
			return false;
		}
	}

    @Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Long getCodigoEmpresa() {
		return codigoEmpresa;
	}

	public void setCodigoEmpresa(Long codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}

	public Date getDataExclusao() {
		return dataExclusao;
	}

	public void setDataExclusao(Date dataExclusao) {
		this.dataExclusao = dataExclusao;
	}
	
	public String getStyleColor() {
		return styleColor;
	}

	public void setStyleColor(String styleColor) {
		this.styleColor = styleColor;
	}

	public Boolean getStatusAcumprirPassaControladoria() {
		if(statusAcumprirPassaControladoria == null) {
			return Boolean.FALSE;
		}
		return statusAcumprirPassaControladoria;
	}

	public void setStatusAcumprirPassaControladoria(Boolean statusAcumprirPassaControladoria) {
		this.statusAcumprirPassaControladoria = statusAcumprirPassaControladoria;
	}

	public void validar(boolean existeStatusAgendaCadastrado) throws ValidationException {
		runValidations(buildValidatorFactory().validate(new StatusAgendaValidacaoWrapper(existeStatusAgendaCadastrado)));
	}

	public boolean isStatusUtilizadoSistema(){
		return TipoStatus.fromStatus(descricao) != null;
	}

	public boolean isStatusAcumprir(){
		return !Strings.isNullOrEmpty(descricao) && TipoStatus.ACUMPRIR.getStatus().equals(descricao);
	}

	public boolean isStatusRejeitado(){
		return !Strings.isNullOrEmpty(descricao) && TipoStatus.REJEITADO.getStatus().equals(descricao);
	}

	public String getColorType () {
		if(Strings.isNullOrEmpty(styleColor)) {
			return "";
		}
		return EventoColorTypes.fromTextColor(styleColor).getValue();
	}

	/**
	 *
	 */
	enum TipoStatus {
		CUMPRIDO("Cumprido"), ACUMPRIR("À cumprir"), CANCELADO("Cancelado"), REJEITADO("Rejeitado");

		private String status;

		TipoStatus(String status){
			this.status = status;
		}

		public static TipoStatus fromStatus(String descricao) {
			for (TipoStatus tipoStatus : values()) {
				if(tipoStatus.getStatus().equals(descricao)){
					return tipoStatus;
				}
			}
			return null;
		}

		public String getStatus() {
			return status;
		}
	}
    
}
