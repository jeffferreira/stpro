package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.cadastros.EmpresaValidacaoWrapper;
import com.google.common.collect.Lists;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by jefferson on 22/10/2016.
 */
@Entity
@Table(name = "ADMIN_EMPRESA")
public class Empresa implements IEntity, Serializable {

    private static final String CONF_CONTROLADORIA = "CONF_CONTROLADORIA";
    private static final String CONF_CONTRATO = "CONF_CONTRATO";
    private static final String CONF_FINANCEIRO = "CONF_FINANCEIRO";
    private static final String CONF_ROBOT = "CONF_ROBOT";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_EMPRESA", nullable = false)
    private Long id;

    @Column(name = "DS_EMPRESA", length = 255, nullable = false)
    private String descricao;

    @Column(name = "DS_RAZAO_SOCIAL", length = 255, nullable = false)
    private String razaoSocial;

    @Column(name = "DS_CNPJ", length = 18, nullable = false)
    private String cnpj;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DT_CADASTRO")
    private Date dataCadastro;

    @Column(name = "DS_LOGRADOURO", length = 255, nullable = false)
    private String logradouro;

    @Column(name = "NR_ENDERECO", length = 10, nullable = false)
    private Integer numero;

    @Column(name = "DS_COMPLEMENTO", length = 255, nullable = true)
    private String complemento;

    @Column(name = "DS_BAIRRO", length = 100, nullable = true)
    private String bairro;

    @Column(name = "DS_CIDADE", length = 100, nullable = true)
    private String cidade;

    @Column(name = "DS_LATITUDE_LONGITUDE", length = 45, nullable = true)
    private String latitudeLongitudeCentro;

    @Column(name = "DS_UF", length = 2, nullable = true)
    private String uf;

    @ManyToOne
    @JoinColumn(name = "FK_IMAGE", referencedColumnName = "ID_IMAGE")
    private Imagem imagem;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_CONFIGURACAO_EMAIL", referencedColumnName = "ID_CONFIGURACAO_EMAIL")
    private ConfiguracaoEmail configuracaoEmail;

    @Lob
    @Column(name = "BB_LIBERACAO_SISTEMA", columnDefinition = "BLOB")
    private byte[] liberacao;

    @ManyToOne
    @JoinColumn(name = "FK_MATRIZ", referencedColumnName = "ID_EMPRESA")
    private Empresa matriz;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "empresa", orphanRemoval = true)
    @Fetch(FetchMode.SUBSELECT)
    private List<EmpresaConfiguracao> configuracoes = Lists.newArrayList();

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "matriz")
    private List<Empresa> filiais = Lists.newArrayList();

    @Transient
    private boolean configuracaoContratos;
    @Transient
    private boolean configuracaoControladoria;
    @Transient
    private boolean configuracaoFinanceiro;
    @Transient
    private boolean configuracaoRobot;

    public Empresa() {
        super();
    }

    public Empresa(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(id).toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Empresa) {
            final Empresa other = (Empresa) obj;
            return new EqualsBuilder().append(id, other.id).isEquals();
        } else {
            return false;
        }
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public byte[] getLiberacao() {
        return liberacao;
    }

    public void setLiberacao(byte[] liberacao) {
        this.liberacao = liberacao;
    }

    public Imagem getImagem() {
        return imagem;
    }

    public void setImagem(Imagem imagem) {
        this.imagem = imagem;
    }

    public String getLatitudeLongitudeCentro() {
        return latitudeLongitudeCentro;
    }

    public void setLatitudeLongitudeCentro(String latitudeLongitudeCentro) {
        this.latitudeLongitudeCentro = latitudeLongitudeCentro;
    }

    public List<EmpresaConfiguracao> getConfiguracoes() {
        return configuracoes;
    }

    public void setConfiguracoes(List<EmpresaConfiguracao> configuracoes) {
        this.configuracoes = configuracoes;
    }

    public ConfiguracaoEmail getConfiguracaoEmail() {
        return configuracaoEmail;
    }

    public void setConfiguracaoEmail(ConfiguracaoEmail configuracaoEmail) {
        this.configuracaoEmail = configuracaoEmail;
    }

    public void setConfiguracaoFinanceiro(boolean configuracaoFinanceiro) {
        this.configuracaoFinanceiro = configuracaoFinanceiro;
    }

    public List<Empresa> getFiliais() {
        return filiais;
    }

    public void setFiliais(List<Empresa> filiais) {
        this.filiais = filiais;
    }

    public boolean isConfiguracaoControladoria() {
        if(this.configuracoes.isEmpty()) {
            return Boolean.FALSE;
        }
        if (isPossuiConfiguracao(CONF_CONTROLADORIA)) return Boolean.TRUE;
        return Boolean.FALSE;
    }

    public boolean isConfiguracaoFinanceiro() {
        if(this.configuracoes.isEmpty()) {
            return Boolean.FALSE;
        }
        if (isPossuiConfiguracao(CONF_FINANCEIRO)) return Boolean.TRUE;
        return Boolean.FALSE;
    }

    public boolean isConfiguracaoContratos() {
        if(this.configuracoes.isEmpty()) {
            return Boolean.FALSE;
        }
        if (isPossuiConfiguracao(CONF_CONTRATO)) return Boolean.TRUE;
        return Boolean.FALSE;
    }

    private boolean isPossuiConfiguracao(String configuracao) {
        for (EmpresaConfiguracao empresaConfiguracao : this.configuracoes) {
            if(empresaConfiguracao.getConfiguracao().getDescricao().equals(configuracao)) {
                return true;
            }
        }
        return false;
    }

    public boolean isConfiguracaoRobot() {
        if(this.configuracoes.isEmpty()) {
            return Boolean.FALSE;
        }
        if (isPossuiConfiguracao(CONF_ROBOT)) return Boolean.TRUE;
        return Boolean.FALSE;
    }

    public void setConfiguracaoRobot(boolean configuracaoRobot) {
        this.configuracaoRobot = configuracaoRobot;
    }

    public void setConfiguracaoContratos(boolean configuracaoContratos) {
        this.configuracaoContratos = configuracaoContratos;
    }

    public void setConfiguracaoControladoria(boolean configuracaoControladoria) {
        this.configuracaoControladoria = configuracaoControladoria;
    }

    public Empresa getMatriz() {
        return matriz;
    }

    public void setMatriz(Empresa matriz) {
        this.matriz = matriz;
    }

    public void validar() throws ValidationException {
        runValidations(buildValidatorFactory().validate(new EmpresaValidacaoWrapper(this)));
    }
}
