package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import br.com.juridico.enums.StatusEmailCaixaSaidaIndicador;
import com.google.common.collect.Lists;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Jefferson on 06/08/2016.
 */
@Entity
@Table(name = "TB_EMAIL_CAIXA_SAIDA")
public class EmailCaixaSaida implements IEntity, Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -7933565680979689245L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_EMAIL_CAIXA_SAIDA", nullable = false)
    private Long id;

    @Column(name = "DS_REMETENTE", nullable = false)
    private String remetente;

    @Column(name = "DS_ASSUNTO", nullable = false)
    private String assunto;

    @Column(name = "DS_TEXTO_EMAIL", nullable = false)
    private String corpoEmail;

    @Column(name = "DT_CADASTRO", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataCadastro;

    @Column(name = "DT_ALTERACAO_STATUS", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataAlteracao;

    @Column(name = "DT_PROCESSAMENTO", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataProcessamento;

    @Type(type="true_false")
    @Column(name = "FL_PROCESSADO", nullable = false)
    private Boolean flagProcessado;

    @Column(name = "FL_STATUS", nullable = false)
    private Character flagStatus;

    @Column(name = "NR_EMPRESA", nullable = false)
    private Long codigoEmpresa;

    @OneToMany(cascade = CascadeType.ALL, fetch=FetchType.EAGER, mappedBy = "emailCaixaSaida", orphanRemoval = true)
    private List<EmailDestinatario> emails = Lists.newArrayList();

    public EmailCaixaSaida(){super();}

    public EmailCaixaSaida(String remetente, String assunto, String corpoEmail, Long codigoEmpresa){
        this.remetente = remetente;
        this.assunto = assunto;
        this.corpoEmail = corpoEmail;
        this.dataCadastro = Calendar.getInstance().getTime();
        this.dataAlteracao = Calendar.getInstance().getTime();
        this.flagProcessado = false;
        this.flagStatus = StatusEmailCaixaSaidaIndicador.PENDENTE.getValue().charAt(0);
        this.codigoEmpresa = codigoEmpresa;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRemetente() {
        return remetente;
    }

    public void setRemetente(String remetente) {
        this.remetente = remetente;
    }

    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public Date getDataAlteracao() {
        return dataAlteracao;
    }

    public void setDataAlteracao(Date dataAlteracao) {
        this.dataAlteracao = dataAlteracao;
    }

    public Date getDataProcessamento() {
        return dataProcessamento;
    }

    public void setDataProcessamento(Date dataProcessamento) {
        this.dataProcessamento = dataProcessamento;
    }

    public Boolean getFlagProcessado() {
        return flagProcessado;
    }

    public void setFlagProcessado(Boolean flagProcessado) {
        this.flagProcessado = flagProcessado;
    }

    public Character getFlagStatus() {
        return flagStatus;
    }

    public void setFlagStatus(Character flagStatus) {
        this.flagStatus = flagStatus;
    }

    public Long getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(Long codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }

    public List<EmailDestinatario> getEmails() {
        return emails;
    }

    public void setEmails(List<EmailDestinatario> emails) {
        this.emails = emails;
    }

    public String getAssunto() {
        return assunto;
    }

    public void setAssunto(String assunto) {
        this.assunto = assunto;
    }

    public String getCorpoEmail() {
        return corpoEmail;
    }

    public void setCorpoEmail(String corpoEmail) {
        this.corpoEmail = corpoEmail;
    }
    
    public void addEmails(EmailDestinatario emailDestinatario){
    	this.emails.add(emailDestinatario);
    }

    public String getDescricaoEmail() {
        StringBuilder value = new StringBuilder();
        if(corpoEmail != null && corpoEmail.length() > 60){
            value.append(this.corpoEmail.substring(0, 60)).append("...");
            return value.toString();
        }
        return corpoEmail == null ? "" : corpoEmail;
    }

    public String getDestinatarios(){
        StringBuilder sb = new StringBuilder();
        for (EmailDestinatario email : emails) {
            sb.append(email.getDestinatario()).append("; ");
        }
        return sb.toString();
    }
}
