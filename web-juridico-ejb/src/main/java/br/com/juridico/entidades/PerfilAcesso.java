package br.com.juridico.entidades;

import br.com.juridico.dao.IEntity;
import com.google.common.collect.Lists;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Audited
@Table(name = "PFL_PERFIL_ACESSO")
public class PerfilAcesso implements IEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5780075914003265155L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_PERFIL_ACESSO", nullable = false)
	private Long id;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@JoinColumn(name = "FK_PERFIL", referencedColumnName = "ID_PERFIL", nullable = false)
	@ManyToOne(optional = false)
	private Perfil perfil;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@JoinColumn(name = "FK_ACESSO", referencedColumnName = "ID_ACESSO", nullable = false)
	@ManyToOne(optional = false)
	private Acesso acesso;

	@Column(name = "NR_EMPRESA", nullable = false)
	private Long codigoEmpresa;

	@Column(name = "DT_EXCLUSAO_LOGICA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataExclusao;

	@NotAudited
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "perfilAcesso")
	private List<DireitoPerfil> direitoPerfis = Lists.newArrayList();


	public PerfilAcesso(){super();}

	public PerfilAcesso(Perfil perfil, Acesso acesso, Long codigoEmpresa){
		this.perfil = perfil;
		this.acesso = acesso;
		this.codigoEmpresa = codigoEmpresa;
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	public Acesso getAcesso() {
		return acesso;
	}

	public void setAcesso(Acesso acesso) {
		this.acesso = acesso;
	}

	public Long getCodigoEmpresa() {
		return codigoEmpresa;
	}

	public void setCodigoEmpresa(Long codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}

	public Date getDataExclusao() {
		return dataExclusao;
	}

	public void setDataExclusao(Date dataExclusao) {
		this.dataExclusao = dataExclusao;
	}

	public List<DireitoPerfil> getDireitoPerfis() {
		return direitoPerfis;
	}

	public void setDireitoPerfis(List<DireitoPerfil> direitoPerfis) {
		this.direitoPerfis = direitoPerfis;
	}

	public void addDireitoPerfis(DireitoPerfil direitoPerfil){
		this.direitoPerfis.add(direitoPerfil);
	}
}
