package br.com.juridico.impl;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.builder.EventoBuilder;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.*;
import br.com.juridico.filter.ControladoriaFilter;
import br.com.juridico.filter.RelatorioAgendaGeralFilter;
import br.com.juridico.util.DateUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class EventoSessionBean extends AbstractSessionBean<Evento> {

	/**
	 *
	 */
	private static final long serialVersionUID = -8284502709282755391L;

	private static final String PESSOA = "pessoa";
	private static final String DATA_FIM = "dataFim";
	private static final String DATA_INICIO = "dataInicio";
	private static final String CODIGO_EMPRESA = "codigoEmpresa";
	private static final String COLABORADORES = "COLABORADORES";
	private static final String ADVOGADOS = "ADVOGADOS";
	private static final String STATUS = "status";
	private static final String PESSOA_FISICA = "PF";

	@Inject
	private PessoaFisicaSessionBean pessoaFisicaSessionBean;
	@Inject
	private PessoaSessionBean pessoaSessionBean;
	@Inject
	private StatusAgendaSessionBean statusAgendaSessionBean;

	public EventoSessionBean() {
		super();
	}

	/**
	 *
	 * @param entityManager
	 */
	@Inject
	public EventoSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(Evento.class, entityManager));
	}

	/**
	 *
	 * @param pessoa
	 * @param dataInicio
	 * @param dataFim
	 * @param codigoEmpresa
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Evento> findUserEvents(Pessoa pessoa, Date dataInicio, Date dataFim, Long codigoEmpresa) {
		StringBuilder sql = new StringBuilder();
		sql.append("select e from Evento e where e.codigoEmpresa=:codigoEmpresa ")
				.append("and e.pessoa.id=:pessoa ")
				.append("and e.dataInicio >= :dataInicio and e.dataFim <= :dataFim and e.dataExclusao is null ");

		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter(PESSOA, pessoa.getId());
		query.setParameter(CODIGO_EMPRESA, codigoEmpresa);
		query.setParameter(DATA_INICIO, dataInicio, TemporalType.TIMESTAMP);
		query.setParameter(DATA_FIM, dataFim, TemporalType.TIMESTAMP);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Evento> findUserStatusEvents(StatusAgenda statusAgenda, Pessoa pessoa, Date dataInicio, Date dataFim,
											 Long codigoEmpresa) {
		StringBuilder sql = new StringBuilder();
		sql.append("select e from Evento e where e.codigoEmpresa=:codigoEmpresa ").append("and e.pessoa.id=:idUser ")
				.append("and e.dataInicio >= :dataInicio and e.dataFim <= :dataFim ")
				.append("and e.statusAgenda.id =:statusAgenda and e.dataExclusao is null ");

		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter("idUser", pessoa.getId());
		query.setParameter(CODIGO_EMPRESA, codigoEmpresa);
		query.setParameter(DATA_INICIO, dataInicio, TemporalType.TIMESTAMP);
		query.setParameter(DATA_FIM, dataFim, TemporalType.TIMESTAMP);
		query.setParameter("statusAgenda", statusAgenda.getId());
		return query.getResultList();
	}

	public Integer findCountByUserStatus(StatusAgenda statusAgenda, Pessoa pessoa, Long codigoEmpresa) {
		StringBuilder sql = new StringBuilder();
		sql.append("select count(e) from Evento e where e.codigoEmpresa=:codigoEmpresa ").append("and e.pessoa.id=:idUser ")
				.append("and e.statusAgenda.id =:statusAgenda and e.dataExclusao is null ");

		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter("idUser", pessoa.getId());
		query.setParameter(CODIGO_EMPRESA, codigoEmpresa);
		query.setParameter("statusAgenda", statusAgenda.getId());
		Long result = (Long) query.getSingleResult();
		return result.intValue();
	}

	public Integer findCountByUser(Pessoa pessoa, Long codigoEmpresa) {
		StringBuilder sql = new StringBuilder();
		sql.append("select count(e) from Evento e where e.codigoEmpresa=:codigoEmpresa ")
				.append("and e.pessoa.id=:idUser and e.dataExclusao is null ");

		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter("idUser", pessoa.getId());
		query.setParameter(CODIGO_EMPRESA, codigoEmpresa);
		Long result = (Long) query.getSingleResult();
		return result.intValue();
	}

	public Evento findByChaveId(String chaveId) {
		StringBuilder sql = new StringBuilder();
		sql.append("select e from Evento e where e.chaveId=:chaveId and e.dataExclusao is null ");
		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter("chaveId", chaveId);

		try {
			return (Evento) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	public List<Evento> findByPessoaFisica(PessoaFisica pessoaFisica, Long codigoEmpresa) {
		StringBuilder sql = new StringBuilder();
		sql.append("select e from Evento e where e.codigoEmpresa=:codigoEmpresa ")
				.append("and e.pessoa.id=:pessoa and e.dataExclusao is null ");
		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter(CODIGO_EMPRESA, codigoEmpresa);
		query.setParameter(PESSOA, pessoaFisica.getPessoa().getId());
		return query.getResultList();
	}

	public List<Evento> findByEventosDaEmpresa(Date dataInicio, Date dataFim, Long codigoEmpresa, Boolean retornaEventosSemResponsavel) {
		StringBuilder sql = new StringBuilder();
		sql.append("select e from Evento e where e.codigoEmpresa=:codigoEmpresa ")
				.append("and e.dataInicio >= :dataInicio and e.dataFim <= :dataFim and e.dataExclusao is null ");

		if (!retornaEventosSemResponsavel) {
			sql.append("and e.pessoa is not null ");
		}

		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter(CODIGO_EMPRESA, codigoEmpresa);
		query.setParameter(DATA_INICIO, dataInicio, TemporalType.TIMESTAMP);
		query.setParameter(DATA_FIM, dataFim, TemporalType.TIMESTAMP);

		return query.getResultList();
	}

	public List<Evento> findByEventosDaEmpresaPessoaIndefinida(Date dataInicio, Date dataFim, Long codigoEmpresa) {
		StringBuilder sql = new StringBuilder();
		sql.append("select e from Evento e where e.codigoEmpresa=:codigoEmpresa ")
				.append("and e.dataInicio >= :dataInicio and e.dataFim <= :dataFim ")
				.append("and e.pessoa is null and e.dataExclusao is null ");
		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter(CODIGO_EMPRESA, codigoEmpresa);
		query.setParameter(DATA_INICIO, dataInicio, TemporalType.TIMESTAMP);
		query.setParameter(DATA_FIM, dataFim, TemporalType.TIMESTAMP);

		return query.getResultList();
	}

	public boolean isExisteEventoComPessoaIndefinida(Long codigoEmpresa){
		StringBuilder sql = new StringBuilder();
		sql.append("select e from Evento e where e.codigoEmpresa=:codigoEmpresa and e.pessoa is null and e.dataExclusao is null ");
		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter(CODIGO_EMPRESA, codigoEmpresa);

		List<Evento> results = query.getResultList();
		return !results.isEmpty();
	}

	public List<Evento> findByEventosComProcesso(Long codigoEmpresa) {
		StringBuilder sql = new StringBuilder();
		sql.append("select e from Evento e where e.codigoEmpresa=:codigoEmpresa ")
				.append("and e.processo is not null and e.andamento is not null and e.dataExclusao is null ");

		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter(CODIGO_EMPRESA, codigoEmpresa);
		return query.getResultList();
	}

	public List<Evento> findByEventosControladoriaFilter(ControladoriaFilter filter, Long codigoEmpresa) {
		StringBuilder sql = new StringBuilder();
		sql.append("select e from Evento e  ")
				.append("inner join e.andamento a ")
				.append("inner join e.statusAgenda st ")
				.append("inner join e.processo proc ")
				.append("left join e.pessoa p ")
				.append("left join e.prazoAndamento pa ")
				.append("where e.codigoEmpresa=:codigoEmpresa and e.processo is not null and e.andamento is not null and pa.dataExclusao is null ")
				.append("and e.dataExclusao is null ");

		if(filter != null && filter.getDataInicioAlteracao() != null && filter.getDataFimAlteracao() == null) {
			sql.append("and e.dataHoraAlteracao >= :inicioAlteracao ");
		}
		if(filter != null && filter.getDataInicioAlteracao() == null && filter.getDataFimAlteracao() != null) {
			sql.append("and e.dataHoraAlteracao <= :fimAlteracao ");
		}
		if(filter != null && filter.getDataInicioAlteracao() != null && filter.getDataFimAlteracao() != null) {
			sql.append("and e.dataHoraAlteracao BETWEEN :inicioAlteracao AND :fimAlteracao ");
		}

		if(filter != null && filter.getDataInicioPrazo() != null && filter.getDataFimPrazo() == null) {
			sql.append("and e.dataInicio >= :inicioPrazo ");
		}
		if(filter != null && filter.getDataInicioPrazo() == null && filter.getDataFimPrazo() != null) {
			sql.append("and e.dataInicio <= :fimPrazo ");
		}
		if(filter != null && filter.getDataInicioPrazo() != null && filter.getDataFimPrazo() != null) {
			sql.append("and e.dataInicio >= :inicioPrazo AND e.dataFim <=:fimPrazo ");
		}
		if(filter != null && !Strings.isNullOrEmpty(filter.getStatus())) {
			sql.append("and st.descricao =:status ");
		}
		if(filter != null && filter.getAdvogadoColaborador() != null) {
			sql.append("and p.id =:idPessoa ");
		}
		if(filter != null && !Strings.isNullOrEmpty(filter.getControladoriaStatus())) {
			sql.append("and e.controladoriaStatus =:statusControladoria ");
		}
		if(filter != null && !Strings.isNullOrEmpty(filter.getProcesso())){
			sql.append("and proc.descricao =:processo ");
		}
		sql.append(" order by e.dataHoraAlteracao asc ");
		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter(CODIGO_EMPRESA, codigoEmpresa);
		addParameters(filter, query);

		return filtrarPessoas(query.getResultList(), filter);
	}

	private List<Evento> filtrarPessoas(List<Evento> resultList, ControladoriaFilter filter) {
		List<Evento> filterList = Lists.newArrayList();
		if(filter != null && (filter.getPessoaFisica() != null || filter.getPessoaJuridica() != null)) {
			for (Evento evento : resultList) {
				String nomePessoa = PESSOA_FISICA.equals(filter.getTipoPessoa()) ? filter.getPessoaFisica().getNomePessoa() : filter.getPessoaJuridica().getNomePessoa() ;
				List<Parte> partes = evento.getProcesso().getPartes().stream().filter(p -> p.getPessoa().getDescricao().equals(nomePessoa)).collect(Collectors.toList());
				if(!partes.isEmpty()){
					filterList.add(evento);
				}
			}
			return filterList;
		}
		return resultList;
	}


	private void addParameters(ControladoriaFilter filter, Query query) {
		if(filter != null && filter.getDataInicioAlteracao() != null) {
			query.setParameter("inicioAlteracao", filter.getDataInicioAlteracao(), TemporalType.TIMESTAMP);
		}
		if(filter != null && filter.getDataFimAlteracao() != null) {
			query.setParameter("fimAlteracao", DateUtils.upperLimit(filter.getDataFimAlteracao()), TemporalType.TIMESTAMP);
		}
		if(filter != null && filter.getDataInicioPrazo() != null) {
			query.setParameter("inicioPrazo", filter.getDataInicioPrazo(), TemporalType.TIMESTAMP);
		}
		if(filter != null && filter.getDataFimPrazo() != null) {
			query.setParameter("fimPrazo", DateUtils.upperLimit(filter.getDataFimPrazo()), TemporalType.TIMESTAMP);
		}
		if(filter != null && !Strings.isNullOrEmpty(filter.getStatus())) {
			query.setParameter("status", filter.getStatus());
		}
		if(filter != null && filter.getAdvogadoColaborador() != null) {
			query.setParameter("idPessoa", filter.getAdvogadoColaborador().getPessoa().getId());
		}
		if(filter != null && !Strings.isNullOrEmpty(filter.getControladoriaStatus())) {
			query.setParameter("statusControladoria", filter.getControladoriaStatus());
		}
		if(filter != null && !Strings.isNullOrEmpty(filter.getProcesso())) {
			query.setParameter("processo", filter.getProcesso());
		}
	}

	/**
	 *
	 * @param idAndamento
	 * @param codigoEmpresa
	 * @return
	 */
	public Map<Evento, Evento> findByAndamento(Long idAndamento, Long codigoEmpresa) {
		StringBuilder sql = new StringBuilder();
		sql.append("select e from Evento e where e.codigoEmpresa=:codigoEmpresa ")
				.append("and e.andamento.id =:idAndamento and e.dataExclusao is null ");
		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter(CODIGO_EMPRESA, codigoEmpresa);
		query.setParameter("idAndamento", idAndamento);

		Map<Evento, Evento> resultMap = Maps.newHashMap();
		List<Evento> eventos = query.getResultList();
		for (Evento evento : eventos) {
			resultMap.put(evento, evento);
		}
		return resultMap;
	}

	public Map<Evento, Evento> findByPrazoAndamento(Long idPrazoAndamento, Long codigoEmpresa) {
		StringBuilder sql = new StringBuilder();
		sql.append("select e from Evento e where e.codigoEmpresa=:codigoEmpresa ")
				.append("and e.prazoAndamento.id =:idPrazoAndamento and e.prazoAndamento.dataExclusao is null and e.dataExclusao is null ");
		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter(CODIGO_EMPRESA, codigoEmpresa);
		query.setParameter("idPrazoAndamento", idPrazoAndamento);

		Map<Evento, Evento> resultMap = Maps.newHashMap();
		List<Evento> eventos = query.getResultList();
		for (Evento evento : eventos) {
			resultMap.put(evento, evento);
		}
		return resultMap;
	}

	private Long[] pessoasDaEmpresaList(Long codigoEmpresa) {
		List<PessoaFisica> pessoasFisicas = pessoaFisicaSessionBean.findByAdvogadosAndColaboradores(codigoEmpresa);
		Long[] pessoasIds = new Long[pessoasFisicas.size()];

		for (int i = 0; i < pessoasFisicas.size(); i++) {
			pessoasIds[i] = pessoasFisicas.get(i).getPessoa().getId();
		}
		return pessoasIds;
	}

	@SuppressWarnings("unchecked")
	public List<Evento> findReportByPeriodoAndPessoa(RelatorioAgendaGeralFilter filter) {
		StringBuilder sql = new StringBuilder();
		sql.append("select e.ds_descricao, e.ds_localizacao, "
				+ "e.dt_inicio, e.dt_fim, e.ds_observacao, e.fk_status_agenda, e.fk_pessoa " + " from tb_evento e ");
		sql.append("inner join tb_pessoa p on p.id_pessoa = e.fk_pessoa ");
		sql.append("inner join tb_pessoa_fisica pf on p.id_pessoa = pf.fk_pessoa ");
		sql.append("where e.nr_empresa =:codigoEmpresa and e.dt_inicio >= :dataInicio and e.dt_fim < :dataFim and e.dataExclusao is null ");

		if (ADVOGADOS.equals(filter.getOpcaoPessoa())) {
			sql.append("and pf.fl_advogado_interno = 'S' ");
		} else if (COLABORADORES.equals(filter.getOpcaoPessoa())) {
			sql.append("and pf.fl_colaborador = 'S' ");
		}

		if (filter.getPessoa() != null) {
			sql.append("and p.id_pessoa =:pessoa ");
		}

		if (!Strings.isNullOrEmpty(filter.getStatus())) {
			sql.append("and e.statusAgenda.descricao =:status ");
		}

		sql.append("order by p.ds_pessoa, e.ds_descricao, e.dt_inicio asc");

		Query query = getDao().getEntityManager().createNativeQuery(sql.toString());
		addReportParameters(filter, query);

		List<Evento> results = Lists.newArrayList();
		List<Object[]> listResults = query.getResultList();
		carregaObjetosNoList(results, listResults);

		return results;
	}

	private void addReportParameters(RelatorioAgendaGeralFilter filter, Query query) {
		query.setParameter(CODIGO_EMPRESA, filter.getCodigoEmpresa());
		query.setParameter(DATA_INICIO, filter.getDataInicial());
		query.setParameter(DATA_FIM, filter.getDataFinal());
		if (filter.getPessoa() != null) {
			query.setParameter(PESSOA, filter.getPessoa().getId());
		}
		if (!Strings.isNullOrEmpty(filter.getStatus())) {
			query.setParameter(STATUS, filter.getStatus());
		}
	}

	private void carregaObjetosNoList(List<Evento> results, List<Object[]> listResults) {
		for (Object[] record : listResults) {
			BigInteger idStatusAgenda = (BigInteger) record[5];
			BigInteger idPessoa = (BigInteger) record[6];
			StatusAgenda statusAgenda = idStatusAgenda == null ? null : statusAgendaSessionBean.retrieve(idStatusAgenda.longValue());
			Pessoa pessoa = idPessoa == null ? null : pessoaSessionBean.retrieve(idPessoa.longValue());

			EventoBuilder builder = new EventoBuilder();
			builder.addDescricao(convertToString(record[0])).addLocalizacao(convertToString(record[1]))
					.addDataInicio(convertToDate(record[2])).addDataFim(convertToDate(record[3]))
					.addObservacao(convertToString(record[4])).addStatusAgenda(statusAgenda).addPessoa(pessoa);

			results.add(builder.build());
		}
	}

	private String convertToString(Object object) {
		return object == null ? "" : (String) object;
	}

	private Date convertToDate(Object object) {
		return object == null ? null : (Date) object;
	}

}
