package br.com.juridico.impl;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.Manutencao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

/**
 * 
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class ManutencaoLoginSessionBean extends AbstractSessionBean<Manutencao> {

	/**
	 *
	 */
	private static final long serialVersionUID = -8284502709282755391L;

	public ManutencaoLoginSessionBean() {
		super();
	}

	/**
	 *
	 * @param entityManager
	 */
	@Inject
	public ManutencaoLoginSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(Manutencao.class, entityManager));
	}

	public Manutencao findByLogin(String login) {
		String sql = "select t from Manutencao t where t.login=:login ";
		Query query = getDao().getEntityManager().createQuery(sql);
		query.setParameter("login", login);
		List<Manutencao> results = query.getResultList();

		if(results.isEmpty()) {
			return null;
		}
		return results.get(0);
	}

}
