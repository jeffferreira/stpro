package br.com.juridico.impl;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.ParcelaHonorario;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 * 
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class ParcelaHonorarioSessionBean extends AbstractSessionBean<ParcelaHonorario> {

	/**
	 *
	 */
	private static final long serialVersionUID = -8284502709282755391L;

	public ParcelaHonorarioSessionBean() {
		super();
	}

	/**
	 *
	 * @param entityManager
	 */
	@Inject
	public ParcelaHonorarioSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(ParcelaHonorario.class, entityManager));
	}


}
