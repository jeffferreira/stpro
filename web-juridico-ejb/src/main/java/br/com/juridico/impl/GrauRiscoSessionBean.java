package br.com.juridico.impl;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.GrauRisco;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;

/**
 * 
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class GrauRiscoSessionBean extends AbstractCrudSessionBean<GrauRisco> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3246638270853559888L;
	private static final String DESCRICAO = "descricao";
	private static final String CODIGO_EMPRESA = "codigoEmpresa";
	private static final String GRAU_RISCO_FIND_BY_DESCRICAO = "GrauRisco.findByDescricao";

	public GrauRiscoSessionBean() {
		super();
	}

	/**
	 * 
	 * @param entityManager
	 */
	@Inject
	public GrauRiscoSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(GrauRisco.class, entityManager));
	}

	@Override
	public OrderClause[] getDefaultOrder() {
		return new OrderClause[] { new OrderClause(DESCRICAO, OrderType.ASC) };
	}

	public List<GrauRisco> filtrados(ConsultaLazyFilter filtro) {
		filtro.setAscendente(true);
		filtro.setPropriedadeOrdenacao(DESCRICAO);
		return super.filtrados(filtro, GrauRisco.class);
	}

	public int quantidadeFiltrados(ConsultaLazyFilter filtro) {
		return super.quantidadeFiltrados(filtro, GrauRisco.class);
	}

	/**
	 * 
	 * @param descricao
	 * @param codigoEmpresa
	 * @return
	 */
	public GrauRisco findByDescricao(String descricao, Long codigoEmpresa) {
		Query query = getDao().getEntityManager().createNamedQuery(GRAU_RISCO_FIND_BY_DESCRICAO)
				.setParameter(DESCRICAO, descricao).setParameter(CODIGO_EMPRESA, codigoEmpresa);
		try {
			return (GrauRisco) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public boolean isDescricaoJaCadastrada(GrauRisco grauRisco, Long codigoEmpresa){
		StringBuilder sql = new StringBuilder();
		sql.append("select t from GrauRisco t where t.descricao=:descricao and t.codigoEmpresa=:codigoEmpresa and t.dataExclusao is null");

		Query query = getDao().getEntityManager().createQuery(sql.toString())
				.setParameter(DESCRICAO, grauRisco.getDescricao()).setParameter(CODIGO_EMPRESA, codigoEmpresa);

		List<GrauRisco> results = query.getResultList();

		if(results.isEmpty()) {
			return Boolean.FALSE;
		}
		return validaDuplicidade(grauRisco, results.get(0));
	}

	private boolean validaDuplicidade(GrauRisco grauRisco, GrauRisco result) {
		if(result != null && grauRisco.getId() == result.getId()) {
			return Boolean.FALSE;
		} else if(result != null && grauRisco.getId() != result.getId()){
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
}
