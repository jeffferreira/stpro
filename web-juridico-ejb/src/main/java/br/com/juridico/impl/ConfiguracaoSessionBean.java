package br.com.juridico.impl;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.Configuracao;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 * 
 * @author Marcos
 *
 */
@Stateless
@LocalBean
public class ConfiguracaoSessionBean extends AbstractSessionBean<Configuracao> {

	public ConfiguracaoSessionBean() {
		super();
	}

	@Inject
	public ConfiguracaoSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(Configuracao.class, entityManager));
	}

	@Override
	public OrderClause[] getDefaultOrder() {
		return new OrderClause[] { new OrderClause("id", OrderType.ASC) };
	}

	@SuppressWarnings("unchecked")
	public Configuracao findByDescricao(String configuracao) {
		StringBuilder sb = new StringBuilder();
		sb.append(
				"select c from Configuracao c where c.descricao=:configuracao \n");

		Query query = getDao().getEntityManager().createQuery(sb.toString())
				.setParameter("configuracao", configuracao);

		return (Configuracao) query.getSingleResult();
	}
}
