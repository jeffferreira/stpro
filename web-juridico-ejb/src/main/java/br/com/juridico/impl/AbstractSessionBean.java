/**
 * 
 */
package br.com.juridico.impl;

import br.com.juridico.dao.IEntity;
import br.com.juridico.ejb.AbstractBean;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import java.io.Serializable;

/**
 * @author Jefferson
 *
 */
public abstract class AbstractSessionBean<T extends IEntity> extends AbstractBean<T> implements Serializable {

	private static final long serialVersionUID = 1L;
	
	protected static final Logger LOGGER = Logger.getLogger(IEntity.class);

	protected EntityManager entityManager;

	/**
	 * 
	 */
	public AbstractSessionBean() {
		super();
	}

	/**
	 * 
	 * @param entityManager
	 */
	public AbstractSessionBean(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	protected abstract void initDaoSet(EntityManager entityManager);

	@PostConstruct
	protected void setup() {
		initDaoSet(entityManager);
	}

}


