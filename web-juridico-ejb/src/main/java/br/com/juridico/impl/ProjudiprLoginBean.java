package br.com.juridico.impl;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.ProjudiprLogin;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Stateless
@LocalBean
public class ProjudiprLoginBean extends AbstractSessionBean<ProjudiprLogin> {

	/**
	 *
	 */
	private static final long serialVersionUID = -3833477281795653214L;

	public ProjudiprLoginBean() {
		super();
	}

	@Inject
	public ProjudiprLoginBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(ProjudiprLogin.class, entityManager));
	}

	public ProjudiprLogin findByLoginEmpresa(Long codigoEmpresa){
		StringBuilder sql = new StringBuilder();
		sql.append("select t from ProjudiprLogin t where t.codigoEmpresa=:codigoEmpresa");
		Query query = getDao().getEntityManager().createQuery(sql.toString()).setParameter("codigoEmpresa", codigoEmpresa);

		List<ProjudiprLogin> result = query.getResultList();
		if(result.isEmpty()) {
			return null;
		}
		return result.get(0);
	}

}
