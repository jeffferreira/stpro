package br.com.juridico.impl;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.Andamento;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;
import com.google.common.collect.Lists;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 
 * @author Marcos
 *
 */
@Stateless
@LocalBean
public class AndamentoSessionBean extends AbstractSessionBean<Andamento> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5214967225075676085L;

	public AndamentoSessionBean() {
		super();
	}

	/**
	 * 
	 * @param entityManager
	 */
	@Inject
	public AndamentoSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(Andamento.class, entityManager));
	}

	@Override
	public OrderClause[] getDefaultOrder() {
		return new OrderClause[] { new OrderClause("id", OrderType.ASC) };
	}

	/**
	 *
	 * @return
     */
	public List<BigInteger> findByAndamentosAtingiramDiasAviso(){
		StringBuilder sql = new StringBuilder();
		sql.append("select a.id_andamento from tb_processo p ");
		sql.append("inner join tb_andamento a on a.fk_processo = p.id_processo ");
		sql.append("where p.FL_STATUS = 'A' AND a.fl_envia_email_aviso = 'T' AND a.dt_exclusao_logica is null ");
		sql.append("and DATEDIFF(a.dt_final_prazo, CURDATE()) = a.NR_QTDE_DIAS_EMAIL_AVISO_ANTECEDENCIA ");
		Query query = getDao().getEntityManager().createNativeQuery(sql.toString());
		return query.getResultList();
	}

    public List<BigInteger> findByAndamentosPrazoVencido() {
        StringBuilder sql = new StringBuilder();
        sql.append("select a.id_andamento from tb_processo p \n");
        sql.append("inner join tb_andamento a on a.fk_processo = p.id_processo \n");
        sql.append("where p.FL_STATUS = 'A' AND a.fl_envia_email_aviso = 'T' AND a.dt_exclusao_logica is null \n");
        sql.append("AND a.dt_final_prazo < CURDATE() AND a.fk_status_agenda <> 1 \n");
        sql.append("AND a.fk_pessoa_fisica IS NOT NULL \n");
        Query query = getDao().getEntityManager().createNativeQuery(sql.toString());
        return query.getResultList();
    }
}
