package br.com.juridico.impl;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.ProcessoVinculo;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

/**
 * 
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class ProcessoVinculoSessionBean extends AbstractSessionBean<ProcessoVinculo> {


	public ProcessoVinculoSessionBean() {
		super();
	}

	/**
	 *
	 * @param entityManager
	 */
	@Inject
	public ProcessoVinculoSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(ProcessoVinculo.class, entityManager));
	}

	public List<ProcessoVinculo> findListByProcesso(Long idProcesso, Long codigoEmpresa){
		StringBuilder sql = new StringBuilder();
		sql.append("select t from ProcessoVinculo t where t.codigoEmpresa =:codigoEmpresa and t.processoPai.id=:idProcesso or t.processoFilho.id=:idProcesso");
		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter("codigoEmpresa", codigoEmpresa).setParameter("idProcesso", idProcesso);
		return query.getResultList();
	}


}
