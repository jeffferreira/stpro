package br.com.juridico.impl;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.PessoaJuridicaCentroCusto;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

/**
 * 
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class PessoaJuridicaCentroCustoSessionBean extends AbstractCrudSessionBean<PessoaJuridicaCentroCusto> {

	/**
	 *
	 */
	private static final long serialVersionUID = 1926126227742273076L;

	private static final String CODIGO_EMPRESA = "codigoEmpresa";
	private static final String PESSOA_JURIDICA_ID = "idPessoaJuridica";

	public PessoaJuridicaCentroCustoSessionBean() {
		super();
	}

	/**
	 *
	 * @param entityManager
	 */
	@Inject
	public PessoaJuridicaCentroCustoSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(PessoaJuridicaCentroCusto.class, entityManager));
	}

	public List<PessoaJuridicaCentroCusto> findByPessoaJuridica(Long idPessoaJuridica, Long codigoEmpresa){
		StringBuilder sql = new StringBuilder();
		sql.append("select p from PessoaJuridicaCentroCusto p where p.codigoEmpresa=:codigoEmpresa and p.pessoaJuridica.id=:idPessoaJuridica ");
		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter(CODIGO_EMPRESA, codigoEmpresa).setParameter(PESSOA_JURIDICA_ID, idPessoaJuridica);
		return query.getResultList();
	}
}
