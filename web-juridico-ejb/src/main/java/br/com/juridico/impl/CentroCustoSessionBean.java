package br.com.juridico.impl;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.CentroCusto;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;

/**
 * 
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class CentroCustoSessionBean extends AbstractCrudSessionBean<CentroCusto> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1794922449591480738L;
	private static final String DESCRICAO = "descricao";
	private static final String CODIGO_EMPRESA = "codigoEmpresa";

	public CentroCustoSessionBean() {
		super();
	}

	/**
	 * 
	 * @param entityManager
	 */
	@Inject
	public CentroCustoSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(CentroCusto.class, entityManager));
	}

	@Override
	public OrderClause[] getDefaultOrder() {
		return new OrderClause[] { new OrderClause(DESCRICAO, OrderType.ASC) };
	}

	public List<CentroCusto> filtrados(ConsultaLazyFilter filtro) {
		filtro.setAscendente(true);
		filtro.setPropriedadeOrdenacao(DESCRICAO);
		return super.filtrados(filtro, CentroCusto.class);
	}

	public int quantidadeFiltrados(ConsultaLazyFilter filtro) {
		return super.quantidadeFiltrados(filtro, CentroCusto.class);
	}

	/**
	 * 
	 * @param descricao
	 * @param codigoEmpresa
	 * @return
	 */
	public CentroCusto findByDescricao(String descricao, Long codigoEmpresa) {
		Query query = getDao().getEntityManager().createNamedQuery("CentroCusto.findByDescricao")
				.setParameter("descricao", descricao).setParameter("codigoEmpresa", codigoEmpresa);
		try {
			return (CentroCusto) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public boolean isDescricaoJaCadastrada(CentroCusto centroCusto, Long codigoEmpresa){
		StringBuilder sql = new StringBuilder();
		sql.append("select t from CentroCusto t where t.descricao=:descricao and t.codigoEmpresa=:codigoEmpresa and t.dataExclusao is null");

		Query query = getDao().getEntityManager().createQuery(sql.toString())
				.setParameter(DESCRICAO, centroCusto.getDescricao()).setParameter(CODIGO_EMPRESA, codigoEmpresa);

		List<CentroCusto> results = query.getResultList();

		if(results.isEmpty()) {
			return Boolean.FALSE;
		}
		return validaDuplicidade(centroCusto, results.get(0));
	}

	private boolean validaDuplicidade(CentroCusto centroCusto, CentroCusto result) {
		if(result != null && centroCusto.getId() == result.getId()) {
			return Boolean.FALSE;
		} else if(result != null && centroCusto.getId() != result.getId()){
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
}
