package br.com.juridico.impl;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.TipoAtendimento;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;

/**
 * 
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class TipoAtendimentoSessionBean extends AbstractCrudSessionBean<TipoAtendimento> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4936178098180782606L;
	private static final String DESCRICAO = "descricao";
	private static final String TIPO_ATENDIMENTO_FIND_BY_DESCRICAO = "TipoAtendimento.findByDescricao";
	private static final String CODIGO_EMPRESA = "codigoEmpresa";

	public TipoAtendimentoSessionBean() {
		super();
	}

	/**
	 * 
	 * @param entityManager
	 */
	@Inject
	public TipoAtendimentoSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(TipoAtendimento.class, entityManager));
	}

	@Override
	public OrderClause[] getDefaultOrder() {
		return new OrderClause[] { new OrderClause(DESCRICAO, OrderType.ASC) };
	}

	public List<TipoAtendimento> filtrados(ConsultaLazyFilter filtro) {
		return super.filtrados(filtro, TipoAtendimento.class);
	}

	public int quantidadeFiltrados(ConsultaLazyFilter filtro) {
		return super.quantidadeFiltrados(filtro, TipoAtendimento.class);
	}

	/**
	 * 
	 * @param descricao
	 * @param codigoEmpresa
	 * @return
	 */
	public TipoAtendimento findByDescricao(String descricao, Long codigoEmpresa) {
		Query query = getDao().getEntityManager().createNamedQuery(TIPO_ATENDIMENTO_FIND_BY_DESCRICAO)
				.setParameter(DESCRICAO, descricao).setParameter(CODIGO_EMPRESA, codigoEmpresa);
		try {
			return (TipoAtendimento) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public boolean isDescricaoJaCadastrada(TipoAtendimento tipoAtendimento, Long codigoEmpresa){
		StringBuilder sql = new StringBuilder();
		sql.append("select t from TipoAtendimento t where t.descricao=:descricao and t.codigoEmpresa=:codigoEmpresa and t.dataExclusao is null");

		Query query = getDao().getEntityManager().createQuery(sql.toString())
				.setParameter(DESCRICAO, tipoAtendimento.getDescricao()).setParameter(CODIGO_EMPRESA, codigoEmpresa);

		List<TipoAtendimento> results = query.getResultList();

		if(results.isEmpty()) {
			return Boolean.FALSE;
		}
		return validaDuplicidade(tipoAtendimento, results.get(0));
	}

	private boolean validaDuplicidade(TipoAtendimento tipoAtendimento, TipoAtendimento result) {
		if(result != null && tipoAtendimento.getId() == result.getId()) {
			return Boolean.FALSE;
		} else if(result != null && tipoAtendimento.getId() != result.getId()){
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
}
