package br.com.juridico.impl;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.ContratoPessoa;
import br.com.juridico.entidades.ContratoPessoaVigencia;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;
import com.google.common.collect.Lists;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Jefferson on 21/05/2016.
 */
@Stateless
@LocalBean
public class ContratoPessoaSessioBean extends AbstractCrudSessionBean<ContratoPessoa> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 437500041588525462L;
	public static final String DESCRICAO = "descricao";
	public static final String CONTRATO_PESSOA_FIND_BY_DESCRICAO = "ContratoPessoa.findByDescricao";
	public static final String CODIGO_EMPRESA = "codigoEmpresa";

	public ContratoPessoaSessioBean() {
		super();
	}

	/**
	 * 
	 * @param entityManager
	 */
	@Inject
	public ContratoPessoaSessioBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(ContratoPessoa.class, entityManager));
	}

	@Override
	public OrderClause[] getDefaultOrder() {
		return new OrderClause[] { new OrderClause(DESCRICAO, OrderType.ASC) };
	}

	public int quantidadeFiltrados(ConsultaLazyFilter filtro) {
		return super.quantidadeFiltrados(filtro, ContratoPessoa.class);
	}

	public List<ContratoPessoa> filtrados(ConsultaLazyFilter filtro) {
		return super.filtrados(filtro, ContratoPessoa.class);
	}

	/**
	 * 
	 * @param descricao
	 * @param codigoEmpresa
	 * @return
	 */
	public ContratoPessoa findByDescricao(String descricao, Long codigoEmpresa) {
		Query query = getDao().getEntityManager().createNamedQuery(CONTRATO_PESSOA_FIND_BY_DESCRICAO)
				.setParameter(DESCRICAO, descricao).setParameter(CODIGO_EMPRESA, codigoEmpresa);
		try {
			return (ContratoPessoa) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public List<ContratoPessoa> findByContratosVigentes(Long codigoEmpresa){
		StringBuilder sql = new StringBuilder();
		sql.append("select t from ContratoPessoa t ");
		sql.append("where t.codigoEmpresa=:codigoEmpresa ");

		Query query = getDao().getEntityManager().createQuery(sql.toString()).setParameter(CODIGO_EMPRESA, codigoEmpresa);
		return listaVigente(query.getResultList());
	}

	private List<ContratoPessoa> listaVigente(List<ContratoPessoa> values) {
		List<ContratoPessoa> listaVigente = Lists.newArrayList();
		listaVigente.addAll(values.stream().filter(item -> existePeriodoVigente(item.getVigencias())).collect(Collectors.toList()));
		return listaVigente;
	}

	private boolean existePeriodoVigente(List<ContratoPessoaVigencia> vigencias){
		for (ContratoPessoaVigencia item : vigencias) {
			if (item.isContratoVigente()) {
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}
}
