package br.com.juridico.impl;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.AndamentoAtendimento;
import br.com.juridico.entidades.User;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;

import java.util.List;

/**
 * 
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class AndamentoAtendimentoSessionBean extends AbstractSessionBean<AndamentoAtendimento> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 123208275572017287L;

	public AndamentoAtendimentoSessionBean() {
		// CONSTRUTOR
	}

	/**
	 * 
	 * @param entityManager
	 */
	@Inject
	public AndamentoAtendimentoSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<AndamentoAtendimento>(AndamentoAtendimento.class, entityManager));
	}

	@Override
	public OrderClause[] getDefaultOrder() {
		return new OrderClause[] { new OrderClause("dataCadastro", OrderType.DESC) };
	}

	public List<AndamentoAtendimento> findByMeusAndamentos(User user){
		String sql = "select a from AndamentoAtendimento a where a.pessoa.id =:pessoaId and a.codigoEmpresa =:codigoEmpresa";
		Query query = getDao().getEntityManager().createQuery(sql);
		query.setParameter("pessoaId", user.getId());
		query.setParameter("codigoEmpresa", user.getCodEmpresa());
		return query.getResultList();
	}

}
