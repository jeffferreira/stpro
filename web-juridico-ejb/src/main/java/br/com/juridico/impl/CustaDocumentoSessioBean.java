package br.com.juridico.impl;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.CustaDocumento;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;

@Stateless
@LocalBean
public class CustaDocumentoSessioBean extends AbstractSessionBean<CustaDocumento> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6293398488939147758L;

	public CustaDocumentoSessioBean() {
		// CONSTRUTOR
	}

	/**
	 *
	 * @param entityManager
	 */
	@Inject
	public CustaDocumentoSessioBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<CustaDocumento>(CustaDocumento.class, entityManager));
	}

	@Override
	public OrderClause[] getDefaultOrder() {
		return new OrderClause[] { new OrderClause("descricao", OrderType.ASC) };
	}

    /**
     *
     * @param idCusta
     * @param codigoEmpresa
     * @return
     */
    public List<CustaDocumento> findByCusta(Long idCusta, Long codigoEmpresa) {
        String sql = "select c from CustaDocumento c where c.custa.id =:idCusta and c.codigoEmpresa=:codigoEmpresa";
        Query query = getDao().getEntityManager().createQuery(sql)
                .setParameter("idCusta", idCusta)
                .setParameter("codigoEmpresa", codigoEmpresa);
        return query.getResultList();
    }

}
