package br.com.juridico.impl;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.ContaPagar;
import br.com.juridico.filter.ContasPagarFilter;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

import static br.com.juridico.enums.TipoDataContasPagarType.EMISSAO;
import static br.com.juridico.enums.TipoDataContasPagarType.LANCAMENTO;

@Stateless
@LocalBean
public class AprovacaoContaPagarSessionBean extends AbstractCrudSessionBean<ContaPagar> {

    private static final String CODIGO_EMPRESA = "codigoEmpresa";

    public AprovacaoContaPagarSessionBean() {
        super();
    }

    @Inject
    public AprovacaoContaPagarSessionBean(@JuridicoEntityManager EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected void initDaoSet(EntityManager entityManager) {
        setDao(new GenericDao<>(ContaPagar.class, entityManager));
    }

    public List<ContaPagar> findContasAprovacaoByFilter(ContasPagarFilter filter, Long codigoEmpresa){
        StringBuilder sql = new StringBuilder();
        sql.append("select c from ContaPagar c where c.codigoEmpresa =:codigoEmpresa ");
        if(filter.getStatusContasPagar() != null){
            sql.append("and c.status =:status ");
        }
        if(filter.getTipoDataContasPagarType() != null) {
            filtrarPorTipoData(sql, filter);
        }

        Query query = getDao().getEntityManager().createQuery(sql.toString()).setParameter(CODIGO_EMPRESA, codigoEmpresa);

        if(filter.getStatusContasPagar() != null){
            query.setParameter("status", filter.getStatusContasPagar());
        }

        return query.getResultList();
    }

    private void filtrarPorTipoData(StringBuilder sql, ContasPagarFilter filter) {
        if(EMISSAO.equals(filter.getTipoDataContasPagarType())){
            sql.append("and c.dataEmissao between :dataInicio and :dataFim ");
        } else if(LANCAMENTO.equals(filter.getTipoDataContasPagarType())) {
            sql.append("and c.dataCadastro between :dataInicio and :dataFim ");
        } else {
            sql.append("and c.dataCadastro between :dataInicio and :dataFim ");
        }
    }
}