package br.com.juridico.impl;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.ContratoSubContrato;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

/**
 * 
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class ContratoSubContratoSessionBean extends AbstractSessionBean<ContratoSubContrato> {

	/**
	 *
	 */
	private static final long serialVersionUID = -3833477281795653214L;

	public ContratoSubContratoSessionBean() {
		super();
	}

	@Inject
	public ContratoSubContratoSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(ContratoSubContrato.class, entityManager));
	}

	public List<ContratoSubContrato> findByContrato(Long idContrato) {
		StringBuilder sql = new StringBuilder();
		sql.append("select t from ContratoSubContrato t where t.contrato.id=:idContrato");
		Query query = getDao().getEntityManager().createQuery(sql.toString())
				.setParameter("idContrato", idContrato);
		return query.getResultList();
	}
}
