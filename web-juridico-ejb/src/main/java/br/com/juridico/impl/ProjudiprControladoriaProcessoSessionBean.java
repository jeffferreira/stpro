package br.com.juridico.impl;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.ProjudiprProcessoControladoria;
import br.com.juridico.filter.ControladoriaFilter;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

/**
 *
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class ProjudiprControladoriaProcessoSessionBean extends AbstractSessionBean<ProjudiprProcessoControladoria> {

	/**
	 *
	 */
	private static final long serialVersionUID = 6638941542426771110L;


	public ProjudiprControladoriaProcessoSessionBean() {
		super();
	}

	/**
	 *
	 * @param entityManager
	 */
	@Inject
	public ProjudiprControladoriaProcessoSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(ProjudiprProcessoControladoria.class, entityManager));
	}

	public List<ProjudiprProcessoControladoria> findByProcessos(Long numeroEmpresa){
		StringBuilder sql = new StringBuilder();
		sql.append("select t from ProjudiprProcessoControladoria t ")
				.append("where t.codigoEmpresa =:numeroEmpresa  ")
				.append("and t.vistoPelaControladoria =:vistoControladoria ")
				.append("and t.novaMovimentacao =:novaMovimentacao ")
				.append("order by t.movimentacao.dataUltimaAtualizacao desc");

		Query query = getDao().getEntityManager().createQuery(sql.toString())
				.setParameter("numeroEmpresa", numeroEmpresa)
				.setParameter("vistoControladoria", Boolean.FALSE)
				.setParameter("novaMovimentacao", Boolean.TRUE);

		return query.getResultList();
	}

	public List<ProjudiprProcessoControladoria> findProcessosByFilter(ControladoriaFilter filter, Long numeroEmpresa){
		StringBuilder sql = new StringBuilder();
		sql.append("select t from ProjudiprProcessoControladoria t where t.codigoEmpresa =:numeroEmpresa ");


		if(filter.getDataInicioAlteracao() != null && filter.getDataFimAlteracao() != null) {

		}

		Query query = getDao().getEntityManager().createQuery(sql.toString())
				.setParameter("numeroEmpresa", numeroEmpresa)
				.setParameter("vistoControladoria", Boolean.FALSE)
				.setParameter("novaMovimentacao", Boolean.TRUE);

		return query.getResultList();
	}

}
