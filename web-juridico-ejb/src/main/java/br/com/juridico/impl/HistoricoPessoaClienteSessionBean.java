package br.com.juridico.impl;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.HistoricoPessoaCliente;
import br.com.juridico.entidades.Pessoa;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;

/**
 * 
 * @author Marcos
 *
 */
@Stateless
@LocalBean
public class HistoricoPessoaClienteSessionBean extends AbstractSessionBean<HistoricoPessoaCliente> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7512298046886524645L;

	public HistoricoPessoaClienteSessionBean() {
		super();
	}

	@Inject
	public HistoricoPessoaClienteSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(HistoricoPessoaCliente.class, entityManager));
	}

	@Override
	public OrderClause[] getDefaultOrder() {
		return new OrderClause[] { new OrderClause("id", OrderType.ASC) };
	}

	@Override
	public List<HistoricoPessoaCliente> retrieveAll(Long codigoEmpresa) {
		String sql = "select h from HistoricoPessoaCliente h where h.codigoEmpresa=:codigoEmpresa and h.dataExclusao is null";
		return retrieveByExclusaoLogica(sql, codigoEmpresa);
	}

	@SuppressWarnings("unchecked")
	public List<HistoricoPessoaCliente> findByPessoa(Pessoa pessoa, Long codigoEmpresa) {
		StringBuilder sb = new StringBuilder();
		sb.append(
				"select h from HistoricoPessoaCliente h where h.codigoEmpresa=:codigoEmpresa and h.pessoa.id =:idPessoa \n");

		Query query = getDao().getEntityManager().createQuery(sb.toString())
				.setParameter("idPessoa", pessoa.getId()).setParameter("codigoEmpresa", codigoEmpresa);

		return query.getResultList();
	}

}
