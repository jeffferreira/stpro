package br.com.juridico.impl;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.PessoaJuridica;
import br.com.juridico.factory.ContextPessoaJuridicaSearchFactory;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;
import com.google.common.base.Strings;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.criteria.*;
import java.util.List;

/**
 *
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class PessoaJuridicaSessionBean extends AbstractCrudSessionBean<PessoaJuridica> {

	/**
	 *
	 */
	private static final long serialVersionUID = 3641670506525265696L;
	private static final String PESSOA_DESCRICAO = "pessoa.descricao";
	private static final String RESPONSAVEL = "responsavel";
	private static final String CNPJ = "cnpj";
	private static final String CODIGO_EMPRESA = "codigoEmpresa";

	public PessoaJuridicaSessionBean() {
		super();

	}

	/**
	 *
	 * @param entityManager
	 */
	@Inject
	public PessoaJuridicaSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(PessoaJuridica.class, entityManager));
	}

	@Override
	public OrderClause[] getDefaultOrder() {
		return new OrderClause[] { new OrderClause(PESSOA_DESCRICAO, OrderType.ASC) };
	}

	public List<PessoaJuridica> filtrados(ConsultaLazyFilter filtro) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<PessoaJuridica> criteriaQuery = cb.createQuery(PessoaJuridica.class);
		Root<PessoaJuridica> root = criteriaQuery.from(PessoaJuridica.class);
		Predicate condition = cb.equal(root.get("pessoa").get("ativo"), true);
		condition = cb.and(condition, cb.isNull(root.get("dataExclusao")));
		condition = cb.and(condition, cb.equal(root.get("codigoEmpresa"), filtro.getCodigoEmpresa()));
		if(!Strings.isNullOrEmpty(filtro.getDescricao())) {
			condition = ContextPessoaJuridicaSearchFactory.criterios(filtro.getDescricao(), filtro.getCodigoEmpresa(), cb, root);
		}
		criteriaQuery.where(condition);
		Expression expression = descricaoCampoOrdenacao(root, filtro.getPropriedadeOrdenacao());

		if (filtro.isAscendente() && expression != null) {
			criteriaQuery.orderBy(cb.asc(expression));
		} else if (expression != null) {
			criteriaQuery.orderBy(cb.desc(expression));
		}

		Query query = entityManager.createQuery(criteriaQuery);
		query.setFirstResult(filtro.getPrimeiroRegistro());
		query.setMaxResults(filtro.getQuantidadeRegistros());

		return query.getResultList();
	}

	private Expression descricaoCampoOrdenacao(Root<PessoaJuridica> root, String propriedadeOrdenacao){
		Expression expression = null;

		if (PESSOA_DESCRICAO.equals(propriedadeOrdenacao)) {
			expression = root.get("pessoa").get("descricao");
		} else if(RESPONSAVEL.equals(propriedadeOrdenacao)){
			expression = root.get(RESPONSAVEL);
		} else if(CNPJ.equals(propriedadeOrdenacao)) {
			expression = root.get(CNPJ);
		}
		return expression;
	}

	/**
	 *
	 * @param filtro
	 * @return
	 */
	public int quantidadeFiltrados(ConsultaLazyFilter filtro) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<PessoaJuridica> criteriaQuery = cb.createQuery(PessoaJuridica.class);
		Root<PessoaJuridica> root = criteriaQuery.from(PessoaJuridica.class);
		Predicate condition = cb.equal(root.get("pessoa").get("ativo"), true);
		condition = cb.and(condition, cb.isNull(root.get("dataExclusao")));
		condition = cb.and(condition, cb.equal(root.get("codigoEmpresa"), filtro.getCodigoEmpresa()));
		if(!Strings.isNullOrEmpty(filtro.getDescricao())) {
			condition = ContextPessoaJuridicaSearchFactory.criterios(filtro.getDescricao(), filtro.getCodigoEmpresa(), cb, root);
		}
		criteriaQuery.where(condition);
		List<PessoaJuridica> results = entityManager.createQuery(criteriaQuery).getResultList();
		return results.size();
	}

	/**
	 *
	 * @param condition
	 * @param codEmpresa
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<PessoaJuridica> findByDescricao(String condition, Long codEmpresa) {
		StringBuilder sql = new StringBuilder();
		sql.append("select pj from PessoaJuridica pj where pj.pessoa.descricao like :condition ");
		sql.append("and pj.codigoEmpresa=:codigoEmpresa and pj.pessoa.ativo = true ");
		sql.append("order by pj.pessoa.descricao asc");

		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter("condition", "%" + condition + "%");
		query.setParameter(CODIGO_EMPRESA, codEmpresa);

		return query.getResultList();
	}

	public PessoaJuridica findByPessoa(Long idPessoa, Long codEmpresa){
		StringBuilder sql = new StringBuilder();
		sql.append("select p from PessoaJuridica p where p.pessoa.id =:idPessoa ");
		sql.append("and p.codigoEmpresa=:codigoEmpresa ");
		sql.append("order by p.pessoa.descricao asc");

		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter("idPessoa", idPessoa);
		query.setParameter(CODIGO_EMPRESA, codEmpresa);

		try {
			return (PessoaJuridica) query.getSingleResult();
		} catch (NoResultException nre){
			return null;
		}
	}

	public List<PessoaJuridica> findAllPessoasJuridicas(String filter, Long codEmpresa){
		StringBuilder sql = new StringBuilder();
		sql.append("select p from PessoaJuridica p where ");
		sql.append("p.codigoEmpresa=:codigoEmpresa and p.pessoa.dataExclusao is null and p.pessoa.ativo = true ");
		if(!Strings.isNullOrEmpty(filter)) {
			sql.append("and p.pessoa.descricao like :nome \n");
		}
		sql.append("order by p.pessoa.descricao asc");
		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter(CODIGO_EMPRESA, codEmpresa);
		if(!Strings.isNullOrEmpty(filter)) {
			query.setParameter("nome", "%"+filter+"%");
		}

		return query.getResultList();
	}

	public List<PessoaJuridica> findByNome(Long idPessoa, String nome, Long codEmpresa) {
		StringBuilder sql = new StringBuilder();
		sql.append("select p from PessoaJuridica p where p.pessoa.descricao =:nome ");
		sql.append("and p.id !=:idPessoa ");
		sql.append("and p.codigoEmpresa=:codigoEmpresa and p.pessoa.dataExclusao is null ");
		sql.append("order by p.pessoa.descricao asc");

		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter("nome", nome);
		query.setParameter(CODIGO_EMPRESA, codEmpresa);
		query.setParameter("idPessoa", idPessoa != null ? idPessoa : 0L);

		return query.getResultList();
	}

	public List<PessoaJuridica> findByCnpj(Long idPessoa, String cnpj, Long codEmpresa) {
		StringBuilder sql = new StringBuilder();
		sql.append("select p from PessoaJuridica p where p.cnpj =:cnpj ");
		sql.append("and p.codigoEmpresa=:codigoEmpresa and p.pessoa.dataExclusao is null ");
		if(idPessoa != null) {
			sql.append("and p.id !=:idPessoa ");
		}
		sql.append("order by p.pessoa.descricao asc");

		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter("cnpj", cnpj);
		query.setParameter(CODIGO_EMPRESA, codEmpresa);
		if(idPessoa != null) {
			query.setParameter("idPessoa", idPessoa);
		}
		return query.getResultList();
	}
}
