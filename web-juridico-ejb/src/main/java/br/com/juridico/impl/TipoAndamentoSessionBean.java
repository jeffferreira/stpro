package br.com.juridico.impl;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.TipoAndamento;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;

/**
 * 
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class TipoAndamentoSessionBean extends AbstractCrudSessionBean<TipoAndamento> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3246638270853559888L;
	private static final String DESCRICAO = "descricao";
	private static final String TIPO_ANDAMENTO_FIND_BY_DESCRICAO = "TipoAndamento.findByDescricao";
	private static final String CODIGO_EMPRESA = "codigoEmpresa";

	public TipoAndamentoSessionBean() {
		super();
	}

	/**
	 * 
	 * @param entityManager
	 */
	@Inject
	public TipoAndamentoSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(TipoAndamento.class, entityManager));
	}

	@Override
	public OrderClause[] getDefaultOrder() {
		return new OrderClause[] { new OrderClause(DESCRICAO, OrderType.ASC) };
	}

	public List<TipoAndamento> filtrados(ConsultaLazyFilter filtro) {
		filtro.setAscendente(true);
		filtro.setPropriedadeOrdenacao(DESCRICAO);
		return super.filtrados(filtro, TipoAndamento.class);
	}

	public int quantidadeFiltrados(ConsultaLazyFilter filtro) {
		return super.quantidadeFiltrados(filtro, TipoAndamento.class);
	}

	/**
	 * 
	 * @param descricao
	 * @param codigoEmpresa
	 * @return
	 */
	public TipoAndamento findByDescricao(String descricao, Long codigoEmpresa) {
		Query query = getDao().getEntityManager().createNamedQuery(TIPO_ANDAMENTO_FIND_BY_DESCRICAO)
				.setParameter(DESCRICAO, descricao).setParameter(CODIGO_EMPRESA, codigoEmpresa);
		try {
			return (TipoAndamento) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public boolean isDescricaoJaCadastrada(TipoAndamento tipoAndamento, Long codigoEmpresa){
		StringBuilder sql = new StringBuilder();
		sql.append("select t from TipoAndamento t where t.descricao=:descricao and t.codigoEmpresa=:codigoEmpresa and t.dataExclusao is null");

		Query query = getDao().getEntityManager().createQuery(sql.toString())
				.setParameter(DESCRICAO, tipoAndamento.getDescricao()).setParameter(CODIGO_EMPRESA, codigoEmpresa);

		List<TipoAndamento> results = query.getResultList();

		if(results.isEmpty()) {
			return Boolean.FALSE;
		}
		return validaDuplicidade(tipoAndamento, results.get(0));
	}

	private boolean validaDuplicidade(TipoAndamento tipoAndamento, TipoAndamento result) {
		if(result != null && tipoAndamento.getId() == result.getId()) {
			return Boolean.FALSE;
		} else if(result != null && tipoAndamento.getId() != result.getId()){
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
}
