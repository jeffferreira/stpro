package br.com.juridico.impl;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.Andamento;
import br.com.juridico.entidades.AndamentoAdvogado;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;

/**
 * 
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class AndamentoAdvogadoSessionBean extends AbstractSessionBean<AndamentoAdvogado> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 187799618209962648L;

	public AndamentoAdvogadoSessionBean() {
		super();
	}

	@Inject
	public AndamentoAdvogadoSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(AndamentoAdvogado.class, entityManager));
	}

	@Override
	public OrderClause[] getDefaultOrder() {
		return new OrderClause[] { new OrderClause("id", OrderType.ASC) };
	}

	@Override
	public List<AndamentoAdvogado> retrieveAll(Long codigoEmpresa) {
		String sql = "select a from AndamentoAdvogado a where a.codigoEmpresa=:codigoEmpresa and a.dataExclusao is null";
		return retrieveByExclusaoLogica(sql, codigoEmpresa);
	}

	@SuppressWarnings("unchecked")
	public List<AndamentoAdvogado> findByAndamento(Andamento andamento, Long codigoEmpresa) {
		StringBuilder sb = new StringBuilder();
		sb.append(
				"select aa from AndamentoAdvogado aa where aa.codigoEmpresa=:codigoEmpresa and aa.andamento.id =:idAndamento \n");

		Query query = getDao().getEntityManager().createQuery(sb.toString())
				.setParameter("idAndamento", andamento.getId()).setParameter("codigoEmpresa", codigoEmpresa);

		return query.getResultList();
	}

}
