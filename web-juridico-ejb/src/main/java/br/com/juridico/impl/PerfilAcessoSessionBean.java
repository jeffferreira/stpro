package br.com.juridico.impl;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.Direito;
import br.com.juridico.entidades.Perfil;
import br.com.juridico.entidades.PerfilAcesso;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

/**
 *
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class PerfilAcessoSessionBean extends AbstractSessionBean<PerfilAcesso> {


	/**
	 *
	 */
	private static final long serialVersionUID = -3833477281795653214L;

	public PerfilAcessoSessionBean() {
		super();
	}

	/**
	 *
	 * @param entityManager
	 */
	@Inject
	public PerfilAcessoSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(PerfilAcesso.class, entityManager));
	}

	/**
	 *
	 * @param perfilId
	 * @param codigoEmpresa
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<PerfilAcesso> findByPerfil(Long perfilId, Long codigoEmpresa) {
		String sql = "select t from PerfilAcesso t where t.perfil.id=:perfilId and t.codigoEmpresa=:codigoEmpresa and t.dataExclusao is null";
		Query query = getDao().getEntityManager().createQuery(sql);
		query.setParameter("perfilId", perfilId);
		query.setParameter("codigoEmpresa", codigoEmpresa);
		return query.getResultList();
	}

	/**
	 *
	 * @param perfilId
	 * @param acessoId
	 * @param codigoEmpresa
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public PerfilAcesso findByPerfilAndAcesso(Long perfilId, Long acessoId, Long codigoEmpresa) {
		String sql = "select t from PerfilAcesso t where t.perfil.id=:perfilId and t.acesso.id=:acessoId and t.codigoEmpresa=:codigoEmpresa";
		Query query = getDao().getEntityManager().createQuery(sql);
		query.setParameter("perfilId", perfilId);
		query.setParameter("acessoId", acessoId);
		query.setParameter("codigoEmpresa", codigoEmpresa);

		List<PerfilAcesso> results = query.getResultList();

		if (!results.isEmpty()) {
			return results.get(0);
		}

		return null;
	}

	public List<PerfilAcesso> findByMenu(Long idMenu) {
		StringBuilder sql = new StringBuilder();
		sql.append("select t from PerfilAcesso t ");
		sql.append("left join t.acesso a ");
		sql.append("where a.menu.id =:idMenu ");
		sql.append("order by a.descricao asc ");

		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter("idMenu", idMenu);

		return query.getResultList();
	}

	/**
	 *
	 * @param perfil
	 * @param tela
	 * @param direito
	 * @param codigoEmpresa
     * @return
     */
	public boolean possuiDireitoAcesso(Perfil perfil, String tela, Direito direito, Long codigoEmpresa){
		StringBuilder sql = new StringBuilder();
		sql.append("select dp.id_direito_perfil from pfl_perfil_acesso pa ");
		sql.append("inner join pfl_acesso a on a.id_acesso = pa.fk_acesso ");
		sql.append("inner join pfl_perfil p on p.id_perfil = pa.fk_perfil ");
		sql.append("inner join pfl_direito_perfil dp on dp.fk_perfil_acesso = pa.id_perfil_acesso ");
		sql.append("where p.id_perfil =:perfil ");
		sql.append("and a.ds_funcionalidade =:tela ");
		sql.append("and dp.fk_direito =:direito ");
		sql.append("and pa.nr_empresa =:codigoEmpresa and dp.nr_empresa=:codigoEmpresa");

		Query query = getDao().getEntityManager().createNativeQuery(sql.toString());
		query.setParameter("perfil", perfil.getId());
		query.setParameter("direito", direito.getId());
		query.setParameter("tela", tela);
		query.setParameter("codigoEmpresa", codigoEmpresa);

		return !query.getResultList().isEmpty();
	}

}
