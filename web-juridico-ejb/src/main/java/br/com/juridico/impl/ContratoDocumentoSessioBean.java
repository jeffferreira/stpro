package br.com.juridico.impl;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.ContratoDocumento;
import br.com.juridico.entidades.ContratoPessoa;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by Jefferson on 21/05/2016.
 */
@Stateless
@LocalBean
public class ContratoDocumentoSessioBean extends AbstractSessionBean<ContratoDocumento> {

	/**
	 *
	 */
	private static final long serialVersionUID = 437500041588525462L;

	public ContratoDocumentoSessioBean() {
		super();
	}

	/**
	 *
	 * @param entityManager
	 */
	@Inject
	public ContratoDocumentoSessioBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(ContratoDocumento.class, entityManager));
	}

	@Override
	public OrderClause[] getDefaultOrder() {
		return new OrderClause[] { new OrderClause("descricao", OrderType.ASC) };
	}

    /**
     *
     * @param idContratoPessoa
     * @param codigoEmpresa
     * @return
     */
    public List<ContratoDocumento> findByContratoPessoa(Long idContratoPessoa, Long codigoEmpresa) {
        String sql = "select c from ContratoDocumento c where c.contratoPessoa.id =:idContratoPessoa and c.codigoEmpresa=:codigoEmpresa";
        Query query = getDao().getEntityManager().createQuery(sql)
                .setParameter("idContratoPessoa", idContratoPessoa)
                .setParameter("codigoEmpresa", codigoEmpresa);
        return query.getResultList();
    }

}
