package br.com.juridico.impl;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.EventoRejeito;
import br.com.juridico.entidades.Pessoa;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by jefferson on 07/06/2018.
 */
@Stateless
@LocalBean
public class EventoRejeitoSessionBean extends AbstractSessionBean<EventoRejeito> {

    public EventoRejeitoSessionBean() {
        super();
    }

    @Inject
    public EventoRejeitoSessionBean(@JuridicoEntityManager EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected void initDaoSet(EntityManager entityManager) {
        setDao(new GenericDao<>(EventoRejeito.class, entityManager));
    }

    public List<EventoRejeito> findRejeitosByUsuario(Pessoa pessoa){
        String sql = "select r from EventoRejeito r where r.quemCadastrou =:quemCadastrou and r.codigoEmpresa=:codigoEmpresa order by r.dataRejeito asc ";
        Query query = getDao().getEntityManager().createQuery(sql);
        query.setParameter("quemCadastrou", pessoa.getLogin().getLogin());
        query.setParameter("codigoEmpresa", pessoa.getCodigoEmpresa());
        List<EventoRejeito> results = query.getResultList();
        if(results.isEmpty()){
            return results;
        }
        return results.stream().filter(r -> r.getEvento().getStatusAgenda().isStatusRejeitado()).collect(Collectors.toList());
    }

}
