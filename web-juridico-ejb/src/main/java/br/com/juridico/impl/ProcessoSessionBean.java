package br.com.juridico.impl;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.*;
import br.com.juridico.factory.ContextProcessoSearchFactory;
import br.com.juridico.filter.ProcessoFilter;
import com.google.common.base.Strings;
import org.apache.commons.lang.ArrayUtils;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class ProcessoSessionBean extends AbstractSessionBean<Processo> {

	/**
	 *
	 */
	private static final long serialVersionUID = 6638941542426771110L;

	private static final String CODIGO_EMPRESA = "codigoEmpresa";
	private static final String ID_PROCESSO = "idProcesso";
	public static final String PROCESSO_FIND_BYID = "Processo.findByid";

	public ProcessoSessionBean() {
		super();
	}

	/**
	 *
	 * @param entityManager
	 */
	@Inject
	public ProcessoSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(Processo.class, entityManager));
	}

	/**
	 *
	 * @param idProcesso
	 * @param codigoEmpresa
	 * @return
	 */
	public Processo findById(Long idProcesso, Long codigoEmpresa) {
		Query query = getDao().getEntityManager().createNamedQuery(PROCESSO_FIND_BYID)
				.setParameter("idProcesso", idProcesso).setParameter(CODIGO_EMPRESA, codigoEmpresa);
		try {
			return (Processo) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	/**
	 *
	 */
	public List<Processo> retrieveAll(Long codigoEmpresa, Long idProcesso) {
		StringBuilder sql = new StringBuilder();
		sql.append("select t from Processo t where t.codigoEmpresa=:codigoEmpresa ");
		if(idProcesso != null) {
			sql.append("and t.id <> :idProcesso ");
		}

		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter("codigoEmpresa", codigoEmpresa);
		if(idProcesso != null) {
			query.setParameter("idProcesso", idProcesso);
		}

		return query.getResultList();
	}

	public List<Processo> findListByDescricaoProcesso(String descricao, Long idProcesso, Long codigoEmpresa){
		StringBuilder sql = new StringBuilder();
		sql.append("select t from Processo t where t.descricao=:descricao and t.codigoEmpresa=:codigoEmpresa ");
		if(idProcesso != null) {
			sql.append("and t.id <> :idProcesso ");
		}
		Query query = getDao().getEntityManager().createQuery(sql.toString())
				.setParameter("descricao", descricao)
				.setParameter("codigoEmpresa", codigoEmpresa);
		if(idProcesso != null) {
			query.setParameter("idProcesso", idProcesso);
		}
		return query.getResultList();
	}

	/**
	 *
	 * @param filter
	 * @param codigoEmpresa
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<ViewProcesso> findByFilters(ProcessoFilter filter, Long codigoEmpresa) {
		StringBuilder sql = new StringBuilder();
		List<Long> lista = null;

		sql.append("select p from ViewProcesso p \n");
		sql.append("where p.numeroEmpresa=:codigoEmpresa \n");

		if (filter.getStatus() != null && !filter.getStatus().isEmpty()) {
			sql.append("and p.status=:status \n");
		}

		if (filter.getProcesso() != null && !filter.getProcesso().isEmpty()) {
			sql.append("and p.processo like :processo \n");
		}

		lista = getIdsProcessos(filter, codigoEmpresa, sql, lista);
		sql.append("order by p.dataAbertura DESC");

		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter(CODIGO_EMPRESA, codigoEmpresa);
		if (filter.getStatus() != null && !filter.getStatus().isEmpty()) {
			query.setParameter("status", filter.getStatus());
		}
		if (filter.getProcesso() != null && !filter.getProcesso().isEmpty()) {
			query.setParameter("processo", "%".concat(filter.getProcesso().concat("%")));
		}
		if (((filter.getCliente() != null && !filter.getCliente().isEmpty()) || (filter.getAdverso() != null && !filter.getAdverso().isEmpty())) && lista != null) {
			query.setParameter("processoIds", lista);
		}

		return query.getResultList();
	}

	public List<ViewProcesso> findByTopValue(int maxResult, Long codigoEmpresa) {
		StringBuilder sql = new StringBuilder();
		sql.append("select p from ViewProcesso p \n")
				.append("where p.numeroEmpresa=:codigoEmpresa \n")
				.append("order by p.dataCadastro desc");

		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter(CODIGO_EMPRESA, codigoEmpresa);
		query.setMaxResults(maxResult);

		return query.getResultList();
	}

	/**
	 *
	 * @param ids
	 * @param filtro
	 * @param codigoEmpresa
	 * @return
	 */
	public List<Processo> findAutoCompleteProcesso(List<BigInteger> ids, String filtro, Long codigoEmpresa){
		StringBuilder sql = new StringBuilder();

		sql.append("select p from Processo p ");
		sql.append("where p.codigoEmpresa =:codigoEmpresa and p.status= 'A' ");
		if(!ids.isEmpty()) {
			sql.append("and p.id in (:processoIds) ");
		}
		if(!Strings.isNullOrEmpty(filtro)) {
			sql.append("and p.descricao like :filtro ");
		}

		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter(CODIGO_EMPRESA, codigoEmpresa);
		if(!ids.isEmpty()) {
			query.setParameter("processoIds", Arrays.asList(getProcessoLongIds(ids)));
		}
		if(!Strings.isNullOrEmpty(filtro)) {
			query.setParameter("filtro", filtro);
		}
		return query.getResultList();
	}

	private Long[] getProcessoLongIds(List<BigInteger> ids){
		Long[] processoIds = null;
		if (!ids.isEmpty()) {
			int i = 0;
			processoIds = new Long[ids.size()];
			for (BigInteger id : ids) {
				processoIds[i] = id.longValue();
				i++;
			}
		}
		return processoIds;
	}

	private List<Long> getIdsProcessos(ProcessoFilter filter, Long codigoEmpresa, StringBuilder sql, List<Long> lista) {
		Long[] processosIds = null;
		Long[] processoIdsClientes = null;
		Long[] processoIdsAdversos = null;

		if (filter.getCliente() != null && !filter.getCliente().isEmpty()) {
			processoIdsClientes = findByClientes(filter.getCliente(), codigoEmpresa);
		}

		if (filter.getAdverso() != null && !filter.getAdverso().isEmpty()) {
			processoIdsAdversos = findByAdversos(filter.getAdverso(), codigoEmpresa);
		}

		//TODO PRECISO QUE SEJA ADICIONADO EM processosIds SÓ OS ELEMENTOS QUE CONTÉM EM processosIdsClientes E processosIdsAdversos
		//TODO PORQUE SÓ VAI TRAZER OS RESULTADOS QUE CONTEM O CLIENTE DIGITADO E O ADVERSO

		processosIds = (Long[]) ArrayUtils.addAll(processoIdsClientes, processoIdsAdversos);

		if ((filter.getCliente() != null && !filter.getCliente().isEmpty()) || (filter.getAdverso() != null && !filter.getAdverso().isEmpty())) {
			if (processosIds != null) {
				lista = Arrays.asList(processosIds);
				sql.append(" and p.id in (:processoIds) \n");
			} else {
				sql.append(" and p.id in (-1) \n");
			}
		}

		return lista;
	}

	private Long[] findByClientes(String cliente, Long codigoEmpresa) {
		StringBuilder sql = new StringBuilder();
		Long[] processoIds = null;

		sql.append("select p from ViewCliente p \n");
		sql.append("where p.numeroEmpresa=:codigoEmpresa \n");
		sql.append("and p.cliente like :cliente");

		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter(CODIGO_EMPRESA, codigoEmpresa);
		query.setParameter("cliente", "%".concat(cliente.concat("%")));

		List<ViewCliente> clientes = query.getResultList();

		if (!clientes.isEmpty()) {
			int i = 0;
			processoIds = new Long[clientes.size()];
			for (ViewCliente c : clientes) {
				processoIds[i] = c.getId();
				i++;
			}
		}
		return processoIds;
	}

	private Long[] findByAdversos(String adverso, Long codigoEmpresa) {
		StringBuilder sql = new StringBuilder();
		Long[] processoIds = null;

		sql.append("select p from ViewAdverso p \n");
		sql.append("where p.codigoEmpresa=:codigoEmpresa \n");
		sql.append("and p.adverso like :adverso");

		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter(CODIGO_EMPRESA, codigoEmpresa);
		query.setParameter("adverso", "%".concat(adverso.concat("%")));

		List<ViewAdverso> adversos = query.getResultList();

		if (!adversos.isEmpty()) {
			int i = 0;
			processoIds = new Long[adversos.size()];
			for (ViewAdverso c : adversos) {
				processoIds[i] = c.getProcesso().getId();
				i++;
			}
		}
		return processoIds;
	}

	/**
	 *
	 * @param idProcesso
	 * @return
	 */
	public boolean isProcessoComoAutor(Long idProcesso) {
		String sql = "select id from vw_proc_cliente v where v.id =:idProcesso";
		Query query = getDao().getEntityManager().createNativeQuery(sql);
		query.setParameter(ID_PROCESSO, idProcesso);

		return !query.getResultList().isEmpty();

	}

	/**
	 *
	 * @param idProcesso
	 * @return
	 */
	public boolean isProcessoComoReu(Long idProcesso) {
		String sql = "select id from vw_proc_adverso v where v.id =:idProcesso";
		Query query = getDao().getEntityManager().createNativeQuery(sql);
		query.setParameter(ID_PROCESSO, idProcesso);

		return !query.getResultList().isEmpty();
	}

	/**
	 *
	 * @param idProcesso
	 * @return
	 */
	public boolean isProcessoComoTerceiro(Long idProcesso) {
		String sql = "select id from vw_proc_terceiro v where v.id =:idProcesso";
		Query query = getDao().getEntityManager().createNativeQuery(sql);
		query.setParameter(ID_PROCESSO, idProcesso);

		return !query.getResultList().isEmpty();
	}

	/**
	 *
	 * @param filterValue
	 * @param codigoEmpresa
	 * @return
	 */
	public List<ViewProcesso> findByConsultaAvancada(String filterValue, Long codigoEmpresa) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<ViewProcesso> criteriaQuery = cb.createQuery(ViewProcesso.class);
		Root<ViewProcesso> root = criteriaQuery.from(ViewProcesso.class);

		Predicate condition = ContextProcessoSearchFactory.criterios(filterValue, codigoEmpresa, cb, root);

		Long[] processoClienteIds = findByClientes(filterValue, codigoEmpresa);
		if (processoClienteIds != null) {
			condition = cb.or(condition, root.get("id").in(processoClienteIds));
		}

		Long[] processoAdversoIds = findByAdversos(filterValue, codigoEmpresa);
		if (processoAdversoIds != null) {
			condition = cb.or(condition, root.get("id").in(processoAdversoIds));
		}

		criteriaQuery.where(condition);
		criteriaQuery.orderBy(cb.desc(root.get("dataAbertura")));
		return entityManager.createQuery(criteriaQuery).getResultList();
	}

	public List<Processo> findByDescricao(Long idProcesso, String descricao, Long codigoEmpresa){
		StringBuilder sql = new StringBuilder();
		sql.append("select t from Processo t where t.descricao=:descricao and t.id!=:idProcesso and t.codigoEmpresa=:codigoEmpresa ");

		Query query = getDao().getEntityManager().createQuery(sql.toString())
				.setParameter("descricao", descricao)
				.setParameter("codigoEmpresa", codigoEmpresa)
				.setParameter("idProcesso", idProcesso != null ? idProcesso : 0L);

		return query.getResultList();
	}

	public List<Processo> findProcessosByMeioTramitacao(MeioTramitacao meioTramitacao, Long codigoEmpresa){
		StringBuilder sql = new StringBuilder();
		sql.append("select t from Processo t ")
				.append("inner join t.meioTramitacao mt ")
				.append("where mt.id =:meioTramitacao ")
				.append("and t.codigoEmpresa=:codigoEmpresa");

		Query query = getDao().getEntityManager().createQuery(sql.toString())
				.setParameter("meioTramitacao", meioTramitacao.getId())
				.setParameter("codigoEmpresa", codigoEmpresa);

		return query.getResultList();
	}

	public ViewProcesso findByProcessoAndDataCadastro(String processo, Date dataCadastro){
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<ViewProcesso> criteriaQuery = cb.createQuery(ViewProcesso.class);
		Root<ViewProcesso> root = criteriaQuery.from(ViewProcesso.class);

		Predicate condition = cb.equal(root.get("processo"), processo);
		condition = cb.and(condition, cb.equal(root.get("dataCadastro"), dataCadastro));

		criteriaQuery.where(condition);
		return entityManager.createQuery(criteriaQuery).getSingleResult();
	}

}
