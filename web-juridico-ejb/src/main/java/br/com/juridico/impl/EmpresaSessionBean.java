package br.com.juridico.impl;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.Empresa;
import com.google.common.base.Strings;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

/**
 *
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class EmpresaSessionBean extends AbstractSessionBean<Empresa> {

	/**
	 *
	 */
	private static final long serialVersionUID = -8284502709282755391L;



	public EmpresaSessionBean() {
		super();
	}

	/**
	 *
	 * @param entityManager
	 */
	@Inject
	public EmpresaSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(Empresa.class, entityManager));
	}

	public List<Empresa> findAllEmpresas(){
		String sql = "select e from Empresa e ";
		Query query = getDao().getEntityManager().createQuery(sql);
		return query.getResultList();
	}

	public List<Empresa> findAllFiliais(Long codigoEmpresa) {
		String sql = "SELECT e FROM Empresa e WHERE e.matriz =:codigoEmpresa ";
		Query query = getDao().getEntityManager().createQuery(sql).setParameter("codigoEmpresa", codigoEmpresa);
		return  query.getResultList();
	}

	public List<Empresa> findMatrizAndAllFiliais(String filter, Long codigoEmpresa) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT e FROM Empresa e WHERE e.id =:codigoEmpresa or e.matriz =:codigoEmpresa ");

		if (!Strings.isNullOrEmpty(filter)) {
			sql.append("and e.descricao like :descricao ");
		}

		Query query = getDao().getEntityManager().createQuery(sql.toString()).setParameter("codigoEmpresa", codigoEmpresa);

		if (!Strings.isNullOrEmpty(filter)) {
			query.setParameter("descricao", "%"+filter+"%");
		}

		return  query.getResultList();
	}
}
