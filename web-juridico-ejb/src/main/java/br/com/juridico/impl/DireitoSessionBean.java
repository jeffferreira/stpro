package br.com.juridico.impl;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.Direito;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;

import java.util.List;

/**
 * 
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class DireitoSessionBean extends AbstractSessionBean<Direito> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3833477281795653214L;

	public DireitoSessionBean() {
		super();
	}

	/**
	 * 
	 * @param entityManager
	 */
	@Inject
	public DireitoSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(Direito.class, entityManager));
	}

	@Override
	public OrderClause[] getDefaultOrder() {
		return new OrderClause[] { new OrderClause("descricao", OrderType.ASC) };
	}

	public List<Direito> findAll(){
		String sql = "select t from Direito t ";
		Query query = getDao().getEntityManager().createQuery(sql);
		return query.getResultList();
	}

}
