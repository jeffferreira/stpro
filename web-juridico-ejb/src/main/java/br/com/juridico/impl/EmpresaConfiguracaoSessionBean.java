package br.com.juridico.impl;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.EmpresaConfiguracao;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;

/**
 * 
 * @author Marcos
 *
 */
@Stateless
@LocalBean
public class EmpresaConfiguracaoSessionBean extends AbstractSessionBean<EmpresaConfiguracao> {

	public EmpresaConfiguracaoSessionBean() {
		super();
	}

	@Inject
	public EmpresaConfiguracaoSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(EmpresaConfiguracao.class, entityManager));
	}

	@Override
	public OrderClause[] getDefaultOrder() {
		return new OrderClause[] { new OrderClause("id", OrderType.ASC) };
	}

	@Override
	public List<EmpresaConfiguracao> retrieveAll(Long codigoEmpresa) {
		String sql = "select ec from EmpresaConfiguracao ec";
		return retrieveByExclusaoLogica(sql, codigoEmpresa);
	}

	@SuppressWarnings("unchecked")
	public List<EmpresaConfiguracao> findByEmpresa(Long codigoEmpresa) {
		StringBuilder sb = new StringBuilder();
		sb.append(
				"select ec from EmpresaConfiguracao ec where ec.empresa.id=:codigoEmpresa \n");

		Query query = getDao().getEntityManager().createQuery(sb.toString())
				.setParameter("codigoEmpresa", codigoEmpresa);

		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<EmpresaConfiguracao> findByConfiguracao(Long codigoConfiguracao) {
		StringBuilder sb = new StringBuilder();
		sb.append(
				"select ec from EmpresaConfiguracao ec where ec.configuracao.id=:codigoConfiguracao \n");

		Query query = getDao().getEntityManager().createQuery(sb.toString())
				.setParameter("codigoConfiguracao", codigoConfiguracao);

		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public EmpresaConfiguracao findByEmpresaAndConfiguracao(Long codigoEmpresa, Long codigoConfiguracao) {
		StringBuilder sb = new StringBuilder();
		sb.append(
				"select ec from EmpresaConfiguracao ec where ec.empresa.id=:codigoEmpresa and ec.configuracao.id=:codigoConfiguracao \n");

		Query query = getDao().getEntityManager().createQuery(sb.toString()).setParameter("codigoEmpresa", codigoEmpresa)
				.setParameter("codigoConfiguracao", codigoConfiguracao);

		try {
            return (EmpresaConfiguracao) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
	}
}
