package br.com.juridico.impl;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.TipoCusto;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;

/**
 * 
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class TipoCustoSessionBean extends AbstractCrudSessionBean<TipoCusto> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7927916039551021034L;
	private static final String DESCRICAO = "descricao";
	private static final String TIPO_CUSTO_FIND_BY_DESCRICAO = "TipoCusto.findByDescricao";
	private static final String CODIGO_EMPRESA = "codigoEmpresa";

	public TipoCustoSessionBean() {
		super();
	}

	/**
	 * 
	 * @param entityManager
	 */
	@Inject
	public TipoCustoSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(TipoCusto.class, entityManager));
	}

	public List<TipoCusto> filtrados(ConsultaLazyFilter filtro) {
		filtro.setAscendente(true);
		filtro.setPropriedadeOrdenacao(DESCRICAO);
		return super.filtrados(filtro, TipoCusto.class);
	}

	public int quantidadeFiltrados(ConsultaLazyFilter filtro) {
		return super.quantidadeFiltrados(filtro, TipoCusto.class);
	}

	@Override
	public OrderClause[] getDefaultOrder() {
		return new OrderClause[] { new OrderClause(DESCRICAO, OrderType.ASC) };
	}


	/**
	 * 
	 * @param descricao
	 * @param codigoEmpresa
	 * @return
	 */
	public TipoCusto findByDescricao(String descricao, Long codigoEmpresa) {
		Query query = getDao().getEntityManager().createNamedQuery(TIPO_CUSTO_FIND_BY_DESCRICAO)
				.setParameter(DESCRICAO, descricao).setParameter(CODIGO_EMPRESA, codigoEmpresa);
		try {
			return (TipoCusto) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public boolean isDescricaoJaCadastrada(TipoCusto tipoCusto, Long codigoEmpresa){
		StringBuilder sql = new StringBuilder();
		sql.append("select t from TipoCusto t where t.descricao=:descricao and t.codigoEmpresa=:codigoEmpresa and t.dataExclusao is null");

		Query query = getDao().getEntityManager().createQuery(sql.toString())
				.setParameter(DESCRICAO, tipoCusto.getDescricao()).setParameter(CODIGO_EMPRESA, codigoEmpresa);

		List<TipoCusto> results = query.getResultList();

		if(results.isEmpty()) {
			return Boolean.FALSE;
		}
		return validaDuplicidade(tipoCusto, results.get(0));
	}

	private boolean validaDuplicidade(TipoCusto tipoCusto, TipoCusto result) {
		if(result != null && tipoCusto.getId() == result.getId()) {
			return Boolean.FALSE;
		} else if(result != null && tipoCusto.getId() != result.getId()){
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
}
