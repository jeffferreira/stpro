package br.com.juridico.impl;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.Parametro;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by Jefferson on 30/06/2016.
 */
@Stateless
@LocalBean
public class ParametroSessionBean extends AbstractSessionBean<Parametro> {

    public ParametroSessionBean() {
        super();
    }

    /**
     *
     * @param entityManager
     */
    @Inject
    public ParametroSessionBean(@JuridicoEntityManager EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected void initDaoSet(EntityManager entityManager) {
        setDao(new GenericDao<>(Parametro.class, entityManager));
    }

    @Override
    public OrderClause[] getDefaultOrder() {
        return new OrderClause[] { new OrderClause("descricao", OrderType.ASC) };
    }

    @Override
    public List<Parametro> retrieveAll(Long codigoEmpresa) {
        String sql = "select t from Parametro t where t.codigoEmpresa=:codigoEmpresa";
        Query query = getDao().getEntityManager().createQuery(sql).setParameter("codigoEmpresa", codigoEmpresa);
        return query.getResultList();
    }

    public Parametro findBySigla(String sigla, Long codigoEmpresa) {
        String sql = "select t from Parametro t where t.sigla=:sigla and t.codigoEmpresa=:codigoEmpresa";
        Query query = getDao().getEntityManager().createQuery(sql)
                .setParameter("sigla", sigla)
                .setParameter("codigoEmpresa", codigoEmpresa);

        try{
            return (Parametro) query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        }
    }
}
