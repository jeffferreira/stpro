package br.com.juridico.impl;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.RelatorioCriterioProcesso;
import br.com.juridico.entidades.RelatorioFiltroProcesso;
import br.com.juridico.entidades.ViewProcesso;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;
import com.google.common.base.Strings;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

/**
 *
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class RelatorioProcessoFiltroSessionBean extends AbstractCrudSessionBean<RelatorioFiltroProcesso> {

	/**
	 *
	 */
	private static final long serialVersionUID = 6812000707435610578L;

	private static final String CODIGO_EMPRESA = "codigoEmpresa";
	private static final String TEXT = "text";
	private static final String PERIODO_DATA = "periodoData";
	private static final String PERIODO_VALOR = "periodoValor";

	public RelatorioProcessoFiltroSessionBean() {
		super();
	}

	/**
	 *
	 * @param entityManager
	 */
	@Inject
	public RelatorioProcessoFiltroSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(RelatorioFiltroProcesso.class, entityManager));
	}

	@Override
	public OrderClause[] getDefaultOrder() {
		return new OrderClause[] { new OrderClause("descricao", OrderType.ASC) };
	}

	@Override
	public List<RelatorioFiltroProcesso> retrieveAll(Long codigoEmpresa) {
		StringBuilder sql = new StringBuilder();
		sql.append("select r from RelatorioFiltroProcesso r where r.codigoEmpresa =:codigoEmpresa and r.dataExclusao is null order by r.descricao asc");
		Query query = getDao().getEntityManager().createQuery(sql.toString()).setParameter(CODIGO_EMPRESA, codigoEmpresa);
		return query.getResultList();
	}

	public List<ViewProcesso> findByCriterios(Long codigoEmpresa, List<RelatorioCriterioProcesso> relatorioCriterioProcessos) {
		StringBuilder sql = new StringBuilder();
		sql.append("select v from ViewProcesso v where v.numeroEmpresa =:codigoEmpresa ");

		for (RelatorioCriterioProcesso criterioProcesso : relatorioCriterioProcessos) {
			if (TEXT.equalsIgnoreCase(criterioProcesso.getTipoCampo())) {
				adicionarCriterioText(criterioProcesso, sql);
			} else if(PERIODO_DATA.equalsIgnoreCase(criterioProcesso.getTipoCampo())) {
				adicionarCriterioPeriodoData(criterioProcesso, sql);
			} else if (PERIODO_VALOR.equalsIgnoreCase(criterioProcesso.getTipoCampo())) {
				adicionarCriterioValor(criterioProcesso, sql);
			}
		}
		Query query = getDao().getEntityManager().createQuery(sql.toString()).setParameter(CODIGO_EMPRESA, codigoEmpresa);

		return query.getResultList();
	}

	private void adicionarCriterioValor(RelatorioCriterioProcesso criterioProcesso, StringBuilder sql) {
		if (criterioProcesso.getValorPeriodoNumberUm() != null && criterioProcesso.getValorPeriodoNumberDois() != null) {
			sql.append(criterioProcesso.getCriterioType().getComando() + " v." + criterioProcesso.getCampo() + " >= " + criterioProcesso.getValorPeriodoNumberUm() + " AND " + criterioProcesso.getCampo() + " <= " + criterioProcesso.getValorPeriodoNumberDois());
		} else if (criterioProcesso.getValorPeriodoNumberUm() != null && criterioProcesso.getValorPeriodoNumberDois() == null) {
			sql.append(criterioProcesso.getCriterioType().getComando() + " v." + criterioProcesso.getCampo() + " >= " + criterioProcesso.getValorPeriodoNumberUm());
		} else {
			sql.append(criterioProcesso.getCriterioType().getComando() + " v." + criterioProcesso.getCampo() + " <= " + criterioProcesso.getValorPeriodoNumberDois());
		}
	}

	private void adicionarCriterioPeriodoData(RelatorioCriterioProcesso criterioProcesso, StringBuilder sql) {
		if (criterioProcesso.getValorPeriodoUm() != null && criterioProcesso.getValorPeriodoDois() != null) {
			sql.append(criterioProcesso.getCriterioType().getComando() + " v." + criterioProcesso.getCampo() + " BETWEEN '" + criterioProcesso.getValorPeriodoUm().toInstant() + "' AND '" + criterioProcesso.getValorPeriodoDois().toInstant() + "'");
		} else if (criterioProcesso.getValorPeriodoUm() != null && criterioProcesso.getValorPeriodoDois() == null) {
			sql.append(criterioProcesso.getCriterioType().getComando() + " v." + criterioProcesso.getCampo() + " >= '" + criterioProcesso.getValorPeriodoUm().toInstant() + "'");
		} else {
			sql.append(criterioProcesso.getCriterioType().getComando() + " v." + criterioProcesso.getCampo() + " <= '" + criterioProcesso.getValorPeriodoDois().toInstant() + "'");
		}
	}

	private void adicionarCriterioText(RelatorioCriterioProcesso criterioProcesso, StringBuilder sql) {
		if (!Strings.isNullOrEmpty(criterioProcesso.getValorString())) {
			sql.append(criterioProcesso.getCriterioType().getComando() + " v." + criterioProcesso.getCampo() + " LIKE '%" + criterioProcesso.getValorString() + "%' ");
		}
	}
}
