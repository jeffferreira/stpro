package br.com.juridico.impl;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.Contrato;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;

/**
 * 
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class ContratoSessionBean extends AbstractSessionBean<Contrato> {

	/**
	 *
	 */
	private static final long serialVersionUID = -3833477281795653214L;
	private static final String CONTRATO_FIND_BY_DESCRICAO = "Contrato.findByDescricao";
	private static final String DESCRICAO = "descricao";
	private static final String CODIGO_EMPRESA = "codigoEmpresa";

	public ContratoSessionBean() {
		super();
	}

	@Inject
	public ContratoSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(Contrato.class, entityManager));
	}

	@Override
	public OrderClause[] getDefaultOrder() {
		return new OrderClause[] { new OrderClause("descricao", OrderType.ASC) };
	}

	public List<Contrato> findAll(Long codigoEmpresa){
		StringBuilder sql = new StringBuilder();
		sql.append("select t from Contrato t where t.codigoEmpresa=:codigoEmpresa and t.dataExclusao is null");
		Query query = getDao().getEntityManager().createQuery(sql.toString())
				.setParameter(CODIGO_EMPRESA, codigoEmpresa);
		return query.getResultList();
	}

	public boolean isDescricaoJaCadastrada(Contrato contrato, Long codigoEmpresa){
		StringBuilder sql = new StringBuilder();
		sql.append("select t from Contrato t where t.descricao=:descricao and t.codigoEmpresa=:codigoEmpresa and t.dataExclusao is null");

		Query query = getDao().getEntityManager().createQuery(sql.toString())
				.setParameter(DESCRICAO, contrato.getDescricao()).setParameter(CODIGO_EMPRESA, codigoEmpresa);

		List<Contrato> results = query.getResultList();

		if(results.isEmpty()) {
			return Boolean.FALSE;
		}
		return validaDuplicidade(contrato, results.get(0));
	}

	/**
	 *
	 * @param descricao
	 * @param codigoEmpresa
	 * @return
	 */
	public Contrato findByDescricao(String descricao, Long codigoEmpresa) {
		Query query = getDao().getEntityManager().createNamedQuery(CONTRATO_FIND_BY_DESCRICAO)
				.setParameter(DESCRICAO, descricao).setParameter(CODIGO_EMPRESA, codigoEmpresa);
		try {
			return (Contrato) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	private boolean validaDuplicidade(Contrato contrato, Contrato result) {
		if(result != null && contrato.getId() == result.getId()) {
			return Boolean.FALSE;
		} else if(result != null && contrato.getId() != result.getId()){
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}


}
