package br.com.juridico.impl;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.Imagem;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 * 
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class ImagemSessionBean extends AbstractSessionBean<Imagem> {

	/**
	 *
	 */
	private static final long serialVersionUID = -8284502709282755391L;



	public ImagemSessionBean() {
		super();
	}

	/**
	 *
	 * @param entityManager
	 */
	@Inject
	public ImagemSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(Imagem.class, entityManager));
	}

}
