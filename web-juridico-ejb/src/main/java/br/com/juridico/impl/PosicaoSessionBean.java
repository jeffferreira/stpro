package br.com.juridico.impl;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.Posicao;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;

/**
 * 
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class PosicaoSessionBean extends AbstractCrudSessionBean<Posicao> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1926126227742273076L;
	private static final String DESCRICAO = "descricao";
	private static final String POSICAO_FIND_BY_DESCRICAO = "Posicao.findByDescricao";
	private static final String CODIGO_EMPRESA = "codigoEmpresa";

	public PosicaoSessionBean() {
		super();
	}

	/**
	 * 
	 * @param entityManager
	 */
	@Inject
	public PosicaoSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(Posicao.class, entityManager));
	}

	@Override
	public OrderClause[] getDefaultOrder() {
		return new OrderClause[] { new OrderClause(DESCRICAO, OrderType.ASC) };
	}

	public List<Posicao> filtrados(ConsultaLazyFilter filtro) {
		filtro.setAscendente(true);
		filtro.setPropriedadeOrdenacao(DESCRICAO);
		return super.filtrados(filtro, Posicao.class);
	}

	public int quantidadeFiltrados(ConsultaLazyFilter filtro) {
		return super.quantidadeFiltrados(filtro, Posicao.class);
	}

	/**
	 * 
	 * @param descricao
	 * @param codigoEmpresa
	 * @return
	 */
	public Posicao findByDescricao(String descricao, Long codigoEmpresa) {
		Query query = getDao().getEntityManager().createNamedQuery(POSICAO_FIND_BY_DESCRICAO)
				.setParameter(DESCRICAO, descricao).setParameter(CODIGO_EMPRESA, codigoEmpresa);
		try {
			return (Posicao) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public boolean isDescricaoJaCadastrada(Posicao posicao, Long codigoEmpresa){
		StringBuilder sql = new StringBuilder();
		sql.append("select t from Posicao t where t.descricao=:descricao and t.codigoEmpresa=:codigoEmpresa and t.dataExclusao is null");

		Query query = getDao().getEntityManager().createQuery(sql.toString())
				.setParameter(DESCRICAO, posicao.getDescricao()).setParameter(CODIGO_EMPRESA, codigoEmpresa);

		List<Posicao> results = query.getResultList();

		if(results.isEmpty()) {
			return Boolean.FALSE;
		}
		return validaDuplicidade(posicao, results.get(0));
	}

	private boolean validaDuplicidade(Posicao posicao, Posicao result) {
		if(result != null && posicao.getId() == result.getId()) {
			return Boolean.FALSE;
		} else if(result != null && posicao.getId() != result.getId()){
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
}
