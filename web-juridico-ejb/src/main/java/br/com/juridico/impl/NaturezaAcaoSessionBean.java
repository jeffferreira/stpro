package br.com.juridico.impl;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.NaturezaAcao;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;

/**
 * 
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class NaturezaAcaoSessionBean extends AbstractCrudSessionBean<NaturezaAcao> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1312187778604264067L;
	private static final String DESCRICAO = "descricao";
	private static final String NATUREZA_ACAO_FIND_BY_DESCRICAO = "NaturezaAcao.findByDescricao";
	private static final String CODIGO_EMPRESA = "codigoEmpresa";

	public NaturezaAcaoSessionBean() {
		super();
	}

	/**
	 * 
	 * @param entityManager
	 */
	@Inject
	public NaturezaAcaoSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(NaturezaAcao.class, entityManager));
	}

	@Override
	public OrderClause[] getDefaultOrder() {
		return new OrderClause[] { new OrderClause(DESCRICAO, OrderType.ASC) };
	}

	public List<NaturezaAcao> filtrados(ConsultaLazyFilter filtro) {
		filtro.setAscendente(true);
		filtro.setPropriedadeOrdenacao(DESCRICAO);
		return super.filtrados(filtro, NaturezaAcao.class);
	}

	public int quantidadeFiltrados(ConsultaLazyFilter filtro) {
		return super.quantidadeFiltrados(filtro, NaturezaAcao.class);
	}

	/**
	 * 
	 * @param descricao
	 * @param codigoEmpresa
	 * @return
	 */
	public NaturezaAcao findByDescricao(String descricao, Long codigoEmpresa) {
		Query query = getDao().getEntityManager().createNamedQuery(NATUREZA_ACAO_FIND_BY_DESCRICAO)
				.setParameter(DESCRICAO, descricao).setParameter(CODIGO_EMPRESA, codigoEmpresa);
		try {
			return (NaturezaAcao) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public boolean isDescricaoJaCadastrada(NaturezaAcao naturezaAcao, Long codigoEmpresa){
		StringBuilder sql = new StringBuilder();
		sql.append("select t from NaturezaAcao t where t.descricao=:descricao and t.codigoEmpresa=:codigoEmpresa and t.dataExclusao is null");

		Query query = getDao().getEntityManager().createQuery(sql.toString())
				.setParameter(DESCRICAO, naturezaAcao.getDescricao()).setParameter(CODIGO_EMPRESA, codigoEmpresa);

		List<NaturezaAcao> results = query.getResultList();

		if(results.isEmpty()) {
			return Boolean.FALSE;
		}
		return validaDuplicidade(naturezaAcao, results.get(0));
	}

	private boolean validaDuplicidade(NaturezaAcao naturezaAcao, NaturezaAcao result) {
		if(result != null && naturezaAcao.getId() == result.getId()) {
			return Boolean.FALSE;
		} else if(result != null && naturezaAcao.getId() != result.getId()){
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
}
