package br.com.juridico.impl;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.CausaPedido;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;

/**
 * 
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class CausaPedidoSessionBean extends AbstractCrudSessionBean<CausaPedido> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3246638270853559888L;
	private static final String DESCRICAO = "descricao";
	private static final String CODIGO_EMPRESA = "codigoEmpresa";

	public CausaPedidoSessionBean() {
		super();
	}

	/**
	 * 
	 * @param entityManager
	 */
	@Inject
	public CausaPedidoSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(CausaPedido.class, entityManager));
	}

	@Override
	public OrderClause[] getDefaultOrder() {
		return new OrderClause[] { new OrderClause(DESCRICAO, OrderType.ASC) };
	}

	public List<CausaPedido> filtrados(ConsultaLazyFilter filtro) {
		filtro.setAscendente(true);
		filtro.setPropriedadeOrdenacao(DESCRICAO);
		return super.filtrados(filtro, CausaPedido.class);
	}

	public int quantidadeFiltrados(ConsultaLazyFilter filtro) {
		return super.quantidadeFiltrados(filtro, CausaPedido.class);
	}

	/**
	 *
	 * @param descricao
	 * @param codigoEmpresa
	 * @return
	 */
	public CausaPedido findByDescricao(String descricao, Long codigoEmpresa) {
		Query query = getDao().getEntityManager().createNamedQuery("CausaPedido.findByDescricao")
				.setParameter("descricao", descricao).setParameter("codigoEmpresa", codigoEmpresa);
		try {
			return (CausaPedido) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public boolean isDescricaoJaCadastrada(CausaPedido causaPedido, Long codigoEmpresa){
		StringBuilder sql = new StringBuilder();
		sql.append("select t from CausaPedido t where t.descricao=:descricao and t.codigoEmpresa=:codigoEmpresa and t.dataExclusao is null");

		Query query = getDao().getEntityManager().createQuery(sql.toString())
				.setParameter(DESCRICAO, causaPedido.getDescricao()).setParameter(CODIGO_EMPRESA, codigoEmpresa);

		List<CausaPedido> results = query.getResultList();

		if(results.isEmpty()) {
			return Boolean.FALSE;
		}
		return validaDuplicidade(causaPedido, results.get(0));
	}

	private boolean validaDuplicidade(CausaPedido causaPedido, CausaPedido result) {
		if(result != null && causaPedido.getId() == result.getId()) {
			return Boolean.FALSE;
		} else if(result != null && causaPedido.getId() != result.getId()){
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

}
