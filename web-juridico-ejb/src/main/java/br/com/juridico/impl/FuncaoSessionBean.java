package br.com.juridico.impl;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.Funcao;
import br.com.juridico.filter.ConsultaLazyFilter;

/**
 * Created by jeffersonl2009_00 on 17/08/2016.
 */
@Stateless
@LocalBean
public class FuncaoSessionBean extends AbstractCrudSessionBean<Funcao> {

    private static final String DESCRICAO = "descricao";
    private static final String FUNCAO_FIND_BY_DESCRICAO = "Funcao.findByDescricao";
    private static final String CODIGO_EMPRESA = "codigoEmpresa";

    public FuncaoSessionBean() {
        super();
    }

    /**
     *
     * @param entityManager
     */
    @Inject
    public FuncaoSessionBean(@JuridicoEntityManager EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected void initDaoSet(EntityManager entityManager) {
        setDao(new GenericDao<>(Funcao.class, entityManager));
    }

    public List<Funcao> filtrados(ConsultaLazyFilter filtro) {
        return super.filtrados(filtro, Funcao.class);
    }

    public int quantidadeFiltrados(ConsultaLazyFilter filtro) {
        filtro.setAscendente(true);
        filtro.setPropriedadeOrdenacao(DESCRICAO);
        return super.quantidadeFiltrados(filtro, Funcao.class);
    }

    /**
     *
     * @param descricao
     * @param codigoEmpresa
     * @return
     */
    public Funcao findByDescricao(String descricao, Long codigoEmpresa) {
        Query query = getDao().getEntityManager().createNamedQuery(FUNCAO_FIND_BY_DESCRICAO)
                .setParameter(DESCRICAO, descricao).setParameter(CODIGO_EMPRESA, codigoEmpresa);
        try {
            return (Funcao) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public boolean isDescricaoJaCadastrada(Funcao funcao, Long codigoEmpresa){
        StringBuilder sql = new StringBuilder();
        sql.append("select t from Funcao t where t.descricao=:descricao and t.codigoEmpresa=:codigoEmpresa and t.dataExclusao is null");

        Query query = getDao().getEntityManager().createQuery(sql.toString())
                .setParameter(DESCRICAO, funcao.getDescricao()).setParameter(CODIGO_EMPRESA, codigoEmpresa);

        List<Funcao> results = query.getResultList();

        if(results.isEmpty()) {
            return Boolean.FALSE;
        }
        return validaDuplicidade(funcao, results.get(0));
    }

    private boolean validaDuplicidade(Funcao funcao, Funcao result) {
        if(result != null && funcao.getId() == result.getId()) {
            return Boolean.FALSE;
        } else if(result != null && funcao.getId() != result.getId()){
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }
}
