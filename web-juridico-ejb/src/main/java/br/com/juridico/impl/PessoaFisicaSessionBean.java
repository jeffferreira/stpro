package br.com.juridico.impl;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.PessoaFisica;
import br.com.juridico.factory.ContextPessoaFisicaSearchFactory;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;
import com.google.common.base.Strings;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.criteria.*;
import java.util.List;

/**
 *
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class PessoaFisicaSessionBean extends AbstractCrudSessionBean<PessoaFisica> {

	/**
	 *
	 */
	private static final long serialVersionUID = -3833477281795653214L;
	private static final String CODIGO_EMPRESA = "codigoEmpresa";
	private static final String PESSOA_DESCRICAO = "pessoa.descricao";
	private static final String PESSOA_LOGIN = "pessoa.login.login";

	public PessoaFisicaSessionBean() {
		super();
	}

	/**
	 *
	 * @param entityManager
	 */
	@Inject
	public PessoaFisicaSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(PessoaFisica.class, entityManager));
	}

	@Override
	public OrderClause[] getDefaultOrder() {
		return new OrderClause[] { new OrderClause(PESSOA_DESCRICAO, OrderType.ASC) };
	}


	@SuppressWarnings("unchecked")
	public List<PessoaFisica> filtrados(ConsultaLazyFilter filtro, boolean telaAdvogados, boolean telaColaborador) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<PessoaFisica> criteriaQuery = cb.createQuery(PessoaFisica.class);
		Root<PessoaFisica> root = criteriaQuery.from(PessoaFisica.class);
		Predicate condition = cb.equal(root.get("advogado"), telaAdvogados ? true : false);
		condition = cb.and(condition, cb.equal(root.get("colaborador"), telaColaborador ? true : false));
		condition = cb.and(condition, cb.isNull(root.get("dataExclusao")));
		condition = cb.and(condition, cb.equal(root.get("codigoEmpresa"), filtro.getCodigoEmpresa()));
		if(!Strings.isNullOrEmpty(filtro.getDescricao())) {
			condition = ContextPessoaFisicaSearchFactory.criterios(filtro, telaAdvogados, telaColaborador, cb, root);
		}
		criteriaQuery.where(condition);

		Expression expression = descricaoCampoOrdenacao(root, filtro.getPropriedadeOrdenacao());
		if (filtro.isAscendente() && expression != null) {
			criteriaQuery.orderBy(cb.asc(expression));
		} else if (expression != null) {
			criteriaQuery.orderBy(cb.desc(expression));
		}

		Query query = entityManager.createQuery(criteriaQuery);
		query.setFirstResult(filtro.getPrimeiroRegistro());
		query.setMaxResults(filtro.getQuantidadeRegistros());

		return query.getResultList();
	}

	private Expression descricaoCampoOrdenacao(Root<PessoaFisica> root, String propriedadeOrdenacao){
		Expression expression = null;

		if (PESSOA_DESCRICAO.equals(propriedadeOrdenacao)) {
			expression = root.get("pessoa").get("descricao");
		} else if(PESSOA_LOGIN.equals(propriedadeOrdenacao)){
			expression = root.get("pessoa").get("login").get("login");
		}
		return expression;
	}

	/**
	 *
	 * @param filtro
	 * @return
	 */
	public int quantidadeFiltrados(ConsultaLazyFilter filtro, boolean telaAdvogados, boolean telaColaborador) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<PessoaFisica> criteriaQuery = cb.createQuery(PessoaFisica.class);
		Root<PessoaFisica> root = criteriaQuery.from(PessoaFisica.class);
		Predicate condition = cb.equal(root.get("advogado"), telaAdvogados ? true : false);
		condition = cb.and(condition, cb.equal(root.get("colaborador"), telaColaborador ? true : false));
		condition = cb.and(condition, cb.isNull(root.get("dataExclusao")));
		condition = cb.and(condition, cb.equal(root.get("codigoEmpresa"), filtro.getCodigoEmpresa()));
		if(!Strings.isNullOrEmpty(filtro.getDescricao())) {
			condition = ContextPessoaFisicaSearchFactory.criterios(filtro, telaAdvogados, telaColaborador, cb, root);
		}
		criteriaQuery.where(condition);
		List<PessoaFisica> results = entityManager.createQuery(criteriaQuery).getResultList();

		return results.size();
	}

	/**
	 *
	 * @param advogadoInterno
	 * @param codigoEmpresa
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<PessoaFisica> getAdvogados(boolean advogadoInterno, Long codigoEmpresa) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT pf FROM PessoaFisica pf WHERE pf.codigoEmpresa=:codigoEmpresa \n");
		sql.append("and pf.advogado = true and pf.pessoa.dataExclusao is null \n");
		sql.append("and pf.advogadoInterno =:advogadoInterno ");
		sql.append("order by pf.pessoa.descricao asc");

		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter(CODIGO_EMPRESA, codigoEmpresa);
		query.setParameter("advogadoInterno", advogadoInterno ? true : false);

		return query.getResultList();
	}

	public List<PessoaFisica> getAdvogados(String filtro, boolean advogadoInterno, Long codigoEmpresa) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT pf FROM PessoaFisica pf WHERE pf.codigoEmpresa=:codigoEmpresa \n");
		sql.append("and pf.advogado = true and pf.pessoa.dataExclusao is null \n");
		sql.append("and pf.advogadoInterno =:advogadoInterno ");
		if(!Strings.isNullOrEmpty(filtro)) {
			sql.append("and pf.pessoa.descricao like :nome \n");
		}
		sql.append("order by pf.pessoa.descricao asc");

		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter(CODIGO_EMPRESA, codigoEmpresa);
		query.setParameter("advogadoInterno", advogadoInterno ? true : false);
		if(!Strings.isNullOrEmpty(filtro)) {
			query.setParameter("nome", "%"+filtro+"%");
		}

		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<PessoaFisica> findAllPessoasFisicasSemAdvogados(String filtro, Long codigoEmpresa) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT pf FROM PessoaFisica pf WHERE pf.codigoEmpresa=:codigoEmpresa \n");
		sql.append("and pf.advogado = false and pf.colaborador = false and pf.pessoa.dataExclusao is null and pf.pessoa.ativo = true \n");
		if(!Strings.isNullOrEmpty(filtro)) {
			sql.append("and pf.pessoa.descricao like :nome \n");
		}
		sql.append("order by pf.pessoa.descricao asc");

		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter(CODIGO_EMPRESA, codigoEmpresa);
		if(!Strings.isNullOrEmpty(filtro)) {
			query.setParameter("nome", "%"+filtro+"%");
		}

		return query.getResultList();
	}

	/**
	 *
	 * @param filtro
	 * @param codigoEmpresa
	 * @return
	 */
	public List<PessoaFisica> findAllPessoasFisicasComAdvogados(String filtro, Long codigoEmpresa) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT pf FROM PessoaFisica pf WHERE pf.codigoEmpresa=:codigoEmpresa \n");
		sql.append("and pf.pessoa.dataExclusao is null and pf.pessoa.ativo = true \n");
		if(!Strings.isNullOrEmpty(filtro)) {
			sql.append("and pf.pessoa.descricao like :nome \n");
		}
		sql.append("order by pf.pessoa.descricao asc");

		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter(CODIGO_EMPRESA, codigoEmpresa);
		if(!Strings.isNullOrEmpty(filtro)) {
			query.setParameter("nome", "%"+filtro+"%");
		}

		return query.getResultList();
	}

	/**
	 *
	 * @param codigoEmpresa
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<PessoaFisica> getColaboradores(Long codigoEmpresa) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT pf FROM PessoaFisica pf WHERE pf.codigoEmpresa=:codigoEmpresa \n");
		sql.append("and pf.colaborador = true and pf.pessoa.dataExclusao is null \n");
		sql.append("order by pf.pessoa.descricao asc");

		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter(CODIGO_EMPRESA, codigoEmpresa);

		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<PessoaFisica> findByAdvogadosAndColaboradores(Long codigoEmpresa){
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT pf FROM PessoaFisica pf WHERE pf.codigoEmpresa=:codigoEmpresa \n");
		sql.append("and (pf.advogado = true or pf.colaborador = true) and pf.pessoa.dataExclusao is null \n");
		sql.append("and pf.advogadoInterno = true");

		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter(CODIGO_EMPRESA, codigoEmpresa);
		return query.getResultList();
	}

	public List<PessoaFisica> findByAdvogadosAndColaboradores(String filtro, Long codigoEmpresa){
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT pf FROM PessoaFisica pf WHERE pf.codigoEmpresa=:codigoEmpresa \n");
		sql.append("and ((pf.advogado = true and pf.advogadoInterno = true) or pf.colaborador = true) and pf.pessoa.dataExclusao is null \n");
		if(!Strings.isNullOrEmpty(filtro)) {
			sql.append("and pf.pessoa.descricao like :nome \n");
		}
		sql.append("order by pf.pessoa.descricao asc");

		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter(CODIGO_EMPRESA, codigoEmpresa);
		if(!Strings.isNullOrEmpty(filtro)) {
			query.setParameter("nome", "%"+filtro+"%");
		}
		return query.getResultList();
	}

	/**
	 *
	 * @param condition
	 * @param codEmpresa
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<PessoaFisica> findByDescricao(String condition, Long codEmpresa) {
		StringBuilder sql = new StringBuilder();
		sql.append("select p from PessoaFisica p where p.pessoa.descricao like :condition ");
		sql.append("and p.codigoEmpresa=:codigoEmpresa and p.pessoa.dataExclusao is null ");

		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter("condition", "%" + condition + "%");
		query.setParameter(CODIGO_EMPRESA, codEmpresa);

		return query.getResultList();
	}

	public List<PessoaFisica> findByNome(String nome, Long codEmpresa) {
		StringBuilder sql = new StringBuilder();
		sql.append("select p from PessoaFisica p where p.pessoa.descricao =:nome ");
		sql.append("and p.codigoEmpresa=:codigoEmpresa and p.pessoa.dataExclusao is null ");

		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter("nome", nome);
		query.setParameter(CODIGO_EMPRESA, codEmpresa);

		return query.getResultList();
	}

	public List<PessoaFisica> findByCpf(Long idPessoa, String cpf, Long codEmpresa) {
		StringBuilder sql = new StringBuilder();
		sql.append("select p from PessoaFisica p where p.cpf =:cpf ");
		sql.append("and p.id !=:idPessoa ");
		sql.append("and p.codigoEmpresa=:codigoEmpresa and p.pessoa.dataExclusao is null ");

		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter("cpf", cpf);
		query.setParameter("idPessoa", idPessoa != null ? idPessoa : 0L);
		query.setParameter(CODIGO_EMPRESA, codEmpresa);

		return query.getResultList();
	}

	public PessoaFisica findByPessoa(Long idPessoa, Long codEmpresa){
		StringBuilder sql = new StringBuilder();
		sql.append("select p from PessoaFisica p where p.pessoa.id =:idPessoa ");
		sql.append("and p.codigoEmpresa=:codigoEmpresa ");

		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter("idPessoa", idPessoa);
		query.setParameter(CODIGO_EMPRESA, codEmpresa);

		try {
			return (PessoaFisica) query.getSingleResult();
		} catch (NoResultException nre){
			return null;
		}
	}

}
