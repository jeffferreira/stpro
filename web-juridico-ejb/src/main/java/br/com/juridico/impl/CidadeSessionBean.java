package br.com.juridico.impl;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.Cidade;
import br.com.juridico.entidades.Uf;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;

/**
 * 
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class CidadeSessionBean extends AbstractSessionBean<Cidade> {

	private static final String DESCRICAO = "descricao";
	/**
	 *
	 */
	private static final long serialVersionUID = -8806120845282279395L;

	public CidadeSessionBean() {
		// CONSTRUTOR
	}

	/**
	 * 
	 * @param entityManager
	 */
	@Inject
	public CidadeSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<Cidade>(Cidade.class, entityManager));
	}

	@Override
	public OrderClause[] getDefaultOrder() {
		return new OrderClause[] { new OrderClause(DESCRICAO, OrderType.ASC) };
	}

	/**
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Cidade> retrieveAll() {
		String sql = "select c from Cidade c ";
		Query query = getDao().getEntityManager().createQuery(sql);

		return query.getResultList();
	}

	/**
	 * 
	 * @param descricao
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Cidade> retrieve(String descricao) {
		String sql = "select c from Cidade c where c.descricao like :descricao";
		Query query = getDao().getEntityManager().createQuery(sql);
		query.setParameter(DESCRICAO, "%" + descricao + "%");

		return query.getResultList();
	}

	/**
	 * 
	 * @param uf
	 * @param descricao
	 * @return
	 */
	public Cidade findByDescricao(Uf uf, String descricao) {
		Query query = getDao().getEntityManager().createNamedQuery("Cidade.findByUfDescricao").setParameter("uf", uf)
				.setParameter(DESCRICAO, descricao);
		try {
			return (Cidade) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	/**
	 * 
	 * @param uf
	 * @param descricao
	 * @return
	 */
	public Cidade findByUfDescricao(Uf uf, String descricao) {
		Query query = getDao().getEntityManager().createNamedQuery("Cidade.findByUfDescricao").setParameter("uf", uf)
				.setParameter(DESCRICAO, descricao);
		try {
			return (Cidade) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
}
