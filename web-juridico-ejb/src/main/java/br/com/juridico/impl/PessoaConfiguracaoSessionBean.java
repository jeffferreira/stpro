package br.com.juridico.impl;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.DireitoPerfil;
import br.com.juridico.entidades.PessoaConfiguracao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

/**
 * 
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class PessoaConfiguracaoSessionBean extends AbstractCrudSessionBean<PessoaConfiguracao> {

	/**
	 *
	 */
	private static final long serialVersionUID = 6812000707435610578L;

	public PessoaConfiguracaoSessionBean() {
		super();
	}

	/**
	 *
	 * @param entityManager
	 */
	@Inject
	public PessoaConfiguracaoSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(PessoaConfiguracao.class, entityManager));
	}

	public List<PessoaConfiguracao> findByPessoa(Long idPessoa){
		String sql = "select t from PessoaConfiguracao t where t.pessoa.id=:idPessoa";
		Query query = getDao().getEntityManager().createQuery(sql);
		query.setParameter("idPessoa", idPessoa);
		return query.getResultList();
	}
	
	public List<PessoaConfiguracao> findByDireitoPerfil(Long direitoPerfilId, Long codigoEmpresa) {
		String sql = "select t from PessoaConfiguracao t where t.direitoPerfil.id=:direitoPerfilId and t.codigoEmpresa=:codigoEmpresa";
		Query query = getDao().getEntityManager().createQuery(sql);
		query.setParameter("direitoPerfilId", direitoPerfilId);
		query.setParameter("codigoEmpresa", codigoEmpresa);

		return query.getResultList();
	}

}
