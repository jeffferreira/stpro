package br.com.juridico.impl;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.EstadoCivil;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

/**
 * 
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class EstadoCivilSessionBean extends AbstractSessionBean<EstadoCivil> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6729541341456668244L;

	public EstadoCivilSessionBean() {
		// CONSTRUTOR
	}

	/**
	 * 
	 * @param entityManager
	 */
	@Inject
	public EstadoCivilSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<EstadoCivil>(EstadoCivil.class, entityManager));
	}

	@Override
	public OrderClause[] getDefaultOrder() {
		return new OrderClause[] { new OrderClause("descricao", OrderType.ASC) };
	}

    /**
     *
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<EstadoCivil> retrieveAll() {
        String sql = "select e from EstadoCivil e";
        Query query = getDao().getEntityManager().createQuery(sql);

        return query.getResultList();
    }
}
