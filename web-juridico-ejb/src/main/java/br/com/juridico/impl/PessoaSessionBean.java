package br.com.juridico.impl;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.dto.LoginEmailDto;
import br.com.juridico.dto.RelatorioClienteDto;
import br.com.juridico.entidades.Pessoa;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;
import com.google.common.collect.Lists;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;
import java.util.StringJoiner;

/**
 * 
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class PessoaSessionBean extends AbstractSessionBean<Pessoa> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8284502709282755391L;
	
	private static final Long ADMIN = 1L;
	private static final String CODIGO_EMPRESA = "codigoEmpresa";

	public PessoaSessionBean() {
		super();
	}

	/**
	 * 
	 * @param entityManager
	 */
	@Inject
	public PessoaSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(Pessoa.class, entityManager));
	}

	@Override
	public OrderClause[] getDefaultOrder() {
		return new OrderClause[] { new OrderClause("descricao", OrderType.ASC) };
	}

	/**
	 * 
	 * @param login
	 * @return
	 */
	public List<Pessoa> findUserList(String login) {
		StringBuilder sql = new StringBuilder();
		sql.append("select p from Pessoa p ");
		sql.append("inner join fetch p.login lg ");
		sql.append("where lg.login=:login and p.dataExclusao is null and p.ativo = true ");

		Query query = getDao().getEntityManager().createQuery(sql.toString()).setParameter("login", login);
		List<Pessoa> results = query.getResultList();
		return results;

	}

	public Pessoa findUser(String login, Long codigoEmpresa) {
		StringBuilder sql = new StringBuilder();
		sql.append("select p from Pessoa p ");
		sql.append("inner join fetch p.login lg ");
		sql.append("where lg.login=:login and p.codigoEmpresa=:codigoEmpresa");

		Query query = getDao().getEntityManager().createQuery(sql.toString()).setParameter("login", login);
		query.setParameter(CODIGO_EMPRESA, codigoEmpresa);
		List<Pessoa> results = query.getResultList();

		if (!results.isEmpty()) {
			return results.get(0);
		}
		return null;

	}

	/**
	 *
	 * @param cpf
	 * @return
	 */
	public List<Pessoa> findByCpf(String cpf) {
		StringBuilder sql = new StringBuilder();
		sql.append("select p from Pessoa p ");
		sql.append("inner join fetch p.login lg ");
		sql.append("where lg.login=:cpf and p.ativo = true ");

		Query query = getDao().getEntityManager().createQuery(sql.toString()).setParameter("cpf", cpf);
		return query.getResultList();
	}

	/**
	 *
	 * @param nome
	 * @param codigoEmpresa
     * @return
     */
	@SuppressWarnings("unchecked")
	public Pessoa findByNome(String nome, Long codigoEmpresa) {
		StringBuilder sql = new StringBuilder();
		sql.append("select p from Pessoa p ");
		sql.append("where p.descricao=:descricao and p.codigoEmpresa=:codigoEmpresa ");

		Query query = getDao().getEntityManager().createQuery(sql.toString()).setParameter("descricao", nome).setParameter("codigoEmpresa", codigoEmpresa);
		List<Pessoa> results = query.getResultList();

		if (!results.isEmpty()) {
			return results.get(0);
		}
		return null;

	}

	public List<Pessoa> findBySearchNome(String nome, Long codigoEmpresa) {
		StringBuilder sql = new StringBuilder();
		sql.append("select p from Pessoa p ");
		sql.append("where p.descricao like :descricao and p.codigoEmpresa=:codigoEmpresa ");

		Query query = getDao().getEntityManager().createQuery(sql.toString())
				.setParameter("descricao", "%"+nome+"%")
				.setParameter("codigoEmpresa", codigoEmpresa);

		return query.getResultList();

	}

	/**
	 * 
	 * @param tipoPessoa
	 * @param idPosicao
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<RelatorioClienteDto> findRelatorioClientes(String tipoPessoa, Long idPosicao) {
		StringBuilder sql = new StringBuilder();

		sql.append("select p.ds_pessoa, pf.ds_cpf, pj.ds_cnpj, ct.nr_ddd, ct.nr_numero, \n");
		sql.append("pf.fl_sexo, pf.dt_nascimento, pe.ds_email \n");
		sql.append("from tb_pessoa p \n");
		sql.append("  left join tb_pessoa_fisica pf on pf.fk_pessoa = p.id_pessoa \n");
		sql.append("  left join tb_pessoa_juridica pj on pj.fk_pessoa = p.id_pessoa \n");
		sql.append("  left join tb_processo_pessoa pp on pp.fk_pessoa = p.id_pessoa \n");
		sql.append("  left join tb_posicao ps on ps.id_posicao = pp.fk_posicao \n");
		sql.append("  left join tb_pessoa_email pe on pe.fk_pessoa = p.id_pessoa \n");
		sql.append("  inner join tb_pessoa_contato pc on pc.fk_pessoa = p.id_pessoa \n");
		sql.append("  inner join tb_contato ct on ct.id_contato = pc.fk_contato \n");
		sql.append(" where p.dataExclusao is null \n");

		if (idPosicao != null) {
			sql.append(" and ps.id_posicao =:idPosicao \n");
		}
		if (tipoPessoa != null && !tipoPessoa.isEmpty()) {
			sql.append(" and p.fl_tipo_pessoa =:tipoPessoa \n");
		}

		sql.append("group by p.ds_pessoa, pf.ds_cpf, pj.ds_cnpj, ct.nr_ddd, ct.nr_numero, \n");
		sql.append(" pf.fl_sexo, pf.dt_nascimento, pe.ds_email \n");
		sql.append("order by p.ds_pessoa asc");

		Query query = getDao().getEntityManager().createNativeQuery(sql.toString());
		if (idPosicao != null) {
			query.setParameter("idPosicao", idPosicao);
		}
		if (tipoPessoa != null && !tipoPessoa.isEmpty()) {
			query.setParameter("tipoPessoa", tipoPessoa);
		}

		List<Object> list = query.getResultList();
		List<RelatorioClienteDto> result = Lists.newArrayList();

		for (int i = 0; i < list.size(); i++) {
			result.add(new RelatorioClienteDto((Object[]) list.get(i)));
		}

		return result;
	}

	/**
	 * 
	 */
	@Override
	public List<Pessoa> retrieveAll(Long codigoEmpresa) {
		String sql = "select t from Pessoa t where t.codigoEmpresa=:codigoEmpresa and t.dataExclusao is null";
		Query query = getDao().getEntityManager().createQuery(sql);
		query.setParameter(CODIGO_EMPRESA, codigoEmpresa);
		return query.getResultList();
	}

	/**
	 *
	 * @param email
	 * @param cpf
	 * @param codigoEmpresa
	 * @return
	 */
    public LoginEmailDto findByEmailAtivo(String email, String cpf, Long codigoEmpresa){
        StringBuilder sql = new StringBuilder();
        sql.append("select l.ds_login, l.bb_senha, p.ds_pessoa from tb_pessoa_email pe ");
        sql.append("inner join tb_pessoa p on p.id_pessoa = pe.fk_pessoa ");
        sql.append("inner join tb_login l on l.id_login = p.fk_login ");
        sql.append(" where pe.ds_email =:email and l.ds_login=:cpf and p.dt_exclusao_logica is null and p.nr_empresa =:codigoEmpresa");

		Query query = getDao().getEntityManager().createNativeQuery(sql.toString());
		query.setParameter("email", email)
				.setParameter("codigoEmpresa", codigoEmpresa)
				.setParameter("cpf", cpf);

        try {
            Object[] result = (Object[]) query.getSingleResult();
            return new LoginEmailDto((String)result[0], (byte[]) result[1], (String) result[2]);
        } catch (NoResultException nre) {
            return null;
        }
    }
    
    @SuppressWarnings("unchecked")
	public List<Pessoa> findByAdministradores(Long codigoEmpresa){
    	StringBuilder sql = new StringBuilder();
		sql.append("select p from Pessoa p ");
		sql.append("inner join fetch p.perfil pf ");
		sql.append("where pf.id=:admin and p.codigoEmpresa=:codigoEmpresa");
		
		Query query = getDao().getEntityManager().createQuery(sql.toString())
				.setParameter("admin", ADMIN).setParameter("codigoEmpresa", codigoEmpresa);
		return query.getResultList();
    }
}
