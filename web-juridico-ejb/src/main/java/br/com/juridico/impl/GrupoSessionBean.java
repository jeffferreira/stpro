package br.com.juridico.impl;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.Grupo;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;

/**
 * 
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class GrupoSessionBean extends AbstractCrudSessionBean<Grupo> {

	public static final String GRUPO_FIND_BY_DESCRICAO = "Grupo.findByDescricao";
	/**
	 * 
	 */
	private static final long serialVersionUID = -3468714813991272493L;
	public static final String DESCRICAO = "descricao";
	public static final String CODIGO_EMPRESA = "codigoEmpresa";

	public GrupoSessionBean() {
		super();
	}

	/**
	 * 
	 * @param entityManager
	 */
	@Inject
	public GrupoSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(Grupo.class, entityManager));
	}

	@Override
	public OrderClause[] getDefaultOrder() {
		return new OrderClause[] { new OrderClause(DESCRICAO, OrderType.ASC) };
	}

	public List<Grupo> filtrados(ConsultaLazyFilter filtro) {
		return super.filtrados(filtro, Grupo.class);
	}

	public int quantidadeFiltrados(ConsultaLazyFilter filtro) {
		return super.quantidadeFiltrados(filtro, Grupo.class);
	}

	/**
	 *
	 * 
	 * @param descricao
	 * @param codigoEmpresa
	 * @return
	 */
	public Grupo findByDescricao(String descricao, Long codigoEmpresa) {
		Query query = getDao().getEntityManager().createNamedQuery(GRUPO_FIND_BY_DESCRICAO)
				.setParameter(DESCRICAO, descricao).setParameter(CODIGO_EMPRESA, codigoEmpresa);
		try {
			return (Grupo) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public boolean isDescricaoJaCadastrada(Grupo grupo, Long codigoEmpresa){
		StringBuilder sql = new StringBuilder();
		sql.append("select t from Grupo t where t.descricao=:descricao and t.codigoEmpresa=:codigoEmpresa and t.dataExclusao is null");

		Query query = getDao().getEntityManager().createQuery(sql.toString())
				.setParameter(DESCRICAO, grupo.getDescricao()).setParameter(CODIGO_EMPRESA, codigoEmpresa);

		List<Grupo> results = query.getResultList();

		if(results.isEmpty()) {
			return Boolean.FALSE;
		}
		return validaDuplicidade(grupo, results.get(0));
	}

	private boolean validaDuplicidade(Grupo grupo, Grupo result) {
		if(result != null && grupo.getId() == result.getId()) {
			return Boolean.FALSE;
		} else if(result != null && grupo.getId() != result.getId()){
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
}
