package br.com.juridico.impl;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.ProcessoPessoaJuridicaCentroCusto;

/**
 * Created by Jefferson on 20/03/2016.
 */
@Stateless
@LocalBean
public class ProcessoPJCentroCustoSessionBean extends AbstractSessionBean<ProcessoPessoaJuridicaCentroCusto> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4862512538494360477L;

	public ProcessoPJCentroCustoSessionBean() {
		// CONSTRUTOR
	}

	/**
	 * 
	 * @param entityManager
	 */
	@Inject
	public ProcessoPJCentroCustoSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<ProcessoPessoaJuridicaCentroCusto>(ProcessoPessoaJuridicaCentroCusto.class,
				entityManager));
	}
}
