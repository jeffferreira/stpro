package br.com.juridico.impl;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.DireitoPerfil;
import br.com.juridico.entidades.PerfilAcesso;

import java.util.List;

/**
 * 
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class DireitoPerfilSessionBean extends AbstractSessionBean<DireitoPerfil> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3833477281795653214L;

	public DireitoPerfilSessionBean() {
		super();
	}

	/**
	 * 
	 * @param entityManager
	 */
	@Inject
	public DireitoPerfilSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(DireitoPerfil.class, entityManager));
	}

	public DireitoPerfil findByPerfilAndDireito(Long perfilId, Long direitoId, Long codigoEmpresa) {
		String sql = "select t from DireitoPerfil t where t.perfilAcesso.id=:perfilId and t.direito.id=:direitoId and t.codigoEmpresa=:codigoEmpresa";
		Query query = getDao().getEntityManager().createQuery(sql);
		query.setParameter("perfilId", perfilId);
		query.setParameter("direitoId", direitoId);
		query.setParameter("codigoEmpresa", codigoEmpresa);

		List<DireitoPerfil> results = query.getResultList();

		if (!results.isEmpty()) {
			return results.get(0);
		}

		return null;
	}

	public List<DireitoPerfil> findByPerfilAcesso(Long perfilAcesso, Long codigoEmpresa){
		StringBuilder sql = new StringBuilder();
		sql.append("select t from DireitoPerfil t where t.perfilAcesso.id=:perfilAcesso and t.codigoEmpresa=:codigoEmpresa");
		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter("perfilAcesso", perfilAcesso);
		query.setParameter("codigoEmpresa", codigoEmpresa);
		return query.getResultList();
	}

	public void deleteFromId(Long direitoPerfilId){
		String sql = "delete from DireitoPerfil where id=:direitoPerfilId";
		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter("direitoPerfilId", direitoPerfilId);
		query.executeUpdate();
	}

}
