package br.com.juridico.impl;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.Perfil;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;

/**
 * 
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class PerfilSessionBean extends AbstractCrudSessionBean<Perfil> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3833477281795653214L;
	private static final String DESCRICAO = "descricao";
	private static final String PERFIL_FIND_BY_SIGLA = "Perfil.findBySigla";
	private static final String SIGLA = "sigla";
	private static final String CODIGO_EMPRESA = "codigoEmpresa";

	public PerfilSessionBean() {
		super();
	}

	/**
	 * 
	 * @param entityManager
	 */
	@Inject
	public PerfilSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(Perfil.class, entityManager));
	}

	@Override
	public OrderClause[] getDefaultOrder() {
		return new OrderClause[] { new OrderClause(DESCRICAO, OrderType.ASC) };
	}

	public List<Perfil> filtrados(ConsultaLazyFilter filtro) {
		filtro.setAscendente(true);
		filtro.setPropriedadeOrdenacao(DESCRICAO);
		return super.filtrados(filtro, Perfil.class);
	}

	public int quantidadeFiltrados(ConsultaLazyFilter filtro) {
		return super.quantidadeFiltrados(filtro, Perfil.class);
	}

	/**
	 * 
	 * @param sigla
	 * @param codigoEmpresa
	 * @return
	 */
	public Perfil findBySigla(String sigla, Long codigoEmpresa) {
		Query query = getDao().getEntityManager().createNamedQuery(PERFIL_FIND_BY_SIGLA)
				.setParameter(SIGLA, sigla).setParameter(CODIGO_EMPRESA, codigoEmpresa);
		try {
			return (Perfil) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public List<Perfil> findAllByUsuario(Long codigoEmpresa, boolean admin){
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT t FROM Perfil t WHERE t.codigoEmpresa=:codigoEmpresa ");
		if(!admin){
			sql.append("and t.sigla <> 'ADMIN'");
		}
		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter("codigoEmpresa", codigoEmpresa);
		return query.getResultList();
	}

	public boolean isDescricaoJaCadastrada(Perfil perfil, Long codigoEmpresa){
		StringBuilder sql = new StringBuilder();
		sql.append("select t from Perfil t where t.descricao=:descricao and t.codigoEmpresa=:codigoEmpresa and t.dataExclusao is null");

		Query query = getDao().getEntityManager().createQuery(sql.toString())
				.setParameter(DESCRICAO, perfil.getDescricao()).setParameter(CODIGO_EMPRESA, codigoEmpresa);

		List<Perfil> results = query.getResultList();

		if(results.isEmpty()) {
			return Boolean.FALSE;
		}
		return validaDuplicidade(perfil, results.get(0));

	}

	public boolean isSiglaJaCadastrada(Perfil perfil, Long codigoEmpresa){
		StringBuilder sql = new StringBuilder();
		sql.append("select t from Perfil t where t.sigla=:sigla and t.codigoEmpresa=:codigoEmpresa and t.dataExclusao is null");

		Query query = getDao().getEntityManager().createQuery(sql.toString())
				.setParameter(SIGLA, perfil.getSigla()).setParameter(CODIGO_EMPRESA, codigoEmpresa);

		List<Perfil> results = query.getResultList();

		if(results.isEmpty()) {
			return Boolean.FALSE;
		}
		return validaDuplicidade(perfil, results.get(0));
	}

	private boolean validaDuplicidade(Perfil perfil, Perfil result) {
		if(result != null && perfil.getId() == result.getId()) {
			return Boolean.FALSE;
		} else if(result != null && perfil.getId() != result.getId()){
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
}
