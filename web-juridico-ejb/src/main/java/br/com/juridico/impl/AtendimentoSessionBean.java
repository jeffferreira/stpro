package br.com.juridico.impl;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.AndamentoAtendimento;
import br.com.juridico.entidades.Atendimento;
import br.com.juridico.entidades.Pessoa;
import br.com.juridico.entidades.User;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;

/**
 * 
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class AtendimentoSessionBean extends AbstractSessionBean<Atendimento> {

    /**
     * 
     */
    private static final long serialVersionUID = 5214967225075676085L;

    @Inject
    private AndamentoAtendimentoSessionBean andamentoAtendimentoSessionBean;

    public AtendimentoSessionBean() {
    	// CONSTRUTOR
    }

    /**
     * 
     * @param entityManager
     */
    @Inject
    public AtendimentoSessionBean(@JuridicoEntityManager EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected void initDaoSet(EntityManager entityManager) {
        setDao(new GenericDao<>(Atendimento.class, entityManager));
    }

    @Override
    public OrderClause[] getDefaultOrder() {
        return new OrderClause[] { new OrderClause("id", OrderType.ASC) };
    }

    public List<Atendimento> findByMeusAtendimentos(User user){
        List<AndamentoAtendimento> andamentos = andamentoAtendimentoSessionBean.findByMeusAndamentos(user);
        return null;
    }

}
