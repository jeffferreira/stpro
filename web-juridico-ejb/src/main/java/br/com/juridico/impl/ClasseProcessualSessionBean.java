package br.com.juridico.impl;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.ClasseProcessual;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;

/**
 * 
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class ClasseProcessualSessionBean extends AbstractCrudSessionBean<ClasseProcessual> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6959045693123695975L;
	private static final String DESCRICAO = "descricao";
	private static final String CODIGO_EMPRESA = "codigoEmpresa";

	public ClasseProcessualSessionBean() {
		super();
	}

	/**
	 * 
	 * @param entityManager
	 */
	@Inject
	public ClasseProcessualSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(ClasseProcessual.class, entityManager));
	}

	@Override
	public OrderClause[] getDefaultOrder() {
		return new OrderClause[] { new OrderClause(DESCRICAO, OrderType.ASC) };
	}

	public int quantidadeFiltrados(ConsultaLazyFilter filtro) {
		filtro.setAscendente(true);
		filtro.setPropriedadeOrdenacao(DESCRICAO);
		return super.quantidadeFiltrados(filtro, ClasseProcessual.class);
	}

	public List<ClasseProcessual> filtrados(ConsultaLazyFilter filtro) {
		return super.filtrados(filtro, ClasseProcessual.class);
	}

	/**
	 * 
	 * @param descricao
	 * @param codigoEmpresa
	 * @return
	 */
	public ClasseProcessual findByDescricao(String descricao, Long codigoEmpresa) {
		Query query = getDao().getEntityManager().createNamedQuery("ClasseProcessual.findByDescricao")
				.setParameter("descricao", descricao).setParameter("codigoEmpresa", codigoEmpresa);
		try {
			return (ClasseProcessual) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public boolean isDescricaoJaCadastrada(ClasseProcessual classeProcessual, Long codigoEmpresa){
		StringBuilder sql = new StringBuilder();
		sql.append("select t from ClasseProcessual t where t.descricao=:descricao and t.codigoEmpresa=:codigoEmpresa and t.dataExclusao is null");

		Query query = getDao().getEntityManager().createQuery(sql.toString())
				.setParameter(DESCRICAO, classeProcessual.getDescricao()).setParameter(CODIGO_EMPRESA, codigoEmpresa);

		List<ClasseProcessual> results = query.getResultList();

		if(results.isEmpty()) {
			return Boolean.FALSE;
		}
		return validaDuplicidade(classeProcessual, results.get(0));
	}

	private boolean validaDuplicidade(ClasseProcessual classeProcessual, ClasseProcessual result) {
		if(result != null && classeProcessual.getId() == result.getId()) {
			return Boolean.FALSE;
		} else if(result != null && classeProcessual.getId() != result.getId()){
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
}
