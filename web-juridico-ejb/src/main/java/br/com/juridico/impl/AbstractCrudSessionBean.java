package br.com.juridico.impl;

import br.com.juridico.dao.IEntity;
import br.com.juridico.ejb.AbstractBean;
import br.com.juridico.filter.ConsultaLazyFilter;
import com.google.common.base.Strings;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.List;

/**
 * Created by jeffersonl2009_00 on 13/07/2016.
 */
public abstract class AbstractCrudSessionBean<T extends IEntity> extends AbstractBean<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    protected static final Logger LOGGER = Logger.getLogger(IEntity.class);

    protected EntityManager entityManager;

    /**
     *
     */
    public AbstractCrudSessionBean() {
        super();
    }

    /**
     *
     * @param entityManager
     */
    public AbstractCrudSessionBean(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    protected abstract void initDaoSet(EntityManager entityManager);

    @PostConstruct
    protected void setup() {
        initDaoSet(entityManager);
    }

    protected List<T> filtrados(ConsultaLazyFilter filtro, Class clazz) {
        Criteria criteria = criarCriteriaParaFiltro(filtro, clazz);

        criteria.setFirstResult(filtro.getPrimeiroRegistro());
        criteria.setMaxResults(filtro.getQuantidadeRegistros());

        if (filtro.isAscendente() && filtro.getPropriedadeOrdenacao() != null) {
            criteria.addOrder(Order.asc(filtro.getPropriedadeOrdenacao()));
        } else if (filtro.getPropriedadeOrdenacao() != null) {
            criteria.addOrder(Order.desc(filtro.getPropriedadeOrdenacao()));
        }

        return criteria.list();
    }

    /**
     *
     * @param filtro
     * @return
     */
    protected int quantidadeFiltrados(ConsultaLazyFilter filtro, Class clazz) {
        Criteria criteria = criarCriteriaParaFiltro(filtro, clazz);
        criteria.setProjection(Projections.rowCount());
        return ((Number) criteria.uniqueResult()).intValue();
    }

    private Criteria criarCriteriaParaFiltro(ConsultaLazyFilter filtro, Class clazz) {
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(clazz);

        if (!Strings.isNullOrEmpty(filtro.getDescricao())) {
            criteria.add(Restrictions.ilike("descricao", filtro.getDescricao(), MatchMode.ANYWHERE));
        }

        if (filtro.getCodigoEmpresa() != null) {
            criteria.add(Restrictions.eq("codigoEmpresa", filtro.getCodigoEmpresa()));
        }

        if(filtro.isPossuiExclusaoLogica()) {
            criteria.add(Restrictions.isNull("dataExclusao"));
        }
        return criteria;
    }
}
