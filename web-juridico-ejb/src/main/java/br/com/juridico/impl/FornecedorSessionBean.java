package br.com.juridico.impl;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.Fornecedor;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;
import com.google.common.base.Strings;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

/**
 *
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class FornecedorSessionBean extends AbstractCrudSessionBean<Fornecedor> {

	/**
	 *
	 */
	private static final long serialVersionUID = 6812000707435610578L;

	public FornecedorSessionBean() {
		super();
	}

	/**
	 *
	 * @param entityManager
	 */
	@Inject
	public FornecedorSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(Fornecedor.class, entityManager));
	}

	@Override
	public OrderClause[] getDefaultOrder() {
		return new OrderClause[] { new OrderClause("nome", OrderType.ASC) };
	}

	public List<Fornecedor> filtrados(ConsultaLazyFilter filtro) {
		return search(filtro);
	}

	public int quantidadeFiltrados(ConsultaLazyFilter filtro) {
		return countSearch(filtro);
	}

	public boolean isNomeJaCadastrada(Fornecedor fornecedor, Long codigoEmpresa){
		StringBuilder sql = new StringBuilder();
		sql.append("select t from Fornecedor t where t.nome=:nome and t.codigoEmpresa=:codigoEmpresa and t.dataExclusao is null");

		Query query = getDao().getEntityManager().createQuery(sql.toString())
				.setParameter("nome", fornecedor.getNome()).setParameter("codigoEmpresa", codigoEmpresa);

		List<Fornecedor> results = query.getResultList();

		if(results.isEmpty()) {
			return Boolean.FALSE;
		}
		return validaDuplicidade(fornecedor, results.get(0));
	}

	private boolean validaDuplicidade(Fornecedor fornecedor, Fornecedor result) {
		if(result != null && fornecedor.getId() == result.getId()) {
			return Boolean.FALSE;
		} else if(result != null && fornecedor.getId() != result.getId()){
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	private List<Fornecedor> search(ConsultaLazyFilter filtro) {
		Criteria criteria = criarCriteriaParaFiltro(filtro);

		criteria.setFirstResult(filtro.getPrimeiroRegistro());
		criteria.setMaxResults(filtro.getQuantidadeRegistros());

		if (filtro.isAscendente() && filtro.getPropriedadeOrdenacao() != null) {
			criteria.addOrder(Order.asc(filtro.getPropriedadeOrdenacao()));
		} else if (filtro.getPropriedadeOrdenacao() != null) {
			criteria.addOrder(Order.desc(filtro.getPropriedadeOrdenacao()));
		}

		return criteria.list();
	}

	/**
	 *
	 * @param filtro
	 * @return
	 */
	private int countSearch(ConsultaLazyFilter filtro) {
		Criteria criteria = criarCriteriaParaFiltro(filtro);
		criteria.setProjection(Projections.rowCount());
		return ((Number) criteria.uniqueResult()).intValue();
	}

	private Criteria criarCriteriaParaFiltro(ConsultaLazyFilter filtro) {
		Session session = entityManager.unwrap(Session.class);
		Criteria criteria = session.createCriteria(Fornecedor.class);

		if (!Strings.isNullOrEmpty(filtro.getDescricao())) {
			criteria.add(Restrictions.ilike("nome", filtro.getDescricao(), MatchMode.ANYWHERE));
		}

		if (filtro.getCodigoEmpresa() != null) {
			criteria.add(Restrictions.eq("codigoEmpresa", filtro.getCodigoEmpresa()));
		}

		if(filtro.isPossuiExclusaoLogica()) {
			criteria.add(Restrictions.isNull("dataExclusao"));
		}
		return criteria;
	}
}
