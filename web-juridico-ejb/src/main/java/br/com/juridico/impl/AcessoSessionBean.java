package br.com.juridico.impl;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.Acesso;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;

@Stateless
@LocalBean
public class AcessoSessionBean extends AbstractSessionBean<Acesso> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3833477281795653214L;
	private static final String DESCRICAO = "descricao";

	public AcessoSessionBean() {
		super();
	}

	@Inject
	public AcessoSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(Acesso.class, entityManager));
	}

	@Override
	public OrderClause[] getDefaultOrder() {
		return new OrderClause[] { new OrderClause(DESCRICAO, OrderType.ASC) };
	}

	@SuppressWarnings("unchecked")
	public List<Acesso> findAcessosDoSistema() {
		String sql = "select t from Acesso t";
		Query query = getDao().getEntityManager().createQuery(sql);

		return query.getResultList();
	}

	public List<Acesso> findByMenu(Long idMenu){
		StringBuilder sql = new StringBuilder();
		sql.append("select t from Acesso t ")
				.append("where t.menu.id =:idMenu ")
				.append("and t.acessoPai is null ")
				.append("order by t.descricao asc ");

		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter("idMenu", idMenu);

		return query.getResultList();
	}

	public List<Acesso> findByAcessoFilho(Long idAcesso){
		StringBuilder sql = new StringBuilder();
		sql.append("select t from Acesso t ")
				.append("where t.acessoPai =:idAcesso ")
				.append("order by t.descricao asc ");

		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter("idAcesso", idAcesso);

		return query.getResultList();
	}

	public Acesso findByFuncionalidade(String funcionalidade) {
		StringBuilder sql = new StringBuilder();
		sql.append("select t from Acesso t ")
				.append("where t.funcionalidade =:funcionalidade ")
				.append("order by t.descricao asc ");

		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter("funcionalidade", funcionalidade);
		List<Acesso> results = query.getResultList();

		if(!results.isEmpty()) {
			return results.get(0);
		}
		return null;
	}

}
