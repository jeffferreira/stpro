package br.com.juridico.impl;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.dto.CepDto;
import br.com.juridico.entidades.Cep;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Stateless
@LocalBean
public class CepSessioBean extends AbstractSessionBean<Cep> {

	/**
	 *
	 */
	private static final long serialVersionUID = 6293398488939147758L;

	public CepSessioBean() {
		super();
	}

	/**
	 *
	 * @param entityManager
	 */
	@Inject
	public CepSessioBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(Cep.class, entityManager));
	}

	public CepDto buscaCep(String cep) {
		CepDto cepUnico = findCepUnico(cep);
		if (cepUnico != null) {
			return cepUnico;
		}

		return findCepPorUf(cep);
	}

	private CepDto findCepPorUf(String cep) {
		String cepFormatado = cep.substring(0, 5);

		StringBuilder sql = new StringBuilder();
		sql.append("select c.uf from cep_log_index c where c.cep5 =:cep");

		Query query = getDao().getEntityManager().createNativeQuery(sql.toString());
		query.setParameter("cep", cepFormatado);

		List<Object[]> listResults = query.getResultList() ;
		CepDto result = null;

		if(!listResults.isEmpty()){
			Object[] object = {listResults.get(0)};
			String uf = (String) object[0];
			StringBuilder sqlUF = new StringBuilder();
			sqlUF.append("select c.cidade, c.logradouro, c.bairro, c.cep, c.tp_logradouro from ").append(uf).append(" c where c.cep=:cep ");
			Query queryUF = getDao().getEntityManager().createNativeQuery(sqlUF.toString());
			queryUF.setParameter("cep", cep);
			List<String[]> list = queryUF.getResultList();
			if(list.isEmpty()){
				return result;
			}
			result = new CepDto(list.get(0), false);
			result.setUf(uf);
		}

		return result;
	}

	private CepDto findCepUnico(String cep){
		StringBuilder sql = new StringBuilder();
		sql.append("select c.Nome, c.Cep, c.UF from cep_unico c where c.cep =:cep");

		Query query = getDao().getEntityManager().createNativeQuery(sql.toString());
		query.setParameter("cep", cep);

		List<Object[]> listResults = query.getResultList();
		CepDto result = null;

		if(!listResults.isEmpty()){
			Object[] linha = listResults.get(0);
			result = new CepDto(linha, true);
		}
		return result;
	}
}
