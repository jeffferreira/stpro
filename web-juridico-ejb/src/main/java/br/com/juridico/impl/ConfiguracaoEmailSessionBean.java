package br.com.juridico.impl;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.ConfiguracaoEmail;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 * 
 * @author Marcos
 *
 */
@Stateless
@LocalBean
public class ConfiguracaoEmailSessionBean extends AbstractSessionBean<ConfiguracaoEmail> {

	/**
	 *
	 */
	private static final long serialVersionUID = -3833477281795653214L;

	public ConfiguracaoEmailSessionBean() {
		super();
	}

	/**
	 *
	 * @param entityManager
	 */
	@Inject
	public ConfiguracaoEmailSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(ConfiguracaoEmail.class, entityManager));
	}

	@Override
	public OrderClause[] getDefaultOrder() {
		return new OrderClause[] { new OrderClause("username", OrderType.ASC) };
	}

	public ConfiguracaoEmail findByEmpresa(Long codigoEmpresa) {
		StringBuilder sb = new StringBuilder();
		sb.append(
				"select ce from ConfiguracaoEmail ce where ce.codigoEmpresa=:codigoEmpresa \n");

		Query query = getDao().getEntityManager().createQuery(sb.toString())
				.setParameter("codigoEmpresa", codigoEmpresa);

		return (ConfiguracaoEmail) query.getSingleResult();
	}
}
