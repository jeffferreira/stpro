package br.com.juridico.impl;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.MeioTramitacao;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;

/**
 * 
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class MeioTramitacaoSessionBean extends AbstractCrudSessionBean<MeioTramitacao> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2797985718865711543L;
	private static final String DESCRICAO = "descricao";
	private static final String MEIO_TRAMITACAO_FIND_BY_DESCRICAO = "MeioTramitacao.findByDescricao";
	private static final String CODIGO_EMPRESA = "codigoEmpresa";

	public MeioTramitacaoSessionBean() {
		super();
	}

	/**
	 * 
	 * @param entityManager
	 */
	@Inject
	public MeioTramitacaoSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(MeioTramitacao.class, entityManager));
	}

	@Override
	public OrderClause[] getDefaultOrder() {
		return new OrderClause[] { new OrderClause(DESCRICAO, OrderType.ASC) };
	}

	public List<MeioTramitacao> filtrados(ConsultaLazyFilter filtro) {
		filtro.setAscendente(true);
		filtro.setPropriedadeOrdenacao(DESCRICAO);
		return super.filtrados(filtro, MeioTramitacao.class);
	}

	public int quantidadeFiltrados(ConsultaLazyFilter filtro) {
		return super.quantidadeFiltrados(filtro, MeioTramitacao.class);
	}

	/**
	 * 
	 * @param descricao
	 * @param codigoEmpresa
	 * @return
	 */
	public MeioTramitacao findByDescricao(String descricao, Long codigoEmpresa) {
		Query query = getDao().getEntityManager().createNamedQuery(MEIO_TRAMITACAO_FIND_BY_DESCRICAO)
				.setParameter(DESCRICAO, descricao).setParameter(CODIGO_EMPRESA, codigoEmpresa);
		try {
			return (MeioTramitacao) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public boolean isDescricaoJaCadastrada(MeioTramitacao meioTramitacao, Long codigoEmpresa){
		StringBuilder sql = new StringBuilder();
		sql.append("select t from MeioTramitacao t where t.descricao=:descricao and t.codigoEmpresa=:codigoEmpresa and t.dataExclusao is null");

		Query query = getDao().getEntityManager().createQuery(sql.toString())
				.setParameter(DESCRICAO, meioTramitacao.getDescricao()).setParameter(CODIGO_EMPRESA, codigoEmpresa);

		List<MeioTramitacao> results = query.getResultList();

		if(results.isEmpty()) {
			return Boolean.FALSE;
		}
		return validaDuplicidade(meioTramitacao, results.get(0));
	}

	private boolean validaDuplicidade(MeioTramitacao meioTramitacao, MeioTramitacao result) {
		if(result != null && meioTramitacao.getId() == result.getId()) {
			return Boolean.FALSE;
		} else if(result != null && meioTramitacao.getId() != result.getId()){
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
}
