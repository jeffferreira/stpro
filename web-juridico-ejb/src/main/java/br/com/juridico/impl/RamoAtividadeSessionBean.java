package br.com.juridico.impl;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.RamoAtividade;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by Marcos Melo on 15/12/2016.
 */
@Stateless
@LocalBean
public class RamoAtividadeSessionBean extends AbstractCrudSessionBean<RamoAtividade> {

    private static final String DESCRICAO = "descricao";
    private static final String RAMO_ATIVIDADE_FIND_BY_DESCRICAO = "RamoAtividade.findByDescricao";
    private static final String CODIGO_EMPRESA = "codigoEmpresa";

    public RamoAtividadeSessionBean() {
        super();
    }

    /**
     *
     * @param entityManager
     */
    @Inject
    public RamoAtividadeSessionBean(@JuridicoEntityManager EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected void initDaoSet(EntityManager entityManager) {
        setDao(new GenericDao<>(RamoAtividade.class, entityManager));
    }

    @Override
    public OrderClause[] getDefaultOrder() {
        return new OrderClause[] { new OrderClause(DESCRICAO, OrderType.ASC) };
    }

    public List<RamoAtividade> filtrados(ConsultaLazyFilter filtro) {
        filtro.setAscendente(true);
        filtro.setPropriedadeOrdenacao(DESCRICAO);
        return super.filtrados(filtro, RamoAtividade.class);
    }

    public int quantidadeFiltrados(ConsultaLazyFilter filtro) {
        return super.quantidadeFiltrados(filtro, RamoAtividade.class);
    }

    /**
     *
     * @param descricao
     * @param codigoEmpresa
     * @return
     */
    public RamoAtividade findByDescricao(String descricao, Long codigoEmpresa) {
        Query query = getDao().getEntityManager().createNamedQuery(RAMO_ATIVIDADE_FIND_BY_DESCRICAO)
                .setParameter(DESCRICAO, descricao).setParameter(CODIGO_EMPRESA, codigoEmpresa);
        try {
            return (RamoAtividade) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public boolean isDescricaoJaCadastrada(RamoAtividade ramoAtividade, Long codigoEmpresa){
        StringBuilder sql = new StringBuilder();
        sql.append("select t from RamoAtividade t where t.descricao=:descricao and t.codigoEmpresa=:codigoEmpresa and t.dataExclusao is null");

        Query query = getDao().getEntityManager().createQuery(sql.toString())
                .setParameter(DESCRICAO, ramoAtividade.getDescricao()).setParameter(CODIGO_EMPRESA, codigoEmpresa);

        List<RamoAtividade> results = query.getResultList();

        if(results.isEmpty()) {
            return Boolean.FALSE;
        }
        return validaDuplicidade(ramoAtividade, results.get(0));
    }

    private boolean validaDuplicidade(RamoAtividade ramoAtividade, RamoAtividade result) {
        if(result != null && ramoAtividade.getId() == result.getId()) {
            return Boolean.FALSE;
        } else if(result != null && ramoAtividade.getId() != result.getId()){
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    @Override
    public List<RamoAtividade> retrieveAll(Long codigoEmpresa) {
        String sql = "select ra from RamoAtividade ra where ra.codigoEmpresa=:codigoEmpresa and ra.dataExclusao is null order by ra.descricao asc";
        return retrieveByExclusaoLogica(sql, codigoEmpresa);
    }
}
