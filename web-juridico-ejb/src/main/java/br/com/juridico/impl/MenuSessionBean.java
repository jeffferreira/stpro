package br.com.juridico.impl;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.Menu;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by jefferson on 06/10/2016.
 */
@Stateless
@LocalBean
public class MenuSessionBean extends AbstractSessionBean<Menu> {

    private static final String FIND_BY_DESCRICAO = "Menu.findByDescricao";

    public MenuSessionBean() {
        super();
    }

    @Inject
    public MenuSessionBean(@JuridicoEntityManager EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected void initDaoSet(EntityManager entityManager) {
        setDao(new GenericDao<>(Menu.class, entityManager));
    }

    /**
     *
     * @param descricao
     * @return
     */
    public Menu findByDescricao(String descricao) {
        Query query = getDao().getEntityManager().createNamedQuery(FIND_BY_DESCRICAO).setParameter("descricao", descricao);
        try {
            return (Menu) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public List<Menu> findAllMenus(){
        String sql = "select m from Menu m ";
        Query query = getDao().getEntityManager().createQuery(sql);
        return query.getResultList();
    }
}
