package br.com.juridico.impl;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.Forum;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;

/**
 * 
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class ForumSessionBean extends AbstractCrudSessionBean<Forum> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3246638270853559888L;
	private static final String DESCRICAO = "descricao";
	private static final String CODIGO_EMPRESA = "codigoEmpresa";
	private static final String FORUM_FIND_BY_DESCRICAO = "Forum.findByDescricao";

	public ForumSessionBean() {
		super();
	}

	/**
	 * 
	 * @param entityManager
	 */
	@Inject
	public ForumSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(Forum.class, entityManager));
	}

	@Override
	public OrderClause[] getDefaultOrder() {
		return new OrderClause[] { new OrderClause(DESCRICAO, OrderType.ASC) };
	}

	public List<Forum> filtrados(ConsultaLazyFilter filtro) {
		filtro.setAscendente(true);
		filtro.setPropriedadeOrdenacao(DESCRICAO);
		return super.filtrados(filtro, Forum.class);
	}

	public int quantidadeFiltrados(ConsultaLazyFilter filtro) {
		return super.quantidadeFiltrados(filtro, Forum.class);
	}

	/**
	 * 
	 * @param descricao
	 * @param codigoEmpresa
	 * @return
	 */
	public Forum findByDescricao(String descricao, Long codigoEmpresa) {
		Query query = getDao().getEntityManager().createNamedQuery(FORUM_FIND_BY_DESCRICAO)
				.setParameter(DESCRICAO, descricao).setParameter(CODIGO_EMPRESA, codigoEmpresa);
		try {
			return (Forum) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public boolean isDescricaoJaCadastrada(Forum forum, Long codigoEmpresa){
		StringBuilder sql = new StringBuilder();
		sql.append("select t from Forum t where t.descricao=:descricao and t.codigoEmpresa=:codigoEmpresa and t.dataExclusao is null");

		Query query = getDao().getEntityManager().createQuery(sql.toString())
				.setParameter(DESCRICAO, forum.getDescricao()).setParameter(CODIGO_EMPRESA, codigoEmpresa);

		List<Forum> results = query.getResultList();

		if(results.isEmpty()) {
			return Boolean.FALSE;
		}
		return validaDuplicidade(forum, results.get(0));
	}

	private boolean validaDuplicidade(Forum forum, Forum result) {
		if(result != null && forum.getId() == result.getId()) {
			return Boolean.FALSE;
		} else if(result != null && forum.getId() != result.getId()){
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
}
