package br.com.juridico.impl;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.StatusAgenda;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;

/**
 * 
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class StatusAgendaSessionBean extends AbstractCrudSessionBean<StatusAgenda> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6812000707435610578L;
	private static final String DESCRICAO = "descricao";
	private static final String FIND_BY_DESCRICAO = "StatusAgenda.findByDescricao";
	private static final String CODIGO_EMPRESA = "codigoEmpresa";

	public StatusAgendaSessionBean() {
		super();
	}

	/**
	 * 
	 * @param entityManager
	 */
	@Inject
	public StatusAgendaSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(StatusAgenda.class, entityManager));
	}

	@Override
	public OrderClause[] getDefaultOrder() {
		return new OrderClause[] { new OrderClause(DESCRICAO, OrderType.ASC) };
	}

	public List<StatusAgenda> filtrados(ConsultaLazyFilter filtro) {
		filtro.setAscendente(true);
		filtro.setPropriedadeOrdenacao(DESCRICAO);
		return super.filtrados(filtro, StatusAgenda.class);
	}

	public int quantidadeFiltrados(ConsultaLazyFilter filtro) {
		return super.quantidadeFiltrados(filtro, StatusAgenda.class);
	}

	/**
	 * 
	 * @param descricao
	 * @param codigoEmpresa
	 * @return
	 */
	public StatusAgenda findByDescricao(String descricao, Long codigoEmpresa) {
		Query query = getDao().getEntityManager().createNamedQuery(FIND_BY_DESCRICAO)
				.setParameter(DESCRICAO, descricao).setParameter(CODIGO_EMPRESA, codigoEmpresa);
		try {
			return (StatusAgenda) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public boolean isDescricaoJaCadastrada(StatusAgenda statusAgenda, Long codigoEmpresa){
		StringBuilder sql = new StringBuilder();
		sql.append("select t from StatusAgenda t where t.descricao=:descricao and t.codigoEmpresa=:codigoEmpresa and t.dataExclusao is null");

		Query query = getDao().getEntityManager().createQuery(sql.toString())
				.setParameter(DESCRICAO, statusAgenda.getDescricao()).setParameter(CODIGO_EMPRESA, codigoEmpresa);

		List<StatusAgenda> results = query.getResultList();

		if(results.isEmpty()) {
			return Boolean.FALSE;
		}
		return validaDuplicidade(statusAgenda, results.get(0));
	}

	public List<StatusAgenda> findAllStatusAtivos(Long codigoEmpresa){
		StringBuilder sql = new StringBuilder();
		sql.append("select t from StatusAgenda t where t.codigoEmpresa=:codigoEmpresa and t.dataExclusao is null order by t.descricao ASC");

		Query query = getDao().getEntityManager().createQuery(sql.toString()).setParameter(CODIGO_EMPRESA, codigoEmpresa);

		return query.getResultList();

	}

	private boolean validaDuplicidade(StatusAgenda statusAgenda, StatusAgenda result) {
		if(result != null && statusAgenda.getId() == result.getId()) {
			return Boolean.FALSE;
		} else if(result != null && statusAgenda.getId() != result.getId()){
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
}
