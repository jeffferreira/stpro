package br.com.juridico.impl;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.Processo;
import br.com.juridico.entidades.ProjudiprProcesso;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

/**
 * 
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class ProjudiprProcessoSessionBean extends AbstractSessionBean<ProjudiprProcesso> {

	/**
	 *
	 */
	private static final long serialVersionUID = 6638941542426771110L;


	public ProjudiprProcessoSessionBean() {
		super();
	}

	/**
	 *
	 * @param entityManager
	 */
	@Inject
	public ProjudiprProcessoSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(ProjudiprProcesso.class, entityManager));
	}

	public List<ProjudiprProcesso> findByProcessos(Processo processo){
		StringBuilder sql = new StringBuilder();
		sql.append("select t from ProjudiprProcesso t where id=:idProcesso order by dataCadastro DESC");

		Query query = getDao().getEntityManager().createQuery(sql.toString())
				.setParameter("idProcesso", processo.getId());

		return query.getResultList();
	}

}
