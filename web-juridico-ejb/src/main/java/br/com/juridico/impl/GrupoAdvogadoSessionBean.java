package br.com.juridico.impl;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.Grupo;
import br.com.juridico.entidades.GrupoAdvogado;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;

/**
 * 
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class GrupoAdvogadoSessionBean extends AbstractSessionBean<GrupoAdvogado> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1794922449591480738L;

	public GrupoAdvogadoSessionBean() {
		// CONSTRUTOR
	}

	/**
	 * 
	 * @param entityManager
	 */
	@Inject
	public GrupoAdvogadoSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<GrupoAdvogado>(GrupoAdvogado.class, entityManager));
	}

	@Override
	public OrderClause[] getDefaultOrder() {
		return new OrderClause[] { new OrderClause("id", OrderType.ASC) };
	}

	@Override
	public List<GrupoAdvogado> retrieveAll(Long codigoEmpresa) {
		String sql = "select g from GrupoAdvogado g where g.codigoEmpresa=:codigoEmpresa and g.dataExclusao is null";
		return retrieveByExclusaoLogica(sql, codigoEmpresa);
	}

	/**
	 * 
	 * @param grupo
	 * @param codigoEmpresa
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<GrupoAdvogado> findByGrupo(Grupo grupo, Long codigoEmpresa) {
		StringBuilder sb = new StringBuilder();
		sb.append("select ga from GrupoAdvogado ga where ga.codigoEmpresa=:codigoEmpresa and ga.grupo.id =:idGrupo \n");

		Query query = getDao().getEntityManager().createQuery(sb.toString()).setParameter("idGrupo", grupo.getId())
				.setParameter("codigoEmpresa", codigoEmpresa);

		return query.getResultList();
	}

}
