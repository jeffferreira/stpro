package br.com.juridico.impl;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.PessoaFisica;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

/**
 * 
 * @author Marcos
 *
 */
@Stateless
@LocalBean
public class ColaboradorSessionBean extends AbstractSessionBean<PessoaFisica> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4632394473084908350L;

	public ColaboradorSessionBean() {
		super();
	}

	@Inject
	public ColaboradorSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(PessoaFisica.class, entityManager));
	}

	@Override
	public OrderClause[] getDefaultOrder() {
		return new OrderClause[] { new OrderClause("pessoa.descricao", OrderType.ASC) };
	}

	@Override
	public List<PessoaFisica> retrieveAll(Long codigoEmpresa) {
		String sql = "select pf from PessoaFisica pf where pf.codigoEmpresa=:codigoEmpresa and pf.colaborador = 'S' and pf.pessoa.ativo = true";

		return retrieveByExclusaoLogica(sql, codigoEmpresa);
	}

	@SuppressWarnings("unchecked")
	public PessoaFisica findNumeroOabExistente(PessoaFisica colaborador) {
		StringBuilder sql = new StringBuilder();
		sql.append("select pf from PessoaFisica pf \n");
		sql.append("where pf.id!=:id \n");
		sql.append("and pf.nrOab=:numeroOab \n");
		sql.append("and pf.ufOab=:ufOab \n");
		sql.append("and pf.codigoEmpresa=:codigoEmpresa \n");

		Query query = getDao().getEntityManager().createQuery(sql.toString())
				.setParameter("id", colaborador.getId() == null ? 0 : colaborador.getId())
				.setParameter("numeroOab", colaborador.getNrOab())
				.setParameter("ufOab", colaborador.getUfOab())
				.setParameter("codigoEmpresa", colaborador.getCodigoEmpresa());
		List<PessoaFisica> results = query.getResultList();

		if (!results.isEmpty()) {
			return results.get(0);
		}

		return null;

	}
}
