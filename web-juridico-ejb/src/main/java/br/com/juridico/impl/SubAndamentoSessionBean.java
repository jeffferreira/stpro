package br.com.juridico.impl;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.SubAndamento;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;

/**
 * 
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class SubAndamentoSessionBean extends AbstractCrudSessionBean<SubAndamento> {

	/**
	 *
	 */
	private static final long serialVersionUID = 6812000707435610578L;
	private static final String DESCRICAO = "descricao";
	private static final String SUB_ANDAMENTO_FIND_BY_DESCRICAO = "SubAndamento.findByDescricao";
	private static final String SUB_ANDAMENTO_FIND_LIKE_BY_DESCRICAO = "SubAndamento.findLikeByDescricao";
	private static final String CODIGO_EMPRESA = "codigoEmpresa";

	public SubAndamentoSessionBean() {
		super();
	}

	/**
	 *
	 * @param entityManager
	 */
	@Inject
	public SubAndamentoSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(SubAndamento.class, entityManager));
	}

	@Override
	public OrderClause[] getDefaultOrder() {
		return new OrderClause[] { new OrderClause(DESCRICAO, OrderType.ASC) };
	}

	public List<SubAndamento> filtrados(ConsultaLazyFilter filtro) {
		filtro.setAscendente(true);
		filtro.setPropriedadeOrdenacao(DESCRICAO);
		return super.filtrados(filtro, SubAndamento.class);
	}

	public int quantidadeFiltrados(ConsultaLazyFilter filtro) {
		return super.quantidadeFiltrados(filtro, SubAndamento.class);
	}

	/**
	 * 
	 * @param descricao
	 * @param codigoEmpresa
	 * @return
	 */
	public SubAndamento findByDescricao(String descricao, Long codigoEmpresa) {
		Query query = getDao().getEntityManager().createNamedQuery(SUB_ANDAMENTO_FIND_BY_DESCRICAO)
				.setParameter(DESCRICAO, descricao).setParameter(CODIGO_EMPRESA, codigoEmpresa);
		try {
			return (SubAndamento) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public List<SubAndamento> findListByDescricao(String descricao, Long codigoEmpresa) {
		Query query = getDao().getEntityManager().createNamedQuery(SUB_ANDAMENTO_FIND_LIKE_BY_DESCRICAO)
				.setParameter(DESCRICAO, "%"+descricao+"%").setParameter(CODIGO_EMPRESA, codigoEmpresa);
		return query.getResultList();
	}
	
	public boolean isDescricaoJaCadastrada(SubAndamento subAndamento, Long codigoEmpresa){
		StringBuilder sql = new StringBuilder();
		sql.append("select t from SubAndamento t where t.descricao=:descricao and t.codigoEmpresa=:codigoEmpresa and t.dataExclusao is null");

		Query query = getDao().getEntityManager().createQuery(sql.toString())
				.setParameter(DESCRICAO, subAndamento.getDescricao()).setParameter(CODIGO_EMPRESA, codigoEmpresa);

		List<SubAndamento> results = query.getResultList();

		if(results.isEmpty()) {
			return Boolean.FALSE;
		}
		return validaDuplicidade(subAndamento, results.get(0));
	}

	private boolean validaDuplicidade(SubAndamento subAndamento, SubAndamento result) {
		if(result != null && subAndamento.getId() == result.getId()) {
			return Boolean.FALSE;
		} else if(result != null && subAndamento.getId() != result.getId()){
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
}
