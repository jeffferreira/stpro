package br.com.juridico.impl;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.RelatorioCriterioProcesso;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

/**
 *
 * @author Marcos
 *
 */
@Stateless
@LocalBean
public class RelatorioCriterioProcessoSessionBean extends AbstractCrudSessionBean<RelatorioCriterioProcesso> {

	/**
	 *
	 */
	private static final long serialVersionUID = 6812000707435610578L;

	private static final String CODIGO_EMPRESA = "codigoEmpresa";

	public RelatorioCriterioProcessoSessionBean() {
		super();
	}

	/**
	 *
	 * @param entityManager
	 */
	@Inject
	public RelatorioCriterioProcessoSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(RelatorioCriterioProcesso.class, entityManager));
	}

	@Override
	public OrderClause[] getDefaultOrder() {
		return new OrderClause[] { new OrderClause("descricao", OrderType.ASC) };
	}

	public List<RelatorioCriterioProcesso> findCriteriosAtivosByIdRelatorio(Long idRelatorioFiltroProcesso){
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT r FROM RelatorioCriterioProcesso r WHERE r.relatorioFiltroProcesso.id=:idRelatorioFiltroProcesso \n");
		sql.append("and r.filtroAtivo = true \n");

		Query query = getDao().getEntityManager().createQuery(sql.toString());
		query.setParameter("idRelatorioFiltroProcesso", idRelatorioFiltroProcesso);
		return query.getResultList();
	}

}
