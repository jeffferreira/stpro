package br.com.juridico.impl;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.Parte;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.List;

/**
 *
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class ParteSessionBean extends AbstractSessionBean<Parte> {

	/**
	 *
	 */
	private static final long serialVersionUID = -8806120845282279395L;

	public ParteSessionBean() {
		super();
	}

	/**
	 *
	 * @param entityManager
	 */
	@Inject
	public ParteSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<>(Parte.class, entityManager));
	}

	/**
	 * @param codigoEmpresa
	 */
	public List<Parte> retrieveAll(Long codigoEmpresa) {
		String sql = "select p from Parte p where p.codigoEmpresa=:codigoEmpresa";
		Query query = getDao().getEntityManager().createQuery(sql);
		query.setParameter("codigoEmpresa", codigoEmpresa);

		return query.getResultList();
	}

	/**
	 *
	 * @param codigoProcesso
	 * @param codigoEmpresa
	 * @return
	 */
	public List<Parte> findByProcesso(Long codigoProcesso, Long codigoEmpresa) {
		String sql = "select p from Parte p where p.codigoEmpresa=:codigoEmpresa and p.processo.id=:codigoProcesso";
		Query query = getDao().getEntityManager().createQuery(sql);
		query.setParameter("codigoProcesso", codigoProcesso);
		query.setParameter("codigoEmpresa", codigoEmpresa);

		return query.getResultList();
	}

	public List<BigInteger> findProcessoIdByPessoa(Long idPessoa, Long codigoEmpresa){
		StringBuilder sql = new StringBuilder();
		sql.append(" select p.fk_processo ");
		sql.append(" from tb_parte pt ");
		sql.append(" inner join tb_processo p on p.id_processo = pt.fk_processo ");
		sql.append(" where p.fl_status = 'A' and p.nr_empresa =:codigoEmpresa ");

		if(idPessoa != null) {
			sql.append(" and pt.fk_pessoa =:idPessoa ");
		}
		sql.append(" group by pt.fk_processo ");
		sql.append(" order by p.dt_cadastro desc ");

		Query query = getDao().getEntityManager().createNativeQuery(sql.toString());
		query.setParameter("codigoEmpresa", codigoEmpresa);

		if(idPessoa != null) {
			query.setParameter("idPessoa", idPessoa);
		}
		return query.getResultList();
	}


}
