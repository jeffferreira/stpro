package br.com.juridico.impl;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.ParametroEmail;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by jeffersonl2009_00 on 18/08/2016.
 */
@Stateless
@LocalBean
public class ParametroEmailSessionBean extends AbstractSessionBean<ParametroEmail> {

    public ParametroEmailSessionBean() {
        super();
    }

    /**
     *
     * @param entityManager
     */
    @Inject
    public ParametroEmailSessionBean(@JuridicoEntityManager EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected void initDaoSet(EntityManager entityManager) {
        setDao(new GenericDao<>(ParametroEmail.class, entityManager));
    }

    public List<ParametroEmail> findByGeralAndAgenda(){
        String sql = "select t from ParametroEmail t where t.chaveSessao like 'AGENDA_%' or t.chaveSessao like 'GERAL_%'";
        Query query = getDao().getEntityManager().createQuery(sql);
        return query.getResultList();
    }

    public List<ParametroEmail> findByGeralAndSenha(){
        String sql = "select t from ParametroEmail t where t.chaveSessao like 'SENHA_%' or t.chaveSessao like 'GERAL_%'";
        Query query = getDao().getEntityManager().createQuery(sql);
        return query.getResultList();
    }

    public List<ParametroEmail> findByProcesso(){
        String sql = "select t from ParametroEmail t where t.chaveSessao like 'PROC_%'";
        Query query = getDao().getEntityManager().createQuery(sql);
        return query.getResultList();
    }

    public ParametroEmail findBySigla(String sigla){
        String sql = "select t from ParametroEmail t where t.sigla =:sigla";
        Query query = getDao().getEntityManager().createQuery(sql);
        query.setParameter("sigla", sigla);
        List<ParametroEmail> results = query.getResultList();
        if(results.isEmpty()) {
            return null;
        }
        return results.get(0);
    }

}
