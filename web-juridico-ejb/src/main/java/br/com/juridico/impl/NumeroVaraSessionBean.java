package br.com.juridico.impl;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.NumeroVara;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;

/**
 * 
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class NumeroVaraSessionBean extends AbstractSessionBean<NumeroVara> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5709764194514870805L;

	private static final String VARA_UNICA = "Única";

	public NumeroVaraSessionBean() {
		// CONSTRUTOR
	}

	/**
	 * 
	 * @param entityManager
	 */
	@Inject
	public NumeroVaraSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<NumeroVara>(NumeroVara.class, entityManager));
	}

	@Override
	public OrderClause[] getDefaultOrder() {
		return new OrderClause[] { new OrderClause("descricao", OrderType.ASC) };
	}

	/**
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<NumeroVara> findAll() {
		Query query = getDao().getEntityManager().createQuery("select t from NumeroVara t ");
		return query.getResultList();
	}

	/**
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public NumeroVara findByVaraUnica() {
		Query query = getDao().getEntityManager()
				.createQuery("select t from NumeroVara t where t.descricao =:varaUnica ");
		query.setParameter("varaUnica", VARA_UNICA);

		List<NumeroVara> numerosVara = query.getResultList();

		if (!numerosVara.isEmpty()) {
			return numerosVara.get(0);
		}
		return null;
	}
}
