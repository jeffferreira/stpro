package br.com.juridico.impl;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.Uf;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;

/**
 * 
 * @author Jefferson
 *
 */
@Stateless
@LocalBean
public class UfSessionBean extends AbstractSessionBean<Uf> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1547562349300561067L;

	public UfSessionBean() {
		// CONSTRUTOR
	}

	/**
	 * 
	 * @param entityManager
	 */
	@Inject
	public UfSessionBean(@JuridicoEntityManager EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected void initDaoSet(EntityManager entityManager) {
		setDao(new GenericDao<Uf>(Uf.class, entityManager));
	}

	@Override
	public OrderClause[] getDefaultOrder() {
		return new OrderClause[] { new OrderClause("sigla", OrderType.ASC) };
	}

	/**
	 * 
	 * @param sigla
	 * @return
	 */
	public Uf findBySigla(String sigla) {
		Query query = getDao().getEntityManager().createQuery("SELECT U FROM Uf U WHERE U.sigla =:sigla ");
		query.setParameter("sigla", sigla);

		try {
			return (Uf) query.getSingleResult();
		} catch (NoResultException ignore) {
			LOGGER.info(ignore.getCause(), ignore);
			return null;
		}

	}

	/**
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Uf> findAll() {
		Query query = getDao().getEntityManager().createQuery("SELECT U FROM Uf U ");
		return query.getResultList();
	}
}
