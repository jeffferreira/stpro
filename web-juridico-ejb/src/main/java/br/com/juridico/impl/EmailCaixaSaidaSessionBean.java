package br.com.juridico.impl;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.EmailCaixaSaida;
import br.com.juridico.enums.StatusEmailCaixaSaidaIndicador;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.jms.GeradorDeMensagem;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.List;

import static br.com.juridico.enums.StatusEmailCaixaSaidaIndicador.APROVADO;

/**
 * Created by Jefferson on 06/08/2016.
 */
@Stateless
@Local
public class EmailCaixaSaidaSessionBean extends AbstractSessionBean<EmailCaixaSaida> {

    private static final String CODIGO_EMPRESA = "codigoEmpresa";
    private static final String FLAG_STATUS = "flagStatus";
    private static final String FLAG_PROCESSADO = "flagProcessado";

    public EmailCaixaSaidaSessionBean() {
        super();
    }

    @Inject
    private GeradorDeMensagem geradorDeMensagem;

    @Inject
    public EmailCaixaSaidaSessionBean(@JuridicoEntityManager EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected void initDaoSet(EntityManager entityManager) {
        setDao(new GenericDao<>(EmailCaixaSaida.class, entityManager));
    }

    public int countEmails(String status, boolean processados, Long codigoEmpresa){
        StringBuilder sql = new StringBuilder();
        sql.append("select count(*) from TB_EMAIL_CAIXA_SAIDA t where t.FL_STATUS =:flagStatus ");
        sql.append("and t.NR_EMPRESA =:codigoEmpresa ");
        if (processados){
            sql.append("and t.FL_PROCESSADO = 'T' ");
        } else {
            sql.append("and t.FL_PROCESSADO = 'F' ");
        }

        Query query = getDao().getEntityManager().createNativeQuery(sql.toString());
        query.setParameter(CODIGO_EMPRESA, codigoEmpresa);
        query.setParameter(FLAG_STATUS, status);
        BigInteger result = (BigInteger) query.getSingleResult();
        return result.intValue();
    }

    public List<EmailCaixaSaida> filtrados(ConsultaLazyFilter filtro, boolean processados) {
        Criteria criteria = criarCriteriaParaFiltro(filtro, processados);
        criteria.setFirstResult(filtro.getPrimeiroRegistro());
        criteria.setMaxResults(filtro.getQuantidadeRegistros());

        if (filtro.isAscendente() && filtro.getPropriedadeOrdenacao() != null) {
            criteria.addOrder(Order.asc(filtro.getPropriedadeOrdenacao()));
        } else if (filtro.getPropriedadeOrdenacao() != null) {
            criteria.addOrder(Order.desc(filtro.getPropriedadeOrdenacao()));
        }
        return criteria.list();
    }

    public List<EmailCaixaSaida> findEmailsPendentes(StatusEmailCaixaSaidaIndicador status, Long codigoEmpresa, boolean processados) {
        Criteria criteria = getDao().getHibernateSession().createCriteria(EmailCaixaSaida.class);
        Character c = status.getValue().charAt(0);
        criteria.add(Restrictions.eq(FLAG_STATUS, c));
        criteria.add(Restrictions.eq(CODIGO_EMPRESA, codigoEmpresa));
        criteria.add(Restrictions.eq(FLAG_PROCESSADO, processados));
        List<EmailCaixaSaida> results = criteria.list();
        return results;
    }

    private Criteria criarCriteriaParaFiltro(ConsultaLazyFilter filtro, boolean processados) {
        Criteria criteria = getDao().getHibernateSession().createCriteria(EmailCaixaSaida.class);
        Character status = filtro.getDescricao().charAt(0);
        criteria.add(Restrictions.eq(FLAG_STATUS, status));
        criteria.add(Restrictions.eq(CODIGO_EMPRESA, filtro.getCodigoEmpresa()));
        criteria.add(Restrictions.eq(FLAG_PROCESSADO, processados));

        return criteria;
    }

    public void createSemAprovacao(EmailCaixaSaida emailSaida) throws ValidationException {
        emailSaida.setFlagStatus(APROVADO.getValue().charAt(0));
        super.create(emailSaida);
        geradorDeMensagem.enviarMensagem(emailSaida);
    }
}
