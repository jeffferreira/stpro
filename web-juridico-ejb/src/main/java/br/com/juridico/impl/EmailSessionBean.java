package br.com.juridico.impl;

import br.com.juridico.annotation.JuridicoEntityManager;
import br.com.juridico.dao.GenericDao;
import br.com.juridico.entidades.Email;
import br.com.juridico.filter.ConsultaLazyFilter;
import br.com.juridico.util.OrderClause;
import br.com.juridico.util.OrderType;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by jeffersonl2009_00 on 27/06/2016.
 */
@Stateless
@LocalBean
public class EmailSessionBean extends AbstractCrudSessionBean<Email> {

    /**
     *
     */
    private static final long serialVersionUID = -1794922449591480738L;
    private static final String SIGLA = "sigla";
    private static final String CODIGO_EMPRESA = "codigoEmpresa";
    private static final String DESCRICAO = "descricao";

    public EmailSessionBean() {
        super();
    }

    /**
     *
     * @param entityManager
     */
    @Inject
    public EmailSessionBean(@JuridicoEntityManager EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected void initDaoSet(EntityManager entityManager) {
        setDao(new GenericDao<>(Email.class, entityManager));
    }

    @Override
    public OrderClause[] getDefaultOrder() {
        return new OrderClause[] { new OrderClause(DESCRICAO, OrderType.ASC) };
    }

    public List<Email> filtrados(ConsultaLazyFilter filtro) {
        return super.filtrados(filtro, Email.class);
    }

    public int quantidadeFiltrados(ConsultaLazyFilter filtro) {
        return super.quantidadeFiltrados(filtro, Email.class);
    }

    public Email findBySigla(String sigla, Long codigoEmpresa){
        String sql = "select e from Email e where e.codigoEmpresa=:codigoEmpresa and e.sigla =:sigla";
        Query query = getDao().getEntityManager().createQuery(sql).setParameter(SIGLA, sigla).setParameter(CODIGO_EMPRESA, codigoEmpresa);
        try {
            return (Email) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

}
