package br.com.juridico.state.controladoria;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by jefferson.ferreira on 03/02/2017.
 */
public class RejeitaControladoriaTest {

    private RejeitaControladoria controladoria;

    @Before
    public void up() {
        this.controladoria = new RejeitaControladoria();
    }

    @Test
    public void controladoriaAction_nao_possui_configuracao_test() throws Exception {

    }

    @Test
    public void controladoriaAction_possui_com_status_a_cumprir_configuracao_test() throws Exception {

    }

    @Test
    public void controladoriaAction_possui_sem_status_a_cumprir_configuracao_test() throws Exception {

    }
}