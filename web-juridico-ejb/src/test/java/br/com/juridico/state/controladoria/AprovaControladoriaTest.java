package br.com.juridico.state.controladoria;

import br.com.juridico.entidades.StatusAgenda;
import br.com.juridico.enums.ControladoriaIndicador;
import br.com.juridico.state.ControladoriaState;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Created by jefferson.ferreira on 03/02/2017.
 */
public class AprovaControladoriaTest {

    private ControladoriaContext context;
    private ControladoriaState aprovacaoState;

    @Before
    public void up() {
        this.context = new ControladoriaContext();
    }

    @Test
    public void controladoriaAction_possui_configuracao_statusAgenda_null_test() throws Exception {
        boolean possuiConfiguracaoParaEmpresa = Boolean.TRUE;
        this.aprovacaoState = new AprovaControladoria();
        this.context.setState(aprovacaoState);
        assertNull(this.context.controladoriaAction(false, null, possuiConfiguracaoParaEmpresa, null));
    }

    @Test
    public void controladoriaAction_nao_possui_configuracao_test() throws Exception {
        boolean possuiConfiguracaoParaEmpresa = Boolean.FALSE;
        StatusAgenda statusAgendaACumprir = new StatusAgenda(1L, "À cumprir", 1L);
        this.aprovacaoState = new AprovaControladoria();
        this.context.setState(aprovacaoState);
        assertEquals(ControladoriaIndicador.NAO_SE_APLICA, this.context.controladoriaAction(false, statusAgendaACumprir, possuiConfiguracaoParaEmpresa, null));
    }

    @Test
    public void controladoriaAction_possui_configuracao_e_status_inicial_passa_controladoria_test() throws Exception {
        boolean possuiConfiguracaoParaEmpresa = Boolean.TRUE;
        StatusAgenda statusAgendaACumprir = new StatusAgenda(1L, "À cumprir", 1L);
        statusAgendaACumprir.setStatusAcumprirPassaControladoria(Boolean.TRUE);
        this.aprovacaoState = new AprovaControladoria();
        this.context.setState(aprovacaoState);
        assertEquals(ControladoriaIndicador.PENDENTE, this.context.controladoriaAction(false, statusAgendaACumprir, possuiConfiguracaoParaEmpresa, null));
    }

    @Test
    public void controladoriaAction_controladoria_aprovado_test() throws Exception {
        boolean possuiConfiguracaoParaEmpresa = Boolean.TRUE;
        StatusAgenda statusAgendaACumprir = new StatusAgenda(2L, "Cumprido", 1L);
        this.aprovacaoState = new AprovaControladoria();
        this.context.setState(aprovacaoState);
        assertEquals(ControladoriaIndicador.APROVADO ,this.context.controladoriaAction(false, statusAgendaACumprir, possuiConfiguracaoParaEmpresa, ControladoriaIndicador.PENDENTE));
    }

    @Test
    public void controladoriaAction_possui_configuracao_e_status_inicial_nao_passa_controladoria_test() throws Exception {
        boolean possuiConfiguracaoParaEmpresa = Boolean.TRUE;
        StatusAgenda statusAgendaACumprir = new StatusAgenda(1L, "À cumprir", 1L);
        statusAgendaACumprir.setStatusAcumprirPassaControladoria(Boolean.FALSE);
        this.aprovacaoState = new AprovaControladoria();
        this.context.setState(aprovacaoState);
        assertEquals(ControladoriaIndicador.NAO_SE_APLICA ,this.context.controladoriaAction(false, statusAgendaACumprir, possuiConfiguracaoParaEmpresa, null));
    }

    @Test
    public void controladoriaAction_possui_configuracao_e_edicao_passa_controladoria_test() throws Exception {
        boolean possuiConfiguracaoParaEmpresa = Boolean.TRUE;
        StatusAgenda statusAgendaACumprir = new StatusAgenda(1L, "À cumprir", 1L);
        this.aprovacaoState = new AprovaControladoria();
        this.context.setState(aprovacaoState);
        assertEquals(ControladoriaIndicador.PENDENTE ,this.context.controladoriaAction(true, statusAgendaACumprir, possuiConfiguracaoParaEmpresa, null));
    }
}