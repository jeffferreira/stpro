package br.com.juridico.dto.acessos;

import br.com.juridico.entidades.*;
import br.com.juridico.enums.MenuIndicador;
import br.com.juridico.impl.AcessoSessionBean;
import br.com.juridico.impl.DireitoPerfilSessionBean;
import br.com.juridico.impl.PerfilAcessoSessionBean;
import com.google.common.collect.Lists;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

/**
 * Created by jeffersonl2009_00 on 28/09/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public abstract class AbstractAcessoObjectsTest {

    protected static final Long EMPRESA = 1L;

    @Mock
    protected AcessoSessionBean acessoSessionBean;
    @Mock
    protected DireitoPerfilSessionBean direitoPerfilSessionBean;
    @Mock
    protected PerfilAcessoSessionBean perfilAcessoSessionBean;


    protected Direito getDireito(String silga, String descricao) {
        Direito direito = new Direito(silga, descricao);
        direito.setId(1L);
        return direito;
    }

    protected Perfil getPerfil() {
        Perfil perfil = new Perfil("ADMIN", "Administrador do sistema", EMPRESA, Boolean.TRUE);
        perfil.setId(1L);
        return perfil;
    }

    protected Acesso getAcesso(){
        Acesso acesso = new Acesso();
        acesso.setId(90L);
        Menu menu = new Menu();
        menu.setId(5L);
        acesso.setMenu(menu);
        acesso.setFuncionalidade("COLABORADOR_COLABORADOR_EMAILS_FORM");
        acesso.setDescricao("Pessoas - Emails do Colaborador");
        acesso.setFlagOperacao(1);
        acesso.setAcessoPai(85L);
        return acesso;
    }

    protected void carregaMockDireitoPerfil(Direito direito){
        Perfil perfil = getPerfil();
        PerfilAcesso perfilAcesso = new PerfilAcesso(perfil, getAcesso(), EMPRESA);
        DireitoPerfil direitoPerfil = new DireitoPerfil(direito, perfilAcesso, EMPRESA);
        Mockito.when(direitoPerfilSessionBean.findByPerfilAndDireito(perfil.getId(), direito.getId(), EMPRESA)).thenReturn(direitoPerfil);
    }

    protected void carregaMockAcessos(){
        List<Acesso> acessos = Lists.newArrayList();
        acessos.add(getAcesso());
        Mockito.when(acessoSessionBean.findByMenu(MenuIndicador.PESSOAS.getValue())).thenReturn(acessos);
    }

    protected void carregaMockAcessosFilhos() {
        List<Acesso> acessos = Lists.newArrayList();
        acessos.add(getAcesso());
        Mockito.when(acessoSessionBean.findByAcessoFilho(getAcesso().getId())).thenReturn(acessos);
    }

    protected void carregaMockAcessosFilhosNulo() {
        List<Acesso> acessos = Lists.newArrayList();
        acessos.add(getAcesso());
        Mockito.when(acessoSessionBean.findByAcessoFilho(getAcesso().getId())).thenReturn(null);
    }

    protected void carregaMockPerfilAcesso(){
        PerfilAcesso perfilAcesso = new PerfilAcesso(getPerfil(), getAcesso(), EMPRESA);
        Mockito.when(perfilAcessoSessionBean.findByPerfilAndAcesso(getPerfil().getId(), getAcesso().getId(), EMPRESA)).thenReturn(perfilAcesso);
    }

    protected void carregaMockPerfilAcessoNullo(){
        PerfilAcesso perfilAcesso = new PerfilAcesso(getPerfil(), getAcesso(), EMPRESA);
        Mockito.when(perfilAcessoSessionBean.findByPerfilAndAcesso(getPerfil().getId(), getAcesso().getId(), EMPRESA)).thenReturn(null);
    }

}
