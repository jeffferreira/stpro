package br.com.juridico.dto.acessos;

import br.com.juridico.dto.PerfilAcessoDto;
import br.com.juridico.entidades.Acesso;
import br.com.juridico.entidades.Direito;
import br.com.juridico.entidades.DireitoPerfil;
import br.com.juridico.entidades.PerfilAcesso;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by jeffersonl2009_00 on 28/09/2016.
 */

public class PerfilAcessoDtoTest extends AbstractAcessoObjectsTest {

    public static final String SIGLA_CONSULTA = "CON";
    public static final String CONSULTAR = "CONSULTAR";
    public static final String SIGLA_ALTERACAO = "ALT";
    public static final String ALTERAR = "ALTERAR";
    public static final String SIGLA_EXCLUIR = "EXC";
    public static final String EXCLUIR = "EXCLUIR";
    public static final String SIGLA_INCLUIR = "INC";
    public static final String INCLUIR = "INCLUIR";

    @Before
    public void up() {
        MockitoAnnotations.initMocks(this);
        carregaMockAcessos();
        carregaMockAcessosFilhos();
        carregaMockPerfilAcesso();
    }

    @Test
    public void retorna_um_registro_lista_acesso_filho_test(){
        List<Acesso> acessosFilhos = acessoSessionBean.findByAcessoFilho(getAcesso().getId());
        assertEquals(1, acessosFilhos.size());
    }

    @Test
    public void retorna_objeto_nulo_lista_acesso_filho_test(){
        carregaMockAcessosFilhosNulo();
        List<Acesso> acessosFilhos = acessoSessionBean.findByAcessoFilho(getAcesso().getId());
        assertNull(acessosFilhos);
    }

    @Test
    public void passando_perfil_acesso_nullo_para_o_dto_add_acesso_filho_test(){
        List<Acesso> acessosFilhos = acessoSessionBean.findByAcessoFilho(getAcesso().getId());
        PerfilAcesso pa =  perfilAcessoSessionBean.findByPerfilAndAcesso(getPerfil().getId(), getAcesso().getId(), EMPRESA);
        PerfilAcessoDto dto = new PerfilAcessoDto(getAcesso(), pa);
        carregaMockPerfilAcessoNullo();

        for (Acesso acessosFilho : acessosFilhos) {
            PerfilAcesso perfilFilho = perfilAcessoSessionBean.findByPerfilAndAcesso(getPerfil().getId(), acessosFilho.getId(), EMPRESA);
            dto.addAcessosFilhos(perfilFilho, null);
        }

        assertEquals(0, dto.getPerfilSubAcessoDtos().size());
    }

    @Test
    public void passando_perfil_acesso_alterar_para_o_dto_add_acesso_filho_test(){
        DireitoPerfil direitoPerfil = createDireitoPerfil(SIGLA_ALTERACAO, ALTERAR);
        List<Acesso> acessosFilhos = acessoSessionBean.findByAcessoFilho(getAcesso().getId());
        PerfilAcesso pa =  perfilAcessoSessionBean.findByPerfilAndAcesso(getPerfil().getId(), getAcesso().getId(), EMPRESA);
        PerfilAcessoDto dto = new PerfilAcessoDto(getAcesso(), pa);

        for (Acesso acessosFilho : acessosFilhos) {
            PerfilAcesso perfilFilho = perfilAcessoSessionBean.findByPerfilAndAcesso(getPerfil().getId(), acessosFilho.getId(), EMPRESA);
            perfilFilho.addDireitoPerfis(direitoPerfil);
            dto.addAcessosFilhos(perfilFilho, perfilFilho.getDireitoPerfis());
        }

        assertEquals(1, dto.getPerfilSubAcessoDtos().size());
        assertFalse(dto.getPerfilSubAcessoDtos().get(0).isVisualiza());
        assertFalse(dto.getPerfilSubAcessoDtos().get(0).isExclui());
        assertFalse(dto.getPerfilSubAcessoDtos().get(0).isInsere());
    }

    @Test
    public void passando_perfil_acesso_consultar_para_o_dto_add_acesso_filho_test(){
        DireitoPerfil direitoPerfil = createDireitoPerfil(SIGLA_CONSULTA, CONSULTAR);
        List<Acesso> acessosFilhos = acessoSessionBean.findByAcessoFilho(getAcesso().getId());
        PerfilAcesso pa =  perfilAcessoSessionBean.findByPerfilAndAcesso(getPerfil().getId(), getAcesso().getId(), EMPRESA);
        PerfilAcessoDto dto = new PerfilAcessoDto(getAcesso(), pa);

        for (Acesso acessosFilho : acessosFilhos) {
            PerfilAcesso perfilFilho = perfilAcessoSessionBean.findByPerfilAndAcesso(getPerfil().getId(), acessosFilho.getId(), EMPRESA);
            perfilFilho.addDireitoPerfis(direitoPerfil);
            dto.addAcessosFilhos(perfilFilho, perfilFilho.getDireitoPerfis());
        }

        assertEquals(1, dto.getPerfilSubAcessoDtos().size());
        assertTrue(dto.getPerfilSubAcessoDtos().get(0).isVisualiza());
        assertFalse(dto.getPerfilSubAcessoDtos().get(0).isExclui());
        assertFalse(dto.getPerfilSubAcessoDtos().get(0).isInsere());
    }

    @Test
    public void passando_perfil_acesso_excluir_para_o_dto_add_acesso_filho_test(){
        DireitoPerfil direitoPerfil = createDireitoPerfil(SIGLA_EXCLUIR, EXCLUIR);
        List<Acesso> acessosFilhos = acessoSessionBean.findByAcessoFilho(getAcesso().getId());
        PerfilAcesso pa =  perfilAcessoSessionBean.findByPerfilAndAcesso(getPerfil().getId(), getAcesso().getId(), EMPRESA);
        PerfilAcessoDto dto = new PerfilAcessoDto(getAcesso(), pa);

        for (Acesso acessosFilho : acessosFilhos) {
            PerfilAcesso perfilFilho = perfilAcessoSessionBean.findByPerfilAndAcesso(getPerfil().getId(), acessosFilho.getId(), EMPRESA);
            perfilFilho.addDireitoPerfis(direitoPerfil);
            dto.addAcessosFilhos(perfilFilho, perfilFilho.getDireitoPerfis());
        }

        assertEquals(1, dto.getPerfilSubAcessoDtos().size());
        assertFalse(dto.getPerfilSubAcessoDtos().get(0).isVisualiza());
        assertTrue(dto.getPerfilSubAcessoDtos().get(0).isExclui());
        assertFalse(dto.getPerfilSubAcessoDtos().get(0).isInsere());
    }

    @Test
    public void passando_perfil_acesso_incluir_para_o_dto_add_acesso_filho_test(){
        DireitoPerfil direitoPerfil = createDireitoPerfil(SIGLA_INCLUIR, INCLUIR);
        List<Acesso> acessosFilhos = acessoSessionBean.findByAcessoFilho(getAcesso().getId());
        PerfilAcesso pa =  perfilAcessoSessionBean.findByPerfilAndAcesso(getPerfil().getId(), getAcesso().getId(), EMPRESA);
        PerfilAcessoDto dto = new PerfilAcessoDto(getAcesso(), pa);

        for (Acesso acessosFilho : acessosFilhos) {
            PerfilAcesso perfilFilho = perfilAcessoSessionBean.findByPerfilAndAcesso(getPerfil().getId(), acessosFilho.getId(), EMPRESA);
            perfilFilho.addDireitoPerfis(direitoPerfil);
            dto.addAcessosFilhos(perfilFilho, perfilFilho.getDireitoPerfis());
        }

        assertEquals(1, dto.getPerfilSubAcessoDtos().size());
        assertFalse(dto.getPerfilSubAcessoDtos().get(0).isVisualiza());
        assertFalse(dto.getPerfilSubAcessoDtos().get(0).isExclui());
        assertTrue(dto.getPerfilSubAcessoDtos().get(0).isInsere());
    }

    @Test
    public void passando_todos_perfis_acesso_para_o_dto_add_acesso_filho_test(){
        DireitoPerfil direitoPerfilConsulta = createDireitoPerfil(SIGLA_CONSULTA, CONSULTAR);
        DireitoPerfil direitoPerfilExclusao = createDireitoPerfil(SIGLA_EXCLUIR, EXCLUIR);
        DireitoPerfil direitoPerfilInclusao = createDireitoPerfil(SIGLA_INCLUIR, INCLUIR);
        DireitoPerfil direitoPerfilAlteracao = createDireitoPerfil(SIGLA_ALTERACAO, ALTERAR);

        List<Acesso> acessosFilhos = acessoSessionBean.findByAcessoFilho(getAcesso().getId());
        PerfilAcesso pa =  perfilAcessoSessionBean.findByPerfilAndAcesso(getPerfil().getId(), getAcesso().getId(), EMPRESA);
        PerfilAcessoDto dto = new PerfilAcessoDto(getAcesso(), pa);

        for (Acesso acessosFilho : acessosFilhos) {
            PerfilAcesso perfilFilho = perfilAcessoSessionBean.findByPerfilAndAcesso(getPerfil().getId(), acessosFilho.getId(), EMPRESA);
            perfilFilho.addDireitoPerfis(direitoPerfilConsulta);
            perfilFilho.addDireitoPerfis(direitoPerfilExclusao);
            perfilFilho.addDireitoPerfis(direitoPerfilInclusao);
            perfilFilho.addDireitoPerfis(direitoPerfilAlteracao);
            dto.addAcessosFilhos(perfilFilho, perfilFilho.getDireitoPerfis());
        }

        assertEquals(1, dto.getPerfilSubAcessoDtos().size());
        assertTrue(dto.getPerfilSubAcessoDtos().get(0).isVisualiza());
        assertTrue(dto.getPerfilSubAcessoDtos().get(0).isExclui());
        assertTrue(dto.getPerfilSubAcessoDtos().get(0).isInsere());
    }

    private DireitoPerfil createDireitoPerfil(String sigla, String descricao) {
        Direito direito = getDireito(sigla, descricao);
        carregaMockDireitoPerfil(direito);
        return direitoPerfilSessionBean.findByPerfilAndDireito(getPerfil().getId(), direito.getId(), EMPRESA);
    }

}
