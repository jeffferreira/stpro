package br.com.juridico.proxy;

import br.com.juridico.entidades.Login;
import br.com.juridico.entidades.Perfil;
import br.com.juridico.entidades.Pessoa;
import br.com.juridico.util.EncryptUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;

/**
 * Created by jeffersonl2009_00 on 24/10/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class ProxyLoginTest {

    private static final String USUARIO_NAO_CADASTRADO = "Login inválido: Usuário não está cadastrado no sistema.";
    private static final String SENHA_INVALIDA = "Senha Inválida.";
    private static final String PRIMEIRO_ACESSO = "Primeiro Acesso";
    private static final String USUARIO_SEM_PERFIL = "Usuário sem perfil cadastrado.";

    private ProxyLogin proxy;

    @Test
    public void testExecuteLogin_retorna_pessoa_nao_encotrada() throws Exception {
        this.proxy = new ProxyLogin(null, "abs");
        String paginaAcesso = this.proxy.executeLogin();

        assertEquals("", paginaAcesso);
        assertEquals(USUARIO_NAO_CADASTRADO, proxy.getMessage());
        assertTrue(proxy.isLimpaLogin());
        assertTrue(proxy.isLimpaSenha());
    }

    @Test
    public void testExecuteLogin_retorna_senha_invalida() throws Exception {
        byte[] senha = EncryptUtil.execute("senha123");
        Login login = new Login("login_teste", senha, 1L);
        Pessoa pessoa = new Pessoa();
        pessoa.setLogin(login);
        this.proxy = new ProxyLogin(pessoa, "outra_senha");
        String paginaAcesso = this.proxy.executeLogin();

        assertEquals("", paginaAcesso);
        assertEquals(SENHA_INVALIDA, proxy.getMessage());
        assertFalse(proxy.isLimpaLogin());
        assertTrue(proxy.isLimpaSenha());
    }

    @Test
    public void testExecuteLogin_retorna_primeiro_acesso() throws Exception {
        byte[] senha = EncryptUtil.execute("senha");
        Login login = new Login("login_teste", senha, 1L);
        Pessoa pessoa = new Pessoa();
        pessoa.setPerfil(new Perfil("CLIENTE", "Cliente", 1L, Boolean.FALSE));
        pessoa.setLogin(login);
        this.proxy = new ProxyLogin(pessoa, "senha");
        assertEquals(new PrimeiroAcesso().executeLogin(), proxy.executeLogin());
        assertEquals(PRIMEIRO_ACESSO, proxy.getMessage());
        assertFalse(proxy.isLimpaLogin());
        assertFalse(proxy.isLimpaSenha());
    }

    @Test
    public void testExecuteLogin_retorna_pagina_inicial() throws Exception {
        byte[] senha = EncryptUtil.execute("senha");
        Login login = new Login("login_teste", senha, 1L);
        login.setPrimeiroAcesso(false);
        Pessoa pessoa = new Pessoa();
        pessoa.setPerfil(new Perfil("CLIENTE", "Cliente", 1L, Boolean.FALSE));
        pessoa.setLogin(login);
        this.proxy = new ProxyLogin(pessoa, "senha");
        assertEquals(new PaginaInicial().executeLogin(), proxy.executeLogin());
        assertEquals("", proxy.getMessage());
        assertFalse(proxy.isLimpaLogin());
        assertFalse(proxy.isLimpaSenha());
    }

    @Test
    public void testValidaPessaCadastrada_true() throws Exception {
        this.proxy = new ProxyLogin(new Pessoa(), "abc");
        assertEquals("", proxy.executeLogin());
        assertEquals("", proxy.getMessage());
        assertFalse(proxy.isLimpaLogin());
        assertFalse(proxy.isLimpaSenha());
    }

    @Test
    public void testValidaPessaCadastrada_false() throws Exception {
        this.proxy = new ProxyLogin(null, "abs");
        assertEquals("", proxy.executeLogin());
        assertEquals(USUARIO_NAO_CADASTRADO, proxy.getMessage());
        assertTrue(proxy.isLimpaLogin());
        assertTrue(proxy.isLimpaSenha());
    }

    @Test
    public void testValidaSenhaConfere_true() throws Exception {
        byte[] senha = EncryptUtil.execute("senha123");
        Login login = new Login("login_teste", senha, 1L);
        Pessoa pessoa = new Pessoa();
        pessoa.setLogin(login);
        pessoa.setPerfil(new Perfil("CLIENTE", "Cliente", 1L, Boolean.FALSE));
        this.proxy = new ProxyLogin(pessoa, "senha123");
        assertEquals(new PrimeiroAcesso().executeLogin(), proxy.executeLogin());
        assertEquals(PRIMEIRO_ACESSO, proxy.getMessage());
        assertFalse(proxy.isLimpaLogin());
        assertFalse(proxy.isLimpaSenha());
    }

    @Test
    public void testValidaSenhaConfere_false() throws Exception {
        byte[] senha = EncryptUtil.execute("senha123");
        Login login = new Login("login_teste", senha, 1L);
        Pessoa pessoa = new Pessoa();
        pessoa.setLogin(login);
        this.proxy = new ProxyLogin(pessoa, "outra_senha");
        assertEquals("", proxy.executeLogin());
        assertEquals(SENHA_INVALIDA, proxy.getMessage());
        assertFalse(proxy.isLimpaLogin());
        assertTrue(proxy.isLimpaSenha());
    }

    @Test
    public void testGetMessage() throws Exception {
        this.proxy = new ProxyLogin(null, "");
        assertEquals("", this.proxy.getMessage());
    }

    @Test
    public void testIsLimpaLogin() throws Exception {
        this.proxy = new ProxyLogin(null, "");
        assertFalse(this.proxy.isLimpaLogin());
    }

    @Test
    public void testIsLimpaSenha() throws Exception {
        this.proxy = new ProxyLogin(null, "");
        assertFalse(this.proxy.isLimpaSenha());
    }

    @Test
    public void testValidaPerfilVinculado_true() throws Exception {
        Pessoa pessoa = new Pessoa();
        pessoa.setPerfil(new Perfil("CLIENTE", "Cliente", 1L, Boolean.FALSE));
        this.proxy = new ProxyLogin(pessoa, "abc");
        assertEquals("", proxy.getMessage());
    }

    @Test
    public void testValidaPerfilVinculado_false() throws Exception {
        byte[] senha = EncryptUtil.execute("senha123");
        Login login = new Login("login_teste", senha, 1L);
        Pessoa pessoa = new Pessoa();
        pessoa.setLogin(login);
        this.proxy = new ProxyLogin(pessoa, "senha123");
        this.proxy.executeLogin();
        assertEquals(USUARIO_SEM_PERFIL, this.proxy.getMessage());
    }
}