package br.com.juridico.business;

import br.com.juridico.entidades.*;
import br.com.juridico.util.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Calendar;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by jefferson on 25/10/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class AgendaEmailBusinessTest {

    private static Long AGENDA_TITULO = 3L;
    private static Long AGENDA_DATAHORA_INICIOFIM_DE = 4L;
    private static Long AGENDA_DATAHORA_INICIO_FIM_PARA = 5L;
    private static Long AGENDA_STATUS_DE = 6L;
    private static Long AGENDA_STATUS_PARA = 7L;
    private static Long AGENDA_RESPONSAVEL_DE = 8L;
    private static Long AGENDA_RESPONSAVEL_PARA = 9L;

    private static String TITULO = "AGENDA_TITULO";
    private static String DATAHORA_INICIOFIM_DE = "AGENDA_DATAHORA_INICIOFIM_DE";
    private static String DATAHORA_INICIO_FIM_PARA = "AGENDA_DATAHORA_INICIO_FIM_PARA";
    private static String STATUS_DE = "AGENDA_STATUS_DE";
    private static String STATUS_PARA = "AGENDA_STATUS_PARA";
    private static String RESPONSAVEL_DE = "AGENDA_RESPONSAVEL_DE";
    private static String RESPONSAVEL_PARA = "AGENDA_RESPONSAVEL_PARA";

    private User user;

    @Before
    public void up(){
        this.user = new User();
        this.user.setName("Usuario teste");
    }

    @Test
    public void testExecute_tituloAgenda() throws Exception {
        ParametroEmail parametroEmail = new ParametroEmail();
        parametroEmail.setId(AGENDA_TITULO);
        parametroEmail.setChaveSessao(TITULO);

        Evento eventoDe = new Evento();
        eventoDe.setDescricao("Titulo Evento");
        Evento eventoPara = new Evento();
        eventoPara.setDescricao("Titulo Evento");

        assertTrue(!AgendaEmailBusiness.execute(parametroEmail, eventoDe, eventoPara, user).isEmpty());
        assertEquals("Titulo Evento", AgendaEmailBusiness.execute(parametroEmail, eventoDe, eventoPara, user));
    }

    @Test
    public void testExecute_dataHoraInicioFimEventoDe() throws Exception {
        ParametroEmail parametroEmail = new ParametroEmail();
        parametroEmail.setId(AGENDA_DATAHORA_INICIOFIM_DE);
        parametroEmail.setChaveSessao(DATAHORA_INICIOFIM_DE);

        Calendar calendar = Calendar.getInstance();
        Evento eventoDe = new Evento();
        eventoDe.setDataInicio(calendar.getTime());
        eventoDe.setDataFim(calendar.getTime());

        Evento eventoPara = new Evento();

        String dataHoraInicio = DateUtils.formataDataHoraBrasil(eventoDe.getDataInicio());
        String dataHoraFim = DateUtils.formataDataHoraBrasil(eventoDe.getDataFim());
        String result = dataHoraInicio.concat(" até ").concat(dataHoraFim);

        assertTrue(!AgendaEmailBusiness.execute(parametroEmail, eventoDe, eventoPara, user).isEmpty());
        assertEquals(result, AgendaEmailBusiness.execute(parametroEmail, eventoDe, eventoPara, user));
    }

    @Test
    public void testExecute_dataHoraInicioFimEventoPara() throws Exception {
        ParametroEmail parametroEmail = new ParametroEmail();
        parametroEmail.setId(AGENDA_DATAHORA_INICIO_FIM_PARA);
        parametroEmail.setChaveSessao(DATAHORA_INICIO_FIM_PARA);

        Calendar calendar = Calendar.getInstance();
        Evento eventoDe = new Evento();

        Evento eventoPara = new Evento();
        eventoPara.setDataInicio(calendar.getTime());
        eventoPara.setDataFim(calendar.getTime());

        String dataHoraInicio = DateUtils.formataDataHoraBrasil(eventoPara.getDataInicio());
        String dataHoraFim = DateUtils.formataDataHoraBrasil(eventoPara.getDataFim());
        String result = dataHoraInicio.concat(" até ").concat(dataHoraFim);

        assertTrue(!AgendaEmailBusiness.execute(parametroEmail, eventoDe, eventoPara, user).isEmpty());
        assertEquals(result, AgendaEmailBusiness.execute(parametroEmail, eventoDe, eventoPara, user));
    }

    @Test
    public void testExecute_statusEventoDe() throws Exception {
        ParametroEmail parametroEmail = new ParametroEmail();
        parametroEmail.setId(AGENDA_STATUS_DE);
        parametroEmail.setChaveSessao(STATUS_DE);

        Evento eventoDe = new Evento();
        StatusAgenda statusAgenda = new StatusAgenda(1L, "À cumprir", 1L);
        eventoDe.setStatusAgenda(statusAgenda);
        Evento eventoPara = new Evento();

        assertTrue(!AgendaEmailBusiness.execute(parametroEmail, eventoDe, eventoPara, user).isEmpty());
        assertEquals("À cumprir", AgendaEmailBusiness.execute(parametroEmail, eventoDe, eventoPara, user));
    }

    @Test
    public void testExecute_statusEventoPara() throws Exception {
        ParametroEmail parametroEmail = new ParametroEmail();
        parametroEmail.setId(AGENDA_STATUS_PARA);
        parametroEmail.setChaveSessao(STATUS_PARA);

        Evento eventoDe = new Evento();

        StatusAgenda statusAgenda = new StatusAgenda(2L, "Cumprido", 1L);
        Evento eventoPara = new Evento();
        eventoPara.setStatusAgenda(statusAgenda);

        assertTrue(!AgendaEmailBusiness.execute(parametroEmail, eventoDe, eventoPara, user).isEmpty());
        assertEquals("Cumprido", AgendaEmailBusiness.execute(parametroEmail, eventoDe, eventoPara, user));
    }

    @Test
    public void testExecute_responsavelEventoDe() throws Exception {
        ParametroEmail parametroEmail = new ParametroEmail();
        parametroEmail.setId(AGENDA_RESPONSAVEL_DE);
        parametroEmail.setChaveSessao(RESPONSAVEL_DE);

        Evento eventoDe = new Evento();
        eventoDe.setPessoa(new Pessoa(1L, "Joao", 'F', true));
        Evento eventoPara = new Evento();

        assertTrue(!AgendaEmailBusiness.execute(parametroEmail, eventoDe, eventoPara, user).isEmpty());
        assertEquals("Joao", AgendaEmailBusiness.execute(parametroEmail, eventoDe, eventoPara, user));
    }

    @Test
    public void testExecute_responsavelEventoPara() throws Exception {
        ParametroEmail parametroEmail = new ParametroEmail();
        parametroEmail.setId(AGENDA_RESPONSAVEL_PARA);
        parametroEmail.setChaveSessao(RESPONSAVEL_PARA);

        Evento eventoDe = new Evento();

        Evento eventoPara = new Evento();
        eventoPara.setPessoa(new Pessoa(1L, "Marcos", 'F', true));

        assertTrue(!AgendaEmailBusiness.execute(parametroEmail, eventoDe, eventoPara, user).isEmpty());
        assertEquals("Marcos", AgendaEmailBusiness.execute(parametroEmail, eventoDe, eventoPara, user));
    }
}