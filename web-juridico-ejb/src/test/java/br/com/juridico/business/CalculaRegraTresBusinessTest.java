package br.com.juridico.business;

import br.com.juridico.business.CalculaRegraTresBusiness;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by jeffersonl2009_00 on 24/10/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class CalculaRegraTresBusinessTest {


    @Test
    public void testCalcula_resultado_esperado() throws Exception {
        Double valorEsperado = 100d;
        assertEquals(valorEsperado, CalculaRegraTresBusiness.calcula(100, 100));
    }

    @Test
    public void testCalcula_resultado_nao_esperado() throws Exception {
        Double valorEsperado = 100d;
        assertNotEquals(valorEsperado, CalculaRegraTresBusiness.calcula(80, 100));
    }

    @Test
    public void testCalcula_passando_zerados() throws Exception {
        Double valorEsperado = 0d;
        assertNotEquals(valorEsperado, CalculaRegraTresBusiness.calcula(0, 0));
    }
}