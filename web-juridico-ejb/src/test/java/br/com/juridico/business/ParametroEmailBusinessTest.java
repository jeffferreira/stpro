package br.com.juridico.business;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Created by jeffersonl2009_00 on 27/09/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class ParametroEmailBusinessTest {

    @Before
    public void up() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void retorna_um_parametro_do_texto_test(){
        String texto = "Olá ${parametro.nome}, este email é automático. Não responder...";
        assertEquals(1, ParametroEmailBusiness.retornaParametrosDoTexto(texto).length);
    }

    @Test
    public void retorna_dois_parametros_do_texto_test(){
        String texto = "Olá ${parametro.nome}, este email é automático. Não responder... ${parametro.data}";
        assertEquals(2, ParametroEmailBusiness.retornaParametrosDoTexto(texto).length);
    }

    @Test
    public void retorna_tres_parametros_do_texto_test(){
        String texto = "Olá ${parametro.nome} ${parametro.sobrenome}, este email é automático. Não responder... ${parametro.data}";
        assertEquals(3, ParametroEmailBusiness.retornaParametrosDoTexto(texto).length);
    }

    @Test
    public void nao_retorna_parametros_do_texto_test(){
        String texto = "Olá este email é automático. Não responder...";
        assertNull(ParametroEmailBusiness.retornaParametrosDoTexto(texto));
    }

    @Test
    public void texto_com_cifrao_test(){
        String texto = "Olá este email é automático...o valor de R$. Não responder...";
        assertNull(ParametroEmailBusiness.retornaParametrosDoTexto(texto));
    }

    @Test
    public void passando_texto_nulo_test(){
        assertNull(ParametroEmailBusiness.retornaParametrosDoTexto(null));
    }

    @Test
    public void parametro_comeca_com_cifrao_test(){
        String texto = "Olá ${parametro.nome} este email é automático...o valor de R$. Não responder...";
        String primeiroCaractere = ParametroEmailBusiness.retornaParametrosDoTexto(texto)[0].substring(0,1);
        assertEquals("$", primeiroCaractere);
    }

    @Test
    public void parametro_termina_com_chave_test(){
        String texto = "Olá ${parametro.nome} este email é automático...o valor de R$. Não responder...";
        String retorno = ParametroEmailBusiness.retornaParametrosDoTexto(texto)[0];
        String ultimoCaractere = retorno.substring(retorno.length()-1);
        assertEquals("}", ultimoCaractere);
    }

}
