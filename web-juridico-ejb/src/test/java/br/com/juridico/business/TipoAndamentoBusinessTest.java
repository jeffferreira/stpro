package br.com.juridico.business;

import br.com.juridico.entidades.TipoAndamento;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;

/**
 * Created by jeffersonl2009_00 on 06/07/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class TipoAndamentoBusinessTest {

    private static final Long AUDIENCIA_CONCILIACAO = 1L;
    private static final Long AUDIENCIA_OITIVA = 2L;
    private static final Long AUDIENCIA_INSTRUCAO = 3L;


    @Before
    public void up() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void eh_audiencia_conciliacao_test(){
        TipoAndamento tipoAndamento = new TipoAndamento();
        tipoAndamento.setId(AUDIENCIA_CONCILIACAO);
        boolean result = TipoAndamentoBusiness.isAudienciaConciliacao(tipoAndamento);
        assertEquals(true, result);
    }

    @Test
    public void eh_audiencia_oitiva_test() {
        TipoAndamento tipoAndamento = new TipoAndamento();
        tipoAndamento.setId(AUDIENCIA_OITIVA);
        boolean result = TipoAndamentoBusiness.isAudienciaOitiva(tipoAndamento);
        assertEquals(true, result);
    }

    @Test
    public void eh_audiencia_instrucao_test() {
        TipoAndamento tipoAndamento = new TipoAndamento();
        tipoAndamento.setId(AUDIENCIA_INSTRUCAO);
        boolean result = TipoAndamentoBusiness.isAudienciaInstrucao(tipoAndamento);
        assertEquals(true, result);
    }
}
