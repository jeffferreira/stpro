package br.com.juridico.business;

import br.com.juridico.entidades.Login;
import br.com.juridico.entidades.ParametroEmail;
import br.com.juridico.entidades.Pessoa;
import br.com.juridico.entidades.User;
import br.com.juridico.util.EncryptUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;

/**
 * Created by jefferson on 25/10/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class SenhaEmailBusinessTest {

    private static Long SENHA_DE = 10L;
    private static Long SENHA_PARA = 11L;
    private static Long SENHA_NOME_USUARIO = 12L;
    private static String DE = "SENHA_DE";
    private static String PARA = "SENHA_PARA";
    private static String NOME_USUARIO = "SENHA_NOME_USUARIO";
    private static String AGENDA_TITULO = "AGENDA_TITULO";

    private User user;

    @Before
    public void up(){
        this.user = new User();
        this.user.setName("Usuario teste");
    }

    @Test
    public void testExecute_senha_outro_parametro() throws Exception {
        ParametroEmail parametroEmail = new ParametroEmail();
        parametroEmail.setChaveSessao(AGENDA_TITULO);
        parametroEmail.setId(3L);
        assertTrue(SenhaEmailBusiness.execute(parametroEmail, new Pessoa(), new Pessoa(), user).isEmpty());
    }

    @Test
    public void testExecute_senha_usuario() throws Exception {
        ParametroEmail parametroEmail = new ParametroEmail();
        parametroEmail.setChaveSessao(DE);
        parametroEmail.setId(SENHA_DE);

        Pessoa pessoaDe = new Pessoa(1L, "Joao", 'F', true);
        byte[] senha = EncryptUtil.execute("asdf");
        Login login = new Login("login", senha, 1L);
        pessoaDe.setLogin(login);

        assertFalse(SenhaEmailBusiness.execute(parametroEmail, pessoaDe, new Pessoa(), user).isEmpty());
        assertEquals("asdf", SenhaEmailBusiness.execute(parametroEmail, pessoaDe, new Pessoa(), user));
    }

    @Test
    public void testExecute_nova_senha_usuario() throws Exception {
        ParametroEmail parametroEmail = new ParametroEmail();
        parametroEmail.setChaveSessao(PARA);
        parametroEmail.setId(SENHA_PARA);

        byte[] senha = EncryptUtil.execute("novasenha");
        Login login = new Login("login", senha, 1L);
        Pessoa pessoaPara = new Pessoa(1L, "Joao", 'F', true);
        pessoaPara.setLogin(login);

        assertFalse(SenhaEmailBusiness.execute(parametroEmail, new Pessoa(), pessoaPara, user).isEmpty());
        assertEquals("novasenha", SenhaEmailBusiness.execute(parametroEmail, new Pessoa(), pessoaPara, user));
    }

    @Test
    public void testExecute_nome_usuario() throws Exception {
        ParametroEmail parametroEmail = new ParametroEmail();
        parametroEmail.setChaveSessao(NOME_USUARIO);
        parametroEmail.setId(SENHA_NOME_USUARIO);

        Pessoa pessoaDe = new Pessoa(1L, "Joao", 'F', true);
        assertFalse(SenhaEmailBusiness.execute(parametroEmail, pessoaDe, new Pessoa(), user).isEmpty());
        assertEquals("Joao", SenhaEmailBusiness.execute(parametroEmail, pessoaDe, new Pessoa(), user));
    }
}