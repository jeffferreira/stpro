package br.com.juridico.business;

import br.com.juridico.entidades.ParametroEmail;
import br.com.juridico.entidades.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by jefferson on 25/10/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class GeralEmailBusinessTest {

    private static String GERAL_NOME_RESPONSAVEL = "GERAL_NOME_RESPONSAVEL";
    private static String GERAL_DATAHORA_ALTERACAO = "GERAL_DATAHORA_ALTERACAO";
    private static String AGENDA_TITULO = "AGENDA_TITULO";

    @Test
    public void testExecute_data_alteracao() {
        User user = new User(1L, "Joao do teste");
        ParametroEmail parametroEmail = new ParametroEmail();
        parametroEmail.setId(2L);
        parametroEmail.setChaveSessao(GERAL_DATAHORA_ALTERACAO);
        assertTrue(!GeralEmailBusiness.execute(parametroEmail, user).isEmpty());
    }

    @Test
    public void testExecute_data_alteracao_outro_parametro() {
        ParametroEmail parametroEmail = new ParametroEmail();
        parametroEmail.setChaveSessao(AGENDA_TITULO);
        parametroEmail.setId(3L);
        User user = new User(1L, "Joao do teste");
        assertTrue(GeralEmailBusiness.execute(parametroEmail, user).isEmpty());
    }

    @Test
    public void testExecute_nome_usuario() {
        ParametroEmail parametroEmail = new ParametroEmail();
        parametroEmail.setId(1L);
        parametroEmail.setChaveSessao(GERAL_NOME_RESPONSAVEL);
        User user = new User(1L, "Joao do teste");
        assertTrue(!GeralEmailBusiness.execute(parametroEmail, user).isEmpty());
        assertEquals("Joao do teste", GeralEmailBusiness.execute(parametroEmail, user));
    }

}