package br.com.juridico.util;

import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertEquals;

public class DateUtilsTest {

    private static final Integer JANEIRO = 0;
    private static final Integer MAIO = 4;

    @Test
    public void lowerLimit_test() throws Exception {
        Date quinzeDeMaio = getData(15, MAIO, 2017);
        Date expected = getDataHora(quinzeDeMaio, 0, 0, 0);
        assertEquals(expected, DateUtils.lowerLimit(quinzeDeMaio));
    }


    /**
     *
     * @param dia
     * @param mes
     * @param ano
     * @return
     */
    private Date getData(int dia, int mes, int ano) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, dia);
        calendar.set(Calendar.MONTH, mes);
        calendar.set(Calendar.YEAR, ano);
        return calendar.getTime();
    }

    private Date getDataHora(Date data, int hora, int minuto, int segundo) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(data);
        calendar.set(Calendar.HOUR_OF_DAY, hora);
        calendar.set(Calendar.MINUTE, minuto);
        calendar.set(Calendar.SECOND, segundo);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

}