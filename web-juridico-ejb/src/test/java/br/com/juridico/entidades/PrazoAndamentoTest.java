package br.com.juridico.entidades;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Calendar;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by jefferson on 20/11/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class PrazoAndamentoTest {

    private PrazoAndamento prazoAndamento;

    @Test
    public void testIsPrazoAtrasado_true() throws Exception {
        this.prazoAndamento = new PrazoAndamento();
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);
        prazoAndamento.setDataFinalPrazo(calendar.getTime());
        StatusAgenda statusAgenda = new StatusAgenda(2L, "À cumprir", 1L);
        prazoAndamento.setStatusAgenda(statusAgenda);
        assertTrue(prazoAndamento.isPrazoAtrasado());
    }

    @Test
    public void testIsPrazoAtrasado_prazo_foi_atendido() throws Exception {
        this.prazoAndamento = new PrazoAndamento();
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);
        prazoAndamento.setDataFinalPrazo(calendar.getTime());
        StatusAgenda statusAgenda = new StatusAgenda(1L, "Cumprido", 1L);
        prazoAndamento.setStatusAgenda(statusAgenda);
        assertFalse(prazoAndamento.isPrazoAtrasado());
    }

    @Test
    public void testIsPrazoAtrasado_false() throws Exception {
        this.prazoAndamento = new PrazoAndamento();
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 1);
        prazoAndamento.setDataFinalPrazo(calendar.getTime());
        StatusAgenda statusAgenda = new StatusAgenda(2L, "À cumprir", 1L);
        prazoAndamento.setStatusAgenda(statusAgenda);
        assertFalse(prazoAndamento.isPrazoAtrasado());
    }
}
