package br.com.juridico.entidades;

import br.com.juridico.util.DateUtils;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by jefferson on 19/03/2017.
 */
public class AndamentoTest {

    private Andamento andamento;

    @Before
    public void up(){
        this.andamento = new Andamento();
    }

    @Test
    public void isTodosEventosEstaoPendentes_eventos_vazios_test() throws Exception {
        assertFalse(this.andamento.isTodosEventosEstaoPendentes());
    }

    @Test
    public void testIsTodosEventosEstaoPendentes_true_test() throws Exception {
        Evento evento1 = new Evento("", "Evento1", new StatusAgenda(1L, "À cumprir", 1l), new Date(), new Date(), "", new Pessoa(1L), new Empresa());
        evento1.setControladoriaStatus("P");

        Evento evento2 = new Evento("", "Evento1", new StatusAgenda(1L, "À cumprir", 1l), new Date(), new Date(), "", new Pessoa(1L), new Empresa());
        evento2.setControladoriaStatus("P");

        this.andamento.getEventos().add(evento1);
        this.andamento.getEventos().add(evento2);

        assertTrue(this.andamento.isTodosEventosEstaoPendentes());
    }

    @Test
    public void testIsTodosEventosEstaoPendentes_false_test() throws Exception {
        Evento evento1 = new Evento("", "Evento1", new StatusAgenda(1L, "À cumprir", 1l), new Date(), new Date(), "", new Pessoa(1L), new Empresa());
        evento1.setControladoriaStatus("P");

        Evento evento2 = new Evento("", "Evento1", new StatusAgenda(1L, "À cumprir", 1l), new Date(), new Date(), "", new Pessoa(1L), new Empresa());
        evento2.setControladoriaStatus("A");

        this.andamento.getEventos().add(evento1);
        this.andamento.getEventos().add(evento2);

        assertFalse(this.andamento.isTodosEventosEstaoPendentes());

    }

    @Test
    public void testIsPossuiEventoPendente_eventos_vazio_test() throws Exception {
        assertFalse(this.andamento.isPossuiEventoPendente());
    }

    @Test
    public void testIsPossuiEventoPendente_true_test() throws Exception {
        Evento evento1 = new Evento("", "Evento1", new StatusAgenda(1L, "À cumprir", 1l), new Date(), new Date(), "", new Pessoa(1L), new Empresa());
        evento1.setControladoriaStatus("P");

        Evento evento2 = new Evento("", "Evento1", new StatusAgenda(1L, "À cumprir", 1l), new Date(), new Date(), "", new Pessoa(1L), new Empresa());
        evento2.setControladoriaStatus("A");

        this.andamento.getEventos().add(evento1);
        this.andamento.getEventos().add(evento2);

        assertTrue(this.andamento.isPossuiEventoPendente());
    }

    @Test
    public void testIsPossuiEventoPendente_false_test() throws Exception {
        Evento evento1 = new Evento("", "Evento1", new StatusAgenda(1L, "À cumprir", 1l), new Date(), new Date(), "", new Pessoa(1L), new Empresa());
        evento1.setControladoriaStatus("A");

        Evento evento2 = new Evento("", "Evento1", new StatusAgenda(1L, "À cumprir", 1l), new Date(), new Date(), "", new Pessoa(1L), new Empresa());
        evento2.setControladoriaStatus("A");

        this.andamento.getEventos().add(evento1);
        this.andamento.getEventos().add(evento2);

        assertFalse(this.andamento.isPossuiEventoPendente());
    }

    @Test
    public void testIsPrazoAtrasado_true_test() throws Exception {
        Date dataAtrasada = DateUtils.geraDataUltimoDiaMes(1, 2016);
        this.andamento.setDataFinalPrazoFatal(dataAtrasada);
        StatusAgenda statusAgenda = new StatusAgenda(1L, "À cumprir", 1L);
        this.andamento.setStatusAgenda(statusAgenda);
        assertTrue(this.andamento.isPrazoAtrasado());
    }

    @Test
    public void testIsPrazoAtrasado_false_test() throws Exception {
        Date dataCincoDiasUteisPosterior = DateUtils.addUtilsDays(5, new Date());
        this.andamento.setDataFinalPrazoFatal(dataCincoDiasUteisPosterior);
        StatusAgenda statusAgenda = new StatusAgenda(1L, "À cumprir", 1L);
        this.andamento.setStatusAgenda(statusAgenda);
        assertFalse(this.andamento.isPrazoAtrasado());
    }

    @Test
    public void testIsPrazoAtrasado_cumprido_atrasado_false_test() throws Exception {
        Date dataAtrasada = DateUtils.geraDataUltimoDiaMes(1, 2016);
        this.andamento.setDataFinalPrazoFatal(dataAtrasada);
        StatusAgenda statusAgenda = new StatusAgenda(1L, "Cumprido", 1L);
        this.andamento.setStatusAgenda(statusAgenda);
        assertFalse(this.andamento.isPrazoAtrasado());
    }

    @Test
    public void testIsPrazoAtrasado_cancelado_atrasado_false_test() throws Exception {
        Date dataAtrasada = DateUtils.geraDataUltimoDiaMes(1, 2016);
        this.andamento.setDataFinalPrazoFatal(dataAtrasada);
        StatusAgenda statusAgenda = new StatusAgenda(1L, "Cancelado", 1L);
        this.andamento.setStatusAgenda(statusAgenda);
        assertFalse(this.andamento.isPrazoAtrasado());
    }
}