package br.com.juridico.entidades;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;

/**
 * Created by jeffersonl2009_00 on 13/10/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class PessoaJuridicaTest {

    private PessoaJuridica pessoaJuridica;

    @Before
    public void up() {
        MockitoAnnotations.initMocks(this);
        this.pessoaJuridica = new PessoaJuridica();
    }

    @Test
    public void verifica_descricao_emails_quando_nulo(){
        assertTrue(this.pessoaJuridica.getEmails().isEmpty());
    }

    @Test
    public void verifica_descricao_telefones_quando_nulo(){
        assertTrue(this.pessoaJuridica.getTelefones().isEmpty());
    }

    @Test
    public void verifica_descricao_responsaveis_quando_nulo(){
        assertTrue(this.pessoaJuridica.getResponsaveis().isEmpty());
    }

    @Test
    public void passando_nulo_para_add_responsaveis(){
        this.pessoaJuridica.addPessoasJuridicasResponsaveis(null);
        assertTrue(this.pessoaJuridica.getPessoasJuridicasResponsaveis().isEmpty());
    }

    @Test
    public void verifica_descricao_um_email_emails(){
        Pessoa pessoa1 = new Pessoa(1L, "Joao do Teste", 'F', true);
        PessoaEmail pessoaEmail = new PessoaEmail(pessoa1, "teste@gmail.com", 1L);
        pessoa1.addPessoasEmails(pessoaEmail);
        this.pessoaJuridica.setPessoa(pessoa1);

        assertFalse(this.pessoaJuridica.getEmails().isEmpty());
        assertEquals("teste@gmail.com;", this.pessoaJuridica.getEmails());

    }

    @Test
    public void verifica_descricao_dois_email_emails(){
        Pessoa pessoa1 = new Pessoa(1L, "Joao do Teste", 'F', true);
        PessoaEmail pessoaEmail1 = new PessoaEmail(pessoa1, "teste@gmail.com", 1L);
        PessoaEmail pessoaEmail2 = new PessoaEmail(pessoa1, "marcao@gmail.com", 1L);
        pessoa1.addPessoasEmails(pessoaEmail1);
        pessoa1.addPessoasEmails(pessoaEmail2);
        this.pessoaJuridica.setPessoa(pessoa1);

        assertFalse(this.pessoaJuridica.getEmails().isEmpty());
        assertEquals(2, this.pessoaJuridica.getPessoa().getPessoasEmails().size());
        assertEquals("teste@gmail.com; marcao@gmail.com;", this.pessoaJuridica.getEmails());

    }

    @Test
    public void verifica_descricao_telefones(){
        Pessoa pessoa1 = new Pessoa(1L, "Joao do Teste", 'F', true);
        Contato contato = new Contato(44, "91281273");
        PessoaContato pessoaContato = new PessoaContato(contato, pessoa1, 1L);
        pessoa1.addPessoasContatos(pessoaContato);
        this.pessoaJuridica.setPessoa(pessoa1);

        assertFalse(this.pessoaJuridica.getTelefones().isEmpty());
        assertEquals("(44) 9128-1273;", this.pessoaJuridica.getTelefones());
    }

    @Test
    public void verifica_descricao_dois_telefones(){
        Pessoa pessoa1 = new Pessoa(1L, "Joao do Teste", 'F', true);
        Contato contato1 = new Contato(44, "91281273");
        Contato contato2 = new Contato(11, "960671273");
        PessoaContato pessoaContato1 = new PessoaContato(contato1, pessoa1, 1L);
        PessoaContato pessoaContato2 = new PessoaContato(contato2, pessoa1, 1L);
        pessoa1.addPessoasContatos(pessoaContato1);
        pessoa1.addPessoasContatos(pessoaContato2);
        this.pessoaJuridica.setPessoa(pessoa1);

        assertFalse(this.pessoaJuridica.getTelefones().isEmpty());
        assertEquals(2, this.pessoaJuridica.getPessoa().getPessoasContatos().size());
        assertEquals("(44) 9128-1273; (11) 96067-1273;", this.pessoaJuridica.getTelefones());
    }

    @Test
    public void verifica_descricao_responsaveis(){
        Pessoa pessoaEmpresa = new Pessoa(1L, "Copel Telecom", 'J', true);
        this.pessoaJuridica.setPessoa(pessoaEmpresa);

        Pessoa pessoa1 = new Pessoa(1L, "Joao do Teste", 'F', true);
        PessoaFisica pessoaFisica = new PessoaFisica();
        pessoaFisica.setPessoa(pessoa1);

        PessoaJuridicaResponsavel responsavel = new PessoaJuridicaResponsavel(this.pessoaJuridica, pessoaFisica, 1L);
        this.pessoaJuridica.addPessoasJuridicasResponsaveis(responsavel);

        assertFalse(this.pessoaJuridica.getResponsaveis().isEmpty());
        assertEquals("Joao do Teste;", this.pessoaJuridica.getResponsaveis());
    }

}
