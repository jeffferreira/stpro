package br.com.juridico.entidades;

import br.com.juridico.enums.TipoEndereco;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Created by jeffersonl2009_00 on 11/10/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class EnderecoTest {

    private Endereco endereco;

    @Before
    public void up() {
        MockitoAnnotations.initMocks(this);
        this.endereco = new Endereco();
    }

    @Test
    public void valida_descricao_tipo_endereco_residencial_test(){
        String tipoEndereco = TipoEndereco.RESIDENCIAL.getValue();
        endereco.setTipoEndereco(tipoEndereco);
        Assert.assertEquals(TipoEndereco.RESIDENCIAL.getDescricao(), this.endereco.getTipoEnderecoDescricao());
    }

    @Test
    public void valida_descricao_tipo_endereco_comercial_test(){
        String tipoEndereco = TipoEndereco.COMERCIAL.getValue();
        endereco.setTipoEndereco(tipoEndereco);
        Assert.assertEquals(TipoEndereco.COMERCIAL.getDescricao(), this.endereco.getTipoEnderecoDescricao());
    }

    @Test
    public void valida_descricao_tipo_endereco_nulo_test(){
        endereco.setTipoEndereco(null);
        Assert.assertEquals(TipoEndereco.COMERCIAL.getDescricao(), this.endereco.getTipoEnderecoDescricao());
    }

}
