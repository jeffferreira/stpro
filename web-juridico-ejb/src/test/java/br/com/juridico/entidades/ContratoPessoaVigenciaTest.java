package br.com.juridico.entidades;

import br.com.juridico.util.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by jeffersonl2009_00 on 11/10/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class ContratoPessoaVigenciaTest extends AbstractEntityTest {

    private ContratoPessoaVigencia contratoPessoaVigencia;

    @Before
    public void up() {
        MockitoAnnotations.initMocks(this);
        this.contratoPessoaVigencia = new ContratoPessoaVigencia();
    }

    @Test
    public void datas_em_vigencia_test(){
        contratoPessoaVigencia.setDataInicio(DateUtils.lowerLimit(new Date()));
        contratoPessoaVigencia.setDataFim(DateUtils.upperLimit(new Date()));
        assertTrue(contratoPessoaVigencia.isContratoVigente());
    }

    @Test
    public void datas_fora_de_vigencia_test(){
        int mes = 1;
        int ano = 2009;
        contratoPessoaVigencia.setDataInicio(DateUtils.lowerLimit(DateUtils.geraPrimeiroDiaDoMes(mes, ano)));
        contratoPessoaVigencia.setDataFim(DateUtils.upperLimit(DateUtils.geraPrimeiroDiaDoMes(mes, ano)));
        assertFalse(contratoPessoaVigencia.isContratoVigente());
    }

    @Test
    public void adicionando_05_linhas_nos_andamentos_test(){
        int i = 1;
        contratoPessoaVigencia.addAndamentosAtos(new ContratoPessoaTipoAndamento());
        contratoPessoaVigencia.addAndamentosAtos(new ContratoPessoaTipoAndamento());
        contratoPessoaVigencia.addAndamentosAtos(new ContratoPessoaTipoAndamento());
        contratoPessoaVigencia.addAndamentosAtos(new ContratoPessoaTipoAndamento());
        contratoPessoaVigencia.addAndamentosAtos(new ContratoPessoaTipoAndamento());
        contratoPessoaVigencia.addRowNumberAndamentos();

        for (ContratoPessoaTipoAndamento andamento : contratoPessoaVigencia.getAndamentoAtos()) {
            assertEquals(i, andamento.getRowNumber());
            i++;
        }

    }
}
