package br.com.juridico.entidades;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.junit.Assert.*;


/**
 * Created by jeffersonl2009_00 on 14/10/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class ProcessoTest {

    private Processo processo;

    @Before
    public void up() {
        MockitoAnnotations.initMocks(this);
        this.processo = new Processo();
    }

    @Test
    public void verifica_retorno_descricao_cliente_sem_existir_clientes_test(){
        assertEquals("", this.processo.getCliente());
    }


    @Test
    public void valida_total_valor_provavel_zero_test(){
        assertEquals(BigDecimal.ZERO, this.processo.getTotalValorProvavel());
    }

    @Test
    public void valida_total_valor_pedido_zero_test(){
        assertEquals(BigDecimal.ZERO, this.processo.getTotalValorPedido());
    }

    @Test
    public void valida_honorario_contrato_null(){
        assertFalse(this.processo.isHonorarioContrato());
    }

    @Test
    public void valida_honorario_flag_contrato_false(){
        ProcessoHonorario honorario = new ProcessoHonorario();
        honorario.setFlagContrato(false);
        this.processo.setProcessoHonorario(honorario);
        Assert.assertFalse(this.processo.isHonorarioContrato());
    }

    @Test
    public void valida_honorario_flag_contrato_true(){
        ProcessoHonorario honorario = new ProcessoHonorario();
        honorario.setFlagContrato(true);
        this.processo.setProcessoHonorario(honorario);
        assertTrue(this.processo.isHonorarioContrato());
    }

    @Test
    public void valida_honorario_valor_null(){
        assertFalse(this.processo.isHonorarioValor());
    }

    @Test
    public void valida_honorario_flag_valor_false(){
        ProcessoHonorario honorario = new ProcessoHonorario();
        honorario.setFlagValor(false);
        this.processo.setProcessoHonorario(honorario);
        assertFalse(this.processo.isHonorarioValor());
    }

    @Test
    public void valida_honorario_flag_valor_true(){
        ProcessoHonorario honorario = new ProcessoHonorario();
        honorario.setFlagValor(true);
        this.processo.setProcessoHonorario(honorario);
        assertTrue(this.processo.isHonorarioValor());
    }

    @Test
    public void valida_honorario_percentual_null(){
        assertFalse(this.processo.isHonorarioPercentual());
    }

    @Test
    public void valida_honorario_flag_percentual_false(){
        ProcessoHonorario honorario = new ProcessoHonorario();
        honorario.setFlagPercentual(false);
        this.processo.setProcessoHonorario(honorario);
        assertFalse(this.processo.isHonorarioPercentual());
    }

    @Test
    public void valida_honorario_flag_percentual_true(){
        ProcessoHonorario honorario = new ProcessoHonorario();
        honorario.setFlagPercentual(true);
        this.processo.setProcessoHonorario(honorario);
        assertTrue(this.processo.isHonorarioPercentual());
    }

    @Test
    public void testIsProvavelNecessidadeImpugnacao_false() throws Exception {
        assertFalse(processo.isProvavelNecessidadeImpugnacao());
    }

    @Test
    public void testIsProvavelNecessidadeImpugnacao_true() throws Exception {
        Parte parte = new Parte();
        parte.setPosicao(new Posicao(1L, "Réu"));
        parte.setClienteAtoProcesso(Boolean.TRUE);
        processo.addParte(parte);
        processo.addPedido(new ProcessoPedido(new Pedido(), processo, new CausaPedido(), new BigDecimal(100), 1L));
        processo.setValorCausa(new BigDecimal(50));
        assertTrue(processo.isProvavelNecessidadeImpugnacao());
    }

    @Test
    public void testExisteMaisDeUmaParte_cliente_cadastrada_true(){
        Pessoa pessoa1 = new Pessoa(1L, "Joao do teste", 'F', true);
        pessoa1.setCliente(true);
        Pessoa pessoa2 = new Pessoa(1L, "Marcao do teste", 'F', true);
        pessoa2.setCliente(true);
        processo.addParte(new Parte(processo, pessoa1, new Posicao(), new Grupo(), 1L));
        processo.addParte(new Parte(processo, pessoa2, new Posicao(), new Grupo(), 1L));
        assertTrue(processo.isExistePartesConcorrentes());
    }

    @Test
    public void testExisteMaisDeUmaParte_cliente_cadastrada_false(){
        assertFalse(processo.isExistePartesConcorrentes());
    }
}
