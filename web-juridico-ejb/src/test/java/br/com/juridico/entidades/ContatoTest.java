package br.com.juridico.entidades;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;

/**
 * Created by jeffersonl2009_00 on 13/10/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class ContatoTest {

    private Contato contato8;
    private Contato contato9;

    @Before
    public void up() {
        MockitoAnnotations.initMocks(this);
        this.contato8 =  new Contato(44, "96241287");
        this.contato9 = new Contato(11, "960671273");
    }

    @Test
    public void descricao_telefone_passando_nulo_test(){
        Contato contato = new Contato();
        assertTrue(contato.getTelefoneFormatado().isEmpty());
    }

    @Test
    public void descricao_ddd_telefone_passando_ddd_nulo_test(){
        Contato contato = new Contato(null, "12345678");
        assertTrue(contato.getDDDTelefoneFormatado().isEmpty());
    }

    @Test
    public void descricao_ddd_telefone_passando_numero_nulo_test(){
        Contato contato = new Contato(44, null);
        assertTrue(contato.getDDDTelefoneFormatado().isEmpty());
    }

    @Test
    public void verifica_mascara_oito_digitos_test(){
        assertFalse(contato8.getTelefoneFormatado().isEmpty());
        assertEquals("9624-1287", contato8.getTelefoneFormatado());
    }

    @Test
    public void verifica_mascara_ddd_telefone_oito_digitos_test(){
        assertFalse(contato8.getDDDTelefoneFormatado().isEmpty());
        assertEquals("(44) 9624-1287", contato8.getDDDTelefoneFormatado());
    }

    @Test
    public void verifica_mascara_nove_digitos_test(){
        assertFalse(contato9.getTelefoneFormatado().isEmpty());
        assertEquals("96067-1273", contato9.getTelefoneFormatado());
    }

    @Test
    public void verifica_mascara_ddd_telefone_nove_digitos_test(){
        assertFalse(contato9.getDDDTelefoneFormatado().isEmpty());
        assertEquals("(11) 96067-1273", contato9.getDDDTelefoneFormatado());
    }

}
