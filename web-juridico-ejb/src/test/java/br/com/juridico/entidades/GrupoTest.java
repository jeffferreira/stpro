package br.com.juridico.entidades;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Date;

import static junit.framework.Assert.assertEquals;

/**
 * Created by jeffersonl2009_00 on 13/10/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class GrupoTest {

    private Grupo grupo;

    @Before
    public void up() {
        MockitoAnnotations.initMocks(this);
        this.grupo = new Grupo(1L, "santiago e tourinho advogados");
    }

    @Test
    public void adiciona_grupo_a_lista_de_grupos(){
        Pessoa pessoa1 = new Pessoa(1L, "Joao do Teste", 'F', true);
        PessoaFisica pessoaFisica1 = new PessoaFisica(1L, "329.767.238-26", 'M', new Date());
        pessoaFisica1.setPessoa(pessoa1);
        GrupoAdvogado grupoAdvogado1 = new GrupoAdvogado(this.grupo, pessoaFisica1, 1L);

        this.grupo.addGruposAdvogados(grupoAdvogado1);

        assertEquals(1, this.grupo.getGruposAdvogados().size());
    }

    @Test
    public void adiciona_grupo_passando_nulo_aos_grupos(){
        this.grupo.addGruposAdvogados(null);
        assertEquals(0, this.grupo.getGruposAdvogados().size());
    }
}
