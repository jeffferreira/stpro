package br.com.juridico.entidades;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Created by jefferson on 18/10/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class StatusAgendaTest {

    @Before
    public void up() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testIsStatusUtilizadoSistema() throws Exception {
        StatusAgenda statusAgendaCumprido = new StatusAgenda(1L, "Cumprido", 1L);
        StatusAgenda statusAgendaACumprir = new StatusAgenda(2L, "À cumprir", 1L);
        StatusAgenda statusAgendaCancelado = new StatusAgenda(3L, "Cancelado", 1L);

        Assert.assertTrue(statusAgendaCumprido.isStatusUtilizadoSistema());
        Assert.assertTrue(statusAgendaACumprir.isStatusUtilizadoSistema());
        Assert.assertTrue(statusAgendaCancelado.isStatusUtilizadoSistema());
    }

    @Test
    public void testIsNotStatusUtilizadoSistema() throws Exception {
        StatusAgenda statusAgendaAdiado = new StatusAgenda(4L, "Adiado", 1L);
        Assert.assertFalse(statusAgendaAdiado.isStatusUtilizadoSistema());
    }

    @Test
    public void testPaasandoObjetoVazioStatusUtilizadoSistema() throws Exception {
        StatusAgenda statusAgendaCancelado = new StatusAgenda();
        Assert.assertFalse(statusAgendaCancelado.isStatusUtilizadoSistema());
    }
}