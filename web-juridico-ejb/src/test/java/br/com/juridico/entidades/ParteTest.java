package br.com.juridico.entidades;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Date;

import static org.junit.Assert.*;

/**
 * Created by jeffersonl2009_00 on 11/10/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class ParteTest {

    private Parte parte;

    @Before
    public void up() {
        MockitoAnnotations.initMocks(this);
        this.parte = new Parte();
    }

    @Test
    public void adiciona_pessoa_a_lista_de_partes(){
        Posicao posicao = new Posicao(1L, "AUTOR");
        Pessoa pessoa1 = new Pessoa(1L, "Joao do Teste", 'F', true);
        this.parte.setPosicao(posicao);
        ParteAdvogado parteAdvogado = new ParteAdvogado(pessoa1, this.parte, 1L);
        this.parte.addParteAdvogados(parteAdvogado);
        assertEquals(1, this.parte.getAdvogados().size());
    }

    @Test
    public void adiciona_pessoa_passando_nulo_aos_advogados_da_parte(){
        this.parte.addParteAdvogados(null);
        assertEquals(0, this.parte.getAdvogados().size());
    }

    @Test
    public void advogados_da_parte_quando_pertencem_a_um_grupo_test(){
        Pessoa pessoa1 = new Pessoa(1L, "Joao do Teste", 'F', true);
        PessoaFisica pessoaFisica1 = new PessoaFisica(1L, "329.767.238-26", 'M', new Date());
        pessoaFisica1.setPessoa(pessoa1);

        Pessoa pessoa2 = new Pessoa(2L, "Marcao do Teste", 'F', true);
        PessoaFisica pessoaFisica2 = new PessoaFisica(2L, "329.000.238-26", 'M', new Date());
        pessoaFisica2.setPessoa(pessoa2);

        Grupo grupo = new Grupo(1L, "santiago e tourinho advogados");
        GrupoAdvogado grupoAdvogado1 = new GrupoAdvogado(grupo, pessoaFisica1, 1L);
        GrupoAdvogado grupoAdvogado2 = new GrupoAdvogado(grupo, pessoaFisica2, 1L);

        grupo.addGruposAdvogados(grupoAdvogado1);
        grupo.addGruposAdvogados(grupoAdvogado2);

        this.parte.setGrupo(grupo);

        assertFalse(this.parte.getDescricaoAdvogados().isEmpty());
        assertEquals("Joao do Teste / Marcao do Teste", this.parte.getDescricaoAdvogados());
    }

    @Test
    public void advogados_da_parte_quando_nao_pertencem_a_um_grupo_test(){
        Pessoa pessoa1 = new Pessoa(1L, "Joao do Teste Advogado1", 'F', true);
        ParteAdvogado parteAdvogado1 = new ParteAdvogado(pessoa1, this.parte, 1L);

        Pessoa pessoa2 = new Pessoa(1L, "Marcao do Teste Advogado2", 'F', true);
        ParteAdvogado parteAdvogado2 = new ParteAdvogado(pessoa2, this.parte, 1L);

        this.parte.addParteAdvogados(parteAdvogado1);
        this.parte.addParteAdvogados(parteAdvogado2);

        assertFalse(this.parte.getAdvogados().isEmpty());
        assertEquals("Joao do Teste Advogado1 / Marcao do Teste Advogado2", this.parte.getDescricaoAdvogados());
    }

    @Test
    public void advogados_da_parte_quando_nulo_test(){
        this.parte.addParteAdvogados(null);
        this.parte.setGrupo(null);
        assertTrue(this.parte.getDescricaoAdvogados().isEmpty());
    }
}
