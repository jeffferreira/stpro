package br.com.juridico.entidades;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Created by jeffersonl2009_00 on 11/10/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class DireitoPerfilTest {

    private DireitoPerfil direitoPerfil;

    @Before
    public void up() {
        MockitoAnnotations.initMocks(this);
        this.direitoPerfil = new DireitoPerfil();
    }

    @Test
    public void verica_se_contains_perfil_passando_somente_sigla_test(){
        Direito direito = new Direito("CON", "");
        direitoPerfil.setDireito(direito);
        Assert.assertFalse(direitoPerfil.containsPerfilDireito("CON", null, null));
    }

    @Test
    public void verica_se_contains_perfil_passando_somente_perfil_acesso_test(){
        Direito direito = new Direito("", "");
        direitoPerfil.setDireito(direito);
        Assert.assertFalse(direitoPerfil.containsPerfilDireito("", 0L, null));
    }

    @Test
    public void verica_se_contains_perfil_passando_somente_direito_id_test(){
        Direito direito = new Direito("", "");
        direitoPerfil.setDireito(direito);
        Assert.assertFalse(direitoPerfil.containsPerfilDireito("", null, 0L));
    }

    @Test
    public void verica_se_contains_perfil_passando_perfil_completo_test(){
        Direito direito = new Direito("CON", "CONSULTAR");
        direito.setId(2L);

        Perfil perfil = new Perfil("ADMIN", "Administrador do sistema", 1L, Boolean.TRUE);
        Acesso acesso = new Acesso();

        PerfilAcesso perfilAcesso = new PerfilAcesso(perfil, acesso, 1L);
        perfilAcesso.setId(1L);

        direitoPerfil.setDireito(direito);
        direitoPerfil.setPerfilAcesso(perfilAcesso);

        Assert.assertTrue(direitoPerfil.containsPerfilDireito("CON", 1L, 2L));
    }

}
