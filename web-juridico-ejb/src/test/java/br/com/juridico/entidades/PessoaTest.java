package br.com.juridico.entidades;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Date;

import static org.junit.Assert.*;

/**
 * Created by jeffersonl2009_00 on 13/10/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class PessoaTest {

    private Pessoa pessoa;

    @Before
    public void up() {
        MockitoAnnotations.initMocks(this);
        this.pessoa =  new Pessoa(1L, "Joao do Teste", 'F', true);
    }

    @Test
    public void adiciona_nulo_a_lista_de_emails(){
        this.pessoa.addPessoasEmails(null);
        assertTrue(this.pessoa.getPessoasEmails().isEmpty());
    }

    @Test
    public void adiciona_email_a_lista_de_emails(){
        PessoaEmail pessoaEmail = new PessoaEmail(pessoa, "teste@gmail.com", 1L);
        this.pessoa.addPessoasEmails(pessoaEmail);
        assertFalse(this.pessoa.getPessoasEmails().isEmpty());
        assertEquals(1, this.pessoa.getPessoasEmails().size());
    }

    @Test
    public void adiciona_nulo_a_lista_de_contatos(){
        this.pessoa.addPessoasContatos(null);
        assertTrue(this.pessoa.getPessoasContatos().isEmpty());
    }

    @Test
    public void adiciona_contato_a_lista_de_contatos(){
        Contato contato = new Contato(44, "91281273");
        PessoaContato pessoaContato = new PessoaContato(contato, this.pessoa, 1L);
        this.pessoa.addPessoasContatos(pessoaContato);
        assertFalse(this.pessoa.getPessoasContatos().isEmpty());
        assertEquals(1, this.pessoa.getPessoasContatos().size());
    }

    @Test
    public void adiciona_endereco_a_lista_de_enderecos(){
        Endereco endereco = new Endereco(1L, "Rua XYZ");
        PessoaEndereco pessoaEndereco = new PessoaEndereco(endereco, this.pessoa, 1L);
        this.pessoa.addPessoasEnderecos(pessoaEndereco);
        assertFalse(this.pessoa.getPessoasEnderecos().isEmpty());
        assertEquals(1, this.pessoa.getPessoasEnderecos().size());
    }

    @Test
    public void testGetClienteSimNao_true() throws Exception {
        this.pessoa.setCliente(true);
        assertEquals("SIM", this.pessoa.getClienteSimNao());
    }

    @Test
    public void testGetClienteSimNao_false() throws Exception {
        this.pessoa.setCliente(false);
        assertEquals("NÃO", this.pessoa.getClienteSimNao());
    }

    @Test
    public void testIsPessoaFisica_true() throws Exception {
        this.pessoa.setTipo('F');
        assertTrue(this.pessoa.isPessoaFisica());
    }

    @Test
    public void testIsPessoaFisica_false() throws Exception {
        this.pessoa.setTipo('J');
        assertFalse(this.pessoa.isPessoaFisica());
    }

    @Test
    public void testIsClienteEmpresa_true() throws Exception {
        HistoricoPessoaCliente historico = new HistoricoPessoaCliente(true, this.pessoa, new Date(), "", 1L);
        pessoa.addHistorico(historico);
        assertTrue(this.pessoa.isClienteEmpresa());
    }

    @Test
    public void testIsClienteEmpresa_false() throws Exception {
        assertFalse(this.pessoa.isClienteEmpresa());
    }

    @Test
    public void testSetNomeIniciais_null() throws Exception {
        this.pessoa.setNomeIniciais(null);
        assertNull(this.pessoa.getNomeIniciais());

        this.pessoa.setNomeIniciais("");
        assertNull(this.pessoa.getNomeIniciais());
    }

    @Test
    public void testSetNomeIniciais_upper_case() throws Exception {
        this.pessoa.setNomeIniciais("aa");
        assertEquals("AA", this.pessoa.getNomeIniciais());

        this.pessoa.setNomeIniciais("aA");
        assertEquals("AA", this.pessoa.getNomeIniciais());

        this.pessoa.setNomeIniciais("Aa");
        assertEquals("AA", this.pessoa.getNomeIniciais());

        this.pessoa.setNomeIniciais("AA");
        assertEquals("AA", this.pessoa.getNomeIniciais());
    }

    @Test
    public void testAddHistorico() throws Exception {
        HistoricoPessoaCliente historico = new HistoricoPessoaCliente();
        this.pessoa.addHistorico(historico);
        assertEquals(1, this.pessoa.getHistoricoClienteList().size());
        assertTrue(this.pessoa.getHistoricoClienteList().get(0).getAtivo());

        HistoricoPessoaCliente historico1 = new HistoricoPessoaCliente();
        this.pessoa.addHistorico(historico1);
        assertEquals(2, this.pessoa.getHistoricoClienteList().size());
        assertFalse(this.pessoa.getHistoricoClienteList().get(0).getAtivo());
        assertTrue(this.pessoa.getHistoricoClienteList().get(1).getAtivo());
    }
}
