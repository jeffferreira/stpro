package br.com.juridico.entidades;

import br.com.juridico.util.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Date;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by jeffersonl2009_00 on 11/10/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class ContratoPessoaTest {

    private ContratoPessoa contratoPessoa;

    @Before
    public void up() {
        MockitoAnnotations.initMocks(this);
        this.contratoPessoa = new ContratoPessoa();
    }

    @Test
    public void ordenou_crescente_vigencias_test(){
        int mes = 1;
        int ano = 2009;
        ContratoPessoaVigencia contratoForaVigencia = new ContratoPessoaVigencia();
        contratoForaVigencia.setDataInicio(DateUtils.lowerLimit(DateUtils.geraPrimeiroDiaDoMes(mes, ano)));
        contratoForaVigencia.setDataFim(DateUtils.upperLimit(DateUtils.geraPrimeiroDiaDoMes(mes, ano)));

        ContratoPessoaVigencia contratoVigente = new ContratoPessoaVigencia();
        contratoVigente.setDataInicio(DateUtils.lowerLimit(new Date()));
        contratoVigente.setDataFim(DateUtils.upperLimit(new Date()));

        contratoPessoa.addVigencia(contratoForaVigencia);
        contratoPessoa.addVigencia(contratoVigente);

        assertTrue(contratoPessoa.getVigencias().get(0).isContratoVigente());

    }

    @Test
    public void ultima_posicao_lista_nao_esta_vigente_test(){
        int mes = 1;
        int ano = 2009;
        ContratoPessoaVigencia contratoForaVigencia = new ContratoPessoaVigencia();
        contratoForaVigencia.setDataInicio(DateUtils.lowerLimit(DateUtils.geraPrimeiroDiaDoMes(mes, ano)));
        contratoForaVigencia.setDataFim(DateUtils.upperLimit(DateUtils.geraPrimeiroDiaDoMes(mes, ano)));

        ContratoPessoaVigencia contratoVigente = new ContratoPessoaVigencia();
        contratoVigente.setDataInicio(DateUtils.lowerLimit(new Date()));
        contratoVigente.setDataFim(DateUtils.upperLimit(new Date()));

        contratoPessoa.addVigencia(contratoForaVigencia);
        contratoPessoa.addVigencia(contratoVigente);

        assertFalse(contratoPessoa.getVigencias().get(1).isContratoVigente());

    }
}
