package br.com.juridico.entidades;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;

/**
 * Created by jeffersonl2009_00 on 13/10/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class PessoaFisicaTest {

    private PessoaFisica pessoaFisica;

    @Before
    public void up() {
        MockitoAnnotations.initMocks(this);
        this.pessoaFisica = new PessoaFisica();
    }

    @Test
    public void verifica_descricao_emails_quando_nulo(){
        assertTrue(this.pessoaFisica.getEmails().isEmpty());
    }

    @Test
    public void verifica_descricao_telefones_quando_nulo(){
        assertTrue(this.pessoaFisica.getTelefones().isEmpty());
    }

    @Test
    public void verifica_descricao_um_email_emails(){
        Pessoa pessoa1 = new Pessoa(1L, "Joao do Teste", 'F', true);
        PessoaEmail pessoaEmail = new PessoaEmail(pessoa1, "teste@gmail.com", 1L);
        pessoa1.addPessoasEmails(pessoaEmail);
        this.pessoaFisica.setPessoa(pessoa1);

        assertFalse(this.pessoaFisica.getEmails().isEmpty());
        assertEquals("teste@gmail.com;", this.pessoaFisica.getEmails());

    }

    @Test
    public void verifica_descricao_dois_email_emails(){
        Pessoa pessoa1 = new Pessoa(1L, "Joao do Teste", 'F', true);
        PessoaEmail pessoaEmail1 = new PessoaEmail(pessoa1, "teste@gmail.com", 1L);
        PessoaEmail pessoaEmail2 = new PessoaEmail(pessoa1, "marcao@gmail.com", 1L);
        pessoa1.addPessoasEmails(pessoaEmail1);
        pessoa1.addPessoasEmails(pessoaEmail2);
        this.pessoaFisica.setPessoa(pessoa1);

        assertFalse(this.pessoaFisica.getEmails().isEmpty());
        assertEquals(2, this.pessoaFisica.getPessoa().getPessoasEmails().size());
        assertEquals("teste@gmail.com; marcao@gmail.com;", this.pessoaFisica.getEmails());

    }

    @Test
    public void verifica_descricao_telefones(){
        Pessoa pessoa1 = new Pessoa(1L, "Joao do Teste", 'F', true);
        Contato contato = new Contato(44, "91281273");
        PessoaContato pessoaContato = new PessoaContato(contato, pessoa1, 1L);
        pessoa1.addPessoasContatos(pessoaContato);
        this.pessoaFisica.setPessoa(pessoa1);

        assertFalse(this.pessoaFisica.getTelefones().isEmpty());
        assertEquals("(44) 9128-1273;", this.pessoaFisica.getTelefones());
    }

    @Test
    public void verifica_descricao_dois_telefones(){
        Pessoa pessoa1 = new Pessoa(1L, "Joao do Teste", 'F', true);
        Contato contato1 = new Contato(44, "91281273");
        Contato contato2 = new Contato(11, "960671273");
        PessoaContato pessoaContato1 = new PessoaContato(contato1, pessoa1, 1L);
        PessoaContato pessoaContato2 = new PessoaContato(contato2, pessoa1, 1L);
        pessoa1.addPessoasContatos(pessoaContato1);
        pessoa1.addPessoasContatos(pessoaContato2);
        this.pessoaFisica.setPessoa(pessoa1);

        assertFalse(this.pessoaFisica.getTelefones().isEmpty());
        assertEquals(2, this.pessoaFisica.getPessoa().getPessoasContatos().size());
        assertEquals("(44) 9128-1273; (11) 96067-1273;", this.pessoaFisica.getTelefones());
    }
}
