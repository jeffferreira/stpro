package br.com.juridico.entidades;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;

/**
 * Created by jeffersonl2009_00 on 11/10/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class EmailCaixaSaidaTest {

    private EmailCaixaSaida emailCaixaSaida;

    @Before
    public void up() {
        MockitoAnnotations.initMocks(this);
        this.emailCaixaSaida = new EmailCaixaSaida();
    }

    @Test
    public void descricao_email_menor_que_sessenta_caracteres(){
        String email = "email referente ao teste unitario xyz.";
        emailCaixaSaida.setCorpoEmail(email);
        assertEquals(email, emailCaixaSaida.getDescricaoEmail());
    }

    @Test
    public void descricao_email_maior_que_sessenta_caracteres(){
        String email = "No entanto, não podemos esquecer que a consulta aos diversos militantes auxilia a preparação e a composição do remanejamento dos quadros funcionais.";
        emailCaixaSaida.setCorpoEmail(email);
        int tamanhoEsperado = Integer.valueOf(63);
        assertEquals(tamanhoEsperado, emailCaixaSaida.getDescricaoEmail().length());

        String ultimosCaracteres = emailCaixaSaida.getDescricaoEmail().substring(60, 63);
        assertEquals("...", ultimosCaracteres);
    }

    @Test
    public void descricao_email_passando_null(){
        emailCaixaSaida.setCorpoEmail(null);
        assertEquals(0, emailCaixaSaida.getDescricaoEmail().length());
    }

    @Test
    public void retorno_destinatarios_passando_dois_destinatarios_test(){
        EmailDestinatario destinatario1 = new EmailDestinatario();
        destinatario1.setDestinatario("joao_do_teste@gmail.com");

        EmailDestinatario destinatario2 = new EmailDestinatario();
        destinatario2.setDestinatario("marcao_do_teste@gmail.com");

        emailCaixaSaida.addEmails(destinatario1);
        emailCaixaSaida.addEmails(destinatario2);

        assertEquals(2, emailCaixaSaida.getEmails().size());
        assertEquals("joao_do_teste@gmail.com; marcao_do_teste@gmail.com; ", emailCaixaSaida.getDestinatarios());

    }


}
