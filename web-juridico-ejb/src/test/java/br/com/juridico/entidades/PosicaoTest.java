package br.com.juridico.entidades;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by jeffersonl2009_00 on 14/10/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class PosicaoTest {

    @Before
    public void up() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void pode_excluir_autor_test(){
        Posicao posicao = new Posicao(1L, "Autor");
        assertFalse(!posicao.isPosicaoUtilizadaSistema());

    }

    @Test
    public void pode_excluir_reu_test(){
        Posicao posicao = new Posicao(2L, "Réu");
        assertFalse(!posicao.isPosicaoUtilizadaSistema());
    }

    @Test
    public void pode_excluir_terceiro_test(){
        Posicao posicao = new Posicao(3L, "Terceiro");
        assertFalse(!posicao.isPosicaoUtilizadaSistema());
    }

    @Test
    public void pode_excluir_posicao_passando_outra_descricao_test(){
        Posicao posicao = new Posicao(4L, "Outro");
        assertTrue(!posicao.isPosicaoUtilizadaSistema());
    }

    @Test
    public void verficando_se_pode_excluir_posicao_passando_id_nulo_test(){
        Posicao posicao = new Posicao(null, "");
        assertTrue(!posicao.isPosicaoUtilizadaSistema());
    }
}
