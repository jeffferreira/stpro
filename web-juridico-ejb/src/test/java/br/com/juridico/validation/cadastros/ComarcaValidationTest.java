package br.com.juridico.validation.cadastros;

import br.com.juridico.entidades.Comarca;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.ValidationUtilTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import javax.validation.ConstraintViolation;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * Created by jeffersonl2009_00 on 06/07/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class ComarcaValidationTest extends ValidationUtilTest {

    @Before
    public void up() {
        initValidation();
    }

    @Test
    public void campo_descricao_vazio() {
        Comarca c = new Comarca();
        c.setDescricao("");
        Set<ConstraintViolation<ComarcaValidacaoWrapper>> violations = buildValidatorFactory().validate(new ComarcaValidacaoWrapper(c, false));
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(DESCRICAO_PREENCHIMENTO_OBRIGATORIO, messages.get(0));

    }

    @Test
    public void campo_descricao_nulo(){
        Set<ConstraintViolation<ComarcaValidacaoWrapper>> violations = buildValidatorFactory().validate(new ComarcaValidacaoWrapper(new Comarca(), false));
        List<String> messages = getMensagens(violations);

        assertEquals(1, violations.size());
        assertEquals(DESCRICAO_PREENCHIMENTO_OBRIGATORIO, messages.get(0));

    }

    @Test
    public void campo_descricao_preenchido(){
        Comarca c = new Comarca();
        c.setDescricao("bla bla bla");
        Set<ConstraintViolation<Comarca>> violations = validator.validate(c);
        assertEquals(0, violations.size());
    }

    @Test(expected = ValidationException.class)
    public void campo_duplicado_test() throws ValidationException {
        Comarca c = new Comarca();
        c.validar(true);
    }
}
