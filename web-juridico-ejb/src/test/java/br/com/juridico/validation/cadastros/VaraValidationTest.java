package br.com.juridico.validation.cadastros;

import br.com.juridico.entidades.Vara;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.ValidationUtilTest;
import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * Created by jeffersonl2009_00 on 06/07/2016.
 */
public class VaraValidationTest extends ValidationUtilTest {

    @Before
    public void up() {
        initValidation();
    }

    @Test
    public void campo_descricao_vazio() {
        Vara c = new Vara();
        c.setDescricao("");
        Set<ConstraintViolation<VaraValidacaoWrapper>> violations = buildValidatorFactory().validate(new VaraValidacaoWrapper(c, false));
        List<String> messages = getMensagens(violations);

        assertEquals(1, violations.size());
        assertEquals(DESCRICAO_PREENCHIMENTO_OBRIGATORIO, messages.get(0));

    }

    @Test
    public void campo_descricao_nulo() {
        Set<ConstraintViolation<VaraValidacaoWrapper>> violations = buildValidatorFactory().validate(new VaraValidacaoWrapper(new Vara(), false));
        List<String> messages = getMensagens(violations);

        assertEquals(1, violations.size());
        assertEquals(DESCRICAO_PREENCHIMENTO_OBRIGATORIO, messages.get(0));

    }

    @Test
    public void campo_descricao_preenchido() {
        Vara c = new Vara();
        c.setDescricao("bla bla bla");
        Set<ConstraintViolation<Vara>> violations = validator.validate(c);
        assertEquals(0, violations.size());
    }

    @Test(expected = ValidationException.class)
    public void campo_duplicado_test() throws ValidationException {
        Vara c = new Vara();
        c.validar(true);
    }
}
