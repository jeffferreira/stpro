package br.com.juridico.validation.cadastros;

import br.com.juridico.entidades.GrauRisco;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.ValidationUtilTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import javax.validation.ConstraintViolation;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * Created by jeffersonl2009_00 on 06/07/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class GrauRiscoValidationTest extends ValidationUtilTest {

    @Before
    public void up() {
        initValidation();
    }

    @Test
    public void campo_descricao_vazio(){
        GrauRisco c = new GrauRisco();
        c.setDescricao("");
        Set<ConstraintViolation<GrauRiscoValidacaoWrapper>> violations = buildValidatorFactory().validate(new GrauRiscoValidacaoWrapper(c, false));
        List<String> messages = getMensagens(violations);

        assertEquals(1, violations.size());
        assertEquals(DESCRICAO_PREENCHIMENTO_OBRIGATORIO, messages.get(0));

    }

    @Test
    public void campo_descricao_nulo(){
        Set<ConstraintViolation<GrauRiscoValidacaoWrapper>> violations = buildValidatorFactory().validate(new GrauRiscoValidacaoWrapper(new GrauRisco(), false));
        List<String> messages = getMensagens(violations);

        assertEquals(1, violations.size());
        assertEquals(DESCRICAO_PREENCHIMENTO_OBRIGATORIO, messages.get(0));

    }

    @Test
    public void campo_descricao_preenchido(){
        GrauRisco c = new GrauRisco();
        c.setDescricao("bla bla bla");
        Set<ConstraintViolation<GrauRisco>> violations = validator.validate(c);
        assertEquals(0, violations.size());
    }

    @Test(expected = ValidationException.class)
    public void campo_duplicado_test() throws ValidationException {
        GrauRisco c = new GrauRisco();
        c.validar(true);
    }
}
