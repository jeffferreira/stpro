package br.com.juridico.validation.cadastros;

import br.com.juridico.entidades.Fornecedor;
import br.com.juridico.enums.TipoPessoaType;
import br.com.juridico.validation.ValidationUtilTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import javax.validation.ConstraintViolation;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * Created by jefferson on 07/04/2018.
 */
@RunWith(MockitoJUnitRunner.class)
public class FornecedorValidacaoWrapperTest extends ValidationUtilTest {

    private static final String REGISTRO_JA_CADASTRADO= "Já existe um registro cadastrado com este nome.";
    private static final String NOME_PREENCHIMENTO_OBRIGATORIO = "Nome: preenchimento obrigatório";
    private static final String CPF_CNPJ_PREENCHIMENTO_OBRIGATORIO = "CPF ou CNPJ: preenchimento obrigatório";
    private static final String NOME_FAVORECIDO_PREENCHIMENTO_OBRIGATORIO = "Nome Favorecido: preenchimento obrigatório";
    private static final String CPF_CNPJ_FAVORECIDO_PREENCHIMENTO_OBRIGATORIO = "CPF ou CNPJ Favorecido: preenchimento obrigatório";
    private static final String BANCO_FAVORECIDO_PREENCHIMENTO_OBRIGATORIO = "Banco Favorecido: preenchimento obrigatório";
    private static final String AGENCIA_FAVORECIDO_PREENCHIMENTO_OBRIGATORIO = "Agência Favorecido: preenchimento obrigatório";
    private static final String CONTA_FAVORECIDO_PREENCHIMENTO_OBRIGATORIO = "Conta Corrente: preenchimento obrigatório";

    @Before
    public void up() {
        initValidation();
    }

    @Test
    public void ja_possui_fornecedor_cadastrado_test(){
        Fornecedor fornecedor = new Fornecedor();
        fornecedor.setNome("Nome Teste");
        fornecedor.setCpfCnpj("32923545874");
        fornecedor.setApelido("apelido teste");
        Set<ConstraintViolation<FornecedorValidacaoWrapper>> violations = buildValidatorFactory().validate(new FornecedorValidacaoWrapper(fornecedor, true, false));
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(REGISTRO_JA_CADASTRADO, messages.get(0));
    }

    @Test
    public void campo_cpf_cnpj_vazio_test(){
        Fornecedor fornecedor = new Fornecedor();
        fornecedor.setNome("Nome Teste");
        fornecedor.setApelido("Teste");
        fornecedor.setCpfCnpj("");
        Set<ConstraintViolation<FornecedorValidacaoWrapper>> violations = buildValidatorFactory().validate(new FornecedorValidacaoWrapper(fornecedor, false, false));
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(CPF_CNPJ_PREENCHIMENTO_OBRIGATORIO, messages.get(0));
    }

    @Test
    public void campo_nome_vazio_test(){
        Fornecedor fornecedor = new Fornecedor();
        fornecedor.setNome("");
        fornecedor.setApelido("Apelido teste");
        fornecedor.setCpfCnpj("32154698745");
        Set<ConstraintViolation<FornecedorValidacaoWrapper>> violations = buildValidatorFactory().validate(new FornecedorValidacaoWrapper(fornecedor, false, false));
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(NOME_PREENCHIMENTO_OBRIGATORIO, messages.get(0));
    }

    @Test
    public void campo_nome_favorecido_vazio_test(){
        Fornecedor fornecedor = new Fornecedor();
        fornecedor.setNome("Nome Teste");
        fornecedor.setApelido("Apelido teste");
        fornecedor.setCpfCnpj("32154698745");

        fornecedor.setTipoFavorecido(TipoPessoaType.F);
        fornecedor.setNomeFavorecido("");
        fornecedor.setCpfCnpjFavorecido("3215468745");
        fornecedor.setBancoFavorecido("031");
        fornecedor.setAgenciaFavorecido("3130");
        fornecedor.setContaFavorecido("345221");

        Set<ConstraintViolation<FornecedorValidacaoWrapper>> violations = buildValidatorFactory().validate(new FornecedorValidacaoWrapper(fornecedor, false, true));
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(NOME_FAVORECIDO_PREENCHIMENTO_OBRIGATORIO, messages.get(0));
    }

    @Test
    public void campo_cpfCnpj_favorecido_vazio_test(){
        Fornecedor fornecedor = new Fornecedor();
        fornecedor.setNome("Nome Teste");
        fornecedor.setApelido("Apelido teste");
        fornecedor.setCpfCnpj("32154698745");

        fornecedor.setTipoFavorecido(TipoPessoaType.F);
        fornecedor.setNomeFavorecido("Nome Favorecido");
        fornecedor.setCpfCnpjFavorecido("");
        fornecedor.setBancoFavorecido("031");
        fornecedor.setAgenciaFavorecido("3130");
        fornecedor.setContaFavorecido("345221");

        Set<ConstraintViolation<FornecedorValidacaoWrapper>> violations = buildValidatorFactory().validate(new FornecedorValidacaoWrapper(fornecedor, false, true));
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(CPF_CNPJ_FAVORECIDO_PREENCHIMENTO_OBRIGATORIO, messages.get(0));
    }

    @Test
    public void campo_banco_favorecido_vazio_test(){
        Fornecedor fornecedor = new Fornecedor();
        fornecedor.setNome("Nome Teste");
        fornecedor.setApelido("Apelido teste");
        fornecedor.setCpfCnpj("32154698745");

        fornecedor.setTipoFavorecido(TipoPessoaType.F);
        fornecedor.setNomeFavorecido("Nome Favorecido");
        fornecedor.setCpfCnpjFavorecido("Cpf Cnpj");
        fornecedor.setBancoFavorecido("");
        fornecedor.setAgenciaFavorecido("3130");
        fornecedor.setContaFavorecido("345221");

        Set<ConstraintViolation<FornecedorValidacaoWrapper>> violations = buildValidatorFactory().validate(new FornecedorValidacaoWrapper(fornecedor, false, true));
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(BANCO_FAVORECIDO_PREENCHIMENTO_OBRIGATORIO, messages.get(0));
    }

    @Test
    public void campo_agencia_favorecido_vazio_test(){
        Fornecedor fornecedor = new Fornecedor();
        fornecedor.setNome("Nome Teste");
        fornecedor.setApelido("Apelido teste");
        fornecedor.setCpfCnpj("32154698745");

        fornecedor.setTipoFavorecido(TipoPessoaType.F);
        fornecedor.setNomeFavorecido("Nome Favorecido");
        fornecedor.setCpfCnpjFavorecido("Cpf Cnpj");
        fornecedor.setBancoFavorecido("031");
        fornecedor.setAgenciaFavorecido("");
        fornecedor.setContaFavorecido("345221");

        Set<ConstraintViolation<FornecedorValidacaoWrapper>> violations = buildValidatorFactory().validate(new FornecedorValidacaoWrapper(fornecedor, false, true));
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(AGENCIA_FAVORECIDO_PREENCHIMENTO_OBRIGATORIO, messages.get(0));
    }

    @Test
    public void campo_conta_favorecido_vazio_test(){
        Fornecedor fornecedor = new Fornecedor();
        fornecedor.setNome("Nome Teste");
        fornecedor.setApelido("Apelido teste");
        fornecedor.setCpfCnpj("32154698745");

        fornecedor.setTipoFavorecido(TipoPessoaType.F);
        fornecedor.setNomeFavorecido("Nome Favorecido");
        fornecedor.setCpfCnpjFavorecido("Cpf Cnpj");
        fornecedor.setBancoFavorecido("031");
        fornecedor.setAgenciaFavorecido("031");
        fornecedor.setContaFavorecido("");

        Set<ConstraintViolation<FornecedorValidacaoWrapper>> violations = buildValidatorFactory().validate(new FornecedorValidacaoWrapper(fornecedor, false, true));
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(CONTA_FAVORECIDO_PREENCHIMENTO_OBRIGATORIO, messages.get(0));
    }


}