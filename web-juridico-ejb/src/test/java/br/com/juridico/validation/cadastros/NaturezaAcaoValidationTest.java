package br.com.juridico.validation.cadastros;

import br.com.juridico.entidades.NaturezaAcao;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.ValidationUtilTest;
import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * Created by jeffersonl2009_00 on 06/07/2016.
 */
public class NaturezaAcaoValidationTest extends ValidationUtilTest {

    @Before
    public void up() {
        initValidation();
    }

    @Test
    public void campo_descricao_vazio(){
        NaturezaAcao c = new NaturezaAcao();
        c.setDescricao("");
        Set<ConstraintViolation<NaturezaAcaoValidacaoWrapper>> violations = buildValidatorFactory().validate(new NaturezaAcaoValidacaoWrapper(c, false));
        List<String> messages = getMensagens(violations);

        assertEquals(1, violations.size());
        assertEquals(DESCRICAO_PREENCHIMENTO_OBRIGATORIO, messages.get(0));

    }

    @Test
    public void campo_descricao_nulo(){
        Set<ConstraintViolation<NaturezaAcaoValidacaoWrapper>> violations = buildValidatorFactory().validate(new NaturezaAcaoValidacaoWrapper(new NaturezaAcao(), false));
        List<String> messages = getMensagens(violations);

        assertEquals(1, violations.size());
        assertEquals(DESCRICAO_PREENCHIMENTO_OBRIGATORIO, messages.get(0));

    }

    @Test
    public void campo_descricao_preenchido(){
        NaturezaAcao c = new NaturezaAcao();
        c.setDescricao("bla bla bla");
        Set<ConstraintViolation<NaturezaAcao>> violations = validator.validate(c);
        assertEquals(0, violations.size());
    }

    @Test(expected = ValidationException.class)
    public void campo_duplicado_test() throws ValidationException {
        NaturezaAcao c = new NaturezaAcao();
        c.validar(true);
    }
}

