package br.com.juridico.validation.cadastros;

import br.com.juridico.entidades.Empresa;
import br.com.juridico.validation.ValidationUtilTest;
import br.com.juridico.validation.pessoas.AdvogadoValidacaoWrapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * Created by jefferson on 29/01/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class EmpresaValidacaoWrapperTest extends ValidationUtilTest {

    private static Validator validator;

    private String VALIDATION_FANTASIA = "Por favor informe a fantasia.";
    private String VALIDATION_RAZAO_SOCIAL = "Por favor informe a razão social.";
    private String VALIDATION_CNPJ = "Por favor informe o CNPJ.";
    private String VALIDATION_ENDERECO = "Por favor informe o endereço.";
    private String VALIDATION_NUMERO = "Por favor informe o número.";
    private String VALIDATION_CIDADE = "Por favor informe a cidade.";
    private String VALIDATION_UF = "Por favor informe a UF.";

    @Before
    public void up() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void campo_todos_campos_vazios_test(){
        Set<ConstraintViolation<EmpresaValidacaoWrapper>> violations = getConstraintViolations(new Empresa());
        assertEquals(7, violations.size());
    }

    @Test
    public void campo_fantasia_vazio_test(){
        Empresa empresa = new Empresa();
        empresa.setRazaoSocial("teste de razao social");
        empresa.setCnpj("teste de cnpj");
        empresa.setLogradouro("teste de endereco");
        empresa.setNumero(125);
        empresa.setCidade("teste de cidade");
        empresa.setUf("teste de uf");

        Set<ConstraintViolation<EmpresaValidacaoWrapper>> violations = getConstraintViolations(empresa);
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(VALIDATION_FANTASIA, messages.get(0));
    }

    @Test
    public void campo_razao_social_vazio_test(){
        Empresa empresa = new Empresa();
        empresa.setDescricao("teste de fantasia");
        empresa.setCnpj("teste de cnpj");
        empresa.setLogradouro("teste de endereco");
        empresa.setNumero(125);
        empresa.setCidade("teste de cidade");
        empresa.setUf("teste de uf");

        Set<ConstraintViolation<EmpresaValidacaoWrapper>> violations = getConstraintViolations(empresa);
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(VALIDATION_RAZAO_SOCIAL, messages.get(0));
    }

    @Test
    public void campo_cnpj_vazio_test(){
        Empresa empresa = new Empresa();
        empresa.setDescricao("teste de fantasia");
        empresa.setRazaoSocial("teste de razao social");
        empresa.setLogradouro("teste de endereco");
        empresa.setNumero(125);
        empresa.setCidade("teste de cidade");
        empresa.setUf("teste de uf");

        Set<ConstraintViolation<EmpresaValidacaoWrapper>> violations = getConstraintViolations(empresa);
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(VALIDATION_CNPJ, messages.get(0));
    }

    @Test
    public void campo_endereco_vazio_test(){
        Empresa empresa = new Empresa();
        empresa.setDescricao("teste de fantasia");
        empresa.setRazaoSocial("teste de razao social");
        empresa.setCnpj("teste de cnpj");
        empresa.setNumero(125);
        empresa.setCidade("teste de cidade");
        empresa.setUf("teste de uf");

        Set<ConstraintViolation<EmpresaValidacaoWrapper>> violations = getConstraintViolations(empresa);
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(VALIDATION_ENDERECO, messages.get(0));
    }

    @Test
    public void campo_numero_vazio_test(){
        Empresa empresa = new Empresa();
        empresa.setDescricao("teste de fantasia");
        empresa.setRazaoSocial("teste de razao social");
        empresa.setCnpj("teste de cnpj");
        empresa.setLogradouro("teste de endereco");
        empresa.setCidade("teste de cidade");
        empresa.setUf("teste de uf");

        Set<ConstraintViolation<EmpresaValidacaoWrapper>> violations = getConstraintViolations(empresa);
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(VALIDATION_NUMERO, messages.get(0));
    }

    @Test
    public void campo_cidade_vazio_test(){
        Empresa empresa = new Empresa();
        empresa.setDescricao("teste de fantasia");
        empresa.setRazaoSocial("teste de razao social");
        empresa.setCnpj("teste de cnpj");
        empresa.setLogradouro("teste de endereco");
        empresa.setNumero(125);
        empresa.setUf("teste de uf");

        Set<ConstraintViolation<EmpresaValidacaoWrapper>> violations = getConstraintViolations(empresa);
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(VALIDATION_CIDADE, messages.get(0));
    }

    @Test
    public void campo_uf_vazio_test(){
        Empresa empresa = new Empresa();
        empresa.setDescricao("teste de fantasia");
        empresa.setRazaoSocial("teste de razao social");
        empresa.setCnpj("teste de cnpj");
        empresa.setLogradouro("teste de endereco");
        empresa.setNumero(125);
        empresa.setCidade("teste de cidade");

        Set<ConstraintViolation<EmpresaValidacaoWrapper>> violations = getConstraintViolations(empresa);
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(VALIDATION_UF, messages.get(0));
    }

    @Test
    public void todos_os_campos_foram_preenchidos_test(){
        Empresa empresa = new Empresa();
        empresa.setDescricao("teste de fantasia");
        empresa.setRazaoSocial("teste de razao social");
        empresa.setCnpj("teste de cnpj");
        empresa.setLogradouro("teste de endereco");
        empresa.setNumero(125);
        empresa.setCidade("teste de cidade");
        empresa.setUf("teste de uf");

        Set<ConstraintViolation<EmpresaValidacaoWrapper>> violations = getConstraintViolations(empresa);
        assertEquals(0, violations.size());
    }

    private Set<ConstraintViolation<EmpresaValidacaoWrapper>> getConstraintViolations(Empresa empresa) {
        return Validation.buildDefaultValidatorFactory().getValidator()
                .validate(new EmpresaValidacaoWrapper(empresa));
    }

}