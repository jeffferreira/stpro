package br.com.juridico.validation.cadastros;

import br.com.juridico.entidades.CentroCusto;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.ValidationUtilTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import javax.validation.ConstraintViolation;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * Created by jeffersonl2009_00 on 06/07/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class CentroCustoValidationTest extends ValidationUtilTest {

    @Before
    public void up() {
        initValidation();
    }

    @Test
    public void campo_descricao_vazio(){
        CentroCusto centroCusto = new CentroCusto();
        centroCusto.setDescricao("");
        Set<ConstraintViolation<CentroCusto>> violations = validator.validate(centroCusto);
        List<String> messages = getMensagens(violations);

        assertEquals(1, violations.size());
        assertEquals(DESCRICAO_PREENCHIMENTO_OBRIGATORIO, messages.get(0));

    }

    @Test
    public void campo_descricao_nulo(){
        Set<ConstraintViolation<CentroCusto>> violations = validator.validate(new CentroCusto());
        List<String> messages = getMensagens(violations);

        assertEquals(2, violations.size());
        assertEquals(DESCRICAO_PREENCHIMENTO_OBRIGATORIO, messages.get(0));
        assertEquals(NAO_PODE_SER_NULO, messages.get(1));

    }

    @Test
    public void campo_descricao_preenchido(){
        CentroCusto centroCusto = new CentroCusto();
        centroCusto.setDescricao("bla bla bla");
        Set<ConstraintViolation<CentroCusto>> violations = validator.validate(centroCusto);
        assertEquals(0, violations.size());
    }

    @Test(expected = ValidationException.class)
    public void campo_duplicado_test() throws ValidationException {
        CentroCusto c = new CentroCusto();
        c.validar(true);
    }
}
