package br.com.juridico.validation.cadastros;

import br.com.juridico.entidades.ClasseProcessual;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.ValidationUtilTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import javax.validation.ConstraintViolation;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * Created by jeffersonl2009_00 on 06/07/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class ClasseProcessualValidationTest extends ValidationUtilTest {

    @Before
    public void up() {
        initValidation();
    }

    @Test
    public void campo_descricao_vazio(){
        ClasseProcessual c = new ClasseProcessual();
        c.setDescricao("");
        Set<ConstraintViolation<ClasseProcessualValidacaoWrapper>> violations = buildValidatorFactory().validate(new ClasseProcessualValidacaoWrapper(c, false));
        List<String> messages = getMensagens(violations);

        assertEquals(1, violations.size());
        assertEquals(DESCRICAO_PREENCHIMENTO_OBRIGATORIO, messages.get(0));

    }

    @Test
    public void campo_descricao_nulo(){
        Set<ConstraintViolation<ClasseProcessualValidacaoWrapper>> violations = buildValidatorFactory().validate(new ClasseProcessualValidacaoWrapper(new ClasseProcessual(), false));
        List<String> messages = getMensagens(violations);

        assertEquals(1, violations.size());
        assertEquals(DESCRICAO_PREENCHIMENTO_OBRIGATORIO, messages.get(0));

    }

    @Test
    public void campo_descricao_preenchido(){
        ClasseProcessual c = new ClasseProcessual();
        c.setDescricao("bla bla bla");
        Set<ConstraintViolation<ClasseProcessual>> violations = validator.validate(c);
        assertEquals(0, violations.size());
    }

    @Test(expected = ValidationException.class)
    public void campo_duplicado_test() throws ValidationException {
        ClasseProcessual c = new ClasseProcessual();
        c.validar(true);
    }
}
