package br.com.juridico.validation.processo;

import br.com.juridico.entidades.*;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.ValidationUtilTest;
import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * Created by jeffersonl2009_00 on 08/07/2016.
 */
public class DadosProcessoValidationTest extends ValidationUtilTest {

    private static final String CAMPO_VALIDACAO_DATA_ABERTURA = "Favor preencher o campo Data de Abertura.";
    private static final String CAMPO_VALIDACAO_DESCRICAO = "Favor preencher o campo Processo.";
    private static final String CAMPO_VALIDACAO_MEIO_TRAMITACAO = "Favor selecionar o Meio de Tramita\u00e7\u00e3o.";
    private static final String CAMPO_VALIDACAO_FASE = "Favor selecionar a Fase do Processo.";
    private static final String CAMPO_VALIDACAO_CLASSE_PROCESSUAL= "Favor selecionar a Classe Processual.";
    private static final String CAMPO_VALIDACAO_NATUREZA_ACAO = "Favor selecionar a Natureza da A\u00e7\u00e3o.";
    private static final String CAMPO_VALIDACAO_FORUM = "Favor selecionar o F\u00f3rum.";
    private static final String CAMPO_VALIDACAO_COMARCA = "Favor selecionar a Comarca.";
    private static final String CAMPO_VALIDACAO_VARA = "Favor selecionar a Vara.";

    private Processo processo;

    @Before
    public void up() {
        initValidation();
        this.processo = new Processo();
    }

    @Test
    public void todos_os_campos_quando_pessoa_fisica_estao_com_validacao_test(){
        Set<ConstraintViolation<DadosProcessoValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(9, violations.size());
        assertEquals(CAMPO_VALIDACAO_DATA_ABERTURA, messages.get(0));
        assertEquals(CAMPO_VALIDACAO_DESCRICAO, messages.get(1));
        assertEquals(CAMPO_VALIDACAO_CLASSE_PROCESSUAL, messages.get(2));
        assertEquals(CAMPO_VALIDACAO_COMARCA, messages.get(3));
        assertEquals(CAMPO_VALIDACAO_FASE, messages.get(4));
        assertEquals(CAMPO_VALIDACAO_NATUREZA_ACAO, messages.get(5));
        assertEquals(CAMPO_VALIDACAO_VARA, messages.get(6));
        assertEquals(CAMPO_VALIDACAO_FORUM, messages.get(7));
        assertEquals(CAMPO_VALIDACAO_MEIO_TRAMITACAO, messages.get(8));
    }

    @Test
    public void valida_campo_data_abertura_test(){
        this.processo.setDataAbertura(null);
        this.processo.setDescricao("bla bla bla");
        this.processo.setClasseProcessual(new ClasseProcessual());
        this.processo.setComarca(new Comarca());
        this.processo.setFase(new Fase());
        this.processo.setNaturezaAcao(new NaturezaAcao());
        this.processo.setVara(new Vara());
        this.processo.setForum(new Forum());
        this.processo.setMeioTramitacao(new MeioTramitacao());

        Set<ConstraintViolation<DadosProcessoValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(CAMPO_VALIDACAO_DATA_ABERTURA, messages.get(0));
    }

    @Test
    public void valida_campo_descricao_test(){
        this.processo.setDataAbertura(new Date());
        this.processo.setDescricao("");
        this.processo.setClasseProcessual(new ClasseProcessual());
        this.processo.setComarca(new Comarca());
        this.processo.setFase(new Fase());
        this.processo.setNaturezaAcao(new NaturezaAcao());
        this.processo.setVara(new Vara());
        this.processo.setForum(new Forum());
        this.processo.setMeioTramitacao(new MeioTramitacao());

        Set<ConstraintViolation<DadosProcessoValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(CAMPO_VALIDACAO_DESCRICAO, messages.get(0));
    }

    @Test
    public void valida_campo_classe_processual_test(){
        this.processo.setDataAbertura(new Date());
        this.processo.setDescricao("bla bla bla");
        this.processo.setClasseProcessual(null);
        this.processo.setComarca(new Comarca());
        this.processo.setFase(new Fase());
        this.processo.setNaturezaAcao(new NaturezaAcao());
        this.processo.setVara(new Vara());
        this.processo.setForum(new Forum());
        this.processo.setMeioTramitacao(new MeioTramitacao());

        Set<ConstraintViolation<DadosProcessoValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(CAMPO_VALIDACAO_CLASSE_PROCESSUAL, messages.get(0));
    }

    @Test
    public void valida_campo_comarca_test(){
        this.processo.setDataAbertura(new Date());
        this.processo.setDescricao("bla bla bla");
        this.processo.setClasseProcessual(new ClasseProcessual());
        this.processo.setComarca(null);
        this.processo.setFase(new Fase());
        this.processo.setNaturezaAcao(new NaturezaAcao());
        this.processo.setVara(new Vara());
        this.processo.setForum(new Forum());
        this.processo.setMeioTramitacao(new MeioTramitacao());

        Set<ConstraintViolation<DadosProcessoValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(CAMPO_VALIDACAO_COMARCA, messages.get(0));
    }

    @Test
    public void valida_campo_fase_test(){
        this.processo.setDataAbertura(new Date());
        this.processo.setDescricao("bla bla bla");
        this.processo.setClasseProcessual(new ClasseProcessual());
        this.processo.setComarca(new Comarca());
        this.processo.setFase(null);
        this.processo.setNaturezaAcao(new NaturezaAcao());
        this.processo.setVara(new Vara());
        this.processo.setForum(new Forum());
        this.processo.setMeioTramitacao(new MeioTramitacao());

        Set<ConstraintViolation<DadosProcessoValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(CAMPO_VALIDACAO_FASE, messages.get(0));
    }

    @Test
    public void valida_campo_natureza_acao_test(){
        this.processo.setDataAbertura(new Date());
        this.processo.setDescricao("bla bla bla");
        this.processo.setClasseProcessual(new ClasseProcessual());
        this.processo.setComarca(new Comarca());
        this.processo.setFase(new Fase());
        this.processo.setNaturezaAcao(null);
        this.processo.setVara(new Vara());
        this.processo.setForum(new Forum());
        this.processo.setMeioTramitacao(new MeioTramitacao());

        Set<ConstraintViolation<DadosProcessoValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(CAMPO_VALIDACAO_NATUREZA_ACAO, messages.get(0));
    }

    @Test
    public void valida_campo_vara_test(){
        this.processo.setDataAbertura(new Date());
        this.processo.setDescricao("bla bla bla");
        this.processo.setClasseProcessual(new ClasseProcessual());
        this.processo.setComarca(new Comarca());
        this.processo.setFase(new Fase());
        this.processo.setNaturezaAcao(new NaturezaAcao());
        this.processo.setVara(null);
        this.processo.setForum(new Forum());
        this.processo.setMeioTramitacao(new MeioTramitacao());

        Set<ConstraintViolation<DadosProcessoValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(CAMPO_VALIDACAO_VARA, messages.get(0));
    }

    @Test
    public void valida_campo_forum_test(){
        this.processo.setDataAbertura(new Date());
        this.processo.setDescricao("bla bla bla");
        this.processo.setClasseProcessual(new ClasseProcessual());
        this.processo.setComarca(new Comarca());
        this.processo.setFase(new Fase());
        this.processo.setNaturezaAcao(new NaturezaAcao());
        this.processo.setVara(new Vara());
        this.processo.setForum(null);
        this.processo.setMeioTramitacao(new MeioTramitacao());

        Set<ConstraintViolation<DadosProcessoValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(CAMPO_VALIDACAO_FORUM, messages.get(0));
    }

    @Test
    public void valida_campo_meio_tramitacao_test(){
        this.processo.setDataAbertura(new Date());
        this.processo.setDescricao("bla bla bla");
        this.processo.setClasseProcessual(new ClasseProcessual());
        this.processo.setComarca(new Comarca());
        this.processo.setFase(new Fase());
        this.processo.setNaturezaAcao(new NaturezaAcao());
        this.processo.setVara(new Vara());
        this.processo.setForum(new Forum());
        this.processo.setMeioTramitacao(null);

        Set<ConstraintViolation<DadosProcessoValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(CAMPO_VALIDACAO_MEIO_TRAMITACAO, messages.get(0));
    }

    @Test(expected = ValidationException.class)
    public void valida_excecao_classe_entidade_test() throws ValidationException {
        this.processo.validarDadosProcesso();
    }

    @Test
    public void valida_gerador_automatico_processo_test(){
        NaturezaAcao naturezaAcao = new NaturezaAcao();
        naturezaAcao.setDescricao("Criminal");

        MeioTramitacao meioTramitacao = new MeioTramitacao();
        meioTramitacao.setDescricao("Assejapar");
        meioTramitacao.setGeradorAutomatico(true);
        MeioTramitacaoNaturezaAcao meioTramitacaoNaturezaAcao = new MeioTramitacaoNaturezaAcao(meioTramitacao, naturezaAcao, 1L);
        meioTramitacao.addNaturezas(meioTramitacaoNaturezaAcao);

        this.processo.setNaturezaAcao(naturezaAcao);
        this.processo.setMeioTramitacao(meioTramitacao);
        this.processo.setDataAbertura(new Date());
        this.processo.setDescricao("123456");
        this.processo.setComarca(new Comarca());

        Set<ConstraintViolation<DadosProcessoValidacaoWrapper>> violations = getConstraintViolations();
        assertEquals(0, violations.size());
    }

    private Set<ConstraintViolation<DadosProcessoValidacaoWrapper>> getConstraintViolations() {
        return Validation.buildDefaultValidatorFactory().getValidator()
                .validate(new DadosProcessoValidacaoWrapper(this.processo));
    }
}
