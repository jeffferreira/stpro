package br.com.juridico.validation;

import br.com.juridico.dto.ParteDto;
import br.com.juridico.entidades.Processo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.junit.Assert.assertEquals;


/**
 * Created by jeffersonl2009_00 on 06/07/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class ProcessoValidationTest {

    private static Validator validator;

    @Before
    public void up() {
        MockitoAnnotations.initMocks(this);
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void valida_campos_obrigatorios(){
        Processo processo = new Processo();
        Set<ConstraintViolation<ProcessoValidacaoWrapper>> violations = validator.validate(new ProcessoValidacaoWrapper(processo, new ParteDto()));
        assertEquals(3, violations.size());
    }
}
