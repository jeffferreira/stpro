package br.com.juridico.validation.pessoas;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;

import org.junit.Before;
import org.junit.Test;

import br.com.juridico.entidades.Login;
import br.com.juridico.entidades.Perfil;
import br.com.juridico.entidades.Pessoa;
import br.com.juridico.entidades.PessoaContato;
import br.com.juridico.entidades.PessoaEmail;
import br.com.juridico.entidades.PessoaEndereco;
import br.com.juridico.entidades.PessoaFisica;
import br.com.juridico.entidades.Uf;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.util.EncryptUtil;
import br.com.juridico.validation.ValidationUtilTest;

/**
 * Created by jeffersonl2009_00 on 08/07/2016.
 */
public class AdvogadoValidationTest extends ValidationUtilTest {

    private static final String VALIDACAO_CAMPO_NOME = "Favor preencher o campo Nome.";
    private static final String VALIDACAO_CAMPO_SEXO = "Favor selecionar o campo Sexo.";
    private static final String VALIDACAO_CAMPO_ADVOGADO_INTERNO = "Favor selecionar o campo Advogado Interno.";
    private static final String VALIDACAO_CAMPO_PERFIL = "Favor selecionar um Perfil.";
    private static final String VALIDACAO_CAMPO_ENVIA_EMAIL = "Favor selecionar o campo Envia e-mail.";
    private static final String VALIDACAO_CAMPO_OAB = "Favor preencher o campo N\u00famero OAB.";
    private static final String VALIDACAO_CAMPO_UF_OAB = "Favor selecionar o campo UF OAB.";
    private static final String VALIDACAO_CAMPO_OAB_INVALIDO = "O N\u00famero de OAB fornecido j\u00e1 est\u00e1 sendo utilizado por outro advogado. Por favor, forne\u00e7a outro N\u00famero OAB.";
    private static final String VALIDACAO_CONTATO_OBRIGATORIO = "Favor adicionar pelo menos um contato.";
    private static final String VALIDACAO_EMAIL_OBRIGATORIO = "Favor adicionar pelo menos um email.";
    private static final String VALIDACAO_ENDERECO_OBRIGATORIO = "Favor adicionar pelo menos um endere\u00e7o.";
    private static final String JEFFERSON_FERREIRA = "Jefferson Ferreira";

    private PessoaFisica pessoaFisica;

    @Before
    public void up() {
        initValidation();
        this.pessoaFisica = new PessoaFisica();
        this.pessoaFisica.setPessoa(new Pessoa());
    }

    @Test
    public void todos_os_campos_estao_com_validacao_test(){
        this.pessoaFisica.setSexo(null);
        this.pessoaFisica.setAdvogadoInterno(null);
        this.pessoaFisica.getPessoa().setEnviaEmail(null);
        this.pessoaFisica.getPessoa().setCliente(null);
        this.pessoaFisica.getPessoa().setPerfil(null);
        Set<ConstraintViolation<AdvogadoValidacaoWrapper>> violations = getConstraintViolations(false);
        List<String> messages = getMensagens(violations);

        assertEquals(7, violations.size());
        assertEquals(VALIDACAO_CONTATO_OBRIGATORIO, messages.get(0));
        assertEquals(VALIDACAO_ENDERECO_OBRIGATORIO, messages.get(1));
        assertEquals(VALIDACAO_CAMPO_NOME, messages.get(2));
        assertEquals(VALIDACAO_CAMPO_OAB, messages.get(3));
        assertEquals(VALIDACAO_CAMPO_ADVOGADO_INTERNO, messages.get(4));
        assertEquals(VALIDACAO_CAMPO_SEXO, messages.get(5));
        assertEquals(VALIDACAO_CAMPO_UF_OAB, messages.get(6));

    }

    @Test(expected = ValidationException.class)
    public void valida_excecao_classe_entidade() throws ValidationException {
        this.pessoaFisica.validarAdvogado(false);
    }

    @Test
    public void campo_nome_esta_vazio_ou_nulo_test() throws Exception {
        this.pessoaFisica.setSexo('M');
        this.pessoaFisica.setAdvogadoInterno(true);
        this.pessoaFisica.getPessoa().setEnviaEmail(true);
        this.pessoaFisica.getPessoa().setCliente(false);
        this.pessoaFisica.setCpf("05121038950");
        this.pessoaFisica.getPessoa().setPerfil(new Perfil());
        this.pessoaFisica.setNrOab("123.123");
        this.pessoaFisica.setUfOab(new Uf());
        this.pessoaFisica.getPessoa().setDescricao(null);
        this.pessoaFisica.getPessoa().setLogin(new Login());
        this.pessoaFisica.getPessoa().getLogin().setLogin("login");
        this.pessoaFisica.getPessoa().getLogin().setSenha(EncryptUtil.execute("senha"));
        this.pessoaFisica.getPessoa().getPessoasEmails().add(new PessoaEmail());
        this.pessoaFisica.getPessoa().getPessoasContatos().add(new PessoaContato());
        Set<ConstraintViolation<AdvogadoValidacaoWrapper>> violations = getConstraintViolations(false);
        List<String> messages = getMensagens(violations);
        assertEquals(2, violations.size());
        assertEquals(VALIDACAO_ENDERECO_OBRIGATORIO, messages.get(0));
        assertEquals(VALIDACAO_CAMPO_NOME, messages.get(1));
    }

    @Test
    public void campo_sexo_esta_nulo_test() throws Exception {
        this.pessoaFisica.setSexo(null);
        this.pessoaFisica.setAdvogadoInterno(true);
        this.pessoaFisica.getPessoa().setEnviaEmail(true);
        this.pessoaFisica.getPessoa().setCliente(false);
        this.pessoaFisica.setCpf("05121038950");
        this.pessoaFisica.getPessoa().setPerfil(new Perfil());
        this.pessoaFisica.setNrOab("123.123");
        this.pessoaFisica.setUfOab(new Uf());
        this.pessoaFisica.getPessoa().setDescricao(JEFFERSON_FERREIRA);
        this.pessoaFisica.getPessoa().getPessoasEmails().add(new PessoaEmail());
        this.pessoaFisica.getPessoa().setLogin(new Login());
        this.pessoaFisica.getPessoa().getLogin().setLogin("login");
        this.pessoaFisica.getPessoa().getLogin().setSenha(EncryptUtil.execute("senha"));
        this.pessoaFisica.getPessoa().getPessoasEmails().add(new PessoaEmail());
        this.pessoaFisica.getPessoa().getPessoasContatos().add(new PessoaContato());
        this.pessoaFisica.getPessoa().getPessoasEnderecos().add(new PessoaEndereco());
        Set<ConstraintViolation<AdvogadoValidacaoWrapper>> violations = getConstraintViolations(false);
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(VALIDACAO_CAMPO_SEXO, messages.get(0));
    }

    @Test
    public void campo_advogado_interno_esta_nulo(){
        this.pessoaFisica.setSexo('M');
        this.pessoaFisica.setAdvogadoInterno(null);
        this.pessoaFisica.getPessoa().setEnviaEmail(true);
        this.pessoaFisica.getPessoa().setCliente(false);
        this.pessoaFisica.getPessoa().setPerfil(new Perfil());
        this.pessoaFisica.setNrOab("123.123");
        this.pessoaFisica.setUfOab(new Uf());
        this.pessoaFisica.getPessoa().setDescricao(JEFFERSON_FERREIRA);
        this.pessoaFisica.getPessoa().getPessoasEmails().add(new PessoaEmail());
        this.pessoaFisica.getPessoa().getPessoasContatos().add(new PessoaContato());
        this.pessoaFisica.getPessoa().getPessoasEnderecos().add(new PessoaEndereco());
        Set<ConstraintViolation<AdvogadoValidacaoWrapper>> violations = getConstraintViolations(false);
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(VALIDACAO_CAMPO_ADVOGADO_INTERNO, messages.get(0));
    }

    @Test
    public void campo_numero_oab_esta_nulo_test() throws Exception {
        this.pessoaFisica.setSexo('M');
        this.pessoaFisica.setAdvogadoInterno(true);
        this.pessoaFisica.getPessoa().setEnviaEmail(true);
        this.pessoaFisica.getPessoa().setCliente(false);
        this.pessoaFisica.setCpf("05121038950");
        this.pessoaFisica.getPessoa().setPerfil(new Perfil());
        this.pessoaFisica.setNrOab(null);
        this.pessoaFisica.setUfOab(new Uf());
        this.pessoaFisica.getPessoa().setDescricao(JEFFERSON_FERREIRA);
        this.pessoaFisica.getPessoa().getPessoasEmails().add(new PessoaEmail());
        this.pessoaFisica.getPessoa().setLogin(new Login());
        this.pessoaFisica.getPessoa().getLogin().setLogin("login");
        this.pessoaFisica.getPessoa().getLogin().setSenha(EncryptUtil.execute("senha"));
        this.pessoaFisica.getPessoa().getPessoasEmails().add(new PessoaEmail());
        this.pessoaFisica.getPessoa().getPessoasContatos().add(new PessoaContato());
        this.pessoaFisica.getPessoa().getPessoasEnderecos().add(new PessoaEndereco());
        Set<ConstraintViolation<AdvogadoValidacaoWrapper>> violations = getConstraintViolations(false);
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(VALIDACAO_CAMPO_OAB, messages.get(0));
    }

    @Test
    public void campo_uf_oab_esta_nulo_test() throws Exception {
        this.pessoaFisica.setSexo('M');
        this.pessoaFisica.setAdvogadoInterno(true);
        this.pessoaFisica.getPessoa().setEnviaEmail(true);
        this.pessoaFisica.getPessoa().setCliente(false);
        this.pessoaFisica.setCpf("05121038950");
        this.pessoaFisica.getPessoa().setPerfil(new Perfil());
        this.pessoaFisica.setNrOab("123.123");
        this.pessoaFisica.setUfOab(null);
        this.pessoaFisica.getPessoa().setDescricao(JEFFERSON_FERREIRA);
        this.pessoaFisica.getPessoa().getPessoasEmails().add(new PessoaEmail());
        this.pessoaFisica.getPessoa().setLogin(new Login());
        this.pessoaFisica.getPessoa().getLogin().setLogin("login");
        this.pessoaFisica.getPessoa().getLogin().setSenha(EncryptUtil.execute("senha"));
        this.pessoaFisica.getPessoa().getPessoasEmails().add(new PessoaEmail());
        this.pessoaFisica.getPessoa().getPessoasContatos().add(new PessoaContato());
        this.pessoaFisica.getPessoa().getPessoasEnderecos().add(new PessoaEndereco());
        Set<ConstraintViolation<AdvogadoValidacaoWrapper>> violations = getConstraintViolations(false);
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(VALIDACAO_CAMPO_UF_OAB, messages.get(0));
    }

    @Test
    public void campo_perfil_esta_nulo() throws Exception {
        this.pessoaFisica.setSexo('M');
        this.pessoaFisica.setAdvogadoInterno(true);
        this.pessoaFisica.getPessoa().setEnviaEmail(true);
        this.pessoaFisica.getPessoa().setCliente(false);
        this.pessoaFisica.setCpf("05121038950");
        this.pessoaFisica.getPessoa().setPerfil(null);
        this.pessoaFisica.setNrOab("123.123");
        this.pessoaFisica.setUfOab(new Uf());
        this.pessoaFisica.getPessoa().setDescricao(JEFFERSON_FERREIRA);
        this.pessoaFisica.getPessoa().getPessoasEmails().add(new PessoaEmail());
        this.pessoaFisica.getPessoa().setLogin(new Login());
        this.pessoaFisica.getPessoa().getLogin().setLogin("login");
        this.pessoaFisica.getPessoa().getLogin().setSenha(EncryptUtil.execute("senha"));
        this.pessoaFisica.getPessoa().getPessoasEmails().add(new PessoaEmail());
        this.pessoaFisica.getPessoa().getPessoasContatos().add(new PessoaContato());
        this.pessoaFisica.getPessoa().getPessoasEnderecos().add(new PessoaEndereco());
        Set<ConstraintViolation<AdvogadoValidacaoWrapper>> violations = getConstraintViolations(false);
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(VALIDACAO_CAMPO_PERFIL, messages.get(0));
    }

    @Test
    public void campo_envia_email_obrigatorio_e_esta_nulo() throws Exception {
        this.pessoaFisica.setSexo('M');
        this.pessoaFisica.setAdvogadoInterno(true);
        this.pessoaFisica.getPessoa().setEnviaEmail(null);
        this.pessoaFisica.getPessoa().setCliente(false);
        this.pessoaFisica.setCpf("05121038950");
        this.pessoaFisica.getPessoa().setPerfil(new Perfil());
        this.pessoaFisica.setNrOab("123.123");
        this.pessoaFisica.setUfOab(new Uf());
        this.pessoaFisica.getPessoa().setDescricao(JEFFERSON_FERREIRA);
        this.pessoaFisica.getPessoa().getPessoasEmails().add(new PessoaEmail());
        this.pessoaFisica.getPessoa().setLogin(new Login());
        this.pessoaFisica.getPessoa().getLogin().setLogin("login");
        this.pessoaFisica.getPessoa().getLogin().setSenha(EncryptUtil.execute("senha"));
        this.pessoaFisica.getPessoa().getPessoasEmails().add(new PessoaEmail());
        this.pessoaFisica.getPessoa().getPessoasContatos().add(new PessoaContato());
        this.pessoaFisica.getPessoa().getPessoasEnderecos().add(new PessoaEndereco());
        Set<ConstraintViolation<AdvogadoValidacaoWrapper>> violations = getConstraintViolations(false);
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(VALIDACAO_CAMPO_ENVIA_EMAIL, messages.get(0));
    }

    @Test
    public void numero_oab_ja_existe_test() throws Exception {
        this.pessoaFisica.setSexo('M');
        this.pessoaFisica.setAdvogadoInterno(true);
        this.pessoaFisica.getPessoa().setEnviaEmail(true);
        this.pessoaFisica.getPessoa().setCliente(false);
        this.pessoaFisica.setCpf("05121038950");
        this.pessoaFisica.getPessoa().setPerfil(new Perfil());
        this.pessoaFisica.setNrOab("123.123");
        this.pessoaFisica.setUfOab(new Uf());
        this.pessoaFisica.getPessoa().setDescricao(JEFFERSON_FERREIRA);
        this.pessoaFisica.getPessoa().getPessoasEmails().add(new PessoaEmail());
        this.pessoaFisica.getPessoa().setLogin(new Login());
        this.pessoaFisica.getPessoa().getLogin().setLogin("login");
        this.pessoaFisica.getPessoa().getLogin().setSenha(EncryptUtil.execute("senha"));
        this.pessoaFisica.getPessoa().getPessoasEmails().add(new PessoaEmail());
        this.pessoaFisica.getPessoa().getPessoasContatos().add(new PessoaContato());
        this.pessoaFisica.getPessoa().getPessoasEnderecos().add(new PessoaEndereco());

        Set<ConstraintViolation<AdvogadoValidacaoWrapper>> violations = getConstraintViolations(true);
        List<String> messages = getMensagens(violations);
        assertEquals(VALIDACAO_CAMPO_OAB_INVALIDO, messages.get(0));
    }

    @Test
    public void email_contato_endereco_eh_obrigatorio_test() throws Exception{
        this.pessoaFisica.setSexo('M');
        this.pessoaFisica.setAdvogadoInterno(true);
        this.pessoaFisica.getPessoa().setEnviaEmail(true);
        this.pessoaFisica.getPessoa().setCliente(false);
        this.pessoaFisica.getPessoa().setPerfil(new Perfil());
        this.pessoaFisica.setNrOab("123.123");
        this.pessoaFisica.setUfOab(new Uf());
        this.pessoaFisica.getPessoa().setDescricao(JEFFERSON_FERREIRA);
        this.pessoaFisica.getPessoa().setLogin(new Login());
        this.pessoaFisica.getPessoa().getLogin().setLogin("login");
        this.pessoaFisica.getPessoa().getLogin().setSenha(EncryptUtil.execute("senha"));

        Set<ConstraintViolation<AdvogadoValidacaoWrapper>> violations = getConstraintViolations(false);
        List<String> messages = getMensagens(violations);
        assertEquals(VALIDACAO_CONTATO_OBRIGATORIO, messages.get(0));
        assertEquals(VALIDACAO_EMAIL_OBRIGATORIO, messages.get(1));
        assertEquals(VALIDACAO_ENDERECO_OBRIGATORIO, messages.get(2));
    }

    @Test
    public void todos_os_campos_foram_preenchidos_test() throws Exception {
        this.pessoaFisica.setSexo('M');
        this.pessoaFisica.setAdvogadoInterno(true);
        this.pessoaFisica.getPessoa().setEnviaEmail(true);
        this.pessoaFisica.getPessoa().setCliente(false);
        this.pessoaFisica.setCpf("05121038950");
        this.pessoaFisica.getPessoa().setPerfil(new Perfil());
        this.pessoaFisica.setNrOab("123.123");
        this.pessoaFisica.setUfOab(new Uf());
        this.pessoaFisica.getPessoa().setDescricao(JEFFERSON_FERREIRA);
        this.pessoaFisica.getPessoa().getPessoasEmails().add(new PessoaEmail());
        this.pessoaFisica.getPessoa().getPessoasContatos().add(new PessoaContato());
        this.pessoaFisica.getPessoa().getPessoasEnderecos().add(new PessoaEndereco());
        this.pessoaFisica.getPessoa().setLogin(new Login());
        this.pessoaFisica.getPessoa().getLogin().setLogin("login");
        this.pessoaFisica.getPessoa().getLogin().setSenha(EncryptUtil.execute("senha"));

        Set<ConstraintViolation<AdvogadoValidacaoWrapper>> violations = getConstraintViolations(false);
        List<String> messages = getMensagens(violations);

        assertEquals(0, messages.size());
        assertEquals(0, violations.size());
    }

    private Set<ConstraintViolation<AdvogadoValidacaoWrapper>> getConstraintViolations(boolean existeNumeroOabCadastrado) {
        return Validation.buildDefaultValidatorFactory().getValidator()
                .validate(new AdvogadoValidacaoWrapper(this.pessoaFisica, existeNumeroOabCadastrado));
    }
}
