package br.com.juridico.validation.processo;

import br.com.juridico.entidades.Processo;
import br.com.juridico.validation.ValidationUtilTest;
import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * Created by jeffersonl2009_00 on 08/07/2016.
 */
public class ResumoAcaoValidationTest extends ValidationUtilTest {

    private static final String CAMPO_VALIDACAO_RESUMO_ACAO = "Favor preencher o resumo da ação.";

    private Processo processo;

    @Before
    public void up() {
        initValidation();
        this.processo = new Processo();
    }

    @Test
    public void campo_resumo_acao_vazio_test(){
        Set<ConstraintViolation<ResumoValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(CAMPO_VALIDACAO_RESUMO_ACAO, messages.get(0));

    }

    private Set<ConstraintViolation<ResumoValidacaoWrapper>> getConstraintViolations() {
        return Validation.buildDefaultValidatorFactory().getValidator()
                .validate(new ResumoValidacaoWrapper(this.processo));
    }

}
