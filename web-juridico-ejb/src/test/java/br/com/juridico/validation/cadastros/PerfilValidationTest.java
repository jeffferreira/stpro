package br.com.juridico.validation.cadastros;

import br.com.juridico.entidades.Perfil;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.ValidationUtilTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import javax.validation.ConstraintViolation;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * Created by jeffersonl2009_00 on 06/07/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class PerfilValidationTest extends ValidationUtilTest {

    private static final String SIGLA_PREENCHIMENTO_OBRIGATORIO = "Sigla: preenchimento obrigatório";

    @Before
    public void up() {
        initValidation();
    }

    @Test
    public void campo_descricao_vazio(){
        Perfil c = new Perfil();
        c.setDescricao("");
        c.setSigla("bla bla bla");
        Set<ConstraintViolation<Perfil>> violations = validator.validate(c);
        List<String> messages = getMensagens(violations);

        assertEquals(1, violations.size());
        assertEquals(DESCRICAO_PREENCHIMENTO_OBRIGATORIO, messages.get(0));

    }

    @Test
    public void campo_sigla_vazio(){
        Perfil c = new Perfil();
        c.setDescricao("bla bla bla");
        c.setSigla("");
        Set<ConstraintViolation<Perfil>> violations = validator.validate(c);
        List<String> messages = getMensagens(violations);

        assertEquals(1, violations.size());
        assertEquals(SIGLA_PREENCHIMENTO_OBRIGATORIO, messages.get(0));

    }

    @Test
    public void campo_descricao_nulo(){
        Perfil c = new Perfil();
        c.setSigla("bla bla");
        Set<ConstraintViolation<Perfil>> violations = validator.validate(c);
        List<String> messages = getMensagens(violations);

        assertEquals(2, violations.size());
        assertEquals(DESCRICAO_PREENCHIMENTO_OBRIGATORIO, messages.get(0));
        assertEquals(NAO_PODE_SER_NULO, messages.get(1));

    }

    @Test
    public void campo_sigla_nulo(){
        Perfil c = new Perfil();
        c.setDescricao("bla bla");
        Set<ConstraintViolation<Perfil>> violations = validator.validate(c);
        List<String> messages = getMensagens(violations);

        assertEquals(2, violations.size());
        assertEquals(SIGLA_PREENCHIMENTO_OBRIGATORIO, messages.get(0));
        assertEquals(NAO_PODE_SER_NULO, messages.get(1));

    }

    @Test
    public void campo_descricao_e_sigla_preenchidos(){
        Perfil c = new Perfil();
        c.setDescricao("bla bla bla");
        c.setSigla("bla bla bla");
        Set<ConstraintViolation<Perfil>> violations = validator.validate(c);
        assertEquals(0, violations.size());
    }

    @Test(expected = ValidationException.class)
    public void campo_descricao_duplicado_test() throws ValidationException {
        Perfil c = new Perfil();
        c.validar(true, false);
    }

    @Test(expected = ValidationException.class)
    public void campo_sigla_duplicado_test() throws ValidationException {
        Perfil c = new Perfil();
        c.validar(false, true);
    }

    @Test(expected = ValidationException.class)
    public void campo_sigla_e_descricao_duplicado_test() throws ValidationException {
        Perfil c = new Perfil();
        c.validar(true, true);
    }

    @Test
    public void campo_sigla_e_descricao_validos_test() throws ValidationException {
        Perfil c = new Perfil();
        c.validar(false, false);
    }

}
