package br.com.juridico.validation.processo;

import br.com.juridico.validation.ValidationUtilTest;
import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * Created by jeffersonl2009_00 on 08/07/2016.
 */
public class ProcessoExistenteValidationTest extends ValidationUtilTest {

    private static final String CAMPO_VALIDACAO_PROCESSO_EXISTENTE = "Já existe um processo cadastrado com este número.";

    private Boolean existeProcessoCadastrado;

    @Before
    public void up() {
        initValidation();
    }

    @Test
    public void processo_ja_existe_test(){
        existeProcessoCadastrado = Boolean.TRUE;
        Set<ConstraintViolation<ProcessoExistenteWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(CAMPO_VALIDACAO_PROCESSO_EXISTENTE, messages.get(0));
    }

    @Test
    public void processo_nao_existe_test(){
        existeProcessoCadastrado = Boolean.FALSE;
        Set<ConstraintViolation<ProcessoExistenteWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(0, violations.size());
    }

    private Set<ConstraintViolation<ProcessoExistenteWrapper>> getConstraintViolations() {
        return Validation.buildDefaultValidatorFactory().getValidator()
                .validate(new ProcessoExistenteWrapper(this.existeProcessoCadastrado));
    }

}
