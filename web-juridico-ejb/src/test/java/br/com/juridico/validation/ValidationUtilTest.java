package br.com.juridico.validation;

import br.com.juridico.dao.IEntity;
import com.google.common.collect.Lists;
import org.mockito.MockitoAnnotations;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * Created by jeffersonl2009_00 on 06/07/2016.
 */
public abstract class ValidationUtilTest<T extends IEntity> {

    protected static final String DESCRICAO_PREENCHIMENTO_OBRIGATORIO = "Descrição: preenchimento obrigatório";
    protected static final String NAO_PODE_SER_NULO = "não pode ser nulo";

    protected static Validator validator;

    protected void initValidation() {
        MockitoAnnotations.initMocks(this);
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    protected List<String> getMensagens(Set<ConstraintViolation<T>> violations){
        List<String> messages = Lists.newArrayList();

        for (ConstraintViolation<T> violation : violations) {
            messages.add(violation.getMessage());
        }

        Collections.sort(messages);
        return messages;
    }

    protected Validator buildValidatorFactory(){
        return Validation.buildDefaultValidatorFactory().getValidator();
    }
}
