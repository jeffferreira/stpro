package br.com.juridico.validation.cadastros;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import br.com.juridico.entidades.StatusAgenda;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.ValidationUtilTest;

/**
 * Created by jeffersonl2009_00 on 06/07/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class StatusAgendalValidationTest extends ValidationUtilTest {

    @Before
    public void up() {
        initValidation();
    }

    @Test
    public void campo_descricao_vazio(){
        StatusAgenda c = new StatusAgenda();
        c.setDescricao("");
        Set<ConstraintViolation<StatusAgenda>> violations = validator.validate(c);
        List<String> messages = getMensagens(violations);

        assertEquals(1, violations.size());
        assertEquals(DESCRICAO_PREENCHIMENTO_OBRIGATORIO, messages.get(0));

    }

    @Test
    public void campo_descricao_nulo(){
        Set<ConstraintViolation<StatusAgenda>> violations = validator.validate(new StatusAgenda());
        List<String> messages = getMensagens(violations);

        assertEquals(2, violations.size());
        assertEquals(DESCRICAO_PREENCHIMENTO_OBRIGATORIO, messages.get(0));
        assertEquals(NAO_PODE_SER_NULO, messages.get(1));

    }

    @Test
    public void campo_descricao_preenchido(){
    	StatusAgenda c = new StatusAgenda();
        c.setDescricao("bla bla bla");
        Set<ConstraintViolation<StatusAgenda>> violations = validator.validate(c);
        assertEquals(0, violations.size());
    }

    @Test(expected = ValidationException.class)
    public void campo_duplicado_test() throws ValidationException {
    	StatusAgenda c = new StatusAgenda();
        c.validar(true);
    }
}
