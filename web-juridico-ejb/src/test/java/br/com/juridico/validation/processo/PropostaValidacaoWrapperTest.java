package br.com.juridico.validation.processo;

import br.com.juridico.entidades.HistoricoProposta;
import br.com.juridico.entidades.Pessoa;
import br.com.juridico.entidades.Processo;
import br.com.juridico.validation.ValidationUtilTest;
import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * Created by jefferson.ferreira on 18/01/2017.
 */
public class PropostaValidacaoWrapperTest extends ValidationUtilTest {

    private static final String CAMPO_VALIDACAO_DATA_PROPOSTA = "Favor preencher a data da proposta.";
    private static final String CAMPO_VALIDACAO_DATA_FIM_PRAZO = "Favor preencher a data final do prazo.";
    private static final String CAMPO_VALIDACAO_PARTE = "Favor preencher o proponente.";
    private static final String CAMPO_VALIDACAO_VALOR = "Favor preencher o valor.";

    private HistoricoProposta proposta;

    @Before
    public void up() {
        initValidation();
        this.proposta = new HistoricoProposta(new Processo(), 1L);
    }

    @Test
    public void todos_os_campos_vazios_test(){
        Set<ConstraintViolation<PropostaValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(4, violations.size());
        assertEquals(CAMPO_VALIDACAO_DATA_PROPOSTA, messages.get(0));
        assertEquals(CAMPO_VALIDACAO_DATA_FIM_PRAZO, messages.get(1));
        assertEquals(CAMPO_VALIDACAO_PARTE, messages.get(2));
        assertEquals(CAMPO_VALIDACAO_VALOR, messages.get(3));
    }

    @Test
    public void campo_proposta_data_vazia_test(){
        this.proposta.setPessoa(new Pessoa());
        this.proposta.setValorProposta(new BigDecimal("100"));
        this.proposta.setDataFimPrazo(new Date());
        Set<ConstraintViolation<PropostaValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(CAMPO_VALIDACAO_DATA_PROPOSTA, messages.get(0));
    }

    @Test
    public void campo_proposta_data_fim_prazo_test(){
        this.proposta.setPessoa(new Pessoa());
        this.proposta.setValorProposta(new BigDecimal("100"));
        this.proposta.setDataProposta(new Date());
        Set<ConstraintViolation<PropostaValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(CAMPO_VALIDACAO_DATA_FIM_PRAZO, messages.get(0));
    }

    @Test
    public void campo_proposta_parte_vazia_test(){
        this.proposta.setDataProposta(new Date());
        this.proposta.setValorProposta(new BigDecimal("100"));
        this.proposta.setDataFimPrazo(new Date());
        Set<ConstraintViolation<PropostaValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(CAMPO_VALIDACAO_PARTE, messages.get(0));
    }

    @Test
    public void campo_proposta_valor_vazio_test(){
        this.proposta.setDataProposta(new Date());
        this.proposta.setPessoa(new Pessoa());
        this.proposta.setDataFimPrazo(new Date());
        Set<ConstraintViolation<PropostaValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(CAMPO_VALIDACAO_VALOR, messages.get(0));
    }

    private Set<ConstraintViolation<PropostaValidacaoWrapper>> getConstraintViolations() {
        return Validation.buildDefaultValidatorFactory().getValidator()
                .validate(new PropostaValidacaoWrapper(this.proposta));
    }

}