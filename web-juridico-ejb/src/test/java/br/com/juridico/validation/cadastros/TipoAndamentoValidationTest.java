package br.com.juridico.validation.cadastros;

import br.com.juridico.entidades.TipoAndamento;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.ValidationUtilTest;
import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * Created by jeffersonl2009_00 on 06/07/2016.
 */
public class TipoAndamentoValidationTest extends ValidationUtilTest {

    @Before
    public void up() {
        initValidation();
    }

    @Test
    public void campo_descricao_vazio() {
        TipoAndamento c = new TipoAndamento();
        c.setDescricao("");
        Set<ConstraintViolation<TipoAndamento>> violations = validator.validate(c);
        List<String> messages = getMensagens(violations);

        assertEquals(1, violations.size());
        assertEquals(DESCRICAO_PREENCHIMENTO_OBRIGATORIO, messages.get(0));

    }

    @Test
    public void campo_descricao_nulo() {
        Set<ConstraintViolation<TipoAndamento>> violations = validator.validate(new TipoAndamento());
        List<String> messages = getMensagens(violations);

        assertEquals(2, violations.size());
        assertEquals(DESCRICAO_PREENCHIMENTO_OBRIGATORIO, messages.get(0));
        assertEquals(NAO_PODE_SER_NULO, messages.get(1));

    }

    @Test
    public void campo_descricao_preenchido() {
        TipoAndamento c = new TipoAndamento();
        c.setDescricao("bla bla bla");
        Set<ConstraintViolation<TipoAndamento>> violations = validator.validate(c);
        assertEquals(0, violations.size());
    }

    @Test(expected = ValidationException.class)
    public void campo_duplicado_test() throws ValidationException {
        TipoAndamento c = new TipoAndamento();
        c.validar(true);
    }
}
