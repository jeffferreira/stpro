package br.com.juridico.validation.pessoas;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;

import org.junit.Before;
import org.junit.Test;

import br.com.juridico.entidades.Login;
import br.com.juridico.entidades.Pessoa;
import br.com.juridico.entidades.PessoaContato;
import br.com.juridico.entidades.PessoaEmail;
import br.com.juridico.entidades.PessoaEndereco;
import br.com.juridico.entidades.PessoaJuridica;
import br.com.juridico.entidades.PessoaJuridicaCentroCusto;
import br.com.juridico.entidades.RamoAtividade;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.util.EncryptUtil;
import br.com.juridico.validation.ValidationUtilTest;

/**
 * Created by Jefferson on 08/07/2016.
 */
public class PessoaJuridicaValidationTest extends ValidationUtilTest {

	private static final String VALIDACAO_CAMPO_RAZAO_SOCIAL = "Favor preencher a razão social.";
    private static final String VALIDACAO_CAMPO_ENVIA_EMAIL = "Favor selecionar o campo Envia e-mail.";
    private static final String VALIDACAO_CAMPO_CLIENTE = "Favor selecionar o campo Cliente.";
    private static final String VALIDACAO_CNPJ_OBRIGATORIO = "Favor informar o CNPJ.";
    private static final String VALIDACAO_ATIVIDADE_PRINCIPAL_OBRIGATORIA = "Favor informar a Atividade Principal.";
    private static final String VALIDACAO_CONTATO_OBRIGATORIO = "Favor adicionar pelo menos um contato.";
    private static final String VALIDACAO_EMAIL_OBRIGATORIO = "Favor adicionar pelo menos um email.";
    private static final String VALIDACAO_ENDERECO_OBRIGATORIO = "Favor adicionar pelo menos um endere\u00e7o.";
    private static final String VALIDACAO_CENTRO_CUSTO_OBRIGATORIO = "Por favor, selecione um centro de custo.";
    private static final String JEFFERSON_FERREIRA = "Jefferson Ferreira";

    private PessoaJuridica pessoaJuridica;

    @Before
    public void up() {
        initValidation();
        this.pessoaJuridica = new PessoaJuridica();
        this.pessoaJuridica.setPessoa(new Pessoa());
    }

    @Test
    public void todos_os_campos_estao_com_validacao_test(){
        this.pessoaJuridica.getPessoa().setEnviaEmail(null);
        this.pessoaJuridica.getPessoa().setCliente(null);
        Set<ConstraintViolation<PessoaJuridicaValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(3, violations.size());
        assertEquals(VALIDACAO_CAMPO_RAZAO_SOCIAL, messages.get(0));
        assertEquals(VALIDACAO_CAMPO_CLIENTE, messages.get(1));
        assertEquals(VALIDACAO_CAMPO_ENVIA_EMAIL, messages.get(2));
    }

    @Test(expected = ValidationException.class)
    public void valida_excecao_classe_entidade() throws ValidationException {
        this.pessoaJuridica.validar();
    }

    @Test
    public void centro_custo_pessoa_fisica_vazia_ou_nula_test(){
        this.pessoaJuridica.getPessoa().setEnviaEmail(true);
        this.pessoaJuridica.getPessoa().setCliente(false);
        this.pessoaJuridica.getPessoa().setDescricao(JEFFERSON_FERREIRA);
        this.pessoaJuridica.getPessoasJuridicasCentrosCustos().add(new PessoaJuridicaCentroCusto());
        Set<ConstraintViolation<PessoaJuridicaValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(VALIDACAO_CENTRO_CUSTO_OBRIGATORIO, messages.get(0));
    }

    @Test
    public void campo_razao_social_esta_vazio_ou_nulo_test(){
        this.pessoaJuridica.getPessoa().setCliente(false);
        Set<ConstraintViolation<PessoaJuridicaValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(VALIDACAO_CAMPO_RAZAO_SOCIAL, messages.get(0));
    }
    
    @Test
    public void campo_cnpj_esta_vazio_ou_nulo_test() throws Exception {
        this.pessoaJuridica.getPessoa().setCliente(true);
        this.pessoaJuridica.getPessoa().setDescricao(JEFFERSON_FERREIRA);
        this.pessoaJuridica.getPessoa().setLogin(new Login());
        this.pessoaJuridica.getPessoa().getLogin().setLogin("login");
        this.pessoaJuridica.setRamoAtividade(new RamoAtividade());
        this.pessoaJuridica.getPessoa().getLogin().setSenha(EncryptUtil.execute("senha"));
        this.pessoaJuridica.getPessoa().getPessoasContatos().add(new PessoaContato());
        this.pessoaJuridica.getPessoa().getPessoasEnderecos().add(new PessoaEndereco());
        this.pessoaJuridica.getPessoa().getPessoasEmails().add(new PessoaEmail());
        Set<ConstraintViolation<PessoaJuridicaValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(VALIDACAO_CNPJ_OBRIGATORIO, messages.get(0));
    }
    
    @Test
    public void campo_atividade_principal_esta_vazio_ou_nulo_test() throws Exception {
        this.pessoaJuridica.getPessoa().setCliente(true);
        this.pessoaJuridica.getPessoa().setDescricao(JEFFERSON_FERREIRA);
        this.pessoaJuridica.getPessoa().setLogin(new Login());
        this.pessoaJuridica.getPessoa().getLogin().setLogin("login");
        this.pessoaJuridica.getPessoa().getLogin().setSenha(EncryptUtil.execute("senha"));
        this.pessoaJuridica.getPessoa().getPessoasContatos().add(new PessoaContato());
        this.pessoaJuridica.getPessoa().getPessoasEnderecos().add(new PessoaEndereco());
        this.pessoaJuridica.getPessoa().getPessoasEmails().add(new PessoaEmail());
        this.pessoaJuridica.setCnpj("99.999.999/9999-99");
        Set<ConstraintViolation<PessoaJuridicaValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(VALIDACAO_ATIVIDADE_PRINCIPAL_OBRIGATORIA, messages.get(0));
    }

    @Test
    public void campo_envia_email_esta_nulo_test(){
        this.pessoaJuridica.getPessoa().setEnviaEmail(null);
        this.pessoaJuridica.getPessoa().setCliente(false);
        this.pessoaJuridica.getPessoa().setDescricao(JEFFERSON_FERREIRA);
        Set<ConstraintViolation<PessoaJuridicaValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(VALIDACAO_CAMPO_ENVIA_EMAIL, messages.get(0));
    }

    @Test
    public void campo_eh_cliente_nulo_test(){
        this.pessoaJuridica.getPessoa().setCliente(null);
        this.pessoaJuridica.getPessoa().setDescricao(JEFFERSON_FERREIRA);
        Set<ConstraintViolation<PessoaJuridicaValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(VALIDACAO_CAMPO_CLIENTE, messages.get(0));
    }

    @Test
    public void todos_os_campos_foram_preenchidos_test(){
        this.pessoaJuridica.getPessoa().setEnviaEmail(true);
        this.pessoaJuridica.getPessoa().setCliente(false);
        this.pessoaJuridica.getPessoa().setDescricao(JEFFERSON_FERREIRA);
        Set<ConstraintViolation<PessoaJuridicaValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(0, violations.size());
    }

    @Test
    public void eh_obrigatorio_preencher_email_contato_endereco() throws Exception {
        this.pessoaJuridica.getPessoa().setEnviaEmail(true);
        this.pessoaJuridica.getPessoa().setCliente(true);
        this.pessoaJuridica.getPessoa().setDescricao(JEFFERSON_FERREIRA);
        this.pessoaJuridica.setCnpj("99.999.999/9999-99");
        this.pessoaJuridica.getPessoa().setLogin(new Login());
        this.pessoaJuridica.setRamoAtividade(new RamoAtividade());
        this.pessoaJuridica.getPessoa().getLogin().setLogin("login");
        this.pessoaJuridica.getPessoa().getLogin().setSenha(EncryptUtil.execute("senha"));
        Set<ConstraintViolation<PessoaJuridicaValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(3, violations.size());
        assertEquals(VALIDACAO_CONTATO_OBRIGATORIO, messages.get(0));
        assertEquals(VALIDACAO_EMAIL_OBRIGATORIO, messages.get(1));
        assertEquals(VALIDACAO_ENDERECO_OBRIGATORIO, messages.get(2));
    }

    @Test
    public void campo_email_possui_validacao_test() throws Exception {
        this.pessoaJuridica.getPessoa().setEnviaEmail(true);
        this.pessoaJuridica.getPessoa().setCliente(true);
        this.pessoaJuridica.getPessoa().setDescricao(JEFFERSON_FERREIRA);
        this.pessoaJuridica.setCnpj("99.999.999/9999-99");
        this.pessoaJuridica.setRamoAtividade(new RamoAtividade());
        this.pessoaJuridica.getPessoa().getPessoasEmails().add(new PessoaEmail());
        this.pessoaJuridica.getPessoa().setLogin(new Login());
        this.pessoaJuridica.getPessoa().getLogin().setLogin("login");
        this.pessoaJuridica.getPessoa().getLogin().setSenha(EncryptUtil.execute("senha"));
        this.pessoaJuridica.getPessoa().getPessoasContatos().add(new PessoaContato());
        this.pessoaJuridica.getPessoa().getPessoasEnderecos().add(new PessoaEndereco());
        Set<ConstraintViolation<PessoaJuridicaValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(0, violations.size());
        assertEquals(0, messages.size());
    }

    private Set<ConstraintViolation<PessoaJuridicaValidacaoWrapper>> getConstraintViolations() {
        return Validation.buildDefaultValidatorFactory().getValidator()
                .validate(new PessoaJuridicaValidacaoWrapper(this.pessoaJuridica));
    }
}
