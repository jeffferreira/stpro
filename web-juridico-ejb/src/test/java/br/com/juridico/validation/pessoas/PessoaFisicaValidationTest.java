package br.com.juridico.validation.pessoas;

import br.com.juridico.entidades.*;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.util.EncryptUtil;
import br.com.juridico.validation.ValidationUtilTest;
import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * Created by jeffersonl2009_00 on 07/07/2016.
 */
public class PessoaFisicaValidationTest extends ValidationUtilTest {

    private static final String VALIDACAO_CAMPO_NOME = "Favor preencher o campo Nome.";
    private static final String VALIDACAO_CAMPO_SEXO = "Favor selecionar o campo Sexo.";
    private static final String VALIDACAO_CAMPO_ENVIA_EMAIL = "Favor selecionar o campo Envia e-mail.";
    private static final String VALIDACAO_CAMPO_CLIENTE = "Favor selecionar o campo Cliente.";
    private static final String VALIDACAO_CONTATO_OBRIGATORIO = "Favor adicionar pelo menos um contato.";
    private static final String VALIDACAO_EMAIL_OBRIGATORIO = "Favor adicionar pelo menos um email.";
    private static final String VALIDACAO_ENDERECO_OBRIGATORIO = "Favor adicionar pelo menos um endere\u00e7o.";
    private static final String JEFFERSON_FERREIRA = "Jefferson Ferreira";

    private PessoaFisica pessoaFisica;

    @Before
    public void up() {
        initValidation();
        this.pessoaFisica = new PessoaFisica();
        this.pessoaFisica.setPessoa(new Pessoa());
    }

    @Test
    public void todos_os_campos_estao_com_validacao_test(){
        this.pessoaFisica.setSexo(null);
        this.pessoaFisica.getPessoa().setEnviaEmail(null);
        this.pessoaFisica.getPessoa().setCliente(null);
        Set<ConstraintViolation<PessoaFisicaValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);

        assertEquals(4, violations.size());
        assertEquals(VALIDACAO_CAMPO_NOME, messages.get(0));
        assertEquals(VALIDACAO_CAMPO_CLIENTE, messages.get(1));
        assertEquals(VALIDACAO_CAMPO_ENVIA_EMAIL, messages.get(2));
        assertEquals(VALIDACAO_CAMPO_SEXO, messages.get(3));

    }

    @Test
    public void campo_nome_esta_vazio_ou_nulo_test(){
        this.pessoaFisica.getPessoa().setCliente(false);
        this.pessoaFisica.setSexo('M');
        Set<ConstraintViolation<PessoaFisicaValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(VALIDACAO_CAMPO_NOME, messages.get(0));
    }

    @Test
    public void campo_sexo_esta_nulo_test(){
        this.pessoaFisica.setSexo(null);
        this.pessoaFisica.getPessoa().setCliente(false);
        this.pessoaFisica.getPessoa().setDescricao(JEFFERSON_FERREIRA);
        Set<ConstraintViolation<PessoaFisicaValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(VALIDACAO_CAMPO_SEXO, messages.get(0));
    }

    @Test
    public void campo_envia_email_esta_nulo_test(){
        this.pessoaFisica.getPessoa().setEnviaEmail(null);
        this.pessoaFisica.getPessoa().setDescricao(JEFFERSON_FERREIRA);
        this.pessoaFisica.getPessoa().setCliente(false);
        this.pessoaFisica.setSexo('M');
        Set<ConstraintViolation<PessoaFisicaValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(VALIDACAO_CAMPO_ENVIA_EMAIL, messages.get(0));
    }

    @Test
    public void campo_eh_cliente_nulo_test(){
        this.pessoaFisica.getPessoa().setCliente(false);
        this.pessoaFisica.setSexo('M');
        this.pessoaFisica.getPessoa().setCliente(null);
        this.pessoaFisica.getPessoa().setDescricao(JEFFERSON_FERREIRA);
        Set<ConstraintViolation<PessoaFisicaValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(VALIDACAO_CAMPO_CLIENTE, messages.get(0));
    }

    @Test
    public void todos_os_campos_foram_preenchidos_test(){
        this.pessoaFisica.setSexo('M');
        this.pessoaFisica.getPessoa().setEnviaEmail(true);
        this.pessoaFisica.getPessoa().setCliente(false);
        this.pessoaFisica.getPessoa().setDescricao(JEFFERSON_FERREIRA);
        Set<ConstraintViolation<PessoaFisicaValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(0, violations.size());
    }

    @Test
    public void eh_obrigatorio_preencher_email_contato_endereco() throws Exception {
        this.pessoaFisica.setSexo('M');
        this.pessoaFisica.getPessoa().setEnviaEmail(true);
        this.pessoaFisica.getPessoa().setCliente(true);
        this.pessoaFisica.setCpf("05121038950");
        this.pessoaFisica.getPessoa().setDescricao(JEFFERSON_FERREIRA);
        this.pessoaFisica.getPessoa().setLogin(new Login());
        this.pessoaFisica.getPessoa().getLogin().setLogin("login");
        this.pessoaFisica.getPessoa().getLogin().setSenha(EncryptUtil.execute("senha"));
        Set<ConstraintViolation<PessoaFisicaValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(3, violations.size());
        assertEquals(VALIDACAO_CONTATO_OBRIGATORIO, messages.get(0));
        assertEquals(VALIDACAO_EMAIL_OBRIGATORIO, messages.get(1));
        assertEquals(VALIDACAO_ENDERECO_OBRIGATORIO, messages.get(2));
    }

    @Test
    public void campo_email_possui_validacao_test() throws Exception {
        this.pessoaFisica.setSexo('M');
        this.pessoaFisica.getPessoa().setEnviaEmail(true);
        this.pessoaFisica.getPessoa().setCliente(true);
        this.pessoaFisica.setCpf("05121038950");
        this.pessoaFisica.getPessoa().setDescricao(JEFFERSON_FERREIRA);
        this.pessoaFisica.getPessoa().getPessoasEmails().add(new PessoaEmail());
        this.pessoaFisica.getPessoa().setLogin(new Login());
        this.pessoaFisica.getPessoa().getLogin().setLogin("login");
        this.pessoaFisica.getPessoa().getLogin().setSenha(EncryptUtil.execute("senha"));
        this.pessoaFisica.getPessoa().getPessoasContatos().add(new PessoaContato());
        this.pessoaFisica.getPessoa().getPessoasEnderecos().add(new PessoaEndereco());

        Set<ConstraintViolation<PessoaFisicaValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(0, violations.size());
        assertEquals(0, messages.size());
    }

    @Test(expected = ValidationException.class)
    public void valida_excecao_classe_entidade() throws ValidationException {
        this.pessoaFisica.validar();
    }

    private Set<ConstraintViolation<PessoaFisicaValidacaoWrapper>> getConstraintViolations() {
        return Validation.buildDefaultValidatorFactory().getValidator()
                .validate(new PessoaFisicaValidacaoWrapper(this.pessoaFisica));
    }

}
