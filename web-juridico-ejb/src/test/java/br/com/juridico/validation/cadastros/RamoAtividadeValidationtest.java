package br.com.juridico.validation.cadastros;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;

import org.junit.Before;
import org.junit.Test;

import br.com.juridico.entidades.RamoAtividade;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.ValidationUtilTest;

/**
 * Created by Marcos Melo on 15/12/2016.
 */
public class RamoAtividadeValidationtest extends ValidationUtilTest {

    @Before
    public void up() {
        initValidation();
    }

    @Test
    public void campo_descricao_vazio_test() {
        RamoAtividade c = new RamoAtividade();
        c.setDescricao("");
        Set<ConstraintViolation<RamoAtividade>> violations = validator.validate(c);
        List<String> messages = getMensagens(violations);

        assertEquals(1, violations.size());
        assertEquals(DESCRICAO_PREENCHIMENTO_OBRIGATORIO, messages.get(0));

    }

    @Test
    public void campo_descricao_nulo_test() {
        Set<ConstraintViolation<RamoAtividade>> violations = validator.validate(new RamoAtividade());
        List<String> messages = getMensagens(violations);

        assertEquals(2, violations.size());
        assertEquals(DESCRICAO_PREENCHIMENTO_OBRIGATORIO, messages.get(0));
        assertEquals(NAO_PODE_SER_NULO, messages.get(1));

    }

    @Test
    public void campo_descricao_preenchido_test() {
        RamoAtividade c = new RamoAtividade();
        c.setDescricao("bla bla bla");
        Set<ConstraintViolation<RamoAtividade>> violations = validator.validate(c);
        assertEquals(0, violations.size());
    }

    @Test(expected = ValidationException.class)
    public void campo_duplicado_test() throws ValidationException {
        RamoAtividade c = new RamoAtividade();
        c.validar(true);
    }
}
