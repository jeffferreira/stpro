package br.com.juridico.validation.relatorios;

import br.com.juridico.builder.RelatorioFiltroBuilder;
import br.com.juridico.entidades.*;
import br.com.juridico.enums.RelatorioCriterioType;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.util.EncryptUtil;
import br.com.juridico.validation.ValidationUtilTest;
import br.com.juridico.validation.pessoas.PessoaFisicaValidacaoWrapper;
import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * Created by Marcos on 02/09/2017.
 */
public class RelatorioFiltroProcessoValidationTest extends ValidationUtilTest {

    private static final String VALIDACAO_CAMPO_DESCRICAO = "Favor preencher o campo Descrição do relatório.";
    private static final String VALIDACAO_CRITERIOS_OBRIGATORIOS = "Favor selecionar uma opção para cada um dos Critérios ativos.";

    private RelatorioFiltroProcesso relatorioFiltroProcesso;

    @Before
    public void up() {
        initValidation();
        this.relatorioFiltroProcesso = new RelatorioFiltroBuilder(1L).build();
    }

    @Test
    public void campo_descricao_esta_vazio_test(){
        this.relatorioFiltroProcesso.setDescricao("");

        for (RelatorioCriterioProcesso relatorioCriterioProcesso : this.relatorioFiltroProcesso.getCriterios()) {
            relatorioCriterioProcesso.setCriterioType(RelatorioCriterioType.E);
            relatorioCriterioProcesso.setFiltroAtivo(Boolean.FALSE);
        }

        Set<ConstraintViolation<RelatorioFiltroProcessoValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(VALIDACAO_CAMPO_DESCRICAO, messages.get(0));
    }

    @Test
    public void campo_descricao_esta_nulo_test(){
        for (RelatorioCriterioProcesso relatorioCriterioProcesso : this.relatorioFiltroProcesso.getCriterios()) {
            relatorioCriterioProcesso.setCriterioType(RelatorioCriterioType.E);
            relatorioCriterioProcesso.setFiltroAtivo(Boolean.FALSE);
        }

        Set<ConstraintViolation<RelatorioFiltroProcessoValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(VALIDACAO_CAMPO_DESCRICAO, messages.get(0));
    }

    @Test
    public void relatorio_sem_criterio_type_test(){
        this.relatorioFiltroProcesso.setDescricao("Nome do relatorio");

        Set<ConstraintViolation<RelatorioFiltroProcessoValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(VALIDACAO_CRITERIOS_OBRIGATORIOS, messages.get(0));
    }

    @Test
    public void campo_descricao_esta_vazio_ou_nulo_e_sem_criterio_type_test(){
        Set<ConstraintViolation<RelatorioFiltroProcessoValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(2, violations.size());
        assertEquals(VALIDACAO_CAMPO_DESCRICAO, messages.get(0));
        assertEquals(VALIDACAO_CRITERIOS_OBRIGATORIOS, messages.get(1));
    }

    private Set<ConstraintViolation<RelatorioFiltroProcessoValidacaoWrapper>> getConstraintViolations() {
        return Validation.buildDefaultValidatorFactory().getValidator()
                .validate(new RelatorioFiltroProcessoValidacaoWrapper(this.relatorioFiltroProcesso));
    }

}
