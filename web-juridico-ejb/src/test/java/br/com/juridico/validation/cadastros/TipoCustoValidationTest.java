package br.com.juridico.validation.cadastros;

import br.com.juridico.entidades.TipoCusto;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.ValidationUtilTest;
import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * Created by jeffersonl2009_00 on 06/07/2016.
 */
public class TipoCustoValidationTest extends ValidationUtilTest {

    @Before
    public void up() {
        initValidation();
    }

    @Test
    public void campo_descricao_vazio() {
        TipoCusto c = new TipoCusto();
        c.setDescricao("");
        Set<ConstraintViolation<TipoCusto>> violations = validator.validate(c);
        List<String> messages = getMensagens(violations);

        assertEquals(1, violations.size());
        assertEquals(DESCRICAO_PREENCHIMENTO_OBRIGATORIO, messages.get(0));

    }

    @Test
    public void campo_descricao_nulo() {
        Set<ConstraintViolation<TipoCusto>> violations = validator.validate(new TipoCusto());
        List<String> messages = getMensagens(violations);

        assertEquals(2, violations.size());
        assertEquals(DESCRICAO_PREENCHIMENTO_OBRIGATORIO, messages.get(0));
        assertEquals(NAO_PODE_SER_NULO, messages.get(1));

    }

    @Test
    public void campo_descricao_preenchido() {
        TipoCusto c = new TipoCusto();
        c.setDescricao("bla bla bla");
        Set<ConstraintViolation<TipoCusto>> violations = validator.validate(c);
        assertEquals(0, violations.size());
    }

    @Test(expected = ValidationException.class)
    public void campo_duplicado_test() throws ValidationException {
        TipoCusto c = new TipoCusto();
        c.validar(true);
    }
}
