package br.com.juridico.validation.cadastros;

import br.com.juridico.entidades.Pedido;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.ValidationUtilTest;
import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * Created by jeffersonl2009_00 on 06/07/2016.
 */
public class PedidoValidationTest extends ValidationUtilTest {

    @Before
    public void up() {
        initValidation();
    }

    @Test
    public void campo_descricao_vazio() {
        Pedido c = new Pedido();
        c.setDescricao("");
        Set<ConstraintViolation<PedidoValidacaoWrapper>> violations = buildValidatorFactory().validate(new PedidoValidacaoWrapper(c, false));
        List<String> messages = getMensagens(violations);

        assertEquals(1, violations.size());
        assertEquals(DESCRICAO_PREENCHIMENTO_OBRIGATORIO, messages.get(0));

    }

    @Test
    public void campo_descricao_nulo() {
        Set<ConstraintViolation<PedidoValidacaoWrapper>> violations = buildValidatorFactory().validate(new PedidoValidacaoWrapper(new Pedido(), false));
        List<String> messages = getMensagens(violations);

        assertEquals(1, violations.size());
        assertEquals(DESCRICAO_PREENCHIMENTO_OBRIGATORIO, messages.get(0));

    }

    @Test
    public void campo_descricao_preenchido() {
        Pedido c = new Pedido();
        c.setDescricao("bla bla bla");
        Set<ConstraintViolation<Pedido>> violations = validator.validate(c);
        assertEquals(0, violations.size());
    }

    @Test(expected = ValidationException.class)
    public void campo_duplicado_test() throws ValidationException {
        Pedido c = new Pedido();
        c.validar(true);
    }
}