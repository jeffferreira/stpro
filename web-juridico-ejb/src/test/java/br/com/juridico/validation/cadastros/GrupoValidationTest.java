package br.com.juridico.validation.cadastros;

import br.com.juridico.entidades.Grupo;
import br.com.juridico.entidades.GrupoAdvogado;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.ValidationUtilTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import javax.validation.ConstraintViolation;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * Created by jeffersonl2009_00 on 06/07/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class GrupoValidationTest extends ValidationUtilTest {

    @Before
    public void up() {
        initValidation();
    }

    @Test
    public void campo_descricao_vazio(){
        Grupo c = new Grupo();
        c.setDescricao("");
        Set<ConstraintViolation<Grupo>> violations = validator.validate(c);
        List<String> messages = getMensagens(violations);

        assertEquals(1, violations.size());
        assertEquals(DESCRICAO_PREENCHIMENTO_OBRIGATORIO, messages.get(0));

    }

    @Test
    public void campo_descricao_nulo(){
        Set<ConstraintViolation<Grupo>> violations = validator.validate(new Grupo());
        List<String> messages = getMensagens(violations);

        assertEquals(2, violations.size());
        assertEquals(DESCRICAO_PREENCHIMENTO_OBRIGATORIO, messages.get(0));
        assertEquals(NAO_PODE_SER_NULO, messages.get(1));

    }

    @Test
    public void campo_descricao_preenchido(){
        Grupo c = new Grupo();
        c.setDescricao("bla bla bla");
        Set<ConstraintViolation<Grupo>> violations = validator.validate(c);
        assertEquals(0, violations.size());
    }

    @Test(expected = ValidationException.class)
    public void campo_advogados_vazio_test() throws ValidationException {
        Grupo c = new Grupo();
        c.validar(false);
    }

    @Test
    public void passou_na_validacao_com_cadastro_correto_test() throws ValidationException {
        Grupo c = new Grupo();
        c.addGruposAdvogados(new GrupoAdvogado());
        c.validar(false);
    }

    @Test(expected = ValidationException.class)
    public void campo_duplicado_test() throws ValidationException {
        Grupo c = new Grupo();
        c.validar(true);
    }
}
