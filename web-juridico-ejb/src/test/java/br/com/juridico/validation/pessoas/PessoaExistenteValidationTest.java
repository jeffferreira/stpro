package br.com.juridico.validation.pessoas;

import br.com.juridico.entidades.PessoaFisica;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.ValidationUtilTest;
import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * Created by jeffersonl2009_00 on 07/07/2016.
 */
public class PessoaExistenteValidationTest extends ValidationUtilTest {

    private static final String VALIDACAO_CAMPO_CPF = "Já existe uma pessoa cadastrada com este CPF.";
    private static final String VALIDACAO_CAMPO_CNPJ = "Já existe uma pessoa cadastrada com este CNPJ.";
    private static final String VALIDACAO_CAMPO_NOME = "Já existe uma pessoa cadastrada com este nome.";

    private boolean existeCpfCnpj;
    private boolean existeNome;
    private boolean pessoaFisica;

    @Before
    public void up() {
        initValidation();
    }

    @Test
    public void cnpj_ja_existe_validacao_test(){
        this.pessoaFisica = Boolean.FALSE;
        this.existeCpfCnpj = Boolean.TRUE;
        this.existeNome = Boolean.FALSE;
        Set<ConstraintViolation<PessoaExistenteValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);

        assertEquals(1, violations.size());
        assertEquals(VALIDACAO_CAMPO_CNPJ, messages.get(0));
    }

    @Test
    public void cpf_ja_existe_validacao_test(){
        this.pessoaFisica = Boolean.TRUE;
        this.existeCpfCnpj = Boolean.TRUE;
        this.existeNome = Boolean.FALSE;
        Set<ConstraintViolation<PessoaExistenteValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);

        assertEquals(1, violations.size());
        assertEquals(VALIDACAO_CAMPO_CPF, messages.get(0));
    }

    @Test
    public void nome_ja_existe_validacao_test(){
        this.pessoaFisica = Boolean.TRUE;
        this.existeCpfCnpj = Boolean.FALSE;
        this.existeNome = Boolean.TRUE;
        Set<ConstraintViolation<PessoaExistenteValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);

        assertEquals(1, violations.size());
        assertEquals(VALIDACAO_CAMPO_NOME, messages.get(0));
    }

    @Test
    public void nome_e_cpf_nao_existem_cadastrados_validacao_test(){
        this.pessoaFisica = Boolean.TRUE;
        this.existeCpfCnpj = Boolean.FALSE;
        this.existeNome = Boolean.FALSE;
        Set<ConstraintViolation<PessoaExistenteValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(0, violations.size());
    }

    @Test(expected = ValidationException.class)
    public void valida_excecao_classe_entidade() throws ValidationException {
        new PessoaFisica().validarPessoaJaCadastrada(true, true);
    }

    private Set<ConstraintViolation<PessoaExistenteValidacaoWrapper>> getConstraintViolations() {
        return Validation.buildDefaultValidatorFactory().getValidator()
                .validate(new PessoaExistenteValidacaoWrapper( this.pessoaFisica, this.existeCpfCnpj, this.existeNome));
    }

}
