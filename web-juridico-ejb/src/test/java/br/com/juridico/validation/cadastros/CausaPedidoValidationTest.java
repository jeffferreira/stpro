package br.com.juridico.validation.cadastros;

import br.com.juridico.entidades.CausaPedido;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.ValidationUtilTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import javax.validation.ConstraintViolation;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * Created by jeffersonl2009_00 on 06/07/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class CausaPedidoValidationTest extends ValidationUtilTest {

    @Before
    public void up() {
        initValidation();
    }

    @Test
    public void campo_descricao_vazio(){
        CausaPedido causaPedido = new CausaPedido();
        causaPedido.setDescricao("");
        Set<ConstraintViolation<CausaPedidoValidacaoWrapper>> violations = buildValidatorFactory().validate(new CausaPedidoValidacaoWrapper(causaPedido, false));
        List<String> messages = getMensagens(violations);

        assertEquals(1, violations.size());
        assertEquals(DESCRICAO_PREENCHIMENTO_OBRIGATORIO, messages.get(0));

    }

    @Test
    public void campo_descricao_nulo(){
        Set<ConstraintViolation<CausaPedidoValidacaoWrapper>> violations = buildValidatorFactory().validate(new CausaPedidoValidacaoWrapper(new CausaPedido(), false));
        List<String> messages = getMensagens(violations);

        assertEquals(1, violations.size());
        assertEquals(DESCRICAO_PREENCHIMENTO_OBRIGATORIO, messages.get(0));

    }

    @Test
    public void campo_descricao_preenchido(){
        CausaPedido causaPedido = new CausaPedido();
        causaPedido.setDescricao("Descrição");
        Set<ConstraintViolation<CausaPedido>> violations = validator.validate(causaPedido);
        assertEquals(0, violations.size());
    }

    @Test(expected = ValidationException.class)
    public void campo_duplicado_test() throws ValidationException {
        CausaPedido c = new CausaPedido();
        c.validar(true);
    }

}
