package br.com.juridico.validation.cadastros;

import br.com.juridico.entidades.Funcao;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.ValidationUtilTest;
import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * Created by jeffersonl2009_00 on 17/08/2016.
 */
public class FuncaoValidationtest extends ValidationUtilTest {

    @Before
    public void up() {
        initValidation();
    }

    @Test
    public void campo_descricao_vazio_test() {
        Funcao c = new Funcao();
        c.setDescricao("");
        Set<ConstraintViolation<Funcao>> violations = validator.validate(c);
        List<String> messages = getMensagens(violations);

        assertEquals(1, violations.size());
        assertEquals(DESCRICAO_PREENCHIMENTO_OBRIGATORIO, messages.get(0));

    }

    @Test
    public void campo_descricao_nulo_test() {
        Set<ConstraintViolation<Funcao>> violations = validator.validate(new Funcao());
        List<String> messages = getMensagens(violations);

        assertEquals(2, violations.size());
        assertEquals(DESCRICAO_PREENCHIMENTO_OBRIGATORIO, messages.get(0));
        assertEquals(NAO_PODE_SER_NULO, messages.get(1));

    }

    @Test
    public void campo_descricao_preenchido_test() {
        Funcao c = new Funcao();
        c.setDescricao("bla bla bla");
        Set<ConstraintViolation<Funcao>> violations = validator.validate(c);
        assertEquals(0, violations.size());
    }

    @Test(expected = ValidationException.class)
    public void campo_duplicado_test() throws ValidationException {
        Funcao c = new Funcao();
        c.validar(true);
    }
}
