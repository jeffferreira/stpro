package br.com.juridico.validation.processo;

import br.com.juridico.entidades.*;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.ValidationUtilTest;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * Created by jeffersonl2009_00 on 08/07/2016.
 */
public class ValoresValidationTest extends ValidationUtilTest {

    private static final String VALIDACAO_CAMPO_GRAU_RISCO = "Favor selecionar o Grau de Risco.";
    private static final String VALIDACAO_CAMPO_VALOR_CAUSA = "Favor preencher o Valor da Causa.";
    private static final String VALIDACAO_CAMPO_VALOR_PROVAVEL = "Favor preencher o Valor Provável.";
    private static final String VALIDACAO_CAMPO_PEDIDO = "Favor selecionar o Pedido.";
    private static final String VALIDACAO_CAMPO_CAUSA_PEDIDO = "Favor selecionar a Causa do Pedido.";

    private Processo processo;
    private List<ProcessoPedido> pedidos;

    @Before
    public void up() {
        initValidation();
        this.processo = new Processo();
        this.pedidos = Lists.newArrayList();
    }

    @Test
    public void todos_os_campos_estao_com_validacao_test(){
        Set<ConstraintViolation<ValoresValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(5, violations.size());
        assertEquals(VALIDACAO_CAMPO_VALOR_PROVAVEL, messages.get(0));
        assertEquals(VALIDACAO_CAMPO_VALOR_CAUSA, messages.get(1));
        assertEquals(VALIDACAO_CAMPO_CAUSA_PEDIDO, messages.get(2));
        assertEquals(VALIDACAO_CAMPO_GRAU_RISCO, messages.get(3));
        assertEquals(VALIDACAO_CAMPO_PEDIDO, messages.get(4));
    }

    @Test
    public void validacao_valor_provavel_test(){
        ProcessoPedido pedido = new ProcessoPedido(new Pedido(), this.processo, new CausaPedido(), BigDecimal.ONE, 1L);
        this.processo.setValorProvavel(null);
        this.processo.setValorCausa(BigDecimal.TEN);
        this.processo.setGrauRisco(new GrauRisco());
        this.pedidos.add(pedido);
        this.processo.setPedidos(pedidos);

        Set<ConstraintViolation<ValoresValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(VALIDACAO_CAMPO_VALOR_PROVAVEL, messages.get(0));
    }

    @Test
    public void validacao_valor_causa_test(){
        ProcessoPedido pedido = new ProcessoPedido(new Pedido(), this.processo, new CausaPedido(), BigDecimal.ONE, 1L);
        this.processo.setValorProvavel(BigDecimal.TEN);
        this.processo.setValorCausa(null);
        this.processo.setGrauRisco(new GrauRisco());
        this.pedidos.add(pedido);
        this.processo.setPedidos(pedidos);

        Set<ConstraintViolation<ValoresValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(VALIDACAO_CAMPO_VALOR_CAUSA, messages.get(0));
    }

    @Test
    public void validacao_causa_pedido_test(){
        ProcessoPedido pedido = new ProcessoPedido(new Pedido(), this.processo, null, BigDecimal.ONE, 1L);
        this.processo.setValorProvavel(BigDecimal.TEN);
        this.processo.setValorCausa(BigDecimal.TEN);
        this.processo.setGrauRisco(new GrauRisco());
        this.pedidos.add(pedido);
        this.processo.setPedidos(pedidos);

        Set<ConstraintViolation<ValoresValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(VALIDACAO_CAMPO_CAUSA_PEDIDO, messages.get(0));
    }

    @Test
    public void validacao_grau_risco_test(){
        ProcessoPedido pedido = new ProcessoPedido(new Pedido(), this.processo, new CausaPedido(), BigDecimal.ONE, 1L);
        this.processo.setValorProvavel(BigDecimal.TEN);
        this.processo.setValorCausa(BigDecimal.TEN);
        this.processo.setGrauRisco(null);
        this.pedidos.add(pedido);
        this.processo.setPedidos(pedidos);

        Set<ConstraintViolation<ValoresValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(VALIDACAO_CAMPO_GRAU_RISCO, messages.get(0));
    }

    @Test
    public void validacao_pedido_test(){
        ProcessoPedido pedido = new ProcessoPedido(null, this.processo, new CausaPedido(), BigDecimal.ONE, 1L);
        this.processo.setValorProvavel(BigDecimal.TEN);
        this.processo.setValorCausa(BigDecimal.TEN);
        this.processo.setGrauRisco(new GrauRisco());
        this.pedidos.add(pedido);
        this.processo.setPedidos(pedidos);

        Set<ConstraintViolation<ValoresValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(VALIDACAO_CAMPO_PEDIDO, messages.get(0));
    }

    @Test(expected = ValidationException.class)
    public void valida_excecao_classe_entidade_test() throws ValidationException {
        this.processo.validarValoresProcesso();
    }

    private Set<ConstraintViolation<ValoresValidacaoWrapper>> getConstraintViolations() {
        return Validation.buildDefaultValidatorFactory().getValidator()
                .validate(new ValoresValidacaoWrapper(this.processo));
    }
}
