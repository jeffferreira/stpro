package br.com.juridico.validation.processo;

import br.com.juridico.dto.CentroCustoDto;
import br.com.juridico.dto.ParteDto;
import br.com.juridico.entidades.*;
import br.com.juridico.exception.ValidationException;
import br.com.juridico.validation.ValidationUtilTest;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * Created by jeffersonl2009_00 on 08/07/2016.
 */
public class ParteValidationTest extends ValidationUtilTest {

    private static final String VALIDACAO_CAMPO_TIPO = "Favor selecionar o Tipo.";
    private static final String VALIDACAO_CAMPO_NOME = "Favor preencher o campo Nome.";
    private static final String VALIDACAO_CAMPO_RAZAO_SOCIAL = "Favor selecionar a Raz\u00e3o Social.";
    private static final String VALIDACAO_CAMPO_POSICAO = "Favor selecionar a Posi\u00e7\u00e3o.";
    private static final String VALIDACAO_CAMPO_ADVOGADOS = "Favor selecionar Advogado(s).";
    private static final String VALIDACAO_CAMPO_GRUPO_ADVOGADOS = "Favor selecionar o Grupo de advogados";
    private static final String VALIDACAO_CAMPO_ADVOGADO_GRUPO = "Favor selecionar Advogado/Grupo.";
    private static final String VALIDACAO_CAMPO_PERCENTUAL = "A soma dos percentuais dos Centros de Custo deve ser de 100%. Por favor, informe corretamente.";
    private static final String TIPO_ADVOGADO_INDIVIDUAL = "I";
    private static final String TIPO_ADVOGADO_GRUPO = "G";
    private static final String PESSOA_FISICA = "PF";
    private static final String PESSOA_JURIDICA = "PJ";
    private static final String JEFFERSON_FERREIRA = "Jefferson Ferreira";

    private Processo processo;
    private ParteDto parte;

    @Before
    public void up() {
        initValidation();
        this.processo = new Processo();
        this.parte = new ParteDto();
        Pessoa pessoa = new Pessoa();
        this.parte.setPessoa(pessoa);
    }

    @Test
    public void todos_os_campos_quando_pessoa_fisica_estao_com_validacao_test(){
        this.parte.setTipoPessoa(null);
        this.parte.setPosicao(null);
        this.parte.setTipoAdvogado(null);
        Set<ConstraintViolation<ParteValidationWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(5, violations.size());
        assertEquals(VALIDACAO_CAMPO_NOME, messages.get(0));
        assertEquals(VALIDACAO_CAMPO_ADVOGADOS, messages.get(1));
        assertEquals(VALIDACAO_CAMPO_ADVOGADO_GRUPO, messages.get(2));
        assertEquals(VALIDACAO_CAMPO_POSICAO, messages.get(3));
        assertEquals(VALIDACAO_CAMPO_TIPO, messages.get(4));
    }

    @Test
    public void todos_os_campos_quando_pessoa_juridica_estao_com_validacao_test(){
        this.parte.setTipoPessoa(PESSOA_JURIDICA);
        this.parte.setPosicao(null);
        this.parte.setTipoAdvogado(null);
        List<ProcessoPessoaJuridicaCentroCusto> centroCustos = Lists.newArrayList();
        centroCustos.add(new ProcessoPessoaJuridicaCentroCusto(1L));
        this.parte.getParte().setCentroCustos(centroCustos);
        Set<ConstraintViolation<ParteValidationWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(5, violations.size());
        assertEquals(VALIDACAO_CAMPO_PERCENTUAL, messages.get(0));
        assertEquals(VALIDACAO_CAMPO_ADVOGADOS, messages.get(1));
        assertEquals(VALIDACAO_CAMPO_ADVOGADO_GRUPO, messages.get(2));
        assertEquals(VALIDACAO_CAMPO_POSICAO, messages.get(3));
        assertEquals(VALIDACAO_CAMPO_RAZAO_SOCIAL, messages.get(4));
    }

    @Test
    public void validacao_campo_tipo_test(){
        this.parte.setTipoPessoa(null);
        this.parte.getPessoa().setDescricao(JEFFERSON_FERREIRA);
        this.parte.setPosicao(new Posicao());
        this.parte.setTipoAdvogado(TIPO_ADVOGADO_INDIVIDUAL);
        this.parte.getSelectedAdvogados().add(new PessoaFisica());
        Set<ConstraintViolation<ParteValidationWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(VALIDACAO_CAMPO_TIPO, messages.get(0));
    }

    @Test
    public void validacao_campo_nome_test(){
        this.parte.setTipoPessoa(PESSOA_FISICA);
        this.parte.setPosicao(new Posicao());
        this.parte.setTipoAdvogado(TIPO_ADVOGADO_INDIVIDUAL);
        this.parte.getSelectedAdvogados().add(new PessoaFisica());
        Set<ConstraintViolation<ParteValidationWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(VALIDACAO_CAMPO_NOME, messages.get(0));
    }

    @Test
    public void validacao_campo_razao_social_test(){
        this.parte.setPosicao(new Posicao());
        this.parte.setTipoAdvogado(TIPO_ADVOGADO_INDIVIDUAL);
        this.parte.getSelectedAdvogados().add(new PessoaFisica());
        this.parte.setTipoPessoa(PESSOA_JURIDICA);
        Set<ConstraintViolation<ParteValidationWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(VALIDACAO_CAMPO_RAZAO_SOCIAL, messages.get(0));
    }

    @Test
    public void validacao_campo_posicao_test(){
        this.parte.setPosicao(null);
        this.parte.setTipoAdvogado(TIPO_ADVOGADO_INDIVIDUAL);
        this.parte.getSelectedAdvogados().add(new PessoaFisica());
        this.parte.setTipoPessoa(PESSOA_JURIDICA);
        this.parte.getPessoa().setDescricao(JEFFERSON_FERREIRA);
        Set<ConstraintViolation<ParteValidationWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(VALIDACAO_CAMPO_POSICAO, messages.get(0));
    }

    @Test
    public void validacao_campo_advogados_individuais_externo_test(){
        this.parte.setPosicao(new Posicao());
        this.parte.setTipoAdvogado(TIPO_ADVOGADO_INDIVIDUAL);
        this.parte.setTipoPessoa(PESSOA_JURIDICA);
        Pessoa pessoa = new Pessoa(1L, JEFFERSON_FERREIRA, 'F', false);
        pessoa.setCliente(false);
        this.parte.setPessoa(pessoa);
        ProcessoPessoaJuridicaCentroCusto pessoaJuridicaCentroCusto = new ProcessoPessoaJuridicaCentroCusto(1L);
        pessoaJuridicaCentroCusto.setPercentual(new BigDecimal(100).setScale(2));
        this.parte.getParte().addCentroCustos(pessoaJuridicaCentroCusto);
        Set<ConstraintViolation<ParteValidationWrapper>> violations = getConstraintViolations();
        assertEquals(0, violations.size());
    }

    @Test
    public void validacao_campo_advogados_individuais_interno_test(){
        this.parte.setPosicao(new Posicao());
        this.parte.setTipoAdvogado(TIPO_ADVOGADO_INDIVIDUAL);
        this.parte.setTipoPessoa(PESSOA_JURIDICA);
        Pessoa pessoa = new Pessoa(1L, JEFFERSON_FERREIRA, 'F', true);
        pessoa.setCliente(true);
        this.parte.setPessoa(pessoa);

        Set<ConstraintViolation<ParteValidationWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(VALIDACAO_CAMPO_ADVOGADOS, messages.get(0));
    }

    @Test
    public void validacao_campo_grupo_test(){
        this.parte.setTipoPessoa(PESSOA_JURIDICA);
        this.parte.setPosicao(new Posicao());
        this.parte.setTipoAdvogado(TIPO_ADVOGADO_GRUPO);
        this.parte.getPessoa().setDescricao(JEFFERSON_FERREIRA);
        Set<ConstraintViolation<ParteValidationWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(VALIDACAO_CAMPO_GRUPO_ADVOGADOS, messages.get(0));
    }

    @Test
    public void validacao_campo_percentual_centro_custo_test(){
        this.parte.setTipoPessoa(PESSOA_JURIDICA);
        this.parte.setPosicao(new Posicao());
        this.parte.setTipoAdvogado(TIPO_ADVOGADO_INDIVIDUAL);
        this.parte.getPessoa().setDescricao(JEFFERSON_FERREIRA);
        this.parte.getSelectedAdvogados().add(new PessoaFisica());
        this.parte.getCentroCustos().add(new CentroCustoDto(0));
        List<ProcessoPessoaJuridicaCentroCusto> centroCustos = Lists.newArrayList();
        centroCustos.add(new ProcessoPessoaJuridicaCentroCusto(1L));
        this.parte.getParte().setCentroCustos(centroCustos);

        Set<ConstraintViolation<ParteValidationWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);
        assertEquals(1, violations.size());
        assertEquals(VALIDACAO_CAMPO_PERCENTUAL, messages.get(0));
    }

    @Test(expected = ValidationException.class)
    public void valida_excecao_classe_entidade_test() throws ValidationException {
        this.processo.validarParte(this.parte);
    }

    private Set<ConstraintViolation<ParteValidationWrapper>> getConstraintViolations() {
        return Validation.buildDefaultValidatorFactory().getValidator()
                .validate(new ParteValidationWrapper(this.parte));
    }
}
