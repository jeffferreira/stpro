package br.com.juridico.validation.processo.andamento;

import br.com.juridico.entidades.Andamento;
import br.com.juridico.validation.ValidationUtilTest;
import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * Created by jeffersonl2009_00 on 30/08/2016.
 */
public class AndamentoValidationTest extends ValidationUtilTest {

    private static final String CAMPO_VALIDACAO_DATA_ANDAMENTO = "Favor preencher a data do andamento.";
    private static final String CAMPO_VALIDACAO_CONFIDENCIAL = "Favor preencher se informação confidencial.";
    private static final String CAMPO_VALIDACAO_DIAS_AVISO = "Favor preencher o(s) dias para aviso.";
    private static final String CAMPO_VALIDACAO_TIPO_ANDAMENTO = "Favor preencher o tipo do andamento.";
    private static final String CAMPO_VALIDACAO_ADVOGADOS = "Favor preencher o(s) advogado(s).";
    private static final String CAMPO_VALIDACAO_LOCALIZACAO = "Favor preencher a localização.";

    private Andamento andamento;

    @Before
    public void up() {
        initValidation();
        this.andamento = new Andamento();
    }

    @Test
    public void todos_os_campos_estao_com_validacao_test(){
        this.andamento.setDataAndamento(null);
        this.andamento.setConfidencial(null);
        this.andamento.setTipoAndamento(null);
        this.andamento.setEnviaResumoAdvogados(false);
        this.andamento.setEnviaResumoAdvogado(true);
        this.andamento.setEnviaEmailAviso(true);

        Set<ConstraintViolation<AndamentoValidacaoWrapper>> violations = getConstraintViolations();
        List<String> messages = getMensagens(violations);

        assertEquals(CAMPO_VALIDACAO_DATA_ANDAMENTO, messages.get(0));
        assertEquals(CAMPO_VALIDACAO_LOCALIZACAO, messages.get(1));
        assertEquals(CAMPO_VALIDACAO_TIPO_ANDAMENTO, messages.get(2));
        assertEquals(CAMPO_VALIDACAO_ADVOGADOS, messages.get(3));
        assertEquals(CAMPO_VALIDACAO_DIAS_AVISO, messages.get(4));
        assertEquals(CAMPO_VALIDACAO_CONFIDENCIAL, messages.get(5));

        assertEquals(6, violations.size());
    }



    private Set<ConstraintViolation<AndamentoValidacaoWrapper>> getConstraintViolations() {
        return Validation.buildDefaultValidatorFactory().getValidator()
                .validate(new AndamentoValidacaoWrapper(this.andamento));
    }
}
