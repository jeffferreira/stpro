# Host: localhost  (Version: 5.5.44)
# Date: 2015-10-21 17:26:37
# Generator: MySQL-Front 5.3  (Build 4.234)

/*!40101 SET NAMES latin1 */;

#
# Structure for table "tb_classe_processual"
#

DROP TABLE IF EXISTS `tb_classe_processual`;
CREATE TABLE `tb_classe_processual` (
  `ID_CLASSE_PROCESSUAL` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identificador �nico da tabela de CLASSE PROCESSUAL.',
  `DS_CLASSE_PROCESSUAL` varchar(200) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`ID_CLASSE_PROCESSUAL`),
  UNIQUE KEY `DS_CLASSE_PROCESSUAL_UNIQUE` (`DS_CLASSE_PROCESSUAL`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci COMMENT='	';

#
# Data for table "tb_classe_processual"
#

INSERT INTO `tb_classe_processual` VALUES (1,'A��o de cobran�a'),(2,'A��o de despejo'),(3,'A��o de indeniza��o'),(4,'Div�rcio'),(5,'Execu��o de alimentos'),(6,'Impugna��o do valor da causa'),(7,'Nova classe processual 123');

#
# Structure for table "tb_comarca"
#

DROP TABLE IF EXISTS `tb_comarca`;
CREATE TABLE `tb_comarca` (
  `ID_COMARCA` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identificador �nico da tabela de COMARCA.',
  `DS_COMARCA` varchar(200) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`ID_COMARCA`),
  UNIQUE KEY `DS_COMARCA_UNIQUE` (`DS_COMARCA`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci COMMENT='	';

#
# Data for table "tb_comarca"
#

INSERT INTO `tb_comarca` VALUES (1,'Maring�');

#
# Structure for table "tb_estado_civil"
#

DROP TABLE IF EXISTS `tb_estado_civil`;
CREATE TABLE `tb_estado_civil` (
  `ID_ESTADO_CIVIL` bigint(20) unsigned NOT NULL DEFAULT '0',
  `DS_ESTADO_CIVIL` varchar(100) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID_ESTADO_CIVIL`),
  UNIQUE KEY `DS_ESTADO_CIVIL_UK` (`DS_ESTADO_CIVIL`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

#
# Data for table "tb_estado_civil"
#

INSERT INTO `tb_estado_civil` VALUES (1,'Solteiro(a)'),(2,'Casado(a)'),(3,'Divorciado(a)'),(4,'Vi�vo(a)'),(5,'Separado(a)'),(6,'Companheiro(a)');

#
# Structure for table "tb_fase"
#

DROP TABLE IF EXISTS `tb_fase`;
CREATE TABLE `tb_fase` (
  `ID_FASE` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identificador �nico da tabela de FASE.',
  `DS_FASE` varchar(200) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`ID_FASE`),
  UNIQUE KEY `DS_FASE_UNIQUE` (`DS_FASE`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci COMMENT='	';

#
# Data for table "tb_fase"
#

INSERT INTO `tb_fase` VALUES (1,'Sem fase'),(2,'Execu��o'),(3,'Inicial'),(4,'Final'),(5,'Recursal');

#
# Structure for table "tb_forum"
#

DROP TABLE IF EXISTS `tb_forum`;
CREATE TABLE `tb_forum` (
  `ID_FORUM` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identificador �nico da tabela de FORUM.',
  `DS_FORUM` varchar(200) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`ID_FORUM`),
  UNIQUE KEY `DS_FORUM_UNIQUE` (`DS_FORUM`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci COMMENT='	';

#
# Data for table "tb_forum"
#

INSERT INTO `tb_forum` VALUES (1,'F�rum de Maring�');

#
# Structure for table "tb_grupo"
#

DROP TABLE IF EXISTS `tb_grupo`;
CREATE TABLE `tb_grupo` (
  `ID_GRUPO` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identificador �nico da tabela de GRUPO.',
  `DS_GRUPO` varchar(200) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`ID_GRUPO`),
  UNIQUE KEY `DS_GRUPO_UNIQUE` (`DS_GRUPO`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

#
# Data for table "tb_grupo"
#

INSERT INTO `tb_grupo` VALUES (1,'Grupo 01');

#
# Structure for table "tb_indice_correcao"
#

DROP TABLE IF EXISTS `tb_indice_correcao`;
CREATE TABLE `tb_indice_correcao` (
  `ID_INDICE_CORRECAO` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identificador �nico da tabela de INDICE CORRE��O.',
  `DS_INDICE_CORRECAO` varchar(200) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`ID_INDICE_CORRECAO`),
  UNIQUE KEY `DS_INDICE_CORRECAO_UNIQUE` (`DS_INDICE_CORRECAO`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci COMMENT='	';

#
# Data for table "tb_indice_correcao"
#

INSERT INTO `tb_indice_correcao` VALUES (1,'CDI'),(2,'CUB (Sinduscon)'),(3,'CUB Nacional (CBIC)'),(4,'CUB-SC (Sinduscon)'),(5,'ICV (Dieese)'),(6,'IGP-DI (FGV)'),(7,'IGP-M (FGV)'),(8,'INCC-DI (FGV)'),(9,'INPC (IBGE)'),(10,'IPA-DI (FGV)'),(11,'IPA-M (FGV)'),(12,'IPC (Fipe)'),(13,'IPC (IBGE)'),(14,'IPC-DI (FGV)'),(15,'IPC-r'),(16,'IPCA (IBGE)'),(17,'IPCA-E (IBGE)'),(18,'M�dia do INPC (IBGE) e IGP-DI (FGV)'),(19,'Poupan�a'),(20,'Selic (c�lculo capitalizada mensalmente)'),(21,'TBF'),(22,'TJLP (BACEN)'),(23,'TJLP (RECEITA FEDERAL)'),(24,'TR (Bacen)');

#
# Structure for table "tb_natureza_acao"
#

DROP TABLE IF EXISTS `tb_natureza_acao`;
CREATE TABLE `tb_natureza_acao` (
  `ID_NATUREZA_ACAO` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identificador �nico da tabela de NATUREZA A��O.',
  `DS_NATUREZA_ACAO` varchar(200) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`ID_NATUREZA_ACAO`),
  UNIQUE KEY `DS_NATUREZA_ACAO_UNIQUE` (`DS_NATUREZA_ACAO`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci COMMENT='	';

#
# Data for table "tb_natureza_acao"
#

INSERT INTO `tb_natureza_acao` VALUES (1,'N�o definido'),(2,'C�vel'),(3,'Criminal'),(4,'Fam�lia'),(5,'Trabalhista');

#
# Structure for table "tb_pessoa"
#

DROP TABLE IF EXISTS `tb_pessoa`;
CREATE TABLE `tb_pessoa` (
  `ID_PESSOA` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `DS_PESSOA` varchar(200) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `FL_TIPO_PESSOA` char(1) CHARACTER SET latin1 NOT NULL DEFAULT 'F' COMMENT 'F: F�SICA / J: JUR�DICA',
  `DS_LOGIN` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `DS_SENHA` blob,
  `FL_ENVIAR_EMAIL` char(1) COLLATE latin1_general_ci NOT NULL DEFAULT 'S' COMMENT 'S: SIM / N: N�O',
  PRIMARY KEY (`ID_PESSOA`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

#
# Data for table "tb_pessoa"
#


#
# Structure for table "tb_pessoa_email"
#

DROP TABLE IF EXISTS `tb_pessoa_email`;
CREATE TABLE `tb_pessoa_email` (
  `ID_PESSOA_EMAIL` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identificador unico da tabela PESSOA_EMAIL',
  `FK_PESSOA` bigint(20) unsigned NOT NULL DEFAULT '0',
  `DS_EMAIL` varchar(75) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID_PESSOA_EMAIL`),
  KEY `PESSOA_EMAIL_FK_idx` (`FK_PESSOA`),
  CONSTRAINT `PESSOA_EMAIL_FK` FOREIGN KEY (`FK_PESSOA`) REFERENCES `tb_pessoa` (`ID_PESSOA`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci COMMENT='Tabelas de emails';

#
# Data for table "tb_pessoa_email"
#


#
# Structure for table "tb_pessoa_fisica"
#

DROP TABLE IF EXISTS `tb_pessoa_fisica`;
CREATE TABLE `tb_pessoa_fisica` (
  `ID_PESSOA_FISICA` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `FK_PESSOA` bigint(20) unsigned NOT NULL DEFAULT '0',
  `DS_CPF` varchar(11) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `FL_SEXO` char(1) COLLATE latin1_general_ci NOT NULL DEFAULT 'M' COMMENT 'M: MASCULINO / F: FEMININO',
  `DT_NASCIMENTO` date NOT NULL DEFAULT '0000-00-00',
  `DS_RG` varchar(12) COLLATE latin1_general_ci DEFAULT NULL,
  `FK_ESTADO_CIVIL` bigint(20) unsigned NOT NULL DEFAULT '0',
  `DS_NACIONALIDADE` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `DS_PROFISSAO` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `DS_OBSERVACOES` varchar(4000) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`ID_PESSOA_FISICA`),
  KEY `pessoa_fisica_pessoa_fk_idx` (`FK_PESSOA`),
  KEY `pessoa_fisica_estado_civil_fk_idx` (`FK_ESTADO_CIVIL`),
  CONSTRAINT `pessoa_fisica_estado_civil_fk` FOREIGN KEY (`FK_ESTADO_CIVIL`) REFERENCES `tb_estado_civil` (`ID_ESTADO_CIVIL`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `pessoa_fisica_pessoa_fk` FOREIGN KEY (`FK_PESSOA`) REFERENCES `tb_pessoa` (`ID_PESSOA`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

#
# Data for table "tb_pessoa_fisica"
#


#
# Structure for table "tb_pessoa_juridica"
#

DROP TABLE IF EXISTS `tb_pessoa_juridica`;
CREATE TABLE `tb_pessoa_juridica` (
  `ID_PESSOA_JURIDICA` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `FK_PESSOA` bigint(20) unsigned NOT NULL DEFAULT '0',
  `DS_CNPJ` varchar(14) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `DS_RESPONSAVEL` varchar(100) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `DT_FUNDACAO` date DEFAULT NULL,
  `DS_TIPO_EMPRESA` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `DS_ATIVIDADE_PRINCIPAL` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `DS_INSCRICAO_MUNICIPAL` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `DS_INSCRICAO_ESTADUAL` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `DS_OBSERVACOES` varchar(4000) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`ID_PESSOA_JURIDICA`),
  KEY `pessoa_juridica_pessoa_fk_idx` (`FK_PESSOA`),
  CONSTRAINT `pessoa_juridica_pessoa_fk` FOREIGN KEY (`FK_PESSOA`) REFERENCES `tb_pessoa` (`ID_PESSOA`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

#
# Data for table "tb_pessoa_juridica"
#


#
# Structure for table "tb_posicao"
#

DROP TABLE IF EXISTS `tb_posicao`;
CREATE TABLE `tb_posicao` (
  `ID_POSICAO` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identificador �nico da tabela de POSI��O.',
  `DS_POSICAO` varchar(200) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`ID_POSICAO`),
  UNIQUE KEY `DS_POSICAO_UNIQUE` (`DS_POSICAO`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

#
# Data for table "tb_posicao"
#

INSERT INTO `tb_posicao` VALUES (1,'Adverso'),(2,'Advogado'),(3,'Advogado Adverso'),(4,'Autor'),(5,'Cliente'),(6,'Reclamada'),(7,'Reclamante'),(8,'Relator'),(9,'Requerente'),(10,'Requerido'),(11,'R�u'),(12,'Testemunha'),(13,'TESTE');

#
# Structure for table "tb_rito"
#

DROP TABLE IF EXISTS `tb_rito`;
CREATE TABLE `tb_rito` (
  `ID_RITO` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identificador �nico da tabela de RITO.',
  `DS_RITO` varchar(200) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`ID_RITO`),
  UNIQUE KEY `DS_RITO_UNIQUE` (`DS_RITO`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci COMMENT='	';

#
# Data for table "tb_rito"
#

INSERT INTO `tb_rito` VALUES (1,'N�o definido'),(2,'Especial'),(3,'Ordin�rio'),(4,'Sum�rio'),(5,'Sumar�ssimo');

#
# Structure for table "tb_tipo_atendimento"
#

DROP TABLE IF EXISTS `tb_tipo_atendimento`;
CREATE TABLE `tb_tipo_atendimento` (
  `ID_TIPO_ATENDIMENTO` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identificado unico da tabela TIPO_ATENDIMENTO',
  `DS_DESCRICAO` varchar(255) COLLATE latin1_general_ci NOT NULL DEFAULT '' COMMENT 'DESCRICAO DO TIPO DE ATENDIMENTO',
  `DT_EXCLUSAO_LOGICA` datetime DEFAULT NULL,
  PRIMARY KEY (`ID_TIPO_ATENDIMENTO`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci COMMENT='Tabela tipo de atendimento';

#
# Data for table "tb_tipo_atendimento"
#

INSERT INTO `tb_tipo_atendimento` VALUES (1,'PARECER',NULL),(2,'CONTRATO',NULL),(3,'D�VIDAS DE PROCESSOS',NULL),(4,'NOVA A��O',NULL);

#
# Structure for table "tb_tipo_contato"
#

DROP TABLE IF EXISTS `tb_tipo_contato`;
CREATE TABLE `tb_tipo_contato` (
  `ID_TIPO_CONTATO` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identificador �nico da tabela de TIPO_CONTATO.',
  `DS_TIPO_CONTATO` varchar(200) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`ID_TIPO_CONTATO`),
  UNIQUE KEY `DS_TIPO_CONTATO_UNIQUE` (`DS_TIPO_CONTATO`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

#
# Data for table "tb_tipo_contato"
#

INSERT INTO `tb_tipo_contato` VALUES (1,'Telefone'),(2,'Celular'),(3,'Residencial'),(4,'Comercial'),(5,'Fax');

#
# Structure for table "tb_contato"
#

DROP TABLE IF EXISTS `tb_contato`;
CREATE TABLE `tb_contato` (
  `ID_CONTATO` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `FK_TIPO_CONTATO` bigint(20) unsigned NOT NULL DEFAULT '0',
  `NR_DDD` int(3) DEFAULT '0',
  `NR_NUMERO` varchar(9) COLLATE latin1_general_ci DEFAULT NULL,
  `NR_RAMAL` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `DS_SETOR` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `DS_CONTATO` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`ID_CONTATO`),
  KEY `contato_tipo_contato_fk_idx` (`FK_TIPO_CONTATO`),
  CONSTRAINT `contato_tipo_contato_fk` FOREIGN KEY (`FK_TIPO_CONTATO`) REFERENCES `tb_tipo_contato` (`ID_TIPO_CONTATO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

#
# Data for table "tb_contato"
#


#
# Structure for table "tb_pessoa_contato"
#

DROP TABLE IF EXISTS `tb_pessoa_contato`;
CREATE TABLE `tb_pessoa_contato` (
  `ID_PESSOA_CONTATO` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `FK_PESSOA` bigint(20) unsigned NOT NULL DEFAULT '0',
  `FK_CONTATO` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID_PESSOA_CONTATO`),
  KEY `pesso_contato_pessoa_fk_idx` (`FK_PESSOA`),
  KEY `pessoa_contato_contato_fk_idx` (`FK_CONTATO`),
  CONSTRAINT `pessoa_contato_contato_fk` FOREIGN KEY (`FK_CONTATO`) REFERENCES `tb_contato` (`ID_CONTATO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `pessoa_contato_pessoa_fk` FOREIGN KEY (`FK_PESSOA`) REFERENCES `tb_pessoa` (`ID_PESSOA`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

#
# Data for table "tb_pessoa_contato"
#


#
# Structure for table "tb_uf"
#

DROP TABLE IF EXISTS `tb_uf`;
CREATE TABLE `tb_uf` (
  `ID_UF` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identificador �nico da tabela de UF.',
  `SG_UF` char(2) COLLATE latin1_general_ci NOT NULL COMMENT 'Sigla da UF.',
  `NO_UF` varchar(45) COLLATE latin1_general_ci NOT NULL COMMENT 'Nome da UF. Exemplo: S�o Paulo',
  PRIMARY KEY (`ID_UF`),
  UNIQUE KEY `SG_UF_UNIQUE` (`SG_UF`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

#
# Data for table "tb_uf"
#

INSERT INTO `tb_uf` VALUES (1,'AC','Acre'),(2,'AL','Alagoas'),(3,'AP','Amap�'),(4,'AM','Amazonas'),(5,'BA','Bahia'),(6,'CE','Cear�'),(7,'DF','Distrito Federal'),(8,'ES','Esp�rito Santo'),(9,'GO','Goi�s'),(10,'MA','Maranh�o'),(11,'MT','Mato Grosso'),(12,'MS','Mato Grosso do Sul'),(13,'MG','Minas Gerais'),(14,'PA','Par�'),(15,'PB','Para�ba'),(16,'PR','Paran�'),(17,'PE','Pernambuco'),(18,'PI','Piau�'),(19,'RJ','Rio de Janeiro'),(20,'RN','Rio Grande do Norte'),(21,'RS','Rio Grande do Sul'),(22,'RO','Rond�nia'),(23,'RR','Roraima'),(24,'SC','Santa Catarina'),(25,'SP','S�o Paulo'),(26,'SE','Sergipe'),(27,'TO','Tocantins');

#
# Structure for table "tb_cidade"
#

DROP TABLE IF EXISTS `tb_cidade`;
CREATE TABLE `tb_cidade` (
  `ID_CIDADE` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `DS_CIDADE` varchar(255) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `FK_CIDADE` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`ID_CIDADE`),
  UNIQUE KEY `DS_CIDADE_UNIQUE` (`DS_CIDADE`),
  KEY `fk_cidade_uf_idx` (`FK_CIDADE`),
  CONSTRAINT `fk_cidade_uf` FOREIGN KEY (`FK_CIDADE`) REFERENCES `tb_uf` (`ID_UF`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

#
# Data for table "tb_cidade"
#

INSERT INTO `tb_cidade` VALUES (1,'Maring�',16),(2,'Londrina',16);

#
# Structure for table "tb_endereco"
#

DROP TABLE IF EXISTS `tb_endereco`;
CREATE TABLE `tb_endereco` (
  `ID_ENDERECO` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `DS_ENDERECO` varchar(200) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `DS_NUMERO` varchar(25) COLLATE latin1_general_ci DEFAULT NULL,
  `DS_COMPLEMENTO` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `DS_BAIRRO` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `FK_CIDADE` bigint(20) unsigned NOT NULL DEFAULT '0',
  `NR_CEP` int(8) DEFAULT '0',
  PRIMARY KEY (`ID_ENDERECO`),
  KEY `fk_endereco_cidade_idx` (`FK_CIDADE`),
  CONSTRAINT `fk_endereco_cidade` FOREIGN KEY (`FK_CIDADE`) REFERENCES `tb_cidade` (`ID_CIDADE`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

#
# Data for table "tb_endereco"
#


#
# Structure for table "tb_pessoa_endereco"
#

DROP TABLE IF EXISTS `tb_pessoa_endereco`;
CREATE TABLE `tb_pessoa_endereco` (
  `ID_PESSOA_ENDERECO` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `FK_PESSOA` bigint(20) unsigned NOT NULL DEFAULT '0',
  `FK_ENDERECO` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID_PESSOA_ENDERECO`),
  KEY `pessoa_endereco_pessoa_fk_idx` (`FK_PESSOA`),
  KEY `pessoa_endereco_endereco_fk_idx` (`FK_ENDERECO`),
  CONSTRAINT `pessoa_endereco_endereco_fk` FOREIGN KEY (`FK_ENDERECO`) REFERENCES `tb_endereco` (`ID_ENDERECO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `pessoa_endereco_pessoa_fk` FOREIGN KEY (`FK_PESSOA`) REFERENCES `tb_pessoa` (`ID_PESSOA`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

#
# Data for table "tb_pessoa_endereco"
#


#
# Structure for table "tb_advogado"
#

DROP TABLE IF EXISTS `tb_advogado`;
CREATE TABLE `tb_advogado` (
  `ID_ADVOGADO` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identificador �nico da tabela de ADVOGADO.',
  `DS_ADVOGADO` varchar(200) COLLATE latin1_general_ci NOT NULL,
  `NR_OAB` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `FK_UF_OAB` bigint(20) unsigned NOT NULL,
  `FK_PESSOA` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'Identificador chave estrangeira da tabela TB_PESSOA',
  PRIMARY KEY (`ID_ADVOGADO`),
  UNIQUE KEY `NR_OAB_UNIQUE` (`NR_OAB`),
  UNIQUE KEY `ID_ADVOGADO_UNIQUE` (`ID_ADVOGADO`),
  KEY `ADVOGADO_UF_FK_idx` (`FK_UF_OAB`),
  KEY `ADVOGADO_PESSOA_FK_idx` (`FK_PESSOA`),
  CONSTRAINT `ADVOGADO_PESSOA_FK` FOREIGN KEY (`FK_PESSOA`) REFERENCES `tb_pessoa` (`ID_PESSOA`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `advogado_uf_fk` FOREIGN KEY (`FK_UF_OAB`) REFERENCES `tb_uf` (`ID_UF`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

#
# Data for table "tb_advogado"
#


#
# Structure for table "tb_grupo_advogado"
#

DROP TABLE IF EXISTS `tb_grupo_advogado`;
CREATE TABLE `tb_grupo_advogado` (
  `ID_GRUPO_ADVOGADO` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identificador �nico da tabela de GRUPO_ADVOGADO.',
  `FK_GRUPO` bigint(20) unsigned NOT NULL DEFAULT '0',
  `FK_ADVOGADO` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID_GRUPO_ADVOGADO`),
  KEY `GRUPO_ADV_GRUPO_FK_idx` (`FK_GRUPO`),
  KEY `GRUPO_ADV_ADVOGADO_FK_idx` (`FK_ADVOGADO`),
  CONSTRAINT `grupo_adv_advogado_fk` FOREIGN KEY (`FK_ADVOGADO`) REFERENCES `tb_advogado` (`ID_ADVOGADO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `grupo_adv_grupo_fk` FOREIGN KEY (`FK_GRUPO`) REFERENCES `tb_grupo` (`ID_GRUPO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

#
# Data for table "tb_grupo_advogado"
#


#
# Structure for table "tb_atendimento"
#

DROP TABLE IF EXISTS `tb_atendimento`;
CREATE TABLE `tb_atendimento` (
  `ID_ATENDIMENTO` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identificador unico da tabela ATENDIMENTO',
  `FK_PESSOA` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'IDENTIFICADOR CHAVE ESTRANGEIRA COM TB_PESSOA',
  `FK_ADVOGADO` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'Identificador Chave Estangeira da tabela TB_ADVOGADO',
  `FK_TIPO_ATENDIMENTO` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'Identificador do tipo de atendimento',
  `FL_ATENDIMENTO` varchar(1) COLLATE latin1_general_ci NOT NULL DEFAULT 'S' COMMENT 'Flag que indica se est� em atendimento ou atendimento ja foi finalizado',
  PRIMARY KEY (`ID_ATENDIMENTO`),
  KEY `ATENDIMENTO_PESSOA_FK_idx` (`FK_PESSOA`),
  KEY `ATENDIMENTO_ADVOGADO_FK_idx` (`FK_ADVOGADO`),
  KEY `ATENDIMENTO_TIPO_FK_idx` (`FK_TIPO_ATENDIMENTO`),
  CONSTRAINT `ATENDIMENTO_ADVOGADO_FK` FOREIGN KEY (`FK_ADVOGADO`) REFERENCES `tb_advogado` (`ID_ADVOGADO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ATENDIMENTO_PESSOA_FK` FOREIGN KEY (`FK_PESSOA`) REFERENCES `tb_pessoa` (`ID_PESSOA`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ATENDIMENTO_TIPO_FK` FOREIGN KEY (`FK_TIPO_ATENDIMENTO`) REFERENCES `tb_tipo_atendimento` (`ID_TIPO_ATENDIMENTO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

#
# Data for table "tb_atendimento"
#


#
# Structure for table "tb_andamento_atendimento"
#

DROP TABLE IF EXISTS `tb_andamento_atendimento`;
CREATE TABLE `tb_andamento_atendimento` (
  `ID_ANDAMENTO_ATENDIMENTO` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identificador unico da tabela ANDAMENTO_ATENDIMENTO',
  `FK_PESSOA` bigint(20) unsigned NOT NULL DEFAULT '0',
  `FK_ATENDIMENTO` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'Identificado chave estrangeira de ATENDIMENTO',
  `DS_DESCRICAO` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT '' COMMENT 'Descricao do atendimento',
  `BB_ANEXO` blob COMMENT 'Upload de anexos do atendimento',
  `FL_STATUS` varchar(1) COLLATE latin1_general_ci NOT NULL DEFAULT 'P' COMMENT 'P = PENDENTE; A = EM ANDAMENTO; C = CANCELADO; F = FECHADO',
  `DT_DATA_CADASTRO` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `DS_PROTOCOLO` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`ID_ANDAMENTO_ATENDIMENTO`),
  KEY `ANDAMENTO_ATENDIMENTO_PESSOA_FK_idx` (`FK_PESSOA`),
  KEY `ANDAMENTO_ATENDIMENTO_FK_FK_idx` (`FK_ATENDIMENTO`),
  CONSTRAINT `ANDAMENTO_ATENDIMENTO_FK` FOREIGN KEY (`FK_ATENDIMENTO`) REFERENCES `tb_atendimento` (`ID_ATENDIMENTO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ANDAMENTO_ATENDIMENTO_PESSOA_FK` FOREIGN KEY (`FK_PESSOA`) REFERENCES `tb_pessoa` (`ID_PESSOA`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci COMMENT='Tabela de Andamentos do Atendimento';

#
# Data for table "tb_andamento_atendimento"
#


#
# Structure for table "tb_vara"
#

DROP TABLE IF EXISTS `tb_vara`;
CREATE TABLE `tb_vara` (
  `ID_VARA` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identificador �nico da tabela de VARA.',
  `DS_VARA` varchar(200) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`ID_VARA`),
  UNIQUE KEY `DS_VARA_UNIQUE` (`DS_VARA`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci COMMENT='	';

#
# Data for table "tb_vara"
#

INSERT INTO `tb_vara` VALUES (1,'Vara 01');

#
# Structure for table "tb_processo"
#

DROP TABLE IF EXISTS `tb_processo`;
CREATE TABLE `tb_processo` (
  `ID_PROCESSO` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identificador �nico da tabela de Processo.',
  `FK_GRUPO_ADVOGADO` bigint(20) unsigned DEFAULT NULL,
  `FL_PARTES` char(1) COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  `FL_PRIVADO` char(1) CHARACTER SET latin1 NOT NULL DEFAULT 'N',
  `FL_STATUS` char(1) CHARACTER SET latin1 NOT NULL DEFAULT 'A' COMMENT 'Status do Processo= A:Ativo S:Suspenso B:Baixado',
  `FK_FASE` bigint(20) unsigned NOT NULL DEFAULT '0',
  `FK_CLASSE_PROCESSUAL` bigint(20) unsigned NOT NULL DEFAULT '0',
  `FK_NATUREZA_ACAO` bigint(20) unsigned NOT NULL DEFAULT '0',
  `FK_RITO` bigint(20) unsigned NOT NULL DEFAULT '0',
  `DS_PROCESSO` varchar(200) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `DT_ABERTURA` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `VL_CAUSA` decimal(20,2) DEFAULT NULL,
  `FL_ATUALIZA_VALOR_MONETARIAMENTE` char(1) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `FK_INDICE_CORRECAO` bigint(20) unsigned NOT NULL DEFAULT '0',
  `DT_BASE_INDICE_CORRECAO` datetime DEFAULT NULL,
  `DS_PASTA` varchar(200) COLLATE latin1_general_ci DEFAULT NULL,
  `DS_CONTROLE` varchar(200) COLLATE latin1_general_ci DEFAULT NULL,
  `FK_FORUM` bigint(20) unsigned NOT NULL DEFAULT '0',
  `FK_COMARCA` bigint(20) unsigned NOT NULL DEFAULT '0',
  `FK_VARA` bigint(20) unsigned NOT NULL DEFAULT '0',
  `DS_OBSERVACAO` varchar(4000) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`ID_PROCESSO`),
  KEY `PROCESSO_FASE_FK_idx` (`FK_FASE`),
  KEY `PROCESSO_CLASSE_PROCESSUAL_FK_idx` (`FK_CLASSE_PROCESSUAL`),
  KEY `PROCESSO_NATUREZA_ACAO_FK_idx` (`FK_NATUREZA_ACAO`),
  KEY `PROCESSO_RITO_FK_idx` (`FK_RITO`),
  KEY `PROCESSO_INDICE_CORRECAO_FK_idx` (`FK_INDICE_CORRECAO`),
  KEY `PROCESSO_FORUM_FK_idx` (`FK_FORUM`),
  KEY `PROCESSO_COMARCA_FK_idx` (`FK_COMARCA`),
  KEY `PROCESSO_VARA_FK_idx` (`FK_VARA`),
  KEY `processo_grupo_advogado_fk_idx` (`FK_GRUPO_ADVOGADO`),
  CONSTRAINT `processo_classe_processual_fk` FOREIGN KEY (`FK_CLASSE_PROCESSUAL`) REFERENCES `tb_classe_processual` (`ID_CLASSE_PROCESSUAL`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `processo_comarca_fk` FOREIGN KEY (`FK_COMARCA`) REFERENCES `tb_comarca` (`ID_COMARCA`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `processo_fase_fk` FOREIGN KEY (`FK_FASE`) REFERENCES `tb_fase` (`ID_FASE`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `processo_forum_fk` FOREIGN KEY (`FK_FORUM`) REFERENCES `tb_forum` (`ID_FORUM`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `processo_grupo_advogado_fk` FOREIGN KEY (`FK_GRUPO_ADVOGADO`) REFERENCES `tb_grupo_advogado` (`ID_GRUPO_ADVOGADO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `processo_indice_correcao_fk` FOREIGN KEY (`FK_INDICE_CORRECAO`) REFERENCES `tb_indice_correcao` (`ID_INDICE_CORRECAO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `processo_natureza_acao_fk` FOREIGN KEY (`FK_NATUREZA_ACAO`) REFERENCES `tb_natureza_acao` (`ID_NATUREZA_ACAO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `processo_rito_fk` FOREIGN KEY (`FK_RITO`) REFERENCES `tb_rito` (`ID_RITO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `processo_vara_fk` FOREIGN KEY (`FK_VARA`) REFERENCES `tb_vara` (`ID_VARA`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

#
# Data for table "tb_processo"
#


#
# Structure for table "tb_processo_pessoa"
#

DROP TABLE IF EXISTS `tb_processo_pessoa`;
CREATE TABLE `tb_processo_pessoa` (
  `ID_PROCESSO_PESSOA` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `FK_PROCESSO` bigint(20) unsigned NOT NULL DEFAULT '0',
  `FK_PESSOA` bigint(20) unsigned NOT NULL DEFAULT '0',
  `FK_POSICAO` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'Identificador da tabela POSICAO',
  PRIMARY KEY (`ID_PROCESSO_PESSOA`),
  KEY `processo_pessoa_processo_fk_idx` (`FK_PROCESSO`),
  KEY `processo_pessoa_pessoa_fk_idx` (`FK_PESSOA`),
  KEY `PROCESSO_PESSOA_POSICAO_FK_idx` (`FK_POSICAO`),
  CONSTRAINT `processo_pessoa_pessoa_fk` FOREIGN KEY (`FK_PESSOA`) REFERENCES `tb_pessoa` (`ID_PESSOA`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `PROCESSO_PESSOA_POSICAO_FK` FOREIGN KEY (`FK_POSICAO`) REFERENCES `tb_posicao` (`ID_POSICAO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `processo_pessoa_processo_fk` FOREIGN KEY (`FK_PROCESSO`) REFERENCES `tb_processo` (`ID_PROCESSO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

#
# Data for table "tb_processo_pessoa"
#


#
# Structure for table "tb_processo_advogado"
#

DROP TABLE IF EXISTS `tb_processo_advogado`;
CREATE TABLE `tb_processo_advogado` (
  `ID_PROCESSO_ADVOGADO` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identificador �nico da tabela de PROCESSO_ADVOGADO.',
  `FK_PROCESSO` bigint(20) unsigned NOT NULL DEFAULT '0',
  `FK_ADVOGADO` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID_PROCESSO_ADVOGADO`),
  KEY `PROCESSO_ADV_PROCESSO_FK_idx` (`FK_PROCESSO`),
  KEY `PROCESSO_ADV_ADVOGADO_idx` (`FK_ADVOGADO`),
  CONSTRAINT `processo_adv_advogado_fk` FOREIGN KEY (`FK_ADVOGADO`) REFERENCES `tb_advogado` (`ID_ADVOGADO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `processo_adv_processo_fk` FOREIGN KEY (`FK_PROCESSO`) REFERENCES `tb_processo` (`ID_PROCESSO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

#
# Data for table "tb_processo_advogado"
#

