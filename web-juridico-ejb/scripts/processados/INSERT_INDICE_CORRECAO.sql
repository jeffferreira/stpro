-- -----------------------------------------------------
-- Insert in Table `JURIDICO_DESENV`.`TB_INDICE_CORRECAO`
-- -----------------------------------------------------
INSERT INTO `juridico_desenv`.`tb_indice_correcao` (`ID_INDICE_CORRECAO`, `DS_INDICE_CORRECAO`) VALUES ('1', 'CDI');
INSERT INTO `juridico_desenv`.`tb_indice_correcao` (`ID_INDICE_CORRECAO`, `DS_INDICE_CORRECAO`) VALUES ('2', 'CUB (Sinduscon)');
INSERT INTO `juridico_desenv`.`tb_indice_correcao` (`ID_INDICE_CORRECAO`, `DS_INDICE_CORRECAO`) VALUES ('3', 'CUB Nacional (CBIC)');
INSERT INTO `juridico_desenv`.`tb_indice_correcao` (`ID_INDICE_CORRECAO`, `DS_INDICE_CORRECAO`) VALUES ('4', 'CUB-SC (Sinduscon)');
INSERT INTO `juridico_desenv`.`tb_indice_correcao` (`ID_INDICE_CORRECAO`, `DS_INDICE_CORRECAO`) VALUES ('5', 'ICV (Dieese)');
INSERT INTO `juridico_desenv`.`tb_indice_correcao` (`ID_INDICE_CORRECAO`, `DS_INDICE_CORRECAO`) VALUES ('6', 'IGP-DI (FGV)');
INSERT INTO `juridico_desenv`.`tb_indice_correcao` (`ID_INDICE_CORRECAO`, `DS_INDICE_CORRECAO`) VALUES ('7', 'IGP-M (FGV)');
INSERT INTO `juridico_desenv`.`tb_indice_correcao` (`ID_INDICE_CORRECAO`, `DS_INDICE_CORRECAO`) VALUES ('8', 'INCC-DI (FGV)');
INSERT INTO `juridico_desenv`.`tb_indice_correcao` (`ID_INDICE_CORRECAO`, `DS_INDICE_CORRECAO`) VALUES ('9', 'INPC (IBGE)');
INSERT INTO `juridico_desenv`.`tb_indice_correcao` (`ID_INDICE_CORRECAO`, `DS_INDICE_CORRECAO`) VALUES ('10', 'IPA-DI (FGV)');
INSERT INTO `juridico_desenv`.`tb_indice_correcao` (`ID_INDICE_CORRECAO`, `DS_INDICE_CORRECAO`) VALUES ('11', 'IPA-M (FGV)');
INSERT INTO `juridico_desenv`.`tb_indice_correcao` (`ID_INDICE_CORRECAO`, `DS_INDICE_CORRECAO`) VALUES ('12', 'IPC (Fipe)');
INSERT INTO `juridico_desenv`.`tb_indice_correcao` (`ID_INDICE_CORRECAO`, `DS_INDICE_CORRECAO`) VALUES ('13', 'IPC (IBGE)');
INSERT INTO `juridico_desenv`.`tb_indice_correcao` (`ID_INDICE_CORRECAO`, `DS_INDICE_CORRECAO`) VALUES ('14', 'IPC-DI (FGV)');
INSERT INTO `juridico_desenv`.`tb_indice_correcao` (`ID_INDICE_CORRECAO`, `DS_INDICE_CORRECAO`) VALUES ('15', 'IPC-r');
INSERT INTO `juridico_desenv`.`tb_indice_correcao` (`ID_INDICE_CORRECAO`, `DS_INDICE_CORRECAO`) VALUES ('16', 'IPCA (IBGE)');
INSERT INTO `juridico_desenv`.`tb_indice_correcao` (`ID_INDICE_CORRECAO`, `DS_INDICE_CORRECAO`) VALUES ('17', 'IPCA-E (IBGE)');
INSERT INTO `juridico_desenv`.`tb_indice_correcao` (`ID_INDICE_CORRECAO`, `DS_INDICE_CORRECAO`) VALUES ('18', 'Média do INPC (IBGE) e IGP-DI (FGV)');
INSERT INTO `juridico_desenv`.`tb_indice_correcao` (`ID_INDICE_CORRECAO`, `DS_INDICE_CORRECAO`) VALUES ('19', 'Poupança');
INSERT INTO `juridico_desenv`.`tb_indice_correcao` (`ID_INDICE_CORRECAO`, `DS_INDICE_CORRECAO`) VALUES ('20', 'Selic (cálculo capitalizada mensalmente)');
INSERT INTO `juridico_desenv`.`tb_indice_correcao` (`ID_INDICE_CORRECAO`, `DS_INDICE_CORRECAO`) VALUES ('21', 'TBF');
INSERT INTO `juridico_desenv`.`tb_indice_correcao` (`ID_INDICE_CORRECAO`, `DS_INDICE_CORRECAO`) VALUES ('22', 'TJLP (BACEN)');
INSERT INTO `juridico_desenv`.`tb_indice_correcao` (`ID_INDICE_CORRECAO`, `DS_INDICE_CORRECAO`) VALUES ('23', 'TJLP (RECEITA FEDERAL)');
INSERT INTO `juridico_desenv`.`tb_indice_correcao` (`ID_INDICE_CORRECAO`, `DS_INDICE_CORRECAO`) VALUES ('24', 'TR (Bacen)');
