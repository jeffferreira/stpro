-- -----------------------------------------------------
-- Insert in Table `JURIDICO_DESENV`.`TB_CLASSE_PROCESSUAL`
-- -----------------------------------------------------
INSERT INTO `juridico_desenv`.`tb_classe_processual` (`ID_CLASSE_PROCESSUAL`, `DS_CLASSE_PROCESSUAL`) VALUES ('1', 'Ação de cobrança');
INSERT INTO `juridico_desenv`.`tb_classe_processual` (`ID_CLASSE_PROCESSUAL`, `DS_CLASSE_PROCESSUAL`) VALUES ('2', 'Ação de despejo');
INSERT INTO `juridico_desenv`.`tb_classe_processual` (`ID_CLASSE_PROCESSUAL`, `DS_CLASSE_PROCESSUAL`) VALUES ('3', 'Ação de indenização');
INSERT INTO `juridico_desenv`.`tb_classe_processual` (`ID_CLASSE_PROCESSUAL`, `DS_CLASSE_PROCESSUAL`) VALUES ('4', 'Divórcio');
INSERT INTO `juridico_desenv`.`tb_classe_processual` (`ID_CLASSE_PROCESSUAL`, `DS_CLASSE_PROCESSUAL`) VALUES ('5', 'Execução de alimentos');
INSERT INTO `juridico_desenv`.`tb_classe_processual` (`ID_CLASSE_PROCESSUAL`, `DS_CLASSE_PROCESSUAL`) VALUES ('6', 'Impugnação do valor da causa');
