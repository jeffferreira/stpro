-- -----------------------------------------------------
-- Table `JURIDICO_DESENV`.`TB_GRUPO`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `JURIDICO_DESENV`.`TB_GRUPO` (
  `ID_GRUPO` INT(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Identificador único da tabela de GRUPO.',
  `DS_GRUPO` VARCHAR(200) NOT NULL ,
  PRIMARY KEY (`ID_GRUPO`),
  UNIQUE INDEX `DS_GRUPO_UNIQUE` (`DS_GRUPO` ASC) )
ENGINE = InnoDB;