-- -----------------------------------------------------
-- Table `JURIDICO_DESENV`.`TB_PARTE`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `JURIDICO_DESENV`.`TB_PARTE` (
  `ID_PARTE` INT(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Identificador único da tabela de PARTE.',
  `FK_POSICAO_PARTE` INT(20) NOT NULL ,
  `DS_NOME` VARCHAR(200) NOT NULL ,
  PRIMARY KEY (`ID_PARTE`) )
ENGINE = InnoDB;
