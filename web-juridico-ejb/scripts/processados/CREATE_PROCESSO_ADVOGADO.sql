-- -----------------------------------------------------
-- Table `JURIDICO_DESENV`.`TB_PROCESSO_ADVOGADO`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `JURIDICO_DESENV`.`TB_PROCESSO_ADVOGADO` (
  `ID_PROCESSO_ADVOGADO` INT(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Identificador único da tabela de PROCESSO_ADVOGADO.',
  `FK_PROCESSO` INT(20) NOT NULL ,
  `FK_ADVOGADO` INT(20) NOT NULL ,
  PRIMARY KEY (`ID_PROCESSO_ADVOGADO`) )
ENGINE = InnoDB;