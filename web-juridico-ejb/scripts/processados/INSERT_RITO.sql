-- -----------------------------------------------------
-- Insert in Table `JURIDICO_DESENV`.`TB_RITO`
-- -----------------------------------------------------
INSERT INTO `juridico_desenv`.`tb_rito` (`ID_RITO`, `DS_RITO`) VALUES ('1', 'Não definido');
INSERT INTO `juridico_desenv`.`tb_rito` (`ID_RITO`, `DS_RITO`) VALUES ('2', 'Especial');
INSERT INTO `juridico_desenv`.`tb_rito` (`ID_RITO`, `DS_RITO`) VALUES ('3', 'Ordinário');
INSERT INTO `juridico_desenv`.`tb_rito` (`ID_RITO`, `DS_RITO`) VALUES ('4', 'Sumário');
INSERT INTO `juridico_desenv`.`tb_rito` (`ID_RITO`, `DS_RITO`) VALUES ('5', 'Sumaríssimo');
