-- -----------------------------------------------------
-- Table `JURIDICO_DESENV`.`TB_RITO`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `JURIDICO_DESENV`.`TB_RITO` (
  `ID_RITO` INT(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Identificador único da tabela de RITO.',
  `DS_RITO` VARCHAR(200) NULL ,
  PRIMARY KEY (`ID_RITO`) ,
  UNIQUE INDEX `DS_RITO_UNIQUE` (`DS_RITO` ASC) )
COMMENT = '	'
ENGINE = InnoDB;