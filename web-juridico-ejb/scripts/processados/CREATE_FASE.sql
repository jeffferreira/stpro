-- -----------------------------------------------------
-- Table `JURIDICO_DESENV`.`TB_FASE`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `JURIDICO_DESENV`.`TB_FASE` (
  `ID_FASE` INT(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Identificador único da tabela de FASE.',
  `DS_FASE` VARCHAR(200) NULL ,
  PRIMARY KEY (`ID_FASE`) ,
  UNIQUE INDEX `DS_FASE_UNIQUE` (`DS_FASE` ASC) )
COMMENT = '	'
ENGINE = InnoDB;