-- -----------------------------------------------------
-- Table `JURIDICO_DESENV`.`TB_UF`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `JURIDICO_DESENV`.`TB_UF` (
  `ID_UF` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Identificador único da tabela de UF.',
  `SG_UF` CHAR(2) NOT NULL COMMENT 'Sigla da UF.',
  `NO_UF` VARCHAR(45) NOT NULL COMMENT 'Nome da UF. Exemplo: São Paulo',
  PRIMARY KEY (`ID_UF`),
  UNIQUE INDEX `SG_UF_UNIQUE` (`SG_UF` ASC))
ENGINE = InnoDB;