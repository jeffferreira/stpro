-- -----------------------------------------------------
-- Table `JURIDICO_DESENV`.`TB_GRUPO_ADVOGADO`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `JURIDICO_DESENV`.`TB_GRUPO_ADVOGADO` (
  `ID_GRUPO_ADVOGADO` INT(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Identificador único da tabela de GRUPO_ADVOGADO.',
  `FK_GRUPO` INT(20) NOT NULL ,
  `FK_ADVOGADO` INT(20) NOT NULL ,
  PRIMARY KEY (`ID_GRUPO_ADVOGADO`) )
ENGINE = InnoDB;