-- -----------------------------------------------------
-- Table `JURIDICO_DESENV`.`TB_FORUM`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `JURIDICO_DESENV`.`TB_FORUM` (
  `ID_FORUM` INT(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Identificador único da tabela de FORUM.',
  `DS_FORUM` VARCHAR(200) NULL ,
  PRIMARY KEY (`ID_FORUM`) ,
  UNIQUE INDEX `DS_FORUM_UNIQUE` (`DS_FORUM` ASC) )
COMMENT = '	'
ENGINE = InnoDB;