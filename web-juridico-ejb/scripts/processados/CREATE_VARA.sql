-- -----------------------------------------------------
-- Table `JURIDICO_DESENV`.`TB_VARA`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `JURIDICO_DESENV`.`TB_VARA` (
  `ID_VARA` INT(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Identificador único da tabela de VARA.',
  `DS_VARA` VARCHAR(200) NULL ,
  PRIMARY KEY (`ID_VARA`) ,
  UNIQUE INDEX `DS_VARA_UNIQUE` (`DS_VARA` ASC) )
COMMENT = '	'
ENGINE = InnoDB;