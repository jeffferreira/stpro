-- -----------------------------------------------------
-- Insert in Table `JURIDICO_DESENV`.`TB_NATUREZA_ACAO`
-- -----------------------------------------------------
INSERT INTO `juridico_desenv`.`tb_natureza_acao` (`ID_NATUREZA_ACAO`, `DS_NATUREZA_ACAO`) VALUES ('1', 'Não definido');
INSERT INTO `juridico_desenv`.`tb_natureza_acao` (`ID_NATUREZA_ACAO`, `DS_NATUREZA_ACAO`) VALUES ('2', 'Cível');
INSERT INTO `juridico_desenv`.`tb_natureza_acao` (`ID_NATUREZA_ACAO`, `DS_NATUREZA_ACAO`) VALUES ('3', 'Criminal');
INSERT INTO `juridico_desenv`.`tb_natureza_acao` (`ID_NATUREZA_ACAO`, `DS_NATUREZA_ACAO`) VALUES ('4', 'Família');
INSERT INTO `juridico_desenv`.`tb_natureza_acao` (`ID_NATUREZA_ACAO`, `DS_NATUREZA_ACAO`) VALUES ('5', 'Trabalhista');
