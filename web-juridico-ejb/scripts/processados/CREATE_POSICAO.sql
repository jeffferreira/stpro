-- -----------------------------------------------------
-- Table `JURIDICO_DESENV`.`TB_POSICAO`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `JURIDICO_DESENV`.`TB_POSICAO` (
  `ID_POSICAO` INT(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Identificador único da tabela de POSIÇÃO.',
  `DS_POSICAO` VARCHAR(200) NOT NULL ,
  PRIMARY KEY (`ID_POSICAO`),
  UNIQUE INDEX `DS_POSICAO_UNIQUE` (`DS_POSICAO` ASC) )
ENGINE = InnoDB;
