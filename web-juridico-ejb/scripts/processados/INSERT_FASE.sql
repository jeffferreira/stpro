-- -----------------------------------------------------
-- Insert in Table `JURIDICO_DESENV`.`TB_FASE`
-- -----------------------------------------------------
INSERT INTO `juridico_desenv`.`tb_fase` (`ID_FASE`, `DS_FASE`) VALUES ('1', 'Sem fase');
INSERT INTO `juridico_desenv`.`tb_fase` (`ID_FASE`, `DS_FASE`) VALUES ('2', 'Execução');
INSERT INTO `juridico_desenv`.`tb_fase` (`ID_FASE`, `DS_FASE`) VALUES ('3', 'Inicial');
INSERT INTO `juridico_desenv`.`tb_fase` (`ID_FASE`, `DS_FASE`) VALUES ('4', 'Final');
INSERT INTO `juridico_desenv`.`tb_fase` (`ID_FASE`, `DS_FASE`) VALUES ('5', 'Recursal');
