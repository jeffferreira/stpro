-- -----------------------------------------------------
-- Table `JURIDICO_DESENV`.`TB_ADVOGADO`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `JURIDICO_DESENV`.`TB_ADVOGADO` (
  `ID_ADVOGADO` INT(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Identificador único da tabela de ADVOGADO.',
  `DS_ADVOGADO` VARCHAR(200) NOT NULL ,
  `NR_OAB` VARCHAR(20) NOT NULL ,
  `FK_UF_OAB` INT(20) NOT NULL ,
  PRIMARY KEY (`ID_ADVOGADO`),
  UNIQUE INDEX `NR_OAB_UNIQUE` (`NR_OAB` ASC)  )
ENGINE = InnoDB;