-- -----------------------------------------------------
-- Insert in Table `JURIDICO_DESENV`.`TB_TIPO_CONTATO`
-- -----------------------------------------------------
INSERT INTO `juridico_desenv`.`tb_tipo_contato` (`ID_TIPO_CONTATO`, `DS_TIPO_CONTATO`) VALUES ('1', 'Telefone');
INSERT INTO `juridico_desenv`.`tb_tipo_contato` (`ID_TIPO_CONTATO`, `DS_TIPO_CONTATO`) VALUES ('2', 'Celular');
INSERT INTO `juridico_desenv`.`tb_tipo_contato` (`ID_TIPO_CONTATO`, `DS_TIPO_CONTATO`) VALUES ('3', 'Residencial');
INSERT INTO `juridico_desenv`.`tb_tipo_contato` (`ID_TIPO_CONTATO`, `DS_TIPO_CONTATO`) VALUES ('4', 'Comercial');
INSERT INTO `juridico_desenv`.`tb_tipo_contato` (`ID_TIPO_CONTATO`, `DS_TIPO_CONTATO`) VALUES ('5', 'Fax');
