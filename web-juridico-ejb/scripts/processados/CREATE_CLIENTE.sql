-- -----------------------------------------------------
-- Table `JURIDICO_DESENV`.`TB_CLIENTE`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `JURIDICO_DESENV`.`TB_CLIENTE` (
  `ID_CLIENTE` INT(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Identificador único da tabela de CLIENTE.',
  `FK_POSICAO_CLIENTE` INT(20) NOT NULL ,
  `DS_NOME` VARCHAR(200) NOT NULL ,
  PRIMARY KEY (`ID_CLIENTE`) )
ENGINE = InnoDB;