-- -----------------------------------------------------
-- Insert in Table `JURIDICO_DESENV`.`TB_POSICAO`
-- -----------------------------------------------------
INSERT INTO `juridico_desenv`.`tb_posicao` (`ID_POSICAO`, `DS_POSICAO`) VALUES ('1', 'Adverso');
INSERT INTO `juridico_desenv`.`tb_posicao` (`ID_POSICAO`, `DS_POSICAO`) VALUES ('2', 'Advogado');
INSERT INTO `juridico_desenv`.`tb_posicao` (`ID_POSICAO`, `DS_POSICAO`) VALUES ('3', 'Advogado Adverso');
INSERT INTO `juridico_desenv`.`tb_posicao` (`ID_POSICAO`, `DS_POSICAO`) VALUES ('4', 'Autor');
INSERT INTO `juridico_desenv`.`tb_posicao` (`ID_POSICAO`, `DS_POSICAO`) VALUES ('5', 'Cliente');
INSERT INTO `juridico_desenv`.`tb_posicao` (`ID_POSICAO`, `DS_POSICAO`) VALUES ('6', 'Reclamada');
INSERT INTO `juridico_desenv`.`tb_posicao` (`ID_POSICAO`, `DS_POSICAO`) VALUES ('7', 'Reclamante');
INSERT INTO `juridico_desenv`.`tb_posicao` (`ID_POSICAO`, `DS_POSICAO`) VALUES ('8', 'Relator');
INSERT INTO `juridico_desenv`.`tb_posicao` (`ID_POSICAO`, `DS_POSICAO`) VALUES ('9', 'Requerente');
INSERT INTO `juridico_desenv`.`tb_posicao` (`ID_POSICAO`, `DS_POSICAO`) VALUES ('10', 'Requerido');
INSERT INTO `juridico_desenv`.`tb_posicao` (`ID_POSICAO`, `DS_POSICAO`) VALUES ('11', 'Réu');
INSERT INTO `juridico_desenv`.`tb_posicao` (`ID_POSICAO`, `DS_POSICAO`) VALUES ('12', 'Testemunha');
