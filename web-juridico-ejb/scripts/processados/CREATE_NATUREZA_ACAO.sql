-- -----------------------------------------------------
-- Table `JURIDICO_DESENV`.`TB_NATUREZA_ACAO`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `JURIDICO_DESENV`.`TB_NATUREZA_ACAO` (
  `ID_NATUREZA_ACAO` INT(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Identificador único da tabela de NATUREZA AÇÃO.',
  `DS_NATUREZA_ACAO` VARCHAR(200) NULL ,
  PRIMARY KEY (`ID_NATUREZA_ACAO`) ,
  UNIQUE INDEX `DS_NATUREZA_ACAO_UNIQUE` (`DS_NATUREZA_ACAO` ASC) )
COMMENT = '	'
ENGINE = InnoDB;