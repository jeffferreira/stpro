-- -----------------------------------------------------
-- Table `JURIDICO_DESENV`.`TB_TIPO_CONTATO`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `JURIDICO_DESENV`.`TB_TIPO_CONTATO` (
  `ID_TIPO_CONTATO` INT(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Identificador único da tabela de TIPO_CONTATO.',
  `DS_TIPO_CONTATO` VARCHAR(200) NOT NULL ,
  PRIMARY KEY (`ID_TIPO_CONTATO`),
  UNIQUE INDEX `DS_TIPO_CONTATO_UNIQUE` (`DS_TIPO_CONTATO` ASC) )
ENGINE = InnoDB;