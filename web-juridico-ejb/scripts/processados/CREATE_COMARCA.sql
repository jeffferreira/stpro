-- -----------------------------------------------------
-- Table `JURIDICO_DESENV`.`TB_COMARCA`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `JURIDICO_DESENV`.`TB_COMARCA` (
  `ID_COMARCA` INT(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Identificador único da tabela de COMARCA.',
  `DS_COMARCA` VARCHAR(200) NULL ,
  PRIMARY KEY (`ID_COMARCA`) ,
  UNIQUE INDEX `DS_COMARCA_UNIQUE` (`DS_COMARCA` ASC) )
COMMENT = '	'
ENGINE = InnoDB;