-- -----------------------------------------------------
-- Table `JURIDICO_DESENV`.`TB_ADVERSO`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `JURIDICO_DESENV`.`TB_ADVERSO` (
  `ID_ADVERSO` INT(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Identificador único da tabela de ADVERSO.',
  `FK_POSICAO_ADVERSO` INT(20) NOT NULL ,
  `DS_NOME` VARCHAR(200) NOT NULL ,
  PRIMARY KEY (`ID_ADVERSO`) )
ENGINE = InnoDB;